//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from: Cmd.proto
namespace Cmd
{
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"GetBabyInfo")]
  public partial class GetBabyInfo : global::ProtoBuf.IExtensible
  {
    public GetBabyInfo() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ReturnBabyInfo")]
  public partial class ReturnBabyInfo : global::ProtoBuf.IExtensible
  {
    public ReturnBabyInfo() {}
    
    private Status _status;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"status", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public Status status
    {
      get { return _status; }
      set { _status = value; }
    }
    private string _info;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string info
    {
      get { return _info; }
      set { _info = value; }
    }

    private BabyInfo _babyInfo = null;
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"babyInfo", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue(null)]
    public BabyInfo babyInfo
    {
      get { return _babyInfo; }
      set { _babyInfo = value; }
    }

    private int _server_time = default(int);
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"server_time", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int server_time
    {
      get { return _server_time; }
      set { _server_time = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"BabyInfo")]
  public partial class BabyInfo : global::ProtoBuf.IExtensible
  {
    public BabyInfo() {}
    

    private string _name = "";
    [global::ProtoBuf.ProtoMember(1, IsRequired = false, Name=@"name", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string name
    {
      get { return _name; }
      set { _name = value; }
    }

    private int _age_range = default(int);
    [global::ProtoBuf.ProtoMember(2, IsRequired = false, Name=@"age_range", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int age_range
    {
      get { return _age_range; }
      set { _age_range = value; }
    }

    private int _gender = default(int);
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"gender", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int gender
    {
      get { return _gender; }
      set { _gender = value; }
    }

    private int _rest_switch = default(int);
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"rest_switch", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int rest_switch
    {
      get { return _rest_switch; }
      set { _rest_switch = value; }
    }

    private string _rest_start_time = "";
    [global::ProtoBuf.ProtoMember(5, IsRequired = false, Name=@"rest_start_time", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string rest_start_time
    {
      get { return _rest_start_time; }
      set { _rest_start_time = value; }
    }

    private string _rest_end_time = "";
    [global::ProtoBuf.ProtoMember(6, IsRequired = false, Name=@"rest_end_time", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string rest_end_time
    {
      get { return _rest_end_time; }
      set { _rest_end_time = value; }
    }

    private int _play_finite_time = default(int);
    [global::ProtoBuf.ProtoMember(7, IsRequired = false, Name=@"play_finite_time", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int play_finite_time
    {
      get { return _play_finite_time; }
      set { _play_finite_time = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ChangeBabyInfo")]
  public partial class ChangeBabyInfo : global::ProtoBuf.IExtensible
  {
    public ChangeBabyInfo() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }
    private BabyInfo _babyInfo;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"babyInfo", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public BabyInfo babyInfo
    {
      get { return _babyInfo; }
      set { _babyInfo = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"GetCommonData")]
  public partial class GetCommonData : global::ProtoBuf.IExtensible
  {
    public GetCommonData() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ReturnCommonData")]
  public partial class ReturnCommonData : global::ProtoBuf.IExtensible
  {
    public ReturnCommonData() {}
    
    private Status _status;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"status", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public Status status
    {
      get { return _status; }
      set { _status = value; }
    }
    private string _info;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string info
    {
      get { return _info; }
      set { _info = value; }
    }
    private readonly global::System.Collections.Generic.List<CourseInfo> _courseInfo = new global::System.Collections.Generic.List<CourseInfo>();
    [global::ProtoBuf.ProtoMember(3, Name=@"courseInfo", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public global::System.Collections.Generic.List<CourseInfo> courseInfo
    {
      get { return _courseInfo; }
    }
  

    private int _server_time = default(int);
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"server_time", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int server_time
    {
      get { return _server_time; }
      set { _server_time = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"Default_out")]
  public partial class Default_out : global::ProtoBuf.IExtensible
  {
    public Default_out() {}
    
    private Status _status;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"status", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public Status status
    {
      get { return _status; }
      set { _status = value; }
    }
    private string _info;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string info
    {
      get { return _info; }
      set { _info = value; }
    }

    private string _message = "";
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"message", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string message
    {
      get { return _message; }
      set { _message = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"InitDeliveryData")]
  public partial class InitDeliveryData : global::ProtoBuf.IExtensible
  {
    public InitDeliveryData() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ReturnDeliveryData")]
  public partial class ReturnDeliveryData : global::ProtoBuf.IExtensible
  {
    public ReturnDeliveryData() {}
    
    private Status _status;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"status", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public Status status
    {
      get { return _status; }
      set { _status = value; }
    }
    private string _info;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string info
    {
      get { return _info; }
      set { _info = value; }
    }

    private int _energy = default(int);
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"energy", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int energy
    {
      get { return _energy; }
      set { _energy = value; }
    }

    private int _total_photo_num = default(int);
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"total_photo_num", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int total_photo_num
    {
      get { return _total_photo_num; }
      set { _total_photo_num = value; }
    }
    private readonly global::System.Collections.Generic.List<PhotoInfo> _my_photo_info = new global::System.Collections.Generic.List<PhotoInfo>();
    [global::ProtoBuf.ProtoMember(5, Name=@"my_photo_info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public global::System.Collections.Generic.List<PhotoInfo> my_photo_info
    {
      get { return _my_photo_info; }
    }
  

    private string _bonus_energy_time = "";
    [global::ProtoBuf.ProtoMember(6, IsRequired = false, Name=@"bonus_energy_time", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string bonus_energy_time
    {
      get { return _bonus_energy_time; }
      set { _bonus_energy_time = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"PhotoInfo")]
  public partial class PhotoInfo : global::ProtoBuf.IExtensible
  {
    public PhotoInfo() {}
    
    private string _photo_id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"photo_id", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string photo_id
    {
      get { return _photo_id; }
      set { _photo_id = value; }
    }

    private string _title = "";
    [global::ProtoBuf.ProtoMember(2, IsRequired = false, Name=@"title", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string title
    {
      get { return _title; }
      set { _title = value; }
    }

    private int _area_type = default(int);
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"area_type", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int area_type
    {
      get { return _area_type; }
      set { _area_type = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"Travel")]
  public partial class Travel : global::ProtoBuf.IExtensible
  {
    public Travel() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"TakePhoto")]
  public partial class TakePhoto : global::ProtoBuf.IExtensible
  {
    public TakePhoto() {}
    
    private Status _status;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"status", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public Status status
    {
      get { return _status; }
      set { _status = value; }
    }
    private string _info;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string info
    {
      get { return _info; }
      set { _info = value; }
    }

    private PhotoInfo _photo_info = null;
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"photo_info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue(null)]
    public PhotoInfo photo_info
    {
      get { return _photo_info; }
      set { _photo_info = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"GetCourseList")]
  public partial class GetCourseList : global::ProtoBuf.IExtensible
  {
    public GetCourseList() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ReturnCourseList")]
  public partial class ReturnCourseList : global::ProtoBuf.IExtensible
  {
    public ReturnCourseList() {}
    
    private Status _status;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"status", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public Status status
    {
      get { return _status; }
      set { _status = value; }
    }
    private string _info;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string info
    {
      get { return _info; }
      set { _info = value; }
    }
    private readonly global::System.Collections.Generic.List<CourseInfo> _courseInfo = new global::System.Collections.Generic.List<CourseInfo>();
    [global::ProtoBuf.ProtoMember(3, Name=@"courseInfo", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public global::System.Collections.Generic.List<CourseInfo> courseInfo
    {
      get { return _courseInfo; }
    }
  
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"CourseInfo")]
  public partial class CourseInfo : global::ProtoBuf.IExtensible
  {
    public CourseInfo() {}
    
    private int _course_id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"course_id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int course_id
    {
      get { return _course_id; }
      set { _course_id = value; }
    }

    private string _name = "";
    [global::ProtoBuf.ProtoMember(2, IsRequired = false, Name=@"name", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string name
    {
      get { return _name; }
      set { _name = value; }
    }

    private int _price = default(int);
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"price", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int price
    {
      get { return _price; }
      set { _price = value; }
    }

    private string _cover_url = "";
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"cover_url", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string cover_url
    {
      get { return _cover_url; }
      set { _cover_url = value; }
    }

    private string _source_url = "";
    [global::ProtoBuf.ProtoMember(5, IsRequired = false, Name=@"source_url", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string source_url
    {
      get { return _source_url; }
      set { _source_url = value; }
    }

    private int _putaway_time = default(int);
    [global::ProtoBuf.ProtoMember(6, IsRequired = false, Name=@"putaway_time", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int putaway_time
    {
      get { return _putaway_time; }
      set { _putaway_time = value; }
    }

    private int _is_recommend = default(int);
    [global::ProtoBuf.ProtoMember(7, IsRequired = false, Name=@"is_recommend", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int is_recommend
    {
      get { return _is_recommend; }
      set { _is_recommend = value; }
    }

    private int _hot_index = default(int);
    [global::ProtoBuf.ProtoMember(8, IsRequired = false, Name=@"hot_index", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int hot_index
    {
      get { return _hot_index; }
      set { _hot_index = value; }
    }

    private int _is_buy = default(int);
    [global::ProtoBuf.ProtoMember(9, IsRequired = false, Name=@"is_buy", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int is_buy
    {
      get { return _is_buy; }
      set { _is_buy = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"Pay")]
  public partial class Pay : global::ProtoBuf.IExtensible
  {
    public Pay() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }
    private int _product_id;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"product_id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int product_id
    {
      get { return _product_id; }
      set { _product_id = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ReturnOrderSn")]
  public partial class ReturnOrderSn : global::ProtoBuf.IExtensible
  {
    public ReturnOrderSn() {}
    
    private Status _status;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"status", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public Status status
    {
      get { return _status; }
      set { _status = value; }
    }
    private string _info;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string info
    {
      get { return _info; }
      set { _info = value; }
    }

    private string _order_sn = "";
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"order_sn", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string order_sn
    {
      get { return _order_sn; }
      set { _order_sn = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"Validate")]
  public partial class Validate : global::ProtoBuf.IExtensible
  {
    public Validate() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }
    private string _order_sn;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"order_sn", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string order_sn
    {
      get { return _order_sn; }
      set { _order_sn = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ReturnOrderStatus")]
  public partial class ReturnOrderStatus : global::ProtoBuf.IExtensible
  {
    public ReturnOrderStatus() {}
    
    private Status _status;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"status", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public Status status
    {
      get { return _status; }
      set { _status = value; }
    }
    private string _info;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string info
    {
      get { return _info; }
      set { _info = value; }
    }

    private string _code = "";
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"code", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string code
    {
      get { return _code; }
      set { _code = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"LoginInfo")]
  public partial class LoginInfo : global::ProtoBuf.IExtensible
  {
    public LoginInfo() {}
    
    private int _type;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"type", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int type
    {
      get { return _type; }
      set { _type = value; }
    }
    private string _mac;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"mac", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string mac
    {
      get { return _mac; }
      set { _mac = value; }
    }

    private string _open_uuid = "";
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"open_uuid", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string open_uuid
    {
      get { return _open_uuid; }
      set { _open_uuid = value; }
    }

    private string _phone = "";
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"phone", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string phone
    {
      get { return _phone; }
      set { _phone = value; }
    }

    private string _extra = "";
    [global::ProtoBuf.ProtoMember(5, IsRequired = false, Name=@"extra", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string extra
    {
      get { return _extra; }
      set { _extra = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ReturnLoginStatus")]
  public partial class ReturnLoginStatus : global::ProtoBuf.IExtensible
  {
    public ReturnLoginStatus() {}
    
    private Status _status;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"status", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public Status status
    {
      get { return _status; }
      set { _status = value; }
    }
    private string _info;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string info
    {
      get { return _info; }
      set { _info = value; }
    }

    private UserInfo _user_info = null;
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"user_info", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue(null)]
    public UserInfo user_info
    {
      get { return _user_info; }
      set { _user_info = value; }
    }

    private int _server_time = default(int);
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"server_time", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int server_time
    {
      get { return _server_time; }
      set { _server_time = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"UserInfo")]
  public partial class UserInfo : global::ProtoBuf.IExtensible
  {
    public UserInfo() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }

    private string _wx_nickname = "";
    [global::ProtoBuf.ProtoMember(2, IsRequired = false, Name=@"wx_nickname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string wx_nickname
    {
      get { return _wx_nickname; }
      set { _wx_nickname = value; }
    }

    private string _wx_open_uuid = "";
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"wx_open_uuid", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string wx_open_uuid
    {
      get { return _wx_open_uuid; }
      set { _wx_open_uuid = value; }
    }

    private string _phone = "";
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"phone", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string phone
    {
      get { return _phone; }
      set { _phone = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"WalletInfo")]
  public partial class WalletInfo : global::ProtoBuf.IExtensible
  {
    public WalletInfo() {}
    
    private int _id;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"id", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int id
    {
      get { return _id; }
      set { _id = value; }
    }
    private int _energy;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"energy", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int energy
    {
      get { return _energy; }
      set { _energy = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
    [global::ProtoBuf.ProtoContract(Name=@"Status")]
    public enum Status
    {
            
      [global::ProtoBuf.ProtoEnum(Name=@"FAILURE", Value=0)]
      FAILURE = 0,
            
      [global::ProtoBuf.ProtoEnum(Name=@"SUCCESS", Value=1)]
      SUCCESS = 1,
            
      [global::ProtoBuf.ProtoEnum(Name=@"MESSAGE", Value=2)]
      MESSAGE = 2
    }
  
}
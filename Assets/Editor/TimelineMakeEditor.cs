﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TimelineMake))]
public class TimelineMakeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TimelineMake myScript = target as TimelineMake;
        

        if (GUILayout.Button("加载Timeline（从AllData中加载）"))
        {
            myScript.LoadTimelineForOld();
        }
        if (GUILayout.Button("加载Timeline"))
        {
            myScript.LoadTimeline();
        }
        if (GUILayout.Button("保存Timeline"))
        {
            myScript.SaveTimeline();
        }
        if (GUILayout.Button("卸载Timeline"))
        {
            myScript.DisposeTimeline();
        }
        if (GUILayout.Button("删除Timeline"))
        {
            myScript.DeleteTimeline();
        }
    }
}

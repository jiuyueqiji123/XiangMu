﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Playables;

public class TimelineEditor
{

    private static string savePath = "/Resources/game_timeline/timeline_data/timelinealldata.json";
    private static string newSavePath = "/Resources/game_timeline/timeline_data/timeline_newdata.json";
    public const string TIMELINE_DATA_BASEPATH = "Assets/Resources/game_timeline/timeline_single_data/";

    [MenuItem("TimelineTool/SaveTimelineData")]
    public static void ExportTimelineData()
    {
        GameObject go = GameObject.Find("Timeline");

        TimelineData allData = new TimelineData();

        foreach (Transform child in go.transform)
        {
            TimelineSingleData singleData = new TimelineSingleData();
            Debug.Log(child.name);
            PlayableDirector director = child.GetComponent<PlayableDirector>();
            if (director != null)
            {
                IEnumerable<PlayableBinding> Bindings = director.playableAsset.outputs;

                singleData.ID = child.name;
                foreach (PlayableBinding bindind in Bindings)
                {
                    Object obj = director.GetGenericBinding(bindind.sourceObject);
                    if (obj != null)
                    {
                        Debug.Log("obj name:" + obj.name);
                      

                        TrackData trackData = new TrackData();
                        trackData.trackName = bindind.streamName;
                        trackData.objName = obj.name;
                        GameObject objGo = obj as GameObject;
                        if (objGo)
                        {
                            
                            TLIdentify_Parent indentify = objGo.GetComponent<TLIdentify_Parent>();
                            if (indentify != null)
                            {
                                trackData.bIsIdentify_Parent = true;
                                trackData.storyName = indentify.storyname.ToString();
                                singleData.trackParentList.Add(trackData);
                                
                            }
                            else
                            {
                                trackData.bIsIdentify_Parent = false;
                                TLIdentify_Parent indentifyParent = objGo.GetComponentInParent<TLIdentify_Parent>();
                                if (indentifyParent != null)
                                {
                                    trackData.parentName = indentifyParent.name;
                                    singleData.trackChildList.Add(trackData);
                                    Debug.Log("parentName name:" + indentifyParent.name);
                                }
                            }
                        }
                        else
                        {
                            trackData.bIsIdentify_Parent = false;
                            trackData.parentName = "Audio_Parent";
                            singleData.trackAudioList.Add(trackData);
                            Debug.Log("parentName name: Audio_Parent");
                        }
                    }
                }
                allData.timelineList.Add(singleData);
            }
        }
      
        Debug.Log(allData.timelineList.Count);
        string jsonData = JsonUtility.ToJson(allData);
        
        Debug.Log(jsonData);

        File.WriteAllText(Application.dataPath + savePath, jsonData);

        AssetDatabase.Refresh();

    }


    [MenuItem("TimelineTool/合并所有timeline数据")]
    public static void ExportAllTimelineData()
    {

        FTimelineAllInfo allData = new FTimelineAllInfo();

        if (Directory.Exists(TIMELINE_DATA_BASEPATH))
        {
            DirectoryInfo dirInfo = new DirectoryInfo(TIMELINE_DATA_BASEPATH);
            FileInfo[] files = dirInfo.GetFiles("*", SearchOption.AllDirectories); //包括子目录
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Name.EndsWith(".json"))
                {
                    string path = TIMELINE_DATA_BASEPATH + files[i].Name;
                    TextAsset textAsset = AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset)) as TextAsset;

                    FTimelineInfo singleData = JsonUtility.FromJson<FTimelineInfo>(textAsset.text);

                    allData.timelineAllInfoList.Add(singleData);
                }

            }
        }

        string jsonData = JsonUtility.ToJson(allData);
        File.WriteAllText(Application.dataPath + newSavePath, jsonData);

        AssetDatabase.Refresh();

    }

}
    
    





﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DrawPathMono))]
public class DrawPathMonoEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();


        DrawPathMono myScript = target as DrawPathMono;

        if (GUILayout.Button("保存路径数据"))
        {
            myScript.SavePathForDotweenPath();
        }
        if (GUILayout.Button("创建DotweenPath"))
        {
            myScript.SetDotweenpath();
        }
        if (GUILayout.Button("显示路径Image"))
        {
            myScript.ShowPathImage();
        }


    }



}

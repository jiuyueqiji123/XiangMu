﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TimelineHolder))]
public class TimelineHolderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TimelineHolder myScript = target as TimelineHolder;


        if (GUILayout.Button("加载Timeline（从AllData中加载）"))
        {
            myScript.LoadTimelineForOld();
        }
        if (GUILayout.Button("加载Timeline"))
        {
            myScript.LoadTimeline();
        }
        if (GUILayout.Button("卸载Timeline"))
        {
            myScript.DisposeTimeline();
        }
        if (GUILayout.Button("保存Timeline数据"))
        {
            myScript.SaveTimelineInfo();
        }
        if (GUILayout.Button("删除Timeline数据"))
        {
            myScript.DeleteTimelineInfo();
        }
    }
}

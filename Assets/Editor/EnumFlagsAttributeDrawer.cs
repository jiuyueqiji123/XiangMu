﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(EnumFlagsAttribute))]
public class EnumFlagsAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        /*
         *绘制多值枚举 0：全部不选 -1：全部选中
         */

        property.intValue = EditorGUI.MaskField(position, label, property.intValue, property.enumNames);

    }
}
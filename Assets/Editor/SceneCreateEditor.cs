﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SceneCreateMono))]
public class SceneCreateEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SceneCreateMono myScript = target as SceneCreateMono;

        if (GUILayout.Button("创建场景"))
        {
            myScript.CreateScene();
        }
        if (GUILayout.Button("移除资源"))
        {
            myScript.ClearAsset();
        }
        if (GUILayout.Button("测试创建"))
        {
            myScript.CreateTest();
        }
    }
}

//[CustomEditor(typeof(GeometryActorComponent))]
//public class GeometryActorComponentEditor : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();

//        GeometryActorComponent myScript = target as GeometryActorComponent;

//        //if (GUILayout.Button("测试"))
//        //{
//            //myScript.Test();
//        //}
//    }
//}

//[CustomEditor(typeof(DrawLetterMono))]
//public class DrawLetterMonoEditor : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();

//        DrawLetterMono myScript = target as DrawLetterMono;

//        if (GUILayout.Button("测试"))
//        {
//            myScript.Test();
//        }
//    }
//}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ReceiverPlayableAsset))]
public class PlayableAssetHandlerInspector : Editor
{
	private SerializedProperty clipType;
	private int typeIndex;

	private void OnEnable()
	{
		//SceneView.onSceneGUIDelegate += OnSceneGUI;
        clipType = serializedObject.FindProperty("clipType");
    }

	public override void OnInspectorGUI()
	{
		EditorGUILayout.PropertyField(clipType);

		typeIndex = serializedObject.FindProperty("clipType").enumValueIndex;
        EClipType type = (EClipType)typeIndex;

        //Draws only the appropriate information based on the Command Type
        switch (type)
        {
            case EClipType.Event_Default:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("eventID"));
                break;
            case EClipType.Event_Click:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("eventID"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("audioName"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("isPauseTimeline"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("isResumeTimeline"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("clickPosition"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("receiverMono"));
                break;

            //case EClipType.Event_Animation:
            //case EClipType.Event_Raycast:
                //EditorGUILayout.PropertyField(serializedObject.FindProperty("eventID"));
                //break;
            case EClipType.Event_Speak:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("isPauseTimeline"));
                break;
            case EClipType.Event_NextTimeline:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("nextTimelineID"));
                break;
            case EClipType.Event_SelectCJFX:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("heroType"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("isResumeTimeline"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("nextTimelineID"));
                break;
            case EClipType.Event_Custom:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("eventID"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("isPauseTimeline"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("receiverMono"));
                break;
            case EClipType.Event_Effect:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("effectPath"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("effectParent"));
                break;
        }

        serializedObject.ApplyModifiedProperties();
	}

	private void OnDisable()
	{
		//SceneView.onSceneGUIDelegate -= OnSceneGUI;
	}


	//Draws a position handle on the position associated with the AICommand
	//the handle can be moved to reposition the targetPosition property
	//private void OnSceneGUI(SceneView v)
 //   {
 //       if ((EClipType)typeIndex == EClipType.Click || (EClipType)typeIndex == EClipType.Animation)
 //       {
 //           EditorGUI.BeginChangeCheck();
 //           Vector3 gizmoPos = Handles.PositionHandle(serializedObject.FindProperty("targetPosition").vector3Value, Quaternion.identity);

 //           if (EditorGUI.EndChangeCheck())
 //           {
 //               serializedObject.FindProperty("targetPosition").vector3Value = gizmoPos;
 //               serializedObject.ApplyModifiedProperties();

 //               Repaint();
 //           }
 //       }
 //   }
}

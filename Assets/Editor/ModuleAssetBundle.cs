﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public enum ModuleType
{
    喜马拉雅,
    马尔代夫,
    数学加减,
    认识数字,
    认识字母,
    自然拼读,
    图形认知,
    拼图,
    蛋糕店,
    水果店,
    丹麦恐龙,
    乐迪超市,
    乐迪中餐厅,
    数学减法,
    思维训练,
    逻辑矩阵,
    AR动物,
    AR蔬果,
    AR交通,
    AR运动,
}

public class ModuleAssetItem
{
    public string Name;
    public int CodeVersion;
    public int ResVersion;
}

public class ModuleAssetBundle : EditorWindow {

	private const string ModulePath = "AssetBundleModule";

	private static List<string> BundleNames = new List<string> ();

    private ModuleType moduleType;

    private ModuleType uploadModuleType;

    private Dictionary<ModuleType, ModuleAssetItem> items = new Dictionary<ModuleType, ModuleAssetItem>();

    public static List<ModuleItemInfo> ModuleList = new List<ModuleItemInfo> () {



#if PACKAGE_ENGLISH
        
        new ModuleItemInfo() {Name = ModuleConst.StoryHimalayaName, CodeVersion = ModuleConst.StoryHimalayaCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StoryMaerdaifuName, CodeVersion = ModuleConst.StoryMaerdaifuCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StoryDanMaiName, CodeVersion = ModuleConst.StoryDanMaiCodeVersion},

        new ModuleItemInfo() {Name = ModuleConst.GamePuzzleName, CodeVersion = ModuleConst.GamePuzzleCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameCakeName, CodeVersion = ModuleConst.GameCakeCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameFruitName, CodeVersion = ModuleConst.GameFruitCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameSuperName, CodeVersion = ModuleConst.GameSuperCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameRestaurantName, CodeVersion = ModuleConst.GameRestaurantCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameHospitalName, CodeVersion = ModuleConst.GameHospitalCodeVersion},

        new ModuleItemInfo() {Name = ModuleConst.StudyGeometryName, CodeVersion = ModuleConst.StudyGeometryCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyAddName, CodeVersion = ModuleConst.StudyAddCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyMathName, CodeVersion = ModuleConst.StudyMathCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudySubName, CodeVersion = ModuleConst.StudySubCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyMatrixName, CodeVersion = ModuleConst.StudyMatrixCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyCardGameName, CodeVersion = ModuleConst.StudyCardGameCodeVersion},

        //new ModuleItemInfo() {Name = ModuleConst.StudyPhonicsName, CodeVersion = ModuleConst.StudyPhonicsCodeVersion},
        //new ModuleItemInfo() {Name = ModuleConst.StudyEnglishName, CodeVersion = ModuleConst.StudyEnglishCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyEnglishColorName, CodeVersion = ModuleConst.StudyEnglishColorCodeVersion},

        new ModuleItemInfo() {Name = ModuleConst.MainGameName, CodeVersion = ModuleConst.MainGameCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.ExpressName, CodeVersion = ModuleConst.ExpressCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.PaintName, CodeVersion = ModuleConst.PaintCodeVersion},
        //new ModuleItemInfo() {Name = ModuleConst.RadioName, CodeVersion = ModuleConst.RadioCodeVersion},

        //new ModuleItemInfo() {Name = ModuleConst.OtherModelName, CodeVersion = ModuleConst.OtherModelCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.OtherPlayerAnimationName, CodeVersion = ModuleConst.OtherPlayerAnimationCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.OtherTimelineName, CodeVersion = ModuleConst.OtherTimelineCodeVersion},

        //AR百科
        new ModuleItemInfo() {Name = ModuleConst.ARBaikeSportsName, CodeVersion = ModuleConst.ARBaikeSportsCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.ARBaikeFruitsName, CodeVersion = ModuleConst.ARBaikeFruitsCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.ARBaikeTrafficName, CodeVersion = ModuleConst.ARBaikeTrafficCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.ARBaikeAnimalName, CodeVersion = ModuleConst.ARBaikeAnimalCodeVersion},
        
#elif PACKAGE_MATH

        new ModuleItemInfo() {Name = ModuleConst.StoryHimalayaName, CodeVersion = ModuleConst.StoryHimalayaCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StoryMaerdaifuName, CodeVersion = ModuleConst.StoryMaerdaifuCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StoryDanMaiName, CodeVersion = ModuleConst.StoryDanMaiCodeVersion},

        new ModuleItemInfo() {Name = ModuleConst.GamePuzzleName, CodeVersion = ModuleConst.GamePuzzleCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameCakeName, CodeVersion = ModuleConst.GameCakeCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameFruitName, CodeVersion = ModuleConst.GameFruitCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameSuperName, CodeVersion = ModuleConst.GameSuperCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameRestaurantName, CodeVersion = ModuleConst.GameRestaurantCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameHospitalName, CodeVersion = ModuleConst.GameHospitalCodeVersion},

        new ModuleItemInfo() {Name = ModuleConst.StudyPhonicsName, CodeVersion = ModuleConst.StudyPhonicsCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyEnglishName, CodeVersion = ModuleConst.StudyEnglishCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyEnglishColorName, CodeVersion = ModuleConst.StudyEnglishColorCodeVersion},

        new ModuleItemInfo() {Name = ModuleConst.MainGameName, CodeVersion = ModuleConst.MainGameCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.ExpressName, CodeVersion = ModuleConst.ExpressCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.PaintName, CodeVersion = ModuleConst.PaintCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.RadioName, CodeVersion = ModuleConst.RadioCodeVersion},

        //new ModuleItemInfo() {Name = ModuleConst.OtherModelName, CodeVersion = ModuleConst.OtherModelCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.OtherPlayerAnimationName, CodeVersion = ModuleConst.OtherPlayerAnimationCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.OtherTimelineName, CodeVersion = ModuleConst.OtherTimelineCodeVersion},

        //AR百科
        new ModuleItemInfo() {Name = ModuleConst.ARBaikeSportsName, CodeVersion = ModuleConst.ARBaikeSportsCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.ARBaikeFruitsName, CodeVersion = ModuleConst.ARBaikeFruitsCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.ARBaikeTrafficName, CodeVersion = ModuleConst.ARBaikeTrafficCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.ARBaikeAnimalName, CodeVersion = ModuleConst.ARBaikeAnimalCodeVersion},

#elif UNITY_IOS

        // 上线下载
        //new ModuleItemInfo() {Name = ModuleConst.StoryDanMaiName, CodeVersion = ModuleConst.StoryDanMaiCodeVersion},
        //new ModuleItemInfo() {Name = ModuleConst.StudyAddName, CodeVersion = ModuleConst.StudyAddCodeVersion},
        //new ModuleItemInfo() {Name = ModuleConst.GameRestaurantName, CodeVersion = ModuleConst.GameRestaurantCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudySubName, CodeVersion = ModuleConst.StudySubCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyMatrixName, CodeVersion = ModuleConst.StudyMatrixCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyCardGameName, CodeVersion = ModuleConst.StudyCardGameCodeVersion},

        // 开发功能屏蔽
        new ModuleItemInfo() {Name = ModuleConst.StudyEnglishColorName, CodeVersion = ModuleConst.StudyEnglishColorCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.MainGameName, CodeVersion = ModuleConst.MainGameCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.GameHospitalName, CodeVersion = ModuleConst.GameHospitalCodeVersion},

#else

        // 上线下载
        //new ModuleItemInfo() {Name = ModuleConst.StoryDanMaiName, CodeVersion = ModuleConst.StoryDanMaiCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyAddName, CodeVersion = ModuleConst.StudyAddCodeVersion},
        //new ModuleItemInfo() {Name = ModuleConst.GameRestaurantName, CodeVersion = ModuleConst.GameRestaurantCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudySubName, CodeVersion = ModuleConst.StudySubCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyMatrixName, CodeVersion = ModuleConst.StudyMatrixCodeVersion},
        new ModuleItemInfo() {Name = ModuleConst.StudyCardGameName, CodeVersion = ModuleConst.StudyCardGameCodeVersion},

        // 开发功能屏蔽
        //new ModuleItemInfo() {Name = ModuleConst.StudyEnglishColorName, CodeVersion = ModuleConst.StudyEnglishColorCodeVersion},
        //new ModuleItemInfo() {Name = ModuleConst.MainGameName, CodeVersion = ModuleConst.MainGameCodeVersion},
        //new ModuleItemInfo() {Name = ModuleConst.GameHospitalName, CodeVersion = ModuleConst.GameHospitalCodeVersion},
#endif

    };

	static string GetOutPath(string path) {
		return FileTools.CombinePath (path, AssetHeplerEditor.GetPlatformName ());
	}

    private void Awake()
    {
        items.Add(ModuleType.马尔代夫, new ModuleAssetItem()
        {
            Name = ModuleConst.StoryMaerdaifuName,
            CodeVersion = ModuleConst.StoryMaerdaifuCodeVersion,
            ResVersion = ModuleConst.StoryMaerdaifuResVersion
        });
        items.Add(ModuleType.喜马拉雅, new ModuleAssetItem()
        {
            Name = ModuleConst.StoryHimalayaName,
            CodeVersion = ModuleConst.StoryHimalayaCodeVersion,
            ResVersion = ModuleConst.StoryHimalayaResVersion
        });
        items.Add(ModuleType.数学加减, new ModuleAssetItem()
        {
            Name = ModuleConst.StudyAddName,
            CodeVersion = ModuleConst.StudyAddCodeVersion,
            ResVersion = ModuleConst.StudyAddResVersion
        });
        items.Add(ModuleType.认识字母, new ModuleAssetItem()
        {
            Name = ModuleConst.StudyEnglishName,
            CodeVersion = ModuleConst.StudyEnglishCodeVersion,
            ResVersion = ModuleConst.StudyEnglishResVersion
        });
        items.Add(ModuleType.认识数字, new ModuleAssetItem()
        {
            Name = ModuleConst.StudyMathName,
            CodeVersion = ModuleConst.StudyMathCodeVersion,
            ResVersion = ModuleConst.StudyMathResVersion
        });
        items.Add(ModuleType.自然拼读, new ModuleAssetItem()
        {
            Name = ModuleConst.StudyPhonicsName,
            CodeVersion = ModuleConst.StudyPhonicsCodeVersion,
            ResVersion = ModuleConst.StudyPhonicsResVersion
        });
        items.Add(ModuleType.图形认知, new ModuleAssetItem()
        {
            Name = ModuleConst.StudyGeometryName,
            CodeVersion = ModuleConst.StudyGeometryCodeVersion,
            ResVersion = ModuleConst.StudyGeometryResVersion
        });
        items.Add(ModuleType.拼图, new ModuleAssetItem()
        {
            Name = ModuleConst.GamePuzzleName,
            CodeVersion = ModuleConst.GamePuzzleCodeVersion,
            ResVersion = ModuleConst.GamePuzzleResVersion
        });
        items.Add(ModuleType.蛋糕店, new ModuleAssetItem()
        {
            Name = ModuleConst.GameCakeName,
            CodeVersion = ModuleConst.GameCakeCodeVersion,
            ResVersion = ModuleConst.GameCakeResVersion
        });
        items.Add(ModuleType.水果店, new ModuleAssetItem()
        {
            Name = ModuleConst.GameFruitName,
            CodeVersion = ModuleConst.GameFruitCodeVersion,
            ResVersion = ModuleConst.GameFruitResVersion
        });
        items.Add(ModuleType.丹麦恐龙, new ModuleAssetItem()
        {
            Name = ModuleConst.StoryDanMaiName,
            CodeVersion = ModuleConst.StoryDanMaiCodeVersion,
            ResVersion = ModuleConst.StoryDanMaiResVersion
        });
        items.Add(ModuleType.乐迪超市, new ModuleAssetItem()
        {
            Name = ModuleConst.GameSuperName,
            CodeVersion = ModuleConst.GameSuperCodeVersion,
            ResVersion = ModuleConst.GameSuperResVersion
        });
        items.Add(ModuleType.乐迪中餐厅, new ModuleAssetItem()
        {
            Name = ModuleConst.GameRestaurantName,
            CodeVersion = ModuleConst.GameRestaurantCodeVersion,
            ResVersion = ModuleConst.GameRestaurantResVersion
        });
        items.Add(ModuleType.数学减法, new ModuleAssetItem()
        {
            Name = ModuleConst.StudySubName,
            CodeVersion = ModuleConst.StudySubCodeVersion,
            ResVersion = ModuleConst.StudySubResVersion
        });
        items.Add(ModuleType.逻辑矩阵, new ModuleAssetItem()
        {
            Name = ModuleConst.StudyMatrixName,
            CodeVersion = ModuleConst.StudyMatrixCodeVersion,
            ResVersion = ModuleConst.StudyMathResVersion
        });
        items.Add(ModuleType.思维训练, new ModuleAssetItem()
        {
            Name = ModuleConst.StudyCardGameName,
            CodeVersion = ModuleConst.StudyCardGameCodeVersion,
            ResVersion = ModuleConst.StudyCardGameResVersion
        });

#region package-english

        items.Add(ModuleType.AR动物, new ModuleAssetItem()
        {
            Name = ModuleConst.ARBaikeAnimalName,
            CodeVersion = ModuleConst.ARBaikeAnimalCodeVersion,
            ResVersion = ModuleConst.ARBaikeAnimalResVersion
        });
        items.Add(ModuleType.AR交通, new ModuleAssetItem()
        {
            Name = ModuleConst.ARBaikeTrafficName,
            CodeVersion = ModuleConst.ARBaikeTrafficCodeVersion,
            ResVersion = ModuleConst.ARBaikeTrafficResVersion
        });
        items.Add(ModuleType.AR蔬果, new ModuleAssetItem()
        {
            Name = ModuleConst.ARBaikeFruitsName,
            CodeVersion = ModuleConst.ARBaikeFruitsCodeVersion,
            ResVersion = ModuleConst.ARBaikeFruitsResVersion
        });
        items.Add(ModuleType.AR运动, new ModuleAssetItem()
        {
            Name = ModuleConst.ARBaikeSportsName,
            CodeVersion = ModuleConst.ARBaikeSportsCodeVersion,
            ResVersion = ModuleConst.ARBaikeSportsResVersion
        });

#endregion

    }

    [MenuItem("发布/资源打包上传")]
    public static void ModulePack()
    {
        Rect wr = new Rect(0, 0, 300, 300);
        ModuleAssetBundle window = (ModuleAssetBundle)EditorWindow.GetWindowWithRect(typeof(ModuleAssetBundle), wr, true, "打包上传资源");
        window.Show();
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal("box", GUILayout.ExpandWidth(true));
        moduleType = (ModuleType)EditorGUILayout.EnumPopup(moduleType);
        if (GUILayout.Button("开始打包"))
        {
            CreateAsset(ModulePath, items[moduleType].Name, CreateAssetBundle.AppV, items[moduleType].CodeVersion, items[moduleType].ResVersion);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal("box", GUILayout.ExpandWidth(true));
        uploadModuleType = (ModuleType)EditorGUILayout.EnumPopup(uploadModuleType);
        if (GUILayout.Button("打包上传"))
        {
            CreateAsset(ModulePath, items[uploadModuleType].Name, CreateAssetBundle.AppV, items[uploadModuleType].CodeVersion, items[uploadModuleType].ResVersion);
            UploadAsset(ModulePath, items[uploadModuleType].Name, items[uploadModuleType].CodeVersion, items[uploadModuleType].ResVersion);
        }
        GUILayout.EndHorizontal();
    }

    static void CreateAsset(string rootName, string name, int appV, int codeV, int resV) {
        PublishEditor.MoveResToModuleAsset(name);
        BundleNames.Clear();
		ResourceInfo resInfo = new ResourceInfo ();
		resInfo.AppVersion = appV;
		resInfo.CodeVersion = codeV;
		resInfo.ResVersion = resV;
		resInfo.ResList = new List<ResourceItemInfo> ();
		ResourceItemInfo item = new ResourceItemInfo ();
		item.Path = name;
		item.Name = name;
		item.IsResident = false;
        GetResItemInfo(resInfo, item.Path, false, BundleNames);
		item.ResNames = new List<string> ();
		resInfo.ResList.Add (item);
		item.IsAsset = AssetHeplerEditor.SetAssetBundleName ("Assets/" + rootName + "/" + name, item.Name, item.ResNames);

		string outP = GetOutPath (CreateAssetBundle.AssetPath);

		//FileTools.ClearDirectory (outP);
		FileTools.CreateDirectory (outP);

		BuildPipeline.BuildAssetBundles (outP, BuildAssetBundleOptions.UncompressedAssetBundle | BuildAssetBundleOptions.ForceRebuildAssetBundle, EditorUserBuildSettings.activeBuildTarget);

		CreateAssetBundle.SetDependencies (resInfo, outP); 

		CreateAssetBundle.SaveResInfoFile (FileTools.CombinePath(outP, name + CreateAssetBundle.ResInfoName), resInfo);

		CreateAssetBundle.ClearAssetName (rootName, resInfo);

        //CopyToSteaming(rootName, name);
        PublishEditor.MoveModuleAssetToRes(name);
		AssetDatabase.Refresh();
		//EditorUtility.DisplayDialog("提示", "打包AssetBundle完成！", "确定");
	}

	static void UploadAsset(string rootName, string name, int codeV, int resV) {

        string ResPath = "D:\\wamp\\www\\SuperWings\\" + name + "\\" + codeV;
		FileTools.CreateDirectory (ResPath);

		ZipAndCopy (ResPath, name);

		string outP = GetOutPath (CreateAssetBundle.AssetPath);
		string resP = Path.Combine (outP, name + CreateAssetBundle.ResInfoName);
		FileTools.CopyFile (resP, FileTools.CombinePath (ResPath, name + CreateAssetBundle.ResInfoName));

		EditorUtility.DisplayDialog ("提示", "上传完成！", "确定");
	}

	static void CopyToSteaming(string rootName, string name) {
		string ResPath = FileTools.CombinePath (Application.streamingAssetsPath, CreateAssetBundle.AssetPath);
		FileTools.CreateDirectory (ResPath);

		ZipAndCopy (ResPath, name);
	}

	static void ZipAndCopy(string outPath, string name) {
		string outP = GetOutPath (CreateAssetBundle.AssetPath);
		string resP = Path.Combine (outP, name + CreateAssetBundle.ResInfoName);

		string info = File.ReadAllText (Path.Combine (outP, name+CreateAssetBundle.ResInfoName));
		ResourceInfo itemInfo = JsonUtility.FromJson<ResourceInfo> (info);
		BundleNames.Clear ();
		for (int i = 0; i < itemInfo.ResList.Count; i++) {
			if (itemInfo.ResList [i].IsAsset) {
				BundleNames.Add (itemInfo.ResList [i].Name);
                string filePath = Path.Combine(outP, itemInfo.ResList[i].Name);
                long size = FileTools.GetFileSize(filePath);
                itemInfo.ResLength += size;
            }
		}
        CreateAssetBundle.SaveResInfoFile(FileTools.CombinePath(outP, name + CreateAssetBundle.ResInfoName), itemInfo);
        BundleNames.Add (name + CreateAssetBundle.ResInfoName);
		string zipPath = Path.Combine(outPath, name + ".zip");
		ZipUtils.CreateZipFile (zipPath, outP, BundleNames);
	}

    public static void GetResItemInfo(ResourceInfo resInfo, string itemPath, bool isResident, List<string> assetNames)
    {
        string[] Dirs = Directory.GetDirectories("Assets/" + ModulePath + "/" + itemPath);
        for (int i = 0; i < Dirs.Length; i++)
        {
            string path = Dirs[i].Replace("\\", "/");
            string name = FileTools.GetFullName(path);
            ResourceItemInfo item = new ResourceItemInfo();
            item.Path = itemPath + "/" + name;
            item.Name = name;
            item.IsResident = isResident;
            item.ResNames = new List<string>();
            resInfo.ResList.Add(item);
            assetNames.Add(item.Name);
            item.IsAsset = AssetHeplerEditor.SetAssetBundleName(path, item.Name, item.ResNames);
        }
    }
}

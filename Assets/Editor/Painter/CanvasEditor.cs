﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class CanvasEditor : EditorWindow {

	string[] names = new string[] {"大", "中", "小"};
	int[] sizes = {0, 1, 2};

	public const string PaintAssetPath = "Assets/Resources/paint_asset/painttable/";

	private string assetName = "CanvasTable.asset";

	private List<PaintAnimAttr> animList = new List<PaintAnimAttr> ();


	Vector2 monsterScroll;
	private Dictionary<int, bool> animFoldouts = new Dictionary<int, bool> ();


	[MenuItem ("Painter/Canvas Editor")]
	static void AddWindow ()
	{       
		//创建窗口
		Rect  wr = new Rect (0,0,1200,800);
		CanvasEditor window = (CanvasEditor)EditorWindow.GetWindowWithRect (typeof (CanvasEditor), wr, true, "场景编辑");	
		window.Init ();
		window.Show ();
	}

	public void Init() {
		CanvasTableData data = AssetDatabase.LoadAssetAtPath<CanvasTableData> (PaintAssetPath + assetName);
		if (data != null) {
			animList = data.Lists;
			for(int i=0; i<animList.Count; i++) {
				animFoldouts[animList[i].Id] = false;
			}
		}
	}

	void OnGUI() {
		monsterScroll = EditorGUILayout.BeginScrollView (monsterScroll);
		GUILayout.BeginHorizontal (); 
		if (GUILayout.Button ("添加")) {
			this.Add ();
		}
		if (GUILayout.Button ("排序")) {
			SortList ();
		}
		GUILayout.EndHorizontal ();

		List<PaintAnimAttr> removePA = new List<PaintAnimAttr> ();
		for(int i=0; i<animList.Count; i++) {
			if (ShowAnimList (animList [i]))
				removePA.Add (animList [i]);
		}

		EditorGUILayout.EndScrollView();

		if (removePA.Count > 0) {
			foreach (PaintAnimAttr ma in removePA) {
				animList.Remove(ma);
				animFoldouts.Remove (ma.Id);
			}
		}

		GUILayout.FlexibleSpace();
		if(GUILayout.Button("保存")){
			this.Save();
		}
	}

	private bool ShowAnimList(PaintAnimAttr attr) {
		animFoldouts [attr.Id] = EditorGUILayout.Foldout (animFoldouts [attr.Id], attr.Name);
		if (!animFoldouts [attr.Id])
			return false;

		GUILayout.BeginVertical("box", GUILayout.ExpandWidth(true));
		GUILayout.BeginHorizontal (); 
		int id = attr.Id;
		attr.Id = EditorGUILayout.IntField("ID: ",  attr.Id);
		if (id != attr.Id) {
			animFoldouts.Remove(id);
			animFoldouts [attr.Id] = true;
		}
		attr.Name = EditorGUILayout.TextField("名字: ",  attr.Name);
		//attr.GuideId = EditorGUILayout.IntField("指引ID: ",  attr.GuideId);
		attr.LastCameraSize = EditorGUILayout.FloatField ("画完后摄像头大小：", attr.LastCameraSize);
		attr.LastCameraX = EditorGUILayout.FloatField ("摄像头X：", attr.LastCameraX);
		attr.LastCameraY = EditorGUILayout.FloatField ("摄像头Y：", attr.LastCameraY);
		GUILayout.EndHorizontal ();
		GUILayout.BeginHorizontal (); 
		bool remove = false;
		if (GUILayout.Button ("删除")) {
			remove = EditorUtility.DisplayDialog("提示", "是否删除！", "确定", "取消");
		}
		if (GUILayout.Button ("添加部件")) {
			AddPart (attr);
		}
		if (GUILayout.Button ("排序")) {
			SortPartList (attr);
		}
		GUILayout.EndHorizontal ();

		List<PaintAnimPartAttr> removePA = new List<PaintAnimPartAttr> ();
		for(int i=0; i<attr.Parts.Count; i++) {
			if (ShowAnimPart (attr.Parts [i]))
				removePA.Add (attr.Parts [i]);
		}
		if (removePA.Count > 0) {
			foreach (PaintAnimPartAttr ma in removePA) {
				attr.Parts.Remove(ma);
			}
		}
		GUILayout.EndVertical ();
		return remove;
	}

	private bool ShowAnimPart(PaintAnimPartAttr part) {
		bool remove = false;
		GUILayout.BeginVertical("box", GUILayout.ExpandWidth(true));
		GUILayout.BeginHorizontal (); 
		part.Id = EditorGUILayout.IntField("部件ID: ",  part.Id);
		part.Name = EditorGUILayout.TextField("名字: ",  part.Name);
		part.Anim = EditorGUILayout.Toggle ("动画: ", part.Anim);
		if (GUILayout.Button ("删除")) {
			remove = EditorUtility.DisplayDialog("提示", "是否删除！", "确定", "取消");
		}
		if (GUILayout.Button ("编辑")) {
			EditorPart (part);
		}
		GUILayout.EndHorizontal ();
		GUILayout.BeginHorizontal (); 
		part.CameraSize = EditorGUILayout.FloatField ("摄像头尺寸：", part.CameraSize);
		part.CameraX = EditorGUILayout.FloatField ("摄像头X：", part.CameraX);
		part.CameraY = EditorGUILayout.FloatField ("摄像头Y：", part.CameraY);
		part.Brush = EditorGUILayout.IntPopup ("笔刷大小：", part.Brush, names, sizes);
		GUILayout.EndHorizontal ();
		GUILayout.EndVertical ();
		return remove;
	}

	private void Add() {
		PaintAnimAttr attr = new PaintAnimAttr ();
		if (animList.Count > 0) {
			attr.Id = animList [animList.Count - 1].Id + 1;
		}
		animList.Add (attr);
		animFoldouts[attr.Id] = true;
	}

	private void Save() {
		CanvasTableData data = ScriptableObject.CreateInstance<CanvasTableData> ();
		data.Lists = animList;
		AssetDatabase.CreateAsset(data, PaintAssetPath + assetName);
		AssetDatabase.Refresh ();
		EditorUtility.DisplayDialog("提示", "导出成功！", "确定");
	}

	private void EditorPart(PaintAnimPartAttr part) {
		Rect  wr = new Rect (0,0,1280,900);
		PainterEditor window = (PainterEditor)EditorWindow.GetWindowWithRect (typeof (PainterEditor), wr, true, part.Id + "--" + part.Name);	
		window.Init (part.Id);
		window.Show ();
	}

	private void AddPart(PaintAnimAttr attr) {
		PaintAnimPartAttr part = new PaintAnimPartAttr ();
		if (attr.Parts.Count > 0) {
			part.Id = attr.Parts [attr.Parts.Count - 1].Id + 1;
		}
		attr.Parts.Add (part);
	}

	private void SortList() {
		animList.Sort(delegate(PaintAnimAttr x, PaintAnimAttr y)
		{
			return x.Id.CompareTo(y.Id);
		});
	}

	private void SortPartList(PaintAnimAttr attr) {
		attr.Parts.Sort(delegate(PaintAnimPartAttr x, PaintAnimPartAttr y)
		{
			return x.Id.CompareTo(y.Id);
		});
	}

	void OnDestroy() {
		//EditorUtility.DisplayDialog("提示", "导出成功！", "确定");
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class PainterEditor : EditorWindow {

	public class PaintRect {
		public Rect rect;
		public int Row;
		public int Col;
		public bool Select;

		public override string ToString ()
		{
			return "row:" + Row + ",col:" + Col + "," + rect.ToString();
		}
	}

	Texture2D texture;
	int RowField = 0;
	int ColField = 0;

	public List<PaintRect> Rects = new List<PaintRect>();

	int imageInitX = 5;
	int imageInitY = 80;

	int Row = 0;
	int Col = 0;
	int Id = 0;

	int SelectNum = 0;

	Vector2 scroll;
[MenuItem ("Painter/Image Editor")]
	static void AddWindow ()
	{       
		//创建窗口
		Rect  wr = new Rect (0,0,1280,1280);
		PainterEditor window = (PainterEditor)EditorWindow.GetWindowWithRect (typeof (PainterEditor), wr, true, "场景编辑");	
		window.Init (100);
		window.Show ();
	}

	public void Init(int id) {
		this.Id = id;

		PainterTableData data = AssetDatabase.LoadAssetAtPath<PainterTableData> (GetFullAssetPath(this.Id.ToString()));
		if (data != null) {
			this.Row = data.Row;
			this.Col = data.Col;
			this.RowField = this.Row;
			this.ColField = this.Col;
			Debug.Log ("Assets/Resources/paint_asset/Image/" + id.ToString() + "/" + data.ImageName + ".png");
			texture = AssetDatabase.LoadAssetAtPath<Texture2D> ("Assets/Resources/paint_asset/paintimage/" + data.ImageName + ".png");
			InitRects (data.Rects);
		} else {
			InitRects (null);
		}

	}

	void OnGUI() {
		scroll = EditorGUILayout.BeginScrollView (scroll, false, false, GUILayout.Height(870));


		GUILayout.BeginHorizontal("box", GUILayout.Height(70), GUILayout.ExpandWidth(true));
		EditorGUILayout.LabelField("ID: "+ Id, GUILayout.Width(100));
		texture = (Texture2D)EditorGUILayout.ObjectField ("选择图片", texture, typeof(Texture), true);
		RowField = EditorGUILayout.IntField("行: ", RowField);
		ColField = EditorGUILayout.IntField("列: ", ColField);

		if (GUILayout.Button ("修改行列")) {
			ChangeRowCol ();
		}
		EditorGUILayout.LabelField ("格子数量:" + SelectNum);
		GUILayout.EndHorizontal ();

		ShowTexture ();
		EditorGUILayout.EndScrollView();
		GUILayout.FlexibleSpace();
		if(GUILayout.Button("保存")){
			this.Save();
		}



	}

	#region View
	private void ShowTexture() {
		if (texture != null) {
			//scroll = EditorGUILayout.BeginScrollView (scroll, GUILayout.Height(600));
			GUILayout.Box(texture);
//			float tw = texture.width;
//			float th = texture.height;
//			if (texture.height > 600f) {
//				th = 600f;
//				tw = texture.width * 600f / texture.height;
//			}
//
			EditorGUI.DrawTextureTransparent (new Rect (imageInitX, imageInitY, texture.width, texture.height), texture);

			if (this.Row > 0 && this.Col > 0) {
				float h = texture.height / (float)Row;
				float w = texture.width / (float)Col;
				for (int row = 1; row < Row; row++) {
					EditorGUI.DrawRect (new Rect (imageInitX, imageInitY + (row * h), texture.width, 1), new Color (1, 1, 1, 1f));
				}
				for (int col = 1; col < Col; col++) {
					EditorGUI.DrawRect (new Rect (imageInitX + (col * w), imageInitY, 1, texture.height), new Color (1, 1, 1, 1f));
				}

				foreach (PaintRect rect in Rects) {
					if (rect.Select) {
						Rect r = rect.rect;
						r.x += imageInitX;
						r.y = imageInitY + (texture.height - r.y - h);
						//Debug.LogError (r);
						//Debug.LogError (rect.rect);
						EditorGUI.DrawRect (r, new Color (1f, 0.5f, 0.5f, 0.5f));
					}
				}

				if (Event.current != null) {
					if (Event.current.type == EventType.MouseUp) {
						Vector2 pos = Event.current.mousePosition;
						if (pos.x > imageInitX && pos.y > imageInitY && pos.x < imageInitX + texture.width && pos.y < imageInitY + texture.height) {
							Event.current.Use ();
							//Debug.LogError (pos);
							//pos.x -= imageInitX;
							//pos.y = texture.height - (pos.y - imageInitY);
							foreach (PaintRect rect in Rects) {
								Rect r = rect.rect;
								r.x += imageInitX;
								r.y = imageInitY + (texture.height - r.y - h);
								if (r.Contains (pos)) {
									rect.Select = !rect.Select;
									if (rect.Select) {
										this.SelectNum++;
									} else {
										this.SelectNum--;
									}
									break;
								}
							}
							//Debug.LogError (pos);
						}
					}
				}
			}
			//EditorGUILayout.EndScrollView();
		}
	}
	#endregion

	#region Data
	private void InitRects(List<PainterRectAttr> rects) {
		this.SelectNum = 0;
		this.Rects.Clear ();
		if (this.Row > 0 && this.Col > 0 && texture != null) {
			float h = texture.height / (float)Row;
			float w = texture.width / (float)Col;
			for (int row = 0; row < Row; row++) {
				for (int col = 0; col < Col; col++) {
					PaintRect rect = new PaintRect ();
					rect.Row = row;
					rect.Col = col;
					float x = col * w;
					float y = row * h;
					rect.rect = new Rect (x, y, w, h);
					if (rects != null) {
						foreach(PainterRectAttr attr in rects) {
							if (attr.Row == row && attr.Col == col) {
								rect.Select = true;
								this.SelectNum++;
								break;
							}
						}
					}
					//Debug.Log (rect);
					Rects.Add (rect);
				}
			}
		}
	}
	#endregion

	#region event
	private void ChangeRowCol() {
		if (texture != null) {
			this.Row = this.RowField;
			this.Col = this.ColField;
			InitRects (null);
		}
	}

	private void Save() {
		if (texture != null) {
			PainterTableData data = ScriptableObject.CreateInstance<PainterTableData> ();
			data.Id = this.Id;
			data.ImageName = texture.name;
			data.Row = this.Row;
			data.Col = this.Col;
			data.Width = texture.width;
			data.Height = texture.height;
			foreach (PaintRect rect in Rects) {
				if (rect.Select) {
					PainterRectAttr attr = new PainterRectAttr ();
					attr.Row = rect.Row;
					attr.Col = rect.Col;
					attr.Rect = rect.rect;
					data.Rects.Add (attr);
				}
			}
			AssetDatabase.CreateAsset(data, GetFullAssetPath(this.Id.ToString()));
			AssetDatabase.Refresh ();
			EditorUtility.DisplayDialog("提示", "导出成功！", "确定");
		}
	}
	#endregion

	#region get
	string GetFullAssetPath(string filename) {
		return "Assets/Resources/paint_asset/painttable/" + filename + ".asset";
	}
	#endregion
}
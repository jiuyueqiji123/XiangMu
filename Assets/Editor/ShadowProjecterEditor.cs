﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DynamicShadowProjector;

[CustomEditor(typeof(DrawTargetObject))]
public class ShadowProjecterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DrawTargetObject myScript = target as DrawTargetObject;

        if (GUILayout.Button("刷新阴影"))
        {
            myScript.RefreshShaowProjecter();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

public class StoryEnumEditor : MonoBehaviour
{

    static string himalayaPath = "Assets/Resources/story_himalaya/";
    static string CommomPath = "Assets/Resources/model/model_story/";
    static string maerdaifuPath = "Assets/Resources/story_maerdaifu/";

    [MenuItem("Asset/冒险编辑/喜马拉雅")]

    static void SetHimalayaEnum()
    {
        //获得指定路径下面的所有资源文件
        if (Directory.Exists(himalayaPath))
        {
            DirectoryInfo dirInfo = new DirectoryInfo(himalayaPath);
            FileInfo[] files = dirInfo.GetFiles("*", SearchOption.AllDirectories); //包括子目录
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Name.EndsWith(".prefab"))
                {
                    Debug.Log("预制体名字" + files[i].Name);
                    string path = himalayaPath + files[i].Name;
                    Debug.Log("预制体路径" + path);
                    GameObject obj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
                    Debug.Log("obj的名字" + obj.name);
                    //Transform tra = obj.transform.FindChild("Label");
                    TLIdentify_Parent[] tls = obj.transform.GetComponentsInChildren<TLIdentify_Parent>(true);
                    foreach (TLIdentify_Parent tl in tls)
                    {
                        tl.storyname = TLIdentify_Parent.StoryName.story_himalaya;
                    }


                    //通知你的编辑器 obj 改变了
                    EditorUtility.SetDirty(obj);
                    //保存修改
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();


                }

            }
            Debug.Log("修改完成");


        }

    }

    [MenuItem("Asset/冒险编辑/公用")]

    static void SetCommomEnum()
    {
        if (Directory.Exists(CommomPath))
        {
            DirectoryInfo dirInfo = new DirectoryInfo(CommomPath);
            FileInfo[] files = dirInfo.GetFiles("*", SearchOption.AllDirectories); //包括子目录
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Name.EndsWith(".prefab"))
                {
                    Debug.Log("预制体名字" + files[i].Name);
                    string path = CommomPath + files[i].Name;
                    Debug.Log("预制体路径" + path);
                    GameObject obj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
                    Debug.Log("obj的名字" + obj.name);
                    //Transform tra = obj.transform.FindChild("Label");
                    TLIdentify_Parent[] tls = obj.transform.GetComponentsInChildren<TLIdentify_Parent>(true);
                    foreach (TLIdentify_Parent tl in tls)
                    {
                        tl.storyname = TLIdentify_Parent.StoryName.Commom;
                    }


                    //通知你的编辑器 obj 改变了
                    EditorUtility.SetDirty(obj);
                    //保存修改
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();


                }
            }
            Debug.Log("修改完成");
            

        }
    }

    [MenuItem("Asset/冒险编辑/马尔代夫")]

    static void SetMaerdaifu()
    {
        //获得指定路径下面的所有资源文件
        if (Directory.Exists(maerdaifuPath))
        {
            DirectoryInfo dirInfo = new DirectoryInfo(maerdaifuPath);
            FileInfo[] files = dirInfo.GetFiles("*", SearchOption.AllDirectories); //包括子目录
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Name.EndsWith(".prefab"))
                {
                    Debug.Log("预制体名字" + files[i].Name);
                    string path = maerdaifuPath + files[i].Name;
                    Debug.Log("预制体路径" + path);
                    GameObject obj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
                    Debug.Log("obj的名字" + obj.name);
                    //Transform tra = obj.transform.FindChild("Label");
                    TLIdentify_Parent[] tls = obj.transform.GetComponentsInChildren<TLIdentify_Parent>(true);
                    foreach (TLIdentify_Parent tl in tls)
                    {
                        tl.storyname = TLIdentify_Parent.StoryName.story_maerdaifu;
                    }


                    //通知你的编辑器 obj 改变了
                    EditorUtility.SetDirty(obj);
                    //保存修改
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();


                }

            }
            Debug.Log("修改完成");


        }

    }

    [MenuItem("Asset/冒险编辑/一键编辑所有")]

    static void SetAllEnum()
    {
        SetHimalayaEnum();
        SetCommomEnum();
        SetMaerdaifu();
        EditorUtility.DisplayDialog("提示", "全部修改完成！", "确定");
    }

    [MenuItem("Asset/一键清除持久化数据")]
    static void ClearPlayerPrefs() {
        PlayerPrefs.DeleteAll();
    }




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class XLuaHotfix {

	[MenuItem("XLua/打开hotfix模式")]
	static void OpenHotFixMode() {
		AssetHeplerEditor.AddSymbolsForGroup (BuildTargetGroup.Android, "HOTFIX_ENABLE");
	}

	[MenuItem("XLua/关闭hotfix模式")]
	static void CloseHotFixMode() {
		AssetHeplerEditor.RemoveSymbolsForGroup (BuildTargetGroup.Android, "HOTFIX_ENABLE");
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class TimelineEditorTest
{
    [MenuItem("Test/Test")]
    static void Test()
    {
        string path = "Assets/Resources/game_cake/game_cake_timeline/Level_1_Timeline.playable";
        TimelineAsset director = (TimelineAsset)AssetDatabase.LoadAssetAtPath(path, typeof(TimelineAsset));
        Debug.Log(director);
        Debug.Log(director.outputTrackCount);
        Debug.Log(director.GetOutputTrack(1));
        TrackAsset asset = director.GetOutputTrack(0);
        asset.name = "111";

        AssetDatabase.SaveAssets();

        Texture2D t = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Resources/game_cake/game_cake_texture/304010315_01.png", typeof(Texture2D));
        Debug.Log(t);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum emHttpRequestState {
	Finished,
	ServerError,
	Error,
	Aborted,
	ConnectionTimedOut,
	TimedOut,
}

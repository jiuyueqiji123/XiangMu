﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HttpServiceBase  {
#if LOCAL_RELEASE
    private string host = "http://superwings.soulgame.mobi/";
#else
    private string host = "http://test.superwings.soulgame.mobi/";
#endif

    protected abstract string Url {
		get;
	}

	public virtual void Send(HttpTask task) {
		task.Url = host + Url;
		HttpManager.Instance.Request (task);
	}
}

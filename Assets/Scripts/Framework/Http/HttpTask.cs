﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BestHTTP;
using System;

public class HttpTask : PooledClassObject {

	public delegate void SucCallbackDelegate (byte[] data);

	public delegate void FailCallbackDelegate (emHttpRequestState state);

	private SucCallbackDelegate sucCallback;

	private FailCallbackDelegate failCallback;

	private string url;

	private byte[] data;

	private string header;

    private string appconfig;

	public byte[] Data {
		get {
			return data;
		}
	}

	public string Header {
		get {
			return header;
		}
	}

    public string APPConfig
    {
        get
        {
            return appconfig;
        }
    }

	public string Url {
		get {
			return url;
		}
		set {
			url = value;
		}
	}

	public void AddSucCallback(SucCallbackDelegate callback) {
		if (callback != null) {
			sucCallback += callback; 
		}
	}

	public void AddFailCallback(FailCallbackDelegate callback) {
		if (callback != null) {
			failCallback += callback;
		}
	}

	public void SetData(byte[] data) {
		SetData (data, null, null);
	}

	public void SetData(byte[] data, SucCallbackDelegate onSuc) {
		SetData (data, onSuc, null);
	}

	public void SetData(byte[] data, SucCallbackDelegate onSuc, FailCallbackDelegate onFail) {
		this.data = data;
		AddSucCallback (onSuc);
		AddFailCallback (onFail);
		string mac = UserManager.Instance.MAC;
		long timestamp = TimeUtils.ToUtcSeconds (DateTime.Now);
		string token = StringUtils.CreateMD5Hash (timestamp.ToString() + StringUtils.CreateMD5Hash(mac + "superwings0755"));
		this.header = string.Format("mac={0}&timestamp={1}&token={2}", UserManager.Instance.MAC, timestamp, token);
        this.appconfig = string.Format("version=v1.0.0&device_type=1&channel=soulgame");
	}

	public override void OnUse ()
	{
		base.OnUse ();
		sucCallback = null;
		failCallback = null;
		this.data = null;
	}

	public override void OnRelease ()
	{
		base.OnRelease ();
		sucCallback = null;
		failCallback = null;
		this.data = null;
	}

	public void Success(byte[] data) {
		if (sucCallback != null) {
			sucCallback (data);
		}
	}

	public void Fail(emHttpRequestState state) {
		if (failCallback != null) {
			failCallback (state);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputClickHandler : IInputEventHandler {

	void OnInputClick(InputEventData eventData);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputBeginDragHandler : IInputEventHandler {

	void OnInputBeginDrag(InputEventData eventData);
}

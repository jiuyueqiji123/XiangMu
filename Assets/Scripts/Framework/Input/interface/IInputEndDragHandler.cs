﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputEndDragHandler : IInputEventHandler {

	void OnInputEndDrag(InputEventData eventData);
}

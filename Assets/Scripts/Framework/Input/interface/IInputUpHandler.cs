﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputUpHandler : IInputEventHandler {

	void OnInputUp(InputEventData eventData);
}

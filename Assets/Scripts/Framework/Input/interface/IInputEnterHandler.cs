﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputEnterHandler : IInputEventHandler {

	void OnInputEnter(InputEventData eventData);
}

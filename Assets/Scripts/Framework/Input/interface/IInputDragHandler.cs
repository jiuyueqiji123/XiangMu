﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputDragHandler : IInputEventHandler {

	void OnInputDrag(InputEventData eventData);
}

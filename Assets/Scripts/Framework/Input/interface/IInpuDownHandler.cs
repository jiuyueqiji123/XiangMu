﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputDownHandler : IInputEventHandler {

	void OnInputDown (InputEventData eventData);
}

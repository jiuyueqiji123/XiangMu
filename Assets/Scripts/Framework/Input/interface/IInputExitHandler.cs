﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputExitHandler : IInputEventHandler {

	void OnInputExit(InputEventData eventData);
}

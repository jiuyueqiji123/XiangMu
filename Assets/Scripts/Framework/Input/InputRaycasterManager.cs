﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputRaycasterManager {

	private static readonly List<InputRaycasterBase> s_Raycasters = new List<InputRaycasterBase>();

	public static void AddRaycaster(InputRaycasterBase baseRaycaster)
	{
		if (!InputRaycasterManager.s_Raycasters.Contains(baseRaycaster))
		{
			InputRaycasterManager.s_Raycasters.Add(baseRaycaster);
		}
	}

	public static List<InputRaycasterBase> GetRaycasters()
	{
		return InputRaycasterManager.s_Raycasters;
	}

	public static void RemoveRaycasters(InputRaycasterBase baseRaycaster)
	{
		if (InputRaycasterManager.s_Raycasters.Contains(baseRaycaster))
		{
			InputRaycasterManager.s_Raycasters.Remove(baseRaycaster);
		}
	}
}

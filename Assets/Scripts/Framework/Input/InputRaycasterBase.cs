﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputRaycasterBase : MonoBehaviour {

	public abstract Camera eventCamera {
		get;
	}

	public virtual int sortOrderPriority
	{
		get
		{
			return int.MinValue;
		}
	}

	public virtual int renderOrderPriority
	{
		get
		{
			return int.MinValue;
		}
	}

	public abstract void Raycast(InputEventData eventData, List<stRaycastResult> resultAppendList);

	protected virtual void OnEnable() {
		InputRaycasterManager.AddRaycaster (this);
	}

	protected virtual void OnDisable() {
		InputRaycasterManager.RemoveRaycasters (this);
	}
}

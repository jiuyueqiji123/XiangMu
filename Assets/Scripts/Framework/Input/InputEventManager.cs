﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(InputModule))]
public class InputEventManager : MonoSingleton<InputEventManager> {

	private InputModule mInputModule;

	private static readonly Comparison<stRaycastResult> s_RaycastComparer = new Comparison<stRaycastResult> (RaycastComparer);

	private bool m_HasFocus = true;

    private bool m_interlFocus = true;

	private int m_DragThreshold = 5;

	public bool isFocused
	{
		get
		{
            return this.m_HasFocus && m_interlFocus;
		}
	}

    public bool InputFocused
    {
        set
        {
            this.m_HasFocus = value;
        }
    }

	public int pixelDragThreshold
	{
		get
		{
			return this.m_DragThreshold;
		}
		set
		{
			this.m_DragThreshold = value;
		}
	}

	protected override void Init ()
	{
		base.Init ();
		this.mInputModule = GetComponent<InputModule> ();
	}

	void Update() {
		if (this.mInputModule != null) {
			this.mInputModule.Process ();
		}
	}

	void OnApplicationFocus(bool hasFocus)
	{
        m_interlFocus = hasFocus;
	}

	public void RaycastAll(InputEventData eventData, List<stRaycastResult> rayResult) {
		rayResult.Clear ();
		List<InputRaycasterBase> raycasters = InputRaycasterManager.GetRaycasters();
		for (int i = 0; i < raycasters.Count; i++)
		{
			InputRaycasterBase baseRaycaster = raycasters[i];
			if ((baseRaycaster != null) && baseRaycaster.isActiveAndEnabled)
			{
				baseRaycaster.Raycast(eventData, rayResult);
			}
		}
		rayResult.Sort (InputEventManager.s_RaycastComparer);
	}

	private static int RaycastComparer(stRaycastResult lhs, stRaycastResult rhs)
	{
		int result;
		if (rhs.module != lhs.module) {
			Camera eventCamera = lhs.module.eventCamera;
			Camera eventCamera2 = rhs.module.eventCamera;
			if (eventCamera != null && eventCamera2 != null && eventCamera.depth != eventCamera2.depth) { //降序
				if (eventCamera.depth < eventCamera2.depth) {
					result = 1;
					return result;
				}
				if (eventCamera.depth == eventCamera2.depth) {
					result = 0;
					return result;
				}
				result = -1;
				return result;
			} else {
				if (lhs.module.sortOrderPriority != rhs.module.sortOrderPriority)
				{
					result = rhs.module.sortOrderPriority.CompareTo(lhs.module.sortOrderPriority);
					return result;
				}
				if (lhs.module.renderOrderPriority != rhs.module.renderOrderPriority)
				{
					result = rhs.module.renderOrderPriority.CompareTo(lhs.module.renderOrderPriority);
					return result;
				}
			}
		}
		if (lhs.sortingLayer != rhs.sortingLayer) //降序
		{
			int layerValueFromID = SortingLayer.GetLayerValueFromID(rhs.sortingLayer);
			int layerValueFromID2 = SortingLayer.GetLayerValueFromID(lhs.sortingLayer);
			result = layerValueFromID.CompareTo(layerValueFromID2);
		}
		else if (lhs.sortingOrder != rhs.sortingOrder) //降序
		{
			result = rhs.sortingOrder.CompareTo(lhs.sortingOrder);
		}
		else if (lhs.distance != rhs.distance) //升序
		{
			result = lhs.distance.CompareTo(rhs.distance);
		}
		else //升序
		{
			result = lhs.index.CompareTo(rhs.index);
		}
		return result;
	}
} 

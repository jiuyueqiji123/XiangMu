﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputBase : MonoBehaviour {

	public virtual bool mousePresent {
		get {
			return Input.mousePresent;
		}
	}

	public virtual bool GetMouseButtonDown(int button)
	{
		return Input.GetMouseButtonDown(button);
	}

	public virtual bool GetMouseButtonUp(int button)
	{
		return Input.GetMouseButtonUp(button);
	}

	public virtual bool GetMouseButton(int button)
	{
		return Input.GetMouseButton(button);
	}

	public virtual Vector2 mousePosition
	{
		get
		{
			return Input.mousePosition;
		}
	}

	public virtual Touch GetTouch(int index)
	{
		return Input.GetTouch(index);
	}

	public virtual bool touchSupported
	{
		get
		{
			return Input.touchSupported;
		}
	}

	public virtual int touchCount
	{
		get
		{
			return Input.touchCount;
		}
	}
}

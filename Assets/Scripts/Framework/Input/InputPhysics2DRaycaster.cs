﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Camera))]
public class InputPhysics2DRaycaster : InputPhysicsRaycaster {

	private RaycastHit2D[] m_Hits;

	public override void Raycast (InputEventData eventData, List<stRaycastResult> resultAppendList)
	{
		if (this.eventCamera != null) {
			Ray r;
			float disToClip;
			this.ComputeRayAndDistance (eventData, out r, out disToClip);
			int num;
			this.m_Hits = Physics2D.GetRayIntersectionAll (r, disToClip, this.eventMask);
			num = this.m_Hits.Length;
			if (num > 0) {
				int i = 0;
				while (i < num) {
					SpriteRenderer component = this.m_Hits [i].collider.gameObject.GetComponent<SpriteRenderer> ();
					stRaycastResult item = new stRaycastResult {
						gameObject = this.m_Hits [i].collider.gameObject,
						module = this,
						distance = Vector3.Distance(this.eventCamera.transform.position, this.m_Hits[i].point),
						worldPosition = this.m_Hits [i].point,
						worldNormal = this.m_Hits [i].normal,
						screenPosition = eventData.Position,
						index = (float)resultAppendList.Count,
						sortingLayer = (component == null ? 0 : component.sortingLayerID),
						sortingOrder = (component == null ? 0 : component.sortingOrder)
					};
					resultAppendList.Add (item);
					i++;
				}
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct stRaycastResult {

	private GameObject m_GameObject;

	public InputRaycasterBase module;

	public float distance;

	public float index;

	public int sortingLayer;

	public int sortingOrder;

	public Vector3 worldPosition;

	public Vector3 worldNormal;

	public Vector2 screenPosition;

	public GameObject gameObject
	{
		get
		{
			return this.m_GameObject;
		}
		set
		{
			this.m_GameObject = value;
		}
	}
}

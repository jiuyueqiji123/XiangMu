﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class InputEventData {

	public enum FramePressState
	{
		Pressed,
		Released,
		PressedAndReleased,
		NotChanged
	}

	protected bool m_Used;

	public virtual bool used
	{
		get
		{
			return this.m_Used;
		}
	}

	public virtual void Reset()
	{
		this.m_Used = false;
	}

	public virtual void Use()
	{
		this.m_Used = true;
	}

	private InputEventManager mEventManager;

	public Vector2 Position;

	public int PointerId;

	public Vector2 Delta;

	public stRaycastResult CurrentRaycast;

	/// <summary>
	/// 是合适的click对象
	/// </summary>
	public bool eligibleForClick;

	public bool dragging;

	public bool useDragThreshold;

	public Vector2 pressPosition;

	public stRaycastResult PressRaycast;

	/// <summary>
	/// 上次点击的对象
	/// </summary>
	public GameObject lastPress;

	/// <summary>
	/// 上次点击的时间
	/// </summary>
	public float clickTime;

	public int clickCount;

	private GameObject m_PointerPress;

	/// <summary>
	/// 点击对象，可能是父对象
	/// </summary>
	/// <value>The pointer press.</value>
	public GameObject pointerPress
	{
		get
		{
			return this.m_PointerPress;
		}
		set
		{
			if (this.m_PointerPress != value)
			{
				this.lastPress = this.m_PointerPress;
				this.m_PointerPress = value;
			}
		}
	}

	/// <summary>
	/// 原始点击对象
	/// </summary>
	public GameObject rawPointerPress;

	public GameObject pointerDrag;

	public GameObject pointerEnter;

	public List<GameObject> hovered = new List<GameObject>();

	public Camera enterEventCamera
	{
		get
		{
			return (this.CurrentRaycast.module != null) ? this.CurrentRaycast.module.eventCamera : null;
		}
	}

	public Camera pressEventCamera
	{
		get
		{
			return (this.PressRaycast.module != null) ? this.PressRaycast.module.eventCamera : null;
		}
	}

	public InputEventData (InputEventManager eventManager) {
		this.mEventManager = eventManager;
		this.Position = Vector2.zero;
		this.PointerId = -1;
		this.Delta = Vector2.zero;
		this.pressPosition = Vector2.zero;
		this.clickTime = 0;
		this.clickCount = 0;
		this.useDragThreshold = true;
		this.dragging = false;
	}

	public bool IsPointerMoving()
	{
		return this.Delta.sqrMagnitude > 0f;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("<b>Position</b>: " + this.Position);
		stringBuilder.AppendLine("<b>delta</b>: " + this.Delta);
		stringBuilder.AppendLine("<b>eligibleForClick</b>: " + this.eligibleForClick);
		stringBuilder.AppendLine("<b>pointerEnter</b>: " + this.pointerEnter);
		stringBuilder.AppendLine("<b>pointerPress</b>: " + this.pointerPress);
		stringBuilder.AppendLine("<b>lastPointerPress</b>: " + this.lastPress);
		stringBuilder.AppendLine("<b>pointerDrag</b>: " + this.pointerDrag);
		stringBuilder.AppendLine("<b>Use Drag Threshold</b>: " + this.useDragThreshold);
		stringBuilder.AppendLine("<b>Current Rayast:</b>");
		stringBuilder.AppendLine(this.CurrentRaycast.ToString());
		stringBuilder.AppendLine("<b>Press Rayast:</b>");
		stringBuilder.AppendLine(this.PressRaycast.ToString());
		return stringBuilder.ToString();
	}
}

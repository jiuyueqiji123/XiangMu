﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

public class AppVersion {

	private static int mVersion;

	private static int mCodeVersion;

	private static int mResourceVersion;

    public static Dictionary<string, int> ModuleVersion = new Dictionary<string, int>();

	public static void Set(int app, int code, int res) {
		mVersion = app;
		mCodeVersion = code;
		mResourceVersion = res;
	}

	public static int Version {
		get {
			if (mVersion == 0) {
				Init ();
			}
			return mVersion;
		}
	}

	public static int CodeVersion {
		get {
			if (mCodeVersion == 0) {
				Init ();
			}
			return mCodeVersion;
		}
	}

	public static int ResVersion {
		get {
			if (mResourceVersion == 0) {
				Init ();
			}
			return mResourceVersion;
		}
	}

	public static void Init() {
		string filePath = ResourceManager.ResInfoFilePath;
		if (FileTools.IsFileExist (filePath)) {
			string info = File.ReadAllText (filePath);
			ResourceInfo resInfo = JsonUtility.FromJson<ResourceInfo> (info);
			mVersion = resInfo.AppVersion;
			mCodeVersion = resInfo.CodeVersion;
			mResourceVersion = resInfo.ResVersion;
            foreach(ModuleItemInfo item in resInfo.ModuleList)
            {
                Debug.Log("------- ModuleVersion  -- Name: " + item.Name + "   CodeVersion: " + item.CodeVersion);
                ModuleVersion[item.Name] = item.CodeVersion;
            }
		} else {
			Debug.LogError ("没有版本文件！");
			return;
		}
	}

	public static string ToString() {
		return mVersion + "." + mCodeVersion + "." + mResourceVersion;
	}
}

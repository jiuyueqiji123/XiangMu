﻿using UnityEngine;
using System.Collections;
using System;

public class Resource {
	
	public int m_key;

	public string m_name;

	public string m_fullPathInResources;

	public string m_fullPathInResourcesWithoutExtension;

	public string m_fileFullPathInResources;

	public Type m_contentType;

	public enResourceType m_resourceType;

	public enResourceState m_state;

	public bool m_unloadBelongedAssetBundleAfterLoaded;

    public Action<Resource> callback;

    public UnityEngine.Object m_content;

	public bool m_isAbandon;

	public Resource(int keyHash, string fullPathInResources, Type contentType, enResourceType resourceType, bool unloadBelongedAssetBundleAfterLoaded)
	{
		this.m_key = keyHash;
		this.m_fullPathInResources = fullPathInResources;
		this.m_fullPathInResourcesWithoutExtension = FileTools.EraseExtension(this.m_fullPathInResources);
		this.m_name = FileTools.GetFullName (fullPathInResources);
		this.m_resourceType = resourceType;
		this.m_state = enResourceState.Unload;
		this.m_contentType = contentType;
		this.m_unloadBelongedAssetBundleAfterLoaded = unloadBelongedAssetBundleAfterLoaded;
		this.m_content = null;
    }

    public Resource(int keyHash, string fullPathInResources, Type contentType, enResourceType resourceType, bool unloadBelongedAssetBundleAfterLoaded, Action<Resource> callback)
    {
        this.m_key = keyHash;
        this.m_fullPathInResources = fullPathInResources;
        this.m_fullPathInResourcesWithoutExtension = FileTools.EraseExtension(this.m_fullPathInResources);
        this.m_name = FileTools.GetFullName(fullPathInResources);
        this.m_resourceType = resourceType;
        this.m_state = enResourceState.Unload;
        this.m_contentType = contentType;
        this.m_unloadBelongedAssetBundleAfterLoaded = unloadBelongedAssetBundleAfterLoaded;
        this.callback = callback;
        this.m_content = null;
    }

    public void Load()
	{
		if (this.m_isAbandon)
		{
			this.m_state = enResourceState.Unload;
			return;
		}
		if (this.m_contentType == null)
		{
			this.m_content = Resources.Load(this.m_fullPathInResourcesWithoutExtension);
		}
		else
		{
			this.m_content = Resources.Load(this.m_fullPathInResourcesWithoutExtension, this.m_contentType);
		}
		this.m_state = enResourceState.Loaded;
		if (this.m_content != null && this.m_content.GetType() == typeof(TextAsset))
		{
			BinaryObject cBinaryObject = ScriptableObject.CreateInstance<BinaryObject>();
			cBinaryObject.m_data = (this.m_content as TextAsset).bytes;
			this.m_content = cBinaryObject;
		}
    }

    public void LoadAsync()
    {
        if (this.m_isAbandon)
        {
            this.m_state = enResourceState.Unload;
            return;
        }
        if (this.m_contentType == null)
        {
            ResourceRequest request = Resources.LoadAsync(this.m_fullPathInResourcesWithoutExtension);
            request.completed += Request_completed;
        }
        else
        {
            ResourceRequest request = Resources.LoadAsync(this.m_fullPathInResourcesWithoutExtension, this.m_contentType);
            request.completed += Request_completed;
        }
        this.m_state = enResourceState.Loaded;
    }

    private void Request_completed(AsyncOperation obj)
    {
        ResourceRequest request = obj as ResourceRequest;
        this.m_content = request.asset;
        if (this.m_content != null && this.m_content.GetType() == typeof(TextAsset))
        {
            BinaryObject cBinaryObject = ScriptableObject.CreateInstance<BinaryObject>();
            cBinaryObject.m_data = (this.m_content as TextAsset).bytes;
            this.m_content = cBinaryObject;
        }
        if (callback != null)
        {
            callback(this);
        }
    }

    /// <summary>
    /// 从路径中加载， 不是AssetBundle, 也不是Resources
    /// </summary>
    /// <param name="ifsExtractPath">Ifs extract path.</param>
    public void Load(string ifsExtractPath)
	{
		if (this.m_isAbandon)
		{
			this.m_state = enResourceState.Unload;
			return;
		}
		byte[] array = FileTools.ReadFile(FileTools.CombinePath(ifsExtractPath, this.m_fileFullPathInResources));
		this.m_state = enResourceState.Loaded;
		if (array != null)
		{
			BinaryObject cBinaryObject = ScriptableObject.CreateInstance<BinaryObject>();
			cBinaryObject.m_data = array;
			cBinaryObject.name = this.m_name;
			this.m_content = cBinaryObject;
		}
	}

	public void LoadFromAssetBundle(ResourcePackerInfo resourcePackerInfo)
	{
		if (this.m_isAbandon)
		{
			this.m_state = enResourceState.Unload;
			return;
		}
		string text = FileTools.EraseExtension(this.m_name);
		if (this.m_contentType == null)
		{
			this.m_content = resourcePackerInfo.m_assetBundle.LoadAsset(text);
		}
		else
		{
			this.m_content = resourcePackerInfo.m_assetBundle.LoadAsset(text, this.m_contentType);
		}
		this.m_state = enResourceState.Loaded;
		if (this.m_content != null && this.m_content.GetType() == typeof(TextAsset))
		{
			BinaryObject cBinaryObject = ScriptableObject.CreateInstance<BinaryObject>();
			cBinaryObject.m_data = (this.m_content as TextAsset).bytes;
			this.m_content = cBinaryObject;
		}
    }

    public void Unload()
	{
		if (this.m_state == enResourceState.Loaded)
		{
			this.m_content = null;
			this.m_state = enResourceState.Unload;
		}
		else if (this.m_state == enResourceState.Loading)
		{
			this.m_isAbandon = true;
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResourcePackerInfoSet {

	private Dictionary<string, ResourcePackerInfo> m_resourcePackerInfosAll = new Dictionary<string, ResourcePackerInfo>();

	private Dictionary<int, ResourcePackerInfo> m_resourceMap = new Dictionary<int, ResourcePackerInfo>();

	public void Dispose()
	{
		foreach (string key in m_resourcePackerInfosAll.Keys) {
			if (this.m_resourcePackerInfosAll[key].IsAssetBundleLoaded())
			{
				this.m_resourcePackerInfosAll[key].UnloadAssetBundle(false);
			}
			this.m_resourcePackerInfosAll[key] = null;
		}

		this.m_resourcePackerInfosAll.Clear();
		this.m_resourceMap.Clear();
	}

	public void Read(string info) {
		ResourceInfo resInfo = JsonUtility.FromJson<ResourceInfo> (info);
		AppVersion.Set (resInfo.AppVersion, resInfo.CodeVersion, resInfo.ResVersion);
		this.m_resourcePackerInfosAll.Clear ();
		for (int i = 0; i < resInfo.ResList.Count; i++) {
			ResourcePackerInfo packerInfo = new ResourcePackerInfo ();
			packerInfo.Read (resInfo.ResList [i]);
			this.m_resourcePackerInfosAll.Add(packerInfo.m_name, packerInfo);
		}
	}

	public void AddInfo(string info) {
		ResourceInfo resInfo = JsonUtility.FromJson<ResourceInfo> (info);
		if (resInfo != null) {
			for (int i = 0; i < resInfo.ResList.Count; i++) {
				ResourcePackerInfo packerInfo = new ResourcePackerInfo ();
				packerInfo.Read (resInfo.ResList [i]);
                if (this.m_resourcePackerInfosAll.ContainsKey(packerInfo.m_name))
                {
                    this.m_resourcePackerInfosAll[packerInfo.m_name].UnloadAssetBundle(true);
                }
                this.m_resourcePackerInfosAll[packerInfo.m_name] = packerInfo;
				packerInfo.SetDependenies();
				packerInfo.AddToResourceMap(this.m_resourceMap);
			}
		}
	}

//	public void AddResourcePackerInfo(ResourcePackerInfo resourceInfo)
//	{
//		this.m_resourcePackerInfosAll.Add(resourceInfo);
//		for (int i = 0; i < resourceInfo.m_children.Count; i++)
//		{
//			this._AddResourcePackerInfoAll(resourceInfo.m_children[i]);
//		}
//	}
//
//	private void _AddResourcePackerInfoAll(ResourcePackerInfo resourceInfo)
//	{
//		this.m_resourcePackerInfosAll.Add(resourceInfo);
//		for (int i = 0; i < resourceInfo.m_children.Count; i++)
//		{
//			this._AddResourcePackerInfoAll(resourceInfo.m_children[i]);
//		}
//	}

	public void CreateResourceMap()
	{
		foreach (string key in m_resourcePackerInfosAll.Keys) {
			ResourcePackerInfo cResourcePackerInfo = this.m_resourcePackerInfosAll[key];
			//随便设置一下引用
			cResourcePackerInfo.SetDependenies();
			cResourcePackerInfo.AddToResourceMap(this.m_resourceMap);
		}
	}

	public ResourcePackerInfo GetPackerInfo(string name) {
		ResourcePackerInfo result = null;
		if (this.m_resourcePackerInfosAll.TryGetValue (name, out result)) {
			return  result;
		}
		return null;
	}


	public ResourcePackerInfo GetResourceBelongedPackerInfo(int resourceKeyHash)
	{
		ResourcePackerInfo result = null;
		if (this.m_resourceMap.TryGetValue(resourceKeyHash, out result))
		{
			return result;
		}
		return null;
	}

	public void UnloadAll() {
		foreach(int key in m_resourceMap.Keys) {
			if (this.m_resourceMap[key].IsAssetBundleLoaded())
			{
				this.m_resourceMap[key].UnloadAssetBundle(false);
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public enum enAssetBundleState {
	Unload,
	Loading,
	Loaded
}

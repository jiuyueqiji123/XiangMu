﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ResourcePackerInfo {

	public bool m_isAssetBundle;

	public enAssetBundleState m_assetBundleState;

	public AssetBundle m_assetBundle;

	public string m_path;

	public string m_name;

	public bool m_useASyncLoadingData;

	public Dictionary<int, string> m_fileExtMap;

	private ResourcePackerInfo m_parent;

	private string[] m_dependencies;

	public List<ResourcePackerInfo> m_dependencyList = new List<ResourcePackerInfo>();

	private List<int> m_resourceInfos = new List<int> ();

	private bool m_Resident; 

	public List<ResourcePackerInfo> Dependencies
	{
		get
		{
			return this.m_dependencyList;
		}
	}

	public ResourcePackerInfo()
	{
		this.m_assetBundleState = enAssetBundleState.Unload;
		this.m_useASyncLoadingData = false;
	}

	public bool IsResident()
	{
		return this.Dependencies.Count == 0 && this.m_Resident;
	}

	public bool IsAssetBundleLoaded()
	{
		return this.m_isAssetBundle && this.m_assetBundleState == enAssetBundleState.Loaded;
	}

	public bool IsAssetBundleInLoading()
	{
		return this.m_isAssetBundle && this.m_assetBundleState == enAssetBundleState.Loading;
	}

	public void Read(ResourceItemInfo itemInfo) {
		this.m_isAssetBundle = itemInfo.IsAsset;
		this.m_path = itemInfo.Path;
		this.m_name = itemInfo.Name;
		this.m_Resident = itemInfo.IsResident;
		if (this.m_isAssetBundle) {
			for (int i = 0; i < itemInfo.ResNames.Count; i++) {
				this.m_resourceInfos.Add ((this.m_path + "/" + itemInfo.ResNames[i]).JavaHashCodeIgnoreCase());
			}
		}
		this.m_dependencies = itemInfo.Dependencies;
	}

	public void SetDependenies() {
		m_dependencyList.Clear ();
		for (int j = 0; j < m_dependencies.Length; j++) {
			
			ResourcePackerInfo packerInfo = ResourceManager.Instance.GetPackerInfoByName (m_dependencies [j]);
			if (packerInfo != null) {
				m_dependencyList.Add (packerInfo);
			}
			//packerInfo.Read (itemInfo.Dependencies [j]);
			//packerInfo.dependency = this;
		}
	}

	public void LoadAssetBundle(string ifsExtractPath)
	{
		if (this.m_isAssetBundle)
		{
			for (int i = 0; i < Dependencies.Count; i++) {
				if (this.Dependencies[i] != null && this.Dependencies[i].m_isAssetBundle && !this.Dependencies[i].IsAssetBundleLoaded())
				{
					this.Dependencies[i].LoadAssetBundle(ifsExtractPath);
				}
			}

			if (this.m_assetBundleState != enAssetBundleState.Unload)
			{
				return;
			}
			this.m_useASyncLoadingData = false;
			string text = FileTools.CombinePath(ifsExtractPath, this.m_name);
			if (FileTools.IsFileExist(text))
			{
				int num = 0;
				while (true)
				{
					try
					{
						this.m_assetBundle = AssetBundle.LoadFromFile(text);
					}
					catch (Exception)
					{
						this.m_assetBundle = null;
					}
					if (this.m_assetBundle != null)
					{
						break;
					}
					num++;
					if (num >= 3)
					{
						break;
					}
				}
					
				if (this.m_assetBundle == null) {
					Debug.LogError("Load AssetBundle " + text + " Error!!!");
				}
			}
			else
			{
				Debug.LogError("File " + text + " can not be found!!!");
			}
			this.m_assetBundleState = enAssetBundleState.Loaded;
		}
	}

	public IEnumerator ASyncLoadAssetBundle(string ifsExtractPath)
	{
		if (this.m_isAssetBundle) {
			this.m_useASyncLoadingData = true;
			this.m_assetBundleState = enAssetBundleState.Loading;
			AssetBundleCreateRequest bundle = AssetBundle.LoadFromFileAsync(FileTools.CombinePath(ifsExtractPath, this.m_name));
			yield return bundle;
			if (m_useASyncLoadingData) {
				this.m_assetBundle = bundle.assetBundle;
			}
			this.m_assetBundleState = enAssetBundleState.Loaded;
		}
	}

	public void UnloadAssetBundle(bool force = false)
	{
		if (!this.m_isAssetBundle)
		{
			return;
		}
		if (this.IsResident() && !force)
		{
			return;
		}
		if (this.m_assetBundleState == enAssetBundleState.Loaded)
		{
			if (this.m_assetBundle != null)
			{
				this.m_assetBundle.Unload(false);
				this.m_assetBundle = null;
			}
			this.m_assetBundleState = enAssetBundleState.Unload;
		}
		else if (this.m_assetBundleState == enAssetBundleState.Loading)
		{
			this.m_useASyncLoadingData = false;
		}
		for (int i = 0; i < Dependencies.Count; i++) {
			if (this.Dependencies[i] != null) {
				this.Dependencies[i].UnloadAssetBundle (force);
			}
		}
	}

	public void AddToResourceMap(Dictionary<int, ResourcePackerInfo> map)
	{
		for (int i = 0; i < this.m_resourceInfos.Count; i++)
		{
            map[this.m_resourceInfos[i]] = this;
            //if (!map.ContainsKey(this.m_resourceInfos[i]))
            //{
            //	map.Add(this.m_resourceInfos[i], this);
            //}
        }
	}
}

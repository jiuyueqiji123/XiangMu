﻿using UnityEngine;
using System.Collections;

public enum enResourceType {
	UIForm,
	UIPrefab,
	Sound,
	Prefab,
	TableProto,
	GameString,
	ScenePrefab,
	Lua,
}

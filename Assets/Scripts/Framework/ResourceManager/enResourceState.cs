﻿using UnityEngine;
using System.Collections;

public enum enResourceState {
	Unload,
	Loading,
	Loaded
}

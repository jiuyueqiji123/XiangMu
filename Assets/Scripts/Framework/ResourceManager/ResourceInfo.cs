﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ResourceInfo {

	public int AppVersion;
	public int CodeVersion;
	public int ResVersion;
	public long ResLength;
	public List<ResourceItemInfo> ResList;
	public List<ModuleItemInfo> ModuleList;
}

[Serializable]
public class ResourceItemInfo {
	public string Path;
	public string Name;
	public bool IsAsset;
	public bool IsResident;
	public List<string> ResNames;
	public string[] Dependencies;
}

[Serializable]
public class ModuleItemInfo {
	public string Name;
	public int CodeVersion;
}

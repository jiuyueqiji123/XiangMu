﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class TextManager : Singleton<TextManager> {

	private const string TEXT_PATH = "text/Text.txt";

	private Dictionary<string, string> m_table;

	public void Load() {
		BinaryObject content = ResourceManager.Instance.GetResource (TEXT_PATH, typeof(TextAsset), enResourceType.GameString).m_content as BinaryObject;
		if (content == null) {
			Debug.LogError ("游戏字符文件加载失败！");
			return;
		}
		m_table = new Dictionary<string, string>();
		string str = Encoding.UTF8.GetString(content.m_data);
		string[] strArray = str.Split (new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
		for(int i=0; i<strArray.Length; i++) {
			string text = strArray [i];
			if (text.Length != 0 && !(text.StartsWith ("#"))) {
				string[] array = text.Split (new char[] {'\t'});
				if (array.Length == 2) {
					m_table [array [0]] = array [1];
				}
			}
		}
	}

	public bool IsTextLoaded() {
		return m_table != null;
	}

	public string GetText(string key) {
		if (m_table == null) {
			this.Load ();
		}
		string text = string.Empty;
		this.m_table.TryGetValue(key, out text);
		if (string.IsNullOrEmpty(text))
		{
			text = key;
		}
		return text;
	}

	public string GetText(string key, params object[] args)
	{
		return string.Format(GetText(key), args);
	}
}

﻿using UnityEngine;
using System.Collections;

public class EventID
{

    #region common
    public const string GAME_QUIT = "GameQuit";
    public const string GAME_PAUSE = "GamePause";
    public const string GAME_RESUME = "GameResume";
    #endregion

    public const string PLAYER_DIAMOND_CHANGE = "PlayerDiamondChange"; //钻石改变
    public const string PLAYER_GOLD_CHANGE = "PlayerGoldChange"; //金币改变

    public const string VERSION_UPDATE_PROGRESS_CHANGE = "VersionUpdateProgressChange";
    public const string VERSION_UPDATE_DES_CHANGE = "VersionUpdateDesChange";
    public const string MODULE_NEED_UPDATE = "ModuleNeedUpdate";
    public const string MODULE_UNZIP_DONE = "ModuleUnzipDone";
    public const string MODULE_DOWNLOAD_ERROR = "ModuleDownloadError";
    public const string MODULE_PROGRESS_CHANGE = "ModuleProgressChange";

    public const string SDK_DOWNLOADFINISHED = "SDK_DOWNLOADFINISHED";  //通过sdk下载完毕

    #region Set
    public const string SET_AUDIO_SOUND_CHANGE = "ChangeSoundMuted";
    public const string SET_AUDIO_MUSIC_CHANGE = "ChangeMusicMuted";
    #endregion

    public const string ON_MAIN_GAME_PLAY_OR_EXIT = "ON_MAIN_GAME_PLAY_OR_EXIT";    // 主界面小游戏开始或者退出

    // GameState ChangeEvent

    #region study english math geometry

    public const string ON_GAMESTATE_STUDYENGLISH_ENTER = "ON_GAMESTATE_STUDYENGLISH_ENTER";
    public const string ON_GAMESTATE_STUDYENGLISH_EXIT = "ON_GAMESTATE_STUDYENGLISH_EXIT";
    public const string ON_GAMESTATE_STUDYMATH_ENTER = "ON_GAMESTATE_STUDYMATH_ENTER";
    public const string ON_GAMESTATE_STUDYMATH_EXIT = "ON_GAMESTATE_STUDYMATH_EXIT";
    public const string ON_GAMESTATE_STUDYGEOMETRY_ENTER = "ON_GAMESTATE_STUDYGEOMETRY_ENTER";
    public const string ON_GAMESTATE_STUDYGEOMETRY_EXIT = "ON_GAMESTATE_STUDYGEOMETRY_EXIT";

    public const string ON_MOUSE_BUTTON_DOWN = "ON_MOUSE_BUTTON_DOWN";    // 鼠标按住事件
    public const string ON_MOUSE_BUTTON_HOVER = "ON_MOUSE_BUTTON_HOVER";    // 鼠标按住事件
    public const string ON_MOUSE_BUTTON_UP = "ON_MOUSE_BUTTON_UP";    // 鼠标按住事件

    public const string ON_CARD_GAME_EXIT = "ON_CARD_GAME_EXIT";
    public const string ON_CARD_GAME_CARRY_OUT = "ON_CARD_GAME_CARRY_OUT";


    // 字母认知

    public const string ON_ENGLISH_WINDOW_BTN_RETURN_CLICK = "ON_ENGLISH_WINDOW_BTN_RETURN_CLICK";    // 英语界面返回按钮事件
    public const string ON_ENGLISH_SELECT_GAME = "ON_ENGLISH_SELECT_GAME";    // 字母认知游戏选择
    public const string ON_ENGLISH_TV_START_PLAY = "ON_ENGLISH_TV_START_PLAY";    // 绘本电视开始播放
    public const string ON_ENGLISH_GAME_PART_EXIT = "ON_ENGLISH_GAME_PART_EXIT";    // 英语游戏推出

    // 认识数字

    public const string ON_MATHNUMBER_WINDOW_BTN_RETURN_CLICK = "ON_MATHNUMBER_WINDOW_BTN_RETURN_CLICK";    // 认识数字场景返回按钮事件
    public const string ON_MATH_GAME_FLOW_EXIT = "ON_MATH_GAME_FLOW_EXIT";    // 认识数字


    #endregion


    /// 职业体验-小爱蛋糕师
    /// 场景元素响应
    public const string ON_SCENEGAMEBODY_CLICK = "ON_JOBEXPERIENCE_NEXT";
    /// 拖动鸡蛋
    public const string ON_SCENEGAMEBODY_DRAG_EGG = "ON_SCENEGAMEBODY_DRAG_EGG";

    #region 冒险故事
    ///喜马拉雅的风筝
    public const string juqing10001 = "10001";

    public const string juqing10002 = "10002";

    public const string juqing10003 = "10003";

    public const string juqing10004 = "10004";

    public const string juqing10005 = "10005";

    public const string juqing10006 = "10006";

    public const string juqing10007 = "10007";

    public const string juqing10008 = "10008";

    public const string juqing10009 = "10009";

    public const string juqing10010 = "10010";

    public const string juqing10011 = "10011";

    public const string juqing10012 = "10012";

    public const string juqing10013 = "10013";

    public const string juqing10014 = "10014";

    public const string juqing10015 = "10015";

    public const string juqing10016 = "10016";

    public const string juqing10017 = "10017";

    public const string juqing10018 = "10018";

    public const string juqing10019 = "10019";

    //马尔代夫的海底
    public const string juqing20001 = "20001";

    public const string juqing20002 = "20002";

    public const string juqing20003 = "20003";

    public const string juqing20004 = "20004";

    public const string juqing20005 = "20005";

    public const string juqing20006 = "20006";

    public const string juqing20007 = "20007";

    public const string juqing20008 = "20008";

    public const string juqing20009 = "20009";

    public const string juqing20010 = "20010";

    public const string juqing20011 = "20011";

    public const string juqing20012 = "20012";

    public const string juqing20013 = "20013";

    public const string juqing20014 = "20014";

    public const string juqing20015 = "20015";

    public const string juqing20016 = "20016";

    public const string juqing20017 = "20017";

    public const string juqing20018 = "20018";

    public const string juqing20019 = "20019";

    #endregion

    #region phonics
    public const string PHONICS_CLICK_LETTER = "PhonicsClickLetter";
    public const string PHONICS_GAME_END = "PhonicsGameEnd";
    #endregion

    //趣味拼图
    public const string PHOTO_GAME_END = "PhotoGameEnd";
    public const string PHOTO_SHOW_EFFECT = "PhotoShowEffect";
    public const string SHOW_FINISH_BTN = "ShowFinishBtn";

    #region parent
    public const string USER_INFO_CHANGE = "UserInfoChange";
    public const string PHONE_LOGIN_SUC = "PhoneLoginSuc";
    public const string PHONE_CODE_VARIFY = "PhoneCodeVarify";
    public const string VERITY_COUNT_DOWN = "VerityCountDown";
    #endregion

    public const string SET_PHOTO = "SetPhoto";

    public const string MOVE_PHOTO = "MovePhoto";

    #region 快递

    public const string ON_EXPRESS_DOWNLOAD_SUCCESS = "ON_EXPRESS_DOWNLOAD_SUCCESS";
    public const string ON_EXPRESS_DOWNLOAD_START = "ON_EXPRESS_DOWNLOAD_START";
    public const string ON_EXPRESS_DOWNLOAD_PROESSUPDATE = "ON_EXPRESS_DOWNLOAD_PROESSUPDATE";

    public const string ON_EXPRESS_DATA_INIT_FINISHED = "ON_EXPRESS_DATA_INIT_FINISHED";


    //点击了快递按钮
    public const string ON_EXPRESS_BTN_CLICKED = "ON_EXPRESS_BTN_CLICKED";

    //送快递介绍完毕
    public const string ON_EXPRESS_ANIM_FINISHED = "ON_EXPRESS_ANIM_FINISHED";

    public const string ON_EXPRESS_MOVE_COMPLETE = "ON_EXPRESS_MOVE_COMPLETE";

    public const string ON_EXPRESS_PIC_INITED = "ON_EXPRESS_PIC_INITED";


    #endregion

    #region 主界面小游戏
    public const string RESET_TIMER = "RESET_TIMER";
    #endregion

    #region 减法

    public const string CARRY_OUT_ANIMALGAME = "CARRY_OUT_ANIMALGAME";


    #endregion
    #region main
    public const string MAIN_STATE_ENTER = "MainStateEnter";
    public const string MAIN_STATE_EXIT = "MainStateExit";
    public const string MAIN_MOVE_COMPELTE = "MainMoveComplete";
    public const string MAIN_GO_OTHER_VIEW = "MainGoOtherView";
    #endregion

    public const string GUIDAO_MOVE = "GuiDaoMove";

    public const string SET_ZHUANPAN_ACTIVE = "SetZhuanPanActive";

    public const string SET_LEDI2D_ACTIVE = "SetLedi2dActive";


    #region Timeline

    public const string ON_TIMELINE_CLICK_EVENT = "ON_TIMELINE_CLICK_EVENT";
    public const string ON_TIMELINE_PAUSE = "ON_TIMELINE_PAUSE";
    public const string ON_TIMELINE_RAYCAST_ENABLE = "ON_TIMELINE_RAYCAST_ENABLE";
    public const string ON_TIMELINE_RAYCAST_CLOSE = "ON_TIMELINE_RAYCAST_CLOSE";

    #endregion



    public const string PAY_BUY_SUC = "PayManagerBuySuc";

    public const string CANCEL_VERITY = "CancelVerity";
    public const string CANCEL_PAY = "CancelPay";

    public const string ADS_DONE_CALLBACK = "ads_done_callback";

    #region package-english

    #region AR百科

    public const string ARBaike_CommonResourcesLoadComplete = "ARBaike_CommonResourcesLoadComplete";
    public const string ARPlace_Canceled = "ARPlace_Canceled";
    public const string Arkit_Ground_Checked = "Arkit_Ground_Checked";
    public const string ArKit_Gyro_Start = "ArKit_Gyro_Start";
    public const string ARBaike_RefreshMainWindow = "ARBaike_RefreshMainWindow";
    public const string ArKit_Device_Need_Down = "ArKit_Device_Need_Down";
    public const string ArKit_Device_Need_Up = "ArKit_Device_Need_Up";
    public const string ArKit_Device_Good = "ArKit_Device_Good";
    public const string ArKit_Gyro_Stop = "ArKit_Gyro_Stop";
    public const string Arkit_Target_Placed = "Arkit_Target_Placed";
    public const string Arkit_RecoProgress_value_Update = "Arkit_RecoProgress_value_Update";
    public const string Arkit_RecoProgress_Start = "Arkit_RecoProgress_Start";
    public const string ARBaike_LogResult = "ARBaike_LogResult";
    public const string ARBaike_GuideStep1 = "ARBaike_GuideStep1";
    public const string ARBaike_GuideStep2 = "ARBaike_GuideStep2";

    public const string ArAndroid_Reset = "ArAndroid_Reset";
    public const string Arkit_PAUSED = "Arkit_PAUSED";

    public const string ARBaike_Course_Buy_Success = "ARBaike_Course_Buy_Success";

    #endregion


    #endregion


}

﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using System;

public static class GeneralUtils {

	#region TryParse and Force
	public static bool TryParseBool(string strVal, out bool boolVal)
	{
		if (bool.TryParse(strVal, out boolVal))
		{
			return true;
		}
		string a = strVal.ToLowerInvariant().Trim();
		if (a == "off" || a == "0" || a == "false")
		{
			boolVal = false;
			return true;
		}
		if (a == "on" || a == "1" || a == "true")
		{
			boolVal = true;
			return true;
		}
		boolVal = false;
		return false;
	}
	
	public static bool ForceBool(string strVal)
	{
		string a = strVal.ToLowerInvariant().Trim();
		return a == "on" || a == "1" || a == "true";
	}
	
	public static bool TryParseInt(string str, out int val)
	{
		return int.TryParse(str, NumberStyles.Any, null, out val);
	}
	
	public static int ForceInt(string str)
	{
		int result = 0;
		GeneralUtils.TryParseInt(str, out result);
		return result;
	}

	public static uint ForceUInt(string str)
	{
		uint result = 0;
		uint.TryParse (str, out result);
		return result;
	}
	
	public static bool TryParseLong(string str, out long val)
	{
		return long.TryParse(str, NumberStyles.Any, null, out val);
	}
	
	public static long ForceLong(string str)
	{
		long result = 0L;
		GeneralUtils.TryParseLong(str, out result);
		return result;
	}
	
	public static bool TryParseULong(string str, out ulong val)
	{
		return ulong.TryParse(str, NumberStyles.Any, null, out val);
	}
	
	public static ulong ForceULong(string str)
	{
		ulong result = 0uL;
		GeneralUtils.TryParseULong(str, out result);
		return result;
	}
	
	public static bool TryParseFloat(string str, out float val)
	{
		return float.TryParse(str, NumberStyles.Any, null, out val);
	}
	
	public static float ForceFloat(string str)
	{
		float result = 0f;
		GeneralUtils.TryParseFloat(str, out result);
		return result;
	}
	#endregion

	public static string SafeFormat(string format, params object[] args)
	{
		string result;
		if (args.Length == 0)
		{
			result = format;
		}
		else
		{
			result = string.Format(format, args);
		}
		return result;
	}

	/// <summary>
	/// 对象是否活动
	/// </summary>
	public static bool IsObjectAlive(object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (!(obj is UnityEngine.Object))
		{
			return true;
		}
		UnityEngine.Object exists = (UnityEngine.Object)obj;
		return exists;
	}

	/// <summary>
	/// 回调是否可用
	/// </summary>
	public static bool IsCallbackValid(Delegate callback)
	{
		bool flag = true;
		if (callback == null)
		{
			flag = false;
		}
		else if (!callback.Method.IsStatic)
		{
			object target = callback.Target;
			flag = GeneralUtils.IsObjectAlive(target);
			if (!flag)
			{
				Debug.LogError(string.Format("Target for callback {0} is null.", callback.Method.Name));
			}
		}
		return flag;
	}

    public static float SizeBToMB (long size)
    {
        return size / (float)(1024 * 1024);
    }

    public static string SizeFormat(float size)
    {
        return string.Format("{0:F}", size);
    }
}

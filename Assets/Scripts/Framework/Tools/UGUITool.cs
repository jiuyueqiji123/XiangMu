﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UGUITool
{

    public static Vector3 WorldPointToUGUIPoint(Canvas canvas, Vector3 worldPosition)
    {
        Vector3 v_v3 = Camera.main.WorldToScreenPoint(worldPosition);
        Vector3 v_ui = canvas.worldCamera.ScreenToWorldPoint(v_v3);
        Vector3 v_new = new Vector3(v_ui.x, v_ui.y, canvas.GetComponent<RectTransform>().anchoredPosition3D.z);
        return v_new;
    }

    public static Vector3 ScreenPointToUGUIPoint(Canvas canvas, Vector2 screenPosition)
    {
        Vector3 v_ui = canvas.worldCamera.ScreenToWorldPoint(screenPosition);
        Vector3 v_new = new Vector3(v_ui.x, v_ui.y, canvas.GetComponent<RectTransform>().anchoredPosition3D.z);
        return v_new;
    }
    


}

﻿using UnityEngine;
using System.Collections;

public class DownloadTask {

	public delegate void OnProgressDelegate(long totalSize, long nowSize);

	public delegate void OnSuccessDelegate();

	public delegate void OnErrorDelegate(string errorString);

	public string URL;

	public string SavePath;

	public long Size;

	public long DownloadedSize;

	public OnProgressDelegate OnProgress;

	public OnSuccessDelegate OnSuccess;

	public OnErrorDelegate OnError;

	public void Progress(long totalSize, long nowSize) {
		if (this.OnProgress != null) {
			this.OnProgress (totalSize, nowSize);
		}
	}

	public void Success() {
		if (this.OnSuccess != null) {
			this.OnSuccess ();
		}
	}

	public void Error(string error) {
		if (this.OnError != null) {
			this.OnError (error);
		}
	}
}

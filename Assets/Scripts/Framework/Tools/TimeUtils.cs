﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TimeUtils {

    private static long s_unixTime;

    private static float s_startTime;

    /// <summary>
    /// DataTime类型的时间
    /// </summary>
    /// <value>The current time.</value>
    public static DateTime CurTime
    {
        get
        {
            return ToDateTime(CurUnixTime);
        }
    }

    public static long CurUnixTime
    {
        set
        {
            s_unixTime = value;
            s_startTime = UnityEngine.Time.realtimeSinceStartup;
        }
        get
        {
            return s_unixTime + (long)(UnityEngine.Time.realtimeSinceStartup - s_startTime);
        }
    }

    public static long ToUtcSeconds(DateTime dateTime)
	{
		DateTime dateTime2 = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
		if (dateTime.CompareTo(dateTime2) < 0)
		{
			return 0u;
		}
		return (long)(dateTime - dateTime2).TotalSeconds;
	}

    public static DateTime ToDateTime(long unixTimeStamp)
    {
        DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
        DateTime dt = startTime.AddSeconds(unixTimeStamp);
        return dt;
    }

    public static int ToDayAgo(long time)
    {
        DateTime dt = ToDateTime(time);
        return (CurTime - dt).Days;
    }

    /// <summary>
    /// 是不是晚上
    /// </summary>
    /// <returns></returns>
    public static bool IsNight()
    {
        TimeSpan start = DateTime.Parse("19:00:00").TimeOfDay;
        TimeSpan end = DateTime.Parse("07:00:00").TimeOfDay;
        TimeSpan nowT = TimeUtils.CurTime.TimeOfDay;
        TimeSpan endT = DateTime.Parse("23:59:59").TimeOfDay;
        TimeSpan startT = DateTime.Parse("00:00:00").TimeOfDay;
        if ((nowT > start && nowT <= endT) || (nowT >= startT && nowT < end))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 是不是同一天
    /// </summary>
    /// <param name="oldT"></param>
    /// <param name="newT"></param>
    /// <returns></returns>
    public static bool IsSameDay(DateTime oldT, DateTime newT)
    {
        //如果是同一天，表示已经领取
        if (oldT.ToShortDateString() == newT.ToShortDateString())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using System;

public class AtlasLoader : MonoBehaviour {

	void Awake () {
		// 注册事件
		SpriteAtlasManager.atlasRequested += OnAtlasRequested;
	}

	private void OnAtlasRequested(string tag, Action<SpriteAtlas> action) {
		// 加载纹理集
		// 当收到回调后必须立刻用纹理集填充
		Debug.LogError(tag);
		SpriteAtlas atlas = Resources.Load<SpriteAtlas>(tag);
		action(atlas);
	}
}

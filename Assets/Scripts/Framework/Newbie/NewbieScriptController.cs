﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class NewbieScriptController : MonoBehaviour {

	public delegate void NewbieScriptControlDelegate();

	public delegate NewbieScriptBase AddScriptDelegate(NewbieScriptType type, GameObject gameObject);

	public event NewbieScriptController.NewbieScriptControlDelegate CompleteEvent;

	public event NewbieScriptController.NewbieScriptControlDelegate SaveEvent;

	public NewbieScriptController.AddScriptDelegate AddScriptDel;

	private NewBieInfo curNewbieInfo;

	public static string FormGuideMaskPath = "ui/ui_newbie/NewbieForm";

	private List<NewBieScript> mConfList;

	private uint mSavePoint;

	private int mCurrentScriptIndex;

	private NewbieScriptBase mCurrentScript;

	private float m_timeOutTimer;

	public uint CurrentMainLineId
	{
		get;
		private set;
	}

	public int StartIndex
	{
		get;
		private set;
	}

	public static HUIFormScript FormGuideMask
	{
		get;
		private set;
	}

	public NewbieScriptBase CurrentScript
	{
		get
		{
			return this.mCurrentScript;
		}
	}

	public static void OpenGuideForm()
	{
		if (NewbieScriptController.FormGuideMask == null)
		{
			NewbieScriptController.FormGuideMask = HUIManager.Instance.OpenForm(NewbieScriptController.FormGuideMaskPath, true, true);
		}
	}

	public static void CloseGuideForm()
	{
		if (NewbieScriptController.FormGuideMask != null)
		{
			HUIManager.Instance.CloseForm(NewbieScriptController.FormGuideMask);
			NewbieScriptController.FormGuideMask = null;
		}
	}

	public void SetData(uint id, int startIndex)
	{
		this.CurrentMainLineId = id;
		this.StartIndex = startIndex;
		this.curNewbieInfo = NewbieDataManager.Instance.GetNewbieInfoById(this.CurrentMainLineId);
	}

	void Start() {
		NewbieScriptController.OpenGuideForm ();
		this.mConfList = NewbieDataManager.Instance.GetScriptList(this.CurrentMainLineId);
		if (this.mConfList == null) {
			CompleteAll ();
		} else {
			NewBieInfo info = NewbieDataManager.Instance.GetNewbieInfoById (this.CurrentMainLineId);
			if (info != null) {
				this.mSavePoint = info.SavePoint;
				if (this.StartIndex > 0)
				{
					this.SetCurrentScriptIndex(this.StartIndex - 1);
				}
				else
				{
					this.SetCurrentScriptIndex(0);
				}
				this.CheckNext();
			}
		}
	}

	void Update() {
		if (this.m_timeOutTimer > 0f)
		{
			this.m_timeOutTimer -= Time.deltaTime;
			if (this.m_timeOutTimer <= 0f)
			{
				this.m_timeOutTimer = 0f;
				NewbieManager.Instance.ForceCompleteNewbieGuide();
			}
		}
	}

	public bool CheckSavePoint()
	{
		return 0 < this.mSavePoint && this.mCurrentScriptIndex + 1 >= this.mSavePoint;
	}

	private void CheckNext()
	{
		if (this.mCurrentScriptIndex < this.mConfList.Count)
		{
			NewBieScript script = this.mConfList[this.mCurrentScriptIndex];
			this.mCurrentScript = this.AddScript((NewbieScriptType)script.Type);
			if (null != this.mCurrentScript)
			{
				this.mCurrentScript.SetData(script);
				this.mCurrentScript.CompleteEvent += new NewbieScriptBase.NewbieScriptBaseDelegate(this.ScriptCompleteHandler);
				this.mCurrentScript.CompleteAllEvent += new NewbieScriptBase.NewbieScriptBaseDelegate(this.CompleteAll);
			}
			else
			{
				this.CompleteAll();
			}
		}
		else
		{
			this.CompleteAll();
		}
		if (this.mCurrentScript != null && this.mCurrentScript.IsTimeOutSkip())
		{
			this.m_timeOutTimer = 30f;
		}
	}

	private void CompleteAll()
	{
		NewbieScriptController.CloseGuideForm();
		this.curNewbieInfo = null;
		this.CurrentMainLineId = 0u;
		if (this.CompleteEvent != null)
		{
			this.CompleteEvent();
		}
	}

	private void ScriptCompleteHandler()
	{
		this.ScriptCheckSaveHandler();
		this.DestroyCurrentScript();
		this.SetCurrentScriptIndex(this.mCurrentScriptIndex + 1);
		this.CheckNext();
	}

	private void ScriptCheckSaveHandler()
	{
		this.CheckSave();
	}

	private void CheckSave()
	{
		if (this.SaveEvent != null)
		{
			this.SaveEvent();
		}
	}

	private void DestroyCurrentScript()
	{
		if (null != this.mCurrentScript)
		{
			this.mCurrentScript.CompleteEvent -= new NewbieScriptBase.NewbieScriptBaseDelegate(this.ScriptCompleteHandler);
			this.mCurrentScript.CompleteAllEvent -= new NewbieScriptBase.NewbieScriptBaseDelegate(this.CompleteAll);
			UnityEngine.Object.Destroy(this.mCurrentScript);
			this.mCurrentScript = null;
		}
	}

	private void SetCurrentScriptIndex(int value)
	{
		this.mCurrentScriptIndex = value;
	}

	private NewbieScriptBase AddScript(NewbieScriptType type)
	{
		if (this.AddScriptDel != null)
		{
			return this.AddScriptDel(type, base.gameObject);
		}
		return null;
	}

	public void Stop()
	{
		if (this.mCurrentScript != null)
		{
			this.mCurrentScript.Stop();
		}
		this.DestroyCurrentScript();
		NewbieScriptController.CloseGuideForm();
	}
}

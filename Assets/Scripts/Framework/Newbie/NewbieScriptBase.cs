﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using UnityEngine.UI;

public class NewbieScriptBase : MonoBehaviour {

	private enum ECompleteType
	{
		ClickButton,
		ClickAnyWhere,
		WaitOneWhile
	}

	public enum EOpacityLadder
	{
		Standard,
		Transparent,
		Darker,
		Count
	}

	private static Vector3 s_FlipNone = Vector3.one;

	private static Vector3 s_FlipX = new Vector3(-1f, 1f, 1f);

	private static Vector3 s_FlipY = new Vector3(1f, -1f, 1f);

	private static Vector3 s_FlipXY = new Vector3(-1f, -1f, 1f);

	public delegate void NewbieScriptBaseDelegate();

	public event NewbieScriptBase.NewbieScriptBaseDelegate CompleteEvent;

	public event NewbieScriptBase.NewbieScriptBaseDelegate CompleteAllEvent;

	protected static List<GameObject> ms_originalGo = new List<GameObject>();

	private static HUIFormScript ms_originalForm = null;

	protected static List<GameObject> ms_highlitGo = new List<GameObject>();

	protected static List<GameObject> ms_highlighter = new List<GameObject>();

	protected static List<GameObject> ms_guideTextList = new List<GameObject>();

	private NewbieScriptBase.ECompleteType m_completeType;

	public bool isInitialize
	{
		get;
		protected set;
	}

	public bool isGuiding
	{
		get;
		protected set;
	}

	public NewBieScript currentConf
	{
		get;
		protected set;
	}

	// Use this for initialization
	void Start () {
		Initialize ();
	}

	protected virtual void Initialize()
	{
		if (!this.isInitialize)
		{
			this.TryToPauseGame();
			this.TryToPlayerSound();
			this.AddDelegate();
			this.isInitialize = true;
			this.isGuiding = true;
		}
	}

	
	// Update is called once per frame
	void Update () {
		HUIFormScript formGuideMask = NewbieScriptController.FormGuideMask;
		if (NewbieScriptBase.ms_originalForm != null && formGuideMask != null)
		{
			int count = NewbieScriptBase.ms_highlitGo.Count;
			for (int i = 0; i < count; i++)
			{
				GameObject gameObject = NewbieScriptBase.ms_highlitGo[i];
				GameObject gameObject2 = NewbieScriptBase.ms_originalGo[i];
				if (gameObject != null && gameObject2 != null)
				{
					gameObject.SetActive(gameObject2.activeSelf);
					Image component = gameObject2.GetComponent<Image>();
					Image component2 = gameObject.GetComponent<Image>();
					if (component != null && component2 != null)
					{
						component2.color = component.color;
					}
					RectTransform rectTransform = gameObject.transform as RectTransform;
					RectTransform rectTransform2 = gameObject2.transform as RectTransform;
					rectTransform.localScale = rectTransform2.localScale;
					rectTransform.pivot = rectTransform2.pivot;
					rectTransform.sizeDelta = rectTransform2.sizeDelta;
					LayoutElement component3 = rectTransform2.GetComponent<LayoutElement>();
					if (component3 != null && rectTransform.sizeDelta == Vector2.zero)
					{
						rectTransform.sizeDelta = new Vector2(component3.preferredWidth, component3.preferredHeight);
					}
					rectTransform.position = rectTransform2.position;
					Vector2 screenPoint = HUIUtility.WorldToScreenPoint(NewbieScriptBase.ms_originalForm.GetCamera(), rectTransform2.position);
					Vector3 vector = HUIUtility.ScreenToWorldPoint(NewbieScriptController.FormGuideMask.GetCamera(), screenPoint, rectTransform.position.z);
				}
			}
		}
	}

	protected virtual void Clear() {
		this.isGuiding = false;
		this.TryToResumeGame();
		this.ClearDelegate();
		this.ClearHighLightGameObject();
	}

	public void SetData(NewBieScript conf) {
		this.currentConf = conf;
	}

	public void Stop()
	{
		this.Clear();
	}

	private void TryToPauseGame()
	{
		if (this.currentConf.PauseGame == 1)
		{
			//暂停游戏
		}
	}

	private void TryToResumeGame()
	{
		if (this.currentConf.PauseGame == 1)
		{
			//恢复游戏
		}
	}

	private void TryToPlayerSound()
	{
		if (!string.IsNullOrEmpty(this.currentConf.SoundFileName))
		{
			//播放声音
			AudioManager.Instance.PlaySound(this.currentConf.SoundFileName);
		}
	}

	private bool DoesShowArrow()
	{
		return this.currentConf.NotShowArrow == 0;
	}
		
	private void AddDelegate()
	{
		if (this.IsDelegateClickEvent())
		{
			this.TryToAddClickEvent();
		}
		if (this.IsDelegateSpecialTip())
		{
			this.AddSpecialTip();
		}
		if (!this.IsShowGuideMask())
		{
			this.ShowGuideMask(false);
		}
	}

	private void ClearDelegate()
	{
		if (this.IsDelegateClickEvent())
		{
			this.TryToRemoveClickEvent();
		}
		if (this.IsDelegateSpecialTip())
		{
			this.RemoveSpecialTip();
		}
		if (!this.IsShowGuideMask())
		{
			this.ShowGuideMask(true);
		}
	}

	protected virtual bool IsDelegateClickEvent()
	{
		return false;
	}

	protected virtual bool IsDelegateSpecialTip()
	{
		return true;
	}

	protected virtual bool IsShowGuideMask()
	{
		return true;
	}

	protected virtual bool IsDelegateAutoDispatchCompleteEvent()
	{
		return true;
	}

	public virtual bool IsTimeOutSkip()
	{
		return true;
	}

	private void TryToAddClickEvent()
	{
		List<GameObject>.Enumerator enumerator = NewbieScriptBase.ms_highlitGo.GetEnumerator();
		while (enumerator.MoveNext())
		{
			GameObject current = enumerator.Current;
			if (current != null)
			{
				HUIEventScript component = current.GetComponent<HUIEventScript>();
				if (component != null)
				{
					component.onClick += new HUIEventScript.OnUIEventHandler(this.ClickHandler);
				}
				HUIMiniEventScript cUIMiniEventScript = current.GetComponent<HUIMiniEventScript>();
				if (cUIMiniEventScript != null)
				{
					cUIMiniEventScript.onClick += new HUIMiniEventScript.OnUIEventHandler(this.ClickHandler);
				}
				if (component == null && cUIMiniEventScript == null)
				{
					cUIMiniEventScript = current.AddComponent<HUIMiniEventScript>();
					cUIMiniEventScript.onClick += new HUIMiniEventScript.OnUIEventHandler(this.ClickHandler);
				}
			}
		}
		if (this.m_completeType == NewbieScriptBase.ECompleteType.ClickAnyWhere)
		{
			HUIEventManager.Instance.AddUIEventListener(enUIEventID.UI_Form_ClickAnyWhere, new HUIEventManager.OnUIEventHandler(this.AnyWhereClick));
		}
	}

	private void AnyWhereClick(HUIEvent inUiEvent)
	{
		this.ClickHandler(inUiEvent);
	}

	protected virtual void ClickHandler(HUIEvent uiEvent)
	{
		this.CompleteHandler();
	}

	private void TryToRemoveClickEvent()
	{
		int num = 0;
		List<GameObject>.Enumerator enumerator = NewbieScriptBase.ms_highlitGo.GetEnumerator();
		while (enumerator.MoveNext())
		{
			GameObject current = enumerator.Current;
			if (current != null)
			{
				HUIEventScript component = current.GetComponent<HUIEventScript>();
				if (component != null)
				{
					component.onClick -= new HUIEventScript.OnUIEventHandler(this.ClickHandler);
				}
				HUIMiniEventScript component2 = current.GetComponent<HUIMiniEventScript>();
				if (component2 != null)
				{
					component2.onClick -= new HUIMiniEventScript.OnUIEventHandler(this.ClickHandler);
				}
				num++;
			}
		}
		if (this.m_completeType == NewbieScriptBase.ECompleteType.ClickAnyWhere)
		{
			HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.UI_Form_ClickAnyWhere, new HUIEventManager.OnUIEventHandler(this.AnyWhereClick));
		}
	}

	private void AddSpecialTip()
	{
		//预留特别提示接口
		if (this.currentConf.SpecialTip != null)
		{
			if (NewbieScriptController.FormGuideMask == null)
			{
				NewbieScriptController.OpenGuideForm();
			}

		}
	}

	private void RemoveSpecialTip() {

	}

	private void ShowGuideMask(bool bShow)
	{
		if (bShow)
		{
			NewbieScriptController.OpenGuideForm();
		}
		else
		{
			NewbieScriptController.CloseGuideForm();
		}
	}

	#region HightLight
	protected void AddHighLightGameObject(GameObject baseGo, bool isUI, HUIFormScript inOriginalForm, bool cloneEvent = true)
	{
		this.AddHighlightInternal(baseGo, inOriginalForm, cloneEvent, true);
	}

	private void AddHighlightInternal(GameObject baseGo, HUIFormScript inOriginalForm, bool cloneEvent, bool bShowFinger)
	{
		this.PreHighlight ();
		if (baseGo != null)
		{
			NewbieScriptBase.ms_originalGo.Add(baseGo);
		}
		NewbieScriptBase.ms_originalForm = inOriginalForm;
		this.OpenGuideForm();
		if (NewbieScriptController.FormGuideMask == null)
		{
			NewbieScriptController.OpenGuideForm();
		}
		List<GameObject>.Enumerator enumerator = NewbieScriptBase.ms_originalGo.GetEnumerator();
		int num = 0;
		while (enumerator.MoveNext())
		{
			GameObject current = enumerator.Current;
			if (current != null) {
				GameObject gameObject = UnityEngine.Object.Instantiate(current) as GameObject;
				if (gameObject != null) {
					RectTransform rectTransform = gameObject.transform as RectTransform;
					rectTransform.SetParent(NewbieScriptController.FormGuideMask.transform);
					rectTransform.SetSiblingIndex(1);
					rectTransform.localScale = current.transform.localScale;
					RectTransform rectTransform2 = current.transform as RectTransform;
					rectTransform.pivot = rectTransform2.pivot;
					rectTransform.sizeDelta = rectTransform2.sizeDelta;
					LayoutElement component = current.GetComponent<LayoutElement>();
					if (component != null && rectTransform.sizeDelta == Vector2.zero)
					{
						rectTransform.sizeDelta = new Vector2(component.preferredWidth, component.preferredHeight);
					}
					rectTransform.position = current.transform.position;
					Vector2 screenPoint = HUIUtility.WorldToScreenPoint(inOriginalForm.GetCamera(), current.transform.position);
					Vector3 worldPosition = HUIUtility.ScreenToWorldPoint(NewbieScriptController.FormGuideMask.GetCamera(), screenPoint, rectTransform.position.z);
					NewbieScriptController.FormGuideMask.InitializeWidgetPosition(gameObject, worldPosition);
					gameObject.SetActive(false);
					if (cloneEvent)
					{
						HUIEventScript component2 = current.GetComponent<HUIEventScript>();
						HUIEventScript component3 = gameObject.GetComponent<HUIEventScript>();
						if (component2 && component3)
						{
							component3.m_onDownEventParams = component2.m_onDownEventParams;
							component3.m_onUpEventParams = component2.m_onUpEventParams;
							component3.m_onClickEventParams = component2.m_onClickEventParams;
							component3.m_onHoldStartEventParams = component2.m_onHoldStartEventParams;
							component3.m_onHoldEventParams = component2.m_onHoldEventParams;
							component3.m_onHoldEndEventParams = component2.m_onHoldEndEventParams;
							component3.m_onDragStartEventParams = component2.m_onDragStartEventParams;
							component3.m_onDragEventParams = component2.m_onDragEventParams;
							component3.m_onDragEndEventParams = component2.m_onDragEndEventParams;
							component3.m_onDropEventParams = component2.m_onDropEventParams;
							component3.m_closeFormWhenClicked = component2.m_closeFormWhenClicked;
							component3.m_belongedFormScript = component2.m_belongedFormScript;
						}
						HUIMiniEventScript component4 = current.GetComponent<HUIMiniEventScript>();
						HUIMiniEventScript component5 = gameObject.GetComponent<HUIMiniEventScript>();
						if (component4 && component5)
						{
							component5.m_onDownEventParams = component4.m_onDownEventParams;
							component5.m_onUpEventParams = component4.m_onUpEventParams;
							component5.m_onClickEventParams = component4.m_onClickEventParams;
							component5.m_closeFormWhenClicked = component4.m_closeFormWhenClicked;
							component5.m_belongedFormScript = component4.m_belongedFormScript;
						}
					}
					else
					{
						HUIEventScript component6 = gameObject.GetComponent<HUIEventScript>();
						if (component6)
						{
							component6.enabled = false;
						}
						HUIMiniEventScript component7 = gameObject.GetComponent<HUIMiniEventScript>();
						if (component7)
						{
							component7.enabled = false;
						}
					}
					gameObject.SetActive(true);
					HUIAnimatorScript component8 = current.GetComponent<HUIAnimatorScript>();
					if (component8 != null)
					{
						HUICommonSystem.PlayAnimator(gameObject, component8.m_currentAnimatorStateName);
					}
					NewbieScriptBase.ms_highlitGo.Add(gameObject);
					if (bShowFinger)
					{
						GameObject gameObject2 = ResourceManager.Instance.GetResource("ui/ui_newbie/Highlighter", typeof(GameObject), enResourceType.UIPrefab, false, false).m_content as GameObject;
						if (gameObject2 != null)
						{
							GameObject gameObject3 = UnityEngine.Object.Instantiate(gameObject2) as GameObject;
							if (gameObject3 != null)
							{
								gameObject3.transform.SetParent(gameObject.transform);
								Transform transform = gameObject3.transform;
								switch (this.currentConf.FlipType)
								{
								case 0:
									transform.localScale = NewbieScriptBase.s_FlipNone;
									break;
								case 1:
									transform.localScale = NewbieScriptBase.s_FlipX;
									break;
								case 2:
									transform.localScale = NewbieScriptBase.s_FlipY;
									break;
								case 3:
									transform.localScale = NewbieScriptBase.s_FlipXY;
									break;
								}
								gameObject3.transform.position = gameObject.transform.position;
								(gameObject3.transform as RectTransform).anchoredPosition = new Vector2((float)this.currentConf.OffsetHighLightX, (float)this.currentConf.OffsetHighLightY);
								if (!this.DoesShowArrow())
								{
									gameObject3.transform.Find("Panel/ImageFinger").gameObject.SetActive(false);
								}
								NewbieScriptBase.ms_highlighter.Add(gameObject3);
							}
						}
					}
					num++;
				}
			}
		}
	}

	private void OpenGuideForm()
	{
		this.SetBackgroundTransparency((NewbieScriptBase.EOpacityLadder)this.currentConf.ShowTransparency);
	}

	protected void SetBackgroundTransparency(NewbieScriptBase.EOpacityLadder inLadder)
	{
		if (NewbieScriptController.FormGuideMask != null && NewbieScriptController.FormGuideMask.transform != null)
		{
			float a = 0f;
			switch (inLadder)
			{
			case EOpacityLadder.Standard:
				a = 0.4f;
				break;
			case EOpacityLadder.Transparent:
				a = 0f;
				break;
			case EOpacityLadder.Darker:
				a = 0.7f;
				break;
			}
			Transform transform = NewbieScriptController.FormGuideMask.transform.Find("Bg");
			if (transform != null)
			{
				GameObject gameObject = transform.gameObject;
				Image component = gameObject.GetComponent<Image>();
				if (component != null)
				{
					component.color = new Color(0f, 0f, 0f, a);
				}
			}
		}
	}

	protected void AddHighLightAreaClickAnyWhere(GameObject baseGo, HUIFormScript inOriginalForm)
	{
		this.AddHighlightInternal(baseGo, inOriginalForm, false, false);
		List<GameObject>.Enumerator enumerator = NewbieScriptBase.ms_highlitGo.GetEnumerator();
		while (enumerator.MoveNext())
		{
			GameObject current = enumerator.Current;
			if (current != null)
			{
				RectTransform rectTransform = current.transform as RectTransform;
				GameObject gameObject = ResourceManager.Instance.GetResource("UGUI/Form/System/Dialog/HighlightAreaMask.prefab", typeof(GameObject), enResourceType.UIPrefab, false, false).m_content as GameObject;
				if (gameObject != null)
				{
					GameObject gameObject2 = UnityEngine.Object.Instantiate(gameObject) as GameObject;
					if (gameObject2 != null)
					{
						RectTransform rectTransform2 = gameObject2.transform as RectTransform;
						rectTransform2.SetParent(rectTransform);
						rectTransform2.SetAsLastSibling();
						rectTransform2.localScale = NewbieScriptBase.s_FlipNone;
						Transform transform = baseGo.transform;
						if (transform.parent.name == "ScrollRect")
						{
							Rect rect = (transform.parent.transform as RectTransform).rect;
							Vector2 sizeDelta = new Vector2(rect.width, rect.height);
							rectTransform2.sizeDelta = sizeDelta;
						}
						else
						{
							rectTransform2.sizeDelta = rectTransform.sizeDelta;
						}
						rectTransform2.position = rectTransform.position;
						rectTransform2.anchoredPosition = Vector2.zero;
					}
				}
			}
		}
		this.m_completeType = NewbieScriptBase.ECompleteType.ClickAnyWhere;
	}

	protected void AddHighLightAnyWhere()
	{
		this.PreHighlight();
		this.m_completeType = NewbieScriptBase.ECompleteType.ClickAnyWhere;
		this.OpenGuideForm();
	}

	protected void AddHighlightWaiting()
	{
		this.PreHighlight();
		this.m_completeType = NewbieScriptBase.ECompleteType.WaitOneWhile;
		this.OpenGuideForm();
	}

	private void PreHighlight()
	{
		this.m_completeType = NewbieScriptBase.ECompleteType.ClickButton;
		NewbieScriptBase.ms_originalGo.Clear();
		NewbieScriptBase.ms_originalForm = null;
		this.ClearHighlitObjs();
	}

	private void ClearHighlitObjs()
	{
		List<GameObject> list = new List<GameObject>();
		list.AddRange(NewbieScriptBase.ms_highlitGo);
		list.AddRange(NewbieScriptBase.ms_highlighter);
		List<GameObject>.Enumerator enumerator = list.GetEnumerator();
		while (enumerator.MoveNext())
		{
			GameObject current = enumerator.Current;
			if (current != null)
			{
				current.transform.SetParent(null);
				UnityEngine.Object.Destroy(current);
			}
		}
		NewbieScriptBase.ms_highlitGo.Clear();
		NewbieScriptBase.ms_highlighter.Clear();
	}

	private void ClearHighlightInternal()
	{
		this.SetBackgroundTransparency(NewbieScriptBase.EOpacityLadder.Transparent);
		this.ClearHighlitObjs();
		NewbieScriptBase.ms_originalGo.Clear();
		NewbieScriptBase.ms_originalForm = null;
		this.m_completeType = NewbieScriptBase.ECompleteType.ClickButton;
	}

	private void ClearHighLightGameObject() {
		this.ClearHighlightInternal();
	}

	#endregion

	#region handler
	protected virtual void CompleteHandler()
	{
		this.Clear();
		if (this.IsDelegateAutoDispatchCompleteEvent())
		{
			this.DispatchCompleteEvent();
		}
	}

	protected virtual void CompleteAllHandler()
	{
		this.Clear();
		if (this.IsDelegateAutoDispatchCompleteEvent())
		{
			this.DispatchCompleteAllEvent();
		}
	}

	protected void DispatchCompleteEvent()
	{
		if (this.CompleteEvent != null)
		{
			this.CompleteEvent();
		}
	}

	protected void DispatchCompleteAllEvent()
	{
		if (this.CompleteAllEvent != null)
		{
			this.CompleteAllEvent();
		}
	}
	#endregion

}

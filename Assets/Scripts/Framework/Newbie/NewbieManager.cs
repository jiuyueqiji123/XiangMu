﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class NewbieManager : MonoSingleton<NewbieManager> {

	private uint currentNewbieId;

	private NewbieScriptController currentScriptController;

	public static NewbieScriptController.AddScriptDelegate AddScriptDelegate;

	private Dictionary<uint, bool> mCompleteCacheDic;

	public bool NewbieGuideEnable;

	private bool mIsInit = false;

	protected override void Awake ()
	{
		base.Awake ();
		NewbieManager.AddScriptDelegate = new NewbieScriptController.AddScriptDelegate(NewbieScriptFactory.AddScript);
	}

	protected override void Init ()
	{
		base.Init ();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
	}

	public void SetData() {
		this.mCompleteCacheDic = new Dictionary<uint, bool> ();
		List<uint> mainLineIDList = NewbieDataManager.Instance.GetNewbieIDList();
		int count = mainLineIDList.Count;
		for (int i = 0; i < count; i++)
		{
			this.mCompleteCacheDic.Add(mainLineIDList[i], false);
		}

		this.NewbieGuideEnable = true;
		this.mIsInit = true;
	}

	public void StartNewbieGuide(NewBieInfo info, int startIndex) {
		this.currentNewbieId = info.id;
		this.currentScriptController = base.gameObject.AddComponent<NewbieScriptController> ();
		this.currentScriptController.CompleteEvent += new NewbieScriptController.NewbieScriptControlDelegate (this.NewbieGuideCompleteHandler);
		this.currentScriptController.SaveEvent += new NewbieScriptController.NewbieScriptControlDelegate (this.NewbieGuideSaveHandler);
		this.currentScriptController.SetData (info.id, startIndex);
		this.currentScriptController.AddScriptDel = NewbieManager.AddScriptDelegate;
	}

	private bool CheckStart(NewbieStartTimeType type, NewBieInfo info) {
		if (this.CanStart (info)) {
			int startIndex = this.GetStartIndexByTriggerTime (type, info);
			this.StartNewbieGuide (info, startIndex);
			return true;
		}
		return false;
	}

	private bool CanStart(NewBieInfo info) {
		if (IsNewbieComplete (info.id)) {
			return false;
		}
		//等级检测
		return true;
	}

	public bool CheckStartTime(NewbieStartTimeType type) {
		if (!this.NewbieGuideEnable) {
			return false;
		}
		if (this.currentNewbieId == 0u)
		{
			List<NewBieInfo> newbieInfoListByTriggerTimeType = NewbieDataManager.Instance.GetNewbieInfoListByStartTimeType(type);
			int count = newbieInfoListByTriggerTimeType.Count;
			for (int i = 0; i < count; i++)
			{
				NewBieInfo conf = newbieInfoListByTriggerTimeType[i];
				if (this.CheckStart(type, conf))
				{
					return true;
				}
			}
		}
		return false;
	}

	private int GetStartIndexByTriggerTime(NewbieStartTimeType type, NewBieInfo conf)
	{
		return (int)conf.StartIndex;
	}

	private bool IsNewbieComplete(uint id) {
		bool flag;
		return this.mCompleteCacheDic.TryGetValue (id, out flag) && flag;
	}

	public void ForceCompleteNewbieGuide() {

	}

	private void NewbieGuideCompleteHandler() {

	}

	private void NewbieGuideSaveHandler() {

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class NewbieDataManager : Singleton<NewbieDataManager> {

	Dictionary<uint, NewBieInfo> newbieInfoDict;
	private Dictionary<uint, List<NewBieScript>> mScriptDict;

	private List<NewBieInfo> mCacheInfoSourceList;

	private List<NewBieInfo> mCacheInfoTargetList;

	public override void Init ()
	{
		base.Init ();
	}

	public void LoadNewbieData() {
		newbieInfoDict = TableProtoLoader.NewBieInfoDict;

		mScriptDict = new Dictionary<uint, List<NewBieScript>> ();
		foreach (NewBieScript script in TableProtoLoader.NewBieScriptDict.Values) {
			List<NewBieScript> list;
			if (!this.mScriptDict.TryGetValue (script.NewbieId, out list)) {
				list = new List<NewBieScript> ();
				this.mScriptDict.Add (script.NewbieId, list);
			}
			list.Add (script);
		}

		this.mCacheInfoSourceList = new List<NewBieInfo> ();
		this.mCacheInfoTargetList = new List<NewBieInfo> ();
	}

	public NewBieInfo GetNewbieInfoById(uint id) {
		NewBieInfo info;
		if (newbieInfoDict.TryGetValue (id, out info))
			return info;
		return null;
	}

	public List<uint> GetNewbieIDList()
	{
		List<uint> list = new List<uint>();
		foreach (uint id in this.newbieInfoDict.Keys)
		{
			list.Add(id);
		}
		return list;
	}

	public List<NewBieScript> GetScriptList(uint infoId) {
		List<NewBieScript> list;
		if (this.mScriptDict.TryGetValue (infoId, out list)) {
			return list;
		}
		return null;
	}

	public List<NewBieInfo> GetNewbieInfoListByStartTimeType(NewbieStartTimeType type)
	{
		this.mCacheInfoSourceList.Clear();
		this.mCacheInfoSourceList.AddRange (this.newbieInfoDict.Values);
		this.mCacheInfoTargetList.Clear();
		int count = this.mCacheInfoSourceList.Count;
		for (int i = 0; i < count; i++)
		{
			NewBieInfo newbieInfo = this.mCacheInfoSourceList[i];
			if (type == (NewbieStartTimeType)newbieInfo.StartTimeType)
			{
				this.mCacheInfoTargetList.Add(newbieInfo);
			}
		}
		return this.mCacheInfoTargetList;
	}
}

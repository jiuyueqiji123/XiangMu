﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;

public class ClassEnumerator {

	List<Type> results = new List<Type>();

	public List<Type> Results
	{
		get
		{
			return this.results;
		}
	}

	public ClassEnumerator(Type InAttributeType, Type InInterfaceType, Assembly InAssembly, bool bIgnoreAbstract = true, bool bInInheritAttribute = false)
	{
		Type[] types = InAssembly.GetTypes();
		if (types != null)
		{
			for (int i = 0; i < types.Length; i++)
			{
				Type type = types[i];
				if ((InInterfaceType == null || InInterfaceType.IsAssignableFrom(type)) && (!bIgnoreAbstract || (bIgnoreAbstract && !type.IsAbstract)) && type.GetCustomAttributes(InAttributeType, bInInheritAttribute).Length > 0)
				{
					this.results.Add(type);
				}
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public abstract class BaseState : IState {

	public virtual string name
	{
		get
		{
			return base.GetType().Name;
		}
	}

	public virtual void OnStateEnter()
	{
		Debug.Log (name +  " Enter");
	}

	public virtual void OnStateLeave()
	{
		Debug.Log (name +  " Leave");
	}

	public virtual void OnStatePause()
	{
		Debug.Log (name +  " Pause");
	}

	public virtual void OnStateResume()
	{
		Debug.Log (name +  " Resume");
	}
}

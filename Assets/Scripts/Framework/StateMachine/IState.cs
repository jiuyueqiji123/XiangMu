﻿using UnityEngine;
using System.Collections;

public interface IState {

	string name
	{
		get;
	}

	void OnStateEnter();

	void OnStateLeave();

	void OnStatePause();

	void OnStateResume();
}

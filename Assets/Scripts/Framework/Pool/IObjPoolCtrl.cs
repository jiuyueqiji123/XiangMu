﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 类对象控制器
/// </summary>
public interface IObjPoolCtrl
{
	void Release(PooledClassObject obj);
}

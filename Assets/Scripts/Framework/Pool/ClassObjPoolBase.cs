﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ClassObjPoolBase : IObjPoolCtrl {

	protected List<object> pool = new List<object>(128);

	/// <summary>
	/// 会一直递增，对象的数量
	/// </summary>
	protected uint reqSeq;

	public int capacity
	{
		get
		{
			return this.pool.Capacity;
		}
		set
		{
			this.pool.Capacity = value;
		}
	}

	public abstract void Release(PooledClassObject obj);
}

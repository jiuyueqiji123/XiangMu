﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledClassObject {

	/// <summary>
	/// 正在使用的引用，在对象池里面会自增，唯一
	/// </summary>
	public uint usingSeq;
	/// <summary>
	/// 对象控制器
	/// </summary>
	public IObjPoolCtrl holder;

	public virtual void OnUse()
	{
	}

	public virtual void OnRelease()
	{
	}

	public void Release()
	{
		if (this.holder != null)
		{
			this.OnRelease();
			this.holder.Release(this);
		}
	}
}

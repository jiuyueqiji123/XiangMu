﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class HUIEvent {

    public enUIEventID m_eventID;

    public bool m_inUse;

    public stUIEventParams m_eventParams;

	public HUIFormScript m_srcFormScript;

	public PointerEventData m_pointerEventData;

	public GameObject m_srcWidget;

	public HUIComponent m_srcWidgetScript;

    public void Clear()
    {
        this.m_eventID = enUIEventID.None;
        this.m_inUse = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enTimerDisplayType {
	None,
	H_M_S,
	M_S,
	S,
	H_M,
	D_H_M_S,
	D_H_M,
	D
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUIUtility {

	public static void SetGameObjectLayer(GameObject gameObject, int layer)
	{
		gameObject.layer = layer;
		for (int i = 0; i < gameObject.transform.childCount; i++)
		{
			HUIUtility.SetGameObjectLayer(gameObject.transform.GetChild(i).gameObject, layer);
		}
	}

	public static void GetComponentsInChildren<T>(GameObject go, T[] components, ref int count) where T : Component
	{
		T component = go.GetComponent<T>();
		if (component != null)
		{
			components[count] = component;
			count++;
		}
		for (int i = 0; i < go.transform.childCount; i++)
		{
			HUIUtility.GetComponentsInChildren<T>(go.transform.GetChild(i).gameObject, components, ref count);
		}
	}

	public static void SetUIMiniEvent(GameObject go, enUIEventType type, enUIEventID eventId)
    {
        HUIMiniEventScript miniEvent = go.GetComponent<HUIMiniEventScript>();
        if (miniEvent == null)
        {
            miniEvent = go.AddComponent<HUIMiniEventScript>();
        }
		miniEvent.SetUIEvent (type, eventId);
    }

	public static void SetUIMiniEvent(GameObject go, enUIEventType type, enUIEventID eventId, stUIEventParams eventParams)
	{
		HUIMiniEventScript miniEvent = go.GetComponent<HUIMiniEventScript>();
		if (miniEvent == null)
		{
			miniEvent = go.AddComponent<HUIMiniEventScript>();
		}
		miniEvent.SetUIEvent (type, eventId, eventParams);
	}

	public static Vector2 WorldToScreenPoint(Camera camera, Vector3 worldPoint)
	{
		if (camera != null) {
			Vector3 res = camera.WorldToScreenPoint (worldPoint);
			return new Vector2 (res.x, res.y);
		}
		return new Vector2(worldPoint.x, worldPoint.y);
	}

	public static Vector3 ScreenToWorldPoint(Camera camera, Vector2 screenPoint, float z)
	{
		return (camera != null) ? camera.ViewportToWorldPoint(new Vector3(screenPoint.x / (float)Screen.width, screenPoint.y / (float)Screen.height, z)) : new Vector3(screenPoint.x, screenPoint.y, z);
	}

	public static void SetParent(Transform child, Transform parent) {
		child.transform.SetParent (parent);
		child.transform.localScale = Vector3.one;
		child.transform.localRotation = Quaternion.identity;
		child.transform.localPosition = Vector3.zero;
	}
}

﻿using UnityEngine;
using System.Collections;

public enum enUIEventID
{
    None,
    UI_OnFormVisibleChanged,
    UI_OnFormPriorityChanged,
	UI_Form_ClickAnyWhere,

    Main_Btn_Adventure_Click,
    Main_Btn_GoldChange_Click, Main_Btn_Game_Click,
    Main_Btn_Gift_Click,
    Main_Btn_Study_Math_Click,
    Main_Btn_Study_English_Click,
    Main_Btn_Paint_Click,
    Main_Btn_AR_Click,
    Main_Btn_Parent_Click,
	Main_Btn_Walk_Click,
	Main_Btn_Stand_Click,
	Main_Btn_Item_Child_Click,
    Main_Btn_Express_Click,
    Main_Btn_Radio_Click,

    //解锁所有礼包
    UnlockBag_Button_Close,
    UnlockBag_Button_Buy,

    //电台推广
    RainbowRadioGen_Button_Close,
    RainbowRadioGen_Button_Download,

    // ----乐学 英语
    // 返回按钮
    StudyEnglish_Button_Return_Click,
    // 画字母返回
    DrawLetter_Button_Return_Click,
    // ----乐学 数学
    StudyDrawNumber_Button_Return_Click,
    StudyMathNumber_Button_Return_Click,

    Job_Btn_Close_Click,
    MakeCake_Use_Material_Click,

    //乐学数学 加减法
    AddPlus_Btn_Return,
    AddPlus_Image_XunZhangAdd,
    AddPlus_Numbers_ShowUp,
    AddPlus_NumbersBtn_Click,
    AddPlus_Numbers_Hide,
    AddPlus_Numbes_HideOver,
    AddPlus_StartMove_Xunzhang,
    AddPlus_RoundOver,
    AddPlus_GameProcedure_Change,
    AddPlus_Load_Equation,
    AddPlus_SheepFeather_Cuted,
    AddPlus_Box_Click,
    AddPlus_Car_Click,
    AddPlus_SheepShout,
    AddPlus_HideEquation,
    AddPlus_UpdateOperateTime,
    AddPlus_WrongNumber_Click,
    AddPlus_GestureGuide_Show,
    AddPlus_GestureGuide_Hide,
    AddPlus_ConfigFeatherPoints,
    AddPlus_FeatherInit,
    AddPlus_FeatherAdd,
    AddPlus_FeatherRightShow,
    AddPlus_FeatherDestroy,


    Subtraction_BtnBack,
    Subtraction_JiZhang,
    Subtraction_CarryOut,


    //水果
    Fruit_return,
    Fruit_Qipao_Click,
    Fruit_Qipao_Hide,
    Fruit_TimeTo_xuanShuiguo,
    Fruit_Btn_Ok,
    Fruit_Drink,
    Fruit_GameOver,

    //医院
    Hospital_return,

    #region Radio
    Radio_Btn_Play,
	Radio_Btn_Next,
	Radio_Btn_Pre,
	Radio_Btn_Back,
	Radio_Btn_Exit,
	Radio_Slider_Value_Change,
	#endregion

	#region Study Phonics
	Phonics_Back_Click,
	Phonics_Letter_Click,
	Phonics_Next_Letter,
    Phonics_Kcjs_Click,
    #endregion

    //趣味拼图
    EmailPuzzle_Btn_Click,
    StampPanel_Btn_Click,
    TextPanel_Btn_Click,
    ItemPanel_Btn_Click,
    ShowPhoto_Btn_Click,
    ReturnMain_Btn_Click,
    Finish_Btn_Click,
    Close_Btn_Click,
    Yes_Btn_Click,
    No_Btn_Click,


    #region parent
    Parent_Close_Btn,
    Setting_Close_Btn,
    Setting_More_Game,
    #endregion


    // Cake shop UI event
    CakeShop_Btn_Back,

    #region notic
    Notice_Btn_Close_Click,
    Notice_Btn_Cancel_Click,
    Notice_Btn_Ok_Click,
    #endregion

    #region SuperMarket
    ShopList_Btn_Bg,
    MenuList1_Btn,
    MenuList2_Btn,
    MenuList3_Btn,
    MenuList4_Btn,
    MenuList5_Btn,
    MenuList6_Btn,
    Return_Btn,
    Guid_Btn,
    #endregion

    //电台
    CaiHongFM_Click,
    Recommend_Click,
    TypeOne_Click,
    TypeTwo_Click,
    TypeThree_Click,
    TypeFour_Click,
    TypeFive_Click,
    Radio_Slider_DragBegin,
    Radio_Slider_DragEnd,
    Radio_Slider_Click,
    Radio_GetAudioInfo,
    Radio_Scroll_Move,
    Radio_Net_Loading,

    FootBallGameReturnClick,
}

﻿using UnityEngine;
using System.Collections;

public class HUIAnimatorScript : HUIComponent {

	private Animator m_animator;

	private int m_currentAnimatorStateCounter;

	public string m_currentAnimatorStateName
	{
		get;
		private set;
	}

	public override void Initialize(HUIFormScript formScript)
	{
		if (this.m_isInitialized)
		{
			return;
		}
		base.Initialize(formScript);
		this.m_animator = base.gameObject.GetComponent<Animator>();
	}

	protected override void OnDestroy()
	{
		this.m_animator = null;
		base.OnDestroy();
	}

	private void Update()
	{
		if (this.m_belongedFormScript != null && this.m_belongedFormScript.IsClosed())
		{
			return;
		}
		if (this.m_currentAnimatorStateName == null)
		{
			return;
		}
		//播放一次
		if ((int)this.m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime > this.m_currentAnimatorStateCounter)
		{
			this.m_animator.StopPlayback();
			this.m_currentAnimatorStateName = null;
			this.m_currentAnimatorStateCounter = 0;
		}
	}

	public void PlayAnimator(string stateName)
	{
		if (this.m_animator == null)
		{
			this.m_animator = base.gameObject.GetComponent<Animator>();
		}
		if (!this.m_animator.enabled)
		{
			this.m_animator.enabled = true;
		}
		this.m_animator.Play(stateName, 0, 0f);
		this.m_currentAnimatorStateName = stateName;
		this.m_animator.Update(0f);
		this.m_animator.Update(0f);
		//有可能是循环了多次
		this.m_currentAnimatorStateCounter = (int)this.m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
	}

	public void SetBool(string name, bool value)
	{
		this.m_animator.SetBool(name, value);
	}

	public void SetAnimatorEnable(bool isEnable)
	{
		if (this.m_animator)
		{
			this.m_animator.enabled = isEnable;
			base.enabled = isEnable;
		}
	}

	public void SetInteger(string name, int value)
	{
		this.m_animator.SetInteger(name, value);
	}

	public void SetTriiger(string name)
	{
		this.m_animator.SetTrigger(name);
	}

	public void StopAnimator()
	{
	}

	public bool IsAnimationStopped(string animationName)
	{
		return string.IsNullOrEmpty(animationName) || !string.Equals(this.m_currentAnimatorStateName, animationName);
	}
}

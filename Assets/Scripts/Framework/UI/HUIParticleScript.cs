﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUIParticleScript : HUIComponent {

	public string m_resPath = string.Empty;

	public bool m_isFixScaleToForm;

	private Renderer[] m_renderers;

	private int m_rendererCount;

	private void LoadRes()
	{
		string text = this.m_resPath;
		if (!string.IsNullOrEmpty(text))
		{
			GameObject gameObject = ResourceManager.Instance.GetResource(text, typeof(GameObject), enResourceType.UIPrefab, false, false).m_content as GameObject;
			if (gameObject != null && base.gameObject.transform.childCount == 0)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate(gameObject) as GameObject;
				gameObject2.transform.SetParent(base.gameObject.transform);
				gameObject2.transform.localPosition = Vector3.zero;
				gameObject2.transform.localRotation = Quaternion.identity;
				gameObject2.transform.localScale = Vector3.one;
			}
		}
	}

	public void LoadRes(string resName)
	{
		if (!this.m_isInitialized)
		{
			return;
		}
		this.m_resPath = resName;
		this.LoadRes();
		this.InitializeRenderers();
		this.SetSortingOrder(this.m_belongedFormScript.GetSortingOrder());
		if (this.m_isFixScaleToForm)
		{
			this.ResetScale();
		}
		if (this.m_belongedFormScript.IsHided())
		{
			this.Hide();
		}
	}

	public override void Initialize(HUIFormScript formScript)
	{
		if (this.m_isInitialized)
		{
			return;
		}
		this.LoadRes();
		this.InitializeRenderers();
		base.Initialize(formScript);
		if (this.m_isFixScaleToForm)
		{
			this.ResetScale();
		}
		if (this.m_belongedFormScript.IsHided())
		{
			this.Hide();
		}
	}

	protected override void OnDestroy()
	{
		this.m_renderers = null;
		base.OnDestroy();
	}

	public override void Hide()
	{
		base.Hide();
		HUIUtility.SetGameObjectLayer(base.gameObject, 31);
	}

	public override void Appear()
	{
		base.Appear();
		HUIUtility.SetGameObjectLayer(base.gameObject, 5); //5是ui层
	}

	public override void SetSortingOrder(int sortingOrder)
	{
		base.SetSortingOrder(sortingOrder);
		for (int i = 0; i < this.m_rendererCount; i++)
		{
			this.m_renderers[i].sortingOrder = sortingOrder;
		}
	}

	private void InitializeRenderers()
	{
		this.m_renderers = new Renderer[100];
		this.m_rendererCount = 0;
		HUIUtility.GetComponentsInChildren<Renderer>(base.gameObject, this.m_renderers, ref this.m_rendererCount);
	}

	private void ResetScale()
	{
		float num = 1f / this.m_belongedFormScript.gameObject.transform.localScale.x;
		base.gameObject.transform.localScale = new Vector3(num, num, 0f);
	}
}

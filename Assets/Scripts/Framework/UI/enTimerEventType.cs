﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enTimerEventType {
	TimeStart,
	TimeUp,
	TimeChanged
}

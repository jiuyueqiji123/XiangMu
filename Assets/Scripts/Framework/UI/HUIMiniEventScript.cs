﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class HUIMiniEventScript : HUIComponent, IPointerDownHandler, IEventSystemHandler, IPointerClickHandler, IPointerUpHandler {

	public delegate void OnUIEventHandler(HUIEvent uiEvent);

	public HUIMiniEventScript.OnUIEventHandler onClick;

	[HideInInspector]
	public enUIEventID m_onDownEventID;

	[HideInInspector]
	public stUIEventParams m_onDownEventParams;

	[HideInInspector]
	public enUIEventID m_onUpEventID;

	[HideInInspector]
	public stUIEventParams m_onUpEventParams;

	[HideInInspector]
	public enUIEventID m_onClickEventID;

	[HideInInspector]
	public stUIEventParams m_onClickEventParams;

	public bool m_closeFormWhenClicked;

	protected override void OnDestroy()
	{
		this.onClick = null;
		base.OnDestroy();
	}

	public void SetUIEvent(enUIEventType eventType, enUIEventID eventID)
	{
		if (eventType == enUIEventType.Down) {
			this.m_onDownEventID = eventID;
		} else if (eventType == enUIEventType.Up) {
			this.m_onUpEventID = eventID;
		} else if (eventType == enUIEventType.Click) {
			this.m_onClickEventID = eventID;
		}
	}

	public void SetUIEvent(enUIEventType eventType, enUIEventID eventID, stUIEventParams eventParams)
	{
		if (eventType == enUIEventType.Down) {
			this.m_onDownEventID = eventID;
			this.m_onDownEventParams = eventParams;
		} else if (eventType == enUIEventType.Up) {
			this.m_onUpEventID = eventID;
			this.m_onUpEventParams = eventParams;
		} else if (eventType == enUIEventType.Click) {
			this.m_onClickEventID = eventID;
			this.m_onClickEventParams = eventParams;
		}
	}

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		this.DispatchUIEvent(enUIEventType.Down, eventData);
	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		this.DispatchUIEvent(enUIEventType.Up, eventData);
	}

	public virtual void OnPointerClick(PointerEventData eventData)
	{
		bool flag = true;
		if (this.m_belongedFormScript != null && !this.m_belongedFormScript.m_enableMultiClickedEvent)
		{
			flag = (this.m_belongedFormScript.m_clickedEventDispatchedCounter == 0);
			this.m_belongedFormScript.m_clickedEventDispatchedCounter++;
		}
		if (flag)
		{
			this.DispatchUIEvent(enUIEventType.Click, eventData);
			if (this.m_closeFormWhenClicked && this.m_belongedFormScript != null)
			{
				this.m_belongedFormScript.Close();
			}
		}
	}

	private void DispatchUIEvent(enUIEventType eventType, PointerEventData pointerEventData)
	{
		HUIEvent uIEvent = HUIEventManager.Instance.GetUIEvent();
		if (eventType == enUIEventType.Down) {
			if (this.m_onDownEventID != enUIEventID.None) {
				uIEvent.m_eventID = this.m_onDownEventID;
				uIEvent.m_eventParams = this.m_onDownEventParams;
			}
		} else if (eventType == enUIEventType.Up) {
			if (this.m_onUpEventID != enUIEventID.None) {
				uIEvent.m_eventID = this.m_onUpEventID;
				uIEvent.m_eventParams = this.m_onUpEventParams;
			}
		} else if (eventType == enUIEventType.Click) {
            CommonSound.PlayClick();
			if (this.m_onClickEventID == enUIEventID.None)
			{
				if (this.onClick != null)
				{
					uIEvent.m_eventID = enUIEventID.None;
					uIEvent.m_eventParams = this.m_onClickEventParams;
					this.onClick(uIEvent);
				}
				return;
			}
			uIEvent.m_eventID = this.m_onClickEventID;
			uIEvent.m_eventParams = this.m_onClickEventParams;
		}
		
		uIEvent.m_srcFormScript = this.m_belongedFormScript;
		uIEvent.m_pointerEventData = pointerEventData;
		uIEvent.m_srcWidget = base.gameObject;
		uIEvent.m_srcWidgetScript = this;
		if (eventType == enUIEventType.Click && this.onClick != null)
		{
			this.onClick(uIEvent);
		}
		base.DispatchUIEvent(uIEvent);
	}
}

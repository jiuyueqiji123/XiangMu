﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUILocalizationScript : HUIComponent {

	private string m_key;

	private Text m_text;

	public override void Initialize (HUIFormScript formScript)
	{
		if (this.m_isInitialized)
		{
			return;
		}
		base.Initialize(formScript);
		this.m_text = base.gameObject.GetComponent<Text>();
		this.SetDisplay();
	}

	protected override void OnDestroy ()
	{
		this.m_text = null;
		base.OnDestroy ();
	}

	public void SetKey(string key)
	{
		this.m_key = key;
		this.SetDisplay();
	}

	public void SetDisplay()
	{
		if (this.m_text == null || string.IsNullOrEmpty(this.m_key) || !TextManager.Instance.IsTextLoaded())
		{
			return;
		}
		this.m_text.text = TextManager.Instance.GetText(this.m_key);
	}
}

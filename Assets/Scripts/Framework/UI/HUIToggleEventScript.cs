﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class HUIToggleEventScript : HUIComponent {

	[HideInInspector]
	public enUIEventID m_onValueChangedEventID;

	private Toggle m_toggle;

	public override void Initialize(HUIFormScript formScript)
	{
		if (this.m_isInitialized)
		{
			return;
		}
		this.m_toggle = base.gameObject.GetComponent<Toggle>();
		this.m_toggle.onValueChanged.RemoveAllListeners();
		this.m_toggle.onValueChanged.AddListener(new UnityAction<bool>(this.OnToggleValueChanged));
		Transform transform = base.gameObject.transform.Find("Label");
		if (transform != null)
		{
			if (this.m_toggle.isOn)
			{
				
			}
			else
			{
				
			}
		}
		base.Initialize(formScript);
	}

	protected override void OnDestroy()
	{
		this.m_toggle = null;
		base.OnDestroy();
	}

	private void OnToggleValueChanged(bool isOn)
	{
		if (this.m_onValueChangedEventID == enUIEventID.None)
		{
			return;
		}
		HUIEvent uIEvent = HUIEventManager.Instance.GetUIEvent();
		uIEvent.m_srcFormScript = this.m_belongedFormScript;
		uIEvent.m_srcWidget = base.gameObject;
		uIEvent.m_srcWidgetScript = this;
		uIEvent.m_pointerEventData = null;
		uIEvent.m_eventID = this.m_onValueChangedEventID;
		uIEvent.m_eventParams.togleIsOn = isOn;
		Transform transform = base.gameObject.transform.Find("Label");
		if (transform != null)
		{
			if (isOn)
			{
				
			}
			else
			{
				
			}
		}
		base.DispatchUIEvent(uIEvent);
	}
}

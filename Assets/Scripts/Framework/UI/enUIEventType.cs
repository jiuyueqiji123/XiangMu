﻿using UnityEngine;
using System.Collections;

public enum enUIEventType
{
	Down,
	Click,
	HoldStart,
	Hold,
	HoldEnd,
	DragStart,
	Drag,
	DragEnd,
	Drop,
	Up
}

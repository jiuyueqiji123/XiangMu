﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUICommonSystem : Singleton<HUICommonSystem> {


	public static void PlayAnimator(GameObject target, string stateName)
	{
		HUIAnimatorScript cUIAnimatorScript = (target == null) ? null : target.GetComponent<HUIAnimatorScript>();
		if (cUIAnimatorScript == null)
		{
			return;
		}
		cUIAnimatorScript.PlayAnimator(stateName);
	}
}

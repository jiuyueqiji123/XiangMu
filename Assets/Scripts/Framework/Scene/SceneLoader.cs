﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoSingleton<SceneLoader> {

	public static string EmptyScene = "Empty";

    public static string StudyEnglishScene = "StudyEnglish";

    public static string StudyMathScene = "StudyMath";

    public static string StudyAddPlusScene = "StudyAddPlus";            //10以内加减法

    public static string MaoXianScene = "MaoXian";

    public static string ExpressScene = "Express";

	public static string MainScene = "Main";

    public static string BaseFruitScene = "Fruit";

    public static string EmailScene = "Email";

    public static string CakeShopScene = "CakeShop";

    public static string AdditionScene = "Addition";

    public static string ChineseRestaurantScene = "ChineseRestaurant";

    public static string SuperMarketScene = "SuperMarket";

    public static string HospitalScene = "Hospital";

    public static string RadioScene = "Radio";

    public static string StudySubtractionScene = "StudySubtraction";

    public static string CardGameScene = "CardGame";

    public static string EmptyWithoutCameraScene = "EmptyWithoutCameraScene";

    #region package-english

    public static string ARBaikeScene = "ARBaike";

    #endregion

    public delegate void LoadCompletedDelegate();

	private string sceneName;

	private LoadCompletedDelegate finishDel;

	public void LoadScene(string name, SceneLoader.LoadCompletedDelegate finishDelegate = null)
	{
		this.sceneName = name;
		this.finishDel = finishDelegate;
		StartCoroutine (load ());
	}

	protected override void OnDestroy ()
	{
		StopCoroutine (load ());
		base.OnDestroy ();
	}

	private IEnumerator load() {
		SceneManager.LoadScene(sceneName);

        yield return null;

        if (finishDel != null)
        {
            finishDel();

            finishDel = null;
        }
	}
}

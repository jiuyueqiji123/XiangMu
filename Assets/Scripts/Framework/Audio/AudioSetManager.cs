﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSetManager : Singleton<AudioSetManager> {

	private const string MUTE_KEY = "AudioSetManager.muted";

	private const string MUTE_SOUND_KEY = "AudioSetManager.muted.sound";

	private const string MUTE_MUSIC_KEY = "AudioSetManager.muted.music";

	public bool Muted;

	public bool SoundMuted;

	public bool MusicMuted;

	public override void Init ()
	{
		base.Init ();
		this.Muted = PlayerPrefsX.GetBool (MUTE_KEY, false);
		this.SoundMuted = PlayerPrefsX.GetBool (MUTE_SOUND_KEY, this.Muted);
		this.MusicMuted = PlayerPrefsX.GetBool (MUTE_MUSIC_KEY, this.Muted);
	}

	public void ToggleMute() {
		this.Muted = !this.Muted;
		PlayerPrefsX.SetBool (MUTE_KEY, this.Muted);
		SetMuteSound (this.Muted);
		SetMuteMusic (this.Muted);
	}

	public void SetMuteSound(bool val) {
		this.SoundMuted = val;
		PlayerPrefsX.SetBool (MUTE_SOUND_KEY, this.SoundMuted);
		EventBus.Instance.BroadCastEvent<bool> (EventID.SET_AUDIO_SOUND_CHANGE, this.SoundMuted);
	}

	public void SetMuteMusic(bool val) {
		this.MusicMuted = val;
		PlayerPrefsX.SetBool (MUTE_MUSIC_KEY, this.MusicMuted);
		EventBus.Instance.BroadCastEvent<bool> (EventID.SET_AUDIO_MUSIC_CHANGE, this.MusicMuted);
	}
}

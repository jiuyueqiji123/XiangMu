﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAudioSource {

	private float PauseTime;

	private AudioSource AudioSource;

	private AudioClip AudioClip;

	public bool StopOnSceneChange
	{
		get;
		private set;
	}

	public AudioClip Clip
	{
		get
		{
			return this.AudioClip;
		}
		set
        {
            if (this.AudioSource == null)
                return;
            this.AudioClip = value;
			this.AudioSource.clip = this.AudioClip;
		}
	}

	public bool PlayOnAwake
	{
		get
		{
			return this.AudioSource == null ? false : this.AudioSource.playOnAwake;
		}
		set
        {
            if (this.AudioSource == null)
                return;
            this.AudioSource.playOnAwake = value;
		}
	}

	public bool Loop
	{
		get
		{
			return this.AudioSource == null ? false : this.AudioSource.loop;
		}
		set
        {
            if (this.AudioSource == null)
                return;
            this.AudioSource.loop = value;
		}
	}

	public bool Mute
	{
		get
		{
			return this.AudioSource == null ? false : this.AudioSource.mute;
		}
		set
        {
            if (this.AudioSource == null)
            {
                return;
            }
            this.AudioSource.mute = value;
		}
	}

	public bool IsPlaying
	{
		get
		{
            if (Clip == null)
                return false;
			return this.AudioSource == null ? false : this.AudioSource.isPlaying;
		}
	}

    public float Length
    {
        get
        {
            if (this.Clip == null)
                return this.AudioSource == null ? 0 : this.AudioSource.time;
            return this.Clip.length;
        }
    }

    public float Time
	{
		get
		{
			return this.AudioSource == null ? 0 : this.AudioSource.time;
		}
	}

	public float Pitch
	{
		get
		{
            return this.AudioSource == null ? 0 : this.AudioSource.pitch;
		}
		set
        {
            if (this.AudioSource == null)
            {
                return;
            }
            this.AudioSource.pitch = value;
		}
	}

	public float Volume
	{
		get
		{
			return this.AudioSource == null ? 0 : this.AudioSource.volume;
		}
		set
		{
			if (this.AudioSource == null)
			{
				return;
			}
			this.AudioSource.volume = value;
		}
	}

	public GameAudioSource(AudioSource audioSource)
	{
		this.AudioSource = audioSource;
	}

	public void Play()
    {
        if (this.AudioSource == null) return;
        this.Play(true);
	}

	public void Stop()
	{
        if (this.AudioSource == null) return;
		this.AudioClip = null;
		this.AudioSource.Stop();
	}

	public void Pause()
    {
        if (this.AudioSource == null) return;
        if (this.AudioSource.isPlaying)
		{
			this.PauseTime = this.AudioSource.time;
			this.AudioSource.Stop();
		}
	}

	public void Play(bool stopOnSceneChange)
    {
        if (this.AudioSource == null) return;
        this.Play(false, stopOnSceneChange);
	}

	public void Play(bool resumePlaying, bool stopOnSceneChange)
    {
        if (this.AudioSource == null) return;
        this.Play(resumePlaying, stopOnSceneChange, 0f);
	}

	public void Play(bool resumePlaying, bool stopOnSceneChange, float startAtTime)
    {
        if (this.AudioSource == null) return;
        this.StopOnSceneChange = stopOnSceneChange;
		if (resumePlaying)
		{
			if (this.AudioSource.clip != this.Clip)
			{
				this.AudioSource.clip = this.Clip;
			}
			else if (this.AudioSource.isPlaying)
			{
				return;
			}
			this.AudioSource.time = this.PauseTime;
		}
		if (this.PauseTime == 0f)
		{
			this.AudioSource.time = startAtTime;
		}
		this.PauseTime = 0f;
		this.AudioSource.Play();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PlayAnimParam {

	public string AnimState;

	public float BlendTime;

    public float Speed;

	public bool CancelAll;

	public bool CancelCurrent;

	public bool Loop;
}

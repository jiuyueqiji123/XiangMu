﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;

public class TLIdentifyHolder : MonoBehaviour {

    public enum EAssetPathType
    {
        None = 0,
        ximalaya = 1,
        maerdaifu = 2,
        danmai_prefab = 3,
        danmai_prefab_big = 4,
        monage_prefab = 5,
    }

    public EAssetPathType pathType;

    [SerializeField]
    private string _AssetPath;    //资源路径
    public string AssetPath
    {
        get
        {
            switch (pathType)
            {
                case EAssetPathType.danmai_prefab:
                    return "story_danmai/story_danmai_prefab/";
                case EAssetPathType.danmai_prefab_big:
                    return "story_danmai/story_danmai_prefab_big/";
                case EAssetPathType.monage_prefab:
                    return "story_monage/story_monage_prefab/";
                default:
                    return _AssetPath;
            }
        }
    }

    public void SetAssetPath(int type, string path)
    {
        pathType = (EAssetPathType)type;
        _AssetPath = path;
    }

    public string GetAudioPath()
    {
        switch (pathType)
        {
            case EAssetPathType.danmai_prefab:
            case EAssetPathType.danmai_prefab_big:
                return "story_danmai/story_danmai_audio/";
            default:
                return string.Empty;
        }
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;

public class TimelineInput : MonoBehaviour {


    private Camera cam;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    void Update () {

        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnDown();
            }
            //if (Input.GetMouseButton(0))
            //{
            //    EventBus.Instance.BroadCastEvent(EventID.ON_MOUSE_BUTTON_HOVER);
            //}
            if (Input.GetMouseButtonUp(0))
            {
                OnUp();
            }
        }
        else
        {
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    OnDown();
                }
                //if (Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Moved)
                //{
                    //EventBus.Instance.BroadCastEvent(EventID.ON_MOUSE_BUTTON_HOVER);
                //}
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    OnUp();
                }
            }
        }


	}

    private void OnDown()
    {
        //Transform target = FTools.GetRaycastHitTarget(cam, Input.mousePosition, 999999);
        //BaseTrackHolder holder = target == null? null : target.GetComponent<BaseTrackHolder>();
        //if (holder != null)
        //{
        //    holder.OnRaycast();
        //}
    }

    private void OnUp()
    {
        
    }


}

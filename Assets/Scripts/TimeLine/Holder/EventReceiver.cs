﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;

public class EventReceiver : BaseTrackHolder
{

    public int seq;
    public RaycastHolder[] raycastArray;



    public override void Initialize(string id)
    {
        base.Initialize(id);

        raycastArray = FTimelineDispatch.GetObjArray<RaycastHolder>(TimelineID);
        //foreach (RaycastHolder holder in raycastArray)
        //{
        //    holder.Initialize(TimelineID);
        //}
    }


    protected override void OnPlay()
    {
        base.OnPlay();

        //RefreshRaycast(clipInfo.isEnableRaycast);
    }

    protected override void OnPause()
    {
        base.OnPause();

        RefreshRaycast(false);
    }

    private void RefreshRaycast(bool isEnable)
    {
        //if (raycastArray != null)
        //{
        //    foreach (RaycastHolder holder in raycastArray)
        //    {
        //        holder.Refresh(isEnable, clipInfo == null ? string.Empty : clipInfo.eventID);
        //    }
        //}
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTrackHolder : MonoBehaviour, IEasyTimer, IEasyAudio
{
    public bool bTimerCallback
    {
        get { return this == null ? false : true; }
    }


    private string _TimelineID;
    public string TimelineID
    {
        get { return _TimelineID; }
    }

    private BoxCollider _boxCollider;
    public BoxCollider boxCollider
    {
        get
        {
            if (_boxCollider == null)
                _boxCollider = GetComponent<BoxCollider>();
            return _boxCollider;
        }
    }

    //public AudioSource audioSource
    //{
    //    get { return gameObject.AddComponentEx<AudioSource>(); }
    //}

    protected ClipInfo clipInfo;



    protected virtual void Awake()
    {
        AddListeners();
        
    }

    protected virtual void Start()
    {

    }

    protected virtual void OnDestroy()
    {
        RemoveListeners();

    }

    protected virtual void AddListeners() { }
    protected virtual void RemoveListeners() { }

    // 进入Clip的时候调用，会多次调用，如果暂停之后继续播放，会再调用一次
    public void OnBehaviourPlay(ClipInfo info)
    {
        clipInfo = info;
        OnPlay();
        //Debug.LogError("OnBehaviourPlay");
    }
    public void OnBehaviourPause()
    {
        OnPause();
        //Debug.LogError("OnBehaviourPause");
    }


    public virtual void Initialize(string id)
    {
        _TimelineID = id;
    }

    public virtual void OnRaycast(string id) { }



    protected virtual void OnPlay() { }
    protected virtual void OnPause() { }

    protected void PauseTimeline()
    {
        FTimelineDispatch.PauseTimeline(_TimelineID);
    }

    protected void ResumeTimeline()
    {
        FTimelineDispatch.ResumeTimeline(_TimelineID);
    }

    protected void EnableCollider(bool value)
    {
        boxCollider.enabled = value;
    }


}

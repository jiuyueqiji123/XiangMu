﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;


[System.Serializable]
public class AnimationInfo
{
    public string eventID;
    public string audioAnimation;
    public string idleAnimation;
    public string audioClipName;
    public AudioClip audioClip;
}
public class AnimationHolder : BaseTrackHolder
{

    private Animator _animator;
    public Animator animator
    {
        get
        {
            if (_animator == null && transform.childCount > 0)
                _animator = transform.GetChild(0).GetComponent<Animator>();
            return _animator;
        }
    }


    public string audioClipPath;
    public AnimationInfo[] infoArray;

    protected GameAudioSource audioSource;
    private Dictionary<string, AnimationInfo> m_infoDict = new Dictionary<string, AnimationInfo>();


    protected override void Start()
    {
        base.Start();

        if (infoArray != null)
        {
            foreach (AnimationInfo info in infoArray)
            {
                m_infoDict.Add(info.eventID, info);
            }
        }

    }


    //private void Update()
    //{
    //    if (transform.name == "ledi")
    //    {
    //        Debug.Log(transform.position);
    //    }
    //}

    protected override void AddListeners()
    {
        EventBus.Instance.AddEventHandler<string>(EventID.ON_TIMELINE_PAUSE, OnTimelinePause);
    }

    protected override void RemoveListeners()
    {
        EventBus.Instance.RemoveEventHandler<string>(EventID.ON_TIMELINE_PAUSE, OnTimelinePause);
    }

    public override void OnRaycast(string id)
    {
        PlayAnimationAndAudio(id);
    }

    protected virtual void PlayAnimationAndAudio(string id)
    {
        if (audioSource != null)
            return;

        if (m_infoDict.ContainsKey(id))
        {
            AnimationInfo info = m_infoDict[id];
            if (info != null && animator != null)
            {
                string[] animArr = info.audioAnimation.Split(';');
                if (!string.IsNullOrEmpty(info.audioAnimation))
                    animator.CrossFade(animArr.Length == 2 ? animArr[0] : info.audioAnimation, 0.1f);
                PlayAudio(info.audioClipName, () =>
                {
                    if (!string.IsNullOrEmpty(info.idleAnimation))
                    {
                        if (animArr.Length == 2)
                        {
                            animator.CrossFade(animArr[1], 0.1f);
                            this.AddTimerEx(0.1f, () => {

                                animator.CrossFade(info.idleAnimation, 0.1f);
                            });
                        }
                        else
                            animator.CrossFade(info.idleAnimation, 0.1f);
                    }
                });
            }
        }
    }

    protected void OnTimelinePause(string id)
    {
        PlayPuaseAnimation(id);
    }

    protected void PlayPuaseAnimation(string id)
    {
        if (m_infoDict.ContainsKey(id))
        {
            animator.CrossFade(m_infoDict[id].idleAnimation, 0);
        }
    }

    protected void PlayAudio(string clipName, System.Action callback = null)
    {
        if (string.IsNullOrEmpty(clipName))
        {
            if (callback != null)
                callback();
            return;
        }

        TLIdentifyHolder holder = GetComponent<TLIdentifyHolder>();
        if (holder == null)
            holder = GetComponentInParent<TLIdentifyHolder>();
        audioSource = this.PlayAudioEx(holder.GetAudioPath() + clipName);
        //audioSource.clip = clip;
        //audioSource.Play();
        this.AddTimerEx(audioSource.Length, () =>
        {
            if (callback != null)
                callback();
            audioSource = null;
        });
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class TrackData
{

    // 轨道名
    public string trackName;

    // 资源名
    public string objName;

    // 是否是根节点
    public bool bIsIdentify_Parent;

    // 根节点名字
    public string parentName;

    //剧情名字
    public string storyName;

}

[Serializable]
public class TimelineSingleData
{
    public string ID;

    public List<TrackData> trackParentList = new List<TrackData>();
    public List<TrackData> trackChildList = new List<TrackData>();
    public List<TrackData> trackAudioList = new List<TrackData>();

}

[Serializable]
public class TimelineData
{
    public List<TimelineSingleData> timelineList = new List<TimelineSingleData>();
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class FTimelineTrackInfo
{
    public FTimelineTrackInfo()
    {
        assetType = EAssetType.GameObject;
    }

    public enum EAssetType
    {
        GameObject,
        AudioSource,
        //EventReceiver,
        BaseTrackHolder,
        Prefab,
    }

    public string trackName;    // 轨道名
    public string objName;    // 资源名
    public string parentName;    // 父物体名字
    public string assetPath;    // 完整资源路径 具体到资源
    //public string className;    // holder class name
    public int assetPathType;

    public EAssetType assetType = EAssetType.GameObject;

}

[Serializable]
public class FTimelineInfo
{
    public string ID;   // timeline标识ID
    public string describe; // 描述

    public List<FTimelineTrackInfo> trackParentList = new List<FTimelineTrackInfo>();
    public List<FTimelineTrackInfo> trackChildList = new List<FTimelineTrackInfo>();
    public List<FTimelineTrackInfo> trackAudioList = new List<FTimelineTrackInfo>();
    public List<FTimelineTrackInfo> trackOtherList = new List<FTimelineTrackInfo>();
    //public List<FTimelineTrackInfo> trackOtherList = new List<FTimelineTrackInfo>();

}

[Serializable]
public class FTimelineAllInfo
{
    public List<FTimelineInfo> timelineAllInfoList = new List<FTimelineInfo>();
}


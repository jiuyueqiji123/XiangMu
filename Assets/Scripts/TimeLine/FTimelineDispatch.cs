﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class FTimelineDispatch
{

    // 判断某个timeline是否正在播放
    public static bool IsPlaying(string timelineID)
    {
        if (TimelinePlayer.HasInstance() && TimelinePlayer.Instance.IsRuning(timelineID))
            return TimelinePlayer.Instance.IsPlaying(timelineID);

        PlayableDirector director = FindDirector(timelineID);
        if (director != null)
        {
            return director.state == PlayState.Playing;
        }
        return false;
    }

    public static void PlayTimeline(string timelineID)
    {
        TimelinePlayer.Instance.Play(timelineID);
    }

    public static void DestroyTimeline(string timelineID)
    {
        if (TimelinePlayer.Instance.IsRuning(timelineID))
        {
            TimelinePlayer.Instance.Remove(timelineID);
            return;
        }
        PlayableDirector director = FindDirector(timelineID);
        if (director != null)
        {
            director.Pause();
            GameObject.Destroy(director.gameObject);
        }
    }

    // 暂停
    public static void PauseTimeline(string timelineID)
    {
        Debug.Log("FPlayableTrackDispatch PauseTimeline");

        if (TimelinePlayer.Instance.IsPlaying(timelineID))
        {
            TimelinePlayer.Instance.Pause(timelineID);
            return;
        }
        PlayableDirector director = FindDirector(timelineID);
        if (director != null)
        {
            director.Pause();
        }
    }

    // 恢复
    public static void ResumeTimeline(string timelineID)
    {
        Debug.Log("FPlayableTrackDispatch ResumeTimeline");

        if (TimelinePlayer.Instance.IsRuning(timelineID))
        {
            TimelinePlayer.Instance.Resume(timelineID);
            return;
        }
        PlayableDirector director = FindDirector(timelineID);
        if (director != null)
        {
            director.Resume();
        }
    }

    public static GameObject GetTimelineObj(string timelineID)
    {
        if (TimelinePlayer.Instance.IsRuning(timelineID))
        {
            return TimelinePlayer.Instance.GetDirector(timelineID).gameObject;
        }
        PlayableDirector director = FindDirector(timelineID);
        return director == null ? null : director.gameObject;
    }

    public static Transform GetTimelineTransform(string timelineID)
    {
        if (TimelinePlayer.Instance.IsRuning(timelineID))
        {
            return TimelinePlayer.Instance.GetDirector(timelineID).transform;
        }
        PlayableDirector director = FindDirector(timelineID);
        return director == null ? null : director.transform;
    }

    public static PlayableDirector GetDirector(string timelineID)
    {
        if (TimelinePlayer.Instance.IsRuning(timelineID))
        {
            return TimelinePlayer.Instance.GetDirector(timelineID);
        }
        return FindDirector(timelineID);
    }

    public static PlayableDirector FindDirector(string timelineID)
    {
        PlayableDirector[] directoies = GameObject.FindObjectsOfType<PlayableDirector>();
        for (int i = 0; i < directoies.Length; i++)
        {
            if (directoies[i].name == timelineID || directoies[i].name == "Timeline:" + timelineID)
                return directoies[i];
        }
        return null;
    }

    // 请求语音麦克风，5秒时间
    public static void RequestSpeak(System.Action callback = null)
    {
        WindowManager.Instance.OpenWindow(WinNames.StoryMicroPanel);
        StoryMicroWindow.Instance.AddEvent((result) => {
            WindowManager.Instance.CloseWindow(WinNames.StoryMicroPanel);
            if (callback != null)
                callback();
        });
    }

    //public static void ToNextTimeline(string currentTimeline, string nextTimeline)
    //{

    //    this.StartLodingAndEndEx(null, () =>
    //    {
    //        FTimelineDispatch.DestroyTimeline(timelineID);
    //        FTimelineDispatch.PlayTimeline(nextTimelineID);
    //    });
    //}


    public static T[] GetObjArray<T>(string timelineID)
    {
        PlayableDirector playableDirector = null;
        if (Application.isPlaying && TimelinePlayer.Instance.IsRuning(timelineID))
        {
            playableDirector = TimelinePlayer.Instance.GetDirector(timelineID);
            return playableDirector.gameObject.GetComponentsInChildren<T>(true);
        }
        playableDirector = FindDirector(timelineID);
        return playableDirector == null ? null : playableDirector.gameObject.GetComponentsInChildren<T>(true);
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using System.IO;
using WCBG.ToolsForUnity.Tools;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TimelineHolder : MonoBehaviour {



    // 唯一ID   取消ID参数，预制名就是ID
    public string timeline_ID
    {
        get
        {
            return transform.name;
        }
    }
    public string describe; // 描述




    private PlayableDirector _playableDirector;
    public PlayableDirector playableDirector
    {
        get
        {
            if (_playableDirector == null)
            {
                _playableDirector = gameObject.AddComponentEx<PlayableDirector>();
            }
            return _playableDirector;
        }
    }

    [Range(0,1)]
    public float range = 0;

    private void Start()
    {
        //Camera[] arr = gameObject.GetComponentsInChildren<Camera>(true);
        //foreach (Camera cam in arr)
        //{
        //    cam.gameObject.AddComponentEx<RaycastHolder>();
        //}

        //playableDirector.initialTime = range * playableDirector.duration;
        //playableDirector.Play();
        //gameObject.SetActive(false);
        TimelinePlayer.Instance.Play(timeline_ID, range);
        GameObject.Destroy(this.gameObject);
    }

    private void Update()
    {
        //range = (float)(playableDirector.time / playableDirector.duration);
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    ScreenCapture.CaptureScreenshot(Application.streamingAssetsPath + "/ScreenShot.png", 0);
        //}
    }

    public void LoadTimelineForOld()
    {
        DisposeTimeline();

        Dictionary<string, FTimelineInfo> tlDict = FTimelineTool.LoadTimeLineDict();
        if (tlDict != null && tlDict.ContainsKey(timeline_ID))
        {
            FTimelineInfo data = tlDict[timeline_ID];
            //Debug.LogError(JsonUtility.ToJson(data));
            LoadPlayableDirector(data);
        }
    }

    public void LoadTimeline()
    {
        DisposeTimeline();
        
        LoadPlayableDirector(FTimelineTool.LoadTimelineInfo(timeline_ID));
    }


    public void SaveTimelineInfo()
    {
        //FTimelineTool.Test();

        RenameAllTrack();
        SaveTimelineToData();
    }

    // 卸载 timeline
    public void DisposeTimeline()
    {
        describe = "";
        List<Transform> list = new List<Transform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            list.Add(transform.GetChild(i));
        }
        foreach (Transform target in list)
        {
            GameObject.DestroyImmediate(target.gameObject);
        }
        list.Clear();
    }

    // 删除数据
    public void DeleteTimelineInfo()
    {
        DisposeTimeline();
        FTimelineTool.DeleteTimelineInfo(timeline_ID);
    }

    public void RenameAllTrack()
    {
        FTimelineTool.RenameAllTrack(timeline_ID);
    }

    // 加载还原timeline 引用对象
    private void LoadPlayableDirector(FTimelineInfo data)
    {
        describe = data.describe;
        FTimelineTool.SetPlayableDirector(playableDirector, FTimelineTool.LoadPlayableAsset(timeline_ID), data);
    }

    // 保存timeline数据
    private void SaveTimelineToData()
    {
        FTimelineTool.SaveTimelineToInfo(playableDirector, timeline_ID, describe);
    }



}

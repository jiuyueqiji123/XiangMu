﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackColor(0f, 0.4866645f, 1f)]
[TrackClipType(typeof(ReceiverPlayableAsset))]
[TrackBindingType(typeof(BaseTrackHolder))]
public class ReceiverTrackAsset : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        PlayableDirector director = graph.GetResolver() as PlayableDirector;
        object obj = director.GetGenericBinding(this);
        if (obj != null)
        {
            BaseTrackHolder holder = obj as BaseTrackHolder;
            
            foreach (var c in GetClips())
            {
                //Clips are renamed after the actionType of the clip itself
                ReceiverPlayableAsset clip = (ReceiverPlayableAsset)c.asset;
                c.displayName = clip.clipType.ToString();
                clip.trackBinding = holder != null ? holder.gameObject : null;
            }
        }
        return base.CreateTrackMixer(graph, go, inputCount);
        //return ScriptPlayable<AICommandMixerBehaviour>.Create(graph, inputCount);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public enum EClipType
{
    Event_Default = 0,
    Event_Click = 1,
    Event_Speak = 2,    // 语音麦克风
    Event_NextTimeline = 3, // 切换timeline
    Event_SelectCJFX = 4,   // 选择超级飞侠
    Event_Custom = 5,   // 创建一个自定义对象
    Event_Effect = 6,   // 特效
}
[System.Serializable]
public class ReceiverPlayableAsset : PlayableAsset
{
    public EClipType clipType;

    public HeroSelectManager.EHeroType heroType;
    public bool isPauseTimeline;
    public bool isResumeTimeline;
    public string eventID;
    public string nextTimelineID;
    public string audioName;
    public string effectPath;
    public string effectParent;
    public Vector3 clickPosition;
    public ReceiverBaseMono receiverMono;

    [HideInInspector]
    public GameObject trackBinding;

    // Factory method that generates a playable based on this asset
    public override Playable CreatePlayable(PlayableGraph graph, GameObject go) {

        //Get access to the Playable Behaviour script
        ReceiverPlayableBehaviour playableBehaviour = new ReceiverPlayableBehaviour();
        PlayableDirector director = graph.GetResolver() as PlayableDirector;
        playableBehaviour.Initialize(director, trackBinding);
        playableBehaviour.clipType = clipType;
        playableBehaviour.isPauseTimeline = isPauseTimeline;
        playableBehaviour.isResumeTimeline = isResumeTimeline;
        playableBehaviour.eventID = eventID;
        playableBehaviour.nextTimelineID = nextTimelineID;
        playableBehaviour.audioName = audioName;
        playableBehaviour.effectPath = effectPath;
        playableBehaviour.effectParent = effectParent;
        playableBehaviour.clickPosition = clickPosition;
        playableBehaviour.receiverMono = receiverMono;
        playableBehaviour.heroType = heroType;

        //Create a custom playable from this script using the Player Behaviour script
        return ScriptPlayable<ReceiverPlayableBehaviour>.Create(graph, playableBehaviour);
	}
    
}

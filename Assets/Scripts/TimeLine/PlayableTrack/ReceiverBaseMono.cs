﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiverBaseMono : MonoBehaviour, IEasyTimer, IEasyAudio
{
    public bool bTimerCallback
    {
        get { return this == null ? false : true; }
    }

    private string _TimelineID;
    public string TimelineID
    {
        get { return _TimelineID; }
    }

    public void Initialize(string id)
    {
        _TimelineID = id;
        Initialize();
    }

    public void OnClick()
    {
        OnGameStart();
    }

    protected virtual void Initialize()
    {

    }

    protected virtual void OnGameStart() { }

    protected virtual void OnDestroy()
    {

    }

    protected void PauseTimeline()
    {
        FTimelineDispatch.PauseTimeline(_TimelineID);
    }

    protected void ResumeTimeline()
    {
        FTimelineDispatch.ResumeTimeline(_TimelineID);
    }



}

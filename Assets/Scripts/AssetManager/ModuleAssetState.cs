﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ModuleAssetState {
    NoDownload,
    Downloading,
    NeedUpdate,
    Downloaded,
}

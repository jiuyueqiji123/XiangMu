﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ModuleConst {





    // 冒险喜马拉雅
	public const string StoryHimalayaName = "story_himalaya";
    public const int StoryHimalayaCodeVersion = 1;
	public const int StoryHimalayaResVersion = 1;

    // 冒险马尔代夫
    public const string StoryMaerdaifuName = "story_maerdaifu";
    public const int StoryMaerdaifuCodeVersion = 1;
    public const int StoryMaerdaifuResVersion = 1;

    // 冒险丹麦恐龙
    public const string StoryDanMaiName = "story_danmai";
    public const int StoryDanMaiCodeVersion = 1;
    public const int StoryDanMaiResVersion = 1;





    // 基地-拼图
    public const string GamePuzzleName = "game_puzzle";
    public const int GamePuzzleCodeVersion = 1;
    public const int GamePuzzleResVersion = 1;

    // 基地-蛋糕
    public const string GameCakeName = "game_cake";
    public const int GameCakeCodeVersion = 1;
    public const int GameCakeResVersion = 1;

    // 基地-水果
    public const string GameFruitName = "game_fruit";
    public const int GameFruitCodeVersion = 1;
    public const int GameFruitResVersion = 1;

    // 基地-超市
    public const string GameSuperName = "super_market";
    public const int GameSuperCodeVersion = 1;
    public const int GameSuperResVersion = 1;

    // 基地-中餐厅
    public const string GameRestaurantName = "game_restaurant_zh";
    public const int GameRestaurantCodeVersion = 1;
    public const int GameRestaurantResVersion = 1;

    // 基地-医院
    public const string GameHospitalName = "game_hospital";
    public const int GameHospitalCodeVersion = 1;
    public const int GameHospitalResVersion = 1;





    // 数学-图形认知
    public const string StudyGeometryName = "study_geometry";
    public const int StudyGeometryCodeVersion = 1;
    public const int StudyGeometryResVersion = 1;

    // 数学-数学加法
    public const string StudyAddName = "study_addition";
    public const int StudyAddCodeVersion = 1;
    public const int StudyAddResVersion = 1;

    // 数学-认识数字
    public const string StudyMathName = "study_math";
    public const int StudyMathCodeVersion = 1;
    public const int StudyMathResVersion = 1;

    // 数学-数学减法
    public const string StudySubName = "study_subtraction";
    public const int StudySubCodeVersion = 1;
    public const int StudySubResVersion = 1;

    // 数学-逻辑矩阵
    public const string StudyMatrixName = "study_matrix";
    public const int StudyMatrixCodeVersion = 1;
    public const int StudyMatrixResVersion = 1;

    // 数学-思维训练
    public const string StudyCardGameName = "study_cardgame";   // 思维训练
    public const int StudyCardGameCodeVersion = 1;
    public const int StudyCardGameResVersion = 1;




    // 英语-自然拼读
    public const string StudyPhonicsName = "study_phonics";
    public const int StudyPhonicsCodeVersion = 1;
    public const int StudyPhonicsResVersion = 1;

    // 英语-英语启蒙
    public const string StudyEnglishName = "study_english";
    public const int StudyEnglishCodeVersion = 1;
    public const int StudyEnglishResVersion = 1;

    // 英语-英语颜色
    public const string StudyEnglishColorName = "study_english_color";
    public const int StudyEnglishColorCodeVersion = 1;
    public const int StudyEnglishColorResVersion = 1;




    // 主界面-主控台
    public const string MainGameName = "game_toybox";
    public const int MainGameCodeVersion = 1;
    public const int MainGameResVersion = 1;




    // 快递
    public const string ExpressName = "express";
    public const int ExpressCodeVersion = 1;
    public const int ExpressResVersion = 1;

    // 画画
    public const string PaintName = "paint_asset";
    public const int PaintCodeVersion = 1;
    public const int PaintResVersion = 1;

    // 电台
    public const string RadioName = "radio";
    public const int RadioCodeVersion = 1;
    public const int RadioResVersion = 1;




    // 其它
    public const string OtherModelName = "model";
    public const int OtherModelCodeVersion = 1;
    public const int OtherModelResVersion = 1;

    public const string OtherPlayerAnimationName = "study_player_animation";
    public const int OtherPlayerAnimationCodeVersion = 1;
    public const int OtherPlayerAnimationResVersion = 1;

    public const string OtherTimelineName = "game_timeline";
    public const int OtherTimelineCodeVersion = 1;
    public const int OtherTimelineResVersion = 1;




    #region package-english

    public const string ARBaikeAnimalName = "baike_animal";
    public const int ARBaikeAnimalCodeVersion = 1;
    public const int ARBaikeAnimalResVersion = 1;

    public const string ARBaikeFruitsName = "baike_fruits";
    public const int ARBaikeFruitsCodeVersion = 1;
    public const int ARBaikeFruitsResVersion = 1;

    public const string ARBaikeSportsName = "baike_sprots";
    public const int ARBaikeSportsCodeVersion = 1;
    public const int ARBaikeSportsResVersion = 1;

    public const string ARBaikeTrafficName = "baike_traffic";
    public const int ARBaikeTrafficCodeVersion = 1;
    public const int ARBaikeTrafficResVersion = 1;

    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class DownloadItemData {
    public uint Id;
    public uint Type;
    public long Size;
    public long UseTime;
}

[Serializable]
public class DownloadItemList
{
    public List<DownloadItemData> List = new List<DownloadItemData>();
}

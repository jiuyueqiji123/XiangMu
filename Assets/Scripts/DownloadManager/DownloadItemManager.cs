﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using System;

public class DownloadItemManager : Singleton<DownloadItemManager> {

    private const string ListSaveKey = "DownloadItemManager.ListSaveKey";

    private Dictionary<emMainType, List<DownloadItemData>> downloadInfos = new Dictionary<emMainType, List<DownloadItemData>>();

    private DownloadItemList itemList;

    public override void Init()
    {
        base.Init();
        itemList = PlayerPrefsX.GetXml<DownloadItemList>(ListSaveKey, new DownloadItemList());
        foreach(DownloadItemData data in itemList.List)
        {
            AddToDict(data);
        }
    }

    private void AddToDict(DownloadItemData data)
    {
        emMainType type = (emMainType)(data.Type - 1);
        if (!downloadInfos.ContainsKey(type))
        {
            downloadInfos.Add(type, new List<DownloadItemData>());
        }
        downloadInfos[type].Add(data);
    }

    public void AddDownload(int id, long size)
    {
        MainItemInfo info = TableProtoLoader.MainItemInfoDict[(uint)id];
        emMainType type = (emMainType)(info.type - 1);
        DownloadItemData data = IsHasData(id);
        if (data != null)
        {
            data.Size = size;
            data.UseTime = TimeUtils.CurUnixTime;
        }
        else
        {
            data = new DownloadItemData()
            {
                Id = info.id,
                Type = info.type,
                Size = size,
                UseTime = TimeUtils.CurUnixTime,
            };
            AddToDict(data);
            itemList.List.Add(data);
        }
        Save();
    }

    public void ReflashUseTime(int id)
    {
        DownloadItemData data = IsHasData(id);
        if (data != null)
        {
            data.UseTime = TimeUtils.CurUnixTime;
            Save();
        }
    }

    public void Save()
    {
        PlayerPrefsX.SetXml<DownloadItemList>(ListSaveKey, itemList);
    }

    private DownloadItemData IsHasData(int id)
    {
        foreach (DownloadItemData data in itemList.List)
        {
            if (data.Id == id)
            {
                return data;
            }
        }
        return null;
    }

    public List<DownloadItemData> Items(emMainType type)
    {
        if (downloadInfos.ContainsKey(type))
        {
            return downloadInfos[type];
        }
        else
        {
            return new List<DownloadItemData>();
        }
    }

    public void OrderItemsByTime()
    {
        foreach(List<DownloadItemData> items in downloadInfos.Values)
        {
            items.Sort(delegate (DownloadItemData x, DownloadItemData y)
            {
                return y.UseTime.CompareTo(x.UseTime);
            });
        }
    }

    public void OrderItemsBySize()
    {
        foreach (List<DownloadItemData> items in downloadInfos.Values)
        {
            items.Sort(delegate (DownloadItemData x, DownloadItemData y)
            {
                return y.Size.CompareTo(x.Size);
            });
        }
    }

    public void DelDownloadItem( DownloadItemData data)
    {
        itemList.List.Remove(data);
        downloadInfos[(emMainType)(data.Type - 1)].Remove(data);
        ModuleAssetManager.Instance.DeleteRes((int)data.Id);
    }

 
}

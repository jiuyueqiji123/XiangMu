﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[GameState]
public class AdditionState : BaseState {

    public override void OnStateEnter()
    {
        base.OnStateEnter();
        UISpriteManager.Instance.LoadAtlas(emUIAltas.AdditionAtlas);
        SceneLoader.Instance.LoadScene(SceneLoader.AdditionScene, OnLoadComplete);
    }

    private void OnLoadComplete()
    {
        AdditionController.Instance.OpenView();
    }

    public override void OnStateLeave()
    {
        base.OnStateLeave();
        AdditionController.Instance.CloseView();
    }
}

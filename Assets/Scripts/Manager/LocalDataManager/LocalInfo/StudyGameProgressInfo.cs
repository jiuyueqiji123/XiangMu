﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StudyGameProgressInfo
{

    // 游戏进度 （0 是一个未完成， 1 是第一个游戏已经完成）
    public int gameProgress = 0;
	

}

public class StudyEnglishProgressInfo
{
    // 26 个字母进度（0 - 25）
    public int letterProgress = 0;

    // 四个小游戏的进度 （0 - 4）0未通关一个
    public int letterGameProgress = 0;

    public int adsCount = 0;    // 英语启蒙 D字母广告次数
    public int zrpd_adsCount = 0;  // 自然拼读 D字母广告次数

}

public class MainTipInfo
{
    public List<uint> itemList = new List<uint>(); // 保存的新功能标识ID
}

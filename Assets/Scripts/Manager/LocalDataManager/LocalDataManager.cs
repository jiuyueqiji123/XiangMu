﻿/*----------------------------------------------------------------
// Copyright (C) 2018 
// author：meng cheng xin
// data：2018/4/23
// describe： 本地数据管理类，管理保存本地临时创建的数据，方便下次读取加载
//----------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public enum ELocalDataType
{
    StudyMathGame,  //认识数字游戏数据
    StudyGeometryGame,  //图形认知游戏数据
    StudyEnglishGame,  //字母认知游戏数据
    StudyEnglishColours,  //英语颜色游戏数据
}
public class LocalDataManager : Singleton<LocalDataManager>
{

    //private Dictionary<string, string> m_dataDic = new Dictionary<string, string>();


    public override void Init()
    {
        base.Init();

    }


    public void Save(ELocalDataType type, object data)
    {
        string jsonData = JsonConvert.SerializeObject(data);
        SaveDataToLocal(type.ToString(), jsonData);
    }

    public void Save(string key, string data)
    {
        SaveDataToLocal(key, data);
    }

    public void Save<T>(object data)
    {
        string jsonData = JsonConvert.SerializeObject(data);
        SaveDataToLocal(typeof(T).Name, jsonData);
    }

    public string Load(string key)
    {
        return LoadDataFromLocal(key);
    }

    public T Load<T>(string key) where T : new()
    {
        string jsonData = LoadDataFromLocal(key);
        if (string.IsNullOrEmpty(jsonData))
            return new T();
        T t = JsonConvert.DeserializeObject<T>(jsonData);
        return t;
    }

    public T Load<T>(ELocalDataType type) where T : new()
    {
        string jsonData = LoadDataFromLocal(type.ToString());
        if (string.IsNullOrEmpty(jsonData))
            return new T();
        T t = JsonConvert.DeserializeObject<T>(jsonData);
        return t;
    }

    public T Load<T>() where T : new()
    {
        string jsonData = LoadDataFromLocal(typeof(T).Name);
        if (string.IsNullOrEmpty(jsonData))
            return new T();
        T t = JsonConvert.DeserializeObject<T>(jsonData);
        return t;
    }

    private string LoadDataFromLocal(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            // 临时用这个类保存，后续改为写入本地数据
            return PlayerPrefs.GetString(key);
        }
        return string.Empty;
    }

    private void SaveDataToLocal(string key, string data)
    {
        PlayerPrefs.SetString(key, data);
    }

}

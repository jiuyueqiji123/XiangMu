﻿/*----------------------------------------------------------------
// Copyright (C) 2018 
// author：meng cheng xin
// data：2018/4/23
// describe： 无操作时间统计
//----------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerInfo
{
    public TimerInfo(int _ID, float _Time, bool _Loop, System.Action<int> _Callback)
    {
        this._ID = _ID;
        this.eventTime = _Time;
        this.loop = _Loop;
        this.callback = _Callback;

        _IsComplete = false;
        timeSign = Time.time;
    }

    private int _ID;
    public int ID { get { return _ID; } }
    private bool _IsComplete;
    public bool IsComplete { get { return _IsComplete; } }

    private float eventTime;
    public float EventTime { get { return eventTime; } }
    private bool loop;
    private System.Action<int> callback;

    private float timeSign;


    public void OnUpdate(float deltaTime)
    {
        float intervalTime = Time.time - timeSign;
        if (intervalTime > eventTime)
        {
            if (callback != null)
            {
                callback(_ID);
            }
            timeSign = Time.time + intervalTime - eventTime;
            if (!loop)
                _IsComplete = true;
        }
    }

    public void ResetTime()
    {
        timeSign = Time.time;
    }

}

public class InputTimerManager : MonoSingleton<InputTimerManager>
{


    private List<TimerInfo> m_timers = new List<TimerInfo>();
    private List<TimerInfo> m_noInputTimers = new List<TimerInfo>();

    private int m_timerSequence;

    protected override void Init()
    {
        base.Init();

        m_timerSequence = 0;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        RemoveAllTimerNoInput();
    }

    void Update () {

        if (m_noInputTimers.Count > 0)
        {
            if (Input.GetMouseButton(0))
            {
                foreach (TimerInfo info in m_noInputTimers)
                {
                    info.ResetTime();
                }
            }
            int i = 0;
            while (i < m_noInputTimers.Count)
            {
                TimerInfo info = m_noInputTimers[i];
                if (info.IsComplete)
                {
                    m_noInputTimers.RemoveAt(i);
                }
                else
                {
                    info.OnUpdate(Time.deltaTime);
                    i++;
                }
            }
        }
        if (m_timers.Count > 0)
        {
            int i = 0;
            while (i < m_timers.Count)
            {
                TimerInfo info = m_timers[i];
                if (info.IsComplete)
                {
                    m_timers.RemoveAt(i);
                }
                else
                {
                    info.OnUpdate(Time.deltaTime);
                    i++;
                }
            }
        }

    }

    /// <summary>
    /// 添加 无操作时间统计事件
    /// </summary>
    /// <param name="intervalTime">间隔时间</param>
    /// <param name="loop">是否循环</param>
    /// <param name="callback">事件回调</param>
    public int AddTimerNoInput(float time, bool loop, System.Action<int> callback)
    {
        m_timerSequence++;
        m_noInputTimers.Add(new TimerInfo(m_timerSequence, time, loop, callback));
        return m_timerSequence;
    }

    public int AddTimer(float time, bool loop, System.Action<int> callback)
    {
        m_timerSequence++;
        m_timers.Add(new TimerInfo(m_timerSequence, time, loop, callback));
        return m_timerSequence;
    }

    public void RemoveTimerNoInput(int sequence)
    {
        for (int j = 0; j < m_noInputTimers.Count; j++)
        {
            if (m_noInputTimers[j].ID == sequence)
            {
                m_noInputTimers.RemoveAt(j);
                break;
            }
        }
    }

    public void RemoveTimer(int sequence)
    {
        for (int j = 0; j < m_timers.Count; j++)
        {
            if (m_timers[j].ID == sequence)
            {
                m_timers.RemoveAt(j);
                break;
            }
        }
    }

    public void RemoveAllTimerNoInput()
    {
        this.m_noInputTimers.Clear();
    }

    public void RemoveAllTimer()
    {
        this.m_timers.Clear();
    }



}

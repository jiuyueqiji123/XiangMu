﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


/// <summary>
/// 所有字段为 1 是开启，0 关闭
/// </summary>
public class AppConfigInfo
{
    public int logState;
}

public class AppConfig : MonoBehaviour
{



#if PACKAGE_ENGLISH
    public const string ServerURL = "http://page.soulgame.mobi/superwings-english";
    public const string MainURL = "http://page.soulgame.mobi/superwings-english/main/";
#else
    public const string ServerURL = "http://page.soulgame.mobi/superwings";
    public const string MainURL = "http://page.soulgame.mobi/superwings/main/";
#endif



    private const string appConfigPath = "/AppConfig.json";

    private static AppConfigInfo _appConfig;

    private void Start()
    {
        StartCoroutine(Init());   
    }

    IEnumerator Init()
    {
        string path = Application.streamingAssetsPath + appConfigPath;
        using (UnityWebRequest request = new UnityWebRequest(path) { downloadHandler = new DownloadHandlerBuffer() })
        {
            yield return request.SendWebRequest();

            if (!request.isHttpError)
            {
                Debug.Log("---------------- load appconfig successful!");
                _appConfig = JsonUtility.FromJson<AppConfigInfo>(request.downloadHandler.text);
            }
            else
            {
                Debug.LogError("---- UnityWebRequest URL:" + path + "  -----Error:" + request.error);
                _appConfig = new AppConfigInfo();
#if UNITY_EDITOR
                System.IO.File.WriteAllText(path, JsonUtility.ToJson(_appConfig));
#endif
            }

        }


#if UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
#else
        Debug.unityLogger.logEnabled = _appConfig.logState == 1;
#endif

    }


}

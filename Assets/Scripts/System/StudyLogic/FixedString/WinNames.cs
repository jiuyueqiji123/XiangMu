﻿
public class WinNames
{

    // common

    public const string StoryMicroPanel = "common_asset/common_asset_ugui_window/StoryMicroPanel";  // 冒险故事麦克风
    public const string MedalPanel = "common_asset/common_asset_ugui_window/MedalPanel";            // 勋章
    public const string EnglishMioPanel = "common_asset/common_asset_ugui_window/EnglishMioPanel";   // 公众号
    public const string AdsTipsWindow = "common_asset/common_asset_ugui_window/AdsTipsWindow";   // IOS 广告弹窗提醒


    // english 

    public const string EnglishPanel = "study_english/study_english_ugui_panel/EnglishPanel";
    public const string DrawLetterPanel = "study_english/study_english_ugui_panel/DrawLetterPanel";
    public const string ReadLetterPanel = "study_english/study_english_ugui_panel/ReadLetterPanel";
    public const string LetterAnimationPanel = "study_english/study_english_ugui_letteranimation/LetterAnimationPanel";
    public const string EnglishSelectGamePanel = "study_english/study_english_ugui_panel/EnglishSelectGamePanel";
    public const string EnglishTVPanel = "study_english/study_english_ugui_panel/EnglishTVPanel";
    public const string EnglishSelectLetterPanel = "study_english/study_english_ugui_panel/EnglishSelectLetterPanel";
    public const string EnglishLediAnimationPanel = "study_english/study_english_ugui_panel/EnglishLediAnimationPanel";
    public const string EnglishKCJSPanel = "study_english/study_english_ugui_panel/EnglishKCJSPanel";

    // english color
    public const string EnglishColorMainWindow = "study_english_color/study_english_color_ugui_window/EnglishColorMainWindow";
    public const string FillColorGameWindow = "study_english_color/study_english_color_ugui_window/FillColorGameWindow";
    public const string FillLetterGameWindow = "study_english_color/study_english_color_ugui_window/FillLetterGameWindow";
    public const string FindHouseGameWindow = "study_english_color/study_english_color_ugui_window/FindHouseGameWindow";

    // study_phonics
    public const string EnglishZRPDKCJSPanel = "study_phonics/study_phonics_ugui_panel/EnglishZRPDKCJSPanel";

    // math

    public const string MathNumberPanel = "study_math/study_math_ugui_panel/MathNumberPanel";
    public const string DrawNumberPanel = "study_math/study_math_ugui_panel/DrawNumberPanel";
    public const string CardGamePanel = "study_cardgame/study_cardgame_ui/CardGamePanel";
    //addplus

    // geometry

    public const string DrawGeometryPanel = "study_geometry/study_geometry_ugui_panel/DrawGeometryPanel";

    // 快递

    public const string GallaryPanel = "express/express_ui/GallaryPanel";
    public const string StartPanel = "express/express_ui/StartPanel";
    public const string PhotoShowPanel = "express/express_ui/PhotoShowPanel";
    public const string PowerBuyPanel = "express/express_ui/PowerBuyPanel";
    public const string WarningPanel = "express/express_ui/WarningPanel";
    public const string GallayPictureShowPanel = "express/express_ui/GallayPictureShowPanel";


    //医院
    public const string ClinicRoomPanel = "game_hospital/game_hospital_ugui_panel/ClinicRoomPanel";
    public const string StomachachePanel = "game_hospital/game_hospital_ugui_panel/StomachachePanel";
    public const string VaccinePanel = "game_hospital/game_hospital_ugui_panel/VaccinePanel";
    public const string DispenPanel = "game_hospital/game_hospital_ugui_panel/DispenPanel";
    public const string LowFeventPanel = "game_hospital/game_hospital_ugui_panel/LowFevelPanel";
    public const string ToothachePanle = "game_hospital/game_hospital_ugui_panel/ToothachePanel";
    public const string InjectionPanle = "game_hospital/game_hospital_ugui_panel/InjectionPanel";
    public const string HospitalMicroPanel = "game_hospital/game_hospital_ugui_panel/HospitalMicroPanel";


    // 冒险

    public const string StoryPanel = "story_danmai/story_danmai_ugui_panel/StoryPanel";
    public const string StoryPuzzlePanel = "story_danmai/story_danmai_ugui_panel/StoryPuzzlePanel"; 
    public const string StoryDMKLPKPanel = "story_danmai/story_danmai_ugui_panel/StoryDMKLPKPanel";


    public const string StoryFlyGamePanel = "story_monage/story_monage_ui_panel/FlyGamePanel";

    //matrix
    public const string MatrixLevelPanel = "study_matrix/study_matrix_prefab/MatrixLevelPanel";
    public const string MatrixInfoPanel = "study_matrix/study_matrix_prefab/MatrixInfoPanel";

    //小游戏
    public const string GuessPanel = "game_toybox/game_toybox_ui/FingerGuessPanel";
    public const string DinosaurPkPanel = "game_toybox/game_toybox_ui/DinosaurPkPanel";
    public const string LediCleanPanel = "game_toybox/game_toybox_ui/LediCleanPanel";





    #region package-english

    public const string EnglishMainWindow = "package_english/package_english_ugui_window/EnglishMainWindow";
    public const string SettingWindow = "package_english/package_english_ugui_window/SettingWindow";
    public const string VersionUpdateWindow = "package_english/package_english_ugui_window/VersionUpdateWindow";
    public const string TipWindow = "package_english/package_english_ugui_window/TipWindow";

    //ar百科
    public const string ARBaikeMainPage = "package_english/package_english_arbaike_ui/ARbaikeMainPage";
    public const string ARBaikeDownloadPage = "package_english/package_english_arbaike_ui/ARBaikeDownloadPage";
    public const string ARBaikeLoadingPage = "package_english/package_english_arbaike_ui/ARBaikeLoadingPage";
    public const string ARBaikeWaitPage = "package_english/package_english_arbaike_ui/ARBaikeWaitPage";
    public const string ARbaikeIndroducePage = "package_english/package_english_arbaike_ui/ARbaikeIndroducePage";
    public const string ARbaikeUnlockPage = "package_english/package_english_arbaike_ui/ARbaikeUnlockPage";
    public const string ARHandPoseWindow = "package_english/package_english_arbaike_ui/ARHandPoseWindow";
    public const string CircleProgressWindow = "package_english/package_english_arbaike_ui/CircleProgressWindow";
    public const string ARBaikeSharePage = "package_english/package_english_arbaike_ui/ARBaikeSharePage";
    public const string ARBaikeUnlockPageSingle = "package_english/package_english_arbaike_ui/ARBaikeUnlockPageSingle";
    public const string ARbaikeIndroducePageSingle = "package_english/package_english_arbaike_ui/ARbaikeIndroducePageSingle";

    #endregion


    #region package-math

    public const string MathMainWindow = "package_math/package_math_ui/MathMainWindow";

    #endregion





}

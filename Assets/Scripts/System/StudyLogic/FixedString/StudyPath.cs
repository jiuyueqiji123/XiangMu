﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StudyPath
{
    public const float ENGLISH_ONINPUT_TIME = 60;  // 英语1分钟无操作时间提示



    // common asset path

    public const string english_daoju_sprite_path = "common_asset/english_daoju_sprite/";  // UI资源路径


    // ugui asset path

    public const string math_drawnumberBasePath = "study_math/study_math_ugui_numbers/";  // 画数字资源路径
    public const string math_drawletterBasePath = "study_english/study_english_ugui_letters/";  // 画字母资源路径
    public const string math_drawgeometryBasePath = "study_geometry/study_geometry_ugui_getmetry/";  // 画图形资源路径

    // 乐学  - 英语 字母认知 ---------------------------------------


    public const string english_audio_basepath = "study_english/study_english_audio/";  //英语音频存放路径
    public const string english_select_letter_texture_basepath = "study_english/study_english_selectletter_textures/303010503_";  //英语字母贴图路径  如：303010503_A
    public const string english_bigandsmall_letter_basepath = "study_english/study_english_sprite_letter/";  //大小写字母的资源路径
    public const string english_selectgame_letter_basepath = "study_english/study_english_sprite_selectgame_letter/";  //游戏选择界面 UI字母资源
    public const string english_sprite_path = "study_english/study_english_sprite/";  // UI资源路径
    public const string english_sprite_picturebook = "study_english/study_english_sprite_picturebook/";  // 绘本用的UI资源

    public const string english_select_letter_scene = "study_english/study_english_scene/303010500_02";   // 字母选择场景
    public const string english_lawn_scene = "study_english/study_english_scene/303010100";  // 草地场景
    public const string english_train_scene = "study_english/study_english_scene/303010200";   // 火车跑道场景
    public const string english_sea_scene = "study_english/study_english_scene/303010300";  // 海底场景-鱼泡
    public const string english_sea_box_scene = "study_english/study_english_scene/303010300_01";  // 海底场景-宝箱
    public const string english_paddyfield_scene = "study_english/study_english_scene/303010400";  // 麦田场景-稻草人
    public const string english_paddyfield_caoduo_scene = "study_english/study_english_scene/303010400_01";  // 麦田场景-草垛
    public const string english_snow_scene = "study_english/study_english_scene/303010500";  // 雪地场景-礼物盒子
    public const string english_snow_man_scene = "study_english/study_english_scene/303010500_01";  // 雪地场景-雪人


    public const string english_ui_scene_lawn = "study_english/study_english_scene/ui_scene_lawn";      // 画字母场景
    public const string english_ui_scene_paddyfield = "study_english/study_english_scene/ui_scene_paddyfield";      // 画字母场景
    public const string english_ui_scene_sea = "study_english/study_english_scene/ui_scene_sea";      // 画字母场景
    public const string english_ui_scene_snow = "study_english/study_english_scene/ui_scene_snow";      // 画字母场景

    public const string english_select_letter = "study_english/study_english_prefab_select_scene/303010503";   // 字母选择

    public const string english_lawn_package_green = "study_english/study_english_prefab_lawn/Pack_Green";  // 火车跑道资源 包裹
    public const string english_lawn_package_orange = "study_english/study_english_prefab_lawn/Pack_Orange";  // 火车跑道资源 包裹
    public const string english_lawn_package_yellow = "study_english/study_english_prefab_lawn/Pack_Yellow";  // 火车跑道资源 包裹

    public const string english_sea_paopao = "study_english/study_english_prefab_sea/303010301";  // 鱼泡
    public const string english_sea_fish = "study_english/study_english_prefab_sea/303010302";  // 大鱼
    public const string english_sea_baoxiang = "study_english/study_english_prefab_sea/303010303";  // 宝箱

    public const string english_lawn_balloon_blue = "study_english/study_english_prefab_lawn/303010101_blue";    // 气球
    public const string english_lawn_balloon_orange = "study_english/study_english_prefab_lawn/303010101_orange";  // 气球
    public const string english_lawn_balloon_red = "study_english/study_english_prefab_lawn/303010101_red";  // 气球

    public const string english_snow_box_green = "study_english/study_english_prefab_snow/303010501_green";    // 礼物盒子
    public const string english_snow_box_orange = "study_english/study_english_prefab_snow/303010501_orange";  // 礼物盒子
    public const string english_snow_box_purple = "study_english/study_english_prefab_snow/303010501_purple";  // 礼物盒子
    public const string english_snow_snowman = "study_english/study_english_prefab_snow/303010502";  // 雪人

    public const string english_paddyfield_scarecrow = "study_english/study_english_prefab_paddyfield/303010401";  // 稻草人
    public const string english_paddyfield_hayrick = "study_english/study_english_prefab_paddyfield/303010402";  // 草垛

    // =============================================================








    // 乐学  - 数学 认识数字 ---------------------------------------

    public const string number_audio_basepath = "study_math/study_math_audio/";  //数学音频存放路径


    public const string math_scene_303020000 = "study_math/study_math_scene/303020000";  // 选择火车数字的场景
    public const string math_selecttrain_scene = "study_math/study_math_scene/303020100";  // 选择火车数字的场景
    public const string math_drawtrain_scene = "study_math/study_math_scene/303020200";  // 画火车数字的场景
    public const string math_selectball_scene = "study_math/study_math_scene/303020400";  // 拉球的火车场景
    public const string math_dragtraintrack_scene = "study_math/study_math_scene/303020500";    //拖枕木场景

    public const string math_trainHeadPathNoAnim = "study_math/study_math_prefab_number/303020101"; // 火车头 无动画
    public const string math_trainHeadPath = "study_math/study_math_prefab_number/303020101_anim"; // 火车头
    public const string math_trainNodeBluePath = "study_math/study_math_prefab_number/303020202_blue_anim"; // 蓝色车厢
    public const string math_trainNodeBlue_Line_Path = "study_math/study_math_prefab_number/303020202_blue_anim_line"; // 蓝色虚线车厢
    public const string math_trainNodeGreenPath = "study_math/study_math_prefab_number/303020202_green_anim"; // 绿色车厢
    public const string math_zhenmuPath = "study_math/study_math_prefab_number/303020501";    // 枕木


    // =============================================================







    // 乐学  - 数学 图形认知 ---------------------------------------

    public const string s_g_path = "study_geometry/";  //数学 图形认知路径
    public const string study_geometry_audio_basepath = s_g_path + "study_geometry_audio/";  //数学图形认知音频存放路径

    public const string geometry_scene_303040000 = s_g_path + "study_geometry_scene/303040000";  //图形认知 游戏选择场景
    public const string geometry_notrack_scene = s_g_path + "study_geometry_scene/303040100";  //图形认知 无铁轨场景
    public const string geometry_track_scene = s_g_path + "study_geometry_scene/303040400";  //图形认知 铁轨场景 找图形
    public const string geometry_track_1_scene = s_g_path + "study_geometry_scene/303040500";  //图形认知 铁轨场景 拼动物

    public const string geometry_animals_point = s_g_path + "study_geometry_scene/303040100_Animals";  //动物坐标
    public const string geometry_point = s_g_path + "study_geometry_scene/303040100_Geometry";  //图形坐标
    public const string geometry_track_findbricks = s_g_path + "study_geometry_scene/303040400_FindBricks";  //辅助物体 找积木场景
    public const string geometry_track_findanimals = s_g_path + "study_geometry_scene/303040500_FindAnimals";  //辅助物体 拼动物场景

    public const string geometry_animal_broken_ji = s_g_path + "study_geometry_prefab_animalbroken/303040101";  //鸡
    public const string geometry_animal_broken_mao = s_g_path + "study_geometry_prefab_animalbroken/303040102";  //鸡
    public const string geometry_animal_broken_tuzi = s_g_path + "study_geometry_prefab_animalbroken/303040103";  //鸡
    public const string geometry_animal_broken_niu = s_g_path + "study_geometry_prefab_animalbroken/303040104";  //鸡
    public const string geometry_animal_broken_zhu = s_g_path + "study_geometry_prefab_animalbroken/303040105";  //鸡
    public const string geometry_animal_broken_gou = s_g_path + "study_geometry_prefab_animalbroken/303040106";  //鸡

    public const string geometry_animal_ji = s_g_path + "study_geometry_prefab_animal/303040101";  //鸡
    public const string geometry_animal_mao = s_g_path + "study_geometry_prefab_animal/303040102";  //鸡
    public const string geometry_animal_tuzi = s_g_path + "study_geometry_prefab_animal/303040103";  //鸡
    public const string geometry_animal_niu = s_g_path + "study_geometry_prefab_animal/303040104";  //鸡
    public const string geometry_animal_zhu = s_g_path + "study_geometry_prefab_animal/303040105";  //鸡
    public const string geometry_animal_gou = s_g_path + "study_geometry_prefab_animal/303040106";  //鸡

    public const string geometry_circle_1 = s_g_path + "study_geometry_prefab/303040201_1";  //圆形 green
    public const string geometry_circle_2 = s_g_path + "study_geometry_prefab/303040201_2";  //圆形 orange
    public const string geometry_circle_3 = s_g_path + "study_geometry_prefab/303040201_3";  //圆形 pink 粉红色
    public const string geometry_circle_4 = s_g_path + "study_geometry_prefab/303040201_4";  //圆形 red
    public const string geometry_circle_5 = s_g_path + "study_geometry_prefab/303040201_5";  //圆形 yellow
    public const string geometry_circle_6 = s_g_path + "study_geometry_prefab/303040201_6";  //圆形 grey
    public const string geometry_circle_7 = s_g_path + "study_geometry_prefab/303040201_7";  //圆形 blue

    public const string geometry_square_1 = s_g_path + "study_geometry_prefab/303040202_1";  //正方形 dark blue
    public const string geometry_square_2 = s_g_path + "study_geometry_prefab/303040202_2";  //正方形 lake blue
    public const string geometry_square_3 = s_g_path + "study_geometry_prefab/303040202_3";  //正方形 orange
    public const string geometry_square_4 = s_g_path + "study_geometry_prefab/303040202_4";  //正方形 purple
    public const string geometry_square_5 = s_g_path + "study_geometry_prefab/303040202_5";  //正方形 yellow
    public const string geometry_square_6 = s_g_path + "study_geometry_prefab/303040202_6";  //正方形 pink

    public const string geometry_triangle_1 = s_g_path + "study_geometry_prefab/303040203_1";  //等边三角形 green
    public const string geometry_triangle_2 = s_g_path + "study_geometry_prefab/303040203_2";  //等边三角形 lake blue
    public const string geometry_triangle_3 = s_g_path + "study_geometry_prefab/303040203_3";  //等边三角形 black
    public const string geometry_triangle_4 = s_g_path + "study_geometry_prefab/303040203_4";  //等边三角形 red
    public const string geometry_isoscelesTriangle_1 = s_g_path + "study_geometry_prefab/303040401_1";  //等腰三角形 lake blue
    public const string geometry_isoscelesTriangle_2 = s_g_path + "study_geometry_prefab/303040401_2";  //等腰三角形 pink
    public const string geometry_rightTriangle_1 = s_g_path + "study_geometry_prefab/303040402_1";  //直角三角形 blue purple
    public const string geometry_rightTriangle_2 = s_g_path + "study_geometry_prefab/303040402_2";  //直角三角形 purple
    public const string geometry_rightTriangle_3 = s_g_path + "study_geometry_prefab/303040402_3";  //直角三角形 orange
    public const string geometry_rightTriangle_4 = s_g_path + "study_geometry_prefab/303040402_4";  //直角三角形 light brown

    public const string geometry_rectangle_1 = s_g_path + "study_geometry_prefab/303040204_1";  //长方形 grass green
    public const string geometry_rectangle_2 = s_g_path + "study_geometry_prefab/303040204_2";  //长方形 orange
    public const string geometry_rectangle_3 = s_g_path + "study_geometry_prefab/303040204_3";  //长方形 pink
    public const string geometry_rectangle_4 = s_g_path + "study_geometry_prefab/303040204_4";  //长方形 purple
    public const string geometry_rectangle_5 = s_g_path + "study_geometry_prefab/303040204_5";  //长方形 yellow
    public const string geometry_rectangle_6 = s_g_path + "study_geometry_prefab/303040204_6";  //长方形 black
    public const string geometry_rectangle_7 = s_g_path + "study_geometry_prefab/303040204_7";  //长方形 blue
    public const string geometry_rectangle_8 = s_g_path + "study_geometry_prefab/303040204_8";  //长方形 brown

    public const string geometry_ellipse_1 = s_g_path + "study_geometry_prefab/303040206_1";  //椭圆 grass green
    public const string geometry_ellipse_2 = s_g_path + "study_geometry_prefab/303040206_2";  //椭圆 light orange
    public const string geometry_ellipse_3 = s_g_path + "study_geometry_prefab/303040206_3";  //椭圆 pink
    public const string geometry_ellipse_4 = s_g_path + "study_geometry_prefab/303040206_4";  //椭圆 purple
    public const string geometry_ellipse_5 = s_g_path + "study_geometry_prefab/303040206_5";  //椭圆 rose red
    public const string geometry_ellipse_6 = s_g_path + "study_geometry_prefab/303040206_6";  //椭圆 blue
    public const string geometry_ellipse_7 = s_g_path + "study_geometry_prefab/303040206_7";  //椭圆 brown

    public const string geometry_isoscelesTrapezoid_1 = s_g_path + "study_geometry_prefab/303040205_1";  //等腰梯形 pink
    public const string geometry_isoscelesTrapezoid_2 = s_g_path + "study_geometry_prefab/303040205_2";  //等腰梯形 brown
    public const string geometry_isoscelesTrapezoid_3 = s_g_path + "study_geometry_prefab/303040205_3";  //等腰梯形 red
    public const string geometry_trapezoid_1 = s_g_path + "study_geometry_prefab/303040403_1";  //梯形 blue
    public const string geometry_trapezoid_2 = s_g_path + "study_geometry_prefab/303040403_2";  //梯形 grass green
    public const string geometry_trapezoid_3 = s_g_path + "study_geometry_prefab/303040403_3";  //梯形 pink
    public const string geometry_trapezoid_4 = s_g_path + "study_geometry_prefab/303040404_1";  //直角梯形 purple
    public const string geometry_trapezoid_5 = s_g_path + "study_geometry_prefab/303040404_2";  //直角梯形 red
    public const string geometry_trapezoid_6 = s_g_path + "study_geometry_prefab/303040404_3";  //直角梯形 orange

    public const string geometry_ball = s_g_path + "study_geometry_prefab_learn/303040207";  //球
    public const string geometry_flag = s_g_path + "study_geometry_prefab_learn/303040208";  //球
    public const string geometry_door = s_g_path + "study_geometry_prefab_learn/303040209";  //球
    public const string geometry_fangkuai = s_g_path + "study_geometry_prefab_learn/303040210";  //球
    public const string geometry_ladder = s_g_path + "study_geometry_prefab_learn/303040211";  //球
    public const string geometry_rugby = s_g_path + "study_geometry_prefab_learn/303040212";  //球

    public const string geometry_train_head = s_g_path + "study_geometry_prefab_fixed/train_head";  //火车
    public const string geometry_train_node = s_g_path + "study_geometry_prefab_fixed/train_node";  //火车
    public const string geometry_flatcar = s_g_path + "study_geometry_prefab_fixed/303040501";  //火车
    public const string geometry_wanjuchengbao = s_g_path + "study_geometry_prefab_fixed/303040100_wangjuchengbao";  //火车


    // =============================================================




    // 特效 ---------------------------------------

    public const string effect_502003005 = "Effect/502003005"; // 撒花特效
    public const string effect_502002008 = "Effect/502002008"; // 风的特效
    public const string math_traineffect_hint = "Effect/502003001"; // 提示特效
    public const string math_traineffect_in = "Effect/502003006"; // 火车进入特效
    public const string math_traineffect_stop = "Effect/502003007"; // 火车刹车特效
    public const string math_traineffect_out = "Effect/502003008"; // 火车离开特效
    public const string math_traineffect_out_big = "Effect/502003008_1"; // 火车离开特效

    public const string xz_effect = "Effect/502003002"; // 勋章特效
    
    public const string effect_502003021 = "Effect/502003021"; // 书写完成后，粒子效果
    public const string effect_502003022 = "Effect/502003022"; // 鱼泡破掉（海底场景）
    public const string effect_502003023 = "Effect/502003023"; // 宝箱放大并且发亮 （海底场景）
    public const string effect_502003024 = "Effect/502003024"; // 盒子放大并且发亮（雪地场景）
    public const string effect_502003025 = "Effect/502003025"; // 点击雪人，雪人原地动
    public const string effect_502003026 = "Effect/502003026"; // 火车缓慢前进汽笛缩放并冒烟
    public const string effect_502003027 = "Effect/502003027"; // 图形进入虚框泡泡特效 自动吸附后出现泡泡并破裂
    public const string effect_502003028 = "Effect/502003028"; // 雪人身上掉下雪花片
    public const string effect_502003029 = "Effect/502003029"; // 稻草人身上掉下稻草
    public const string effect_502003030 = "Effect/502003030"; // 草垛掉下稻草
    public const string effect_502003031 = "Effect/502003031"; // 盒子弹跳
    public const string effect_502003035 = "Effect/502003035"; // 雪花屏闪烁的动画
    //public const string effect_502003033 = "Effect/502003033"; // 形似物体出现在遮罩页面
    public const string effect_502003066 = "Effect/502003066"; // 雪花飘落
    public const string effect_502003067 = "Effect/502003067"; // 点击空白位置出现字母特效
    public const string effect_502003068 = "Effect/502003068"; // 麦克风提示特效
    public const string effect_502004045 = "Effect/502004045"; // 闪光特效


    // =============================================================

    // 人物动画 ---------------------------------------

    public const string player_animation_scene_path = "study_player_animation/study_player_sceneroot/PlayerAnimationRoot";  // 人物动画场景root

    public const string player_ledi_path = "study_player_animation/study_player_prefab/Ledi_Study";  // 乐迪

    // =============================================================




    // 认识数字场景，相关贴图的Base路径
    public static readonly string studyMathTextureBasePath = "study_math/study_math_textures_number/";

    // 火车头 红色贴图
    public static readonly string trainHeadRedTexture = "study_math/study_math_textures_number/303020101_red";

    // 火车头 黄色贴图
    public static readonly string trainHeadYellowTexture = "study_math/study_math_textures_number/303020101_yellow";

    // 火车车厢数字贴图
    public static readonly List<string> trainNodeNumberNameList = new List<string>() {
            "303020202_num0",
            "303020202_num1",
            "303020202_num2",
            "303020202_num3",
            "303020202_num4",
            "303020202_num5",
            "303020202_num6",
            "303020202_num7",
            "303020202_num8",
            "303020202_num9",
            "303020202_num10"
        };

    // 火车头高亮贴图
    public static readonly List<string> trainHeadHighLightTextureNameList = new List<string>() {
            "null",
            "303020101_yellow_b_1",
            "303020101_red_b_2",
            "303020101_yellow_b_3",
            "303020101_red_b_4",
            "303020101_yellow_b_5",
            "303020101_red_b_6",
            "303020101_yellow_b_7",
            "303020101_red_b_8",
            "303020101_yellow_b_9",
            "303020101_red_b_10"
        };

    // 火车头虚线贴图
    public static readonly List<string> trainHeadUnHighLightTextureNameList = new List<string>() {
            "null",
            "303020101_yellow_a_1",
            "303020101_red_a_2",
            "303020101_yellow_a_3",
            "303020101_red_a_4",
            "303020101_yellow_a_5",
            "303020101_red_a_6",
            "303020101_yellow_a_7",
            "303020101_red_a_8",
            "303020101_yellow_a_9",
            "303020101_red_a_10"
        };


    /// <summary>
    /// 获得火车车厢的数字贴图路径
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public static string GetTrainNodeNumberTexturePath(int number)
    {
        return studyMathTextureBasePath + trainNodeNumberNameList[number];
    }

    /// <summary>
    /// 获得火车头的 高亮数字贴图
    /// </summary>
    /// <param name="number">加载数字1就传1</param>
    /// <returns></returns>
    public static string GetTrainHeadHighLightTexturePath(int number)
    {
        return studyMathTextureBasePath + trainHeadHighLightTextureNameList[number];
    }

    /// <summary>
    /// 获得火车头的 虚线数字贴图
    /// </summary>
    /// <param name="number">加载数字1就传1</param>
    /// <returns></returns>
    public static string GetTrainHeadUnHighLightTexturePath(int number)
    {
        return studyMathTextureBasePath + trainHeadUnHighLightTextureNameList[number];
    }


    // 图形认知贴图路径
    public static readonly string studyGeometryTextureBasePath = "study_geometry/study_geometry_textures/";

    public static string GetTrainHeadGeometryTexturePath(EGeometryEnum type)
    {
        switch (type)
        {
            case EGeometryEnum.Circle:
                return studyGeometryTextureBasePath + "303040213";
            case EGeometryEnum.Square:
                return studyGeometryTextureBasePath + "303040214";
            case EGeometryEnum.Triangle:
                return studyGeometryTextureBasePath + "303040215";
            case EGeometryEnum.Rectangle:
                return studyGeometryTextureBasePath + "303040216";
            case EGeometryEnum.Trapezoid:
                return studyGeometryTextureBasePath + "303040217";
            case EGeometryEnum.Ellipse:
                return studyGeometryTextureBasePath + "303040218";
            default:
                return studyGeometryTextureBasePath + "303040213";
        }
    }

}

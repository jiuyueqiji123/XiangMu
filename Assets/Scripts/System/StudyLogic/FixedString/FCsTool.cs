﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using System.IO;

public class FCsTool
{

    public static void CreateCs()
    {

        string txt = "using UnityEngine;" + "\n" + "\n" +
            "public class StudyAudioName" + "\n" +
            "{" + "\n" + "\n";

        txt += "// 认识数字-1 图形认知-2 字母认知-3 英语颜色-4 =========================== " + "\n" + "\n";

        foreach (AudioInfo info in TableProtoLoader.AudioInfoDict.Values)
        {
            string t = "    public const uint t_" + info.id + " = " + info.id + ";      // 音频：" + info.name + "  内容：" + info.audio_describe + "  描述：" + info.describe;
            txt += t + "\n";
        }

        //txt += "\n" + "\n" + "// 字母认知 =========================== " + "\n" + "\n";

        //foreach (AudioEnglishInfo info in TableProtoLoader.AudioEnglishInfoDict.Values)
        //{
        //    string t = "    public const uint t_" + info.id + " = " + info.id + ";      // 音频：" + info.name + "  内容：" + info.audio_describe + "  描述：" + info.describe;
        //    txt += t + "\n";
        //}

        txt += "\n" + "\n" + "}";


        File.WriteAllText(Application.dataPath.Replace("Assets", "StudyAudioName.cs"), txt);


        txt = "using UnityEngine;" + "\n" + "\n" +
            "public class ActionAudioName" + "\n" +
            "{" + "\n" + "\n";

        foreach (ActionAudioInfo info in TableProtoLoader.ActionAudioInfoDict.Values)
        {
            string t = "    public const uint t_" + info.id + " = " + info.id + ";      // 音频：" + info.audio_name + "  内容：" + info.audio_describe + "  描述：" + info.describe;
            txt += t + "\n";
        }
        txt += "\n" + "\n" + "}";

        File.WriteAllText(Application.dataPath.Replace("Assets", "ActionAudioName.cs"), txt);

        Debug.Log("生成成功！");

    }

}

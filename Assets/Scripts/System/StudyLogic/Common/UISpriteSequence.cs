﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class SpriteSequenceInfo
{
    public EnglishLedi2DAnimationWindow.ELediAnimation animType;
    public Sprite[] spriteArray;
}
public class UISpriteSequence : MonoBehaviour {


    private Image _Img;
    public Image Img
    {
        get
        {
            if (_Img == null)
                _Img = transform.GetComponent<Image>();
            return _Img;
        }
    }

    private Dictionary<EnglishLedi2DAnimationWindow.ELediAnimation, Sprite[]> m_SpriteDic;
    public Dictionary<EnglishLedi2DAnimationWindow.ELediAnimation, Sprite[]> SpriteDic
    {
        get
        {
            if (m_SpriteDic == null)
            {
                m_SpriteDic = new Dictionary<EnglishLedi2DAnimationWindow.ELediAnimation, Sprite[]>();
                foreach (SpriteSequenceInfo info in infoArray)
                {
                    m_SpriteDic.Add(info.animType, info.spriteArray);
                }
            }
            return m_SpriteDic;
        }
    }

    public float intervalTime = 0.1f;
    public SpriteSequenceInfo[] infoArray;
    public Sprite[] spriteArray;
    
    public bool isPlaying;
    public int index;

    private float addTime;
    public bool isLoop;
    private System.Action callback;

    void Update ()
    {
        if (isPlaying)
        {
            addTime += Time.deltaTime;
            if (addTime > intervalTime)
            {
                addTime -= intervalTime;

                if (index >= spriteArray.Length)
                {
                    index = 0;
                    if (!isLoop)
                    {
                        Stop();
                        if (callback != null)
                        {
                            callback();
                        }
                        return;
                    }
                }
                Sprite sprite = spriteArray[index];
                if (sprite != null && Img != null)
                {
                    Img.sprite = sprite;
                    Img.SetNativeSize();
                }
                index++;
            }
        }
	}

    public void Play(EnglishLedi2DAnimationWindow.ELediAnimation type, bool isLoop = true, System.Action callback = null)
    {
        if (!SpriteDic.ContainsKey(type))
        {
            return;
        }
        this.isLoop = isLoop;
        this.callback = callback;

        spriteArray = SpriteDic[type];
        if (spriteArray == null)
            return;

        isPlaying = true;
        index = 0;
        addTime = 0;
    }

    public void Stop()
    {
        isPlaying = false;
    }

    public void SetFirstSprite(EnglishLedi2DAnimationWindow.ELediAnimation type)
    {
        if (!SpriteDic.ContainsKey(type))
            return;

        Sprite[] arr = SpriteDic[type];
        if (arr == null || arr.Length == 0)
            return;
        if (Img != null)
        {
            Img.sprite = arr[0];
            Img.SetNativeSize();
        }
    }



}

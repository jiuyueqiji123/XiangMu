﻿

public class FEnglishTool
{


    /// <summary>
    /// 获取字母对应的游戏数据
    /// </summary>
    /// <param name="letterIndex"> A - Z ( 0 - 25 )</param>
    /// <returns></returns>
    public static uint GetEnglishDataID(int letterIndex)
    {
        return (uint)(10001 + letterIndex);
    }


}

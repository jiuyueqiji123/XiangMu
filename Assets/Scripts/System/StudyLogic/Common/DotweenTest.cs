﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DotweenTest : MonoBehaviour {


    public Transform target1;
    public Transform target2;

    public float time = 1.5f;

    public Ease type;

    
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            transform.position = target1.position;
            transform.DOKill();
            transform.DOMove(target2.position, time).SetEase(type);
        }

	}
}

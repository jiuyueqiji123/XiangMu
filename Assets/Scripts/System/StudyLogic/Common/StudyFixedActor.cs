﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StudyFixedActor : StudyBaseActor
{


    public enum AnimatorNameEnum
    {
        Idle,
        Jump,
    }
    public AnimatorNameEnum fixedType;

    public void PlayAnimation()
    {
        CrossFade(AnimatorNameEnum.Jump, 0.01f);
    }

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }


}

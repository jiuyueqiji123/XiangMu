﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LediActor : StudyBaseActor
{


    public enum AnimatorNameEnum
    {
        idell_p,
        happy_p,
        good_p,
        exciting_p,
        surprised_p,
        clap_p,     // 屏幕中心击掌
        all_talk,   // 抬手 循环说话 放手
        all_talk_01,   // 抬手
        all_talk_03,   // 放手
        all_talk02,   // 抬手 循环说话 放手
        all_talk02_01,   // 抬手
        all_talk02_03,   // 放手
    }
    
    

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }

    public void CrossFade(string name, float val)
    {
        AnimatorEx.CrossFade(name, val);
    }




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class StudyGameBaseMono : MonoBehaviour {



    // 显示的字母信息
    public string Letter
    {
        get { return m_Letter; }
    }
    [SerializeField]
    protected string m_Letter;

    public string m_audioName;

    protected Vector3 m_StartPoint;    // 出生点
    protected Vector3 m_EndPoint;    // 到达点
    protected Vector3 m_OutPoint;    // 消失点

    // 字母飞行点   暂时代替
    protected Vector3 m_FlyPoint;

    protected Vector3 m_LeftBottomPoint;
    protected Vector3 m_RightBottomPoint;
    protected Vector3 m_LeftTopPoint;

    private Text _LetterText;
    public Text LetterText
    {
        get {
            if (_LetterText == null)
            {
                _LetterText = transform.GetComponentInChildren<Text>();
            }
            return _LetterText;
        }
    }
    private Image _LetterImage;
    public Image LetterImage
    {
        get
        {
            if (_LetterImage == null)
            {
                _LetterImage = transform.GetComponentInChildren<Image>();
            }
            return _LetterImage;
        }
    }

    public virtual void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint)
    {
        m_StartPoint = startPoint;
        m_EndPoint = endPoint;
        m_OutPoint = outPoint;
    }

    public virtual void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string audioName)
    {
        m_StartPoint = startPoint;
        m_EndPoint = endPoint;
        m_OutPoint = outPoint;
        m_Letter = letter;
        m_audioName = audioName;
        if (LetterText != null)
            LetterText.text = letter;
    }

    public virtual void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string audioName, float duration)
    {
        m_StartPoint = startPoint;
        m_EndPoint = endPoint;
        m_OutPoint = outPoint;
        m_Letter = letter;
        m_audioName = audioName;
        if (LetterText != null)
            LetterText.text = letter;
    }

    public virtual void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string audioName, float duration, bool isBig)
    {
        letter = letter.Trim();
        m_StartPoint = startPoint;
        m_EndPoint = endPoint;
        m_OutPoint = outPoint;
        m_Letter = letter;
        m_audioName = audioName;
        if (LetterText != null)
            LetterText.text = letter;
        if (LetterImage != null)
        {
            string spriteName = isBig ? letter + "(big)" : letter + "(small)";
            Sprite sprite = Res.LoadSprite(StudyPath.english_bigandsmall_letter_basepath + spriteName);
            if (sprite == null)
            {
                Debug.LogError("load sprite error! spriteName:" + name);
            }
            LetterImage.sprite = sprite;
        }
    }

    public virtual void LoadAndSetLetter(string letter, string audioName)
    {
        letter = letter.Trim();
        m_Letter = letter;
        m_audioName = audioName;
        if (LetterText != null)
            LetterText.text = letter;
    }

    public virtual void LoadAndSetLetterImage(string letter, string audioName, bool isBig)
    {
        letter = letter.Trim();
        m_Letter = letter;
        m_audioName = audioName;
        if (LetterText != null)
            LetterText.text = letter;
        if (LetterImage != null)
        {
            string spriteName = isBig ? letter + "(big)" : letter + "(small)";
            Sprite sprite = Res.LoadSprite(StudyPath.english_bigandsmall_letter_basepath + spriteName);
            if (sprite == null)
            {
                Debug.LogError("load sprite error! spriteName:" + name);
            }
            LetterImage.sprite = sprite;
        }
    }
    

    //public virtual void Init(Vector3 startPoint, Vector3 endPoint, string letter, Vector3 leftBottomPoint, Vector3 rightBottomPoint, Vector3 leftTopPoint)
    //{
    //    m_StartPoint = startPoint;
    //    m_EndPoint = endPoint;
    //    m_Letter = letter;
    //    m_LeftBottomPoint = leftBottomPoint;
    //    m_RightBottomPoint = rightBottomPoint;
    //    m_LeftTopPoint = leftTopPoint;
    //}

    public virtual void FlyTo(Vector3 flyPoint, float duration = 1.5f, System.Action callback = null)
    {
        m_FlyPoint = flyPoint;

        transform.DOMove(m_FlyPoint, duration);
        transform.DOScale(0, duration);

        AddTimer((int)(duration * 1000), () =>
        {
            if (callback != null)
            {
                callback();
            }
        });
    }
    
    /// <summary>
    /// 请开始你的表演
    /// 选中正确元素的时候，改如何处理
    /// </summary>
    public virtual void StartYourShowtime(float duration = 1.5f)
    {
        Debug.Log("Is time to show");
        //gameObject.SetActive(false);
    }

    /// <summary>
    /// 请开始你的表演
    /// 选中正确元素的时候，改如何处理
    /// </summary>
    public virtual void StartYourShowtime(Vector3 position, Vector3 scale, Quaternion rotation, float duration = 1.5f)
    {
        Debug.Log("Is time to show");
        //gameObject.SetActive(false);
    }

    /// <summary>
    /// 离开这个舞台
    /// 选中正确元素的时候，非正确元素如何表现
    /// </summary>
    public virtual void LeaveTheStage(float duration = 1.5f)
    {
        transform.DOMove(m_OutPoint, duration);
    }

    public virtual void Shake(float duration = 0.5f, float strength = 5)
    {
        //m_shakePoint = transform.position;
        transform.DOShakePosition(duration, strength).OnComplete(()=> {
            transform.position = m_EndPoint;
        });
    }
    //public virtual void MoveOut(float duration)
    //{
    //    transform.DOMove(m_OutPoint, duration);
    //}


    protected void AddTimer(float time, System.Action callback)
    {
        TimerManager.Instance.AddTimer(time, () =>
        {
            if (this == null)
            {
                //Debug.LogError("this get error!");
                return;
            }
            if (callback != null)
            {
                callback();
            }
        });
    }

    protected void AddTimer(int time, System.Action callback)
    {
        TimerManager.Instance.AddTimer(time, 1, (seq) => {
            if (this == null)
            {
                //Debug.LogError("this get error!");
                return;
            }
            if (callback != null)
            {
                callback();
            }
        });
    }

    protected void AddTimer(int time, int loop, System.Action callback)
    {
        TimerManager.Instance.AddTimer(time, loop, (seq) =>
        {
            if (this == null)
                return;
            if (callback != null)
            {
                callback();
            }
        });
    }

    public void PlayAudio()
    {
        if (string.IsNullOrEmpty(m_audioName))
        {
            Debug.LogError("无音频参数! Letter:" + Letter);
            return;
        }
        string path = StudyPath.english_audio_basepath + m_audioName;
        AudioManager.Instance.PlaySound(path);
    }

    public void PlayAudio(bool isLoop)
    {
        if (string.IsNullOrEmpty(m_audioName))
        {
            Debug.LogError("无音频参数! Letter:" + Letter);
            return;
        }
        string path = StudyPath.english_audio_basepath + m_audioName;
        AudioManager.Instance.PlaySound(path, isLoop);
    }

    public void PlayEffect(string path, Vector3 position, float destroyTime)
    {
        GameObject effect = Res.LoadObj(path);
        if (effect != null)
        {
            effect.transform.position = position;
            GameObject.Destroy(effect, destroyTime);
        }
    }

    protected virtual void OnDestroy()
    {
        transform.DOKill();
    }

}

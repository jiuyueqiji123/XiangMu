﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateTexture2D : MonoBehaviour {


    public Texture2D texture;

    public Texture2D createTex;

    public int widthHeight;

    Vector2 centerPixelPosition;

    public Material mat;



    void Start ()
    {
        //mat = GetComponent<MeshRenderer>().material;
        mat.SetTexture("_MainTex", CreateSquareTexture(texture));

        print(texture.width + " : " + texture.height);
		
	}

    Texture2D CreateSquareTexture(Texture2D sourceTex)
    {
        bool isWidthMax = sourceTex.width > sourceTex.height ? true : false;
        int max = isWidthMax ? sourceTex.width : sourceTex.height;
        int min = isWidthMax ? sourceTex.height : sourceTex.width;
        Texture2D result = new Texture2D(max, max);

        int val = (max - min) / 2;

        int startX = isWidthMax ? 0 : (max - min) / 2;
        int startY = isWidthMax ? (max - min) / 2 : 0;
        int endX = isWidthMax ? max : (max - min) / 2 + min;
        int endY = isWidthMax ? (max - min) / 2 + min : max;
        

        for (int x = 0; x < max; x++)
        {
            for (int y = 0; y < max; y++)
            {
                bool isTransprent = false;
                if (x < startX || x > endX)
                    isTransprent = true;
                if (y < startY || y > endY)
                    isTransprent = true;

                Color pixelColor = new Color(0, 0, 0, 0);

                if (!isTransprent)
                {
                    pixelColor = sourceTex.GetPixel(x - startX, y - startY);
                }

                result.SetPixel(x, y, pixelColor);
            }
        }

        result.Apply();
        return result;
    }

    Texture2D generateTexture()
    {
        Texture2D texture2D = new Texture2D(widthHeight, widthHeight);

        for (int x = 0; x < widthHeight; x++)
        {
            for (int y = 0; y < widthHeight; y++)
            {
                Vector2 currentPixelPosition = new Vector2(x, y);

                //计算当前位置 距离 圆心  
                float pixelDistance = Vector2.Distance(centerPixelPosition, currentPixelPosition);


                //从pixelDistance 规范化 到 (0,1)范围,因为是当前位置 与 圆心的距离，所以把距离除以半径就好了。  
                float distance = pixelDistance / (widthHeight / 2);

                //Vector2.Distance返回的是常量距离，没有方向. Mathf.Abs 是求绝对值，这里没有必要;  
                //distance = Mathf.Abs(distance);  

                //因为是 512x512的正方形，所以有些点的位置距离圆心是 > 512的，这里要限定到 (0,1)范围，超出范围的取0或1;  
                distance = Mathf.Clamp(distance, 0, 1);

                //想想，按照上面的算法，距离圆心越近的地方，distance越小，这个像素点就越黑。  
                //如果要 圆心更亮，然后慢慢的越来越黑，就用 1-distance;  
                distance = 1 - distance;


                //把 distance 赋值给颜色。  
                Color pixelColor = new Color(distance, distance, distance, 1);


                texture2D.SetPixel(x, y, pixelColor);
            }
        }

        texture2D.Apply();
        return texture2D;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteSequenceFrame : MonoBehaviour {

    

    public bool bStartToUpdate = true;
    public float m_intervalTime = 0.1f;

    public Sprite[] spriteArray;

    private Image m_img;

    private float m_addTime;
    private int m_index;

    private bool bUpdate;

	// Use this for initialization
	void Awake () {
        m_img = GetComponent<Image>();
        if (bStartToUpdate)
            Play();

    }
	
	// Update is called once per frame
	void Update () {

        if (!bUpdate)
            return;

        m_addTime += Time.deltaTime;
        if (m_addTime > m_intervalTime)
        {
            m_addTime = 0;
            ChangeSprite();   
        }

    }

    public void Play()
    {
        if (spriteArray.Length == 0)
        {
            bUpdate = false;
            return;
        }
        bUpdate = true;
        m_index = 0;
        m_img.sprite = spriteArray[m_index];
        m_img.SetNativeSize();

    }

    public void Stop()
    {
        bUpdate = false;
    }

    private void ChangeSprite()
    {
        if (spriteArray.Length == 0)
        {
            bUpdate = false;
            return;
        }
        if (m_index > spriteArray.Length - 1)
        {
            m_index = 0;
        }

        m_img.sprite = spriteArray[m_index];
        m_index++;

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventHolder : MonoBehaviour {



    private System.Action completeCallback;
    private System.Action<int> audioCallback;

    private Animator _animator;
    public Animator animator
    {
        get
        {
            if (_animator == null)
            {
                _animator = GetComponent<Animator>();
            }
            return _animator;
        }
    }

    public void PlayAnimatoin(string name, System.Action callback = null)
    {
        completeCallback = callback;

        animator.Play(name, animator.GetLayerIndex("Base Layer"), 0);
    }

    public void PlayAnimatoin(string name, System.Action<int> audioCallback, System.Action callback = null)
    {
        completeCallback = callback;
        this.audioCallback = audioCallback;  

        animator.Play(name, animator.GetLayerIndex("Base Layer"), 0);
    }



    void OnAnimationComplete()
    {
        //Debug.LogError("OnAnimationComplete");
        if (completeCallback != null)
        {
            completeCallback();
        }
    }

    void OnAudioEvent(int index)
    {
        if (audioCallback != null)
        {
            audioCallback(index);
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class DrawPathMono : MonoBehaviour {


    // 路径ID，笔画顺序
    public int pathID;

    public List<GameObject> pathObjList = new List<GameObject>();


    public Vector3[] pathArray
    {
        get
        {
            List<Vector3> pathList = new List<Vector3>();
            for (int i = 0; i < pathObjList.Count; i++)
            {
                GameObject go = pathObjList[i];
                if (go != null)
                {
                    pathList.Add(go.transform.position);
                }
            }
            return pathList.ToArray();
        }
    }

    public void SavePathForDotweenPath()
    {
        DOTweenPath tween = GetComponent<DOTweenPath>();
        if (tween == null)
        {
            Debug.LogError("get dotweenpath faild!");
            return;
        }
        
        CreateObjPath(tween.wps);
        GameObject.DestroyImmediate(tween);
    }

    public void CreateObjPath(List<Vector3> wayPoints)
    {
        ClearPathObj();
        for (int i = 0; i < wayPoints.Count; i++)
        {
            Vector3 vec = wayPoints[i];

            GameObject go = new GameObject(i.ToString());
            go.transform.SetParent(transform);
            go.transform.localScale = Vector3.one;
            go.transform.position = vec;
            go.AddComponent<Image>().enabled = false;

            pathObjList.Add(go);
        }
    }

    public void ClearPathObj()
    {
        for (int i = 0; i < pathObjList.Count; i++)
        {
            GameObject.DestroyImmediate(pathObjList[i]);
        }
        pathObjList.Clear();
    }

    public void SetDotweenpath()
    {
        DOTweenPath tween = GetComponent<DOTweenPath>();
        if (tween == null)
        {
            tween = gameObject.AddComponent<DOTweenPath>();
        }
        List<Vector3> wps = new List<Vector3>();
        for (int i = 0; i < pathObjList.Count; i++)
        {
            Vector3 vec = pathObjList[i].transform.position;
            wps.Add(vec);
        }
        tween.wps = wps;
    }

    public void ShowPathImage()
    {
        for (int i = 0; i < pathObjList.Count; i++)
        {
            GameObject go = pathObjList[i];
            Image image = go.GetComponent<Image>();
            if (image == null)
            {
                image = go.AddComponent<Image>();
            }
            else
            {
                image.enabled = !image.enabled;
            }
        }
    }


}

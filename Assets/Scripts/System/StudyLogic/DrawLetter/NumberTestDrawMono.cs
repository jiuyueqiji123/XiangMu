﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum NumberTestEnum
{
    //number_0,
    number_1,
    number_2,
    number_3,
    number_4,
    number_5,
    number_6,
    number_7,
    number_8,
    number_9,
    number_10



    //number_1 = 1,
    //number_2 = 2,
    //number_3 = 3,
    //number_4 = 4,
    //number_5 = 5,
    //number_6 = 6,
    //number_7 = 7,
    //number_8 = 8,
    //number_9 = 9,
    //number_10 = 10
}
public class NumberTestDrawMono : MonoBehaviour {

    public NumberTestEnum numberEnum;

    public Transform[] m_Numbers;
    public Transform m_Brush;
    public Transform m_Mask;
    public Transform m_AnimParent;
    public Image m_Sign;
    public Canvas m_Canvas;

    private int playIndex;

    IEnumerator Start ()
    {
        yield return new WaitForSeconds(1);

        Debug.Log(m_Canvas.worldCamera.name);

        playIndex = (int)numberEnum;
        Play();
		
	}

    private void Play()
    {
        if (playIndex >= m_Numbers.Length) return;

        DrawRealize2D.Instance.DrawNumber(m_Numbers[playIndex], m_Brush, m_Mask, m_Sign, m_Canvas, m_AnimParent, () =>
        {
            StartCoroutine(DelayPlay());
        });
    }

    IEnumerator DelayPlay()
    {
        yield return new WaitForSeconds(2);
        m_Numbers[playIndex].gameObject.SetActive(false);
        //playIndex++;
        Play();
    }

}

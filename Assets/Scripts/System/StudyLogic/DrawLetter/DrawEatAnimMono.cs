﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawEatAnimMono : BaseRectTransform {


    private readonly string loadBasePath = StudyPath.math_drawletterBasePath;

    public enum EAnimDirection
    {
        Left,
        Right,
    }
    public enum EEatAnimalEnum
    {
        Butterfly,  //蝴蝶
        Carb,       //螃蟹
        Ladybird,       //瓢虫
        Octopus,       //章鱼
        Penguin,       //企鹅
        Frog,       //青蛙

    }

    public EAnimDirection type;
    public EEatAnimalEnum animalType;
    public float speed = 100;

    private AnimationEventHolder _holder;
    public AnimationEventHolder holder
    {
        get {
            if (_holder == null)
            {
                _holder = GetComponent<AnimationEventHolder>();
            }
            return _holder;
        }
    }

    private bool bIsMoving;

    private Transform m_Anim_Target;

    private void Start()
    {

        DrawLetterMono mono = GetComponentInParent<DrawLetterMono>();
        if (mono != null)
        {
            animalType = mono.animalType;
        }
        bIsMoving = false;

        switch (animalType)
        {
            case EEatAnimalEnum.Butterfly:
                m_Anim_Target = Res.LoadObj(loadBasePath + "Anim_Butterfly").transform;
                break;
            case EEatAnimalEnum.Carb:
                m_Anim_Target = Res.LoadObj(loadBasePath + "Anim_Crab").transform;
                break;
            case EEatAnimalEnum.Frog:
                m_Anim_Target = Res.LoadObj(loadBasePath + "Anim_Frog").transform;
                break;
            case EEatAnimalEnum.Ladybird:
                m_Anim_Target = Res.LoadObj(loadBasePath + "Anim_Ladybird").transform;
                break;
            case EEatAnimalEnum.Octopus:
                m_Anim_Target = Res.LoadObj(loadBasePath + "Anim_Octopus").transform;
                break;
            case EEatAnimalEnum.Penguin:
                m_Anim_Target = Res.LoadObj(loadBasePath + "Anim_Penguin").transform;
                break;
        }
        RectTransform rect = m_Anim_Target.transform as RectTransform;
        Transform animParent = transform.Find("ModelTexture");
        animParent.GetComponent<Image>().enabled = false;
        rect.SetParent(animParent);
        rect.ResetTransformForLocal();

        if (type == EAnimDirection.Left)
        {
            rect.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            rect.localScale = new Vector3(1, 1, 1);
        }
    }

    private void Update()
    {
        if (!bIsMoving)
            return;
        if (type == EAnimDirection.Left)
        {
            rectTransform.anchoredPosition += Vector2.left * Time.deltaTime * speed;
        }
        else
        {
            rectTransform.anchoredPosition += Vector2.right * Time.deltaTime * speed;
        }
    }

    public void PlayAnim()
    {
        bIsMoving = false;
        if (type == EAnimDirection.Left)
        {
            holder.PlayAnimatoin("EatAnim_Left", OnAnimationEnd);
        }
        else
        {
            holder.PlayAnimatoin("EatAnim_Right", OnAnimationEnd);
        }
    }

    private void OnAnimationEnd()
    {
        bIsMoving = true;

        if (m_Anim_Target != null)
        {
            m_Anim_Target.GetComponent<SpriteSequenceFrame>().Play();
        }
        RotateTarget();
        Destroy(this.gameObject, 10);
    }

    private void RotateTarget()
    {
        switch (animalType)
        {
            case EEatAnimalEnum.Butterfly:
                m_Anim_Target.rotation = type == EAnimDirection.Left ? Quaternion.Euler(0, 0, 45) : Quaternion.Euler(0, 0, -45);
                break;
            case EEatAnimalEnum.Ladybird:
                m_Anim_Target.rotation = type == EAnimDirection.Left ? Quaternion.Euler(0, 0, 90) : Quaternion.Euler(0, 0, -90);
                break;
        }

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EMathGameType
{
    // 认识数字
    Number,
    // 图形
    Geometry,
}
public class StudyMathManager : MonoSingleton<StudyMathManager>
{


    NumberController numberController = new NumberController();    // 认识数字
    GeometryController geometryController = new GeometryController();    // 图形认知


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            EventBus.Instance.BroadCastEvent(EventID.ON_MOUSE_BUTTON_DOWN);
        }
        if (Input.GetMouseButton(0))
        {
            EventBus.Instance.BroadCastEvent(EventID.ON_MOUSE_BUTTON_HOVER);
        }
        if (Input.GetMouseButtonUp(0))
        {
            EventBus.Instance.BroadCastEvent(EventID.ON_MOUSE_BUTTON_UP);
        }


        numberController.OnUpdate();
        geometryController.OnUpdate();

    }


    public void StartGame(EMathGameType type)
    {
        //numberController.OnStart(OnGameComplete);
        //shapeController.OnStart(OnGameComplete);
        //return;

        switch (type)
        {
            case EMathGameType.Number:

                numberController.OnStart(OnGameComplete);

                break;

            case EMathGameType.Geometry:
                geometryController.OnStart(OnGameComplete);

                break;
        }
    }

    public void ExitGame()
    {
        numberController.OnExit();
        geometryController.OnExit();
    }

    private void OnGameComplete()
    {
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }



}

﻿

public enum EGeometryAnimals
{
    Ji = 1,
    Mao = 2,
    Tuzi = 3,
    Zhu = 4,
    Niu = 5,
    Gou = 6,
}

public enum EGeometryEnum
{
    /// <summary>
    /// 圆形
    /// </summary>
    Circle,
    /// <summary>
    /// 正方形
    /// </summary>
    Square,
    /// <summary>
    /// 三角形
    /// </summary>
    Triangle,
    /// <summary>
    /// 长方形
    /// </summary>
    Rectangle,
    /// <summary>
    /// 梯形
    /// </summary>
    Trapezoid,
    /// <summary>
    /// 椭圆形
    /// </summary>
    Ellipse,
}

public enum EGeometryAssetEnum
{
    Circle,
    Square,
    //Triangle,
    Triangle_1,
    Triangle_2,
    Rectangle,
    //Trapezoid,
    Trapezoid_1,
    Trapezoid_2,
    Trapezoid_3,
    Ellipse,
    RightTriangle,
}
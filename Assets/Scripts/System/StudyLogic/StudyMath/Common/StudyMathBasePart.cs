﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class StudyMathBasePart : InitiativeBasePart, IEasyAudio
{

    private Transform _Scene;
    public Transform Scene
    {
        get { return _Scene; }
    }

    protected readonly float delayTime = 1.5f;




    protected override void OnPartStart()
    {
        base.OnPartStart();

        this.AddTimerEx(delayTime, OnGameStartDelay);
    }

    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        this.AddTimerEx(delayTime, OnGameStartDelay);
    }

    protected override void OnPartExit()
    {
        base.OnPartExit();

        this.StopAllAudioEx();
    }



    protected override void AddEvent()
    {
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_HOVER, OnMouseHover);
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_UP, OnMouseUp);
    }

    protected override void RemoveEvent()
    {
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_HOVER, OnMouseHover);
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_UP, OnMouseUp);
    }

    protected virtual void OnMouseDown() { }
    protected virtual void OnMouseHover() { }
    protected virtual void OnMouseUp() { }

    protected virtual void OnGameStartDelay() { }

    protected virtual void SetScene(Transform scene)
    {
        _Scene = scene;
    }

    protected virtual Transform CreateScene(string scenePath)
    {
        _Scene = Instantiate(scenePath, true).transform;

        Camera targetCamera = _Scene.Find("Camera").GetComponent<Camera>();
        //Camera targetCamera = scene.GetComponentInChildren<Camera>();
        if (targetCamera != null)
        {
            Camera.main.CopyFrom(targetCamera);
            targetCamera.gameObject.SetActive(false);
        }
        return _Scene;
    }

    protected Vector3 GetScreenToWorldPoint(Camera camera, float x, float y, float depth = 20)
    {
        return camera.ScreenToWorldPoint(new Vector3(x, y, depth));
    }

    public void PlayEffect(string path, Transform parent, float destroyTime)
    {
        GameObject effect = Instantiate(path, true);
        if (effect != null)
        {
            effect.transform.SetParent(parent);
            effect.transform.ResetTransformForLocal();
            //effect.transform.position = position;
            if (destroyTime != 0)
                GameObject.Destroy(effect, destroyTime);
        }
    }

    public void PlayEffect(string path, Vector3 position, float destroyTime)
    {
        GameObject effect = Instantiate(path, true);
        if (effect != null)
        {
            effect.transform.position = position;
            GameObject.Destroy(effect, destroyTime);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StudyMathBaseFlow : InitiativeBaseFlow, IEasyLoading, IEasyAudio
{

    private Transform _Scene;
    public Transform Scene
    {
        get { return _Scene; }
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        EventBus.Instance.BroadCastEvent(EventID.ON_MATH_GAME_FLOW_EXIT);
    }

    protected virtual void SetScene(Transform scene)
    {
        _Scene = scene;
    }

    protected virtual Transform CreateScene(string scenePath)
    {
        _Scene = Instantiate(scenePath, true).transform;

        Camera targetCamera = _Scene.Find("Camera").GetComponent<Camera>();
        //Camera targetCamera = scene.GetComponentInChildren<Camera>();
        if (targetCamera != null)
        {
            Camera.main.CopyFrom(targetCamera);
            targetCamera.gameObject.SetActive(false);
        }
        return _Scene;
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using WCBG.ToolsForUnity.Tools;
using DynamicShadowProjector;

public class StudyGeometryBasePart : InitiativeBasePart, IEasyAudio
{

    private Transform _Scene;
    public Transform Scene
    {
        get { return _Scene; }
    }

    protected int m_partIndex;
    protected bool bLockInput;





    protected override void OnPartExit()
    {
        base.OnPartExit();

        this.StopAllAudioEx();
    }

    protected override void AddEvent()
    {
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_HOVER, OnMouseHover);
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_UP, OnMouseUp);
    }

    protected override void RemoveEvent()
    {
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_HOVER, OnMouseHover);
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_UP, OnMouseUp);
    }


    protected virtual void OnMouseDown()
    {

        Transform target = FTools.GetRaycastHitTargetByMousePoint((int)EGameLayerMask.ShadowProjecter);
        if (target != null)
        {
            StudyFixedActor fixedActor = target.GetComponent<StudyFixedActor>();
            if (fixedActor != null) fixedActor.PlayAnimation();
        }
    }
    protected virtual void OnMouseHover() { }
    protected virtual void OnMouseUp() { }

    protected void RefreshShadowProjecter()
    {
        if (Scene != null)
        {
            DrawTargetObject target = Scene.GetComponentInChildren<DrawTargetObject>();
            target.RefreshShaowProjecter();
        }
    }

    /// <summary>
    /// 创建一个对象
    /// </summary>
    /// <param name="sourcePath">资源路径</param>
    /// <param name="bChache">是否缓存，关卡退出会自动销毁该对象</param>
    /// <param name="bActiveShadowProjecter">启用阴影，会设置对象的父物体为场景 Scene </param>
    /// <returns></returns>
    protected GameObject Instantiate(string sourcePath, bool bChache, bool bActiveShadowProjecter)
    {
        GameObject go = Instantiate(sourcePath, bChache);
        if (bActiveShadowProjecter)
        {
            if (Scene != null)
            {
                go.transform.SetParent(Scene);
                go.SetLayer(EGameLayerMask.ShadowProjecter);
                //Transform _projecterParent = Scene.Find("ShadowProjecter_Parent");
                //if (_projecterParent != null)
                //{
                //    go.transform.SetParent(_projecterParent);
                //    go.SetLayer((int)EGameLayerMask.ShadowProjecter);
                //}
                //else
                //{
                //    Debug.LogError("Find ShadowProjecter_Parent error!");
                //}
            }
            else
            {
                Debug.LogError("Get Scene error!");
            }
        }
        return go;
    }

    protected virtual void SetScene(Transform Scene)
    {
        _Scene = Scene;
    }

    protected virtual Transform CreateScene(string ScenePath)
    {
        _Scene = Instantiate(ScenePath, true).transform;

        Camera targetCamera = _Scene.Find("Camera").GetComponent<Camera>();
        //Camera targetCamera = Scene.GetComponentInChildren<Camera>();
        if (targetCamera != null)
        {
            Camera.main.CopyFrom(targetCamera);
            targetCamera.gameObject.SetActive(false);
        }
        return _Scene;
    }

    protected Vector3 GetScreenToWorldPoint(Camera camera, float x, float y, float depth = 20)
    {
        return camera.ScreenToWorldPoint(new Vector3(x, y, depth));
    }
    

    public GameObject PlayEffect(string path, Transform parent, float destroyTime)
    {
        GameObject effect = Instantiate(path, true);
        if (effect != null)
        {
            effect.transform.SetParent(parent);
            effect.transform.ResetTransformForLocal();
            //effect.transform.position = position;
            if (destroyTime != 0)
                GameObject.Destroy(effect, destroyTime);
        }
        return effect;
    }

    public GameObject PlayEffect(string path, Vector3 position, float destroyTime)
    {
        GameObject effect = Instantiate(path, true);
        if (effect != null)
        {
            effect.transform.position = position;
            GameObject.Destroy(effect, destroyTime);
        }
        return effect;
    }

    protected void PlayGeometryAudio(EGeometryEnum type, System.Action callback = null)
    {
        GameAudioSource audio = null;

        switch (type)
        {
            case EGeometryEnum.Circle:
                audio = this.PlayAudioEx(StudyAudioName.t_22011);
                break;
            case EGeometryEnum.Square:
                audio = this.PlayAudioEx(StudyAudioName.t_22013);
                break;
            case EGeometryEnum.Triangle:
                audio = this.PlayAudioEx(StudyAudioName.t_22012);
                break;
            case EGeometryEnum.Rectangle:
                audio = this.PlayAudioEx(StudyAudioName.t_22014);
                break;
            case EGeometryEnum.Trapezoid:
                audio = this.PlayAudioEx(StudyAudioName.t_22015);
                break;
            case EGeometryEnum.Ellipse:
                audio = this.PlayAudioEx(StudyAudioName.t_22016);
                break;
            default:
                audio = this.PlayAudioEx(StudyAudioName.t_22011);
                break;
        }

        if (callback != null)
            this.AddTimerEx(audio.Length, callback);
    }


}

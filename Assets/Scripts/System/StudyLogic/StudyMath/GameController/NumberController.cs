﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberController  : StudyBaseController
{

    public enum ENumberGameType
    {
        None = 0,
        TrainNumber = 1,
        TrainBall = 2,
        TrainTrack = 3,
    }


    NumberGameSelectFlow selectFlow = new NumberGameSelectFlow();   //选择游戏场景
    TrainNumberFlow trainNumberFlow = new TrainNumberFlow();    // 书写火车编号
    TrainBallFlow trainBallFlow = new TrainBallFlow();    // 数球
    TrainTrackFlow trainTrackFlow = new TrainTrackFlow();    // 铺枕木


    // 记录玩过的数字列表
    public List<int> m_numberList = new List<int>();

    private System.Action gameCompleteCallback;

    public void OnStart(Action callback = null)
    {
        gameCompleteCallback = callback;
        bGamePlaying = true;

        WindowManager.Instance.OpenWindow(WinNames.MathNumberPanel);
        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);

        //SaveProgress(ENumberGameType.TrainTrack);
        //SaveProgress(ENumberGameType.TrainTrack);
        StartGameSelectFlow(true);
        //StartTrainNumberFlow();
        //StartTrainBallFlow();
        //StartTrainTrackFlow();

        //this.PlayMusicEx(StudyPath.number_audio_basepath + StudyAudioName.Math_BGAudio, true);
        this.PlayMusicEx(StudyAudioName.t_20001);

        EventBus.Instance.AddEventHandler(EventID.ON_MATHNUMBER_WINDOW_BTN_RETURN_CLICK, OnQuitEvent);
    }

    public void OnExit()
    {
        bGamePlaying = false;
        WindowManager.Instance.CloseWindow(WinNames.MathNumberPanel);
        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);

        selectFlow.OnExit();
        trainNumberFlow.OnExit();
        trainBallFlow.OnExit();
        trainTrackFlow.OnExit();

        AudioManager.Instance.StopAllSound();
        AudioManager.Instance.StopBackgroundMusic();

        EventBus.Instance.RemoveEventHandler(EventID.ON_MATHNUMBER_WINDOW_BTN_RETURN_CLICK, OnQuitEvent);
    }

    public void OnUpdate()
    {
        trainNumberFlow.OnUpdate();
        trainBallFlow.OnUpdate();
        trainTrackFlow.OnUpdate();
    }

    private void OnQuitEvent()
    {
        if (selectFlow.bFlowEnable)
        {
            //selectFlow.OnExit();
            OnAllFlowComplete(false);
        }
        else
        {
            this.StartLoadingAndEndEx(() =>
            {
                trainNumberFlow.OnExit();
                trainBallFlow.OnExit();
                trainTrackFlow.OnExit();

                StartGameSelectFlow(false);
            });
        }
    }


    // -------------------- 游戏选择
    private void StartGameSelectFlow(bool isFirstPlay)
    {
        selectFlow.OnStart(OnGameSelectComplete, isFirstPlay);
    }

    private void OnGameSelectComplete(bool bComplete, object[] paramArray)
    {
        ENumberGameType type = (ENumberGameType)paramArray[0];
        this.StartLoadingAndEndEx(() =>
        {
            selectFlow.OnExit();
            switch (type)
            {
                case ENumberGameType.TrainNumber:
                    StartTrainNumberFlow();
                    break;
                case ENumberGameType.TrainBall:
                    StartTrainBallFlow();
                    break;
                case ENumberGameType.TrainTrack:
                    StartTrainTrackFlow();
                    break;
            }
        });
    }
    // ============================


    // 画数字 ----------------------------------
    private void StartTrainNumberFlow()
    {
        trainNumberFlow.OnStart(TrainNumberFlowComplete);
    }

    private void TrainNumberFlowComplete(bool bComplete, object[] paramArray)
    {
        SaveProgress(ENumberGameType.TrainNumber);

        this.StartLoadingAndEndEx(() =>
        {
            List<int> playedNumberList = (List<int>)paramArray[0];
            m_numberList.AddRange(playedNumberList);
            trainNumberFlow.OnExit();

            string str = "";
            foreach (int number in m_numberList)
            {
                str += number + "; ";
            }
            Debug.Log("number play list: " + str);

            StartTrainBallFlow();
        });
    }
    // =============================================


    // 数球 ----------------------------------
    private void StartTrainBallFlow()
    {
        List<int> list = new List<int>(m_numberList);
        trainBallFlow.OnStart(TrainBallFlowComplete, list);
    }

    private void TrainBallFlowComplete(bool bComplete, object[] paramArray)
    {
        SaveProgress(ENumberGameType.TrainBall);

        this.StartLoadingAndEndEx(() =>
        {
            List<int> playedNumberList = (List<int>)paramArray[0];
            m_numberList.AddRange(playedNumberList);
            trainBallFlow.OnExit();

            string str = "";
            foreach (int number in m_numberList)
            {
                str += number + "; ";
            }
            Debug.Log("number play list: " + str);

            StartTrainTrackFlow();
        });
    }
    // =============================================


    // 铺火车枕木 ----------------------------------
    private void StartTrainTrackFlow()
    {
        List<int> list = new List<int>(m_numberList);
        trainTrackFlow.OnStart(TrainTrackFlowComplete, list);
    }

    private void TrainTrackFlowComplete(bool bComplete, object[] paramArray)
    {
        SaveProgress(ENumberGameType.TrainTrack);

        List<int> playedNumberList = (List<int>)paramArray[0];
        m_numberList.AddRange(playedNumberList);
        //trainTrackFlow.OnExit();

        string str = "";
        foreach (int number in m_numberList)
        {
            str += number + "; ";
        }
        Debug.Log("number play list: " + str);

        OnAllFlowComplete(true);

    }
    // =============================================


    private void SaveProgress(ENumberGameType type)
    {
        StudyGameProgressInfo info = LocalDataManager.Instance.Load<StudyGameProgressInfo>(ELocalDataType.StudyMathGame);
        int progress = (int)type;
        if (progress > info.gameProgress)
        {
            info.gameProgress = progress;
            LocalDataManager.Instance.Save(ELocalDataType.StudyMathGame, info);
        }
    }

    private void OnAllFlowComplete(bool bComplete)
    {
        MathNumberWindow.Instance.UpdateLockInputState(true);
        if (gameCompleteCallback != null)
        {
            gameCompleteCallback();
        }
    }


    
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeometryController : StudyBaseController
{

    public enum EGeometryGameType
    {
        None,
        PlotSummary,
        ToyBrick,
        Animal,
    }


    GeometryGameSelectFlow selectFlow = new GeometryGameSelectFlow();    // 游戏选择
    GeometryPlotSummaryFlow plotSummaryFlow = new GeometryPlotSummaryFlow();    // 剧情介绍
    GeometryStudyToyBricksFlow studyToyBricksFlow = new GeometryStudyToyBricksFlow();    // 画图形
    GeometryFindToyBricksFlow findToyBricksFlow = new GeometryFindToyBricksFlow();    // 找积木
    GeometryFindAnimalsFlow findAnimalsFlow = new GeometryFindAnimalsFlow();    // 拼动物


    private System.Action gameCompleteCallback;

    public void OnStart(Action callback = null)
    {
        gameCompleteCallback = callback;
        bGamePlaying = true;

        WindowManager.Instance.OpenWindow(WinNames.MathNumberPanel);
        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.None);

        //StudyGameProgressInfo info = new StudyGameProgressInfo();
        //info.gameProgress = 1;
        //LocalDataManager.Instance.Save(ELocalDataType.StudyGeometryGame, info);
        //SaveProgress(EGeometryGameType.Animal);
        StartGameSelectFlow(true);
        //StartPlotSummaryFlow();
        //StartStudyToyBricksFlow();
        //StartFindToyBricksFlow();
        //StartFindAnimalsFlow();

        //this.PlayMusicEx(StudyPath.study_geometry_audio_basepath + StudyAudioName.GeometryGameBgAudio, true);
        this.PlayMusicEx(StudyAudioName.t_10001);
        EventBus.Instance.AddEventHandler(EventID.ON_MATHNUMBER_WINDOW_BTN_RETURN_CLICK, OnQuitEvent);
    }

    public void OnExit()
    {
        bGamePlaying = false;
        WindowManager.Instance.CloseWindow(WinNames.MathNumberPanel);
        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);

        selectFlow.OnExit();
        plotSummaryFlow.OnExit();
        studyToyBricksFlow.OnExit();
        findToyBricksFlow.OnExit();
        findAnimalsFlow.OnExit();

        AudioManager.Instance.StopAllSound();
        AudioManager.Instance.StopBackgroundMusic();

        EventBus.Instance.RemoveEventHandler(EventID.ON_MATHNUMBER_WINDOW_BTN_RETURN_CLICK, OnQuitEvent);
    }

    public void OnUpdate()
    {
        plotSummaryFlow.OnUpdate();
        studyToyBricksFlow.OnUpdate();
        findToyBricksFlow.OnUpdate();
        findAnimalsFlow.OnUpdate();
    }

    private void OnQuitEvent()
    {
        if (selectFlow.bFlowEnable)
        {
            //selectFlow.OnExit();
            OnAllFlowComplete(false);
        }
        else
        {
            this.StartLoadingAndEndEx(() =>
            {
                plotSummaryFlow.OnExit();
                studyToyBricksFlow.OnExit();
                findToyBricksFlow.OnExit();
                findAnimalsFlow.OnExit();

                StartGameSelectFlow(false);
            });
        }
    }



    // -------------------- 游戏选择
    private void StartGameSelectFlow(bool isFirstPlay)
    {
        selectFlow.OnStart(OnGameSelectComplete, isFirstPlay);
    }

    private void OnGameSelectComplete(bool bComplete, object[] paramArray)
    {
        //Debug.Log("---------------: OnGameSelectComplete!");

        EGeometryGameType type = (EGeometryGameType)paramArray[0];

        this.StartLoadingAndEndEx(() =>
        {
            selectFlow.OnExit();
            switch (type)
            {
                case EGeometryGameType.PlotSummary:
                    //StartPlotSummaryFlow();
                    StartStudyToyBricksFlow();
                    break;
                case EGeometryGameType.ToyBrick:
                    StartFindToyBricksFlow();
                    break;
                case EGeometryGameType.Animal:
                    StartFindAnimalsFlow();
                    break;
            }
        });
    }
    // ============================



    // -------------------- 剧情介绍
    private void StartPlotSummaryFlow()
    {
        plotSummaryFlow.OnStart(OnPlotSummaryFlowComplete);
    }

    private void OnPlotSummaryFlowComplete(bool bComplete)
    {
        //Debug.Log("---------------: OnPlotSummaryFlowComplete!");

        this.StartLoadingAndEndEx(() =>
        {
            plotSummaryFlow.OnExit();
            StartStudyToyBricksFlow();
        });
    }
    // ============================


    // -------------------- 学习积木
    private void StartStudyToyBricksFlow()
    {
        studyToyBricksFlow.OnStart(OnStudyToyBricksFlowComplete);
    }

    private void OnStudyToyBricksFlowComplete(bool bComplete)
    {
        //Debug.Log("---------------: OnStudyToyBricksFlowComplete!");
        SaveProgress(EGeometryGameType.PlotSummary);

        this.StartLoadingAndEndEx(() =>
        {
            studyToyBricksFlow.OnExit();
            StartFindToyBricksFlow();
        });
    }
    // ============================


    // -------------------- 找积木
    private void StartFindToyBricksFlow()
    {
        findToyBricksFlow.OnStart(OnFindToyBricksFlowComplete);
    }

    private void OnFindToyBricksFlowComplete(bool bComplete)
    {
        //Debug.Log("---------------: OnFindToyBricksFlowComplete!");
        SaveProgress(EGeometryGameType.ToyBrick);

        this.StartLoadingAndEndEx(() =>
        {
            findToyBricksFlow.OnExit();
            StartFindAnimalsFlow();
        });
    }
    // ============================

    // -------------------- 找动物
    private void StartFindAnimalsFlow()
    {
        findAnimalsFlow.OnStart(OnFindAnimalsFlowComplete);
    }

    private void OnFindAnimalsFlowComplete(bool bComplete)
    {
        //Debug.Log("---------------: OnFindAnimalsFlowComplete!");
        SaveProgress(EGeometryGameType.Animal);

        //this.StartLodingAndEndEx(() =>
        //{
        //findAnimalsFlow.OnExit();
        OnAllFlowComplete(true);
        //});
    }
    // ============================



    private void SaveProgress(EGeometryGameType type)
    {
        StudyGameProgressInfo info = LocalDataManager.Instance.Load<StudyGameProgressInfo>(ELocalDataType.StudyGeometryGame);
        int progress = (int)type;
        if (progress > info.gameProgress)
        {
            info.gameProgress = progress;
            LocalDataManager.Instance.Save(ELocalDataType.StudyGeometryGame, info);
        }
    }

    private void OnAllFlowComplete(bool bComplete)
    {
        MathNumberWindow.Instance.UpdateLockInputState(true);
        if (gameCompleteCallback != null)
        {
            gameCompleteCallback();
        }
    }


    
}

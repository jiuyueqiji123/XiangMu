﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// 带动画Actor
/// </summary>
public class TrainHeadActor : StudyBaseActor
{

    public enum AnimatorNameEnum
    {
        Move,
        Static,
        Stop,
        UpAndDown,
        Zoom,   // 启动鸣笛
        BallTrainFinish,
    }

    // ID从数字1开始
    [SerializeField]
    private int _Number;
    public int Number
    {
        get { return _Number; }
    }

    public Vector3[] movePath;

    private Texture _hightTexture = null;
    public Texture hightTexture
    {
        get
        {
            if (_hightTexture == null)
            {
                Resource res = ResourceManager.Instance.GetResource(StudyPath.GetTrainHeadHighLightTexturePath(_Number), typeof(Texture), enResourceType.Prefab);
                if (res.m_content != null)
                {
                    _hightTexture = res.m_content as Texture;
                }
                else
                {
                    Debug.LogError("加载贴图失败!PATH:" + StudyPath.GetTrainHeadHighLightTexturePath(_Number));
                }
            }
            return _hightTexture;
        }
    }
    private Texture _unhightTexture = null;
    public Texture unhightTexture
    {
        get
        {
            if (_unhightTexture == null)
            {
                Resource res = ResourceManager.Instance.GetResource(StudyPath.GetTrainHeadUnHighLightTexturePath(_Number), typeof(Texture), enResourceType.Prefab);
                if (res.m_content != null)
                {
                    _unhightTexture = res.m_content as Texture;
                }
                else
                {
                    Debug.LogError("加载贴图失败!PATH:" + StudyPath.GetTrainHeadUnHighLightTexturePath(_Number));
                }
            }
            return _unhightTexture;
        }
    }
    

    public void Initialize(int number, bool bSetHighTexture)
    {
        _Number = number;
        
        SetHighTexture(bSetHighTexture);
    }

    public void SetHighTexture(bool val)
    {
        if (val)
        {
            Material[] materials = meshRenderer.materials;
            materials[2].SetTexture("_MainTex", hightTexture);
        }
        else
        {
            Material[] materials = meshRenderer.materials;
            materials[2].SetTexture("_MainTex", unhightTexture);
        }
    }

    public void SetTrainRedTexture()
    {
        Resource res = ResourceManager.Instance.GetResource(StudyPath.trainHeadRedTexture, typeof(Texture), enResourceType.Prefab);
        if (res.m_content != null)
        {
            Material[] materials = meshRenderer.materials;
            materials[0].SetTexture("_MainTex", res.m_content as Texture);
        }
    }

    public void SetTrainYellowTexture()
    {
        Resource res = ResourceManager.Instance.GetResource(StudyPath.trainHeadYellowTexture, typeof(Texture), enResourceType.Prefab);
        if (res.m_content != null)
        {
            Material[] materials = meshRenderer.materials;
            materials[0].SetTexture("_MainTex", res.m_content as Texture);
        }
    }

    public void MovePath(Vector3[] path, float duration, System.Action callback = null)
    {
        movePath = path;

        transform.DOPath(path, duration, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() =>
        {
            if (callback != null)
            {
                callback();
            }
        });
    }

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }

    public void PlayEffect(string effectPath)
    {
        Transform effectParent = transform.Find("Effect");
        if (effectParent != null)
        {
            GameObject go = Res.LoadObj(effectPath);
            go.transform.SetParent(effectParent);
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            go.transform.localRotation = Quaternion.identity;
        }
    }

    public void PlayStopEffect(string effectPath)
    {
        Transform effectParent = transform.Find("StopEffect");
        if (effectParent != null)
        {
            GameObject go = Res.LoadObj(effectPath);
            go.transform.SetParent(effectParent);
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            go.transform.localRotation = Quaternion.identity;
        }
    }

    public void PlayHintEffect(string effectPath)
    {
        Transform effectParent = transform.Find("HintEffect");
        if (effectParent != null)
        {
            GameObject go = Res.LoadObj(effectPath);
            go.transform.SetParent(effectParent);
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            go.transform.localRotation = Quaternion.identity;
        }
    }

    public void ClearEffect()
    {
        Transform effectParent = transform.Find("Effect");
        for (int i = 0; i < effectParent.childCount; i++)
        {
            Destroy(effectParent.GetChild(i).gameObject);
        }
    }

    public void ClearAllEffect()
    {
        Transform effectParent = transform.Find("Effect");
        for (int i = 0; i < effectParent.childCount; i++)
        {
            Destroy(effectParent.GetChild(i).gameObject);
        }
        effectParent = transform.Find("HintEffect");
        for (int i = 0; i < effectParent.childCount; i++)
        {
            Destroy(effectParent.GetChild(i).gameObject);
        }
        effectParent = transform.Find("StopEffect");
        for (int i = 0; i < effectParent.childCount; i++)
        {
            Destroy(effectParent.GetChild(i).gameObject);
        }
    }

}

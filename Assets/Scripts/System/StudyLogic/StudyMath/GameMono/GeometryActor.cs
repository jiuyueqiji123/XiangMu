﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GeometryActor : StudyBaseActor
{


    public enum AnimatorNameEnum
    {
        Static,
        Shake,
        ScaleIn,
        ScaleOut,
    }

    private EGeometryEnum _geometryType;
    public EGeometryEnum geometryType
    {
        get
        {
            return _geometryType;
        }
    }

    //private EGeometryAssetEnum _geometryAessetType;
    //public EGeometryAssetEnum geometryAessetType
    //{
    //    get
    //    {
    //        return _geometryAessetType;
    //    }
    //}


    private GameAudioSource m_audio;


    public void Initialize(EGeometryEnum inGeometryType/*, EGeometryAssetEnum inGeometryAessetType*/)
    {
        _geometryType = inGeometryType;
        //_geometryAessetType = inGeometryAessetType;
    }

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }

    public void EnableAnimator(bool value)
    {
        AnimatorEx.animator.enabled = value;
    }

    public GameAudioSource PlayGeometryActorAudio()
    {
        if (m_audio != null && m_audio.IsPlaying)
            return m_audio;

        m_audio = PlayActorAudio();

        return m_audio;
    }

    private GameAudioSource PlayActorAudio()
    {
        switch (_geometryType)
        {
            case EGeometryEnum.Circle:
                return this.PlayAudioEx(StudyAudioName.t_22011);
            case EGeometryEnum.Square:
                return this.PlayAudioEx(StudyAudioName.t_22013);
            case EGeometryEnum.Triangle:
                //case EGeometryEnum.Triangle_1:
                //case EGeometryEnum.Triangle_2:
                //case EGeometryEnum.RightTriangle:
                return this.PlayAudioEx(StudyAudioName.t_22012);
            case EGeometryEnum.Rectangle:
                return this.PlayAudioEx(StudyAudioName.t_22014);
            case EGeometryEnum.Trapezoid:
                //case EGeometryEnum.Trapezoid_1:
                //case EGeometryEnum.Trapezoid_2:
                //case EGeometryEnum.Trapezoid_3:
                return this.PlayAudioEx(StudyAudioName.t_22015);
            case EGeometryEnum.Ellipse:
                return this.PlayAudioEx(StudyAudioName.t_22016);
            default:
                return this.PlayAudioEx(StudyAudioName.t_22011);
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GeometryInfo
{
    public EGeometryEnum geometryType;
    public GameObject gameObject;
    public Transform sign;
}
public class GeometryActorComponent : MonoBehaviour {


    public enum EOutDirectionEnum
    {
        Left,
        Right,
    }

    public Material defaultMaterial;
    public Material transparentMaterial;

    public EOutDirectionEnum outType;

    [SerializeField]
    public GeometryInfo[] geometryInfoList;

    [HideInInspector]
    public List<GeometryInfo> toyBricksList = new List<GeometryInfo>();


    public void SetAnimalRandomToyBricks(int count)
    {
        if (count > geometryInfoList.Length)
            count = geometryInfoList.Length;

        List<int> randomList = new List<int>();
        for (int i = 0; i < geometryInfoList.Length; i++)
        {
            GeometryInfo info = geometryInfoList[i];
            info.gameObject.GetComponent<SkinnedMeshRenderer>().material = defaultMaterial;
            randomList.Add(i);
        }

        toyBricksList.Clear();
        for (int i = 0; i < count; i++)
        {
            int randomIndex = Random.Range(0, randomList.Count);
            int value = randomList[randomIndex];
            randomList.RemoveAt(randomIndex);

            GeometryInfo info = geometryInfoList[value];
            info.gameObject.GetComponent<SkinnedMeshRenderer>().material = transparentMaterial;
            toyBricksList.Add(info);
        }

    }

    public void OnToyBrickComplete(GeometryInfo info)
    {
        info.gameObject.GetComponent<SkinnedMeshRenderer>().material = defaultMaterial;
        toyBricksList.Remove(info);
    }

    //public void Test()
    //{
    //    geometryInfoList = _geometryInfoList;
    //}


}

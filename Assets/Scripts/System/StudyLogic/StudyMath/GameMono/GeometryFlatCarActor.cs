﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class GeometryFlatCarActor : StudyBaseActor
{
    public enum AnimatorNameEnum
    {
        Move,
        Static,
    }

    private Transform _followPoint;
    public Transform followPoint
    {
        get
        {
            if (_followPoint == null)
            {
                _followPoint = transform.Find("followPoint");
            }
            return _followPoint;
        }
    }

    private Transform _geometryPoint;
    public Transform geometryPoint
    {
        get
        {
            if (_geometryPoint == null)
            {
                _geometryPoint = transform.Find("geometry_point");
            }
            return _geometryPoint;
        }
    }

    private EGeometryEnum _geometryType;
    public EGeometryEnum geometryType
    {
        get
        {
            return _geometryType;
        }
    }

    [SerializeField]
    private int _carIndex;
    public int carIndex
    {
        get
        {
            return _carIndex;
        }
    }



    public void Initialize(EGeometryEnum type, int index)
    {
        _geometryType = type;
        _carIndex = index;
    }

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GeometryAnimalActor : StudyBaseActor
{


    public enum AnimatorNameEnum
    {
        Static,
        Jump,
        StandUp,
        Walk,
    }

    private EGeometryAnimals _animalType;
    public EGeometryAnimals animalType
    {
        get
        {
            return _animalType;
        }
    }

    public void Initialize(EGeometryAnimals type)
    {
        _animalType = type;
    }

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }

    public void PlayAnimalAudio()
    {
        switch (animalType)
        {
            case EGeometryAnimals.Ji:
                this.PlayAudioEx(StudyAudioName.t_21008);
                break;
            case EGeometryAnimals.Mao:
                this.PlayAudioEx(StudyAudioName.t_21004);
                break;
            case EGeometryAnimals.Tuzi:
                this.PlayAudioEx(StudyAudioName.t_21006);
                break;
            case EGeometryAnimals.Niu:
                this.PlayAudioEx(StudyAudioName.t_21010);
                break;
            case EGeometryAnimals.Zhu:
                this.PlayAudioEx(StudyAudioName.t_21002);
                break;
            case EGeometryAnimals.Gou:
                this.PlayAudioEx(StudyAudioName.t_21012);
                break;
        }
    }


}

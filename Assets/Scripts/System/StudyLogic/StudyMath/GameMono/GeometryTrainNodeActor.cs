﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class GeometryTrainNodeActor : StudyBaseActor
{
    public enum AnimatorNameEnum
    {
        Move,
        Static,
        RightAneLeft,
        UpAndDown,
    }

    [SerializeField]
    private bool bIsComplete = false;
    public bool IsComplete
    {
        get { return bIsComplete; }
    }

    [SerializeField]
    private int _Number;
    public int Number
    {
        get { return _Number; }
    }

    public Vector3[] m_movePath;

    private Vector3 m_StartPoint;
    private Quaternion m_StartRotation;
    private GameObject m_ClickEffect;

    public void Initialize(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;

        m_StartPoint = position;
        m_StartRotation = rotation;
    }

    public void ResetStartPositionAndRotation()
    {
        float time = 0.3f;
        transform.DOMove(m_StartPoint, time).SetEase(Ease.InOutSine);
        transform.DORotateQuaternion(m_StartRotation, time).SetEase(Ease.InOutSine);
    }

    public void SetNodeNumber(int number)
    {
        _Number = number;
    }

    public void EndDrag(Vector3 position, Quaternion rotation)
    {
        bIsComplete = true;

        float time = 0.2f;
        transform.DOMove(position, time).SetEase(Ease.InOutSine);
        transform.DORotateQuaternion(rotation, time).SetEase(Ease.InOutSine);
    }

    public void MovePath(Vector3[] path, float duration, System.Action callback = null)
    {
        m_movePath = path;
        transform.DOPath(path, duration, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() =>
        {
            if (callback != null)
            {
                callback();
            }
        });
    }

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }

    public void SetNodeNumberTexture(int number)
    {
        Resource res = ResourceManager.Instance.GetResource(StudyPath.GetTrainNodeNumberTexturePath(number), typeof(Texture), enResourceType.Prefab);
        if (res.m_content != null)
        {
            meshRenderer.materials[2].SetTexture("_MainTex", res.m_content as Texture);
        }

    }

    public void PlayEffect(string effectPath)
    {
        if (m_ClickEffect == null)
        {
            m_ClickEffect = Res.LoadObj(effectPath);
            m_ClickEffect.transform.SetParent(this.transform);
            m_ClickEffect.transform.localPosition = new Vector3(-0.313f, 0.461f,0f);
            m_ClickEffect.transform.localScale = Vector3.one;
            m_ClickEffect.transform.localRotation = Quaternion.identity;
        }
        else
        {
            m_ClickEffect.SetActive(true);
        }
    }

    public void StopEffect()
    {
        if (m_ClickEffect != null && m_ClickEffect.activeSelf)
        {
            m_ClickEffect.SetActive(false);
        }
    }
}

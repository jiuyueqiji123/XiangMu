﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


/// <summary>
/// 无动画
/// </summary>
public class TrainHeadMono : MonoBehaviour {


    // ID从数字1开始
    [SerializeField]
    private int _Number;
    public int Number
    {
        get { return _Number; }
    }

    public Vector3[] movePath;

    private Texture _hightTexture = null;
    public Texture hightTexture
    {
        get
        {
            if (_hightTexture == null)
            {
                Resource res = ResourceManager.Instance.GetResource(sceneTextureBasePath + highLightNameList[_Number - 1], typeof(Texture), enResourceType.Prefab);
                if (res.m_content != null)
                {
                    _hightTexture = res.m_content as Texture;
                }
                else
                {
                    Debug.LogError("加载贴图失败!PATH:" + sceneTextureBasePath + highLightNameList[_Number - 1]);
                }
            }
            return _hightTexture;
        }
    }
    private Texture _unhightTexture = null;
    public Texture unhightTexture
    {
        get
        {
            if (_unhightTexture == null)
            {
                Resource res = ResourceManager.Instance.GetResource(sceneTextureBasePath + unhighLightNameList[_Number - 1], typeof(Texture), enResourceType.Prefab);
                if (res.m_content != null)
                {
                    _unhightTexture = res.m_content as Texture;
                }
                else
                {
                    Debug.LogError("加载贴图失败!PATH:" + sceneTextureBasePath + unhighLightNameList[_Number - 1]);
                }
            }
            return _unhightTexture;
        }
    }

    private MeshRenderer _meshRenderer = null;
    public MeshRenderer meshRenderer
    {
        get
        {
            if (_meshRenderer == null)
            {
                _meshRenderer = GetComponentInChildren<MeshRenderer>();
            }
            return _meshRenderer;
        }
    }


    private const string sceneTextureBasePath = "texture/texture_math_png/";

    // 高亮贴图
    List<string> highLightNameList = new List<string>() {
            "303020101_yellow_b_1",
            "303020101_red_b_2",
            "303020101_yellow_b_3",
            "303020101_red_b_4",
            "303020101_yellow_b_5",
            "303020101_red_b_6",
            "303020101_yellow_b_7",
            "303020101_red_b_8",
            "303020101_yellow_b_9",
            "303020101_red_b_10"
        };

    // 虚线贴图
    List<string> unhighLightNameList = new List<string>() {
            "303020101_yellow_a_1",
            "303020101_red_a_2",
            "303020101_yellow_a_3",
            "303020101_red_a_4",
            "303020101_yellow_a_5",
            "303020101_red_a_6",
            "303020101_yellow_a_7",
            "303020101_red_a_8",
            "303020101_yellow_a_9",
            "303020101_red_a_10"
        };



    public void Initialize(int number, bool bSetHighTexture)
    {
        _Number = number;
        
        SetHighTexture(bSetHighTexture);
    }

    public void SetHighTexture(bool val)
    {
        if (val)
        {
            Material[] materials = meshRenderer.materials;
            materials[2].SetTexture("_MainTex", hightTexture);
        }
        else
        {
            Material[] materials = meshRenderer.materials;
            materials[2].SetTexture("_MainTex", unhightTexture);
        }
    }

    public void SetTrainRedTexture()
    {
        Resource res = ResourceManager.Instance.GetResource(StudyPath.trainHeadRedTexture, typeof(Texture), enResourceType.Prefab);
        if (res.m_content != null)
        {
            Material[] materials = meshRenderer.materials;
            materials[0].SetTexture("_MainTex", res.m_content as Texture);
        }
    }

    public void SetTrainYellowTexture()
    {
        Resource res = ResourceManager.Instance.GetResource(StudyPath.trainHeadYellowTexture, typeof(Texture), enResourceType.Prefab);
        if (res.m_content != null)
        {
            Material[] materials = meshRenderer.materials;
            materials[0].SetTexture("_MainTex", res.m_content as Texture);
        }
    }

    public void MovePath(Vector3[] path, float duration, System.Action callback = null)
    {
        movePath = path;

        transform.DOPath(path, duration, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() =>
        {
            if (callback != null)
            {
                callback();
            }
        });
    }

}

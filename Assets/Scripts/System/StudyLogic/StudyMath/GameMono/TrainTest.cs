﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TrainTest : MonoBehaviour {


    public Transform[] path;

    public float moveTime = 5;

    
	IEnumerator Start ()
    {

        yield return new WaitForSeconds(2);

        List<Vector3> list = new List<Vector3>();
        foreach (Transform target in path)
        {
            list.Add(target.position);
        }
        transform.position = list[0];

        transform.DOPath(list.ToArray(), moveTime, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() =>
        {
            
        });

    }
	
}

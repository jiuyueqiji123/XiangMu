﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TrainMono : MonoBehaviour {


    // ID从数字1开始
    [SerializeField]
    private int _Number;
    public int Number
    {
        get { return _Number; }
    }


    public void Initialize(int number)
    {
        _Number = number;
    }

    public void MovePath(Vector3[] path, float duration, System.Action callback = null)
    {
        transform.DOPath(path, duration, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() => {
            if (callback != null)
            {
                callback();
            }
        });
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WoodActor : StudyBaseActor
{


    public enum AnimatorNameEnum
    {
        Static,
        Jump,
    }

    // 子物体木头名
    public readonly string woodName = "303020501";

    public readonly float zoomInValue = 2.5f;

    [SerializeField]
    private bool bIsComplete = false;
    public bool IsComplete
    {
        get { return bIsComplete; }
    }

    private bool bIsRoomIn;

    private Vector3 m_StartPoint;
    private Quaternion m_StartRotation;

    public void Initialize(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;

        m_StartPoint = position;
        m_StartRotation = rotation;
    }

    public void ResetStartPositionAndRotation(float duration = 0.1f)
    {
        transform.DOMove(m_StartPoint, duration).SetEase(Ease.InOutSine);
        transform.DORotateQuaternion(m_StartRotation, duration).SetEase(Ease.InOutSine);
    }

    public void EnableScaleAnimation(bool isZoomIn, float duration = 0.1f)
    {
        bIsRoomIn = isZoomIn;
        transform.DOKill();
        if (bIsRoomIn)
        {
            transform.DOScale(zoomInValue, duration).SetEase(Ease.InOutSine);
        }
        else
        {
            transform.DOScale(1, duration).SetEase(Ease.InOutSine);
        }
    }

    public void DragComplete(Vector3 position, Quaternion rotation)
    {
        float time = 0.2f;
        transform.DOScale(1, time).SetEase(Ease.InOutSine);
        transform.DOMove(position, time).SetEase(Ease.InOutSine);
        transform.DORotateQuaternion(rotation, time).SetEase(Ease.InOutSine);
    }

    public void EnableWood(bool value)
    {
        Transform wood = transform.Find(woodName);
        if (wood != null)
        {
            wood.gameObject.SetActive(value);
        }
    }

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MuBanActor : StudyBaseActor
{


    public enum AnimatorNameEnum
    {
        Static,
        Shake,
    }



    //private Vector3 m_StartPoint;
    //private Quaternion m_StartRotation;


    public void SetNumberTexture(int number)
    {
        Resource res = ResourceManager.Instance.GetResource(StudyPath.GetTrainNodeNumberTexturePath(number), typeof(Texture), enResourceType.Prefab);
        if (res.m_content != null)
        {
            meshRenderer.materials[1].SetTexture("_MainTex", res.m_content as Texture);
        }
    }

    //public void ResetStartPositionAndRotation(float duration = 0.1f)
    //{
    //    transform.DOMove(m_StartPoint, duration).SetEase(Ease.InOutSine);
    //    transform.DORotateQuaternion(m_StartRotation, duration).SetEase(Ease.InOutSine);
    //}

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }
    
    


}

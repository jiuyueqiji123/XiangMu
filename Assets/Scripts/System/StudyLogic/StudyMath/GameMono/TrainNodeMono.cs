﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class TrainNodeMono : MonoBehaviour
{

    public bool IsComplete
    {
        get { return bIsComplete; }
    }
    [SerializeField]
    private bool bIsComplete = false;

    public int NodeIndex
    {
        get { return index; }
    }
    [SerializeField]
    private int index;

    public Vector3[] m_movePath;

    public void EndDrag(int idx)
    {
        index = idx;
        bIsComplete = true;
    }

    public void MovePath(Vector3[] path, float duration, System.Action callback = null)
    {
        m_movePath = path;
        transform.DOPath(path, duration, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() =>
        {
            if (callback != null)
            {
                callback();
            }
        });
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeometryStudyToyBricksFlow : StudyMathBaseFlow
{


    GeometryStudyToyBricksPart part = new GeometryStudyToyBricksPart();

    bool isSdkEnable = false;


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        part.OnStart(OnPartComplete);

        SDKManager.Instance.Event(UmengEvent.GeometryJiMu);
        SDKManager.Instance.StartLevel(UmengLevel.GeometryJiMu);
        Debug.LogError("---------------------  On Event GeometryJiMu!");
        Debug.LogError("---------------------  On StartLevel GeometryJiMu!");
        isSdkEnable = true;
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();
        part.OnUpdate();
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        part.OnExit();

        if (isSdkEnable)
        {
            SDKManager.Instance.FinishLevel(UmengLevel.GeometryJiMu);
            Debug.LogError("---------------------  On FinishLevel GeometryJiMu");
            isSdkEnable = false;
        }
    }

    private void OnPartComplete(bool bComplete)
    {
        OnAllPartComplete();
    }

    private void OnAllPartComplete()
    {
        this.PlayActionAudioEx(ActionAudioName.t_2012, () =>
        {
            OnFlowComplete();
        });
    }

}

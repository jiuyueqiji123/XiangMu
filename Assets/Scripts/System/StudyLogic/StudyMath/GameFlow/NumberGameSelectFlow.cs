﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberGameSelectFlow : StudyMathBaseFlow
{


    NumberGameSelectPart part = new NumberGameSelectPart();    // 选择游戏

    protected override void OnFlowStart(params object[] paramArray)
    {
        base.OnFlowStart(paramArray);
        part.OnStart(OnNumberGameSelectPartComplete, paramArray);
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();
        part.OnUpdate();
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        part.OnExit();
    }

    private void OnNumberGameSelectPartComplete(bool bComplete, object[] paramArray)
    {
        NumberController.ENumberGameType type = (NumberController.ENumberGameType)paramArray[0];
        //part.OnExit();
        OnAllPartComplete(true, type);

    }

    private void OnAllPartComplete(bool bComplete, NumberController.ENumberGameType type)
    {
        OnFlowComplete(bComplete, type);
    }

}
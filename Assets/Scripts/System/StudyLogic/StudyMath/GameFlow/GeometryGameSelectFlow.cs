﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeometryGameSelectFlow : StudyMathBaseFlow
{


    GeometryGameSelectPart part = new GeometryGameSelectPart();    // 选择游戏

    protected override void OnFlowStart(params object[] paramArray)
    {
        base.OnFlowStart(paramArray);
        part.OnStart(OnGeometryGameSelectPartComplete, paramArray);
    }

    protected override void OnFlowUpdate()
    {
        base.OnUpdate();
        part.OnUpdate();
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        part.OnExit();
    }

    private void OnGeometryGameSelectPartComplete(bool bComplete, object[] paramArray)
    {
        GeometryController.EGeometryGameType type = (GeometryController.EGeometryGameType)paramArray[0];
        //part.OnExit();
        OnAllPartComplete(true, type);

    }

    private void OnAllPartComplete(bool bComplete, GeometryController.EGeometryGameType type)
    {
        OnFlowComplete(bComplete, type);
    }

}
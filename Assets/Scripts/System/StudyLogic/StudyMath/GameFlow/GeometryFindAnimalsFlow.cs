﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Extension;


public class GeometryFindAnimalsFlow : StudyMathBaseFlow
{


    GeometryFindAnimalsPart part = new GeometryFindAnimalsPart();

    private const string scenePath = StudyPath.geometry_track_1_scene;
    private const string geometryTrackPath = StudyPath.geometry_track_findanimals;

    private int m_partIndex;
    bool isSdkEnable = false;

    private List<int> m_playedList = new List<int>();


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        CreateScene(scenePath);
        CreateGeometryTrack();

        m_playedList.Clear();
        m_partIndex = 0;
        StartPart();

        SDKManager.Instance.Event(UmengEvent.GeometryNiu);
        SDKManager.Instance.StartLevel(UmengLevel.GeometryNiu);
        AdsManager.Instance.HideAds(AdsNameConst.Ads_Math);
        Debug.LogError("---------------------  On Event GeometryNiu!");
        Debug.LogError("---------------------  On StartLevel GeometryNiu!");
        isSdkEnable = true;
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();
        part.OnUpdate();
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        part.OnExit();

        m_partIndex = 0;

        if (isSdkEnable)
        {
            SDKManager.Instance.FinishLevel(UmengLevel.GeometryNiu);
            Debug.LogError("---------------------  On FinishLevel GeometryNiu");
            isSdkEnable = false;
        }
        AdsManager.Instance.ShowAds(AdsNameConst.Ads_Math);
    }

    private void StartPart()
    {
        part.OnStart((result, paramArray) =>
        {
            if (m_partIndex > 5)
            {
                OnAllPartComplete();
            }
            else
            {
                m_playedList.Add((int)paramArray[0]);
                part.OnExit();
                StartPart();
            }
        }, Scene, m_partIndex++, m_playedList);
    }

    private void OnAllPartComplete()
    {
        this.PlayActionAudioEx(ActionAudioName.t_2011, () =>
        {
            OnFlowComplete();
        });
    }

    private void CreateGeometryTrack()
    {
        GameObject go = Instantiate(geometryTrackPath, true);
        if (go == null)
        {
            Debug.LogError("Load obj faild! path:" + geometryTrackPath);
            return;
        }

        go.transform.SetParent(Scene);
        go.name = geometryTrackPath.GetPathName();
        go.ResetTransformForLocal();

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainNumberFlow : StudyMathBaseFlow
{

    // 选择火车数字
    SelectTrainNumberPart selectNumberPart = new SelectTrainNumberPart();
    // 画数字，拖车厢
    DrawTrainNumberPart drawNumberPart = new DrawTrainNumberPart();

    List<int> drawNumberList = new List<int>();
    bool isSdkEnable = false;

    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        drawNumberList.Clear();
        selectNumberPart.OnStart(OnTrainNumberPartComplete, drawNumberList);

        SDKManager.Instance.Event(UmengEvent.NumberTrain);
        SDKManager.Instance.StartLevel(UmengLevel.NumberTrain);
        Debug.LogError("---------------------  On Event NumberTrain!");
        Debug.LogError("---------------------  On StartLevel NumberTrain!");
        isSdkEnable = true;
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();

        selectNumberPart.OnUpdate();
        drawNumberPart.OnUpdate();
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        selectNumberPart.OnExit();
        drawNumberPart.OnExit();
        drawNumberList.Clear();

        if (isSdkEnable)
        {
            SDKManager.Instance.FinishLevel(UmengLevel.NumberTrain);
            Debug.LogError("---------------------  On FinishLevel NumberTrain");
            isSdkEnable = false;
        }
    }

    private void OnTrainNumberPartComplete(bool bComplete, object[] paramArray)
    {
        int number = (int)paramArray[0];

        this.StartLoadingAndEndEx(() =>
        {
            selectNumberPart.OnExit();
            // 开始书写数字场景
            drawNumberPart.OnStart(OnDrawTrainNumberPartComplete, number);
        });

    }

    private void OnDrawTrainNumberPartComplete(bool bComplete, object[] paramArray)
    {
        int number = (int)paramArray[0];
        Debug.Log("-----------------: OnDrawTrainNumberPartComplete! number:" + number);

        drawNumberPart.OnExit();
        drawNumberList.Add(number);

        if (drawNumberList.Count >= 3)
        {
            selectNumberPart.OnStart((bResult)=> {

                FlowComplete(bResult);

            }, drawNumberList);
        }
        else
        {
            selectNumberPart.OnStart(OnTrainNumberPartComplete, drawNumberList);
        }
    }

    private void FlowComplete(bool bComplete)
    {
        this.PlayActionAudioEx(ActionAudioName.t_1007, () =>
        {
            OnFlowComplete(bComplete, drawNumberList);
        });
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Extension;

/// <summary>
/// 找积木
/// </summary>
public class GeometryFindToyBricksFlow : StudyMathBaseFlow
{


    GeometryFindToyBricksPart part = new GeometryFindToyBricksPart();


    private const string scenePath = StudyPath.geometry_track_scene;
    private const string geometryTrackPath = StudyPath.geometry_track_findbricks;

    private int m_partIndex;

    bool isSdkEnable = false;


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        CreateScene(scenePath);
        CreateGeometryTrack();

        m_partIndex = 0;
        StartPart();

        SDKManager.Instance.Event(UmengEvent.GeometryTrain);
        SDKManager.Instance.StartLevel(UmengLevel.GeometryTrain);
        Debug.LogError("---------------------  On Event GeometryTrain!");
        Debug.LogError("---------------------  On StartLevel GeometryTrain!");
        isSdkEnable = true;
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();
        part.OnUpdate();
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        part.OnExit();
        m_partIndex = 0;

        if (isSdkEnable)
        {
            SDKManager.Instance.FinishLevel(UmengLevel.GeometryTrain);
            Debug.LogError("---------------------  On FinishLevel GeometryTrain");
            isSdkEnable = false;
        }
    }

    private void StartPart()
    {
        part.OnStart((bComplete)=> {

            if (m_partIndex > 5)
            {
                OnAllPartComplete();
            }
            else
            {
                part.OnExit();
                StartPart();
            }
        }, Scene, m_partIndex++);
    }

    private void OnAllPartComplete()
    {
        this.PlayActionAudioEx(ActionAudioName.t_2012, () =>
        {
            OnFlowComplete();
        });
    }

    private void CreateGeometryTrack()
    {
        GameObject go = Instantiate(geometryTrackPath, true);
        if (go == null)
        {
            Debug.LogError("Load obj faild! path:" + geometryTrackPath);
            return;
        }

        go.transform.SetParent(Scene);
        go.name = geometryTrackPath.GetPathName();// "Geometry_FindBricks";
        go.ResetTransformForLocal();

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainBallFlow : StudyMathBaseFlow
{


    // 选个球
    SelectTrainBallPart selectBallPart = new SelectTrainBallPart();

    // 所有玩过的数字列表
    List<int> allPlayNumberList = new List<int>();
    // 当前关卡玩的数字列表
    List<int> playNumberList = new List<int>();
    bool isSdkEnable = false;


    protected override void OnFlowStart(params object[] paramArray)
    {
        base.OnFlowStart(paramArray);

        allPlayNumberList = (List<int>)paramArray[0];
        playNumberList.Clear();
        selectBallPart.OnStart(OnSelectTrainBallPartComplete, allPlayNumberList);

        SDKManager.Instance.Event(UmengEvent.NumberBall);
        SDKManager.Instance.StartLevel(UmengLevel.NumberBall);
        Debug.LogError("---------------------  On Event NumberBall!");
        Debug.LogError("---------------------  On StartLevel NumberBall!");
        isSdkEnable = true;
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        selectBallPart.OnExit();

        if (isSdkEnable)
        {
            SDKManager.Instance.FinishLevel(UmengLevel.NumberBall);
            Debug.LogError("---------------------  On FinishLevel NumberBall");
            isSdkEnable = false;
        }
    }

    private void OnSelectTrainBallPartComplete(bool bComplete, object[] paramArray)
    {
        //selectBallPart.OnExit();
        List<int> number = (List<int>)paramArray[0];
        FlowComplete(number);
    }

    private void FlowComplete(List<int> number)
    {
        this.PlayActionAudioEx(ActionAudioName.t_1007, () =>
        {
            OnFlowComplete(true, number);
        });
    }


}

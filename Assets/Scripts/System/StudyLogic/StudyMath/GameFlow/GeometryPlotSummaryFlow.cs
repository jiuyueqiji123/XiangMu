﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeometryPlotSummaryFlow : StudyMathBaseFlow
{


    GeometryPlotSummaryPart part = new GeometryPlotSummaryPart();


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        part.OnStart(OnPartComplete);
    }

    protected override void OnFlowUpdate()
    {
        part.OnUpdate();
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        part.OnExit();
    }

    private void OnPartComplete(bool bComplete)
    {
        //part.OnExit();
        OnAllPartComplete();
    }

    private void OnAllPartComplete()
    {
        OnFlowComplete();
    }

}

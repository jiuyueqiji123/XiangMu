﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 铺枕木
public class TrainTrackFlow : StudyMathBaseFlow
{

    // 拖枕木
    DragTrainTrackPart dragTrackPart = new DragTrainTrackPart();

    // 之前关卡玩过的数字列表
    List<int> allPlayNumberList = new List<int>();
    // 当前玩的数字列表
    List<int> playNumberList = new List<int>();
    bool isSdkEnable = false;


    protected override void OnFlowStart(params object[] paramArray)
    {
        base.OnFlowStart(paramArray);

        allPlayNumberList = (List<int>)paramArray[0];
        playNumberList.Clear();
        List<int> list = new List<int>();
        list.AddRange(allPlayNumberList);

        dragTrackPart.OnStart(OnTrainTrackPartComplete, list, true);

        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Four);

        SDKManager.Instance.Event(UmengEvent.NumberMuTou);
        SDKManager.Instance.StartLevel(UmengLevel.NumberMuTou);
        Debug.LogError("---------------------  On Event NumberMuTou!");
        Debug.LogError("---------------------  On StartLevel NumberMuTou!");
        isSdkEnable = true;

    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();

        dragTrackPart.OnUpdate();
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        dragTrackPart.OnExit();
        playNumberList.Clear();

        if (isSdkEnable)
        {
            SDKManager.Instance.FinishLevel(UmengLevel.NumberMuTou);
            Debug.LogError("---------------------  On FinishLevel NumberMuTou");
            isSdkEnable = false;
        }
    }

    private void OnTrainTrackPartComplete(bool bComplete, object[] paramArray)
    {
        int number = (int)paramArray[0];

        playNumberList.Add(number);
        if (playNumberList.Count >= 4)
        {
            OnAllFlowComplete();
        }
        else
        {
            dragTrackPart.OnExit();
            List<int> list = new List<int>();
            list.AddRange(allPlayNumberList);
            list.AddRange(playNumberList);
            dragTrackPart.OnStart(OnTrainTrackPartComplete, list, false);
        }
    }

    private void OnAllFlowComplete()
    {
        this.PlayActionAudioEx(ActionAudioName.t_1006, () =>
        {
            OnFlowComplete(true, playNumberList);
        });
    }

}

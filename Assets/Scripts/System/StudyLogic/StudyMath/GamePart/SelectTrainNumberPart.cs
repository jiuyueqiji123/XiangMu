﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;

public class SelectTrainNumberPart : StudyMathBasePart
{


    private const string scenePath = StudyPath.math_selecttrain_scene;
    private const string assetPath = StudyPath.math_trainHeadPath;    // 火车头

    private Dictionary<int, TrainHeadActor> m_trainDict = new Dictionary<int, TrainHeadActor>();

    private int[] drawNumberArray;

    //private bool bPlayFirstAudio;

    protected bool bLockInput = false;


    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        drawNumberArray = ((List<int>)paramArray[0]).ToArray();
        bLockInput = true;

        CreateScene(scenePath);
        CreateTrain();
        //if (drawNumberArray.Length == 0)    // 游戏完成3轮
        if (drawNumberArray.Length >= 3)    // 游戏完成3轮
        {
            SetAllTrainHighLight();

            //this.PlayAudioEx(StudyAudioName.t_11018);
            MedalWindow.Instance.ShowXunzhang(drawNumberArray.Length, true, MedalWindow.EXZCountEnum.Three, () =>
            {
                OnPartComplete();
            });
        }
        else
        {
            SetHighLightTrain();
            //this.PlayAudioEx(StudyAudioName.t_11018);
            MedalWindow.Instance.ShowXunzhang(drawNumberArray.Length, true, MedalWindow.EXZCountEnum.Three, ()=> {
            });

            this.AddTimerEx(delayTime, () =>
            {
                // 首次进来
                if (drawNumberArray.Length == 0)
                {
                    GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_12004);
                    this.AddTimerEx(audio.Length, () => {

                        bLockInput = false;
                        ShowTrainEffect();
                    });
                }
                else
                {
                        bLockInput = false;
                        ShowTrainEffect();
                }
            });
        }
    }

    protected override void OnPartExit()
    {
        base.OnPartExit();
        
        m_trainDict.Clear();
        drawNumberArray = null;
        //System.Array.Clear(drawNumberArray, 0, drawNumberArray.Length);
    }
    protected override void AddEvent()
    {
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
    }

    protected override void RemoveEvent()
    {
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
    }

    protected override void OnMouseDown()
    {
        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target == null)
            return;

        TrainHeadActor mono = target.GetComponent<TrainHeadActor>();
        if (mono == null)
        {
            target.AddComponentEx<StudyFixedActor>().CrossFade(StudyFixedActor.AnimatorNameEnum.Jump, 0.01f);
            return;
        }

        if (bLockInput)
        {
            mono.CrossFade(TrainHeadActor.AnimatorNameEnum.UpAndDown, 0.01f);
            //PlayMathAudio(StudyAudioName.TrainNodeJumpAudio);
            return;
        }

        int number = mono.Number;
        bool bPlay = true;
        for (int i = 0; i < drawNumberArray.Length; i++)
        {
            if (drawNumberArray[i] == number)
            {
                bPlay = false;
                break;
            }
        }
        if (bPlay)
        {
            bLockInput = true;
            OnPartComplete(true, number);
        }
        //PlayAudio(StudyAudioName.Math_ClickAudio);

    }

    private void CreateTrain()
    {
        List<string> trainNameList = new List<string>() {
            "303020101_1",
            "303020101_2",
            "303020101_3",
            "303020101_4",
            "303020101_5",
            "303020101_6",
            "303020101_7",
            "303020101_8",
            "303020101_9",
            "303020101_10"};

        m_trainDict.Clear();
        for (int i = 0; i < trainNameList.Count; i++)
        {
            Transform target = Scene.Find(trainNameList[i]);
            int number = i + 1;

            GameObject go = Instantiate(assetPath, true);
            if (go != null)
            {
                go.transform.position = target.position;
                go.transform.rotation = target.rotation;
                go.transform.localScale = target.localScale;

                TrainHeadActor trainMono = go.AddComponent<TrainHeadActor>();

                if (i % 2 == 0)
                {
                    trainMono.SetTrainRedTexture();
                }
                else
                {
                    trainMono.SetTrainYellowTexture();
                }

                trainMono.Initialize(number, false);
                m_trainDict.Add(number, trainMono);
            }
        }

    }

    private void SetHighLightTrain()
    {
        for (int i = 0; i < drawNumberArray.Length; i++)
        {
            int number = drawNumberArray[i];
            if (m_trainDict.ContainsKey(number))
            {
                m_trainDict[number].SetHighTexture(true);
            }
            else
            {
                Debug.LogError("设置火车材质错误！");
            }
        }
    }

    private void SetAllTrainHighLight()
    {
        foreach (TrainHeadActor actor in m_trainDict.Values)
        {
            actor.SetHighTexture(true);
        }
    }

    private void ShowTrainEffect()
    {
        foreach (TrainHeadActor actor in m_trainDict.Values)
        {
            actor.PlayHintEffect(StudyPath.math_traineffect_hint);
        }
        for (int i = 0; i < drawNumberArray.Length; i++)
        {
            int number = drawNumberArray[i];
            if (m_trainDict.ContainsKey(number))
            {
                m_trainDict[number].ClearAllEffect();
            }
        }

    }


}

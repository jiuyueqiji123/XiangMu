﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;

public class GeometryGameSelectPart : StudyMathBasePart
{


    private const string scenePath = StudyPath.geometry_scene_303040000;

    
    protected bool bLockInput = false;


    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        bool isFirstPlay = (bool)paramArray[0];
        bLockInput = false;
        CreateScene(scenePath);
        SetGameProgress();

        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.None);

        if (isFirstPlay)
        {
            this.PlayActionAudioEx(ActionAudioName.t_2003);
        }
    }
    
    protected override void OnMouseDown()
    {
        if (bLockInput)
            return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target == null)
            return;

        GeometryController.EGeometryGameType type = GeometryController.EGeometryGameType.PlotSummary;
        switch (target.name)
        {
            case "Game_1":
                type = GeometryController.EGeometryGameType.PlotSummary;
                target.AddComponentEx<StudyFixedActor>().CrossFade(StudyFixedActor.AnimatorNameEnum.Jump, 0.01f);
                this.PlayAudioEx(StudyAudioName.t_21023);
                break;
            case "Game_2":
                type = GeometryController.EGeometryGameType.ToyBrick;
                target.AddComponentEx<TrainHeadActor>().CrossFade(TrainHeadActor.AnimatorNameEnum.UpAndDown, 0.01f);
                this.PlayAudioEx(StudyAudioName.t_11005);
                break;
            case "Game_3":
                type = GeometryController.EGeometryGameType.Animal;
                target.AddComponentEx<GeometryAnimalActor>().CrossFade(GeometryAnimalActor.AnimatorNameEnum.Jump, 0.01f);
                this.PlayAudioEx(StudyAudioName.t_21010);
                break;
            default:

                target.AddComponentEx<StudyFixedActor>().CrossFade(StudyFixedActor.AnimatorNameEnum.Jump, 0.01f);

                return;
        }

        bLockInput = true;

        MathNumberWindow.Instance.UpdateLockInputState(true);
        AddTimer(1000, () => {

            OnPartComplete(true, type);
            MathNumberWindow.Instance.UpdateLockInputState(false);
        });

    }

    private void SetGameProgress()
    {
        StudyGameProgressInfo info = LocalDataManager.Instance.Load<StudyGameProgressInfo>(ELocalDataType.StudyGeometryGame);

        Transform game1 = Scene.Find("Game_1");
        Transform game2 = Scene.Find("Game_2");
        Transform game3 = Scene.Find("Game_3");

        switch ((GeometryController.EGeometryGameType)info.gameProgress)
        {
            case GeometryController.EGeometryGameType.None:
                PlayEffect(StudyPath.math_traineffect_hint, game1.Find("HintEffect"), 0);
                game2.GetComponent<BoxCollider>().enabled = false;
                game3.GetComponent<BoxCollider>().enabled = false;

                break;
            case GeometryController.EGeometryGameType.PlotSummary:
                PlayEffect(StudyPath.math_traineffect_hint, game1.Find("HintEffect"), 0);
                PlayEffect(StudyPath.math_traineffect_hint, game2.Find("HintEffect"), 0);
                game3.GetComponent<BoxCollider>().enabled = false;

                break;
            default:

                PlayEffect(StudyPath.math_traineffect_hint, game1.Find("HintEffect"), 0);
                PlayEffect(StudyPath.math_traineffect_hint, game2.Find("HintEffect"), 0);
                PlayEffect(StudyPath.math_traineffect_hint, game3.Find("HintEffect"), 0);

                break;
        }

    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;

public class NumberGameSelectPart : StudyMathBasePart
{


    private const string scenePath = StudyPath.math_scene_303020000;

    protected bool bLockInput = false;


    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);
        bool isFirstPlay = (bool)paramArray[0];

        bLockInput = false;

        CreateScene(scenePath);
        SetGameProgress();

        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.None);

        if (isFirstPlay)
        {
            this.PlayActionAudioEx(ActionAudioName.t_1002);
        }
    }

    protected override void OnMouseDown()
    {
        if (bLockInput)
            return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target == null)
            return;
        //Debug.LogError(target.name);
        NumberController.ENumberGameType type = NumberController.ENumberGameType.TrainNumber;
        switch (target.name)
        {
            case "Game_1":
                type = NumberController.ENumberGameType.TrainNumber;
                target.AddComponentEx<TrainHeadActor>().CrossFade(TrainHeadActor.AnimatorNameEnum.UpAndDown, 0.01f);
                this.PlayAudioEx(StudyAudioName.t_11005);
                //PlayMathAudio(StudyAudioName.TrainNodeJumpAudio);
                break;
            case "Game_2":
                type = NumberController.ENumberGameType.TrainBall;
                target.AddComponentEx<StudyFixedActor>().CrossFade(StudyFixedActor.AnimatorNameEnum.Jump, 0.01f);
                this.PlayAudioEx(StudyAudioName.t_11015);
                break;
            case "Game_3":
            case "Game_4":
                type = NumberController.ENumberGameType.TrainTrack;
                target.AddComponentEx<WoodActor>().CrossFade(WoodActor.AnimatorNameEnum.Jump, 0.01f);
                //PlayMathAudio(StudyAudioName.ZhenmuJumpAudio);
                this.PlayAudioEx(StudyAudioName.t_11008);
                break;
            default:

                target.AddComponentEx<StudyFixedActor>().CrossFade(StudyFixedActor.AnimatorNameEnum.Jump, 0.01f);

                return;
        }

        bLockInput = true;
        MathNumberWindow.Instance.UpdateLockInputState(true);
        AddTimer(1000, () =>
        {
            OnPartComplete(true, type);
            MathNumberWindow.Instance.UpdateLockInputState(false);
        });

    }

    private void SetGameProgress()
    {
        StudyGameProgressInfo info = LocalDataManager.Instance.Load<StudyGameProgressInfo>(ELocalDataType.StudyMathGame);
        Debug.LogError(info.gameProgress);
        Transform game1 = Scene.Find("Game_1");
        Transform game2 = Scene.Find("Game_2");
        Transform game3 = Scene.Find("Game_3");
        Transform game4 = Scene.Find("Game_4");
        
        switch ((NumberController.ENumberGameType)info.gameProgress)
        {
            case NumberController.ENumberGameType.None:
                PlayEffect(StudyPath.math_traineffect_hint, game1.Find("HintEffect"), 0);
                game2.GetComponent<BoxCollider>().enabled = false;
                game3.GetComponent<BoxCollider>().enabled = false;
                game4.GetComponent<BoxCollider>().enabled = false;

                break;
            case NumberController.ENumberGameType.TrainNumber:
                PlayEffect(StudyPath.math_traineffect_hint, game1.Find("HintEffect"), 0);
                PlayEffect(StudyPath.math_traineffect_hint, game2.Find("HintEffect"), 0);
                game3.GetComponent<BoxCollider>().enabled = false;
                game4.GetComponent<BoxCollider>().enabled = false;

                break;
            default:

                PlayEffect(StudyPath.math_traineffect_hint, game1.Find("HintEffect"), 0);
                PlayEffect(StudyPath.math_traineffect_hint, game2.Find("HintEffect"), 0);
                PlayEffect(StudyPath.math_traineffect_hint, game3.Find("HintEffect"), 0);
                PlayEffect(StudyPath.math_traineffect_hint, game4.Find("HintEffect"), 0);

                break;
        }

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using WCBG.ToolsForUnity.Tools;



public class GeometryStudyToyBricksPart : StudyGeometryBasePart
{


    private const string scenePath = StudyPath.geometry_notrack_scene;
    private const string geometryPointPath = StudyPath.geometry_point;
    private const float HINT_INTERVAL_TIME = 1;

    private const string geometry_assetpath1 = StudyPath.geometry_circle_5;
    private const string geometry_assetpath2 = StudyPath.geometry_square_2;
    private const string geometry_assetpath3 = StudyPath.geometry_triangle_1;
    private const string geometry_assetpath4 = StudyPath.geometry_rectangle_2;
    private const string geometry_assetpath5 = StudyPath.geometry_trapezoid_1;
    private const string geometry_assetpath6 = StudyPath.geometry_ellipse_3;

    private const string geometry_ball = StudyPath.geometry_ball;
    private const string geometry_flag = StudyPath.geometry_flag;
    private const string geometry_door = StudyPath.geometry_door;
    private const string geometry_fangkuai = StudyPath.geometry_fangkuai;
    private const string geometry_ladder = StudyPath.geometry_ladder;
    private const string geometry_rugby = StudyPath.geometry_rugby;

    private Transform m_scene;
    private Transform m_geometryMask;
    private Transform m_geometryCenter;
    private GameObject m_currentEffect;
    private int m_index;
    private bool bStudying;
    private bool isHint;
    private float addTime;


    private List<Transform> m_geometryList = new List<Transform>();
    private List<Transform> m_geometryPointList = new List<Transform>();


    protected override void OnPartStart()
    {
        bStudying = false;
        bLockInput = true;
        m_index = 0;
        addTime = 0;
        isHint = false;

        m_scene = CreateScene(scenePath);
        CreateGeometry();


        this.PlayActionAudioEx(ActionAudioName.t_2004, () => {

            bLockInput = false;
            UpdateHintEffect();
        });

        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Six);

        RefreshShadowProjecter();
    }

    protected override void OnPartUpdate()
    {
        base.OnPartUpdate();

        if (isHint)
        {
            addTime += Time.deltaTime;
            if (addTime > HINT_INTERVAL_TIME)
            {
                addTime = 0;
                Transform target = GetCurrentGeometry();
                if (target != null)
                {
                    target.GetComponent<GeometryActor>().CrossFade(GeometryActor.AnimatorNameEnum.Shake, 0.01f);
                }
            }
        }
    }

    protected override void OnPartExit()
    {
        base.OnPartExit();

        addTime = 0;
        isHint = false;
    }

    protected override void OnMouseDown()
    {
        base.OnMouseDown();

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target == null)
            return;

        GeometryActor actor = target.GetComponent<GeometryActor>();
        if (actor == null)
            return;
        //Debug.Log(actor.name + " - " + actor.geometryType);

        if (bLockInput)
        {
            actor.CrossFade(GeometryActor.AnimatorNameEnum.Shake, 0.01f);
            actor.PlayGeometryActorAudio();
            return;
        }
        if (bStudying)
        {
            actor.CrossFade(GeometryActor.AnimatorNameEnum.Shake, 0.01f);
            return;
        }

        //bStudying = true;
        //StartStudyTarget(actor);
        //return;

        if (target == GetCurrentGeometry())
        {
            bStudying = true;
            StartStudyTarget(actor);
        }
        else
        {
            actor.CrossFade(GeometryActor.AnimatorNameEnum.Shake, 0.01f);
            actor.PlayGeometryActorAudio();
        }

    }

    private void OnStudyComplete()
    {
        bLockInput = true;

        //PlayerAnimationController.Instance.PlayAnimaton(PlayerAnimationController.PlayerAnimationEnum.LeftBottomToHalf, LediActor.AnimatorNameEnum.exciting_p, StudyPath.study_geometry_audio_basepath + StudyAudioName.talk_403040218, () =>
        //{
            OnPartComplete();
        //});
    }

    private void StartStudyTarget(GeometryActor actor)
    {
        if (m_currentEffect != null)
        {
            m_currentEffect.SetActive(false);
        }

        PlayGeometryAudio(actor.geometryType, () =>
        {
            WindowManager.Instance.OpenWindow(WinNames.DrawGeometryPanel, EWindowFadeEnum.ScaleIn);
            DrawGeometryWindow.Instance.StartDrawGeometry(actor.geometryType, (result) =>
            {
                WindowManager.Instance.CloseWindow(WinNames.DrawGeometryPanel);
                if (result)
                {
                    m_index++;

                    //this.PlayAudioEx(StudyAudioName.t_21025);
                    MedalWindow.Instance.ShowXunzhang(m_index, true, MedalWindow.EXZCountEnum.Six, () =>
                    {
                        UpdateHintEffect();
                        PlayHeartenAudio(() =>
                        {
                            bStudying = false;
                            if (m_index >= m_geometryList.Count)
                            {
                                OnStudyComplete();
                            }
                        });
                    });
                }
                else
                {
                    bStudying = false;
                }

            });

        });

        return;

        //actor.transform.localScale = Vector3.zero;
        actor.transform.position = m_geometryCenter.position;
        actor.transform.rotation = m_geometryCenter.rotation;
        if (actor.geometryType == EGeometryEnum.Rectangle)
        {
            actor.transform.Rotate(new Vector3(0, 90, 0), Space.Self);
        }

        //PlayEffect(StudyPath.effect_502003032, actor.transform.position, 3);
        actor.CrossFade(GeometryActor.AnimatorNameEnum.ScaleIn, 0.01f);

        m_geometryMask.gameObject.SetActive(true);
        //actor.transform.DOScale(1, 1f).SetEase(Ease.InQuad);
        AddTimer(1, ()=> {

            GameAudioSource audio = PlayLearnGeometryAudio(actor.geometryType);

            AddTimer(audio.Length - 1f, () => {
                actor.CrossFade(GeometryActor.AnimatorNameEnum.ScaleOut, 0.01f);
            });

            AddTimer(audio.Length, () => {

                Transform geometryPoint = GetCurrentGeometryPoint();
                if (geometryPoint != null)
                {
                    actor.CrossFade(GeometryActor.AnimatorNameEnum.Static, 0.01f);
                    actor.transform.position = geometryPoint.position;
                    actor.transform.rotation = geometryPoint.rotation;
                }

                ShowLearnModel(actor.geometryType, () =>
                {
                    WindowManager.Instance.OpenWindow(WinNames.DrawGeometryPanel);
                    DrawGeometryWindow.Instance.StartDrawGeometry(actor.geometryType, (result) =>
                    {
                        if (result)
                        {
                            WindowManager.Instance.CloseWindow(WinNames.DrawGeometryPanel);
                        }

                        m_geometryMask.gameObject.SetActive(false);
                        m_index++;

                        bool _bComplete = false;
                        
                        //this.PlayAudioEx(StudyAudioName.t_21025);
                        MedalWindow.Instance.ShowXunzhang(m_index, true, MedalWindow.EXZCountEnum.Six, ()=> {

                            if (m_index >= m_geometryList.Count)
                            {
                                if (_bComplete)
                                    OnStudyComplete();
                                else
                                    _bComplete = true;
                            }

                            UpdateHintEffect();
                        });

                        PlayHeartenAudio(()=> {

                            bStudying = false;
                            if (m_index >= m_geometryList.Count)
                            {
                                if (_bComplete)
                                    OnStudyComplete();
                                else
                                    _bComplete = true;
                            }
                        });
                    });
                });
            });
        });
    }

    private void PlayHeartenAudio(System.Action callback)
    {
        switch (m_partIndex)
        {
            case 0:
            case 1:
            case 3:
            case 4:
                if (callback != null)
                    callback();
                break;
            case 2:
                this.PlayActionAudioEx(ActionAudioName.t_2005, callback);
                break;
            case 5:
                if (callback != null)
                    callback();
                break;
        }
    }

    private void ShowLearnModel(EGeometryEnum type, System.Action callback)
    {
        string targetpath = "";
        switch (type)
        {
            case EGeometryEnum.Circle:
                targetpath = geometry_ball;
                break;
            case EGeometryEnum.Square:
                targetpath = geometry_fangkuai;
                break;
            case EGeometryEnum.Triangle:
            //case EGeometryEnum.Triangle_1:
            //case EGeometryEnum.Triangle_2:
            //case EGeometryEnum.RightTriangle:
                targetpath = geometry_flag;
                break;
            case EGeometryEnum.Rectangle:
                targetpath = geometry_door;
                break;
            case EGeometryEnum.Trapezoid:
            //case EGeometryEnum.Trapezoid_1:
            //case EGeometryEnum.Trapezoid_2:
            //case EGeometryEnum.Trapezoid_3:
                targetpath = geometry_ladder;
                break;
            case EGeometryEnum.Ellipse:
                targetpath = geometry_rugby;
                break;
            default:
                targetpath = geometry_ball;
                break;
        }
        GameObject go = Instantiate(targetpath, true);
        if (go == null)
        {
            if (callback != null)
                callback();
            Debug.LogError("Load obj error! path:" + targetpath);
            return;
        }

        //go.transform.localScale = Vector3.zero;
        go.transform.position = m_geometryCenter.position;
        go.transform.rotation = m_geometryCenter.rotation;
        float time = 1;
        //go.transform.DOScale(1, time).SetEase(Ease.InQuad);

        go.AddComponentEx<GeometryActor>().CrossFade(GeometryActor.AnimatorNameEnum.ScaleIn, 0.01f);
        //PlayEffect(StudyPath.effect_502003033, go.transform.position, 3);

        AddTimer(time + 2, () =>
        {
            go.AddComponentEx<GeometryActor>().CrossFade(GeometryActor.AnimatorNameEnum.ScaleOut, 0.01f);
        });
        AddTimer(time + 3, () => {

            go.SetActive(false);
            if (callback != null)
                callback();

        });

    }

    private void UpdateHintEffect()
    {
        isHint = true;
        addTime = 0;
        //if (m_currentEffect != null)
        //{
        //    m_currentEffect.SetActive(false);
        //}
        //Transform target = GetCurrentGeometry();
        //if (target != null)
        //{
        //    m_currentEffect = PlayEffect(StudyPath.math_traineffect_hint, target, 0);
        //    //m_currentEffect.transform.localPosition = new Vector3(-4.6f, 30.5f, 19);
        //}
    }

    private Transform GetCurrentGeometry()
    {
        if (m_index < m_geometryList.Count)
        {
            return m_geometryList[m_index];
        }
        return null;
    }

    private Transform GetCurrentGeometryPoint()
    {
        if (m_index < m_geometryPointList.Count)
        {
            return m_geometryPointList[m_index];
        }
        return null;
    }

    private GameAudioSource PlayLearnGeometryAudio(EGeometryEnum type)
    {
        //switch (type)
        //{
        //    case EGeometryEnum.Circle:
        //        return this.PlayAudioEx(StudyAudioName.t_22005);// PlayGeometryAudio(StudyAudioName.talk_403040202);
        //    case EGeometryEnum.Square:
        //        return this.PlayAudioEx(StudyAudioName.t_22006);// PlayGeometryAudio(StudyAudioName.talk_403040203);
        //    case EGeometryEnum.Triangle:
        //    //case EGeometryEnum.Triangle_1:
        //    //case EGeometryEnum.Triangle_2:
        //    //case EGeometryEnum.RightTriangle:
        //        return this.PlayAudioEx(StudyAudioName.t_22007);// PlayGeometryAudio(StudyAudioName.talk_403040204);
        //    case EGeometryEnum.Rectangle:
        //        return this.PlayAudioEx(StudyAudioName.t_22008);// PlayGeometryAudio(StudyAudioName.talk_403040205);
        //    case EGeometryEnum.Trapezoid:
        //    //case EGeometryEnum.Trapezoid_1:
        //    //case EGeometryEnum.Trapezoid_2:
        //    //case EGeometryEnum.Trapezoid_3:
        //        return this.PlayAudioEx(StudyAudioName.t_22009);// PlayGeometryAudio(StudyAudioName.talk_403040206);
        //    case EGeometryEnum.Ellipse:
        //        return this.PlayAudioEx(StudyAudioName.t_22010);// PlayGeometryAudio(StudyAudioName.talk_403040207);
        //    default:
        //        return this.PlayAudioEx(StudyAudioName.t_22005);// PlayGeometryAudio(StudyAudioName.talk_403040202);
        //}
        return this.PlayAudioEx(StudyAudioName.t_22011);
    }

    private void CreateGeometry()
    {
        Transform geometryPoints = Instantiate(geometryPointPath, true).transform;
        geometryPoints.SetParent(m_scene);
        geometryPoints.localPosition = Vector3.zero;

        m_geometryMask = geometryPoints.Find("geometry_mask");
        m_geometryCenter = geometryPoints.Find("geometry_center");

        m_geometryMask.gameObject.SetActive(false);

        Transform point_1 = geometryPoints.Find("geometry_1");
        Transform point_2 = geometryPoints.Find("geometry_2");
        Transform point_3 = geometryPoints.Find("geometry_3");
        Transform point_4 = geometryPoints.Find("geometry_4");
        Transform point_5 = geometryPoints.Find("geometry_5");
        Transform point_6 = geometryPoints.Find("geometry_6");

        GameObject geometry_1 = Instantiate(geometry_assetpath1, true, true);
        GameObject geometry_2 = Instantiate(geometry_assetpath2, true, true);
        GameObject geometry_3 = Instantiate(geometry_assetpath3, true, true);
        GameObject geometry_4 = Instantiate(geometry_assetpath4, true, true);
        GameObject geometry_5 = Instantiate(geometry_assetpath5, true, true);
        GameObject geometry_6 = Instantiate(geometry_assetpath6, true, true);

        geometry_1.SetTransformFormTarget(point_1);
        geometry_2.SetTransformFormTarget(point_2);
        geometry_3.SetTransformFormTarget(point_3);
        geometry_4.SetTransformFormTarget(point_4);
        geometry_5.SetTransformFormTarget(point_5);
        geometry_6.SetTransformFormTarget(point_6);

        geometry_1.AddComponentEx<GeometryActor>().Initialize(EGeometryEnum.Circle);
        geometry_2.AddComponentEx<GeometryActor>().Initialize(EGeometryEnum.Square);
        geometry_3.AddComponentEx<GeometryActor>().Initialize(EGeometryEnum.Triangle); 
        geometry_4.AddComponentEx<GeometryActor>().Initialize(EGeometryEnum.Rectangle);
        geometry_5.AddComponentEx<GeometryActor>().Initialize(EGeometryEnum.Trapezoid);
        geometry_6.AddComponentEx<GeometryActor>().Initialize(EGeometryEnum.Ellipse); 

        m_geometryPointList.Clear();
        m_geometryPointList.Add(point_1);
        m_geometryPointList.Add(point_2);
        m_geometryPointList.Add(point_3);
        m_geometryPointList.Add(point_4);
        m_geometryPointList.Add(point_5);
        m_geometryPointList.Add(point_6);

        m_geometryList.Clear();
        m_geometryList.Add(geometry_1.transform);
        m_geometryList.Add(geometry_2.transform);
        m_geometryList.Add(geometry_3.transform);
        m_geometryList.Add(geometry_4.transform);
        m_geometryList.Add(geometry_5.transform);
        m_geometryList.Add(geometry_6.transform);

    }

}

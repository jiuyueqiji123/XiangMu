﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using WCBG.ToolsForUnity.Tools;


/// <summary>
/// 剧情介绍
/// </summary>
public class GeometryPlotSummaryPart : StudyGeometryBasePart
{


    private const string scenePath = StudyPath.geometry_notrack_scene;
    private const string animalPointsPath = StudyPath.geometry_animals_point;
    private const string geometry_animal_broken_ji = StudyPath.geometry_animal_broken_ji;
    private const string geometry_animal_broken_mao = StudyPath.geometry_animal_broken_mao;
    private const string geometry_animal_broken_tuzi = StudyPath.geometry_animal_broken_tuzi;
    private const string geometry_animal_broken_niu = StudyPath.geometry_animal_broken_niu;
    private const string geometry_animal_broken_zhu = StudyPath.geometry_animal_broken_zhu;
    private const string geometry_animal_broken_gou = StudyPath.geometry_animal_broken_gou;

    private Transform m_scene;


    protected override void OnPartStart()
    {
        base.OnPartStart();

        m_scene = CreateScene(scenePath);
        CreateAnimals();

        AddTimer(0.5f, () => {

            //PlayerAnimationController.Instance.PlayAnimaton(PlayerAnimationController.PlayerAnimationEnum.LeftBottomToHalf, LediActor.AnimatorNameEnum.happy_p, StudyPath.study_geometry_audio_basepath + StudyAudioName.talk_403040101, () =>
            //{
            //    AddTimer(1000, () =>
            //    {
            //        OnPartComplete();
            //    });
            //});
        });

        RefreshShadowProjecter();
    }

    protected override void OnMouseDown()
    {
        base.OnMouseDown();

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target == null)
            return;

        //Debug.Log(target.name);
        GeometryAnimalActor actor = target.GetComponent<GeometryAnimalActor>();
        if (actor != null)
        {
            //Debug.Log(actor.animalType);
            actor.CrossFade(GeometryAnimalActor.AnimatorNameEnum.Jump, 0.01f);
            actor.PlayAnimalAudio();
        }
    }

    private void CreateAnimals()
    {
        Transform animalPoints = Instantiate(animalPointsPath, true).transform;
        animalPoints.SetParent(m_scene);
        animalPoints.localPosition = Vector3.zero;

        Transform point_ji = animalPoints.Find("303040101");
        Transform point_mao = animalPoints.Find("303040102");
        Transform point_tuzi = animalPoints.Find("303040103");
        Transform point_niu = animalPoints.Find("303040104");
        Transform point_zhu = animalPoints.Find("303040105");
        Transform point_gou = animalPoints.Find("303040106");

        GameObject ji = Instantiate(geometry_animal_broken_ji, true, true);
        GameObject mao = Instantiate(geometry_animal_broken_mao, true, true);
        GameObject tuzi = Instantiate(geometry_animal_broken_tuzi, true, true);
        GameObject niu = Instantiate(geometry_animal_broken_niu, true, true);
        GameObject zhu = Instantiate(geometry_animal_broken_zhu, true, true);
        GameObject gou = Instantiate(geometry_animal_broken_gou, true, true);

        ji.SetTransformFormTarget(point_ji);
        mao.SetTransformFormTarget(point_mao);
        tuzi.SetTransformFormTarget(point_tuzi);
        niu.SetTransformFormTarget(point_niu);
        zhu.SetTransformFormTarget(point_zhu);
        gou.SetTransformFormTarget(point_gou);

        ji.AddComponentEx<GeometryAnimalActor>().Initialize(EGeometryAnimals.Ji);
        mao.AddComponentEx<GeometryAnimalActor>().Initialize(EGeometryAnimals.Mao);
        tuzi.AddComponentEx<GeometryAnimalActor>().Initialize(EGeometryAnimals.Tuzi);
        niu.AddComponentEx<GeometryAnimalActor>().Initialize(EGeometryAnimals.Niu);
        zhu.AddComponentEx<GeometryAnimalActor>().Initialize(EGeometryAnimals.Zhu);
        gou.AddComponentEx<GeometryAnimalActor>().Initialize(EGeometryAnimals.Gou);


    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using WCBG.ToolsForUnity.Tools;
using WCBG.ToolsForUnity.Extension;


/// <summary>
/// 拼动物
/// </summary>
public class GeometryFindAnimalsPart : StudyGeometryBasePart
{


    private const string trainHeadPath = StudyPath.geometry_train_head;
    private const string flatcarPath = StudyPath.geometry_flatcar;
    private const string geometry_animal_ji = StudyPath.geometry_animal_ji;
    private const string geometry_animal_mao = StudyPath.geometry_animal_mao;
    private const string geometry_animal_tuzi = StudyPath.geometry_animal_tuzi;
    private const string geometry_animal_niu = StudyPath.geometry_animal_niu;
    private const string geometry_animal_zhu = StudyPath.geometry_animal_zhu;
    private const string geometry_animal_gou = StudyPath.geometry_animal_gou;

    //public const string geometry_circle = StudyPath.geometry_circle_1;
    //public const string geometry_square = StudyPath.geometry_square_1;
    //public const string geometry_triangle_1 = StudyPath.geometry_triangle_1;
    //public const string geometry_rightTriangle_1 = StudyPath.geometry_rightTriangle_1;
    //public const string geometry_rectangle = StudyPath.geometry_rectangle_1;
    //public const string geometry_ellipse = StudyPath.geometry_ellipse_1;
    //public const string geometry_trapezoid = StudyPath.geometry_isoscelesTrapezoid_1;

    private readonly float movePathTime = 15f;
    private readonly float moveCheckDis = 40f;
    
    private List<int> m_playedList;

    private int m_animalIndex;
    private int m_animalBrokenCount;

    private Transform m_animalOutPoint_Left;
    private Transform m_animalPoint_Center;
    private Transform m_animalOutPoint_Right;
    private Transform m_effectParent;

    private EGameLayerMask m_currentHitLayer;

    private GeometryTrainHeadActor m_TrainHeadActor;
    private GeometryAnimalActor m_animalActor;
    private GeometryActorComponent m_animalComp;
    private GeometryActor m_dragActor;

    private Vector3 m_dragOffset;
    private float m_zOffset = 30;
    private float m_zOffsetSpeed = 80;
    private float m_zOffsetAdd = 0;

    private List<Transform> m_trainPointList = new List<Transform>();
    private List<Vector3> m_trainMovePathList = new List<Vector3>();
    private List<GeometryFlatCarActor> m_flatActorList = new List<GeometryFlatCarActor>();


    protected override void OnPartStart(params object[] paramArray)
    {
        SetScene(paramArray[0] as Transform);
        m_partIndex = (int)paramArray[1];
        m_playedList = (List<int>)paramArray[2];


        SetAnimalIndex();
        InitializeGeometryPoints();
        CreateTrains();
        MoveTrains();
        CreateAnimal(m_animalIndex);
        //Debug.LogError(m_animalIndex);

        if (m_partIndex == 0)
        {
            this.PlayActionAudioEx(ActionAudioName.t_2008);
        }
        //else
        //{
        //    PlayHelpAudio((EGeometryAnimals)(m_animalIndex), () =>
        //    {
        //    });
        //}

        MedalWindow.Instance.ShowXunzhang(m_partIndex, false, MedalWindow.EXZCountEnum.Six);

        RefreshShadowProjecter();
    }

    protected override void OnPartExit()
    {
        base.OnPartExit();

        foreach (GeometryFlatCarActor actor in m_flatActorList)
        {
            if (actor != null)
            {
                actor.transform.DOKill();
            }
        }
        if (m_TrainHeadActor != null)
        {
            m_TrainHeadActor.transform.DOKill();
            m_TrainHeadActor = null;
        }

        m_trainPointList.Clear();
        m_trainMovePathList.Clear();
        m_flatActorList.Clear();
    }

    protected override void OnMouseDown()
    {
        base.OnMouseDown();

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target == null)
            return;

        GeometryActor actor = target.GetComponent<GeometryActor>();
        if (actor == null)
        {
            return;
        }
        DragStart(actor);

    }

    protected override void OnMouseHover()
    {
        if (m_dragActor != null)
        {
            if (m_zOffsetAdd < m_zOffset)
            {
                float offset = Time.deltaTime * m_zOffsetSpeed;
                m_zOffsetAdd += offset;
                m_dragOffset.z -= offset;
            }

            Vector3 hitPoint;

            Transform target = FTools.GetRaycastHitTarget(Camera.main, Camera.main.WorldToScreenPoint(m_dragActor.transform.position), 1000, 1 << (int)EGameLayerMask.Floor | 1 << (int)EGameLayerMask.Player, false);
            Vector3 hitMousePoint = FTools.GetRaycastHitPointByMousePoint(1 << (int)EGameLayerMask.Floor | 1 << (int)EGameLayerMask.Player, 99999, false);
            if (target.gameObject.layer == (int)EGameLayerMask.Floor)
            {
                if (m_currentHitLayer == EGameLayerMask.Player)
                {
                    m_currentHitLayer = EGameLayerMask.Floor;
                    m_dragActor.transform.DOKill();
                    m_dragActor.transform.DORotateQuaternion(Quaternion.identity, 0.5f).SetEase(Ease.InQuad);
                }

                m_dragActor.transform.position = hitMousePoint + m_dragOffset;
                return;
            }
            else if (target.gameObject.layer == (int)EGameLayerMask.Player)
            {
                if (m_currentHitLayer == EGameLayerMask.Floor)
                {
                    m_currentHitLayer = EGameLayerMask.Player;
                    m_dragActor.transform.DOKill();
                    m_dragActor.transform.DORotateQuaternion(Quaternion.Euler(40, 0, 0), 0.5f).SetEase(Ease.InQuad);
                }

                m_dragActor.transform.position = hitMousePoint + m_dragOffset;
            }
            List<GeometryInfo> toyBricksList = m_animalComp.toyBricksList;
            foreach (GeometryInfo info in toyBricksList)
            {
                if (m_dragActor.geometryType == info.geometryType)
                {
                    //Vector3 hitPoint;
                    if (FTools.GetRaycastHitPoint(Camera.main, Camera.main.WorldToScreenPoint(m_dragActor.transform.position), 1000, out hitPoint, (int)EGameLayerMask.Player))
                    {
                        Vector3 signPoint = info.sign.position;
                        //signPoint.y = 0;
                        float distance = Vector3.Distance(hitPoint, signPoint);
                        if (distance < moveCheckDis)
                        {
                            m_animalComp.OnToyBrickComplete(info);
                            OnDragComplete(m_dragActor);
                            if (m_animalComp.toyBricksList.Count < 1)
                                OnDrawAllComplete();

                            PlayEffect(StudyPath.effect_502003027, signPoint + m_animalActor.transform.up * 30, 3);
                            this.PlayAudioEx(StudyAudioName.t_21017);
                            break;
                        }
                        //Debug.Log(distance);
                    }
                    
                }
            }
        }
    }

    protected override void OnMouseUp()
    {
        DragEnd();
    }

    private void DragStart(GeometryActor actor)
    {
        m_dragActor = actor;
        m_dragActor.PlayGeometryActorAudio();
        //m_dragActor.EnableCollision(false);
        m_currentHitLayer = EGameLayerMask.Floor;
        PauseTrainMove();
        Vector3 floorPoint = FTools.GetRaycastHitPointByMousePoint((int)EGameLayerMask.Floor);
        m_dragOffset = actor.transform.position - floorPoint;
        m_zOffsetAdd = 0;
        //m_dragActor.transform.DOKill();
        //m_dragActor.transform.DORotateQuaternion(Quaternion.Euler(40, 0, 0), 0.2f).SetEase(Ease.InQuad);

    }

    private void DragEnd()
    {
        if (m_dragActor != null)
        {
            //m_dragActor.EnableCollision(true);
            m_dragActor.transform.DOKill();
            m_dragActor.transform.DOLocalMove(Vector3.zero, 0.2f).SetEase(Ease.InQuad);
            m_dragActor.transform.DOLocalRotateQuaternion(Quaternion.identity, 0.2f).SetEase(Ease.InQuad);
            ResumeTrainMove();
            m_dragActor = null;
            //PlayGeometryAudio(StudyAudioName.sound_603020204);
            this.PlayAudioEx(StudyAudioName.t_21021);
        }
    }

    private void OnDragComplete(GeometryActor actor)
    {
        //actor.EnableCollision(true);
        actor.transform.DOKill();
        actor.transform.localPosition = Vector3.zero;
        actor.transform.localScale = Vector3.zero;
        actor.transform.localRotation = Quaternion.identity;
        actor.transform.DOScale(1, 0.5f).SetEase(Ease.InQuad);
        ResumeTrainMove();
        m_dragActor = null;
    }

    private void OnDrawAllComplete()
    {

        AddTimer(1f, () => {

            this.PlayEffect(StudyPath.effect_502004045, m_effectParent, 3);

            //m_animalActor.CrossFade(GeometryAnimalActor.AnimatorNameEnum.StandUp, 0.01f);
            //float rotateTime = 0.5f;

            //m_animalActor.transform.DORotateQuaternion(Quaternion.Euler(40, 0, 0), rotateTime).SetEase(Ease.InQuad);
            //m_animalActor.transform.GetChild(0).DOScale(1.6f, rotateTime).SetEase(Ease.InQuad);


            AddTimer(1f, () =>
            {
                m_animalActor.CrossFade(GeometryAnimalActor.AnimatorNameEnum.Walk, 0.01f);

                float outTime = 5f;
                if (m_animalComp.outType == GeometryActorComponent.EOutDirectionEnum.Left)
                {
                    m_animalActor.transform.DOMove(m_animalOutPoint_Left.position, outTime).SetEase(Ease.Linear);
                }
                else
                {
                    m_animalActor.transform.DOMove(m_animalOutPoint_Right.position, outTime).SetEase(Ease.Linear);
                }
                GameAudioSource audio = PlayAnimalAudio();
                AddTimer(outTime, () => 
                {
                    audio.Stop();
                    
                    //this.PlayAudioEx(StudyAudioName.t_21025);
                    MedalWindow.Instance.ShowXunzhang(m_partIndex + 1, true, MedalWindow.EXZCountEnum.Six, () =>
                    {
                        PlayHeartenAudio(() =>
                        {
                            // on part complete
                            this.AddTimerEx(1000, () => {

                                OnPartComplete(true, m_animalIndex);
                            });
                        });
                    });
                });

            });
        });
    }

    private void PlayHeartenAudio(System.Action callback)
    {
        switch (m_partIndex)
        {
            case 0:
            case 1:
            case 3:
            case 4:
                if (callback != null)
                    callback();
                break;
            case 2:
                this.PlayActionAudioEx(ActionAudioName.t_2009, callback);
                break;
            case 5:
                if (callback != null)
                    callback();
                break;
        }
    }

    private void PauseTrainMove()
    {
        m_TrainHeadActor.transform.DOPause();
        m_TrainHeadActor.PauseAnimatoin();
        //m_TrainHeadActor.CrossFade(GeometryTrainHeadActor.AnimatorNameEnum.Static, 0.01f);
        foreach (GeometryFlatCarActor actor in m_flatActorList)
        {
            actor.transform.DOPause();
            actor.PauseAnimatoin();
            //actor.CrossFade(GeometryFlatCarActor.AnimatorNameEnum.Static, 0.01f);
        }
    }

    private void ResumeTrainMove()
    {
        m_TrainHeadActor.transform.DOPlay();
        m_TrainHeadActor.ResumeAnimation();
        //m_TrainHeadActor.CrossFade(GeometryTrainHeadActor.AnimatorNameEnum.Move, 0.01f);
        foreach (GeometryFlatCarActor actor in m_flatActorList)
        {
            actor.transform.DOPlay();
            actor.ResumeAnimation();
            //actor.CrossFade(GeometryFlatCarActor.AnimatorNameEnum.Move, 0.01f);
        }
    }

    private void InitializeGeometryPoints()
    {
        Transform geometry_findanimals = Scene.Find(StudyPath.geometry_track_findanimals.GetPathName());
        if (geometry_findanimals == null)
        {
            Debug.LogError("Get obj error! ");
            return;
        }
        Transform pathParent = geometry_findanimals.Find("train_path");
        for (int i = 0; i < pathParent.childCount; i++)
        {
            Transform target = pathParent.GetChild(i);
            m_trainPointList.Add(target);
            m_trainMovePathList.Add(target.position);
        }

        m_animalOutPoint_Left = geometry_findanimals.Find("animal_out_left_point");
        m_animalOutPoint_Right = geometry_findanimals.Find("animal_out_right_point");
        m_animalPoint_Center = geometry_findanimals.Find("animal_center_point");
        m_effectParent = geometry_findanimals.Find("effectParent");

    }

    private void CreateTrains()
    {
        m_TrainHeadActor = Instantiate(trainHeadPath, true, true).AddComponentEx<GeometryTrainHeadActor>();
        Transform target2 = m_trainPointList[0];
        m_TrainHeadActor.transform.SetTransformFormTarget(target2);

        for (int i = 0; i < 12; i++)
        {
            GeometryFlatCarActor car = Instantiate(flatcarPath, true, true).AddComponentEx<GeometryFlatCarActor>();
            //Transform target = m_trainPointList[i];
            car.transform.SetTransformFormTarget(target2);
            car.Initialize((EGeometryEnum)(i % 6), i);
            GameObject go = CreateToyBrick((i % 6));
            go.transform.SetParent(car.geometryPoint);
            go.transform.ResetTransformForLocal();
            m_flatActorList.Add(car);
        }
    }

    private void SetAnimalIndex()
    {
        //m_animalIndex = m_partIndex + 1;

        switch (m_partIndex)
        {
            case 0:
                List<EGeometryAnimals> list = new List<EGeometryAnimals>() { EGeometryAnimals.Ji, EGeometryAnimals.Mao, EGeometryAnimals.Tuzi, EGeometryAnimals.Zhu };
                m_animalIndex = (int)list[Random.Range(0, list.Count)];
                m_animalBrokenCount = 3;
                break;
            case 1:
                if (!m_playedList.Contains((int)EGeometryAnimals.Ji))
                {
                    m_animalIndex = (int)EGeometryAnimals.Ji;
                }
                else
                {
                    List<EGeometryAnimals> list1 = new List<EGeometryAnimals>() { EGeometryAnimals.Mao, EGeometryAnimals.Tuzi, EGeometryAnimals.Zhu };
                    m_animalIndex = (int)list1[Random.Range(0, list1.Count)];
                }
                m_animalBrokenCount = 4;
                break;
            case 2:
                List<EGeometryAnimals> list2 = new List<EGeometryAnimals>() { EGeometryAnimals.Ji, EGeometryAnimals.Mao, EGeometryAnimals.Tuzi, EGeometryAnimals.Zhu };
                for (int i = 0; i < m_playedList.Count; i++)
                    list2.Remove((EGeometryAnimals)m_playedList[i]);
                m_animalIndex = (int)list2[Random.Range(0, list2.Count)];
                m_animalBrokenCount = 4;
                break;
            case 3:
                List<EGeometryAnimals> list3 = new List<EGeometryAnimals>() { EGeometryAnimals.Ji, EGeometryAnimals.Mao, EGeometryAnimals.Tuzi, EGeometryAnimals.Zhu };
                for (int i = 0; i < m_playedList.Count; i++)
                    list3.Remove((EGeometryAnimals)m_playedList[i]);
                m_animalIndex = (int)list3[Random.Range(0, list3.Count)];
                m_animalBrokenCount = 5;
                break;
            case 4:
                if (Random.Range(0, 2) == 1)
                    m_animalIndex = (int)EGeometryAnimals.Gou;
                else
                    m_animalIndex = (int)EGeometryAnimals.Niu;
                m_animalBrokenCount = 6;
                break;
            case 5:
                if (m_playedList.Contains((int)EGeometryAnimals.Gou))
                {
                    m_animalIndex = (int)EGeometryAnimals.Niu;
                }
                else
                {
                    m_animalIndex = (int)EGeometryAnimals.Gou;
                }
                m_animalBrokenCount = 7;
                break;
        }
    }

    // 从1开始 1 - 6
    private void CreateAnimal(int index)
    {
        string path = "";
        EGeometryAnimals type = (EGeometryAnimals)index;
        switch (type)
        {
            case EGeometryAnimals.Ji:
                path = geometry_animal_ji;
                break;
            case EGeometryAnimals.Mao:
                path = geometry_animal_mao;
                break;
            case EGeometryAnimals.Tuzi:
                path = geometry_animal_tuzi;
                break;
            case EGeometryAnimals.Zhu:
                path = geometry_animal_zhu;
                break;
            case EGeometryAnimals.Niu:
                path = geometry_animal_niu;
                break;
            case EGeometryAnimals.Gou:
                path = geometry_animal_gou;
                break;
        }
        //path = geometry_animal_niu;

        m_animalActor = Instantiate(path, true, true).AddComponentEx<GeometryAnimalActor>();
        m_animalActor.Initialize((EGeometryAnimals)index);
        m_animalActor.transform.SetTransformFormTarget(m_animalPoint_Center);
        m_animalComp = m_animalActor.GetComponent<GeometryActorComponent>();
        m_animalComp.SetAnimalRandomToyBricks(m_animalBrokenCount);
    }

    // 从1开始 0 - 5
    private GameObject CreateToyBrick(int index)
    {
        List<string> pathList = null;
        EGeometryAnimals animalType = (EGeometryAnimals)m_animalIndex;
        switch (animalType)
        {
            case EGeometryAnimals.Ji:
                pathList = new List<string>(){
                    StudyPath.geometry_circle_4 , StudyPath.geometry_square_5 , StudyPath.geometry_rightTriangle_3 ,
                    StudyPath.geometry_rectangle_2 , StudyPath.geometry_trapezoid_6  , StudyPath.geometry_ellipse_5};
                break;
            case EGeometryAnimals.Mao:
                pathList = new List<string>(){
                    StudyPath.geometry_circle_6 , StudyPath.geometry_square_1 , StudyPath.geometry_triangle_3 ,
                    StudyPath.geometry_rectangle_6  , StudyPath.geometry_isoscelesTrapezoid_3 ,                   StudyPath.geometry_ellipse_4};
                break;
            case EGeometryAnimals.Tuzi:
                pathList = new List<string>(){
                    StudyPath.geometry_circle_7 , StudyPath.geometry_square_2 , StudyPath.geometry_triangle_2 ,
                    StudyPath.geometry_rectangle_7 , StudyPath.geometry_trapezoid_1 , StudyPath.geometry_ellipse_6 };
                break;
            case EGeometryAnimals.Zhu:
                pathList = new List<string>(){
                    StudyPath.geometry_circle_3 , StudyPath.geometry_square_6 , StudyPath.geometry_isoscelesTriangle_2 ,
                    StudyPath.geometry_rectangle_3 , StudyPath.geometry_trapezoid_3 , StudyPath.geometry_ellipse_3 };
                break;
            case EGeometryAnimals.Niu:
                pathList = new List<string>(){
                    StudyPath.geometry_circle_2 , StudyPath.geometry_square_3 , StudyPath.geometry_rightTriangle_4 ,
                    StudyPath.geometry_rectangle_8 , StudyPath.geometry_isoscelesTrapezoid_2 , StudyPath.geometry_ellipse_3 };
                break;
            case EGeometryAnimals.Gou:
                pathList = new List<string>(){
                    StudyPath.geometry_circle_2 , StudyPath.geometry_square_3 , StudyPath.geometry_rightTriangle_3 ,
                    StudyPath.geometry_rectangle_2 , StudyPath.geometry_trapezoid_6 , StudyPath.geometry_ellipse_7 };
                break;
        }

        EGeometryEnum type = (EGeometryEnum)index;
        GameObject go = Instantiate(pathList[index], true, true);
        go.AddComponentEx<GeometryActor>().Initialize(type);
        return go;
    }

    private void MoveTrains()
    {
        float pathMaxDis = FTools.GetPathDistance(GetMovePath(0, m_trainMovePathList.Count - 1));
        float dis = pathMaxDis / movePathTime;
        //Debug.LogError("dis :" + dis);

        Vector3[] headPath = GetMovePath(4, m_trainMovePathList.Count - 1);
        float headTime = FTools.GetPathDistance(headPath) / dis;
        //Debug.LogError("headTime :" + headTime);
        //Debug.LogError("headTime dis:" + headTime / FTools.GetPathDistance(headPath));
        m_TrainHeadActor.transform.position = headPath[0];
        m_TrainHeadActor.transform.DOPath(headPath, headTime, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() =>
        {
            //OnTrainHeadMoveComplete(m_TrainHeadActor.transform);
            m_TrainHeadActor.CrossFade(GeometryTrainHeadActor.AnimatorNameEnum.Static, 0.01f);
            m_TrainHeadActor.ClearEffect();
        });
        m_TrainHeadActor.CrossFade(GeometryTrainHeadActor.AnimatorNameEnum.Move, 0.01f);
        m_TrainHeadActor.PlayEffect(StudyPath.math_traineffect_out_big);

        for (int i = 0; i < m_flatActorList.Count; i++)
        {
            List<Vector3> _carPathList = GetMovePathList(3, m_trainMovePathList.Count - 1);

            for (int j = 0; j < i; j++)
            {
                _carPathList.Insert(0, m_flatActorList[j].followPoint.position);
            }
            Vector3[] _carPath = _carPathList.ToArray();
            float carMoveTime = FTools.GetPathDistance(_carPath) / dis;
            //Debug.LogError("carMoveTime :" + carMoveTime);
            //Debug.LogError("carMoveTime dis:" + carMoveTime / FTools.GetPathDistance(carPath));
            GeometryFlatCarActor _actor = m_flatActorList[i];
            _actor.transform.position = _carPath[0];
            _actor.transform.DOPath(_carPath, carMoveTime, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() =>
            {
                OnCarMoveComplete(_actor);
            });
            _actor.CrossFade(GeometryFlatCarActor.AnimatorNameEnum.Move, 0.01f);
        }
    }
    //private void OnTrainHeadMoveComplete(Transform target)
    //{
    //    Vector3[] path = GetMovePath(0, m_trainMovePathList.Count - 1);
    //    float pathMaxDis = FTools.GetPathDistance(path);
    //    float dis = pathMaxDis / movePathTime;

    //    GeometryFlatCarActor actor = m_flatActorList[0];
    //    path[0] = actor.followPoint.position;
    //    path[0] = path[0] + (path[0] - path[1]).normalized * 10;
    //    target.position = path[0];

    //    float carMoveTime = FTools.GetPathDistance(path) / dis;

    //    target.DOPath(path, carMoveTime, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() =>
    //    {
    //        OnTrainHeadMoveComplete(target);
    //    });
    //}

    private void OnCarMoveComplete(GeometryFlatCarActor actor)
    {
        Vector3[] path = GetMovePath(0, m_trainMovePathList.Count - 1);
        float pathMaxDis = FTools.GetPathDistance(path);
        float dis = pathMaxDis / movePathTime;

        if (actor.carIndex == 0)
        {
            path[0] = m_flatActorList[m_flatActorList.Count - 1].followPoint.position;
        }
        else
        {
            path[0] = m_flatActorList[actor.carIndex - 1].followPoint.position;
        }
        float carMoveTime = FTools.GetPathDistance(path) / dis;

        actor.transform.position = path[0];
        actor.transform.DOPath(path, carMoveTime, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear).SetLookAt(0.001f).OnComplete(() =>
        {
            OnCarMoveComplete(actor);
        });
    }

    private Vector3[] GetMovePath(int start, int end)
    {
        List<Vector3> list = new List<Vector3>();
        for (int i = start; i <= end ; i++)
        {
            list.Add(m_trainMovePathList[i]);
        }
        return list.ToArray();
    }

    private List<Vector3> GetMovePathList(int start, int end)
    {
        List<Vector3> list = new List<Vector3>();
        for (int i = start; i <= end; i++)
        {
            list.Add(m_trainMovePathList[i]);
        }
        return list;
    }

    public GameAudioSource PlayAnimalAudio()
    {
        EGeometryAnimals type = (EGeometryAnimals)m_animalIndex;
        switch (type)
        {
            case EGeometryAnimals.Ji:
                return this.PlayAudioEx(StudyAudioName.t_21008, true);
            case EGeometryAnimals.Mao:
                return this.PlayAudioEx(StudyAudioName.t_21004, true);
            case EGeometryAnimals.Tuzi:
                return this.PlayAudioEx(StudyAudioName.t_21006, true);
            case EGeometryAnimals.Niu:
                return this.PlayAudioEx(StudyAudioName.t_21010, true);
            case EGeometryAnimals.Zhu:
                return this.PlayAudioEx(StudyAudioName.t_21002, true);
            case EGeometryAnimals.Gou:
                return this.PlayAudioEx(StudyAudioName.t_21012, true);
            default:
                return this.PlayAudioEx(StudyAudioName.t_21002, true);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InitiativeBasePart : IInitiativeBase, IEasyTimer
{
    public bool bTimerCallback
    {
        get { return _IsPartEnable; }
    }
    private bool _IsPartEnable = false;
    public bool IsPartEnable
    {
        get { return _IsPartEnable; }
    }
    private bool _IsPartComplete = false;
    public bool IsPartComplete
    {
        get { return _IsPartComplete; }
    }

    protected int flowIndex
    {
        get;
        private set;
    }
     

    /// <summary>
    /// 物体缓存列表
    /// </summary>
    private List<GameObject> m_cacheList = new List<GameObject>();
    private Delegate completeDelegate = null;


    #region public

    public void Register(int index)
    {
        flowIndex = index;
    }

    public void OnStart(Action<bool> callback)
    {
        _IsPartEnable = true;
        _IsPartComplete = false;
        completeDelegate = callback;
        OnPartStart();
        AddEvent();
    }

    public void OnStart(Action<bool, object[]> callback)
    {
        _IsPartEnable = true;
        _IsPartComplete = false;
        completeDelegate = callback;

        OnPartStart();
        AddEvent();
    }

    public void OnStart(Action<bool> callback, params object[] paramArray)
    {
        _IsPartEnable = true;
        _IsPartComplete = false;
        completeDelegate = callback;

        OnPartStart(paramArray);
        AddEvent();
    }

    public void OnStart(Action<bool, object[]> callback, params object[] paramArray)
    {
        _IsPartEnable = true;
        _IsPartComplete = false;
        completeDelegate = callback;

        OnPartStart(paramArray);
        AddEvent();
    }

    public void OnExit()
    {
        _IsPartEnable = false;
        OnPartExit();
        RemoveEvent();
        DestroyChacheObj();
    }

    public void OnUpdate()
    {
        if (IsPartEnable)
        {
            OnPartUpdate();
        }
    }

    #endregion

    #region protected


    protected void OnPartComplete(bool bComplete = true)
    {
        _IsPartComplete = true;
        if (completeDelegate != null)
        {
            Action<bool> action = completeDelegate as Action<bool>;
            if (action != null)
            {
                action(bComplete);
            }
        }
    }
    protected void OnPartComplete(bool bComplete = true, params object[] paramArray)
    {
        _IsPartComplete = true;
        if (completeDelegate != null)
        {
            Action<bool, object[]> action = completeDelegate as Action<bool, object[]>;
            if (action != null)
            {
                action(bComplete, paramArray);
            }
        }
    }

    /// <summary>
    /// 创建的GameObject 会在GamePart退出时自动清除
    /// </summary>
    /// <param name="source"></param>
    /// <param name="bChache"></param>
    /// <returns></returns>
    protected virtual GameObject Instantiate(GameObject source, bool bChache)
    {
        GameObject go = GameObject.Instantiate(source);
        if (bChache)
        {
            m_cacheList.Add(go);
        }
        return go;
    }

    /// <summary>
    /// 创建的GameObject 会在GamePart退出时自动清除
    /// </summary>
    /// <param name="sourcePath"></param>
    /// <param name="bChache"></param>
    /// <returns></returns>
    protected virtual GameObject Instantiate(string sourcePath, bool bChache)
    {
        Resource res = ResourceManager.Instance.GetResource(sourcePath, typeof(GameObject), enResourceType.ScenePrefab);
        if (res.m_content != null)
        {
            return Instantiate(res.m_content as GameObject, bChache);
        }
        return null;
    }

    /// <summary>
    /// 当前GamePart 退出的时候调用清除
    /// </summary>
    protected void DestroyChacheObj()
    {
        foreach (GameObject go in m_cacheList)
        {
            if (go != null)
            {
                GameObject.Destroy(go);
            }
        }

        m_cacheList.Clear();
    }
    

    protected void AddTimer(float time, System.Action callback)
    {
        this.AddTimerEx(time, callback);
    }

    protected void AddTimer(int time, System.Action callback)
    {
        this.AddTimerEx(time, callback);
    }

    //protected void AddTimer(int time, int loop, System.Action callback)
    //{
    //    this.AddTimerEx(time, loop, callback);
    //}



    #endregion

    #region virtual


    protected virtual void OnPartStart() { }
    //protected virtual void OnFlowStart(params object[] paramArray) { }
    //protected virtual void OnPartStart(object t1) { }
    //protected virtual void OnPartStart(object t1, object t2) { }
    protected virtual void OnPartStart(params object[] paramArray) { }
    protected virtual void OnPartExit() { }
    protected virtual void OnPartUpdate() { }
    protected virtual void AddEvent() { }
    protected virtual void RemoveEvent() { }

    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StudyBaseActor : MonoBehaviour, IEasyAudio
{



    private AnimatorExtension _AnimatorEx;
    public AnimatorExtension AnimatorEx
    {
        get
        {
            if (_AnimatorEx == null)
                _AnimatorEx = new AnimatorExtension(gameObject);
            return _AnimatorEx;
        }
    }


    private SkinnedMeshRenderer _meshRenderer = null;
    public SkinnedMeshRenderer meshRenderer
    {
        get
        {
            if (_meshRenderer == null)
            {
                _meshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
            }
            return _meshRenderer;
        }
    }



    public void PauseAnimatoin()
    {
        AnimatorEx.PauseAnimation();
    }

    public void ResumeAnimation()
    {
        AnimatorEx.ResumeAnimation();
    }

    public void EnableCollision(bool value)
    {
        BoxCollider collider = GetComponent<BoxCollider>();
        if (collider != null)
        {
            collider.enabled = value;
        }
    }


    //protected GameAudioSource PlayGeometryAudio(string audioName)
    //{
    //    return AudioManager.Instance.PlaySound(StudyPath.study_geometry_audio_basepath + audioName, false, true);
    //}

    protected virtual void OnDestroy()
    {
        transform.DOKill();
    }



}

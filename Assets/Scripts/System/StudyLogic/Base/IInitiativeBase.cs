﻿using System;

public interface IInitiativeBase
{

    void Register(int index);

    void OnStart(Action<bool> callback);

    void OnStart(Action<bool, object[]> callback);

    void OnStart(Action<bool> callback, params object[] paramArray);

    void OnStart(Action<bool, object[]> callback, params object[] paramArray);

    //void OnStart<T1>(Action<bool, T1> callback);

    //void OnStart<T1>(object param, Action<bool, T1> callback);

    void OnExit();

    void OnUpdate();

}

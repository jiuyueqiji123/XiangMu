﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InitiativeBaseFlow : IInitiativeBase, IEasyTimer,IEasyAudio
{
    public bool bTimerCallback
    {
        get { return bFlowEnable; }
    }
    private bool _bFlowEnable = false;
    public bool bFlowEnable
    {
        get { return _bFlowEnable; }
    }

    protected int flowIndex
    {
        get;
        private set;
    }

    /// <summary>
    /// 物体缓存列表
    /// </summary>
    private List<GameObject> m_cacheList = new List<GameObject>();
    private Delegate completeDelegate = null;

    // 关卡流程管理器
    protected StudyGameMachine machine = new StudyGameMachine();
    private bool isAutoToNext;

    #region public

    public void Register(int index)
    {
        flowIndex = index;
    }

    public void OnStart(Action<bool> callback)
    {
        _bFlowEnable = true;
        completeDelegate = callback;
        OnFlowStart();
        AddEvent();
    }

    public void OnStart(Action<bool, object[]> callback)
    {
        _bFlowEnable = true;
        completeDelegate = callback;

        OnFlowStart();
        AddEvent();
    }

    public void OnStart(Action<bool> callback, params object[] paramArray)
    {
        _bFlowEnable = true;
        completeDelegate = callback;

        OnFlowStart(paramArray);
        AddEvent();
    }

    public void OnStart(Action<bool, object[]> callback, params object[] paramArray)
    {
        _bFlowEnable = true;
        completeDelegate = callback;

        OnFlowStart(paramArray);
        AddEvent();
    }

    public void OnExit()
    {
        _bFlowEnable = false;

        ClearPart();
        OnFlowExit();
        RemoveEvent();
        DestroyChacheObj();
    }

    public void OnUpdate()
    {
        if (bFlowEnable)
        {
            OnFlowUpdate();
        }
    }

    #endregion

    #region protected

    protected void OnFlowComplete(bool bComplete = true)
    {
        if (completeDelegate != null)
        {
            Action<bool> action = completeDelegate as Action<bool>;
            if (action != null)
            {
                action(bComplete);
            }
            else
            {
                OnFlowComplete(bComplete, null);
            }
        }
    }

    protected void OnFlowComplete(bool bComplete = true, params object[] paramArray)
    {
        if (completeDelegate != null)
        {
            Action<bool, object[]> action = completeDelegate as Action<bool, object[]>;
            if (action != null)
            {
                action(bComplete, paramArray);
            }
        }
    }

    /// <summary>
    /// 创建的GameObject 会在GameFlow退出时自动清除
    /// </summary>
    /// <param name="source"></param>
    /// <param name="bChache"></param>
    /// <returns></returns>
    protected GameObject Instantiate(GameObject source, bool bChache)
    {
        GameObject go = GameObject.Instantiate(source);
        if (bChache)
        {
            m_cacheList.Add(go);
        }
        return go;
    }

    /// <summary>
    /// 创建的GameObject 会在GamePart退出时自动清除
    /// </summary>
    /// <param name="sourcePath"></param>
    /// <param name="bChache"></param>
    /// <returns></returns>
    protected GameObject Instantiate(string sourcePath, bool bChache)
    {
        Resource res = ResourceManager.Instance.GetResource(sourcePath, typeof(GameObject), enResourceType.ScenePrefab);
        if (res.m_content != null)
        {
            return Instantiate(res.m_content as GameObject, bChache);
        }
        return null;
    }

    /// <summary>
    /// 当前GameFlow 退出的时候调用清除
    /// </summary>
    protected void DestroyChacheObj()
    {
        foreach (GameObject go in m_cacheList)
        {
            if (go != null)
            {
                GameObject.Destroy(go);
            }
        }

        m_cacheList.Clear();
    }


    protected void AddTimer(float time, System.Action callback)
    {
        this.AddTimerEx(time, callback);
    }

    protected void AddTimer(int time, System.Action callback)
    {
        this.AddTimerEx(time, callback);
    }

    protected void RegisterPart(IInitiativeBase flow)
    {
        if (machine == null)
            machine = new StudyGameMachine();
        machine.RegisterGamePart(flow);
    }

    protected void StartPart(int index, bool autoToNext, System.Action<bool, object[]> partCallback = null, System.Action<bool> allpartCallback = null)
    {
        isAutoToNext = autoToNext;
        machine.StartGamePart(index, new StudyGameMachine.GameAllPartCompleteDelegate((isComplete)=> {
            if (allpartCallback != null)
            {
                allpartCallback(isComplete);
            }
        }), new StudyGameMachine.GamePartCompleteDelegate((isComplete, paramsArray)=> {
            if (isAutoToNext)
            {
                machine.StopCurrentGamePart();
                if (isComplete)
                {
                    machine.StartNextPart();
                }
                else
                {
                    if (allpartCallback != null)
                    {
                        allpartCallback(false);
                    }
                }
            }
            if (partCallback != null)
            {
                partCallback(isComplete, paramsArray);
            }
        }));
    }

    protected void ClearPart()
    {
        if (machine != null)
        {
            machine.StopAllGamePart();
            machine = null;
        }
    }



    #endregion

    #region virtual

    protected virtual void OnFlowStart() { }
    protected virtual void OnFlowStart(params object[] paramArray) { }
    protected virtual void OnFlowExit() { }
    protected virtual void OnFlowUpdate() { }
    protected virtual void AddEvent() { }
    protected virtual void RemoveEvent() { }

    #endregion

}

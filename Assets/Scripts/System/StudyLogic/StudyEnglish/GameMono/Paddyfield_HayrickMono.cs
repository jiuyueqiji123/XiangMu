﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Paddyfield_HayrickMono : EnglishBaseMono
{


    public override void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration)
    {
        base.Init(startPoint, endPoint, outPoint, letter, letterOffset, audioName, atlasID, hintTexture, duration);

        transform.position = startPoint;
        transform.localScale = Vector3.up * 0.2f;

        transform.GetComponent<MaterialTextureAnimation>().Play(MaterialTextureAnimation.EPlayModeEnum.Loop);

        transform.DOMove(endPoint, duration).SetEase(m_InEase);
        transform.DOScale(Vector3.one, duration).SetEase(m_InEase).OnComplete(() =>
        {
            transform.GetComponent<MaterialTextureAnimation>().Stop();
        });
    }

    public override void LeaveTheStage(float duration = 1.5F)
    {
        transform.GetComponent<MaterialTextureAnimation>().Play(MaterialTextureAnimation.EPlayModeEnum.Loop);
        transform.DOMove(m_OutPoint, duration).SetEase(m_OutEase).OnComplete(() =>
        {
            transform.GetComponent<MaterialTextureAnimation>().Stop();
        });

    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        transform.GetComponent<MaterialTextureAnimation>().Stop();
    }


}

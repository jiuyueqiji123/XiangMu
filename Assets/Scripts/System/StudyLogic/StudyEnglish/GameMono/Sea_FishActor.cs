﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Sea_FishActor : EnglishBaseActor
{


    public enum AnimatorNameEnum
    {
        Stand,
        Shake,
    }
    
    

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }
    
    


}

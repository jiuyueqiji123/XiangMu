﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TableProto;

public class SelectLetterActor : EnglishBaseActor
{



    public enum AnimatorNameEnum
    {
        Static,
        Stand,
        Shake,
    }


    [SerializeField]
    private uint _dataID;
    public uint dataID
    {
        get { return _dataID; }
    }

    private int _LetterIndex;
    public int LetterIndex
    {

        get { return _LetterIndex; }
    }
    
    public bool isBuy
    {
        get;
        private set;
    }

    public bool isPlayAds
    {
        get;
        private set;
    }

    public bool isLock
    {
        get;
        private set;
    }

    //public Transform letter;
    //public Transform letterShadow;
    public Transform saleImg;
    public Transform adsImg;


    private void Awake()
    {
        saleImg = transform.Find("Root/Sale");
        adsImg = transform.Find("Root/Ads");
        adsImg.SetActive(false);
    }

    public void Initialize(int letterIndex, uint dataID)
    {
        _LetterIndex = letterIndex;
        _dataID = dataID;
        EnglishLevelInfo info = TableProtoLoader.EnglishLevelInfoDict[_dataID];
        string imagePath = StudyPath.english_select_letter_texture_basepath + info.name;
        meshRenderer.material.SetTexture("_MainTex", Res.LoadTexture(imagePath));

        CrossFade(AnimatorNameEnum.Static, 0.01f);
    }

    public void Initialize(uint ID, Vector3 position)
    {
        _dataID = ID;
        transform.position = position;

        EnglishLevelInfo info = TableProtoLoader.EnglishLevelInfoDict[ID];
        //letter = transform.Find("root/letter");
        //letterShadow = transform.Find("root/shadow");

        //MeshRenderer meshRenderer = letter.GetComponent<MeshRenderer>();
        string imagePath = StudyPath.english_select_letter_texture_basepath + info.name;
        meshRenderer.material.SetTexture("_MainTex", Res.LoadTexture(imagePath));

        CrossFade(AnimatorNameEnum.Static, 0.01f);
    }

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }

    public void UpdateSaleState(bool buyState, int buyIndex)
    {
        isBuy = buyState;
        if (_LetterIndex < buyIndex)
        {
            isBuy = true;
        }

        if (saleImg != null)
            saleImg.SetActive(!isBuy);
    }

    public void SetAdsState(bool val)
    {
        isPlayAds = val;
        adsImg.SetActive(isPlayAds);
        if (!AdsManager.Instance.AdsReady(AdsNameConst.Ads_English))
        {
            adsImg.SetActive(false);
        }
    }

    public void SetLockState(bool value)
    {
        isLock = value;
        if (meshRenderer != null)
        {
            if (value)
            {
                //meshRenderer.material.shader = Shader.Find("Unlit/Transparent Color");
                float col = 150f / 255f;
                meshRenderer.material.SetColor("_Color", new Color(col, col, col, 1));
                CrossFade(AnimatorNameEnum.Static, 0.01f);
            }
            else
            {
                //meshRenderer.material.shader = Shader.Find("Unlit/Transparent");
                meshRenderer.material.SetColor("_Color", new Color(1, 1, 1, 1));
                CrossFade(AnimatorNameEnum.Stand, 0.01f);
            }
        }
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


// 鱼泡组件
public class Sea_FishMono : EnglishBaseMono
{


    // 要移动的目标点
    private Vector3 m_NextPoint;

    private float m_Speed;

    private bool m_bMoving = false;

    private float m_MinSpeed = 2;
    private float m_MaxSpeed = 6;



    private void Update()
    {
        if (m_bMoving)
            Moving();
    }

    //public override void Init(Vector3 startPoint, Vector3 endPoint, string letter, Vector3 leftBottomPoint, Vector3 rightBottomPoint, Vector3 leftTopPoint)
    //{
    //    base.Init(startPoint, endPoint, letter, leftBottomPoint, rightBottomPoint, leftTopPoint);

    //    transform.position = startPoint;
    //    transform.DOMove(endPoint, 1.5f);

    //    TimerManager.Instance.AddTimer(1500, 1, (seq) => {
    //        StartMove();
    //    });
    //}

    public override void StartYourShowtime(float duration = 1.5f)
    {
        m_MinSpeed = 10;
        m_MaxSpeed = 20;
        SetNextMovePoint();
        SetSpeed();
    }

    public override void LeaveTheStage(float duration = 1.5F)
    {

    }

    private void Moving()
    {
        if (Vector3.Distance(transform.position, m_NextPoint) < 1)
        {
            MoveArrive();
            return;
        }
        Vector3 moveDir = m_NextPoint - transform.position;
        transform.position += moveDir.normalized * m_Speed * Time.deltaTime;
        moveDir.y = 0;
        transform.rotation = Quaternion.LookRotation(moveDir, transform.up);
    }

    private void StartMove()
    {
        m_bMoving = true;
        m_MinSpeed = m_MinSpeed * 2;
        m_MaxSpeed = m_MaxSpeed * 2;

        SetNextMovePoint();
        SetSpeed();
    }

    private void MoveArrive()
    {
        SetNextMovePoint();
        SetSpeed();
    }

    private void SetNextMovePoint()
    {
        //Vector3 rightDir = m_RightBottomPoint - m_LeftBottomPoint;
        //Vector3 upDir = m_LeftTopPoint - m_LeftBottomPoint;

        //m_NextPoint = m_LeftBottomPoint + rightDir * Random.value + upDir * Random.value;
    }

    private void SetSpeed()
    {
        m_Speed = Random.Range(m_MinSpeed, m_MaxSpeed);
    }



}

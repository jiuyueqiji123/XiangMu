﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


// 鱼泡组件
public class Sea_BoxMono : EnglishBaseMono
{



    public override void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration, float delayIn, float delayOut)
    {
        base.Init(startPoint, endPoint, outPoint, letter, letterOffset, audioName, atlasID, hintTexture, duration, delayIn, delayOut);

        transform.position = startPoint;

        //if (delayIn == 0)
        //{
        //    transform.DOMove(endPoint, duration).SetEase(m_InEase);
        //    return;
        //}
        float _start = Time.realtimeSinceStartup;
        float _time = Time.time;
        //Debug.Log("realtimeSinceStartup:" + Time.realtimeSinceStartup + "      delayIn:" + delayIn);
        this.AddTimerEx(delayIn, () =>
        {
            Debug.Log("realtimeSinceStartup:" + (Time.realtimeSinceStartup - _start) + " time:" + (Time.time - _time) + "      delayIn:" + delayIn);
            transform.DOMove(endPoint, duration).SetEase(m_InEase);
        });
    }

    public override void StartYourShowtime(float duration = 1.5F)
    {
        //transform.DOMove(m_OutPoint, duration).SetEase(Ease.Linear);
        transform.DOScale(1.5f, duration).SetEase(m_InEase).OnComplete(()=> {

            GetComponent<MaterialTextureAnimation>().Play(MaterialTextureAnimation.EPlayModeEnum.Once);
            PlayEffect(StudyPath.effect_502003023, transform.position, 1);
        });

        AddTimer(duration + 1.5f, () =>
        {
            gameObject.SetActive(false);
        });
    }

    public override void LeaveTheStage(float duration = 1.5F)
    {
        if (delayOut == 0)
        {
            transform.DOMove(m_OutPoint, duration).SetEase(m_OutEase);
            return;
        }
        this.AddTimerEx(delayOut, () =>
        {
            transform.DOMove(m_OutPoint, duration).SetEase(m_OutEase);
        });
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Snow_BoxMono : EnglishBaseMono
{




    public override void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration)
    {
        base.Init(startPoint, endPoint, outPoint, letter, letterOffset, audioName, atlasID, hintTexture, duration);


        transform.position = startPoint;
        transform.DOMove(endPoint, duration).SetEase(m_InEase);

    }

    public override void StartYourShowtime(float duration = 1.5F)
    {
        //transform.DOMove(m_OutPoint, duration).SetEase(Ease.Linear);
        transform.DOScale(1.5f, duration * 0.5f).SetEase(m_InEase);

        AddTimer(duration, () =>
        {
            gameObject.SetActive(false);
            PlayEffect(StudyPath.effect_502003024, transform.position, duration);
        });
    }

    public override void LeaveTheStage(float duration = 1.5F)
    {
        transform.DOMove(m_OutPoint, duration).SetEase(m_OutEase);
    }

    public override void Shake(float duration = 0.5F, float strength = 5)
    {
        base.Shake(duration, strength);

        PlayEffect(StudyPath.effect_502003031, transform.position, duration);

    }


}

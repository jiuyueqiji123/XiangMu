﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Lawn_DeliverPackageMono : EnglishBaseMono
{

    public override void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration, float delayIn, float delayOut)
    {
        base.Init(startPoint, endPoint, outPoint, letter, letterOffset, audioName, atlasID, hintTexture, duration, delayIn, delayOut);

        transform.position = startPoint;

        //if (delayIn == 0)
        //{
        //    transform.DOMove(endPoint, duration).SetEase(m_InEase);
        //    return;
        //}
        //Debug.LogError(Time.time + "      delayIn:" + delayIn);
        //float _start = Time.realtimeSinceStartup;
        //float _time = Time.time;
        this.AddTimerEx(delayIn, () =>
        {
            //Debug.Log("realtimeSinceStartup:" + (Time.realtimeSinceStartup - _start) + " time:" + (Time.time - _time) + "      delayIn:" + delayIn);
            transform.DOMove(endPoint, duration).SetEase(m_InEase);
        });
    }

    public override void StartYourShowtime(Vector3 position, Vector3 scale, Quaternion rotation, float duration = 1.5F)
    {
        transform.DOScale(1.5f, 0.5f).SetEase(Ease.Linear);

        AddTimer(1f, () =>
        {
            transform.DOMove(position, duration).SetEase(m_OutEase);
            transform.DOScale(scale, duration).SetEase(m_OutEase);
            transform.DORotateQuaternion(rotation, duration).SetEase(m_OutEase);
            GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_31004);
        });

    }

    public override void LeaveTheStage(float duration = 1.5F)
    {
        if (delayOut == 0)
        {
            transform.DOMove(m_OutPoint, duration).SetEase(m_OutEase);
            return;
        }
        this.AddTimerEx(delayOut, () =>
        {
            transform.DOMove(m_OutPoint, duration).SetEase(m_OutEase);
        });
    }


}

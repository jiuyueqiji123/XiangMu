﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Paddyfield_ScarecrowActor : EnglishBaseActor
{


    public enum AnimatorNameEnum
    {
        Shake,
        Around,
        Back,
    }


    public void MoveOut(float duration = 1.5F)
    {
        CrossFade(AnimatorNameEnum.Around, 0.1f);
    }

    public void MoveIn(float duration = 1.5F)
    {
        CrossFade(AnimatorNameEnum.Back, 0.1f);
    }

    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }
    
    


}

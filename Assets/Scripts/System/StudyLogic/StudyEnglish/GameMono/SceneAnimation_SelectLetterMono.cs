﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SceneAnimation_SelectLetterMono : MonoBehaviour {

    private readonly float windmillSpeed = -100;
    private Transform m_windmill;

    private Transform m_bigBg_start;
    private Transform m_bigBg_center;
    private Transform m_bigBg_end;
    //private Transform m_smallBg_start;
    //private Transform m_smallBg_center;
    //private Transform m_smallBg_end;

    private const float bigBgMoveTime = 100;
    //private const float smallBgMoveTime = 40;

    private float dis;  // 每秒移动距离

    private Vector3 m_bigBg_position_start;
    private Vector3 m_bigBg_position_center;
    private Vector3 m_bigBg_position_end;
    private Vector3 m_smallBg_position_start;
    private Vector3 m_smallBg_position_center;
    private Vector3 m_smallBg_position_end;

    void Start () {

        m_windmill = transform.Find("windmill");

        m_bigBg_start = transform.Find("303010515_cloud_bg_start");
        m_bigBg_center = transform.Find("303010515_cloud_bg_center");
        m_bigBg_end = transform.Find("303010515_cloud_bg_end");

        //m_smallBg_start = transform.Find("303010515/303010515_cloud_small_start");
        //m_smallBg_center = transform.Find("303010515/303010515_cloud_small_center");
        //m_smallBg_end = transform.Find("303010515/303010515_cloud_small_end");

        dis = Vector3.Distance(m_bigBg_start.position, m_bigBg_end.position) / bigBgMoveTime;

        m_bigBg_position_start = m_bigBg_start.position;
        m_bigBg_position_center = m_bigBg_center.position;
        m_bigBg_position_end = m_bigBg_end.position;

        //m_smallBg_position_start = m_smallBg_start.position;
        //m_smallBg_position_center = m_smallBg_center.position;
        //m_smallBg_position_end = m_smallBg_end.position;

        m_bigBg_center.DOMove(m_bigBg_position_end, bigBgMoveTime / 2).SetEase(Ease.Linear).OnComplete(()=> {

            MoveBigBg(m_bigBg_center, m_bigBg_start);
        });
        MoveBigBg(m_bigBg_start, m_bigBg_center);

        //m_smallBg_center.DOMove(m_smallBg_position_end, smallBgMoveTime / 2).SetEase(Ease.Linear).OnComplete(() =>
        //{

        //    MoveSmallBg(m_smallBg_center);
        //});
        //MoveSmallBg(m_smallBg_start);
    }

    private void Update()
    {
        if (m_windmill != null)
        {
            m_windmill.Rotate(new Vector3(0, 0, Time.deltaTime * windmillSpeed));
        }
    }

    private void MoveBigBg(Transform target, Transform followTarget)
    {
        Transform moveStart = followTarget.GetChild(0);
        target.position = moveStart.position;
        float moveTime = Vector3.Distance(moveStart.position, m_bigBg_position_end) / dis;
        target.DOMove(m_bigBg_position_end, moveTime).SetEase(Ease.Linear).OnComplete(() =>
        {
            MoveBigBg(target, followTarget);
        });
    }

    //private void MoveSmallBg(Transform target)
    //{
    //    target.position = m_smallBg_position_start;
    //    target.DOMove(m_smallBg_position_end, smallBgMoveTime).SetEase(Ease.Linear).OnComplete(() =>
    //    {
    //        MoveSmallBg(target);
    //    });
    //}


    public void Dispose()
    {
        m_bigBg_start.DOKill();
        m_bigBg_center.DOKill();
    }

}

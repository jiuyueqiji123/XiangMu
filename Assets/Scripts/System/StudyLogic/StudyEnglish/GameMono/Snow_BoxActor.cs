﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Snow_BoxActor : EnglishBaseActor
{


    public enum AnimatorNameEnum
    {
        Static,
        Shake,
    }



    public void CrossFade(AnimatorNameEnum name, float val)
    {
        AnimatorEx.CrossFade(name.ToString(), val);
    }




    public override void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration)
    {
        base.Init(startPoint, endPoint, outPoint, letter, letterOffset, audioName, atlasID, hintTexture, duration);


        transform.position = startPoint;
        transform.DOMove(endPoint, duration).SetEase(m_InEase);

    }

    public override void StartYourShowtime(float duration = 1.5F)
    {
        //transform.DOMove(m_OutPoint, duration).SetEase(Ease.Linear);
        //transform.DOScale(1.5f, duration * 0.5f).SetEase(m_InEase);

        CrossFade(AnimatorNameEnum.Shake, 0.01f);

        this.AddTimerEx(0.5f, () =>
        {
            //gameObject.SetActive(false);
            this.PlayEffectEx(StudyPath.effect_502003024, transform.position, duration);
        });
    }

    public override void LeaveTheStage(float duration = 1.5F)
    {
        transform.DOMove(m_OutPoint, duration).SetEase(m_OutEase);
    }

    public override void Shake(float duration = 0.5F, float strength = 5)
    {
        base.Shake(duration, strength);

        this.PlayEffectEx(StudyPath.effect_502003031, transform.position, duration);

    }


}

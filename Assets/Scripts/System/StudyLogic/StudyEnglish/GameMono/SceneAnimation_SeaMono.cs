﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SceneAnimation_SeaMono : MonoBehaviour {


    private Transform m_fishParent;
    private List<Transform> m_fishList = new List<Transform>();
    private List<GameObject> m_fishObjList = new List<GameObject>();

    private const float fishMinMoveTime = 20;
    private const float fishMaxMoveTime = 50;
    private const float fishIntervalTime = 3;
    private float fishAddTime;

    // Use this for initialization
    void Start () {

        m_fishList.Clear();

        m_fishParent = transform.Find("fishs");
        //if (m_fishParent != null)
        //{
        for (int i = 0; i < m_fishParent.childCount; i++)
        {
            Transform fish = m_fishParent.GetChild(i);
            fish.gameObject.SetActive(false);
            m_fishList.Add(fish);
        }

        for (int i = 0; i < 10; i++)
            CreateFish();

    }
	
	// Update is called once per frame
	void Update ()
    {

        CheckFishCreate();
    }

    private void CheckFishCreate()
    {
        if (Time.time > fishAddTime)
        {
            fishAddTime = Time.time + fishIntervalTime + Random.Range(-0.5f, 0.5f);
            CreateFish();
        }

    }

    private void CreateFish()
    {
        Vector3 startPoint, endPoint;
        bool isLeft = Random.Range(1, 3) == 1;
        GetScreenRandomPoint(Camera.main, out startPoint, out endPoint, isLeft);
        Transform fish = m_fishList[Random.Range(0, m_fishList.Count)];
        GameObject go = GameObject.Instantiate(fish.gameObject);
        go.SetActive(true);
        startPoint.z = fish.position.z;
        endPoint.z = fish.position.z;
        go.transform.SetParent(fish.parent);
        go.transform.position = startPoint;
        go.transform.rotation = fish.rotation;
        go.transform.localScale = fish.localScale;
        if (isLeft)
        {
            Vector3 scale = go.transform.localScale;
            scale.x = -scale.x;
            go.transform.localScale = scale;
        }
        float moveTime = Random.Range(fishMinMoveTime, fishMaxMoveTime);
        go.transform.DOMove(endPoint, moveTime).SetEase(Ease.Linear);
        m_fishObjList.Add(go);
        GameObject.Destroy(go, moveTime + 0.1f);
    }

    private void GetScreenRandomPoint(Camera camera, out Vector3 startPoint, out Vector3 endPoint, bool isLeft, float depth = 10)
    {
        float left_x = Screen.width * -0.1f;
        float right_x = Screen.width * 1.1f;
        float y = Random.Range(Screen.height * 0.1f, Screen.height * 0.9f);

        endPoint = isLeft ? camera.ScreenToWorldPoint(new Vector3(right_x, y, depth)) : camera.ScreenToWorldPoint(new Vector3(left_x, y, depth));
        startPoint = isLeft ? camera.ScreenToWorldPoint(new Vector3(left_x, y, depth)) : camera.ScreenToWorldPoint(new Vector3(right_x, y, depth));
    }

    public void Dispose()
    {

        for (int i = 0; i < m_fishObjList.Count; i++)
        {
            GameObject go = m_fishObjList[i];
            if (go != null)
            {
                go.transform.DOKill();
                GameObject.Destroy(go);
            }
        }
        m_fishObjList.Clear();
    }

}

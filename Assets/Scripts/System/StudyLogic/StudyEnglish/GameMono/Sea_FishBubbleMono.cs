﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


// 鱼泡组件
public class Sea_FishBubbleMono : EnglishBaseMono
{

    private Vector3 randomPoint;

    private bool bMoving;

    private float speed = 3f;

    private void Update()
    {
        if (!bMoving)
            return;

        if (Vector3.Distance(transform.position, randomPoint) < 1f)
        {
            float val = 10f;
            randomPoint = m_EndPoint + Random.Range(-val, val) * Vector3.right + Random.Range(-val, val) * Vector3.up;
        }
        Vector3 dir = randomPoint - transform.position;
        transform.position += dir.normalized * Time.deltaTime * speed;
    }

    public override void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration)
    {
        base.Init(startPoint, endPoint, outPoint, letter, letterOffset, audioName, atlasID, hintTexture, duration);


        transform.localScale = Vector3.zero;
        transform.position = m_StartPoint;
        transform.DOMove(m_EndPoint, duration);
        transform.DOScale(Vector3.one, duration);

        bMoving = false;
        AddTimer(duration, () =>
        {
            bMoving = true;
            randomPoint = transform.position;
        });
    }

    public override void StartYourShowtime(float duration = 1.5F)
    {
        bMoving = false;
        transform.DOScale(1.5f, duration).SetEase(m_InEase);
        AddTimer(duration, () =>
        {
            gameObject.SetActive(false);
            PlayEffect(StudyPath.effect_502003022, transform.position, 3);
        });
        //Destroy(gameObject, duration);
    }

    public override void LeaveTheStage(float duration = 1.5F)
    {
        bMoving = false;
        transform.DOMove(transform.position + Vector3.up * 300, duration).SetEase(m_OutEase);
    }

    public override void Shake(float duration = 0.5F, float strength = 1)
    {
        //base.Shake(duration, strength);

        bMoving = false;
        transform.DOShakePosition(duration, strength).OnComplete(() =>
        {
            transform.position = m_EndPoint;
            randomPoint = transform.position;
            bMoving = true;
        });
    }

}

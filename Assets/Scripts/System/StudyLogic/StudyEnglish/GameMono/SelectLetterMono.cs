﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectLetterMono : MonoBehaviour {



    [SerializeField]
    private uint _dataID;
    public uint dataID
    {
        get { return _dataID; }
    }


    public EEnglishSceneType testType;

    public Transform letter;
    public Transform letterShadow;

    public void Initialize(uint ID, Vector3 position, EEnglishSceneType type, string loadImage)
    {
        _dataID = ID;
        transform.position = position;
        testType = type;

        letter = transform.Find("root/letter");
        letterShadow = transform.Find("root/shadow");

        MeshRenderer meshRenderer = letter.GetComponent<MeshRenderer>();
        string imagePath = StudyPath.english_select_letter_texture_basepath + loadImage;

        meshRenderer.material.SetTexture("_MainTex", Res.LoadTexture(imagePath));

    }



}

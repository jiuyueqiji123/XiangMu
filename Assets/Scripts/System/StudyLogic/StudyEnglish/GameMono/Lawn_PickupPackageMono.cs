﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Lawn_PickupPackageMono : EnglishBaseMono
{

    private Vector3 randomPoint;

    private bool bMoving;

    private float speed = 3f;

    private void Update()
    {
        if (!bMoving)
            return;

        if (Vector3.Distance(transform.position, randomPoint) < 0.2f)
        {
            float val = 7;
            float rightval = 3f;
            randomPoint = m_EndPoint + Random.Range(-val, val) * Vector3.right + Random.Range(-rightval, rightval) * Vector3.up;
        }
        Vector3 dir = randomPoint - transform.position;
        transform.position += dir.normalized * Time.deltaTime * speed;
    }

    public override void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration)
    {
        base.Init(startPoint, endPoint, outPoint, letter, letterOffset, audioName, atlasID, hintTexture, duration);

        transform.position = m_StartPoint;
        transform.DOMove(m_EndPoint, duration).SetEase(m_InEase);

        bMoving = false;
        AddTimer(duration, () =>
        {
            bMoving = true;
            randomPoint = transform.position;
        });
    }

    // 选中的目标直接缩小
    public override void StartYourShowtime(float duration = 1.5f)
    {
        bMoving = false;
        //m_OutPoint = transform.position + transform.right * 15;

        transform.DOScale(1.5f, 0.5f).SetEase(Ease.Linear);
        AddTimer(duration - 0.5f, () => {

            this.PlayAudioEx(StudyAudioName.t_31002);
            transform.DOMove(m_OutPoint, duration);
            transform.DOScale(0, duration);
        });
    }

    // 其它的往右飞
    public override void LeaveTheStage(float duration = 1.5F)
    {
        bMoving = false;
        m_OutPoint = transform.position + transform.right * 500;
        transform.DOMove(m_OutPoint, duration).SetEase(m_OutEase);
    }

    public override void Shake(float duration = 0.5F, float strength = 10)
    {
        //base.Shake(duration, strength);

        bMoving = false;
        transform.DOShakePosition(duration, strength).OnComplete(() =>
        {
            transform.position = m_EndPoint;
            randomPoint = transform.position;
            bMoving = true;
        });
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StudyGameMachine
{
    public StudyGameMachine()
    {
        m_GamepartList = new List<IInitiativeBase>();
    }
    public delegate void GameAllPartCompleteDelegate(bool isComplete);
    private GameAllPartCompleteDelegate gameallpartCallback = null;

    public delegate void GamePartCompleteDelegate(bool bComplete, object[] paramArray);
    private GamePartCompleteDelegate gamepartCallback = null;


    public int partCount
    {
        get { return m_GamepartList == null ? 0 : m_GamepartList.Count; }
    }

    private bool isRandomPart;
    private int m_partIndex = 0;

    private List<int> m_randomPartList = new List<int>();
    // 游戏流程
    private List<IInitiativeBase> m_GamepartList = null;
    // 当前环节
    private IInitiativeBase m_CurrentGamepart = null;


    public void RegisterGamePart(IInitiativeBase part)
    {
        part.Register(m_GamepartList.Count);
        m_GamepartList.Add(part);
    }

    public void StartGamePart(GameAllPartCompleteDelegate allpartCallback = null, GamePartCompleteDelegate partCallback = null)
    {
        StartGamePart(0, allpartCallback, partCallback);
    }


    /// <summary>
    /// 开始游戏关卡流程
    /// </summary>
    /// <param name="startIndex">如果是-1，每轮都是随机抽取</param>
    /// <param name="allpartCallback"></param>
    /// <param name="partCallback"></param>
    public void StartGamePart(int startIndex, GameAllPartCompleteDelegate allpartCallback = null, GamePartCompleteDelegate partCallback = null)
    {
        gameallpartCallback = allpartCallback;
        gamepartCallback = partCallback;

        isRandomPart = startIndex == -1;

        m_partIndex = startIndex;
        if (isRandomPart)
        {
            m_randomPartList.Clear();
            for (int i = 0; i < m_GamepartList.Count; i++)
                m_randomPartList.Add(i);
            int rdIndex = Random.Range(0, m_randomPartList.Count);
            m_partIndex = m_randomPartList[rdIndex];
            m_randomPartList.RemoveAt(rdIndex);
        }
        IInitiativeBase part = GetGamePart(m_partIndex);
        if (part != null)
        {
            m_CurrentGamepart = part;
            part.OnStart(GamePartCompleteCallback);
        }
        else
        {
            if (gameallpartCallback != null)
            {
                gameallpartCallback(false);
            }
            ClearChache();
        }
    }

    public void UpdateGamePart()
    {
        if (m_CurrentGamepart != null)
        {
            m_CurrentGamepart.OnUpdate();
        }
    }

    public void StartNextPart()
    {
        m_partIndex++;
        if (isRandomPart)
        {
            if (m_randomPartList.Count == 0)
            {
                GamePartCompleteCallback(false, null);
                return;
            }
            int rdIndex = Random.Range(0, m_randomPartList.Count);
            m_partIndex = m_randomPartList[rdIndex];
            m_randomPartList.RemoveAt(rdIndex);
        }

        IInitiativeBase part = GetGamePart(m_partIndex);
        if (part != null)
        {
            Debug.Log("----------------------: Machine StartNextPart");
            m_CurrentGamepart = part;
            part.OnStart(GamePartCompleteCallback);
        }
        else
        {
            if (gameallpartCallback != null)
            {
                gameallpartCallback(false);
            }
            ClearChache();
        }
    }

    public void StopCurrentGamePart()
    {
        if (m_CurrentGamepart != null)
        {
            m_CurrentGamepart.OnExit();
            m_CurrentGamepart = null;
        }
    }

    public void StopAllGamePart()
    {
        StopCurrentGamePart();
        ClearChache();
    }

    public IInitiativeBase GetGamePart(int index)
    {
        if (index >= m_GamepartList.Count)
            return null;
        return m_GamepartList[index];
    }

    private void GamePartCompleteCallback(bool bComplete, object[] paramArray)
    {
        if (isRandomPart)
        {
            if (m_randomPartList.Count == 0)
            {
                if (gameallpartCallback != null)
                {
                    gameallpartCallback(bComplete);
                }
                ClearChache();
            }
            else
            {
                if (gamepartCallback != null)
                {
                    gamepartCallback(bComplete, paramArray);
                }
            }
        }
        else
        {
            if (m_partIndex >= m_GamepartList.Count - 1)
            {
                if (gameallpartCallback != null)
                {
                    gameallpartCallback(bComplete);
                }
                ClearChache();
            }
            else
            {
                if (gamepartCallback != null)
                {
                    gamepartCallback(bComplete, paramArray);
                }
            }
        } 
    }

    private void ClearChache()
    {
        m_partIndex = 0;
        m_GamepartList.Clear();
        m_randomPartList.Clear();
    }


}
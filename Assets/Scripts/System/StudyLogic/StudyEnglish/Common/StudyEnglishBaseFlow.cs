﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StudyEnglishBaseFlow : InitiativeBaseFlow, IEasyAudio, IEasyEffect
{
    public StudyEnglishBaseFlow(List<uint> partList)
    {
        m_PartList = partList;
    }


    private Transform _Scene;
    public Transform Scene
    {
        get { return _Scene; }
    }

    protected StudyGameMachine gameMachine = new StudyGameMachine();
    protected List<uint> m_PartList = new List<uint>();


    protected override void AddEvent()
    {
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_HOVER, OnMouseHover);
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_UP, OnMouseUp);
    }

    protected override void RemoveEvent()
    {
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_HOVER, OnMouseHover);
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_UP, OnMouseUp);
    }

    protected virtual void OnMouseDown() { }
    protected virtual void OnMouseHover() { }
    protected virtual void OnMouseUp() { }


    private void OnGamePartCallback(bool bComplete)
    {
        if (bComplete)
        {
            gameMachine.StartNextPart();
        }
        else
        {
            OnFlowComplete(false);
        }
    }

    protected virtual void OnGameAllPartCallback()
    {
        //Debug.Log("----------------------: Flow OnGameAllPartCallback");
        OnFlowComplete(true);
    }


    protected virtual void SetScene(Transform scene)
    {
        _Scene = scene;
    }

    protected virtual Transform CreateScene(string scenePath)
    {
        _Scene = Instantiate(scenePath, true).transform;

        Camera targetCamera = _Scene.Find("Camera").GetComponent<Camera>();
        //Camera targetCamera = scene.GetComponentInChildren<Camera>();
        if (targetCamera != null)
        {
            Camera.main.CopyFrom(targetCamera);
            targetCamera.gameObject.SetActive(false);
        }
        return _Scene;
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        gameMachine.StopAllGamePart();
    }


}

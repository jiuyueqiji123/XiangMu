﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using WCBG.ToolsForUnity.Tools;
using WCBG.ToolsForUnity.Extension;

public class EnglishBaseActor : StudyBaseActor, IEasyTimer, IEasyEffect
{
    public bool bTimerCallback
    {
        get { return this == null ? false : true; }
    }


    private PaoPaoCompnent _PpComp;
    public PaoPaoCompnent PpComp
    {
        get
        {
            if (_PpComp == null)
                _PpComp = GetComponentInChildren<PaoPaoCompnent>();
            return _PpComp;
        }
    }

    [SerializeField]
    protected string _Letter;
    public string Letter
    {
        get { return _Letter; }
    }

    private Text _LetterText;
    public Text LetterText
    {
        get
        {
            if (_LetterText == null)
            {
                _LetterText = transform.GetComponentInChildren<Text>();
            }
            return _LetterText;
        }
    }


    protected Ease m_InEase = Ease.InQuad;   //Ease.InOutCirc
    protected Ease m_OutEase = Ease.OutQuad;


    protected Vector3 m_StartPoint;    // 出生点
    protected Vector3 m_EndPoint;    // 到达点
    protected Vector3 m_OutPoint;    // 消失点
    public string m_audioName;
    protected int atlasID;
    protected float delayIn;
    protected float delayOut;

    private GameAudioSource audio;



    public virtual void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration)
    {
        Init(startPoint, endPoint, outPoint, letter, letterOffset, audioName, atlasID, hintTexture, duration, 0, 0);
    }

    public virtual void Init(Vector3 startPoint, Vector3 endPoint, Vector3 outPoint, string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration, float delayIn, float delayOut)
    {
        m_StartPoint = startPoint;
        m_EndPoint = endPoint;
        m_OutPoint = outPoint;
        _Letter = letter;
        m_audioName = audioName;
        this.atlasID = atlasID;
        this.delayIn = delayIn;
        this.delayOut = delayOut;

        if (LetterText != null)
        {
            LetterText.text = letter;
            if (!string.IsNullOrEmpty(letterOffset))
            {
                LetterText.rectTransform.anchoredPosition3D = letterOffset.ConvertToVector3();
            }
        }
        if (PpComp != null)
            PpComp.UpdateActive(false, false);

        if (atlasID > 0)
        {
            this.AddTimerEx(duration, () =>
            {
                if (PpComp != null)
                    PpComp.UpdateActive(true, true);
            });

            SetHintTexture(atlasID, hintTexture);
        }
    }

    public virtual void LoadAndSetLetter(string letter, string letterOffset, string audioName, int atlasID, string hintTexture, float duration = 1.5f)
    {
        letter = letter.Trim();
        _Letter = letter;
        m_audioName = audioName;
        this.atlasID = atlasID;
        if (LetterText != null)
        {
            LetterText.text = letter;
            if (!string.IsNullOrEmpty(letterOffset))
            {
                LetterText.rectTransform.anchoredPosition3D = letterOffset.ConvertToVector3();
            }
            else
            {
                LetterText.rectTransform.anchoredPosition3D = Vector3.zero;
            }
        }

        if (PpComp != null)
            PpComp.UpdateActive(false, false);
        if (atlasID > 0)
        {
            this.AddTimerEx(duration, () =>
            {
                if (PpComp != null)
                    PpComp.UpdateActive(true, true);
            });

            SetHintTexture(atlasID, hintTexture);
        }

    }

    public virtual void SetHintTexture(int atlasID, string hintTexture)
    {
        //emUIAltas uiAltas = (emUIAltas)atlasID;
        Sprite sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + hintTexture); // UISpriteManager.Instance.GetSprite(uiAltas, hintTexture);
        if (sprite != null && PpComp != null)
        {
            PpComp.SetTexture(sprite.ConvertToTexture2D());
        }
    }

    public void HideHintTexture()
    {
        if (atlasID > 0)
        {
            if (PpComp != null)
            {
                PpComp.UpdateActive(false, true);
            }
        }
    }

    /// <summary>
    /// 请开始你的表演
    /// 选中正确元素的时候，改如何处理
    /// </summary>
    public virtual void StartYourShowtime(float duration = 1.5f)
    {
        Debug.Log("Is time to show");
        //gameObject.SetActive(false);
    }

    /// <summary>
    /// 请开始你的表演
    /// 选中正确元素的时候，改如何处理
    /// </summary>
    public virtual void StartYourShowtime(Vector3 position, Vector3 scale, Quaternion rotation, float duration = 1.5f)
    {
        Debug.Log("Is time to show");
        //gameObject.SetActive(false);
    }

    /// <summary>
    /// 离开这个舞台
    /// 选中正确元素的时候，非正确元素如何表现
    /// </summary>
    public virtual void LeaveTheStage(float duration = 1.5f)
    {
        transform.DOMove(m_OutPoint, duration);
    }

    public virtual void Shake(float duration = 0.5f, float strength = 10)
    {
        //m_shakePoint = transform.position;
        transform.DOShakePosition(duration, strength).OnComplete(() =>
        {
            transform.position = m_EndPoint;
        });
    }

    public GameAudioSource PlayLetterAudio()
    {
        if (audio != null)
            return null;
        if (string.IsNullOrEmpty(m_audioName))
        {
            Debug.LogError("无音频参数! Letter:" + Letter);
            return null;
        }

        string[] arr = m_audioName.Split(';');
        if (arr.Length == 2)
        {
            audio = this.PlayCommonAudioEx(arr[0].Trim());
        }
        else
        {
            audio = this.PlayEnglishAudioEx(m_audioName);
        }

        this.AddTimerEx(audio.Length, () =>
        {
            audio = null;
        });

        return audio;
    }


}

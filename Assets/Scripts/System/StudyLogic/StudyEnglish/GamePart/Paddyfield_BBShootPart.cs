﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using WCBG.ToolsForUnity.Tools;


/*
麦田场景
BB弹射击游戏
*/
public class Paddyfield_BBShootPart : StudyEnglishBasePart
{

    public Paddyfield_BBShootPart(uint infoID) : base(infoID)
    {

    }

    private new Paddyfield_ScarecrowActor gameAsset1;
    private new Paddyfield_ScarecrowActor gameAsset2;
    private new Paddyfield_ScarecrowActor gameAsset3;

    public void SetGameAsset(GameObject go1, GameObject go2, GameObject go3)
    {
        gameAsset1 = go1.GetComponent<Paddyfield_ScarecrowActor>();
        gameAsset2 = go2.GetComponent<Paddyfield_ScarecrowActor>();
        gameAsset3 = go3.GetComponent<Paddyfield_ScarecrowActor>();
    }

    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        SetScene((Transform)paramArray[0]);
        m_partIndex = (int)paramArray[1];

        List<int> rdList = new List<int>();
        rdList.Add(1);
        rdList.Add(2);
        rdList.Add(3);

        int _random1 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random1);
        gameAsset1.LoadAndSetLetter(GetInfoLetter(_random1), GetInfoLetterOffset(_random1), GetInfoLetterAudio(_random1), GetInfoAtlasID(_random1), GetInfoAtlasTexture(_random1));

        int _random2 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random2);
        gameAsset2.LoadAndSetLetter(GetInfoLetter(_random2), GetInfoLetterOffset(_random2), GetInfoLetterAudio(_random2), GetInfoAtlasID(_random2), GetInfoAtlasTexture(_random2));

        gameAsset3.LoadAndSetLetter(GetInfoLetter(rdList[0]), GetInfoLetterOffset(rdList[0]), GetInfoLetterAudio(rdList[0]), GetInfoAtlasID(rdList[0]), GetInfoAtlasTexture(rdList[0]));
        rdList.Clear();

        SetHintLetter();

    }

    protected override void OnPartExit()
    {
        base.OnPartExit();

        gameAsset1 = null;
        gameAsset2 = null;
        gameAsset3 = null;
    }

    protected override void OnGameStartDelay()
    {
        base.OnGameStartDelay();

        if (m_partIndex > 0)
        {
            PlayEffect(StudyPath.effect_502002008, Vector3.zero, 3);
            AddTimer(500, () =>
            {
                gameAsset1.MoveIn(1);
                gameAsset2.MoveIn(1);
                gameAsset3.MoveIn(1);
                this.PlayAudioEx(StudyAudioName.t_31015);
            });
        }
        EnglishLediAnimationWindow.Instance.PlayLediTalkAnimation(EnglishLediAnimationWindow.ELediFadeInType.BottomLeft, info.ledi_hint_audio, () =>
        {
            bLockInput = false;
        });
    }

    // 鼠标点击到某个对象的事件
    protected override void OnMouseDown()
    {

        if (bLockInput) return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target != null)
        {
            Paddyfield_ScarecrowActor mono = target.GetComponent<Paddyfield_ScarecrowActor>();

            if (mono == null)
            {
                PlayLetterEffect();
                return;
            }

            if (mono.Letter == info.param_1)
            {
                Debug.Log("正确答案");
                bLockInput = true;
                gameAsset1.HideHintTexture();
                gameAsset2.HideHintTexture();
                gameAsset3.HideHintTexture();

                List<Paddyfield_ScarecrowActor> list = new List<Paddyfield_ScarecrowActor>() { gameAsset1, gameAsset2, gameAsset3 };
                list.Remove(mono);
                AddTimer(500, () =>
                {
                    mono.MoveOut(1);
                    this.PlayAudioEx(StudyAudioName.t_31015);
                    AddTimer(500, () =>
                    {
                        list[0].MoveOut(1);
                        list[1].MoveOut(1);
                        this.PlayAudioEx(StudyAudioName.t_31015);
                        AddTimer(1000, () =>
                        {
                            //this.PlayAudioEx(StudyAudioName.t_31024);
                            MedalWindow.Instance.ShowXunzhang(m_partIndex + 1, true, MedalWindow.EXZCountEnum.Five, () =>
                            {
                                OnPartComplete();
                            });
                        });
                    });
                });

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31022);
                    });
                }

            }
            else
            {
                mono.CrossFade(Paddyfield_ScarecrowActor.AnimatorNameEnum.Shake, 0.1f);

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31023);
                    });
                }
            }

            PlayEffect(StudyPath.effect_502003029, FTools.GetRaycastHitPointByMousePoint(), 3);
            this.PlayAudioEx(StudyAudioName.t_31014);
        }

    }

    protected string GetInfoLetter(int index)
    {
        switch (index)
        {
            case 1:
                return info.param_1;
            case 2:
                return info.param_2;
            case 3:
                return info.param_3;
            default:
                return "";
        }
    }

    protected string GetInfoLetterAudio(int index)
    {
        switch (index)
        {
            case 1:
                return info.audio_param_1;
            case 2:
                return info.audio_param_2;
            case 3:
                return info.audio_param_3;
            default:
                return "";
        }
    }

    protected string GetInfoLetterOffset(int index)
    {
        switch (index)
        {
            case 1:
                return info.param_1_offset;
            case 2:
                return info.param_2_offset;
            case 3:
                return info.param_3_offset;
            default:
                return "";
        }
    }

    protected int GetInfoAtlasID(int index)
    {
        switch (index)
        {
            case 1:
                return info.param_1_atlas;
            case 2:
                return info.param_2_atlas;
            case 3:
                return info.param_3_atlas;
            default:
                return 0;
        }
    }

    protected string GetInfoAtlasTexture(int index)
    {
        switch (index)
        {
            case 1:
                return info.param_1_texture;
            case 2:
                return info.param_2_texture;
            case 3:
                return info.param_3_texture;
            default:
                return "";
        }
    }

}

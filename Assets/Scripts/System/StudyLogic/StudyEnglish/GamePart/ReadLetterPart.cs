﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

/*
雪地场景
阅读字母
*/
public class ReadLetterPart : StudyEnglishBasePart
{
    public ReadLetterPart(uint infoID) : base(infoID)
    {

    }

    // 资源路径
    //protected string snowflakeAssetPath = "Study/English/Snow/Snowflake";

    protected override void OnPartStart()
    {
        base.OnPartStart();

        //EventBus.Instance.BroadCastEvent<uint>(EventID.ON_SNOW_READ_GAME_START, m_dataID);

        TimerManager.Instance.AddTimer(3000, 1, new Timer.OnTimeUpHandler((seq) =>
        {
            OnPartComplete();
        }));
    }

    protected override void OnPartExit()
    {
        base.OnPartExit();

        info = null;

        //EventBus.Instance.BroadCastEvent(EventID.ON_SNOW_GAME_PART_EXIT);
    }

    protected override void AddEvent()
    {
        //EventBus.Instance.AddEventHandler<Transform>(EventID.ON_MOUSE_CLICK_GAMEOBJECT, OnMouseClickGameObject);
    }

    protected override void RemoveEvent()
    {
        //EventBus.Instance.RemoveEventHandler<Transform>(EventID.ON_MOUSE_CLICK_GAMEOBJECT, OnMouseClickGameObject);
    }


}

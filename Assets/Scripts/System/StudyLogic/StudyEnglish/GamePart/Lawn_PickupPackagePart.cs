﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using WCBG.ToolsForUnity.Tools;


/*
草地场景
拿包裹游戏
*/
public class Lawn_PickupPackagePart : StudyEnglishBasePart
{

    public Lawn_PickupPackagePart(uint infoID) : base(infoID)
    {

    }

    // 素材路径
    private const string assetPath1 = StudyPath.english_lawn_balloon_blue;
    private const string assetPath2 = StudyPath.english_lawn_balloon_orange;
    private const string assetPath3 = StudyPath.english_lawn_balloon_red;

    private Transform m_smallPoint;


    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        SetScene((Transform)paramArray[0]);
        m_partIndex = (int)paramArray[1];

        m_smallPoint = Scene.Find("smallPoint");

        FindGameAssetPoints(Scene);
        SetHintLetter();
    }

    protected override void OnGameStartDelay()
    {
        base.OnGameStartDelay();

        SpawnGameAsset<Lawn_PickupPackageMono>(assetPath1, assetPath2, assetPath3);
        SetGameAssetRandomPoint();

        this.AddTimerEx(gameassetStartTime, () =>
        {
            EnglishLediAnimationWindow.Instance.PlayLediTalkAnimation(EnglishLediAnimationWindow.ELediFadeInType.BottomLeft, info.ledi_hint_audio, () =>
            {
                bLockInput = false;
            });
        });
    }

    protected override void OnMouseDown()
    {
        if (bLockInput) return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target != null)
        {
            EnglishBaseMono mono = target.GetComponent<EnglishBaseMono>();

            if (mono == null)
            {
                PlayLetterEffect();
                return;
            }

            if (mono.Letter == info.param_1)
            {
                //Debug.Log("选择正确！");
                bLockInput = true;
                gameAsset1.HideHintTexture();
                gameAsset2.HideHintTexture();
                gameAsset3.HideHintTexture();

                //AddTimer(0.5f, () =>
                //{
                    GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31022);
                        //GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_31002);
                        //AddTimer(audio.Length, () =>
                        //{
                        //    this.PlayAudioEx(StudyAudioName.t_31022);
                        //});
                    });
                }
                //});

                AddTimer(selectWaitTime, () =>
                {
                    gameAsset1.StartYourShowtime(gameasset1OutTime);

                    AddTimer(otherassetWaitTime, () =>
                    {
                        PlayEffect(StudyPath.effect_502002008, Vector3.zero, 3);

                        AddTimer(500, () =>
                        {
                            gameAsset2.LeaveTheStage(otherassetOutTime);
                            gameAsset3.LeaveTheStage(otherassetOutTime);
                            this.PlayAudioEx(StudyAudioName.t_31001);

                            AddTimer(otherassetOutTime, () =>
                            {
                                //this.PlayAudioEx(StudyAudioName.t_31024);
                                MedalWindow.Instance.ShowXunzhang(m_partIndex + 1, true, MedalWindow.EXZCountEnum.Five, () =>
                                {
                                    OnPartComplete();
                                });
                            });
                        });
                    });
                });

            }
            else
            {
                mono.Shake(0.5f);

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31023);
                    });
                }
            }
        }
    }

    protected override void SetGameAssetRandomPoint()
    {
        Vector3 startPoint = Vector3.zero;
        Vector3 endPoint = Vector3.zero;
        Vector3 outPoint = Vector3.zero;

        List<int> rdList = new List<int>();
        rdList.Add(1);
        rdList.Add(2);
        rdList.Add(3);

        int _random1 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random1);
        GetGameAssetPoints(_random1, out startPoint, out endPoint, out outPoint);
        gameAsset1.Init(startPoint, endPoint, m_smallPoint.position, info.param_1, info.param_1_offset, info.audio_param_1, info.param_1_atlas, info.param_1_texture, gameassetStartTime);

        int _random2 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random2);
        GetGameAssetPoints(_random2, out startPoint, out endPoint, out outPoint);
        gameAsset2.Init(startPoint, endPoint, outPoint, info.param_2, info.param_2_offset, info.audio_param_2, info.param_2_atlas, info.param_2_texture, gameassetStartTime);

        GetGameAssetPoints(rdList[0], out startPoint, out endPoint, out outPoint);
        gameAsset3.Init(startPoint, endPoint, outPoint, info.param_3, info.param_3_offset, info.audio_param_3, info.param_3_atlas, info.param_3_texture, gameassetStartTime);
        rdList.Clear();

        this.PlayAudioEx(StudyAudioName.t_31001);

    }


}

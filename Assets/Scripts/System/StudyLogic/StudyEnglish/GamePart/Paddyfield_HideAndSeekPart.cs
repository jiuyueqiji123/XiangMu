﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using WCBG.ToolsForUnity.Tools;

/*
麦田场景
捉迷藏游戏
*/
public class Paddyfield_HideAndSeekPart : StudyEnglishBasePart
{
    public Paddyfield_HideAndSeekPart(uint infoID) : base(infoID)
    {

    }

    protected string assetPath = StudyPath.english_paddyfield_hayrick;    // 资源路径

    //private new Paddyfield_HayrickMono gameAsset1;
    //private new Paddyfield_HayrickMono gameAsset2;
    //private new Paddyfield_HayrickMono gameAsset3;


    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        SetScene((Transform)paramArray[0]);
        m_partIndex = (int)paramArray[1];

        FindGameAssetPoints(Scene);
        SetHintLetter();

    }

    protected override void OnGameStartDelay()
    {
        base.OnGameStartDelay();

        SpawnGameAsset<Paddyfield_HayrickMono>(assetPath);
        this.AddTimerEx(gameassetStartTime, () =>
        {
            EnglishLediAnimationWindow.Instance.PlayLediTalkAnimation(EnglishLediAnimationWindow.ELediFadeInType.BottomLeft, info.ledi_hint_audio, () =>
            {
                bLockInput = false;
            });
        });
    }


    protected override void OnMouseDown()
    {
        if (bLockInput) return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target != null)
        {
            EnglishBaseMono mono = target.GetComponent<EnglishBaseMono>();

            if (mono == null)
            {
                PlayLetterEffect();
                return;
            }

            if (mono.Letter == info.param_1)
            {
                Debug.Log("选择正确！");
                bLockInput = true;
                gameAsset1.HideHintTexture();
                gameAsset2.HideHintTexture();
                gameAsset3.HideHintTexture();

                AddTimer(selectWaitTime, () =>
                {
                    mono.LeaveTheStage(paddyfield_caoduoOutTime);
                    GameAudioSource gameAudioSource = this.PlayAudioEx(StudyAudioName.t_31016);
                    AddTimer(paddyfield_caoduoOutTime, () => {
                        gameAudioSource.Stop();
                    });

                    List<EnglishBaseMono> list = new List<EnglishBaseMono>() { gameAsset1, gameAsset2, gameAsset3 };
                    list.Remove(mono);
                    AddTimer(otherassetWaitTime, () =>
                    {
                        list[0].LeaveTheStage(paddyfield_caoduoOutTime);
                        list[1].LeaveTheStage(paddyfield_caoduoOutTime);
                        GameAudioSource gameAudioSource1 = this.PlayAudioEx(StudyAudioName.t_31016);
                        AddTimer(paddyfield_caoduoOutTime, () =>
                        {
                            gameAudioSource1.Stop();
                        });

                        AddTimer(1000, () =>
                        {
                            //this.PlayAudioEx(StudyAudioName.t_31024);
                            MedalWindow.Instance.ShowXunzhang(m_partIndex + 1, true, MedalWindow.EXZCountEnum.Four, () =>
                            {
                                OnPartComplete();
                            });
                        });
                    });
                });

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31022);
                    });
                }
            }
            else
            {
                mono.Shake(0.5f);

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31023);
                    });
                }
            }

            mono.PlayEffect(StudyPath.effect_502003030, FTools.GetRaycastHitPointByMousePoint(), 3);

        }
    }


    protected override void SpawnGameAsset<T>(string assetPath)
    {
        base.SpawnGameAsset<T>(assetPath);
        

        Vector3 startPoint, endPoint, outPoint;

        List<int> rdList = new List<int>();
        rdList.Add(1);
        rdList.Add(2);
        rdList.Add(3);

        int _random1 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random1);
        GetGameAssetPoints(_random1, out startPoint, out endPoint, out outPoint);
        gameAsset1.Init(startPoint, endPoint, outPoint, info.param_1, info.param_1_offset, info.audio_param_1, info.param_1_atlas, info.param_1_texture, paddyfield_caoduoStartTime);
        if (_random1 == 1)
        {
            Transform child = gameAsset1.transform.GetChild(0);
            child.localScale = new Vector3(-child.localScale.x, child.localScale.y, child.localScale.z);
        }

        int _random2 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random2);
        GetGameAssetPoints(_random2, out startPoint, out endPoint, out outPoint);
        gameAsset2.Init(startPoint, endPoint, outPoint, info.param_2, info.param_2_offset, info.audio_param_2, info.param_2_atlas, info.param_2_texture, paddyfield_caoduoStartTime);
        if (_random2 == 1)
        {
            Transform child = gameAsset2.transform.GetChild(0);
            child.localScale = new Vector3(-child.localScale.x, child.localScale.y, child.localScale.z);
        }

        GetGameAssetPoints(rdList[0], out startPoint, out endPoint, out outPoint);
        gameAsset3.Init(startPoint, endPoint, outPoint, info.param_3, info.param_3_offset, info.audio_param_3, info.param_3_atlas, info.param_3_texture, paddyfield_caoduoStartTime);
        if (rdList[0] == 1)
        {
            Transform child = gameAsset3.transform.GetChild(0);
            child.localScale = new Vector3(-child.localScale.x, child.localScale.y, child.localScale.z);
        }
        rdList.Clear();

        GameAudioSource gameAudioSource1 = this.PlayAudioEx(StudyAudioName.t_31016);
        AddTimer(gameassetStartTime, () =>
        {
            gameAudioSource1.Stop();
        });

    }
    

}

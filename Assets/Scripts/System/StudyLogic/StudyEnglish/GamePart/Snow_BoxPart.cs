﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using WCBG.ToolsForUnity.Tools;

/*
飘雪花识字母游戏
*/
public class Snow_BoxPart : StudyEnglishBasePart
{
    public Snow_BoxPart(uint infoID) : base(infoID)
    {

    }

    // 资源路径
    protected string assetPath1 = StudyPath.english_snow_box_green;
    protected string assetPath2 = StudyPath.english_snow_box_orange;
    protected string assetPath3 = StudyPath.english_snow_box_purple;

    protected new EnglishBaseActor gameAsset1 = null;
    protected new EnglishBaseActor gameAsset2 = null;
    protected new EnglishBaseActor gameAsset3 = null;

    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        SetScene((Transform)paramArray[0]);
        m_partIndex = (int)paramArray[1];

        FindGameAssetPoints(Scene);
        SetHintLetter();

    }

    protected override void OnGameStartDelay()
    {
        base.OnGameStartDelay();

        SpawnGameAsset<Snow_BoxActor>(assetPath1, assetPath2, assetPath3);
        this.AddTimerEx(gameassetStartTime, () =>
        {
            EnglishLediAnimationWindow.Instance.PlayLediTalkAnimation(EnglishLediAnimationWindow.ELediFadeInType.BottomLeft, info.ledi_hint_audio, () =>
            {
                bLockInput = false;
            });
        });
    }

    protected override void OnMouseDown()
    {
        if (bLockInput) return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target != null)
        {
            EnglishBaseActor mono = target.GetComponent<EnglishBaseActor>();

            if (mono == null)
            {
                PlayLetterEffect();
                return;
            }

            if (mono.Letter == info.param_1)
            {
                Debug.Log("选择正确！");
                bLockInput = true;
                gameAsset1.HideHintTexture();
                gameAsset2.HideHintTexture();
                gameAsset3.HideHintTexture();

                AddTimer(selectWaitTime, () =>
                {
                    gameAsset1.StartYourShowtime();

                    AddTimer(otherassetWaitTime, () =>
                    {
                        gameAsset1.LeaveTheStage(otherassetOutTime);
                        gameAsset2.LeaveTheStage(otherassetOutTime);
                        gameAsset3.LeaveTheStage(otherassetOutTime);
                        this.PlayAudioEx(StudyAudioName.t_31017);

                        AddTimer(otherassetOutTime, () =>
                        {
                            //this.PlayAudioEx(StudyAudioName.t_31024);
                            MedalWindow.Instance.ShowXunzhang(m_partIndex + 1, true, MedalWindow.EXZCountEnum.Five, () =>
                            {
                                OnPartComplete();
                            });
                        });
                    });
                });

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_31018);
                        AddTimer(audio.Length, () =>
                        {
                            this.PlayAudioEx(StudyAudioName.t_31022);
                        });
                    });
                }

            }
            else
            {
                mono.Shake(0.5f);

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31023);
                    });
                }

            }

        }

    }

    protected new void SpawnGameAsset<T>(string assetPath1, string assetPath2, string assetPath3) where T : EnglishBaseActor
    {
        List<string> rdList1 = new List<string>() { assetPath1, assetPath2, assetPath3 };

        string _randomAsset1 = rdList1[Random.Range(0, rdList1.Count - 1)];
        rdList1.Remove(_randomAsset1);
        gameAsset1 = Instantiate(_randomAsset1, true).AddComponent<T>();

        string _randomAsset2 = rdList1[Random.Range(0, rdList1.Count - 1)];
        rdList1.Remove(_randomAsset2);
        gameAsset2 = Instantiate(_randomAsset2, true).AddComponent<T>();

        gameAsset3 = Instantiate(rdList1[0], true).AddComponent<T>();
        rdList1.Clear();

        Vector3 startPoint, endPoint, outPoint;

        List<int> rdList = new List<int>();
        rdList.Add(1);
        rdList.Add(2);
        rdList.Add(3);

        int _random1 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random1);
        GetGameAssetPoints(_random1, out startPoint, out endPoint, out outPoint);
        gameAsset1.Init(startPoint, endPoint, outPoint, info.param_1, info.param_1_offset, info.audio_param_1, info.param_1_atlas, info.param_1_texture, gameassetStartTime);

        int _random2 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random2);
        GetGameAssetPoints(_random2, out startPoint, out endPoint, out outPoint);
        gameAsset2.Init(startPoint, endPoint, outPoint, info.param_2, info.param_2_offset, info.audio_param_2, info.param_2_atlas, info.param_2_texture, gameassetStartTime);

        GetGameAssetPoints(rdList[0], out startPoint, out endPoint, out outPoint);
        gameAsset3.Init(startPoint, endPoint, outPoint, info.param_3, info.param_3_offset, info.audio_param_3, info.param_3_atlas, info.param_3_texture, gameassetStartTime);
        rdList.Clear();

        this.PlayAudioEx(StudyAudioName.t_31017);
    }

}

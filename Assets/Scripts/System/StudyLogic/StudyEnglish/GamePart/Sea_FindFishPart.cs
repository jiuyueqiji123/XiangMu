﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;
using DG.Tweening;
using UnityEngine.UI;


/*
海底场景
找鱼游戏
*/
public class Sea_FindFishPart : StudyEnglishBasePart
{

    public Sea_FindFishPart(uint infoID) : base(infoID)
    {
        
    }

    protected string assetPath = StudyPath.english_sea_baoxiang;    // 资源路径

    private Transform m_centerPoint;
    private Transform m_hintLetter;


    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        SetScene((Transform)paramArray[0]);
        m_partIndex = (int)paramArray[1];

        m_centerPoint = Scene.Find("GameCenterPoint");
        FindGameAssetPoints(Scene);
        m_hintLetter = SetHintLetter();
        MoveTarget();

    }

    protected override void OnPartExit()
    {
        base.OnPartExit();

        if (m_hintLetter != null)
        {
            m_hintLetter.DOKill();
        }
    }

    protected override void OnGameStartDelay()
    {
        base.OnGameStartDelay();

        SpawnGameAsset<Sea_BoxMono>(assetPath);
        this.AddTimerEx(gameassetStartTime, () =>
        {
            EnglishLediAnimationWindow.Instance.PlayLediTalkAnimation(EnglishLediAnimationWindow.ELediFadeInType.BottomLeft, info.ledi_hint_audio, () =>
            {
                bLockInput = false;
            });
        });
    }
    
    protected override void OnMouseDown()
    {
        if (bLockInput) return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target != null)
        {
            EnglishBaseMono mono = target.GetComponent<EnglishBaseMono>();

            if (mono == null)
            {
                PlayLetterEffect();
                return;
            }

            if (mono.Letter == info.param_1)
            {
                Debug.Log("选择正确！");
                bLockInput = true;
                gameAsset1.HideHintTexture();
                gameAsset2.HideHintTexture();
                gameAsset3.HideHintTexture();

                AddTimer(selectWaitTime, () =>
                {
                    float scaleTime = 0.5f;
                    gameAsset1.StartYourShowtime(scaleTime);

                    AddTimer(otherassetWaitTime, () =>
                    {
                        gameAsset2.LeaveTheStage(otherassetOutTime);
                        gameAsset3.LeaveTheStage(otherassetOutTime);
                        this.PlayAudioEx(StudyAudioName.t_31028);
                        AddTimer(otherassetOutTime, () =>
                        {
                            //this.PlayAudioEx(StudyAudioName.t_31024);
                            MedalWindow.Instance.ShowXunzhang(m_partIndex + 1, true, MedalWindow.EXZCountEnum.Four, () =>
                            {
                                OnPartComplete();
                            });
                        });
                    });
                });

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_31011);
                        AddTimer(audio.Length, () =>
                        {
                            this.PlayAudioEx(StudyAudioName.t_31022);
                        });
                    });
                }

            }
            else
            {
                mono.Shake(0.5f);

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31023);
                    });
                }
            }
            
        }

    }

    protected override void SpawnGameAsset<T>(string assetPath)
    {
        base.SpawnGameAsset<T>(assetPath);


        Vector3 startPoint, endPoint, outPoint;

        List<int> rdList = new List<int>();
        rdList.Add(1);
        rdList.Add(2);
        rdList.Add(3);

        int _random1 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random1);
        GetGameAssetPoints(_random1, out startPoint, out endPoint, out outPoint);
        gameAsset1.Init(startPoint, endPoint, m_centerPoint.position, info.param_1, info.param_1_offset, info.audio_param_1, info.param_1_atlas, info.param_1_texture, gameassetStartTime, GetDelayTime(_random1), 0);

        int _random2 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random2);
        bool isFirst = _random2 > rdList[0];
        GetGameAssetPoints(_random2, out startPoint, out endPoint, out outPoint);
        gameAsset2.Init(startPoint, endPoint, outPoint, info.param_2, info.param_2_offset, info.audio_param_2, info.param_2_atlas, info.param_2_texture, gameassetStartTime, GetDelayTime(_random2), isFirst ? 0 : 0.12f);

        GetGameAssetPoints(rdList[0], out startPoint, out endPoint, out outPoint);
        gameAsset3.Init(startPoint, endPoint, outPoint, info.param_3, info.param_3_offset, info.audio_param_3, info.param_3_atlas, info.param_3_texture, gameassetStartTime, GetDelayTime(rdList[0]), isFirst ? 0.12f : 0f);
        rdList.Clear();

        this.PlayAudioEx(StudyAudioName.t_31028);
    }
    
    private void MoveTarget()
    {
        float maxTime = 7;
        if (m_hintLetter != null)
        {
            Transform start = Scene.Find("HintLetter_Start");
            Transform end = Scene.Find("HintLetter_End");

            Vector3 movePoint = start.position + (end.position - start.position) * Random.value;
            float moveTime = Vector3.Distance(movePoint, m_hintLetter.position) / Vector3.Distance(start.position, end.position) * (maxTime + Random.Range(0, 5f));
            m_hintLetter.DOMove(movePoint, moveTime).SetEase(Ease.Linear).OnComplete(MoveTarget);
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

/*
雪地场景
学习字母
*/

public class StudyLetterPart : StudyEnglishBasePart
{

    public StudyLetterPart(uint infoID) : base(infoID)
    {

    }

    protected override void OnPartStart()
    {
        base.OnPartStart();



        WindowManager.Instance.OpenWindow(WinNames.LetterAnimationPanel);
        LetterAnimationWindow.Instance.StartAnimation(DataID, () => {

            Debug.Log("学习结束！");
            OnPartComplete();
        });


        //EventBus.Instance.BroadCastEvent<uint>(EventID.ON_ENGLISH_LEARN_GAME_START, DataID);
    }

    protected override void OnPartExit()
    {
        base.OnPartExit();
        //LetterAnimationController.Instance.CloseView();
        WindowManager.Instance.CloseWindow(WinNames.LetterAnimationPanel);

        //EventBus.Instance.BroadCastEvent(EventID.ON_ENGLISH_LEARN_GAME_EXIT);
    }

    protected override void AddEvent()
    {
        //EventBus.Instance.AddEventHandler<Transform>(EventID.ON_MOUSE_CLICK_GAMEOBJECT, OnMouseClickGameObject);
    }

    protected override void RemoveEvent()
    {
        //EventBus.Instance.RemoveEventHandler<Transform>(EventID.ON_MOUSE_CLICK_GAMEOBJECT, OnMouseClickGameObject);
    }
    

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

/*
画字母
 
*/
public class DrawLetterPart : StudyEnglishBasePart
{

    public DrawLetterPart(uint infoID) : base(infoID)
    {

    }

    // 资源路径
    protected string snowmanAssetPath = "Study/English/Snow/Snowman";
    protected string snowballAssetPath = "Study/English/Snow/Snowball";

    private DrawRealize3D drawController = null;

    GameObject sceneRoot;
    GameObject brush;
    Transform[] letterArray;
    GameObject drawMask;
    GameObject drawLetterParent;

    protected override void OnPartStart()
    {
        base.OnPartStart();
        
        //EventBus.Instance.BroadCastEvent<uint>(EventID.ON_SNOW_DRAW_LETTER_GAME_START, m_dataID);

        //DrawLetterController.Instance.OpenView();
        //DrawLetterController.Instance.StartDrawLetter("a", true, DrawLetterController.EDrawLetterScene.Lawn, (result) =>
        //{
        //    Debug.LogError("Result: " + result);
        //});

        sceneRoot = GameObject.Find("SceneRoot");
        brush = sceneRoot.transform.Find("DrawLetter/Brush_root").gameObject;
        letterArray = new Transform[] { sceneRoot.transform.Find("DrawLetter/B/B_001"), sceneRoot.transform.Find("DrawLetter/B/B_002"), sceneRoot.transform.Find("DrawLetter/B/B_003") };
        drawMask = Resources.Load("Study/Letter/DrawMask", typeof(GameObject)) as GameObject;
        drawLetterParent = sceneRoot.transform.Find("DrawLetter").gameObject;

        drawLetterParent.SetActive(true);

        drawController = new DrawRealize3D(brush.transform, letterArray, drawMask, drawLetterParent);
        drawController.OnStart(OnDrawCompleteCallback);

    }

    protected override void OnPartExit()
    {
        base.OnPartExit();

        info = null;
        if (drawController != null)
        {
            drawController.OnExit();
        }
        drawLetterParent.SetActive(false);

        //EventBus.Instance.BroadCastEvent(EventID.ON_SNOW_GAME_PART_EXIT);
    }
    
    private void OnDrawCompleteCallback()
    {
        Debug.Log("----------------------: OnDrawCompleteCallback");
        TimerManager.Instance.AddTimer(2000, 1, (seq) => {

            OnPartComplete();
        });
    }


}

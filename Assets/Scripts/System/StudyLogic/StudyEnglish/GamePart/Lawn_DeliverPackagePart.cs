﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using WCBG.ToolsForUnity.Tools;


/*
草地场景
送包裹游戏
*/
public class Lawn_DeliverPackagePart : StudyEnglishBasePart
{

    public Lawn_DeliverPackagePart(uint infoID) : base(infoID)
    {

    }

    // 素材路径
    private const string assetPath1 = StudyPath.english_lawn_package_green;
    private const string assetPath2 = StudyPath.english_lawn_package_orange;
    private const string assetPath3 = StudyPath.english_lawn_package_yellow;
    

    private Transform Pack_To1;
    private Transform Pack_To2;
    private Transform Pack_To3;
    private Transform Pack_To4;
    

    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        SetScene((Transform)paramArray[0]);
        m_partIndex = (int)paramArray[1];

        Pack_To1 = Scene.Find("train/Pack_To1");
        Pack_To2 = Scene.Find("train/Pack_To2");
        Pack_To3 = Scene.Find("train/Pack_To3");
        Pack_To4 = Scene.Find("train/Pack_To4");

        FindGameAssetPoints(Scene);
        SetHintLetter();

    }

    protected override void OnGameStartDelay()
    {
        base.OnGameStartDelay();

        SpawnGameAsset<Lawn_DeliverPackageMono>(assetPath1, assetPath2, assetPath3);
        SetGameAssetRandomPoint();

        this.AddTimerEx(gameassetStartTime + 0.4f, () =>
        {
            EnglishLediAnimationWindow.Instance.PlayLediTalkAnimation(EnglishLediAnimationWindow.ELediFadeInType.BottomLeft, info.ledi_hint_audio, ()=> {
                bLockInput = false;
            });
        });
    }

    protected override void OnPartExit()
    {
        base.OnPartExit();

        if (!IsPartComplete)
        {
            if (gameAsset1 != null)
            {
                GameObject.Destroy(gameAsset1.gameObject);
                gameAsset1 = null;
            }
        }
    }
    
    protected override void OnMouseDown()
    {
        if (bLockInput) return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target != null)
        {
            EnglishBaseMono mono = target.GetComponent<EnglishBaseMono>();

            if (mono == null)
            {
                PlayLetterEffect();
                return;
            }

            if (mono.Letter == info.param_1)
            {
                //Debug.Log("正确答案");
                bLockInput = true;
                gameAsset1.HideHintTexture();
                gameAsset2.HideHintTexture();
                gameAsset3.HideHintTexture();

                AddTimer(selectWaitTime, () =>
                {
                    gameAsset1.transform.SetParent(GetPackTo().parent);
                    gameAsset1.StartYourShowtime(GetPackTo().position, GetPackTo().localScale, GetPackTo().rotation, gameasset1OutTime);

                    AddTimer(otherassetWaitTime, () =>
                    {
                        gameAsset2.LeaveTheStage(otherassetOutTime);
                        gameAsset3.LeaveTheStage(otherassetOutTime);
                        this.PlayAudioEx(StudyAudioName.t_31027);

                        AddTimer(otherassetOutTime, () =>
                        {
                            //this.PlayAudioEx(StudyAudioName.t_31024);
                            MedalWindow.Instance.ShowXunzhang(m_partIndex + 1, true, MedalWindow.EXZCountEnum.Four, () =>
                            {
                                OnPartComplete(true, gameAsset1.gameObject);
                            });
                        });
                    });
                });


                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31022);
                    });
                }
            }
            else
            {
                mono.Shake(0.5f);

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31023);
                    });
                }
            }
        }

    }

    private Transform GetPackTo()
    {
        switch (m_partIndex)
        {
            case 0:
                return Pack_To1;
            case 1:
                return Pack_To2;
            case 2:
                return Pack_To3;
            case 3:
                return Pack_To4;
            default:
                Debug.LogError("partIndex: " + m_partIndex);

                return Pack_To1;
        }
    }

    protected override void SpawnGameAsset<T>(string assetPath1, string assetPath2, string assetPath3)
    {
        List<string> rdList = new List<string>() { assetPath1, assetPath2, assetPath3 };

        string _random1 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random1);
        gameAsset1 = Instantiate(_random1, false).AddComponent<T>();

        string _random2 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random2);
        gameAsset2 = Instantiate(_random2, true).AddComponent<T>();

        gameAsset3 = Instantiate(rdList[0], true).AddComponent<T>();
        rdList.Clear();

    }

    protected override void SetGameAssetRandomPoint()
    {
        Vector3 startPoint = Vector3.zero;
        Vector3 endPoint = Vector3.zero;
        Vector3 outPoint = Vector3.zero;

        List<int> rdList = new List<int>();
        rdList.Add(1);
        rdList.Add(2);
        rdList.Add(3);

        int _random1 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random1);
        GetGameAssetPoints(_random1, out startPoint, out endPoint, out outPoint);
        gameAsset1.Init(startPoint, endPoint, outPoint, info.param_1, info.param_1_offset, info.audio_param_1, info.param_1_atlas, info.param_1_texture, gameassetStartTime, GetDelayTime(_random1), 0);

        int _random2 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random2);
        bool isFirst = _random2 > rdList[0];
        GetGameAssetPoints(_random2, out startPoint, out endPoint, out outPoint);
        gameAsset2.Init(startPoint, endPoint, outPoint, info.param_2, info.param_2_offset, info.audio_param_2, info.param_2_atlas, info.param_2_texture, gameassetStartTime, GetDelayTime(_random2), isFirst ? 0 : 0.12f);

        GetGameAssetPoints(rdList[0], out startPoint, out endPoint, out outPoint);
        gameAsset3.Init(startPoint, endPoint, outPoint, info.param_3, info.param_3_offset, info.audio_param_3, info.param_3_atlas, info.param_3_texture, gameassetStartTime, GetDelayTime(rdList[0]), isFirst ? 0.12f : 0f);
        rdList.Clear();

        this.PlayAudioEx(StudyAudioName.t_31027);
    }




}

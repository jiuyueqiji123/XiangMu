﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using WCBG.ToolsForUnity.Tools;

/*
雪地场景
扔雪人
*/
public class Snow_ThrowSnowballPart : StudyEnglishBasePart
{
    public Snow_ThrowSnowballPart(uint infoID) : base(infoID)
    {

    }

    protected new Snow_SnowmanActor gameAsset1 = null;
    protected new Snow_SnowmanActor gameAsset2 = null;
    protected new Snow_SnowmanActor gameAsset3 = null;


    protected override void OnPartStart(params object[] paramArray)
    {
        base.OnPartStart(paramArray);

        //FTimeTool.Record("Snow_ThrowSnowballFlow OnPartStart");

        SetScene((Transform)paramArray[0]);
        m_partIndex = (int)paramArray[1];
        gameAsset1 = (Snow_SnowmanActor)paramArray[2];
        gameAsset2 = (Snow_SnowmanActor)paramArray[3];
        gameAsset3 = (Snow_SnowmanActor)paramArray[4];

        List<int> rdList = new List<int>();
        rdList.Add(1);
        rdList.Add(2);
        rdList.Add(3);

        int _random1 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random1);
        gameAsset1.LoadAndSetLetter(GetInfoLetter(_random1), GetInfoLetterOffset(_random1), GetInfoLetterAudio(_random1), GetInfoAtlasID(_random1), GetInfoAtlasTexture(_random1));

        int _random2 = rdList[Random.Range(0, rdList.Count - 1)];
        rdList.Remove(_random2);
        gameAsset2.LoadAndSetLetter(GetInfoLetter(_random2), GetInfoLetterOffset(_random2), GetInfoLetterAudio(_random2), GetInfoAtlasID(_random2), GetInfoAtlasTexture(_random2));

        gameAsset3.LoadAndSetLetter(GetInfoLetter(rdList[0]), GetInfoLetterOffset(rdList[0]), GetInfoLetterAudio(rdList[0]), GetInfoAtlasID(rdList[0]), GetInfoAtlasTexture(rdList[0]));
        rdList.Clear();

        SetHintLetter();

        //FTimeTool.Record("Snow_ThrowSnowballFlow OnPartStart");
    }

    protected override void OnGameStartDelay()
    {
        base.OnGameStartDelay();

        if (m_partIndex > 0)
        {
            PlayEffect(StudyPath.effect_502002008, Vector3.zero, 3);
            AddTimer(500, () =>
            {
                gameAsset1.MoveIn(1);
                gameAsset2.MoveIn(1);
                gameAsset3.MoveIn(1);
            });
        }

        EnglishLediAnimationWindow.Instance.PlayLediTalkAnimation(EnglishLediAnimationWindow.ELediFadeInType.BottomLeft, info.ledi_hint_audio, () =>
        {
            bLockInput = false;
        });

    }

    protected override void OnMouseDown()
    {
        if (bLockInput) return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();
        if (target != null)
        {
            Snow_SnowmanActor mono = target.GetComponent<Snow_SnowmanActor>();
            if (mono == null)
            {
                PlayLetterEffect();
                return;
            }

            if (mono.Letter == info.param_1)
            {
                Debug.Log("选择正确！");
                bLockInput = true;
                gameAsset1.HideHintTexture();
                gameAsset2.HideHintTexture();
                gameAsset3.HideHintTexture();

                List<Snow_SnowmanActor> list = new List<Snow_SnowmanActor>() { gameAsset1, gameAsset2, gameAsset3 };
                list.Remove(mono);

                AddTimer(selectWaitTime, () =>
                {
                    mono.MoveOut(gameasset1OutTime);
                    this.PlayAudioEx(StudyAudioName.t_31021);

                    AddTimer(500, () =>
                    {
                        list[0].MoveOut(otherassetOutTime);
                        list[1].MoveOut(otherassetOutTime);
                        this.PlayAudioEx(StudyAudioName.t_31021);
                        AddTimer(1000, () =>
                        {
                            //this.PlayAudioEx(StudyAudioName.t_31024);
                            MedalWindow.Instance.ShowXunzhang(m_partIndex + 1, true, MedalWindow.EXZCountEnum.Four, () =>
                            {
                                OnPartComplete();
                            });
                        });
                    });

                });

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_31020);
                        AddTimer(audio.Length, () =>
                        {
                            this.PlayAudioEx(StudyAudioName.t_31022);
                        });
                    });
                }
            }
            else
            {
                mono.CrossFade(Snow_SnowmanActor.AnimatorNameEnum.Shake, 0.01f);

                GameAudioSource letterAudio = mono.PlayLetterAudio();
                if (letterAudio != null)
                {
                    AddTimer(letterAudio.Length, () =>
                    {
                        this.PlayAudioEx(StudyAudioName.t_31023);
                    });
                }
            }
            
            PlayEffect(StudyPath.effect_502003028, FTools.GetRaycastHitPointByMousePoint(), 3);
        }

    }

    protected string GetInfoLetter(int index)
    {
        switch (index)
        {
            case 1:
                return info.param_1;
            case 2:
                return info.param_2;
            case 3:
                return info.param_3;
            default:
                return "";
        }
    }

    protected string GetInfoLetterAudio(int index)
    {
        switch (index)
        {
            case 1:
                return info.audio_param_1;
            case 2:
                return info.audio_param_2;
            case 3:
                return info.audio_param_3;
            default:
                return "";
        }
    }

    protected string GetInfoLetterOffset(int index)
    {
        switch (index)
        {
            case 1:
                return info.param_1_offset;
            case 2:
                return info.param_2_offset;
            case 3:
                return info.param_3_offset;
            default:
                return "";
        }
    }

    protected int GetInfoAtlasID(int index)
    {
        switch (index)
        {
            case 1:
                return info.param_1_atlas;
            case 2:
                return info.param_2_atlas;
            case 3:
                return info.param_3_atlas;
            default:
                return 0;
        }
    }

    protected string GetInfoAtlasTexture(int index)
    {
        switch (index)
        {
            case 1:
                return info.param_1_texture;
            case 2:
                return info.param_2_texture;
            case 3:
                return info.param_3_texture;
            default:
                return "";
        }
    }

}

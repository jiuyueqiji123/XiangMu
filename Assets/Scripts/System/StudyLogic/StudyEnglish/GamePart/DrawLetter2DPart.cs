﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

/*
画字母
 
*/
public class DrawLetter2DPart : StudyEnglishBasePart
{

    public DrawLetter2DPart(uint infoID) : base(infoID)
    {

    }


    protected override void OnPartStart()
    {
        base.OnPartStart();
        
        //EventBus.Instance.BroadCastEvent<uint>(EventID.ON_SNOW_DRAW_LETTER_GAME_START, m_dataID);

        //DrawLetterController.Instance.OpenView();
        //DrawLetterController.Instance.StartDrawLetter("a", true, DrawLetterController.EDrawLetterScene.Lawn, (result) =>
        //{
        //    Debug.LogError("Result: " + result);
        //});


    }

    protected override void OnPartExit()
    {
        base.OnPartExit();

        info = null;

        //EventBus.Instance.BroadCastEvent(EventID.ON_SNOW_GAME_PART_EXIT);
    }
    
    private void OnDrawCompleteCallback()
    {
        Debug.Log("----------------------: OnDrawCompleteCallback");
        TimerManager.Instance.AddTimer(2000, 1, (seq) => {

            OnPartComplete();
        });
    }


}

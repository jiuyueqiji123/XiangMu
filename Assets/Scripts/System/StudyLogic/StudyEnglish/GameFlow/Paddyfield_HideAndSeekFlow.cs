﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 1.流程二
 2.捉迷藏
     */
public class Paddyfield_HideAndSeekFlow : StudyEnglishBaseFlow
{

    public Paddyfield_HideAndSeekFlow(List<uint> partList) : base(partList)
    {
    }


    private const string scenePath = StudyPath.english_paddyfield_caoduo_scene;    // 场景路径
    private Transform m_scene;  // 场景
    private int partIndex = 0;

    private StudyEnglishBasePart m_currentPart;


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        m_scene = CreateScene(scenePath);
        partIndex = 0;
        StartGamePart();

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Four);
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        if (m_currentPart != null)
        {
            m_currentPart.OnExit();
            m_currentPart = null;
        }

        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }


    private void StartGamePart()
    {

        if (partIndex > 3)
        {
            OnAllGamePartComplete();
            return;
        }
        uint partID = m_PartList[partIndex];

        Paddyfield_HideAndSeekPart part = new Paddyfield_HideAndSeekPart(partID);
        m_currentPart = part;
        part.OnStart((bComplete) =>
        {
            part.OnExit();
            StartGamePart();
        }, m_scene, partIndex++);

    }

    private void OnAllGamePartComplete()
    {
        AddTimer(1500, () =>
        {
            OnFlowComplete(true, (int)EEnglishGameEnum.Game3);
        });
    }
    

}

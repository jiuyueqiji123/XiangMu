﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 1.流程二
 2.扔雪球
     */
public class Snow_ThrowSnowballFlow : StudyEnglishBaseFlow
{
    public Snow_ThrowSnowballFlow(List<uint> partList) : base(partList)
    {
    }

    private const string scenePath = StudyPath.english_snow_man_scene;    // 场景路径
    protected string assetPath = StudyPath.english_snow_snowman;    // 资源路径

    protected Snow_SnowmanActor gameAsset1 = null;
    protected Snow_SnowmanActor gameAsset2 = null;
    protected Snow_SnowmanActor gameAsset3 = null;

    protected Transform gameAsset1_End;
    protected Transform gameAsset2_End;
    protected Transform gameAsset3_End;

    private Transform m_scene;  // 场景
    private int partIndex = 0;
    private StudyEnglishBasePart m_currentPart;


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        //FTimeTool.Record("Snow_ThrowSnowballFlow OnFlowStart");

        m_scene = CreateScene(scenePath);
        LoadEffect();
        FindGameAssetPoints(m_scene);
        SpawnGameAsset<Snow_SnowmanActor>(assetPath);
        gameAsset1.transform.position = gameAsset1_End.position;
        gameAsset2.transform.position = gameAsset2_End.position;
        gameAsset3.transform.position = gameAsset3_End.position;

        partIndex = 0;
        StartGamePart();

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Four);

        //FTimeTool.Record("Snow_ThrowSnowballFlow OnFlowStart");
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();

        if (m_currentPart != null)
        {
            m_currentPart.OnUpdate();
        }
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        if (m_currentPart != null)
        {
            m_currentPart.OnExit();
            m_currentPart = null;
        }

        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }

    private void StartGamePart()
    {
        if (partIndex > 3)
        {
            OnAllGamePartComplete();
            return;
        }
        uint partID = m_PartList[partIndex];
        m_currentPart = new Snow_ThrowSnowballPart(partID);
        m_currentPart.OnStart((bComplete) =>
        {
            m_currentPart.OnExit();
            StartGamePart();
        }, m_scene, partIndex++, gameAsset1, gameAsset2, gameAsset3);

    }

    private void OnAllGamePartComplete()
    {
        AddTimer(1500, () =>
        {
            OnFlowComplete(true, (int)EEnglishGameEnum.Game3);
        });
    }


    protected void SpawnGameAsset<T>(string assetPath) where T : Snow_SnowmanActor
    {
        gameAsset1 = Instantiate(assetPath, true).AddComponent<T>();
        gameAsset2 = Instantiate(assetPath, true).AddComponent<T>();
        gameAsset3 = Instantiate(assetPath, true).AddComponent<T>();
    }

    protected void FindGameAssetPoints(Transform parent)
    {
        if (parent == null)
            return;
        gameAsset1_End = parent.Find("GameAsset1_End");
        gameAsset2_End = parent.Find("GameAsset2_End");
        gameAsset3_End = parent.Find("GameAsset3_End");
        if (gameAsset1_End == null) Debug.LogError("gameAsset1_End is null!");
        if (gameAsset2_End == null) Debug.LogError("gameAsset2_End is null!");
        if (gameAsset3_End == null) Debug.LogError("gameAsset3_End is null!");
    }

    private void LoadEffect()
    {
        GameObject go = Instantiate(StudyPath.effect_502003066, true);
        go.transform.SetParent(Scene.Find("EffectPoint"));
        go.transform.ResetTransformForLocal();
    }


}

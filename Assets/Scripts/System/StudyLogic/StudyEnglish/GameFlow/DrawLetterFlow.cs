﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using WCBG.ToolsForUnity.Extension;
using WCBG.ToolsForUnity.Tools;

/*
 1.流程三
 2.写字母
     */
public class DrawLetterFlow : StudyEnglishBaseFlow
{

    public DrawLetterFlow(List<uint> partList, EEnglishSceneType type) : base(partList)
    {
        m_sceneType = type;
    }

    private const string english_ui_scene_lawn = StudyPath.english_ui_scene_lawn;
    private const string english_ui_scene_paddyfield = StudyPath.english_ui_scene_paddyfield;
    private const string english_ui_scene_sea = StudyPath.english_ui_scene_sea;
    private const string english_ui_scene_snow = StudyPath.english_ui_scene_snow;

    private int partIndex = 0;
    private EnglishLetterInfo info;
    private EEnglishSceneType m_sceneType;
    private Texture2D m_letterTexture;

    private List<GameObject> m_packList = new List<GameObject>();

    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        WindowManager.Instance.OpenWindow(WinNames.DrawLetterPanel);
        WindowManager.Instance.OpenWindow(WinNames.EnglishPanel);

        switch (m_sceneType)
        {
            case EEnglishSceneType.Lawn:
                CreateScene(english_ui_scene_lawn);
                break;
            case EEnglishSceneType.Paddyfield:
                CreateScene(english_ui_scene_paddyfield);
                break;
            case EEnglishSceneType.Sea:
                CreateScene(english_ui_scene_sea);
                break;
            case EEnglishSceneType.Snow:
                CreateScene(english_ui_scene_snow);
                LoadEffect();
                break;
        }

        partIndex = 0;
        m_packList.Clear();
        StartGamePart();

        m_letterTexture = CreateLetterTexture();

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Four);
    }
    

    protected override void OnMouseDown()
    {
        base.OnMouseDown();

        PlayLetterEffect();
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        foreach (GameObject go in m_packList)
        {
            GameObject.Destroy(go);
        }
        m_packList.Clear();
        m_letterTexture = null;

        EnglishLediAnimationWindow.Instance.StopAnimation();

        WindowManager.Instance.CloseWindow(WinNames.DrawLetterPanel);
        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }

    private void StartGamePart()
    {
        if (partIndex > 3)
        {
            OnAllGamePartComplete();
            return;
        }
        uint partID = m_PartList[partIndex];
        partIndex++;

        if (!TableProtoLoader.EnglishLetterInfoDict.ContainsKey(partID))
        {
            Debug.LogError("找不到表格数据！ ID:" + partID);
            OnAllGamePartComplete();
            return;
        }
        info = TableProtoLoader.EnglishLetterInfoDict[partID];
        DrawLetterWindow.Instance.StartDrawLetter(info, partIndex > 2, m_sceneType, (result) =>
        {
            //this.PlayAudioEx(StudyAudioName.t_31024);
            MedalWindow.Instance.ShowXunzhang(partIndex, true, MedalWindow.EXZCountEnum.Four, () =>
            {
                StartGamePart();
            });
        });
    }

    private void OnAllGamePartComplete()
    {
        Debug.LogError("-----------------------  OnAllGamePartComplete！");

        //EnglishLediAnimationWindow.Instance.PlayLediWinAnimation(this.GetAudioNameEx(StudyAudioName.t_32009), () =>
        //{
            //this.AddTimerEx(2.0f, () => {
                OnFlowComplete(true, (int)EEnglishGameEnum.Game4);
            //});
        //});
    }

    protected void PlayLetterEffect()
    {
        if (Application.isEditor)
        {
            if (WindowManager.Instance.eventSystem.IsPointerOverGameObject())
                return;
        }

        if (Input.touchCount > 0)
        {
            if (WindowManager.Instance.eventSystem.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                return;
        }

        if (m_letterTexture == null)
            m_letterTexture = CreateLetterTexture();

        this.PlayEffectEx(StudyPath.effect_502003067, FTools.GetRaycastHitPointByMousePoint(), 5, m_letterTexture);
    }

    private Texture2D CreateLetterTexture()
    {
        if (info != null)
        {
            string spriteName = info.bigorsmall == 1 ? info.name + "(big)" : info.name.ToLower() + "(small)";
            Sprite sprite = Res.LoadSprite(StudyPath.english_bigandsmall_letter_basepath + spriteName);
            if (sprite != null)
            {
                Texture2D texture = sprite.ConvertToTexture2D();
                return FTools.CreateSquareTexture(texture);
            }
        }
        return null;
    }

    private void LoadEffect()
    {
        GameObject go = Instantiate(StudyPath.effect_502003066, true);
        go.transform.SetParent(Scene.Find("EffectPoint"));
        go.transform.ResetTransformForLocal();
    }

}

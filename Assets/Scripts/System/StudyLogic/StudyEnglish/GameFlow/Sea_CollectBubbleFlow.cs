﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 1.流程一
 2.收集鱼泡
     */
public class Sea_CollectBubbleFlow : StudyEnglishBaseFlow
{

    public Sea_CollectBubbleFlow(List<uint> partList) : base(partList)
    {
        endPartIndex = partList.Count;
    }

    private int endPartIndex = 5;

    // 场景路径
    private const string scenePath = StudyPath.english_sea_scene;
    private const string assetPath = StudyPath.english_sea_fish;

    private Transform m_scene;  // 场景
    private Sea_FishActor m_fishActor;
    private int partIndex = 0;
    private StudyEnglishBasePart m_currentPart;


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        m_scene = CreateScene(scenePath);
        m_scene.AddComponentEx<SceneAnimation_SeaMono>();

        Transform m_fishPoint = m_scene.Find("303010302");
        if (m_fishPoint != null)
        {
            m_fishActor = Instantiate(assetPath, true).AddComponent<Sea_FishActor>();
            m_fishActor.transform.position = m_fishPoint.position;
            m_fishActor.transform.rotation = m_fishPoint.rotation;
            m_fishActor.transform.localScale = m_fishPoint.localScale;
            m_fishActor.transform.SetParent(m_scene);
        }

        partIndex = 0;
        StartGamePart();

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Five);
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        if (m_currentPart != null)
        {
            m_currentPart.OnExit();
            m_currentPart = null;
        }
        m_scene.GetComponent<SceneAnimation_SeaMono>().Dispose();

        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }

    private void StartGamePart()
    {

        if (partIndex >= endPartIndex)
        {
            OnAllGamePartComplete();
            return;
        }
        uint partID = m_PartList[partIndex];
        //if (partIndex == 3)
        //{
        //    m_currentPart = new StudyLetterPart(partID);
        //    m_currentPart.OnStart((bComplete) => {

        //        m_currentPart.OnExit();
        //        StartGamePart();
        //    });
        //}
        //else
        //{
            m_currentPart = new Sea_CollectBubblePart(partID);
            m_currentPart.OnStart((bComplete) =>
            {
                m_currentPart.OnExit();
                StartGamePart();
            }, m_scene, partIndex++);
        //}

    }

    private void OnAllGamePartComplete()
    {

        AddTimer(1500, () =>
        {
            OnFlowComplete(true, (int)EEnglishGameEnum.Game2);
        });
    }
    

}

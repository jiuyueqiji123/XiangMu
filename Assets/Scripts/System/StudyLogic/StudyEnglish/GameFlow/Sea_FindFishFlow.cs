﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sea_FindFishFlow : StudyEnglishBaseFlow
{

    public Sea_FindFishFlow(List<uint> partList) : base(partList)
    {
    }

    // 场景路径
    private const string scenePath = StudyPath.english_sea_box_scene;


    private Transform m_scene;  // 场景
    private int partIndex = 0;
    private StudyEnglishBasePart m_currentPart;



    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        m_scene = CreateScene(scenePath);
        m_scene.AddComponentEx<SceneAnimation_SeaMono>();
        
        partIndex = 0;
        StartGamePart();

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Four);
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        if (m_currentPart != null)
        {
            m_currentPart.OnExit();
            m_currentPart = null;
        }
        m_scene.GetComponent<SceneAnimation_SeaMono>().Dispose();

        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }

    private void StartGamePart()
    {

        if (partIndex > 3)
        {
            OnAllGamePartComplete();
            return;
        }
        uint partID = m_PartList[partIndex];
        m_currentPart = new Sea_FindFishPart(partID);
        m_currentPart.OnStart((bComplete) =>
        {
            m_currentPart.OnExit();
            StartGamePart();
        }, m_scene, partIndex++);

    }

    private void OnAllGamePartComplete()
    {

        AddTimer(1500, () =>
        {
            OnFlowComplete(true, (int)EEnglishGameEnum.Game3);
        });


    }

}

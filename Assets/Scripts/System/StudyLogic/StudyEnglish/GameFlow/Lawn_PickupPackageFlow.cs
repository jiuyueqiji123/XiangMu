﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 1.流程一
 2.拿包裹
     */
public class Lawn_PickupPackageFlow : StudyEnglishBaseFlow
{

    public Lawn_PickupPackageFlow(List<uint> partList) : base(partList)
    {
        endPartIndex = partList.Count;
    }

    private int endPartIndex = 5;
    private const string scenePath = StudyPath.english_lawn_scene;    // 场景路径


    private Transform m_scene;  // 场景
    private int partIndex = 0;
    private StudyEnglishBasePart m_currentPart;


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        //FTimeTool.Record("Lawn_PickupPackageFlow OnFlowStart");

        m_scene = CreateScene(scenePath);

        //if (m_PartList.Count < 6)
        //{
        //    Debug.LogError("游戏关卡数量不对！");
        //    return;
        //}

        partIndex = 0;
        StartGamePart();

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Five);

        //FTimeTool.Record("Lawn_PickupPackageFlow OnFlowStart");
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        if (m_currentPart != null)
        {
            m_currentPart.OnExit();
            m_currentPart = null;
        }

        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }

    private void StartGamePart()
    {

        if (partIndex >= endPartIndex)
        {
            OnAllGamePartComplete();
            return;
        }
        uint partID = m_PartList[partIndex];
        //if (partIndex == 3)
        //{
        //    m_currentPart = new StudyLetterPart(partID);
        //    m_currentPart.OnStart((bComplete) =>
        //    {

        //        m_currentPart.OnExit();
        //        StartGamePart();
        //    });
        //}
        //else
        //{
            m_currentPart = new Lawn_PickupPackagePart(partID);
            m_currentPart.OnStart((IsComplete) =>
            {
                m_currentPart.OnExit();
                StartGamePart();
            }, m_scene, partIndex++);
        //}

    }

    private void OnAllGamePartComplete()
    {

        AddTimer(1500, () =>
        {
            OnFlowComplete(true, (int)EEnglishGameEnum.Game2);
        });
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 1.流程一
 2.接盒子游戏
     */
public class Snow_BoxFlow : StudyEnglishBaseFlow
{

    public Snow_BoxFlow(List<uint> partList) : base(partList)
    {
        endPartIndex = partList.Count;
    }

    private int endPartIndex = 5;

    // 场景路径
    private const string scenePath = StudyPath.english_snow_scene;


    private Transform m_scene;  // 场景
    private int partIndex = 0;
    private StudyEnglishBasePart m_currentPart;


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        m_scene = CreateScene(scenePath);
        LoadEffect();
        partIndex = 0;
        StartGamePart();

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Five);
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();
        if (m_currentPart != null)
        {
            m_currentPart.OnExit();
            m_currentPart = null;
        }

        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }

    private void StartGamePart()
    {

        if (partIndex >= endPartIndex)
        {
            OnAllGamePartComplete();
            return;
        }
        uint partID = m_PartList[partIndex];
        //if (partIndex == 3)
        //{
        //    m_currentPart = new StudyLetterPart(partID);
        //    m_currentPart.OnStart((bComplete) =>
        //    {
        //        m_currentPart.OnExit();
        //        StartGamePart();
        //    });
        //}
        //else
        //{
            m_currentPart = new Snow_BoxPart(partID);
            m_currentPart.OnStart((bComplete) =>
            {
                m_currentPart.OnExit();
                StartGamePart();
            }, m_scene, partIndex++);
        //}

    }

    private void OnAllGamePartComplete()
    {

        AddTimer(1500, () =>
        {
            OnFlowComplete(true, (int)EEnglishGameEnum.Game2);
        });
    }

    private void LoadEffect()
    {
        GameObject go = Instantiate(StudyPath.effect_502003066, true);
        go.transform.SetParent(Scene.Find("EffectPoint"));
        go.transform.ResetTransformForLocal();
    }


}

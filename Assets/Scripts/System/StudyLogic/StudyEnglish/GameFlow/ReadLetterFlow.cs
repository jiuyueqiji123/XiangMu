﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 1.流程四
 2.阅读学习
     */
public class ReadLetterFlow : StudyEnglishBaseFlow
{

    public ReadLetterFlow(List<uint> partList) : base(partList)
    {
    }


    protected override void OnFlowStart()
    {
        base.OnFlowStart();
        
        if (m_PartList.Count < 1)
        {
            Debug.LogError("表格数据错误！游戏四关卡数据为空！");
            return;
        }

        WindowManager.Instance.OpenWindow(WinNames.EnglishTVPanel);

        EnglishTVWindow.Instance.StartGame(m_PartList[0], (result) => {

            //Debug.LogError("Read letter complete!");
            OnFlowComplete(result, (int)EEnglishGameEnum.Game1);

        });

        EventBus.Instance.AddEventHandler(EventID.ON_ENGLISH_TV_START_PLAY, OnTVStart);
    }

    protected override void OnFlowExit()
    {
        WindowManager.Instance.CloseWindow(WinNames.EnglishTVPanel);

        EventBus.Instance.RemoveEventHandler(EventID.ON_ENGLISH_TV_START_PLAY, OnTVStart);
    }

    private void OnTVStart()
    {
        //this.StopMusicEx();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 1.流程一
 2.赶鸟
     */
public class Paddyfield_BBShootFlow : StudyEnglishBaseFlow
{

    public Paddyfield_BBShootFlow(List<uint> partList) : base(partList)
    {
        endPartIndex = partList.Count;
    }

    private int endPartIndex = 5;

    // 场景路径
    private const string scenePath = StudyPath.english_paddyfield_scene;
    // 资源路径
    protected string assetPath = StudyPath.english_paddyfield_scarecrow;

    protected Paddyfield_ScarecrowActor gameAsset1 = null;
    protected Paddyfield_ScarecrowActor gameAsset2 = null;
    protected Paddyfield_ScarecrowActor gameAsset3 = null;

    protected Transform gameAsset1_Start;
    protected Transform gameAsset2_Start;
    protected Transform gameAsset3_Start;
    protected Transform gameAsset1_End;
    protected Transform gameAsset2_End;
    protected Transform gameAsset3_End;
    protected Transform gameAsset1_Out;
    protected Transform gameAsset2_Out;
    protected Transform gameAsset3_Out;

    private int partIndex = 0;
    private StudyEnglishBasePart m_currentPart;

    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        CreateScene(scenePath);
        FindGameAssetPoints(Scene);

        SpawnGameAsset<Paddyfield_ScarecrowActor>(assetPath);
        gameAsset1.transform.position = gameAsset1_End.position;
        gameAsset2.transform.position = gameAsset2_End.position;
        gameAsset3.transform.position = gameAsset3_End.position;

        partIndex = 0;
        StartGamePart();

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Five);
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        gameAsset1 = null;
        gameAsset2 = null;
        gameAsset3 = null;
        gameAsset1_Start = null;
        gameAsset2_Start = null;
        gameAsset3_Start = null;
        gameAsset1_End = null;
        gameAsset2_End = null;
        gameAsset3_End = null;
        gameAsset1_Out = null;
        gameAsset2_Out = null;
        gameAsset3_Out = null;
        if (m_currentPart != null)
        {
            m_currentPart.OnExit();
            m_currentPart = null;
        }
        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }


    private void StartGamePart()
    {

        if (partIndex >= endPartIndex)
        {
            OnAllGamePartComplete();
            return;
        }
        uint partID = m_PartList[partIndex];
        //if (partIndex == 3)
        //{
        //    gameAsset1.transform.position += new Vector3(0, 10000, 0);
        //    gameAsset2.transform.position += new Vector3(0, 10000, 0);
        //    gameAsset3.transform.position += new Vector3(0, 10000, 0);
        //    //gameAsset2.gameObject.SetActive(false);
        //    //gameAsset3.gameObject.SetActive(false);

        //    m_currentPart = new StudyLetterPart(partID);
        //    m_currentPart.OnStart((bComplete) =>
        //    {
        //        m_currentPart.OnExit();
        //        StartGamePart();
        //    });
        //}
        //else
        //{
            gameAsset1.transform.position = gameAsset1_End.position;
            gameAsset2.transform.position = gameAsset2_End.position;
            gameAsset3.transform.position = gameAsset3_End.position;
            //gameAsset1.gameObject.SetActive(true);
            //gameAsset2.gameObject.SetActive(true);
            //gameAsset3.gameObject.SetActive(true);
            Paddyfield_BBShootPart part = new Paddyfield_BBShootPart(partID);
            m_currentPart = part;
            part.SetGameAsset(gameAsset1.gameObject, gameAsset2.gameObject, gameAsset3.gameObject);
            part.OnStart((bComplete) =>
            {
                part.OnExit();
                StartGamePart();
            }, Scene, partIndex++);
        //}

    }

    private void OnAllGamePartComplete()
    {
        AddTimer(1500, () =>
        {
            OnFlowComplete(true, (int)EEnglishGameEnum.Game2);
        });
    }

    protected void SpawnGameAsset<T>(string assetPath) where T : Paddyfield_ScarecrowActor
    {
        Resource res = ResourceManager.Instance.GetResource(assetPath, typeof(GameObject), enResourceType.ScenePrefab);
        if (res.m_content != null)
        {
            gameAsset1 = Instantiate(res.m_content as GameObject, true).AddComponent<T>();
            gameAsset2 = Instantiate(res.m_content as GameObject, true).AddComponent<T>();
            gameAsset3 = Instantiate(res.m_content as GameObject, true).AddComponent<T>();
        }
        else
        {
            Debug.LogError("Load asset error! assetPath:" + assetPath);
        }
    }

    protected void FindGameAssetPoints(Transform parent)
    {
        if (parent == null)
            return;
        gameAsset1_Start = parent.Find("GameAsset1_Start");
        gameAsset2_Start = parent.Find("GameAsset2_Start");
        gameAsset3_Start = parent.Find("GameAsset3_Start");
        gameAsset1_End = parent.Find("GameAsset1_End");
        gameAsset2_End = parent.Find("GameAsset2_End");
        gameAsset3_End = parent.Find("GameAsset3_End");
        gameAsset1_Out = parent.Find("GameAsset1_Out");
        gameAsset2_Out = parent.Find("GameAsset2_Out");
        gameAsset3_Out = parent.Find("GameAsset3_Out");
        if (gameAsset1_Start == null) Debug.LogError("gameAsset1_Start is null!");
        if (gameAsset2_Start == null) Debug.LogError("gameAsset2_Start is null!");
        if (gameAsset3_Start == null) Debug.LogError("gameAsset3_Start is null!");
        if (gameAsset1_End == null) Debug.LogError("gameAsset1_End is null!");
        if (gameAsset2_End == null) Debug.LogError("gameAsset2_End is null!");
        if (gameAsset3_End == null) Debug.LogError("gameAsset3_End is null!");

        if (gameAsset1_End != null) gameAsset1_End.gameObject.SetActive(false);
        if (gameAsset2_End != null) gameAsset2_End.gameObject.SetActive(false);
        if (gameAsset3_End != null) gameAsset3_End.gameObject.SetActive(false);
    }


}

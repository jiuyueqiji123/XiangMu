﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 1.流程四
 2.阅读学习
     */
public class Sea_ReadLetterFlow : StudyEnglishBaseFlow
{

    public Sea_ReadLetterFlow(List<uint> partList) : base(partList)
    {

    }


    private string scenePath = StudyPath.english_sea_scene;    // 场景路径
    private Transform m_scene;  // 场景


    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        if (m_PartList.Count < 1)
        {
            Debug.LogError("表格数据错误！游戏四关卡数据为空！");
            return;
        }

        m_scene = CreateScene(scenePath);
        m_scene.AddComponentEx<SceneAnimation_SeaMono>();

        WindowManager.Instance.OpenWindow(WinNames.ReadLetterPanel);

        ReadLetterWindow.Instance.InitializeData(m_PartList[0]);
        ReadLetterWindow.Instance.StartReadLetter(() => {

            Debug.LogError("Read letter complete!");
            OnFlowComplete();
        });

    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        m_scene.GetComponent<SceneAnimation_SeaMono>().Dispose();
        WindowManager.Instance.CloseWindow(WinNames.ReadLetterPanel);
    }

}

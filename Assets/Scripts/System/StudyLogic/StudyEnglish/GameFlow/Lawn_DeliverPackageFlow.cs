﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 1.流程二
 2.送包裹
     */
public class Lawn_DeliverPackageFlow : StudyEnglishBaseFlow
{

    public Lawn_DeliverPackageFlow(List<uint> partList) : base(partList)
    {
    }

    Lawn_DeliverPackagePart deleverPart;

    // 场景路径
    private const string scenePath = StudyPath.english_train_scene;
    private Transform m_scene;  // 场景
    private Transform m_train;  // 火车
    private Transform m_train_out;  // 火车

    private int partIndex = 0;

    private List<GameObject> m_packList = new List<GameObject>();

    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        m_scene = CreateScene(scenePath);
        m_train = m_scene.Find("train");
        m_train_out = m_scene.Find("train_out");


        if (m_PartList.Count < 4)
        {
            Debug.LogError("游戏关卡数量不对！");
            return;
        }

        partIndex = 0;
        m_packList.Clear();
        StartGamePart();

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Four);
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();

        if (deleverPart != null)
        {
            deleverPart.OnUpdate();
        }
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        if (deleverPart != null)
        {
            deleverPart.OnExit();
            deleverPart = null;
        } 
        foreach (GameObject go in m_packList)
        {
            GameObject.Destroy(go);
        }
        m_packList.Clear();

        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }

    private void StartGamePart()
    {
        if (partIndex > 3)
        {
            OnAllGamePartComplete();
            return;
        }
        uint partID = m_PartList[partIndex];
        deleverPart = new Lawn_DeliverPackagePart(partID);
        deleverPart.OnStart((bComplete, paramArray)=>{
            GameObject go = (GameObject)paramArray[0];
            //Debug.LogError(go.name);
            m_packList.Add(go);
            deleverPart.OnExit();
            StartGamePart();

        }, m_scene, partIndex++);

    }

    private void OnAllGamePartComplete()
    {

        AddTimer(500, () =>
        {
            float outtime = 1.5f;
            GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_31005);
            m_train.DOMove(m_train_out.position, outtime).SetEase(Ease.InOutSine).OnComplete(()=> {
                audio.Stop();
            });

            AddTimer(outtime, () =>
            {
                OnFlowComplete(true, (int)EEnglishGameEnum.Game3);
            });
        });


    }
    


}

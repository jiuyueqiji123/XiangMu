﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class EnglishBaseController : IEasyAudio, IEasyLoading, IEasyTimer
{

    public bool bTimerCallback { get { return isGamePlaying; } }


    protected StudyGameMachine gameMachine = new StudyGameMachine();

    protected int timeID;
    protected int letterIndex;

    protected bool isGamePlaying;

    protected System.Action<bool, bool> onComplete;


    public void StartGame(int letterIdx, EnglishLevelInfo info, EEnglishGameEnum type, System.Action<bool, bool> callback = null)
    {
        isGamePlaying = true;
        onComplete = callback;
        letterIndex = letterIdx;

        WindowManager.Instance.OpenWindow(WinNames.EnglishPanel);
        EventBus.Instance.AddEventHandler(EventID.ON_ENGLISH_WINDOW_BTN_RETURN_CLICK, OnControllerExit);


        timeID = InputTimerManager.Instance.AddTimerNoInput(StudyPath.ENGLISH_ONINPUT_TIME, true, (seq) =>
        {
            //Debug.LogError("无操作事件！");
            this.PlayAudioEx(StudyAudioName.t_32001);
        });

        OnStart(info, type);
    }

    public virtual void OnUpdate()
    {
        if (gameMachine != null)
            gameMachine.UpdateGamePart();
    }

    public void ExitGame()
    {
        isGamePlaying = false;
        InputTimerManager.Instance.RemoveTimerNoInput(timeID);
        gameMachine.StopAllGamePart();
        WindowManager.Instance.CloseWindow(WinNames.EnglishPanel);
        EventBus.Instance.RemoveEventHandler(EventID.ON_ENGLISH_WINDOW_BTN_RETURN_CLICK, OnControllerExit);
    }

    protected void OnControllerExit()
    {
        //this.StopAllAudioEx();
        OnGameAllPartCallback(false);
    }


    protected void OnGamePartCallback(bool isComplete, params object[] paramArray)
    {
        if (isComplete && paramArray.Length > 0)
        {
            SaveGameProgress(letterIndex, (EEnglishGameEnum)(paramArray[0]));
        }

        PlayLediWinAnimationForEnglish(isComplete, () =>
        {
            this.AddTimerEx(2.0f, () => {

                this.StartLoadingAndEndEx(() =>
                {
                    if (isComplete)
                        EnglishLediAnimationWindow.Instance.StopAnimation();
                    gameMachine.StopCurrentGamePart();
                    if (isComplete)
                    {
                        gameMachine.StartNextPart();
                    }
                    else
                    {
                        if (onComplete != null)
                        {
                            onComplete(false, false);
                        }
                    }
                });
            });
        });
    }

    protected void OnGameAllPartCallback(bool isComplete)
    {
        InputTimerManager.Instance.RemoveTimerNoInput(timeID);

        if (isComplete)
        {
            SaveGameProgress(letterIndex, EEnglishGameEnum.Game4);
        }

        PlayLediWinAnimationForEnglish(isComplete, () => {

            if (isComplete)
            {
                EnglishLediAnimationWindow.Instance.HideMask();
                EnglishWindow.Instance.AddNextEvent(() =>
                {
                    if (EasyLoadingEx.IsLoading)
                        return;
                    this.StartLoadingAndEndEx(() =>
                    {
                        ExitGame();
                        EnglishWindow.Instance.HideMaskAndButton();
                        if (onComplete != null)
                        {
                            onComplete(isComplete, true);
                        }
                    });
                });

            }
            else
            {
                if (EasyLoadingEx.IsLoading)
                    return;
                this.StartLoadingAndEndEx(() =>
                {
                    ExitGame();
                    EnglishWindow.Instance.HideMaskAndButton();
                    if (onComplete != null)
                    {
                        onComplete(isComplete, false);
                    }
                });
            }

        });
    }

    public void PlayLediWinAnimationForEnglish(bool isComplete, System.Action callback)
    {
        if (isComplete)
        {
            EnglishLediAnimationWindow.Instance.PlayLediWinAnimation(this.GetAudioNameEx(StudyAudioName.t_32009), this.GetAudioNameEx(StudyAudioName.t_32005), false, false, () =>
            {
                if (callback != null)
                    callback();
            });
        }
        else
        {
            if (callback != null)
                callback();
        }
    }

    protected void SaveGameProgress(int letterIndex, EEnglishGameEnum gameProgress)
    {
        StudyEnglishProgressInfo info = LocalDataManager.Instance.Load<StudyEnglishProgressInfo>(ELocalDataType.StudyEnglishGame);

        if (info.letterProgress > letterIndex)
            return;

        Debug.Log("letterIndex: " + letterIndex + "  gameProgress:" + gameProgress);

        if (info.letterProgress < letterIndex)
        {
            info.letterProgress = letterIndex;
            info.letterGameProgress = (int)gameProgress;
            if (gameProgress == EEnglishGameEnum.Game4)
            {
                info.letterProgress = letterIndex + 1;
                info.letterGameProgress = 0;
            }

            LocalDataManager.Instance.Save(ELocalDataType.StudyEnglishGame, info);
        }
        else if (info.letterProgress == letterIndex && info.letterGameProgress < (int)gameProgress)
        {
            info.letterGameProgress = (int)gameProgress;
            if (gameProgress == EEnglishGameEnum.Game4)
            {
                info.letterProgress = letterIndex + 1;
                info.letterGameProgress = 0;
            }
            LocalDataManager.Instance.Save(ELocalDataType.StudyEnglishGame, info);
        }
    }


    protected virtual void OnStart(EnglishLevelInfo info, EEnglishGameEnum type) { }

}

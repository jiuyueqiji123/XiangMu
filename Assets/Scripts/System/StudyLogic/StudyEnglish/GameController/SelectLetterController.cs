﻿using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;


public enum EEnglishSceneType
{
    Lawn = 0,
    Sea = 1,
    Paddyfield = 2,
    Snow = 3,
}
public class SelectLetterController : IEasyAudio, IEasyTimer
{
    public bool bTimerCallback
    {
        get { return bLoadScene; }
    }

    private ScrollRect3DComponent scrollComp = new ScrollRect3DComponent();

    private const string scenePath = StudyPath.english_select_letter_scene;    // 场景路径

    private Transform m_scene;  // 场景

    //public SelectLetterPanel panel1 = new SelectLetterPanel();
    //public SelectLetterPanel panel2 = new SelectLetterPanel();
    //public SelectLetterPanel panel3 = new SelectLetterPanel();

    private bool bLockInput;
    private bool bLoadScene;


    private System.Action<uint> completeCallback;

    public void OnUpdate(float deltaTime)
    {
        if (bLockInput) return;
        if (bLoadScene)
        {
            scrollComp.OnUpdate(deltaTime);
        }
    }

    public void LoadScene(System.Action<uint> callback)
    {
        bLockInput = false;
        completeCallback = callback;
        this.PlayMusicEx(StudyAudioName.t_30001);
        if (bLoadScene)
            return;

        bLoadScene = true;
        m_scene = CreateScene(scenePath);
        m_scene.AddComponentEx<SceneAnimation_SelectLetterMono>();

        SelectLetterPanel panel1 = m_scene.Find("letter_left").AddComponentEx<SelectLetterPanel>();
        SelectLetterPanel panel2 = m_scene.Find("letter_center").AddComponentEx<SelectLetterPanel>();
        SelectLetterPanel panel3 = m_scene.Find("letter_right").AddComponentEx<SelectLetterPanel>();

        panel1.Initialize(6);
        panel2.Initialize(0);
        panel3.Initialize(1);

        scrollComp.Initialize(panel1, panel2, panel3, panel2.parentRoot.position, panel3.parentRoot.position, 7, true, OnPageChange);

        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
        EventBus.Instance.AddEventHandler(EventID.ON_MOUSE_BUTTON_UP, OnMouseUp);

        //WindowManager.Instance.OpenWindow(WinNames.EnglishSelectLetterPanel);
    }

    public void DestroyScene()
    {
        bLoadScene = false;
        if (m_scene != null)
        {
            m_scene.GetComponent<SceneAnimation_SelectLetterMono>().Dispose();
            GameObject.Destroy(m_scene.gameObject);
        }
        scrollComp.Dispose();

        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_DOWN, OnMouseDown);
        EventBus.Instance.RemoveEventHandler(EventID.ON_MOUSE_BUTTON_UP, OnMouseUp);

        //this.StopMusicEx();
        //WindowManager.Instance.CloseWindow(WinNames.EnglishSelectLetterPanel);
    }

    private void OnMouseDown()
    {
        if (bLockInput) return;
        scrollComp.OnDown();
    }

    private void OnMouseUp()
    {
        float dragRate = scrollComp.OnUp();
       
        if (Mathf.Abs(dragRate) < 0.05f)
        {
            Transform target = FTools.GetRaycastHitTargetByMousePoint();
            if (target != null)
            {
                SelectLetterActor mono = target.GetComponent<SelectLetterActor>();
                if (mono != null)
                {
                    if (bLockInput) return;
                    bLockInput = true;

                    mono.CrossFade(SelectLetterActor.AnimatorNameEnum.Shake, 0.01f);
                    this.AddTimerEx(1f, () =>
                    {
                        //mono.CrossFade(SelectLetterActor.AnimatorNameEnum.Stand, 0.01f);
                        if (bLoadScene)
                        {
                            if (completeCallback != null)
                            {
                                completeCallback(mono.dataID);
                            }
                            scrollComp.Reset();
                            //DestroyScene();
                        }
                    });
                    this.AddTimerEx(2f, () =>
                    {
                        mono.CrossFade(SelectLetterActor.AnimatorNameEnum.Stand, 0.01f);
                    });
                }
            }
        }
    }

    public void UpdateLockState(bool value)
    {
        bLockInput = value;
    }

    private Transform CreateScene(string scenePath)
    {
        Transform scene = Res.LoadObj(scenePath).transform;

        Camera targetCamera = scene.GetComponentInChildren<Camera>();
        if (targetCamera != null)
        {
            Camera.main.CopyFrom(targetCamera);
            targetCamera.gameObject.SetActive(false);
        }
        return scene;
    }

    private void OnPageChange(int page)
    {
        //EnglishSelectLetterWindow.Instance.UpdatePage(page + 1, 7);
    }



}

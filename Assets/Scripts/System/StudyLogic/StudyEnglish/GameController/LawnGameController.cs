﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

/** 
 * Description: 草地游戏流程控制类
 */
public class LawnGameController : EnglishBaseController
{



    protected override void OnStart(EnglishLevelInfo info, EEnglishGameEnum type)
    {

        gameMachine.RegisterGamePart(new ReadLetterFlow(info.game_flow_4));
        gameMachine.RegisterGamePart(new Lawn_PickupPackageFlow(info.game_flow_1));     // 接包裹
        gameMachine.RegisterGamePart(new Lawn_DeliverPackageFlow(info.game_flow_2));    // 送包裹
        gameMachine.RegisterGamePart(new DrawLetterFlow(info.game_flow_3, EEnglishSceneType.Lawn));   // 画字母

        gameMachine.StartGamePart((int)type - 1, OnGameAllPartCallback, OnGamePartCallback);

        if (type == EEnglishGameEnum.Game1)
            this.PlayMusicEx(StudyAudioName.t_30003);
        else
            this.PlayMusicEx(StudyAudioName.t_30004);

    }
    


}

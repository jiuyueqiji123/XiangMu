﻿using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;


public class SelectLetterController2D : IEasyAudio//, IEasyTimer
{
    //public bool bTimerCallback
    //{
    //    get { return bLoadScene; }
    //}

    private bool bLockInput;
    private bool bLoadScene;


    //private System.Action<int> completeCallback;

    public void OnUpdate(float deltaTime)
    {
    }

    public void LoadScene(System.Action<int> callback)
    {
        bLockInput = false;
        //completeCallback = callback;
        //this.PlayMusicEx(StudyAudioName.t_30001);
        if (bLoadScene)
            return;

        bLoadScene = true;

        WindowManager.Instance.OpenWindow(WinNames.EnglishSelectLetterPanel);
        EnglishSelectLetterWindow.Instance.AddSelectLetterCallback(callback);
    }

    public void DestroyScene()
    {
        bLoadScene = false;

        //this.StopMusicEx();
        WindowManager.Instance.CloseWindow(WinNames.EnglishSelectLetterPanel);
    }

    //private void OnSelectLetterCallback(uint id)
    //{
    //    if (completeCallback != null)
    //    {
    //        completeCallback(id);
    //    }
    //}
    


}

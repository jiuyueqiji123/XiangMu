﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

/** 
 * Description: 海底游戏流程控制类
 */
public class SeaGameController : EnglishBaseController
{

    protected override void OnStart(EnglishLevelInfo info, EEnglishGameEnum type)
    {

        gameMachine.RegisterGamePart(new ReadLetterFlow(info.game_flow_4));
        gameMachine.RegisterGamePart(new Sea_CollectBubbleFlow(info.game_flow_1));
        gameMachine.RegisterGamePart(new Sea_FindFishFlow(info.game_flow_2));
        gameMachine.RegisterGamePart(new DrawLetterFlow(info.game_flow_3, EEnglishSceneType.Sea));

        gameMachine.StartGamePart((int)type - 1, OnGameAllPartCallback, OnGamePartCallback);

        if (type == EEnglishGameEnum.Game1)
            this.PlayMusicEx(StudyAudioName.t_30003);
        else
            this.PlayMusicEx(StudyAudioName.t_30005);

    }

}

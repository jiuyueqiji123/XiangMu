﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

/** 
 * Description: 雪地游戏流程控制类
 */
public class SnowGameController : EnglishBaseController
{


    protected override void OnStart(EnglishLevelInfo info, EEnglishGameEnum type)
    {

        gameMachine.RegisterGamePart(new ReadLetterFlow(info.game_flow_4));
        gameMachine.RegisterGamePart(new Snow_BoxFlow(info.game_flow_1));
        gameMachine.RegisterGamePart(new Snow_ThrowSnowballFlow(info.game_flow_2));
        gameMachine.RegisterGamePart(new DrawLetterFlow(info.game_flow_3, EEnglishSceneType.Snow));

        gameMachine.StartGamePart((int)type - 1, OnGameAllPartCallback, OnGamePartCallback);

        if (type == EEnglishGameEnum.Game1)
            this.PlayMusicEx(StudyAudioName.t_30003);
        else
            this.PlayMusicEx(StudyAudioName.t_30007);
    }
    

}

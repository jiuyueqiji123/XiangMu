﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectLetterPanel : PanelInfo
{

    private const string assetPath = StudyPath.english_select_letter;    // 字母路径

    public Transform parentRoot;
    
    private Transform[] m_letterPointArray;

    private List<SelectLetterActor> m_letterList = new List<SelectLetterActor>();

    public void Initialize(int panelIndex)
    {
        _index = panelIndex;
        parentRoot = transform;
        position = parentRoot.position;

        m_letterPointArray = new Transform[4];

        m_letterPointArray[0] = parentRoot.Find("303010503_1");
        m_letterPointArray[1] = parentRoot.Find("303010503_2");
        m_letterPointArray[2] = parentRoot.Find("303010503_3");
        m_letterPointArray[3] = parentRoot.Find("303010503_4");

        CreateLetter();
        RefreshLetter();
    }

    public void CreateLetter()
    {
        m_letterList.Clear();

        for (int i = 0; i < 4; i++)
        {
            SelectLetterActor letter = Res.LoadObj(assetPath).AddComponent<SelectLetterActor>();
            letter.gameObject.SetActive(false);
            letter.transform.SetParent(parentRoot);
            m_letterList.Add(letter);
        }
    }

    public override void UpdatePosition(Vector3 newPosition)
    {
        base.UpdatePosition(newPosition);

        parentRoot.position = newPosition;
    }

    public override void MoveLeft()
    {
        base.MoveLeft();

        if (type == EPanelPointEnum.Right)
        {
            _index = _index + 3 > 6 ? (_index + 3) % 6 - 1 : _index + 3;
            RefreshLetter();
        }
    }

    public override void MoveRight()
    {
        base.MoveRight();

        if (type == EPanelPointEnum.Left)
        {
            _index = _index - 3 < 0 ? (_index - 3) + 7 : _index - 3;
            RefreshLetter();
        }
    }

    public override void Dispose()
    {
        base.Dispose();

        ClearLetter();
    }

    private void RefreshLetter()
    {
        uint[] array = GetLetterArray(_index);
        foreach (SelectLetterActor go in m_letterList)
        {
            go.gameObject.SetActive(false);
        }
        for (int i = 0; i < array.Length; i++)
        {
            SelectLetterActor letter = m_letterList[i];
            letter.gameObject.SetActive(true);
            letter.Initialize(array[i], m_letterPointArray[i].position);
        }

    }

    private void ClearLetter()
    {
        foreach (SelectLetterActor go in m_letterList)
        {
            if (go != null)
            {
                GameObject.Destroy(go.gameObject);
            }
        }
        m_letterList.Clear();
    }

    private uint[] GetLetterArray(int _index)
    {
        List<uint> list = null;
        switch (_index)
        {
            case 0:
                list = new List<uint>() { 10001, 10002, 10003, 10004 };
                break;
            case 1:
                list = new List<uint>() { 10005, 10006, 10007, 10008 };
                break;
            case 2:
                list = new List<uint>() { 10009, 10010, 10011, 10012 };
                break;
            case 3:
                list = new List<uint>() { 10013, 10014, 10015, 10016 };
                break;
            case 4:
                list = new List<uint>() { 10017, 10018, 10019, 10020 };
                break;
            case 5:
                list = new List<uint>() { 10021, 10022, 10023, 10024 };
                break;
            case 6:
                list = new List<uint>() { 10025, 10026 };
                break;
        }

        return list.ToArray();
    }


}

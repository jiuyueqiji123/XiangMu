﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEasyLoading
{ 
    //bool bTimerCallback { get; }
}

public static class EasyLoadingEx
{
    private static bool _IsLoading;
    public static bool IsLoading
    {
        get { return _IsLoading; }
    }
    
    public static void StartMainLoadingEx<T>(this T t, System.Action callback) where T : IEasyLoading
    {
        TransitionManager.Instance.StartChangeState(() => {
            if (callback != null)
            {
                callback();
            }
        });
    }

    public static void EndMainLoadingEx<T>(this T t, System.Action callback) where T : IEasyLoading
    {
        TransitionManager.Instance.EndChangeState(() =>
        {
            if (callback != null)
            {
                callback();
            }
        });
    }

    /// <summary>
    /// 主转场动画
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t"></param>
    /// <param name="callback"></param>
    /// <param name="waitTime"></param>
    public static void StartMainLoadingAndEndEx<T>(this T t, System.Action callback, float waitTime = 0.5f) where T : IEasyLoading
    {
        StartMainLoadingEx<T>(t, () => {
            if (callback != null)
            {
                callback();
            }
            TimerManager.Instance.AddTimer(waitTime, () =>
            {
                EndMainLoadingEx<T>(t, null);
            });
        });
    }

    // 小场景转场动画
    public static void StartLoadingAndEndEx<T>(this T t, System.Action startCallback = null, System.Action endCallback = null) where T : IEasyLoading
    {
        _IsLoading = true;
        TransitionManager.Instance.StartTransition(startCallback, ()=> {
            if (endCallback != null)
                endCallback();
            _IsLoading = false;
        });
    }

}
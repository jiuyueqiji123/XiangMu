﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using WCBG.ToolsForUnity.Tools;

public interface IEasyAudio
{ 
    //bool bTimerCallback { get; }
}

public static class EasyAudioEx
{

    public static readonly string audio_path_common = "sound/";
    public static readonly string audio_path_common_bg = "sound_bg/";
    public static readonly string audio_path_math = "study_math/study_math_audio/";
    public static readonly string audio_path_math_bg = "study_math/study_math_audio_bg/";
    public static readonly string audio_path_geometry = "study_geometry/study_geometry_audio/";
    public static readonly string audio_path_geometry_bg = "study_geometry/study_geometry_audio_bg/";
    public static readonly string audio_path_english = "study_english/study_english_audio/";
    public static readonly string audio_path_english_bg = "study_english/study_english_audio_bg/";
    public static readonly string audio_path_english_color = "study_english_color/study_english_color_audio/";
    public static readonly string audio_path_english_color_bg = "study_english_color/study_english_color_audio_bg/";
    public static readonly string audio_path_english_colours = "study_english_color/study_english_color_audio/";

    private static string GetAudioPath(int play_path)
    {
        switch (play_path)
        {
            case 0: // 公共
                return audio_path_common;
            case 1: // 认识数字
                return audio_path_math;
            case 2: // 图形认知
                return audio_path_geometry;
            case 3: // 字母认知
                return audio_path_english;
            case 4: // 英语颜色
                return audio_path_english_color;
            default:
                return audio_path_common;
        }
    }



    //public static void PlayMusicEx<T>(this T t, string path, bool loop = true) where T : IEasyAudio
    //{
    //    AudioManager.Instance.PlayMusic(path, loop);
    //}

    /// <summary>
    /// 播放数据表里的背景音频
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t"></param>
    /// <param name="audioID"></param>
    /// <returns></returns>
    public static void PlayMusicEx<T>(this T t, uint audioID, bool loop = true) where T : IEasyAudio
    {
        //uint headID = audioID / 10000;

        int play_path = 0;
        int play_mode = 0;
        bool play_state = false;
        string audioName = "PlayAudioEx null";

        if (TableProtoLoader.AudioInfoDict.ContainsKey(audioID))
        {
            AudioInfo audioInfo = TableProtoLoader.AudioInfoDict[audioID];
            play_path = audioInfo.play_path;
            play_mode = audioInfo.play_mode;
            audioName = audioInfo.name;
            play_state = audioInfo.play_state == 0;
        }
        //switch (headID)
        //{
        //    case 1: // 认识数字
        //    case 2: // 图形认知
        //        if (TableProtoLoader.AudioInfoDict.ContainsKey(audioID))
        //        {
        //            AudioInfo audioInfo = TableProtoLoader.AudioInfoDict[audioID];
        //            play_path = audioInfo.play_path;
        //            play_mode = audioInfo.play_mode;
        //            audioName = audioInfo.name;
        //            play_state = audioInfo.play_state == 0;
        //        }
        //        break;
        //    case 3: // 字母认知
        //        if (TableProtoLoader.AudioEnglishInfoDict.ContainsKey(audioID))
        //        {
        //            AudioEnglishInfo englishInfo = TableProtoLoader.AudioEnglishInfoDict[audioID];
        //            play_path = englishInfo.play_path;
        //            play_mode = englishInfo.play_mode;
        //            audioName = englishInfo.name;
        //            play_state = englishInfo.play_state == 0;
        //        }
        //        break;
        //}

        if (!play_state)
            return;

        switch (play_mode)
        {
            case 0:     //正常播放单个
                break;
            case 1:     //随机播放单个
                string[] _array = audioName.Split(';');
                audioName = _array[Random.Range(0, _array.Length)];
                break;
        }

        switch (play_path)
        {
            case 0: // 公共
                AudioManager.Instance.PlayMusicAsync(audio_path_common_bg + audioName, loop, false, true);
                break;
            case 1: // 认识数字
                AudioManager.Instance.PlayMusicAsync(audio_path_math_bg + audioName, loop, false, true);
                break;
            case 2: // 图形认知
                AudioManager.Instance.PlayMusicAsync(audio_path_geometry_bg + audioName, loop, false, true);
                break;
            case 3: // 字母认知
                AudioManager.Instance.PlayMusicAsync(audio_path_english_bg + audioName, loop, false, true);
                break;
            default:
                AudioManager.Instance.PlayMusicAsync(audio_path_common_bg + audioName, loop, false, true);
                break;
        }
    }

    public static void StopMusicEx<T>(this T t) where T : IEasyAudio
    {
        AudioManager.Instance.StopBackgroundMusic();
    }

    public static void StopAllAudioEx<T>(this T t) where T : IEasyAudio
    {
        AudioManager.Instance.StopAllSound();
    }

    
    /// <summary>
    /// 播放数据表里的音效数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t"></param>
    /// <param name="audioID"></param>
    /// <returns></returns>
    public static GameAudioSource PlayAudioEx<T>(this T t, uint audioID, bool loop = false) where T : IEasyAudio
    {
        //uint headID = audioID / 10000;

        int play_path = 0;
        int play_mode = 0;
        bool play_state = false;
        string audioName = "PlayAudioEx null";

        if (TableProtoLoader.AudioInfoDict.ContainsKey(audioID))
        {
            AudioInfo audioInfo = TableProtoLoader.AudioInfoDict[audioID];
            play_path = audioInfo.play_path;
            play_mode = audioInfo.play_mode;
            audioName = audioInfo.name;
            play_state = audioInfo.play_state == 0;
        }
        //switch (headID)
        //{
        //    case 1: // 认识数字
        //    case 2: // 图形认知
        //        if (TableProtoLoader.AudioInfoDict.ContainsKey(audioID))
        //        {
        //            AudioInfo audioInfo = TableProtoLoader.AudioInfoDict[audioID];
        //            play_path = audioInfo.play_path;
        //            play_mode = audioInfo.play_mode;
        //            audioName = audioInfo.name;
        //            play_state = audioInfo.play_state == 0;
        //        }
        //        break;
        //    case 3: // 字母认知
        //        if (TableProtoLoader.AudioEnglishInfoDict.ContainsKey(audioID))
        //        {
        //            AudioEnglishInfo englishInfo = TableProtoLoader.AudioEnglishInfoDict[audioID];
        //            play_path = englishInfo.play_path;
        //            play_mode = englishInfo.play_mode;
        //            audioName = englishInfo.name;
        //            play_state = englishInfo.play_state == 0;
        //        }
        //        break;
        //    default:
        //        if (audioID < 10000)
        //        {
        //            if (TableProtoLoader.AudioInfoDict.ContainsKey(audioID))
        //            {
        //                AudioInfo audioInfo = TableProtoLoader.AudioInfoDict[audioID];
        //                play_path = audioInfo.play_path;
        //                play_mode = audioInfo.play_mode;
        //                audioName = audioInfo.name;
        //                play_state = audioInfo.play_state == 0;
        //            }
        //        }

        //        break;
        //}

        if (!play_state)
        {
            Debug.LogError("关闭播放该音频：" + audioName);
            return new GameAudioSource(null);
        }

        switch (play_mode)
        {
            case 0:     //正常播放单个
                break;
            case 1:     //随机播放单个
                string[] _array = audioName.Split(';');
                audioName = _array[Random.Range(0, _array.Length)];
                break;
        }

        return t.PlayAudioEx(GetAudioPath(play_path) + audioName, loop);
    }

    public static void PlayActionAudioEx<T>(this T t, uint id, System.Action callback = null) where T : IEasyAudio
    {
        if (!TableProtoLoader.ActionAudioInfoDict.ContainsKey(id))
        {
            if (callback != null)
                callback();
            return;
        }
        ActionAudioInfo info = TableProtoLoader.ActionAudioInfoDict[id];
        if (info.play_state == 1)
        {
            if (callback != null)
                callback();
            return;
        }

        PlayerAnimationController.Instance.PlayAnimaton(t.GetLediActionInfoEx(id), callback);
    }

    public static LediActionInfo GetLediActionInfoEx<T>(this T t, uint id) where T : IEasyAudio
    {
        if (!TableProtoLoader.ActionAudioInfoDict.ContainsKey(id))
        {
            return null;
        }
        ActionAudioInfo info = TableProtoLoader.ActionAudioInfoDict[id];
        string path = GetAudioPath(info.play_path);

        string[] paramArray = info.action_param_array.Split(';');
        ActionInfo[] _actionInfoArray = new ActionInfo[paramArray.Length];
        string str = "";
        for (int i = 0; i < paramArray.Length; i++)
        {
            string[] arr = paramArray[i].Split(',');
            if (arr.Length != 2)
                continue;
            ActionInfo actionInfo = new ActionInfo
            {
                actionName = arr[0].Trim(),
                actionTime = int.Parse(arr[1])
            };
            _actionInfoArray[i] = actionInfo;
            str += actionInfo.actionName + "," + actionInfo.actionTime + ";";
        }
        Debug.Log("id:" + id + " info:" + str);

        LediActionInfo lediActionInfo = new LediActionInfo
        {
            animationType = (PlayerAnimationController.PlayerAnimationEnum)info.play_mode,
            audioPath = path + info.audio_name,
            audioDelayTime = (int)info.audio_delay_time,
            actionInfoArray = _actionInfoArray,
            action_end_name = info.action_end_name,
            action_end_time = (int)info.action_end_time,
            nextActionID = info.next_action_id
        };
        return lediActionInfo;
    }


    //public static void PlayAudioArrayEx<T>(this T t, params uint[] audioArray) where T : IEasyAudio
    //{
    //    t.PlayAudioArray(audioArray, 0, null);
    //}
    //public static void PlayAudioArrayEx<T>(this T t, System.Action callback, params uint[] audioArray) where T : IEasyAudio
    //{
    //    t.PlayAudioArray(audioArray, 0, callback);
    //}
    //private static void PlayAudioArray<T>(this T t, uint[] audioArray, int index, System.Action callback) where T : IEasyAudio
    //{
    //    if (index >= audioArray.Length)
    //    {
    //        if (callback != null)
    //            callback();
    //        return;
    //    }
    //    GameAudioSource audio = t.PlayAudioEx(audioArray[index]);
    //    TimerManager.Instance.AddTimer(audio.Length, () =>
    //    {
    //        index++;
    //        t.PlayAudioArray(audioArray, index, callback);
    //    });
    //}


    public static GameAudioSource PlayAudioEx<T>(this T t, string path, bool loop = false) where T : IEasyAudio
    {
        return AudioManager.Instance.PlaySound(path, loop, true);
    }

    public static GameAudioSource PlayCommonAudioEx<T>(this T t, string audioName, bool loop = false) where T : IEasyAudio
    {
        return t.PlayAudioEx(audio_path_common + audioName, loop);
    }
    public static GameAudioSource PlayEnglishAudioEx<T>(this T t, string audioName, bool loop = false) where T : IEasyAudio
    {
        GameAudioSource audio = t.PlayAudioEx(audio_path_english + audioName, loop);
        if (audio.Clip == null)
            return t.PlayCommonAudioEx(audioName, loop);
        return audio;
    }
    public static GameAudioSource PlayEnglishColoursAudioEx<T>(this T t, string audioName, bool loop = false) where T : IEasyAudio
    {
        GameAudioSource audio = t.PlayAudioEx(audio_path_english_colours + audioName, loop);
        if (audio.Clip == null)
            return t.PlayCommonAudioEx(audioName, loop);
        return audio;
    }


    public static string GetAudioNameEx<T>(this T t, uint audioID) where T : IEasyAudio
    {
        if (TableProtoLoader.AudioInfoDict.ContainsKey(audioID))
        {
            AudioInfo audioInfo = TableProtoLoader.AudioInfoDict[audioID];
            return audioInfo.name;
        }
        return string.Empty;
        //uint headID = audioID / 10000;

        //switch (headID)
        //{
        //    case 1: // 认识数字
        //    case 2: // 图形认知
        //        if (TableProtoLoader.AudioInfoDict.ContainsKey(audioID))
        //        {
        //            AudioInfo audioInfo = TableProtoLoader.AudioInfoDict[audioID];
        //            return audioInfo.name;
        //        }
        //        break;
        //    case 3: // 字母认知
        //        if (TableProtoLoader.AudioEnglishInfoDict.ContainsKey(audioID))
        //        {
        //            AudioEnglishInfo englishInfo = TableProtoLoader.AudioEnglishInfoDict[audioID];
        //            return englishInfo.name;
        //        }
        //        break;
        //}
        //return string.Empty;
    }



}
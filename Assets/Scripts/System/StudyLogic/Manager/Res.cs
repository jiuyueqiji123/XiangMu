﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Res
{

    public static T LoadObj<T>(string path) where T : Object
    {
        Resource res = ResourceManager.Instance.GetResource(path, typeof(T), enResourceType.Prefab);
        return res.m_content as T;
    }

    public static GameObject LoadObj(string path)
    {
        return GameObjectPool.Instance.GetGameObject(path, enResourceType.Prefab);
        //Resource res = ResourceManager.Instance.GetResource(path, typeof(GameObject), enResourceType.ScenePrefab);
        //if (res.m_content != null)
        //{
        //    return GameObject.Instantiate(res.m_content as GameObject);
        //}
        //Debug.LogError("Load obj error! path:" + path);
        //return null;
    }

    public static void LoadObjAsync(string path, System.Action<GameObject> callback)
    {
        ResourceManager.Instance.GetResourceAsync(path, typeof(GameObject), enResourceType.ScenePrefab, (res)=>{
            if (res.m_content != null)
            {
                if (callback != null)
                {
                    callback(GameObject.Instantiate(res.m_content as GameObject));
                }
            }
        });
    }

    public static void PreLoadObjAsync(string path)
    {
        GameObjectPool.Instance.PrepareGameObjectAsync(path, enResourceType.Prefab, 1);
    }

    public static Texture LoadTexture(string path)
    {
        Resource res = ResourceManager.Instance.GetResource(path, typeof(Texture), enResourceType.Prefab);
        if (res.m_content != null)
        {
            return res.m_content as Texture;
        }
        return null;
    }

    public static Texture2D LoadTexture2D(string path)
    {
        Resource res = ResourceManager.Instance.GetResource(path, typeof(Texture2D), enResourceType.Prefab);
        if (res.m_content != null)
        {
            return res.m_content as Texture2D;
        }
        return null;
    }

    public static Sprite LoadSprite(string path)
    {
        Resource res = ResourceManager.Instance.GetResource(path, typeof(Sprite), enResourceType.Prefab);
        if (res.m_content != null)
        {
            return res.m_content as Sprite;
        }
        return null;
    }

    public static string LoadTextAsset(string path)
    {
        Resource res = ResourceManager.Instance.GetResource(path, typeof(TextAsset), enResourceType.GameString, false, false);

        if (res != null)
        {
            BinaryObject dataAsset = res.m_content as BinaryObject;
            return System.Text.Encoding.UTF8.GetString(dataAsset.m_data);
        }
        return string.Empty;
    }

    public static GameObject LoadObjForResources(string path)
    {
        GameObject go = Resources.Load<GameObject>(path);
        if (go != null)
        {
            return GameObject.Instantiate(go);
        }
        return null;
    }

}

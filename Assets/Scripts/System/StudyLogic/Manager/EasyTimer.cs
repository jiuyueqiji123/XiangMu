﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEasyTimer
{
    bool bTimerCallback { get; }
}

public static class EasyTimerEx
{
    public static int AddTimerEx<T>(this T t, float time, System.Action callback) where T : IEasyTimer
    {
        return t.AddTimerEx(time, false, (seq) =>
        {
            if (callback != null)
                callback();
        });
    }

    public static int AddTimerEx<T>(this T t, int time, System.Action callback) where T : IEasyTimer
    {
        return t.AddTimerEx((float)time / 1000, false, (seq) =>
        {
            if (callback != null)
                callback();
        });
    }

    public static int AddTimerEx<T>(this T t, int time, bool loop, System.Action callback) where T : IEasyTimer
    {
        return t.AddTimerEx((float)time / 1000, loop, (seq) => {
            if (callback != null)
                callback();
        });
    }

    public static int AddTimerEx<T>(this T t, float time, bool loop, System.Action<int> callback) where T : IEasyTimer
    {
        return InputTimerManager.Instance.AddTimer(time, loop, (seq) =>
        {
            if (!t.bTimerCallback)
                return;
            if (callback != null)
                callback(seq);
        });
    }

    public static void RemoveTimerEx<T>(this T t, int seq) where T : IEasyTimer
    {
        InputTimerManager.Instance.RemoveTimer(seq);
    }

}
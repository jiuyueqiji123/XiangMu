﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEasyEffect
{ 
    //bool bTimerCallback { get; }
}

public static class EasyEffectEx
{
    
    public static GameObject PlayEffectEx<T>(this T t, string path, Vector3 position, float destroyTime) where T : IEasyEffect
    {
        return t.PlayEffectEx(path, position, Quaternion.identity, Vector3.one, null, destroyTime, null, EGameLayerMask.Default);
    }

    public static GameObject PlayEffectEx<T>(this T t, string path, Vector3 position, float destroyTime, EGameLayerMask layer) where T : IEasyEffect
    {
        return t.PlayEffectEx(path, position, Quaternion.identity, Vector3.one, null, destroyTime, null, layer);
    }

    public static GameObject PlayEffectEx<T>(this T t, string path, Transform parent, float destroyTime) where T : IEasyEffect
    {
        return t.PlayEffectEx(path, parent.position, parent.rotation, parent.localScale, parent, destroyTime, null, EGameLayerMask.Default);
    }

    public static GameObject PlayEffectEx<T>(this T t, string path, Transform parent, float destroyTime, EGameLayerMask layer) where T : IEasyEffect
    {
        return t.PlayEffectEx(path, parent.position, parent.rotation, parent.localScale, parent, destroyTime, null, layer);
    }

    public static GameObject PlayEffectEx<T>(this T t, string path, Vector3 position, float destroyTime, Texture2D texture) where T : IEasyEffect
    {
        return t.PlayEffectEx(path, position, Quaternion.identity, Vector3.one, null, destroyTime, texture, EGameLayerMask.Default);
    }

    public static GameObject PlayEffectEx<T>(this T t, string path, Vector3 position, float destroyTime, Texture2D texture, EGameLayerMask layer) where T : IEasyEffect
    {
        return t.PlayEffectEx(path, position, Quaternion.identity, Vector3.one, null, destroyTime, texture, layer);
    }

    public static GameObject PlayEffectEx<T>(this T t, string path, Transform parent, float destroyTime, Texture2D texture, EGameLayerMask layer) where T : IEasyEffect
    {
        return t.PlayEffectEx(path, parent.position, parent.rotation, parent.localScale, parent, destroyTime, texture, layer);
    }

    public static GameObject PlayEffectEx<T>(this T t, string path, Vector3 position, Quaternion rotation, Vector3 localScale, Transform parent, float destroyTime, Texture2D texture, EGameLayerMask layer) where T : IEasyEffect
    {
        GameObject effect = Res.LoadObj(path);
        if (effect != null)
        {
            effect.SetLayer(layer);
            if (parent != null)
                effect.transform.SetParent(parent);
            effect.transform.position = position;
            effect.transform.rotation = rotation;
            effect.transform.localScale = localScale;

            if (texture != null)
            {
                Renderer[] renderers = effect.GetComponentsInChildren<Renderer>();
                foreach (Renderer renderer in renderers)
                {
                    renderer.material.SetTexture("_MainTex", texture);
                }
            }

            ParticleSystem[] particles = effect.GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem particle in particles)
            {
                particle.Play();
            }

            GameObject.Destroy(effect, destroyTime);
        }
        return effect;
    }


}
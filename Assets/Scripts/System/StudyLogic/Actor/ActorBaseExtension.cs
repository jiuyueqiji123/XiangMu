﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorBaseExtension
{
    public ActorBaseExtension(GameObject go)
    {
        _gameObject = go;
    }

    private GameObject _gameObject;
    public GameObject gameObject
    {
        get { return _gameObject; }
    }

    private Animator _animator;
    public Animator animator
    {
        get
        {
            if (_animator == null)
            {
                _animator = gameObject.GetComponentInChildren<Animator>();
            }
            return _animator;
        }
    }

    public virtual void OnUpdate() { }


}

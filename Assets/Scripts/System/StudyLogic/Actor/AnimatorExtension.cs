﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AnimatorExtension : ActorBaseExtension
{
    
    public AnimatorExtension(GameObject go) : base(go)
    {

    }


    public void CrossFade(string name, float val)
    {
        if (animator != null)
        {
            animator.CrossFade(name, val);
        }
    }

    public void PauseAnimation()
    {
        if (animator != null)
        {
            animator.speed = 0;
        }
    }

    public void ResumeAnimation()
    {
        if (animator != null)
        {
            animator.speed = 1;
        }
    }


}

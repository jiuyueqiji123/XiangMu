﻿using UnityEngine;
 

public enum MainItemEnum  
{
    Himalayas = 1001,
    Maldives = 1002,
    Danmark = 1003,
    Fruits = 2001,
    Cakes = 2002,
    EmailJigsaw = 2003,
    StudyGeometry = 3001,
    StudyMath = 3002,
    Addition = 3003,
    StudyEnglish = 3004,
    StudyPhonics = 3005,
    Energy = 4001,
    Paint = 5001,
}
﻿
//英语 字母认知 游戏类型
public enum EEnglishGameEnum
{
    None = 0,
    Game1 = 1,
    Game2 = 2,
    Game3 = 3,
    Game4 = 4,


}

// 英语颜色游戏类型
public enum EEnglishColorGameType
{
    None = 0,
    FillColor = 1,  // 跟我学
    FillLetter = 2,  // 趣味拼
    FindHouse = 3,  // 快乐练
}
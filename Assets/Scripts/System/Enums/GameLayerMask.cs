﻿

public enum EGameLayerMask
{

    Default = 0,
    TransparentFX = 1,
    IgnoreRaycast = 2,
    Water = 4,
    UI = 5,
    PlayerAnimation = 8,
    Player = 9,
    Floor = 10,
    NoSelfShadow = 11,
    ShadowProjecter = 12,
}

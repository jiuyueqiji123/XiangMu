﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGameManager : Singleton<BaseGameManager> {

    public void GotoMainState()
    {
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }
}
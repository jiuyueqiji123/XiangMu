﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FruitAnimatorController : MonoBehaviour
{
    private Animator Ani;
    private NavMeshAgent Agent;

    private bool m_Moveing = false;
    private int m_Delay = 0;
    public static FruitAnimatorController m_Instance;

    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {

        if (Ani == null || Agent == null)
        {
            Ani = GetComponent<Animator>();
            Agent = GetComponent<NavMeshAgent>();
        }
    }

    void Update()
    {
        if (m_Moveing)
        {
            if (m_Delay > 0)
            {
                m_Delay--;
                return;
            }

            //Debug.LogError("Agent.remainingDistance:" + Agent.remainingDistance);
            if (Agent.remainingDistance <= 3)
            {
                Ani.Play("Stand");
                //Ani.SetTrigger("StopWalk");
                m_Moveing = false;
            }
        }
    }

    public void StartMove(Vector3 pos)
    {
        Agent.SetDestination(pos);
        Ani.Play("Walk");
        m_Moveing = true;
        m_Delay = 3;
    }

    //public void ButtonClick()
    //{
    //    FreshFruitSceneController.Instance.GotoMainState();
    //    //BaseGameManager.Instance.GotoMainState();
    //}
}

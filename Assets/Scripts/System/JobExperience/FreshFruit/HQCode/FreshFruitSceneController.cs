﻿using UnityEngine;
using System;
using DG.Tweening;

public class FreshFruitSceneController : Singleton<FreshFruitSceneController>
{
    #region 变量
    private FruitView m_View;

    private FruitSceneConfig sceneConfig;
    public void SetSceneConfig(FruitSceneConfig fsc)
    {
        sceneConfig = fsc;
    }

    private FreshFruitGameType m_GameType;
    public FreshFruitGameType gameType
    {
        get{return m_GameType;}
    }

    public int EventTimeStamp { get { return m_EventTimeStamp; } }
    
    public Camera currentCamera
    {
        get{return Camera.main;}
    }
    #endregion
    
    #region 初始化相关
    public override void Init()
    {
        base.Init();
    }

    public void InitUI()
    {
        //视图层初始化
        if (m_View == null)
            m_View = new FruitView();

        Register();
        ResourceOk(FruitGameResType.UI);
    }

    void Register()
    {
        Input.multiTouchEnabled = false;
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Fruit_return, ReturnBtnClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Fruit_Qipao_Click, QipaoClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Fruit_Btn_Ok, BtnOkClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Fruit_GameOver, GameOver);
    }

    void RemoveLisener()
    {
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Fruit_return, ReturnBtnClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Fruit_Qipao_Click, QipaoClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Fruit_Btn_Ok, BtnOkClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Fruit_GameOver, GameOver);
    }
    #endregion
    
    #region 交互
    private void Restart()
    {
        sceneConfig.ReStart();
    }

    public void ReturnBtnClick(HUIEvent huiEvent)
    {
        TimerManager.Instance.RemoveAllTimer();
        AudioManager.Instance.StopAllSound();
        Debug.Log(m_GameType);
        if ((m_GameType & FreshFruitGameType.Main) != FreshFruitGameType.Main)
        {
            int index = 0;
            if ((m_GameType & FreshFruitGameType.Zhazhi) == FreshFruitGameType.Zhazhi)
                index = 0;
            else if ((m_GameType & FreshFruitGameType.Shala) == FreshFruitGameType.Shala)
                index = 1;
            else if ((m_GameType & FreshFruitGameType.BingJILing) == FreshFruitGameType.BingJILing)
                index = 2;
            HUIEventManager.Instance.DispatchUIEvent(enUIEventID.Fruit_GameOver, new stUIEventParams { argInt = index });
            Restart();
        }
        else
        {
            Go2MainState();
        }
    }
    
    public void JIQIClick(int index)
    {
        HUIEvent hUIEvent = new HUIEvent();
        hUIEvent.m_eventParams = new stUIEventParams();
        hUIEvent.m_eventParams.argInt = index;
        hUIEvent.m_eventParams.argInt = 1;
        QipaoClick(hUIEvent);
    }

    private int clickIndex;
    public void QipaoClick(HUIEvent hUIEvent)
    {
        sceneConfig._timerstampUpdate();

        if (!canInteractive) return;//交互禁止
        SetInteractive(false);

        clickIndex = hUIEvent.m_eventParams.argInt;
        if (hUIEvent.m_eventParams.argInt2 == 1)
        {
            sceneConfig.AudioPlay("604020101");
            sceneConfig.ShowQiPao();
        }

        float delayTime = 1f;
        if (m_View != null)
            m_View.QipaoHide(delayTime);

        TimerManager.Instance.AddTimer(delayTime, ToGame);
    }

    //进入游戏
    private void ToGame()
    {
        if (clickIndex == 0)
            m_GameType = (FreshFruitGameType.XuanShuiGuo | FreshFruitGameType.Zhazhi);
        else if (clickIndex == 1)
            m_GameType = (FreshFruitGameType.XuanShuiGuo | FreshFruitGameType.Shala);
        else if (clickIndex == 2)
            m_GameType = (FreshFruitGameType.XuanShuiGuo | FreshFruitGameType.BingJILing);

        SDKManager.Instance.Event((int)UmengEvent.FruitJuice + clickIndex);
        SDKManager.Instance.StartLevel((int)UmengLevel.FruitJuice + clickIndex);
        
        sceneConfig.Show(m_GameType);
    }

    public void ShowOkBtn(int stamp)
    {
        if (m_View != null) m_View.OkBtnShowUp();
        m_EventTimeStamp = stamp;
    }

    private int m_EventTimeStamp;
    public void BtnOkClick(HUIEvent hUIEvent)
    {
        sceneConfig.OKBtnClick(m_EventTimeStamp);
    }

    public void ShowSliceTip(bool isshow = true)
    {
        m_View.ShowSliceTip(isshow);
    }

    public void ShowRotateTip(bool isshow = true)
    {
        m_View.ShowRotateTip(isshow);
    }



    private bool canInteractive = true;
    public void SetInteractive(bool canyou)
    {
        canInteractive = canyou;
    }

    public void AudioPlay(string audioIDStr,bool b=true)
    {
        if (sceneConfig != null) sceneConfig.AudioPlay(audioIDStr,b);
    }
    #endregion

    #region 游戏状态控制
    private void Set2Main()
    {
        m_GameType = FreshFruitGameType.Main;
    }

    private void SceneShow()
    {
        sceneConfig.Show(m_GameType);
    }

    private void UIShow()
    {
        SetInteractive(false);
        m_View.OpenForm();
        TimerManager.Instance.AddTimer(3.0f, () => 
        {
            m_View.QipaoShow();
            sceneConfig.ThreeChooseOne();
        });
    }

    bool sceneok = false;
    bool uiok = false;
    //资源加载进度
    public void ResourceOk(FruitGameResType resourceType)
    {
        switch (resourceType)
        {
            case FruitGameResType.Scene:
                InitUI();
                sceneok = true;
                break;
            case FruitGameResType.UI:
                uiok = true;
                break;
        }
        if (sceneok && uiok)
            GameStart();
    }
    
    //前置条件:清理工作完成
    //游戏开始
    public void GameStart()
    {
        Set2Main();
        SceneShow();
        UIShow();
    }

    //游戏结束统计消息发送
    public void GameOver(HUIEvent hUIEvent)
    {
        //游戏结束[正常结束&异常结束]
        SDKManager.Instance.FinishLevel((int)UmengLevel.FruitJuice + hUIEvent.m_eventParams.argInt);
    }

    //离开本游戏
    public void Go2MainState()
    {
        RemoveLisener();
        Input.multiTouchEnabled = true;
        sceneConfig = null;
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }

    
    #endregion
}
#region enums

public enum FruitGameResType
{
    UI,
    Scene,
}

[Flags]
public enum FreshFruitGameType
{
    Main = 1,
    XuanShuiGuo = 2,
    BingJILing = 4,
    Zhazhi = 8,
    Shala = 16,
    Chi = 32,
}

#endregion
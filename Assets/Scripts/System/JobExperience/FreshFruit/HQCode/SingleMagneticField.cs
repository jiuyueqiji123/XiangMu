﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//磁单极【伪】
public class SingleMagneticField : MonoBehaviour
{
    //北极
    public GameObject magneticPoint;

    //磁常量
    private static float mu0 = 10.0f;

    //磁场强度
    public float strength;

    //磁场范围
    private Rigidbody rid;

    private void Awake()
    {
        rid = GetComponent<Rigidbody>();

    }
    private void OnTriggerStay(Collider other)
    {
        Rigidbody otherRigidBody = other.gameObject.GetComponent<Rigidbody>();
        if (other.gameObject.tag == "Paramagnetic")
        {
            //向量p->p
            Vector3 m_n = other.gameObject.transform.position - magneticPoint.transform.position;

            //磁常量
            float mu = Time.fixedDeltaTime * strength * mu0 * otherRigidBody.mass;

            //磁力
            Vector3 f_n = (m_n.normalized) * mu / (Mathf.Pow(Mathf.Max(m_n.magnitude, 0.2f), 3));

            //添加力
            otherRigidBody.AddForce(-1f * f_n);

            rid.AddForceAtPosition(1f * f_n, magneticPoint.transform.position);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


/// <summary>
/// 切片水果拖拽
/// </summary>
public class FragFruitDrag : MonoBehaviour, IInputDragHandler, IInputBeginDragHandler, IInputEndDragHandler
{
    public Rigidbody m_rigidBody
    {
        get
        {
            return GetComponent<Rigidbody>();
        }
    }

    Camera _camera
    {
        get
        {
            return FreshFruitSceneController.Instance.currentCamera;
        }
    }


    FruitShala manager;

    public FragFruitDrag SetManager(FruitShala fsc)
    {
        manager = fsc;
        return this;
    }

    public FragFruitDrag SetDistance(float d)
    {
        distanceRate = d;
        return this;
    }

    private bool LockEvent;
    public FragFruitDrag SetLock(bool fsc)
    {
        LockEvent = fsc;
        return this;
    }

    private float distanceRate = 0.85f;

    private Vector3 offset;
    private Vector3 screenPoint;
    private Vector3 shadowPos;
    private float mass = 0f;


    private Vector3 spawnPosition;
    private Quaternion spawnRotation;
    private Vector3 shadow_offset;
    

    void Awake()
    {
        SetDirty();
    }
    
    public void SetDirty()
    {
        spawnPosition = transform.position;
        spawnRotation = transform.rotation;
        if (m_rigidBody != null)
        {
            mass = m_rigidBody.mass;
            GameObject.DestroyImmediate(this.gameObject.GetComponent<Rigidbody>());
        }
    }

    public void ResetToSpawn()
    {
        transform.DOMove(spawnPosition, 0.4f);
        transform.DORotateQuaternion(spawnRotation, 0.4f).OnComplete(() =>
        {
            Detach();
            if(manager!=null) manager.Lock(false); 
        });
    }

    private void Tach()
    {
        for (int i = this.transform.parent.childCount - 1; i >= 0; i--)
        {
            Transform tmp = this.transform.parent.GetChild(i);
            if (tmp != this.transform)
            {
                tmp.SetParent(this.transform);
                if(tmp.GetComponent<FragFruitDrag>()!=null)
                tmp.GetComponent<FragFruitDrag>().SetLock(true);
            }
        }
    }

    private void Detach()
    {
        for (int i = this.transform.childCount - 1; i >= 0; i--)
        {
            Transform tmp = this.transform.GetChild(i);
            tmp.SetParent(this.transform.parent);
            if (tmp.GetComponent<FragFruitDrag>() != null)
                tmp.GetComponent<FragFruitDrag>().SetLock(false);
        }
    }

    public void OnInputBeginDrag(InputEventData eventData)
    {
        if (LockEvent||manager==null) return;


        Tach();


        offset = spawnPosition - _camera.transform.position;
        shadowPos = _camera.transform.position + offset * distanceRate;
        screenPoint = _camera.WorldToScreenPoint(shadowPos);

        shadow_offset = shadowPos - _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

        if (manager != null)
        {
            manager.FragFruitMouseOver(); /*manager._timerstampUpdate();*/
            manager.Lock();
        }
        else
            Debug.LogError("没有上级");
    }

    public void OnInputDrag(InputEventData eventData)
    {
        if (LockEvent || manager==null) return;
        Vector3 screenPos = _camera.WorldToScreenPoint(transform.position);
        Ray ray = _camera.ScreenPointToRay(screenPos);
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * 1000, Color.red);
        Vector3 myPosition = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z)) + shadow_offset;
        this.transform.position = myPosition;
    }

    public void OnInputEndDrag(InputEventData eventData)
    {
        if (LockEvent||manager==null) return;
        Vector3 screenPos = _camera.WorldToScreenPoint(transform.position);
        Ray ray = _camera.ScreenPointToRay(screenPos);
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * 1000, Color.red);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 1000f, 1 << LayerMask.NameToLayer("DragFruit")))
        {
            if (manager != null) { manager.FruitPiecesEnter(transform); /*manager._timerstampUpdate();*/ }

            else
                Debug.LogError("没有上级");
            Detach();
            
        }
        else
        ResetToSpawn();
    }
}

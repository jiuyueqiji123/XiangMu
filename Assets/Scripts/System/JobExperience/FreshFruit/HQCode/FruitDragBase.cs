﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitDragBase : MonoBehaviour {

    public virtual FruitDragBase SetDirty() {  return this; }

    public virtual FruitDragBase SetManager(FruitGameBase fruitSceneConfig) { return this; }

    public virtual FruitDragBase SetDistance(float f) { return this; }

    public virtual FruitDragBase SetLock(bool l) { return this; }
}

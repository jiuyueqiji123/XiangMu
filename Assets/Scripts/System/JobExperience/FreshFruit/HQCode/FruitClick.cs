﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitClick : MonoBehaviour, IInputClickHandler
{
    Action callback;
    Action<int> callback1;
    public int index;
    public float floatvalue;
    public void OnInputClick(InputEventData eventData)
    {
        //Debug.Log(this.transform.name+" Clicked.");
        if (callback != null)callback();
        if (callback1 != null)callback1(index);
    }

    public FruitClick SetCallBack(Action cb)
    {
        callback = cb;
        return this;
    }

    public FruitClick SetCallBack(Action<int> cb)
    {
        callback1 = cb;
        return this;
    }

    public FruitClick SetIndex(int t)
    {
        index = t;
        return this;
    }

    public FruitClick SetFloat(float f)
    {
        floatvalue = f;
        return this;
    }
}

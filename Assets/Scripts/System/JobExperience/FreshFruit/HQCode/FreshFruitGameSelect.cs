﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreshFruitGameSelect : MonoBehaviour, IInputClickHandler
{
    private bool LockEvent;

    private FruitSceneConfig manager;

    public FreshFruitGameSelect SetManager(FruitSceneConfig m)
    {
        manager = m;
        return this;
    }

    public FreshFruitGameSelect SetLock(bool m)
    {
        LockEvent = m;
        return this;
    }

    public virtual void OnInputClick(InputEventData eventData)
    {
        if (LockEvent) return;

        if (manager != null)
        {
            manager.GameSelectComplete(this.gameObject);
            manager._timerstampUpdate();
        }
    }
}

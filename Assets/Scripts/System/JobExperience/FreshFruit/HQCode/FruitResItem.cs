﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FruitResItem {
    public string Index;

    public string resourceID;

    public Transform avatorInScene;

    public string avatorInScenePath;

    public GameObject prefab;

    public string resourcePath;

    public enResourceType enResourceType = enResourceType.Prefab;

    public string describe;
}

[System.Serializable]
public class FruitCameraResItem
{
    public Transform avatorInscene;

    public float fieldOfView;

    public bool hasEmitter;
}
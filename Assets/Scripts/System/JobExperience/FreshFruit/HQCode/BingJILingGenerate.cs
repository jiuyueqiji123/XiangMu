﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BingJILingGenerate : MonoBehaviour
{
    private BingJILingConfig config;
    private Vector3 dropPoint;
    private Transform parent;
    private FruitBingjiling manager;
    private GameObject prefab;
    private string colorName;

    private float speed = 0.8f;//每秒多少个
    bool isStarted = false;
    float nextGenerateTime;
    int numberCount;

    public BingJILingGenerate Set(BingJILingConfig c,Vector3 point,Transform p, FruitBingjiling fsc,GameObject pf,string colorN)
    {
        config = c;
        dropPoint = point;
        parent = p;
        manager = fsc;
        prefab = pf;
        colorName = colorN;

        isStarted = false;
        return this;
    }

    public BingJILingGenerate StartGenerate()
    {
        isStarted = true;
        numberCount = 0;
        nextGenerateTime = Time.realtimeSinceStartup + 1.0f / speed;
        return this;
    }
    
    private void Update()
    {
        if (isStarted)
        {
            if (numberCount < config.num)
            {
                if (Time.realtimeSinceStartup > nextGenerateTime)
                {
                    nextGenerateTime = Time.realtimeSinceStartup + 1.0f / speed;
                    GameObject tmp;
                    if (prefab != null)
                    {
                        tmp = GameObject.Instantiate(prefab);
                        tmp.SetActive(true);
                        Transform template = parent.Find("pos2/" + (numberCount + 1));
                        tmp.transform.SetParent(parent);
                        tmp.transform.localScale = template.localScale;
                        tmp.transform.rotation = template.rotation;
                        tmp.transform.position = dropPoint;
                        //tmp.AddComponent<Rigidbody>();
                        tmp.transform.DOMove(template.position, 0.3f).SetEase(Ease.InQuad);
                        if (manager != null) manager.AddBJL(tmp);
                        numberCount++;
                    }
                }
            }
            else
            {
                isStarted = false;
                if (manager != null) manager.CreateBingJILingOver();
            }
        }
    }
}
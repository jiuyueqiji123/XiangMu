﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZhaZhiJiAnimCtr:MonoBehaviour
{
    public enum ZhaZhiJIAnim
    {
        Idle,
        Shake,
        Open,
        Close,
    }

    private Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public ZhaZhiJIAnim state = ZhaZhiJIAnim.Idle;

    public void SwitchState(ZhaZhiJIAnim targetState)
    {
        if (targetState != state)
        {
            state = targetState;
            PlayAnim(state);
        }
    }

    private void PlayAnim(ZhaZhiJIAnim animState)
    {
        string animStr = "idle";
        switch (animState)
        {
            case ZhaZhiJIAnim.Close:
                animStr = "close";
                break;
            case ZhaZhiJIAnim.Idle:
                animStr = "idle";
                break;
            case ZhaZhiJIAnim.Open:
                animStr = "open";
                break;
            case ZhaZhiJIAnim.Shake:
                animStr = "shake";
                break;
        }
        anim.CrossFade(animStr,0.2f);
    }
}

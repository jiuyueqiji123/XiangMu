﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BingJILingAnimCtr : MonoBehaviour
{
    public enum BJLAnimState
    {
        Idle,
        Shake,
        Close,
        Open
    }

    private Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public BJLAnimState state = BJLAnimState.Idle;

    public void SwitchState(BJLAnimState targetState)
    {
        if (targetState != state)
        {
            state = targetState;
            PlayAnim(state);
        }
    }

    private void PlayAnim(BJLAnimState animState)
    {
        string animStr = "idle";
        switch (animState)
        {
            case BJLAnimState.Close:
                animStr = "close";
                break;
            case BJLAnimState.Idle:
                animStr = "idle";
                break;
            case BJLAnimState.Open:
                animStr = "open";
                break;
            case BJLAnimState.Shake:
                animStr = "shake";
                break;
        }
        anim.CrossFade(animStr, 0.2f);
    }
}
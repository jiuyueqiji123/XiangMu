﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitLifeLimit : MonoBehaviour {
    
	// Update is called once per frame
	void Update () {
        if (transform.position.y < -1f)
        {
            GameObject.DestroyImmediate(gameObject);
        }
	}
}

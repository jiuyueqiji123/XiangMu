﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class FruitEatDrag : MonoBehaviour, IInputDragHandler, IInputBeginDragHandler, IInputEndDragHandler
{
    private Vector3 m_dragStartPoint;
    private Vector3 m_lastDragPoint;
    private bool m_isInJuiceDrag;

    private bool isDrag = false;
    private Vector3 spawnPosition;
    private Quaternion spawnRotation;
    private Vector3 shadow_offset;
    private float distanceRate = 1f;
    private Vector3 offset;
    private Vector3 screenPoint;
    private Vector3 shadowPos;
    private Camera _camera
    {
        get
        {
            return FreshFruitSceneController.Instance.currentCamera;
        }
    }

    private bool LockEvent;

    private FruitBingjiling manager;

    public FruitEatDrag SetLock(bool isLock)
    {
        LockEvent = isLock;
        return this;
    }

    public FruitEatDrag SetDirty()
    {
        spawnPosition = transform.position;
        spawnRotation = transform.rotation;
        return this;
    }

    public FruitEatDrag SetManager(FruitBingjiling m)
    {
        manager = m;
        return this;
    }

    public FruitEatDrag ResetToSpawn()
    {
        transform.DOMove(spawnPosition, 0.4f);
        transform.DORotateQuaternion(spawnRotation, 0.4f);
        return this;
    }

    void Start()
    {

        spawnPosition = transform.position;
        spawnRotation = transform.rotation;
    }

    public void OnInputDrag(InputEventData eventData)
    {
        if (LockEvent) return;
        Vector3 myPos = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z)) + shadow_offset;
        if (myPos.y < spawnPosition.y)
            myPos = new Vector3(myPos.x,spawnPosition.y,myPos.z);
        transform.position = myPos;

    }

    public void OnInputBeginDrag(InputEventData eventData)
    {
        if (LockEvent) return;
        offset = spawnPosition - _camera.transform.position;
        shadowPos = _camera.transform.position + offset * distanceRate;
        screenPoint = _camera.WorldToScreenPoint(shadowPos);

        shadow_offset = shadowPos - _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        if (startFunc != null) startFunc();
        //if(manager!=null) manager._timerstampUpdate();
    }

    public void OnInputEndDrag(InputEventData eventData)
    {
        if (LockEvent) return;
        //if(manager!=null) manager._timerstampUpdate();
        Vector3 screenPos = _camera.WorldToScreenPoint(transform.position);
        Ray ray = _camera.ScreenPointToRay(screenPos);
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * 1000, Color.red);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 1000f, 1 << LayerMask.NameToLayer("DragFruit")))
        {
            //HUIEventManager.Instance.DispatchUIEvent(enUIEventID.Fruit_Drink);
            if (endDragFunc != null) endDragFunc(hitInfo,gameObject);
            //if (manager != null)
            //{
            //    switch (manager.gameType)
            //    {
            //        case FreshFruitGameType.Chi | FreshFruitGameType.Zhazhi:
            //            {
            //                manager.Drink();
            //                break;
            //            }
            //        case FreshFruitGameType.Chi | FreshFruitGameType.BingJILing:
            //            {
            //                manager.EatBJL(this.gameObject);
            //                break;
            //            }
            //    }
            //}
        }
        else
            ResetToSpawn();
    }

    private Action<RaycastHit,GameObject> endDragFunc;
    private Action startFunc;
    public FruitEatDrag SetCallBack(Action startDragFunc , Action<RaycastHit,GameObject> a1)
    {
        endDragFunc = a1;
        startFunc = startDragFunc;
        return this;
    }
}

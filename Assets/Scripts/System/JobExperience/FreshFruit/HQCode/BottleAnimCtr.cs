﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleAnimCtr : MonoBehaviour
{
    public enum BottleAnimEnum
    {
        Idle,
        Fall,
        Down,
        Shake,
    }

    private Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    private BottleAnimEnum state = BottleAnimEnum.Idle;
    public void SwitchState(BottleAnimEnum targetState)
    {
        if (state != targetState)
        {
            state = targetState;
            PlayAnimByState(state);
        }
    }

    private void PlayAnimByState(BottleAnimEnum targetState)
    {
        string animStr = "idle";
        switch (targetState)
        {
            case BottleAnimEnum.Idle:
                animStr = "idle";
                break;
            case BottleAnimEnum.Down:
                animStr = "down";
                break;
            case BottleAnimEnum.Fall:
                animStr = "fall";
                break;
            case BottleAnimEnum.Shake:
                animStr = "shake";
                break;
        }
        string animString = anim.GetCurrentAnimatorClipInfo(0)[0].clip.name;
        if(animString == "idle")
        {
            anim.CrossFade(animStr,0.2f);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enFruitViewWidget
{
    BtnReturn,
    BtnZhazhi,
    BtnShala,
    BtnBingjiling,
    BtnOk,
    BtnOK_pos,
    SliceTip,
    RotateTip,
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 单例，实例化view和model，负责更新view，监听model的变化，处理service的变化。
/// 
/// </summary>
public class ShowPhotosController : Singleton<ShowPhotosController> {

    private ShowPhotosView view;

    private bool isDrag =false;


    public override void Init()
    {
        base.Init();
        this.view = new ShowPhotosView();
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.ShowPhoto_Btn_Click, this.OnBtnShowPhotosClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.ReturnMain_Btn_Click, this.RetuanMain);
    }

    public override void UnInit()
    {
        base.UnInit();
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.ShowPhoto_Btn_Click, this.OnBtnShowPhotosClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.ReturnMain_Btn_Click, this.RetuanMain);
    }

    public void CloseView()
    {
        this.view.CloseForm();
    }

    private void OnBtnShowPhotosClick(HUIEvent evt)
    {
        
        stUIEventParams param = evt.m_eventParams;
        ShowPhotosView view = new ShowPhotosView();
        //Debug.Log(param.argInt);
        ShowPhotos.Instance.ShowEffect(param.argInt);          
            EmailPuzzleController.Instance.OpenView();
            EmailPuzzle.Instance.play(param.argInt);
            CloseView();


        
    }

    public void SetDrag(bool drag) {
        isDrag = drag;
    }

    public void OpenView()
    {
        this.view.OpenForm();
    }

    public void RetuanMain(HUIEvent evt) {
        //TimerManager.Instance.RemoveAllTimer();
        //GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
        CloseView();
        EmailGame.instance.SetButton(true);
        EmailGame.instance.SetCamEvent();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 单例，实例化view和model，负责更新view，监听model的变化，处理service的变化。
/// 
/// </summary>
public class ChangePhotoController : Singleton<ChangePhotoController> {

    private ChangePhotoView view;



    public override void Init()
    {
        base.Init();
        this.view = new ChangePhotoView();
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Close_Btn_Click, this.CloseThis);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Yes_Btn_Click, this.Yes_Btn_Click);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.No_Btn_Click, this.No_Btn_Click);
        

    }

    public override void UnInit()
    {
        base.UnInit();
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Close_Btn_Click, this.CloseThis);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Yes_Btn_Click, this.Yes_Btn_Click);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.No_Btn_Click, this.No_Btn_Click);
    }

    public void CloseView()
    {
        this.view.CloseForm();
    }

    private void CloseThis(HUIEvent evt) {
        CloseView();
        EmailGame.instance.SetCamEvent();
        
    }

    private void Yes_Btn_Click(HUIEvent evt) {
        CloseView();
        EmailGame.instance.ChangePhoto();
        EmailGame.instance.SetCamEvent();
       
    }

    private void No_Btn_Click(HUIEvent evt) {
        CloseView();
        EmailGame.instance.SetCamEvent();
        
    }



    public void OpenView()
    {
        this.view.OpenForm();
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

/// <summary>
/// 单例，实例化view和model，负责更新view，监听model的变化，处理service的变化。
/// v
/// </summary>
public class MakePhotoController : Singleton<MakePhotoController> {

    private MakePhotoView view;


    public override void Init()
    {
        this.view = new MakePhotoView();

        //HUIEventManager.Instance.AddUIEventListener(enUIEventID.StampPanel_Btn_Click,this.OnStampButtonClick);
        //HUIEventManager.Instance.AddUIEventListener(enUIEventID.TextPanel_Btn_Click, this.OnTestButtonClick);
        //HUIEventManager.Instance.AddUIEventListener(enUIEventID.ItemPanel_Btn_Click, this.OnItemButtonClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.ReturnMain_Btn_Click, this.RetuanMain);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Finish_Btn_Click, this.FinishGame);
    }


    public override void UnInit()
    {
        base.UnInit();
        //HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.StampPanel_Btn_Click, this.OnStampButtonClick);
        //HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.TextPanel_Btn_Click, this.OnTestButtonClick);
        //HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.ItemPanel_Btn_Click, this.OnItemButtonClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.ReturnMain_Btn_Click, this.RetuanMain);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Finish_Btn_Click, this.FinishGame);
    }
 
    

 

    public void CloseView()
    {
        this.view.CloseForm();
    }



    public void OpenView()
    {
        this.view.OpenForm();
    }

    //public void OnStampButtonClick(HUIEvent evt)
    //{
    //    stUIEventParams param = evt.m_eventParams;
    //    int i = param.argInt;
    //    Debug.Log(view.ToString());
    //    view.OnStampButtonClick(i);
    //}

    //public void OnTestButtonClick(HUIEvent evt)
    //{
    //    stUIEventParams param = evt.m_eventParams;
    //    int i = param.argInt;
    //    view.OnTestButtonClick(i);
    //}
      

    //public void OnItemButtonClick(HUIEvent evt)
    //{
    //    stUIEventParams param = evt.m_eventParams;
    //    int i = param.argInt;
    //    //view.OnItemButtonClick(i);
    //}

    public void RetuanMain(HUIEvent evt)
    {
        EmailGame.instance.SetCamEvent();
        EmailGame.instance.SetButton(true);
        CloseView();
    }

    public void FinishGame(HUIEvent evt) {
        Debug.Log("finish");
    }



}

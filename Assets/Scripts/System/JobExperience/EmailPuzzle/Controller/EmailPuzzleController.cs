﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 单例，实例化view和model，负责更新view，监听model的变化，处理service的变化。
/// 
/// </summary>
public class EmailPuzzleController : Singleton<EmailPuzzleController> {

    private EmailPuzzleView view;



    public override void Init()
    {
        base.Init();
        this.view = new EmailPuzzleView();


        HUIEventManager.Instance.AddUIEventListener(enUIEventID.EmailPuzzle_Btn_Click,this.OnEmailPuzzleClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.ReturnMain_Btn_Click, this.RetuanMain);
    }

    public override void UnInit()
    {
        base.UnInit();
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.EmailPuzzle_Btn_Click, this.OnEmailPuzzleClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.ReturnMain_Btn_Click, this.RetuanMain);
    }

    public void CloseView()
    {
        this.view.CloseForm();
    }

    private void OnEmailPuzzleClick(HUIEvent evt)
    {
        stUIEventParams param = evt.m_eventParams;
        
            switch (param.argInt)
            {
                case 0:
                    
                    break;
            }
        
    }


    public void OpenView()
    {
        this.view.OpenForm();
    }

    public void RetuanMain(HUIEvent evt)
    {
        EmailGame.instance.SetCamEvent();
        EmailGame.instance.SetButton(true);
        CloseView();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameTime : MonoSingleton<GameTime> {

    public float time;

    private int count=0;

    public GameObject photo;

    private int sendEmailNum = 0;

    public GameObject item0;

    public GameObject item1;

    public GameObject item2;

    public GameObject image;

    public GameObject effect;

    public GameObject finish;

    public GameObject plane;

    public GameObject bg_Camera;

	// Use this for initialization
	void Start () {

		
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        PhotoFinish();

    }

    public void ResetTime()
    {
        time = 0;
    }



    public void PhotoFinish() {
        if (MakePhotoView.Count >0 &&count<1)
        {
            EventBus.Instance.BroadCastEvent<bool>(EventID.SHOW_FINISH_BTN,true);
            count++;
            //Debug.Log("明信片完成");
            //count++;

 
           
        }
    }

    public void OnFinishBtnClick() {
        photo.transform.position = new Vector3(0, 0, 72);
        EventBus.Instance.BroadCastEvent<bool>(EventID.SHOW_FINISH_BTN, false);
        item0.GetComponent<itemDrag>().enabled = false;
        item1.GetComponent<itemDrag>().enabled = false;
        item2.GetComponent<itemDrag>().enabled = false;
        image.SetActive(false);
        bg_Camera.SetActive(true);
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030203");
        Invoke("SetBg_Camera", 0.5f);
    }

    public void OnPhotoButtonClick()
    {
        if (PlayerPrefs.GetInt(PlayerPrefabInfo.sendEmailNum) != 0)
        {
            sendEmailNum = PlayerPrefs.GetInt(PlayerPrefabInfo.sendEmailNum);
        }
        EventBus.Instance.BroadCastEvent(EventID.PHOTO_GAME_END);
        sendEmailNum++;
        PlayerPrefs.SetInt(PlayerPrefabInfo.sendEmailNum, sendEmailNum);
        photo.SetActive(false);
        effect.SetActive(false);
        finish.SetActive(true);
        plane.SetActive(true);
        Invoke("Finish", 2.5f);
        AudioManager.Instance.StopAllSound();
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030310");
    }

    void SetBg_Camera() {
        photo.GetComponent<Button>().enabled = true;
        effect.SetActive(true);
        bg_Camera.SetActive(false);
        AudioManager.Instance.StopAllSound();
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030309");


    }

    void Finish() {
        
        MakePhotoController.Instance.CloseView();
        EmailGame.instance.SetButton(true);
        EmailGame.instance.SetCamEvent();
        //SceneLoader.Instance.LoadScene(SceneLoader.EmailScene);
    }
}

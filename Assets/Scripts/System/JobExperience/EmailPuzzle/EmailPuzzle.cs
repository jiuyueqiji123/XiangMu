﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class EmailPuzzle : MonoSingleton<EmailPuzzle> {

    
    public GameObject zhuanpan;
    private bool IsStop = true;
    public Image[] puzzles;
    public Transform[] parents;
    public GameObject parent;
    private int Count = 0;
    public GameObject photo;
    public int photonum;
    public GameObject puzzle_bg1;
    public GameObject puzzle_bg2;
    public GameObject puzzle_bg3;
    public GameObject puzzle1;
    public GameObject puzzle2;
    public GameObject puzzle3;
    public Image[] puzzle1s;
    public Image[] puzzle2s;
    public Image[] puzzle3s;
    public GameObject zhuanpanPuzzle;
    public GameObject time;
    public int TotalTime = 3;//总时间
    public GameObject motianlun;
    public GameObject image;
    public GameObject puzzleParent;
    public GameObject effect;
    public GameObject bg_Camera;
    public float gametime;
    private bool isinGame = false;
    public GameObject bg;
    public GameObject returnMainBtn;
    private string spritePath = "game_puzzle/game_email_sprites/";
    private bool isplay =false;
    private bool isSoundPlay = false;




    private void Start()
    {
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030203");
        StartCoroutine(startTime());
 

    }

    public void ClearTime() {
        gametime = 0;
    }
    

    private void Update()
    {
        gametime += Time.deltaTime;
        if (gametime >= 15&&!isplay)
        {
            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030204");
            gametime = 0;
            isplay = true;
            
        }
    }

    // Update is called once per frame

    private void FixedUpdate()
    { 
        if (IsStop == false)
        {
            Rotate();
        }
    }

    void Rotate() {
        if (!isSoundPlay)
        {
            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030205", true);
            isSoundPlay = true;
        }
        zhuanpan.transform.Rotate(new Vector3(0, 0, -1));
    }

 

    public void SetParent(GameObject obj)
    {
        obj.transform.SetParent(parent.transform);
    }

    public void SetZhuanPanParent(GameObject obj)
    {
        for (int i = 0; i < puzzles.Length; i++)
        {
            if (obj.name == puzzles[i].name)
            {
                puzzles[i].transform.SetParent(parents[i]);
                puzzles[i].rectTransform.localPosition = Vector3.zero;
                puzzles[i].rectTransform.localRotation = Quaternion.Euler(Vector3.zero);
                puzzles[i].rectTransform.localScale = new Vector3(1, 1, 1);
                
            }
        }
        
    }

    public void AddCount()
    {
        Count++;
        
    }

    public void play(int num)
    {
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.Email);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.EmailPicture);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.EmailPicture1);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.EmailPuzzle1);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.EmailPuzzle2);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.EmailPuzzle3);
        bg.GetComponent<Image>().sprite = ResourceManager.Instance.GetResource(Itempathinfo.PuzzleGameTexturePath + "qw_bg1", typeof(Sprite), enResourceType.Prefab, false, false).m_content as Sprite;
        motianlun.GetComponent<Image>().sprite = Res.LoadSprite(spritePath + "qw_pt2");
        zhuanpan.GetComponent<Image>().sprite = Res.LoadSprite(spritePath + "qw_pt3");
        image.GetComponent<Image>().sprite = Res.LoadSprite(spritePath + "qw_pt4");


        photonum = num;
        Debug.Log(num);
        if (num >= 0 && num < 6)
        {
            photo.GetComponent<Image>().sprite = Res.LoadSprite(spritePath + "qw_xz" + (num + 2).ToString());
        }
        else if (num >= 6 && num < 12)
        {
            photo.GetComponent<Image>().sprite = Res.LoadSprite(spritePath + "qw_xz" + (num + 2).ToString());
        }
        switch (num)
        {
            case 0:
                puzzle_bg1.SetActive(true);
                puzzle1.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db1_pt_a" + (i + 1).ToString());
                    puzzle1s[i].sprite = Res.LoadSprite(spritePath + "db1_pt_a" + (i + 1).ToString());
                }
               
                break;
            case 1:
                puzzle_bg2.SetActive(true);
                puzzle2.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db2_pt_a" + (i + 1).ToString());
                    puzzle2s[i].sprite = Res.LoadSprite(spritePath + "db2_pt_a" + (i + 1).ToString());
                }
                break;
            case 2:
                puzzle_bg3.SetActive(true);
                puzzle3.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db3_pt_a" + (i + 1).ToString());
                    puzzle3s[i].sprite = Res.LoadSprite(spritePath + "db3_pt_a" + (i + 1).ToString());
                }

                break;
            case 3:
                puzzle_bg2.SetActive(true);
                puzzle2.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db2_pt_b" + (i + 1).ToString());
                    puzzle2s[i].sprite = Res.LoadSprite(spritePath + "db2_pt_b" + (i + 1).ToString());
                }
                break;
            case 4:
                puzzle_bg1.SetActive(true);
                puzzle1.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db1_pt_b" + (i + 1).ToString());
                    puzzle1s[i].sprite = Res.LoadSprite(spritePath + "db1_pt_b" + (i + 1).ToString());
                }
                break;
            case 5:
                puzzle_bg1.SetActive(true);
                puzzle1.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db1_pt_c" + (i + 1).ToString());
                    puzzle1s[i].sprite = Res.LoadSprite(spritePath + "db1_pt_c" + (i + 1).ToString());
                }
                break;
            case 6:
                puzzle_bg3.SetActive(true);
                puzzle3.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db3_pt_b" + (i + 1).ToString());
                    puzzle3s[i].sprite = Res.LoadSprite(spritePath + "db3_pt_b" + (i + 1).ToString());
                }
                break;
            case 7:
                puzzle_bg3.SetActive(true);
                puzzle3.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db3_pt_c" + (i + 1).ToString());
                    puzzle3s[i].sprite = Res.LoadSprite(spritePath + "db3_pt_c" + (i + 1).ToString());
                }
                break;
            case 8:
                puzzle_bg2.SetActive(true);
                puzzle2.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db2_pt_d" + (i + 1).ToString());
                    puzzle2s[i].sprite = Res.LoadSprite(spritePath + "db2_pt_d" + (i + 1).ToString());
                }
                break;
            case 9:
                puzzle_bg3.SetActive(true);
                puzzle3.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db3_pt_d" + (i + 1).ToString());
                    puzzle3s[i].sprite = Res.LoadSprite(spritePath + "db3_pt_d" + (i + 1).ToString());
                }
                break;
            case 10:
                puzzle_bg2.SetActive(true);
                puzzle2.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db2_pt_c" + (i + 1).ToString());
                    puzzle2s[i].sprite = Res.LoadSprite(spritePath + "db2_pt_c" + (i + 1).ToString());
                }
                break;
            case 11:
                puzzle_bg1.SetActive(true);
                puzzle1.SetActive(true);
                for (int i = 0; i < puzzles.Length; i++)
                {
                    puzzles[i].sprite = Res.LoadSprite(spritePath + "db1_pt_d" + (i + 1).ToString());
                    puzzle1s[i].sprite = Res.LoadSprite(spritePath + "db1_pt_d" + (i + 1).ToString());
                }
                break;

        }
        for (int i = 0; i < puzzles.Length; i++)
        {
            puzzles[i].GetComponent<Image>().SetNativeSize();
        }
    }

    public int GetPhotoNum() {
        return photonum;
    }

    public void GotoNext() {
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030301");
        EmailPuzzleController.Instance.CloseView();
        MakePhotoController.Instance.OpenView();       
    }

    public void SetPhoto() {
        photo.SetActive(true);
    }

    public void GameFinish() {
    if(Count==3) {
         
            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030205");
    }
    if(Count>=puzzles.Length) {
            SetPhoto();
           
            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030206");
            photo.transform.DOMove(new Vector3(0,0,72), 1f).OnComplete(SetEffect);
            Invoke("CloseBg_Camera", 1.5f);
            ClosePicture();
            Invoke("GotoNext", 5f);
        }
    }

    public IEnumerator startTime() {

       
        yield return new WaitForSeconds(5);
        time.SetActive(true);
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030203");

        while (TotalTime>0) {
                time.GetComponent<Image>().sprite = Res.LoadSprite(spritePath + "qw_pt" +(TotalTime+4).ToString());
        
            yield return new WaitForSeconds(1);
            TotalTime--;
            
        }
        time.SetActive(false);
        photo.SetActive(false);
        PuzzleMove();
        yield return new WaitForSeconds(1);
        CloseAndShowPuzzle();
        IsStop = false;

    }

    void PuzzleMove() {

        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030204");
     for(int i =0;i<parents.Length;i++) {
            puzzle1s[i].transform.DOMove(parents[i].transform.position, 2f);
            puzzle2s[i].transform.DOMove(parents[i].transform.position, 2f);
            puzzle3s[i].transform.DOMove(parents[i].transform.position, 2f);
            puzzle1s[i].transform.DOScale(new Vector3(0.5f,0.5f,0.5f), 2f);
            puzzle2s[i].transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 2f);
            puzzle3s[i].transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 2f);
        }
    }

    void CloseAndShowPuzzle() {
        puzzle1.SetActive(false);
        puzzle2.SetActive(false);
        puzzle3.SetActive(false);
        zhuanpanPuzzle.SetActive(true);
    }

    void ClosePicture() {
        motianlun.SetActive(false);
        puzzleParent.SetActive(false);
        parent.SetActive(false);
    }

    void SetEffect() {
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030207");
        bg_Camera.SetActive(true);

    }

    void CloseBg_Camera() {
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030208");
        bg_Camera.SetActive(false);
        effect.SetActive(true);
    }

    public void SetIsStop(bool isStop) {
        IsStop = isStop;
        isSoundPlay = isStop;
    }

    
 

  
}

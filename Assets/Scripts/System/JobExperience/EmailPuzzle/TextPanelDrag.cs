﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TextPanelDrag : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler,IPointerClickHandler
{

    private bool isInDrag = false;
    public void OnBeginDrag(PointerEventData eventData)
    {
        isInDrag = true;
        MakePhoto.Instance.LeftParent(this.gameObject);
    }

    public void OnDrag(PointerEventData eventData)
    {
        SetDraggedPosition(eventData);
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isInDrag = false;
        if (this.transform.localPosition.x >= -500 && this.transform.localPosition.x <= 500 && this.transform.localPosition.y >= -260 && this.transform.localPosition.y <= 260)
        {
            MakePhoto.Instance.OnTestButtonClick(this.gameObject);
            this.gameObject.SetActive(false);
        }
        else {
            MakePhoto.Instance.SetTextPanelParent(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public virtual void SetDraggedPosition(PointerEventData eventData)
    {
        var rt = gameObject.GetComponent<RectTransform>();

        // transform the screen point to world point int rectangle
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(rt, eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isInDrag)
        {
            return;
        }
        MakePhoto.Instance.OnTestButtonClick(this.gameObject);
        this.gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShowPhotos : MonoBehaviour {

    private static ShowPhotos _instance;

    private string spritePath = "game_puzzle/game_email_sprites/";

    public static ShowPhotos Instance {
    get {
            return _instance;
    }
    }

    private Image[] photos =new Image[5];

    public Image[]allphotos;

    public Image[] allPictures;

    public Image[] pos;

    /// <summary>  
    /// 第一次按下的位置  
    /// </summary>  
    private Vector2 first = Vector2.zero;
    /// <summary>  
    /// 鼠标的拖拽位置（第二次的位置）  
    /// </summary>  
    private Vector2 second = Vector2.zero;

    private bool isDown = false;

    private float timer;
    public float offsetTime = 0.1f;

    public bool moveDown = false;

    private int count=0;

    private float time;

    private int AudioPlayTime;

    public GameObject[] effect;

    public Image bg;

    public GameObject returnMainBtn;

    






    // Use this for initialization
    void Start()
    {
        _instance = this;
        UISpriteManager.Instance.LoadAtlas(emUIAltas.CommonAtlas);
        bg.GetComponent<Image>().sprite = ResourceManager.Instance.GetResource(Itempathinfo.PuzzleGameTexturePath + "qw_bg1", typeof(Sprite), enResourceType.Prefab, false, false).m_content as Sprite;
        returnMainBtn.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.CommonAtlas, "button_01");
        for (int i = 0; i < 6; i++)
        {
            allPictures[i].sprite = Res.LoadSprite(spritePath+"qw_xz" + (i + 2).ToString());
        }
        for (int i = 6; i < allPictures.Length; i++)
        {
            allPictures[i].sprite = Res.LoadSprite(spritePath + "qw_xz" + (i + 2).ToString());
        }
        if (PlayerPrefs.GetInt(PlayerPrefabInfo.ShowPhotoPlayTime) == 0)
        {
            AudioManager.Instance.StopAllSound();
            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030201");
            count++;
            PlayerPrefs.SetInt(PlayerPrefabInfo.ShowPhotoPlayTime, count);
        }
        
      
        //InitPhoto();
        UpdatePhotos();
        for (int i = 0; i < photos.Length; i++)
        {
            photos[i].rectTransform.position = pos[i].rectTransform.position;
        }
    }

    private void Update()
    {
        time += Time.deltaTime;
        if(time>=8f) {
            if (AudioPlayTime < 1)
            {
                AudioManager.Instance.StopAllSound();
                AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030202");
                AudioPlayTime++;
            }


        }
    }

    // Update is called once per frame

    void OnGUI()
    {

        if (Event.current.type == EventType.MouseDown)
        {
            
            //记录鼠标按下的位置 　　  
            first = Event.current.mousePosition;
            ShowPhotosController.Instance.SetDrag(true);

        }
        if (Event.current.type == EventType.MouseDrag)
        {
            time = 0;
            timer += Time.deltaTime;
            ShowPhotosController.Instance.SetDrag(true);
            
            //记录鼠标拖动的位置 　　
            if (timer > offsetTime)
            {
                second = Event.current.mousePosition;

                if (second.x < first.x - 5)
                {
                    //拖动的位置的x坐标比按下的位置的x坐标小时,响应向左事件 　　  
                    OnLeft();
                    MoveStart();


                }
                if (second.x > first.x + 5)
                {
                    //拖动的位置的x坐标比按下的位置的x坐标大时,响应向右事件 　　  
                    OnRight();
                    MoveStart();

                }
                first = second;
                timer = 0;
            }
        }
    }


    void OnLeft() {
        Debug.Log("左");
        setpos();
        if (moveDown == false)
        {
        
            photos[0].rectTransform.position = pos[4].rectTransform.position;
            photos[1].rectTransform.DOMove(pos[0].rectTransform.position, 0.5f, true);
            photos[1].rectTransform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.5f);
            photos[2].rectTransform.DOMove(pos[1].rectTransform.position, 0.5f, true);
            photos[2].rectTransform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.5f);
            photos[3].rectTransform.DOMove(pos[2].rectTransform.position, 0.5f, true);
            photos[3].rectTransform.DOScale(new Vector3(1f, 1f, 1f), 0.5f);
            photos[4].rectTransform.DOMove(pos[3].rectTransform.position, 0.5f, true);
            photos[4].rectTransform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.5f);
            OnLeftUpdatAllPhotos();
            UpdatePhotos();

            moveDown = true;
            Invoke("MoveFinish", 0.5f);

        }
    }

    void OnRight()
    {
        Debug.Log("右");
        setpos();
        if (moveDown == false)
        {
            photos[4].rectTransform.position = pos[0].rectTransform.position;
            photos[0].rectTransform.DOMove(pos[1].rectTransform.position, 0.5f, false);
            photos[0].rectTransform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.5f);
            photos[1].rectTransform.DOMove(pos[2].rectTransform.position, 0.5f, false);
            photos[1].rectTransform.DOScale(new Vector3(1, 1, 1), 0.5f);
            photos[2].rectTransform.DOMove(pos[3].rectTransform.position, 0.5f, false);
            photos[2].rectTransform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.5f);
            photos[3].rectTransform.DOMove(pos[4].rectTransform.position, 0.5f, false);
            photos[3].rectTransform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.5f);
            OnRightUpdatAllPhotos();
            UpdatePhotos();

            moveDown = true;
            Invoke("MoveFinish", 0.5f);
        }



    }

    void UpdatePhotos() {
    for(int i=0;i<photos.Length;i++) {
            photos[i] = allphotos[i];
            
    }
    }

    void setpos() {
        for (int i = 0; i < photos.Length; i++)
        {   
                photos[i].rectTransform.position= pos[i].rectTransform.position;
        }
    }


    private void OnLeftUpdatAllPhotos()
    {
        for(int i=0;i<allphotos.Length-1;i++) {
            Image a = allphotos[i];
            allphotos[i] = allphotos[i + 1];
            allphotos[i + 1] = a;
        }
    }

    private void OnRightUpdatAllPhotos()
    {
        for (int i = allphotos.Length - 1; i >0; i--)
        {
            Image a = allphotos[i];
            allphotos[i] = allphotos[i -1];
            allphotos[i -1] = a;
        }
    }
    private void MoveFinish()
    {
        moveDown = false;
        ShowPhotosController.Instance.SetDrag(false);
        for (int i = 0; i < allphotos.Length; i++)
        {
            allphotos[i].GetComponent<HUIMiniEventScript>().enabled = true;
        }

    }

    private void InitPhoto()
    {
        for (int i = 0; i < allphotos.Length; i++)
        {
            allphotos[i].sprite = Res.LoadSprite(spritePath + "qw_xz0" + i.ToString());
        }
    }

    private void MoveStart() {
        for (int i = 0; i < allphotos.Length; i++)
        {
            allphotos[i].GetComponent<HUIMiniEventScript>().enabled = false;
        }
    }


    public void ShowEffect(int i)
    {
        effect[i].SetActive(true);
        EventBus.Instance.RemoveEventHandler<int>(EventID.PHOTO_SHOW_EFFECT, this.ShowEffect);
    }















}

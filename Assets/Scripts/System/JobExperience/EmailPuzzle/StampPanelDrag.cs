﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StampPanelDrag : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler,IPointerClickHandler
{

    private bool isInDrag = false;
    private bool isClick = false;
    private bool isDown = false;
    public void OnBeginDrag(PointerEventData eventData)
    {
        MakePhoto.Instance.StampPanelposMove(false);
        isInDrag = true;
        MakePhoto.Instance.LeftParent(this.gameObject);
    }

    public void OnDrag(PointerEventData eventData)
    {
       if(isDown) {
            return;
       }
        SetDraggedPosition(eventData);
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        MakePhoto.Instance.StampPanelposMove(true);
        if (isDown) {
            return;
     }
       
        isInDrag = false;
        if (this.transform.localPosition.x >= -500 && this.transform.localPosition.x <= 500 && this.transform.localPosition.y >= -260 && this.transform.localPosition.y <= 260)
        {
            isDown = true;
            MakePhoto.Instance.MoveToStampPos(this.gameObject);
            isClick = true;
        }else {
            MakePhoto.Instance.SetstampPanelParent(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public virtual void SetDraggedPosition(PointerEventData eventData)
    {
        var rt = gameObject.GetComponent<RectTransform>();

        // transform the screen point to world point int rectangle
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(rt, eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    if(isInDrag) {
            return;
    }
    if(isClick) {
            return;
    }
        isClick = true;
        MakePhoto.Instance.LeftParent(this.gameObject);
        MakePhoto.Instance.MoveToStampPos(this.gameObject);
    }
}

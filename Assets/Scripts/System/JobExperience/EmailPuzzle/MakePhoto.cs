﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MakePhoto : MonoSingleton<MakePhoto>
{

    public float time;

    private int count = 0;

    public GameObject photo;

    private int sendEmailNum = 0;

    public GameObject image;

    public GameObject effect;

    public GameObject finish;

    public GameObject plane;

    public GameObject bg_Camera;

    public Transform[] stampPanelpos;

    public Image[] stampPanelItems;

    public GameObject stampPos;

    public GameObject stampPanel;

    public GameObject textPanel;

    public GameObject itemPanel;

    public Transform[] itemPanelpos;

    public Image[] itemPanelItems;

    public Image[] textPanelItems;

    public Transform[] textPanelpos;

    private List<Sprite> itemPanelspriteList = new List<Sprite>();

    private List<Sprite> stampPanelspriteList = new List<Sprite>();

    private List<Sprite> textPanelspriteList = new List<Sprite>();

    private string spritePath = "game_puzzle/game_email_sprites/";

    private int photoCount =0;

    public Text text;

    private List<GameObject> items = new List<GameObject>();

    public int PhotoCount {
        get {
            return photoCount;
        }
    }





    // Use this for initialization
    void Start()
    {
        LoadSprite();
        Shuffle(itemPanelspriteList);
        Shuffle(stampPanelspriteList);
        SetSprite();
       
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        PhotoFinish();
       
    }

    public void AddPhotoCount() {
        photoCount++;
    }

    public void ResetTime()
    {
        time = 0;
    }



    public void PhotoFinish()
    {
        if (photoCount > 0 && count < 1)
        {
            EventBus.Instance.BroadCastEvent<bool>(EventID.SHOW_FINISH_BTN, true);
            count++;
            //Debug.Log("明信片完成");
            //count++;



        }
    }

    public void OnFinishBtnClick()
    {
        DestroyClick();
        ItemsSetFalse();
        photo.transform.position = new Vector3(0, 0, 72);
        EventBus.Instance.BroadCastEvent<bool>(EventID.SHOW_FINISH_BTN, false);
        image.SetActive(false);
        bg_Camera.SetActive(true);
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030207");
        Invoke("SetBg_Camera", 0.5f);
    }

    public void OnPhotoButtonClick()
    {
        if (PlayerPrefs.GetInt(PlayerPrefabInfo.sendEmailNum) != 0)
        {
            sendEmailNum = PlayerPrefs.GetInt(PlayerPrefabInfo.sendEmailNum);
        }
        EventBus.Instance.BroadCastEvent(EventID.PHOTO_GAME_END);
        sendEmailNum++;
        PlayerPrefs.SetInt(PlayerPrefabInfo.sendEmailNum, sendEmailNum);
        photo.SetActive(false);
        effect.SetActive(false);
        finish.SetActive(true);
        plane.SetActive(true);
        Invoke("Finish", 2.5f);
        AudioManager.Instance.StopAllSound();
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030303");
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030310");
    }

    void SetBg_Camera()
    {
        photo.GetComponent<Button>().enabled = true;
        effect.SetActive(true);
        bg_Camera.SetActive(false);
        AudioManager.Instance.StopAllSound();
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030309");


    }

     void Finish()
    {

        MakePhotoController.Instance.CloseView();
        EmailGame.instance.SetButton(true);
        EmailGame.instance.SetCamEvent();
        EmailGame.instance.GetInfo();
        //SceneLoader.Instance.LoadScene(SceneLoader.EmailScene);
    }

    public void SetstampPanelParent(GameObject obj)
    {
        for (int i = 0; i < stampPanelItems.Length; i++)
        {
            if (obj.name == stampPanelItems[i].name)
            {
                stampPanelItems[i].transform.SetParent(stampPanelpos[i]);
                stampPanelItems[i].rectTransform.localPosition = Vector3.zero;
                stampPanelItems[i].rectTransform.localRotation = Quaternion.Euler(Vector3.zero);
                stampPanelItems[i].rectTransform.localScale = new Vector3(1, 1, 1);

            }
        }

    }
    public void SetTextPanelParent(GameObject obj)
    {
        for (int i = 0; i < textPanelItems.Length; i++)
        {
            if (obj.name == textPanelItems[i].name)
            {
                obj.transform.SetParent(textPanelpos[i]);
                obj.GetComponent<Image>().rectTransform.localPosition = Vector3.zero;
                obj.GetComponent<Image>().rectTransform.localRotation = Quaternion.Euler(Vector3.zero);
                obj.GetComponent<Image>().rectTransform.localScale = new Vector3(1, 1, 1);

            }
        }

    }

    public void SetItemPanelParent(GameObject obj)
    {
        for (int i = 0; i < itemPanelItems.Length; i++)
        {
            if (obj.name == itemPanelItems[i].name)
            {
                obj.transform.SetParent(itemPanelpos[i]);
                obj.GetComponent<Image>().rectTransform.localPosition = Vector3.zero;
                obj.GetComponent<Image>().rectTransform.localRotation = Quaternion.Euler(Vector3.zero);
                obj.GetComponent<Image>().rectTransform.localScale = new Vector3(1, 1, 1);

            }
        }

    }



    public void LeftParent(GameObject obj)
    {
        obj.transform.SetParent(photo.transform);
    }

    public void MoveToStampPos(GameObject obj)
    {
        Destroy(obj.GetComponent<StampPanelDrag>());
        obj.GetComponent<Image>().rectTransform.DOMove(stampPos.GetComponent<Image>().rectTransform.position, 1f, false);
        stampPanel.SetActive(false);
        textPanel.SetActive(true);
        AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030302");
    }




    public void LoadSprite()
    {
        for (int i = 0; i < itemPanelItems.Length; i++)
        {
            itemPanelspriteList.Add(Res.LoadSprite(spritePath + "qw_mxp" + (i + 21).ToString()));
        }

        for (int i = 0; i < textPanelItems.Length; i++)
        {
            textPanelspriteList.Add(Res.LoadSprite(spritePath + "qw_mxp" + (i + 16).ToString()));
        }

        for (int i = 0; i < itemPanelItems.Length; i++)
        {
            stampPanelspriteList.Add(Res.LoadSprite(spritePath + "qw_mxp" + (i + 8).ToString()));
        }
    }

    //打乱排序
    void Shuffle(List<Sprite> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            var temp = list[i];
            int randomIndex = Random.Range(0, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }

    }

    //加载图片

    void SetSprite()
    {
        for (int i = 0; i < itemPanelItems.Length; i++)
        {
            itemPanelItems[i].sprite = itemPanelspriteList[i];
            itemPanelItems[i].SetNativeSize();
        }

        for(int i =0;i<textPanelItems.Length;i++) {
            textPanelItems[i].sprite = textPanelspriteList[i];
            textPanelItems[i].SetNativeSize();
        }

        for(int i =0;i<stampPanelItems.Length;i++) {
            stampPanelItems[i].sprite = stampPanelspriteList[i];
            stampPanelItems[i].SetNativeSize();
        }
    }

    public void OnTestButtonClick(GameObject obj)
    {
        AudioManager.Instance.StopAllSound();
        textPanel.gameObject.SetActive(false);

        switch (obj.name)
        {
            case "Image":
                text.text = "     祝你身体棒棒! 永远不生病";
                AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030303");
                break;
            case "Image (1)":
                text.text = "     希望我们永远都 是好朋友！";
                AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030304");
                break;
            case "Image (2)":
                text.text = "     祝你有吃不完的 零食！";
                AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030305");
                break;
            case "Image (3)":
                text.text = "     希望你没有烦恼 天天开心！";
                AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030306");
                break;
            case "Image (4)":
                text.text = "     祝你在学校，每 天都能得到小红花";
                AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030307");
                break;

        }

        itemPanel.gameObject.SetActive(true);
        //AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030308");
    }

    public void AddList(GameObject obj) {
        items.Add(obj);
    }

    private void ItemsSetFalse() {
    foreach(var temp in items) {
            temp.GetComponent<ItemPanelDrag>().enabled = false;
    }
    }

    public void StampPanelposMove(bool isMove) {
    foreach(var temp in stampPanelpos) {
            temp.GetComponent<ItemMove>().enabled = isMove;
    }
    }

    public void ItemPanelposMove(bool isMove) {
    foreach(var temp in itemPanelpos) {
            temp.GetComponent<ItemPanelMove>().enabled = isMove;
    }
    }

    private void DestroyClick() {
    foreach(Transform child in photo.transform) {
            Destroy(child.GetComponent<ItemPanelDrag>());
            Destroy(child.GetComponent<Button>());
    }
    }



}

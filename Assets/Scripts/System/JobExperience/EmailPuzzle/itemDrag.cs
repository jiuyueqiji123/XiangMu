﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class itemDrag : PuzzleInput {
    public override void OnBeginDrag(PointerEventData eventData)
    {

    }

    public override void OnDrag(PointerEventData eventData)
    {

        if (this.transform.localPosition.x >= -745 && this.transform.localPosition.x <= 337 && this.transform.localPosition.y >= -260 && this.transform.localPosition.y <= 260)
        {
            SetDraggedPosition(eventData);
            this.transform.Find("502004046").SetActive(true);
        }
    }


    public override void OnEndDrag(PointerEventData eventData)
    {
        this.transform.Find("502004046").SetActive(false);
    }

    private void Update()
    {
        if (this.transform.localPosition.x < -745)
        {
            Vector3 vect = this.transform.localPosition;
            vect.x = -744;
            transform.localPosition = vect;
        }
        if (this.transform.localPosition.x > 337)
        {
            Vector3 vect = this.transform.localPosition;
            vect.x = 336;
            transform.localPosition = vect;
        }
        if (this.transform.localPosition.y < -260)
        {
            Vector3 vect = this.transform.localPosition;
            vect.y = -259;
            transform.localPosition = vect;
        }
        if (this.transform.localPosition.y > 260)
        {
            Vector3 vect = this.transform.localPosition;
            vect.y = 259;
            transform.localPosition = vect;
        }
    }




}

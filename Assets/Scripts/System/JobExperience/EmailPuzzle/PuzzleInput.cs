﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PuzzleInput : MonoBehaviour, IDragHandler,IDropHandler,IBeginDragHandler,IEndDragHandler
{
    public bool isOnPuzzle = false;
    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        EmailPuzzle.Instance.SetIsStop(true);
        EmailPuzzle.Instance.ClearTime();
        EmailPuzzle.Instance.SetParent(this.gameObject);
        this.transform.rotation = Quaternion.Euler(Vector3.zero);
        this.transform.localScale = new Vector3(1, 1, 1);
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        
        SetDraggedPosition(eventData);
    }

    public virtual void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnDrop");
       
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        EmailPuzzle.Instance.SetIsStop(false);
        if (isOnPuzzle == false)
        {
            EmailPuzzle.Instance.SetZhuanPanParent(this.gameObject);
            
        }
    }

    public virtual  void SetDraggedPosition(PointerEventData eventData)
    {
        var rt = gameObject.GetComponent<RectTransform>();

        // transform the screen point to world point int rectangle
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(rt, eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
        }
    }

 

    private  void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.name == this.name)
        {
            EmailPuzzle.Instance.SetIsStop(false);
            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030206");
            EmailPuzzle.Instance.AddCount();
            isOnPuzzle = true;
            this.transform.position = other.transform.position;
            Destroy(this);
            EmailPuzzle.Instance.GameFinish();

        }
  

    }

   

    


}

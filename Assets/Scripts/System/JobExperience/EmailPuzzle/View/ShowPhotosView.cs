﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// View用来管理窗口或者其它视图的显示，监听用户输入，然后执行Controller中的逻辑。
/// Controller可以改变view的显示
/// view最好不要引用model对象
/// </summary>

public class ShowPhotosView
{

    private const string VIEW_PATH = "game_puzzle/game_puzzle_ui/ShowPhotos";

    private List<GameObject> childs = new List<GameObject>();

    private HUIFormScript form;

    private string spritePath = "game_puzzle/game_email_sprites/";


    public void OpenForm()
    {
        form = HUIManager.Instance.OpenForm(VIEW_PATH, false);
        this.InitForm();
    }

    public void CloseForm()
    {
        HUIManager.Instance.CloseForm(form);
    }

    private void InitForm()
    {
        Debug.Log(form.name);
        Transform Panel = form.transform.Find("Panel");
        Debug.Log(Panel.name);
        childs.Clear();
        foreach (Transform child in Panel)
        {
            childs.Add(child.gameObject);
        }
        for (int i = 0; i < childs.Count; i++)
        {
            HUIUtility.SetUIMiniEvent(childs[i], enUIEventType.Click, enUIEventID.ShowPhoto_Btn_Click, new stUIEventParams() { argInt = i });
            childs[i].GetComponent<Image>().sprite = Res.LoadSprite(spritePath + "qw_xz1");
        }
        GameObject retuanMainBtn = form.transform.Find("returnMainBtn").gameObject;
        HUIUtility.SetUIMiniEvent(retuanMainBtn, enUIEventType.Click, enUIEventID.ReturnMain_Btn_Click);


    }
}


 


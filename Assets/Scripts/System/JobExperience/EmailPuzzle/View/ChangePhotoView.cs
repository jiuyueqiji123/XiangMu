﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// View用来管理窗口或者其它视图的显示，监听用户输入，然后执行Controller中的逻辑。
/// Controller可以改变view的显示
/// view最好不要引用model对象
/// </summary>

public class ChangePhotoView
{

    private const string VIEW_PATH = "game_puzzle/game_puzzle_ui/ChangePhotos";

    private string TexturePath = "game_puzzle/game_puzzle_texture/";

    private HUIFormScript form;

    private GameObject closeBtn;

    private GameObject btnYes;

    private GameObject btnNo;

    private GameObject newPhoto;

    private GameObject oldPhoto;

    public void OpenForm() {
        form = HUIManager.Instance.OpenForm(VIEW_PATH, false);
        this.InitForm();
    }

    public void CloseForm()
    {
        HUIManager.Instance.CloseForm(form);
    }

    private void InitForm() {
        closeBtn = form.transform.Find("CloseBtn").gameObject;
        btnNo = form.transform.Find("BtnNo").gameObject;
        btnYes = form.transform.Find("BtnYes").gameObject;
        newPhoto = form.transform.Find("RawImage/newPhoto/RawImage").gameObject;
        oldPhoto = form.transform.Find("RawImage/OldPhoto/RawImage").gameObject;
        HUIUtility.SetUIMiniEvent(closeBtn, enUIEventType.Click, enUIEventID.Close_Btn_Click);
        HUIUtility.SetUIMiniEvent(btnYes, enUIEventType.Click, enUIEventID.Yes_Btn_Click);
        HUIUtility.SetUIMiniEvent(btnNo, enUIEventType.Click, enUIEventID.No_Btn_Click);
        newPhoto.GetComponent<RawImage>().texture = ResourceManager.Instance.GetResource(TexturePath + EmailGame.instance.GetLastPhotoName(), typeof(Texture), enResourceType.Prefab).m_content as Texture;
        oldPhoto.GetComponent<RawImage>().texture = ResourceManager.Instance.GetResource(TexturePath + EmailGame.instance.GetFirstPhotoName(), typeof(Texture), enResourceType.Prefab).m_content as Texture;
    }

 
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// View用来管理窗口或者其它视图的显示，监听用户输入，然后执行Controller中的逻辑。
/// Controller可以改变view的显示
/// view最好不要引用model对象
/// </summary>

public class EmailPuzzleView {

    private const string VIEW_PATH = "game_puzzle/game_puzzle_ui/EmailPuzzle";


    private HUIFormScript form;

    public void OpenForm() {
        form = HUIManager.Instance.OpenForm(VIEW_PATH, false);
        this.InitForm();
    }

    public void CloseForm()
    {
        HUIManager.Instance.CloseForm(form);
    }

    private void InitForm() {
        GameObject retuanMainBtn = form.transform.Find("returnMainBtn").gameObject;
        HUIUtility.SetUIMiniEvent(retuanMainBtn, enUIEventType.Click, enUIEventID.ReturnMain_Btn_Click);
    }

 
}

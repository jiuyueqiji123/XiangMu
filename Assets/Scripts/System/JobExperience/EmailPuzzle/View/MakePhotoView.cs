﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
/// <summary>
/// View用来管理窗口或者其它视图的显示，监听用户输入，然后执行Controller中的逻辑。
/// Controller可以改变view的显示
/// view最好不要引用model对象
/// </summary>

public class MakePhotoView {

    private const string VIEW_PATH = "game_puzzle/game_puzzle_ui/MakePhoto";
    private List<GameObject> stampPanelchilds = new List<GameObject>();
    private List<GameObject> textPanelchilds = new List<GameObject>();
    private List<GameObject> itemPanelchilds = new List<GameObject>();

    private GameObject stampPos;
    private Transform stampPanel;
    private Transform textPanel;
    private Transform itemPanel;
    private GameObject photo;
    private Text text;
    private GameObject image;
    private GameObject returnMainBtn;
    public static int Count;
    private GameObject bg;
    private string spritePath = "game_puzzle/game_email_sprites/";
    private GameObject finishBtn;
    private int count = 0;




    private HUIFormScript form;

    public void OpenForm() {
        form = HUIManager.Instance.OpenForm(VIEW_PATH, false);
        this.InitForm();
        Count = 0;
        
    }

    public void CloseForm()
    {
        HUIManager.Instance.CloseForm(form);
        EventBus.Instance.RemoveEventHandler(EventID.PHOTO_GAME_END, OnGameFinish);
        EventBus.Instance.RemoveEventHandler<bool>(EventID.SHOW_FINISH_BTN, ShowFinishBtn);
    }

    private void InitForm() {
        count = 0;
        EventBus.Instance.AddEventHandler(EventID.PHOTO_GAME_END, OnGameFinish);
        

        bg = form.transform.Find("BG").gameObject;
        stampPanel = form.transform.Find("Image/Image/stampPanel");
        textPanel = form.transform.Find("Image/Image/textPanel");
        itemPanel = form.transform.Find("Image/Image/itemPanel");
        stampPos = form.transform.Find("photo/stampPos").gameObject;
        text = form.transform.Find("photo/Text").GetComponent<Text>();
        image = form.transform.Find("Image").gameObject;
        photo = form.transform.Find("photo").gameObject;
        finishBtn = form.transform.Find("finishBtn").gameObject;
        EventBus.Instance.AddEventHandler<bool>(EventID.SHOW_FINISH_BTN, ShowFinishBtn);
        returnMainBtn = form.transform.Find("returnMainBtn").gameObject;
        //returnMainBtn = form.transform.Find("returnMainBtn").gameObject;
        SetImageAndAddEvent();
        GameObject retuanMainBtn = form.transform.Find("returnMainBtn").gameObject;
        HUIUtility.SetUIMiniEvent(retuanMainBtn, enUIEventType.Click, enUIEventID.ReturnMain_Btn_Click);


    }


    public void SetImageAndAddEvent()
    {
        
        int num = EmailPuzzle.Instance.GetPhotoNum();
        returnMainBtn.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.CommonAtlas, "button_01");
        //image.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Email1, "qw_mxp29");
        bg.GetComponent<Image>().sprite = ResourceManager.Instance.GetResource(Itempathinfo.PuzzleGameTexturePath+ "qw_bg1",typeof(Sprite), enResourceType.Prefab, false, false).m_content as Sprite;
        photo.GetComponent<Image>().sprite = ResourceManager.Instance.GetResource(Itempathinfo.PuzzleGameTexturePath + "qw_mxp" + Random.Range(1, 7).ToString(),typeof(Sprite), enResourceType.Prefab, false, false).m_content as Sprite;
        GameObject im = form.transform.Find("photo/Image").gameObject;
        if (num >= 0 && num < 6)
        {
            im.GetComponent<Image>().sprite = Res.LoadSprite(spritePath + "qw_xz" + (num + 2).ToString());
        }
        else if (num >= 6 && num < 12)
        {
            im.GetComponent<Image>().sprite = Res.LoadSprite(spritePath + "qw_xz" + (num + 2).ToString());
        }

        Debug.Log(stampPanel.name);
        foreach (Transform child in stampPanel)
        {
            stampPanelchilds.Add(child.gameObject);
            //Debug.Log(child.name);
        }


        foreach (Transform child in textPanel)
        {
            textPanelchilds.Add(child.gameObject);
        }


        foreach (Transform child in itemPanel)
        {
            itemPanelchilds.Add(child.gameObject);
        }

     


    }






    public void OnGameFinish() {
        Debug.Log("变成飞机·飞走了");
        stampPanelchilds.Clear();
        textPanelchilds.Clear();
        itemPanelchilds.Clear();
    }

    public void ShowFinishBtn(bool isActive) {
        finishBtn.SetActive(isActive);
    }






}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PhotoInput : MonoBehaviour,  IInputClickHandler, IInputDragHandler, IInputBeginDragHandler, IInputEndDragHandler
{
    private bool isDrag =false;
    private bool isClick = false;
    private Vector3 pos;
    private Vector3 scale;

    public delegate void ResetPosdelegate();

    public static event ResetPosdelegate posEvent;

    public GameObject setGameObject;

    private bool isShow = false;

   



   public static void DoEvent() {
        if(posEvent !=null)
        posEvent();
   }

    private void Start()
    {
        pos = this.transform.position;
    }

    private void Update()
    {
        if (this.transform.localPosition.x > 49)
        {
            Vector3 vect = this.transform.localPosition;
            vect.x = 48;
            transform.localPosition = vect;
        }
        if (this.transform.localPosition.x < -59)
        {
            Vector3 vect = this.transform.localPosition;
            vect.x = -58;
            transform.localPosition = vect;
        }
        if (this.transform.localPosition.y > 126)
        {
            Vector3 vect = this.transform.localPosition;
            vect.y= 125;
            transform.localPosition = vect;
        }
        if (this.transform.localPosition.y< 62)
        {
            Vector3 vect = this.transform.localPosition;
            vect.y = 63;
            transform.localPosition = vect;
        }
    }
    public void OnInputBeginDrag(InputEventData eventData)
    {
        Debug.Log("aaaaaaaa");
    }

    public void OnInputClick(InputEventData eventData)
    {
       if(isClick) {
            return;
       }
        if (isDrag == false)
        {
            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030104");
            isClick = true;
            isShow = true;
            scale = this.transform.localScale;
            setGameObject = this.gameObject;
            this.transform.DOScale(new Vector3(6, 3.4f,3.4f),0.1f);
            this.transform.position = new Vector3(0, 97.6f, 0);
            Debug.Log(pos);
            posEvent += ResetPos;
            Camera.main.GetComponent<EmailGame>().SetPlane(true);
            string name =this.GetComponent<MeshRenderer>().material.name.Replace(" (Instance)", string.Empty); 
            
            switch (name)
            {
                case "sd_mxp2":
                    AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030407");
                    break;
                case "sd_mxp7":
                    AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030411");
                    break;
                case "sd_mxp8":
                    AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030409");
                    break;
                case "sd_mxp9":
                    AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030412");
                    break;
                case "sd_mxp6":
                    AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030413");
                    break;
                case "sd_mxp4":
                    AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030414");
                    break;
                case "sd_mxp3":
                    AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030415");
                    break;
                default:
                    int num = UnityEngine.Random.Range(1, 8);
                    switch(num) {
                        case 1:
                            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030402");
                            break;
                        case 2:
                            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030403");
                            break;
                        case 3:
                            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030404");
                            break;
                        case 4:
                            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030405");
                            break;
                        case 5:
                            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030406");
                            break;
                        case 6:
                            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030408");
                            break;
                        case 7:
                            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "404030410");
                            break;

                    }
                    break;

            }


        }
        
    }

   public void ResetPos()
    {
        isClick = false;
        gameObject.transform.localScale = new Vector3(3.0768f,1.709f,1.709f);
        gameObject.transform.position = pos;
        isShow = false;
        posEvent -= ResetPos;
    }






    public void OnInputDrag(InputEventData eventData)
    {
        if (isShow == false)
        {
            isDrag = true;
            AudioManager.Instance.PlaySound(Itempathinfo.puzzleGamePath + "604030102");
            //var position = this.transform.position;

            //position.z = -110;
            //this.transform.position = position;
            if (this.transform.localPosition.x <= 49 && this.transform.localPosition.x >= -59 && this.transform.localPosition.y <= 126 && this.transform.localPosition.y >= 62 && isShow == false)
            {
                Vector3 delta = new Vector3(-eventData.Delta.x, eventData.Delta.y, 0f);

                transform.position += delta * 0.1f;
                Debug.Log(this.transform.localPosition);
            }
            this.transform.Find("502004040").SetActive(true);
        }
    }

    public void OnInputEndDrag(InputEventData eventData)
    {
        
        isDrag = false;
        //var position = this.transform.position;
        this.gameObject.SetActive(false);
        this.gameObject.SetActive(true);

        //position.z = -112;
        //this.transform.position = position;
        this.transform.Find("502004040").SetActive(false);
        if (isShow == false)
        {
            pos = this.transform.position;
        }

    }


}

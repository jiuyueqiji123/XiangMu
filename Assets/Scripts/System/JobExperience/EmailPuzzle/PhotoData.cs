﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class PhotoData  {

    public Vector3 pos;

    public string meterialName;

    public bool isShow;

    
}

[Serializable]
public class PhotoDataList {
    public List<PhotoData> photoDataList = new List<PhotoData>();
}

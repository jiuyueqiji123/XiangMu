﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
/// <summary>
/// 整理购物车
/// </summary>
public class SortoutCarController : MonoSingleton<SortoutCarController>
{
    Transform cars;
    //整理目标位置
    [HideInInspector]
    public Vector3 targetPos = new Vector3(-1.1f, 0, 1.55f);
    //间隔
    [HideInInspector]
    public float intervalX = 0.2f;

    //当前整理数量
    public int CurSortNum
    {
        get; set;
    }
    //整理的总数量
    int totalNum;

    GameObject effect;

    Transform handClick;

    protected override void Awake()
    {
        cars = transform.Find("cars");
        effect = transform.Find("502004088").gameObject;

        handClick = transform.Find("Hand_Click");
        // ===== mjbTest
        //StartGame();
    }
    float interval = 10f;
    public float totalCount = 0;
    public bool isShake = false;

    public bool isHand = false;
    void Update()
    {
        if (isShake)
        {
            totalCount += Time.deltaTime;
            if (totalCount > interval)
            {
                SuperMarketGame.Instance.soundCtrl.PlaySmallGameSound(GameType.ShoppingCart);
                totalCount = 0;
                int r = Random.Range(0, totalNum);
                cars.GetChild(r).GetComponent<DragShoppingCar>().ShakeCar();
            }
        }
    }
    public void StartGame()
    {
        SuperMarketGame.Instance.soundCtrl.PlaySmallGameSound(GameType.ShoppingCart);
        totalNum = Random.Range(5, 7);

        int handGuid = 0;
        if (SuperMarketGame.Instance.sortoutCarGameCount == 1)
        {
            //isShake = true;
            isHand = true;
            //随机一个 car 上方出指引
            //handGuid = Random.Range(0, totalNum);
        }
        //else
        //{
        isShake = true;
        //isHand = false;
        //totalCount = 8f;
        //}
        //TODO  
        CurSortNum = 0;


        for (int i = 0; i < cars.childCount; i++)
        {
            if (i < totalNum)
            {
                cars.GetChild(i).SetActive(true);
                if (isHand && i == handGuid)
                {
                    handClick.gameObject.SetActive(true);
                    PlayHandTween(cars.GetChild(i).position);
                }
            }
            else
                cars.GetChild(i).SetActive(false);
        }
    }

    void PlayHandTween(Vector3 car)
    {
        Vector3 from = new Vector3(car.x, car.y + 1.2f, car.z);
        //Vector3 to = new Vector3(car.x, car.y + 3f, car.z);
        handClick.position = from;
        handClick.DOMoveY(car.y + 0.8f, 0.5f).SetEase(Ease.Linear).SetLoops<Tweener>(-1, LoopType.Yoyo).OnComplete(() =>
        {
            if (!isHand)
                handClick.DOKill();
        });
    }

    public void HideHand()
    {
        isHand = false;
        handClick.gameObject.SetActive(false);
    }

    public void EndGame()
    {
        //TODO  
        isShake = false;
        CurSortNum = 0;
        SuperMarketGame.Instance.gameCount = 0;
        SuperMarketGame.Instance.OnReset(TransitionType.Cloud, () =>
        {
            effect.SetActive(false);
            Destroy(gameObject);
        }, () =>
        {
        
        });
    }

    public void PlayEffect()
    {
        SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_604050101);
        CurSortNum++;
        //播放特效
        //Debug.Log(CurSortNum);
        if (CurSortNum >= totalNum)
        {
            SuperMarketGame.Instance.soundCtrl.PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050405);
            effect.SetActive(true);
            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_604050102);
            ParticleSystem ps = effect.transform.GetChild(0).GetComponent<ParticleSystem>();
            if (!ps.isPlaying)
                ps.Play();
            //TODO    EndGame   
            StartCoroutine(TimeUtility.DelayInvoke(2.5f, () =>
            {
                Debug.Log("  结束整理 ");
                EndGame();
            }));

        }
    }
}

﻿using UnityEngine;
using System.Collections;
/// <summary>
///清洁超市小游戏
/// </summary>
public class CleanMarketController : MonoSingleton<CleanMarketController>
{
    Transform wuzi;

    GameObject handClick;

    GameObject successEffect;

    GameObject cleanEffect;

    DragMop mop;

    /*private static CleanMarketController _instance;

    public static CleanMarketController Instance
    {
        get
        {
            if(_instance == null)
                _instance = FindObjectOfType<CleanMarketController>() as CleanMarketController;

            return _instance;
        }
    }*/
    protected override void Awake()
    {
        //边界值  x  -1.7  0.3      z -0.7  0.2   y 0  
        wuzi = transform.GetChild(0);
        handClick = transform.Find("hand_click").gameObject;
        mop = transform.Find("304050104").gameObject.GetComponent<DragMop>();
        cleanEffect = wuzi.Find("502004086").gameObject;
        successEffect = wuzi.Find("502004087").gameObject;
    }
    public float timeCheck = 10f;           //超时间隔
    private float lastTimestamp;            //最后一次操作时间戳
    public void _timerstampUpdate()    {        lastTimestamp = Time.realtimeSinceStartup;    }   
    private void Update()
    {
        if (Time.realtimeSinceStartup - lastTimestamp >= timeCheck)
        {
            Debug.Log("未操作时间戳. ___" + Time.realtimeSinceStartup);
            _timerstampUpdate();
            SuperMarketGame.Instance.soundCtrl.PlaySmallGameSound(GameType.Cleanliness);
        }
    }

    public void EnableHand(bool enable)
    {
        handClick.SetActive(enable);
    }

    public void StartGame()
    {
        //TODO    隐藏场景的购物车   开启小手指引
        SuperMarketGame.Instance.soundCtrl.PlaySmallGameSound(GameType.Cleanliness);
        MarketMainSceneCtrl.Instance.EnableShopCar(false);
        float x = Random.Range(-1.6f, 0.1f);
        float z = Random.Range(-0.1f, 0.5f);
        wuzi.localPosition = new Vector3(x, 0.1f, z);
        //Debug.Log(wuzi.localPosition);
        EnableHand(true);
        mop.IsClean = true;
    }

    public void PlayEffect()
    {
        successEffect.SetActive(true);
    }

    public void PlayCleanEffect(bool enable)
    {
        cleanEffect.SetActive(enable);
    }

    public void EndGame()
    {
        //TODO   1 播放成功特效 502004014 2 销毁此物体 
        SuperMarketGame.Instance.gameCount = 0;
        SuperMarketGame.Instance.OnReset(TransitionType.Cloud, () =>
        {
            MarketMainSceneCtrl.Instance.EnableShopCar(true);
            Destroy(gameObject);
        });
    }
}

﻿using UnityEngine;
using System.Collections;

public class SuperMarketSoundCtrl : MonoBehaviour
{
    void Awake()
    {
        AudioManager.Instance.StopAllSound();
        PlayBGM();
    }

    public void PlayBGM()
    {
        //背景音乐
        AudioManager.Instance.PlayMusic(FruitConfig.soundPath + "604020603", true, true, true);
        AudioManager.Instance.BackgroundMusicVolume = 0.6f;
    }

    public void PauseBGM()
    {
        AudioManager.Instance.PauseBackgroudMusic();
    }
    /// <summary>
    /// 播放音效
    /// </summary>
    /// <param name="path"></param>
    public void PlaySoud(string path)
    {
        AudioClip clip = AudioManager.Instance.GetAudioClip(path);
        if (AudioManager.Instance.IsPlayingSoundClip(clip)) return;
        AudioManager.Instance.PlaySound(path);
    }

    public void PlaySoundAnim(string path, System.Action act = null)
    {
        var length = AudioManager.Instance.PlaySound(path).Length;
        StartCoroutine(TimeUtility.DelayInvoke(length, act));
    }

    private bool isPlayingLdAniSound = false;
    public void PlayLeDiVoiceAnimation(string voicePath, System.Action act = null)
    {
        //Debug.Log("PlayLeDiVoiceAnimation ===== ");
        AudioClip clip = AudioManager.Instance.GetAudioClip(voicePath);
        //AudioManager.Instance.IsPlayingSoundClip(clip) || SuperMarketGame.Instance.GetLediAnimIsTalk()
        if (isPlayingLdAniSound)
        {
            return;
        }
        isPlayingLdAniSound = true;
        SuperMarketGame.Instance.SetLediAnim("TalkBegin");

        StartCoroutine(TimeUtility.DelayInvoke(0.5f, () =>
        {
            //if (AudioManager.Instance.IsPlayingSoundClip(clip))
            //{
            //SuperMarketGame.Instance.SetLediAnim("idle");
            //return;
            //AudioManager.Instance.StopSound(clip);
            //}
            var length = AudioManager.Instance.PlaySound(voicePath).Length;
            StartCoroutine(TimeUtility.DelayInvoke(length, () =>
            {
                SuperMarketGame.Instance.SetLediAnim("TalkEnd");
                isPlayingLdAniSound = false ;
                if (act != null)
                    act();
            }
            ));
        }));
    }

    public void PlaySmallGameSound(GameType type)
    {
        int r = Random.Range(0, 2);
        if (SuperMarketGame.Instance.gameCount > 0)
            r = 0;
        if (r == 0)
        {
            switch (type)
            {
                case GameType.Cleanliness:
                    if (SuperMarketGame.Instance.gameCount > 0)
                        PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050203);
                    else
                        PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050103);
                    break;
                case GameType.PlaceGoods:
                    if (SuperMarketGame.Instance.gameCount > 0)
                        PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050205);
                    else
                        PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050104);
                    break;
                case GameType.ShoppingCart:
                    if (SuperMarketGame.Instance.gameCount > 0)
                        PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050204);
                    else
                        PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050105);
                    break;
            }
        }
        else
        {
            PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050106);
        }
    }

    public void PlayRoleSound(RoleSoundType type)
    {
        //SuperMarketGame.Instance.SetRoleAnim(10);
        switch (SuperMarketGame.Instance.CurRoleType)
        {
            case RoleType.ajiasi:
                switch (type)
                {
                    case RoleSoundType.Bye:
                        PlaySoud(SuperMarketResPath.Sound_404050206);
                        break;
                    case RoleSoundType.LediBye:
                        string soundPath = SuperMarketResPath.Sound_404050429;
                        int r = Random.Range(0, 3);
                        if(r==1)
                            soundPath = SuperMarketResPath.Sound_404050206;
                        else if(r==2)
                            soundPath = SuperMarketResPath.Sound_404050207;
                        PlaySoud(soundPath);
                        break;
                    case RoleSoundType.IWantBuy:
                        PlaySoud(SuperMarketResPath.Sound_404050208);
                        break;
                    case RoleSoundType.No:
                        PlaySoud(SuperMarketResPath.Sound_404050209);
                        break;
                    default:
                        break;
                }
                break;
            case RoleType.aisha:
                switch (type)
                {
                    case RoleSoundType.Bye:
                        PlaySoud(SuperMarketResPath.Sound_404050210);
                        break;
                    case RoleSoundType.LediBye:
                        //10  11 
                        string soundPath = SuperMarketResPath.Sound_404050428;
                        int r = Random.Range(0, 3);
                        if (r == 1)
                            soundPath = SuperMarketResPath.Sound_404050210;
                        else if (r == 2)
                            soundPath = SuperMarketResPath.Sound_404050211;
                        PlaySoud(soundPath);
                        break;
                    case RoleSoundType.IWantBuy:
                        PlaySoud(SuperMarketResPath.Sound_404050212);
                        break;
                    case RoleSoundType.No:
                        PlaySoud(SuperMarketResPath.Sound_404050213);
                        break;
                    default:
                        break;
                }
                break;
            case RoleType.nuowa:
                switch (type)
                {
                    case RoleSoundType.Bye:
                        PlaySoud(SuperMarketResPath.Sound_404050215);
                        break;
                    case RoleSoundType.LediBye:
                        string soundPath = Random.Range(0, 2) == 0 ? SuperMarketResPath.Sound_404050214 : SuperMarketResPath.Sound_404050215;
                        PlaySoud(soundPath);
                        break;
                    case RoleSoundType.IWantBuy:
                        PlaySoud(SuperMarketResPath.Sound_404050216);
                        break;
                    case RoleSoundType.No:
                        PlaySoud(SuperMarketResPath.Sound_404050217);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}

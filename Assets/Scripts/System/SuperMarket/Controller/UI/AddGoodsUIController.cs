﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
/// <summary>
/// 补货Ctrl
/// </summary>
public class AddGoodsUIController : Singleton<AddGoodsUIController>
{

    private AddGoodsUIView _view;
    Image[] image;
    public override void Init()
    {
        _view = new AddGoodsUIView();
    }

    public void CloseView()
    {
        RemoveEvent();
        _view.CloseForm();
    }
    public void OpenView()
    {
        if (_view.m_Form == null || !_view.m_Form.gameObject.activeSelf)
        {
            _view.OpenForm();
        }
        RegisterEvent();
        image = new Image[3]{ _view.box1_img,_view.box2_img,_view.box3_img };
    }
    /// <summary>
    /// 刷新UI
    /// </summary>
    /// <param name="name">atlas名</param>
    /// <param name="index">索引</param>
    public void Refresh(string name, int index)
    {
        //Debug.LogError("name == "+name + "  ,  index == " + index);
        image[index].sprite = CommonUtility.GetSprite(emUIAltas.SuperMarketAtlas, name);
    }
    private void RegisterEvent()
    {
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Return_Btn, OnReturnClick);
    }
    private void RemoveEvent()
    {
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Return_Btn, OnReturnClick);
    }
    void OnReturnClick(HUIEvent e)
    {
        //SuperMarketGame.Instance.IsCheckShopPanel = false;
        TimerManager.Instance.RemoveAllTimer();
        //Debug.LogError("  OnReturnClick == ");
        Input.multiTouchEnabled = true;
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }

    public void ShakeUI(float strength)
    {
        int random = Random.Range(0, 3);
        Transform trans = image[random].transform.parent;
        trans.DOPunchPosition(new Vector3(1, 1, 0) * strength, 1.8f).SetAutoKill();
    }

    public void CloseHand()
    {
        _view.HandClick.gameObject.SetActive(false);
        _view.HandClick.DOKill();
    }

    public void ShowHand(int fromIndex, Transform toIndex)
    {
        _view.HandClick.SetParent(_view.box_imgs[fromIndex]);
        _view.HandClick.localPosition = new Vector3(10f, -50f, -150f);

        _view.HandClick.gameObject.SetActive(true);
        _view.HandClick.localEulerAngles = new Vector3(45f, 180f, 0f);
        _view.HandClick.localScale = new Vector3(3, 3, 3);

        //_view.HandClick
        //Vector3 screenPos = SuperMarketGame.Instance.superCamera.WorldToScreenPoint(toIndex.position);
        //Vector2 pos;
        //RectTransformUtility.ScreenPointToLocalPointInRectangle(_view.m_Form.GetComponent<RectTransform>(), screenPos, _view.m_Form.GetComponent<Canvas>().worldCamera, out pos);
        //Vector3 worldPos = _view.HandClick.TransformPoint(new Vector3(pos.x, pos.y, -150f));
        Vector3 off = SuperMarketGame.Instance.superCamera.transform.position - toIndex.position;
        Vector3 dragPos = toIndex.position + off * 0.3f;
        Vector3 screenPos = Camera.main.WorldToScreenPoint(dragPos);

        Vector3 worldPos = _view.m_Form.GetComponent<Canvas>().worldCamera.ScreenToWorldPoint(screenPos);

        _view.HandClick.DOMove(worldPos, 1.5f).SetEase(Ease.Linear).SetLoops<Tweener>(-1, LoopType.Restart);
    }
}

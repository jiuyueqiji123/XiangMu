﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 计价器UI
/// </summary>
public class ScanPriceController : Singleton<ScanPriceController>
{
    private ScanPriceView _view;

    public override void Init()
    {
        _view = new ScanPriceView();
    }

    public void CloseView()
    {
        _view.CloseForm();
        RemoveEvent();
    }
    public void OpenView()
    {
        if (_view.m_Form == null || !_view.m_Form.gameObject.activeSelf)
        {
            _view.OpenForm();
            RegisterEvent();
        }

        Refresh(0);
    }


    public void ViewSet(bool active)
    {
        if(_view.m_Form != null)
            _view.m_Form.gameObject.SetActive(active);        
    }
    public void Refresh(float price)
    {
        if (price == 0)
        {
            _view.ten_img.sprite = CommonUtility.GetSprite(emUIAltas.SuperMarketAtlas, "cs_sz0");
            _view.one_img.sprite = CommonUtility.GetSprite(emUIAltas.SuperMarketAtlas, "cs_sz0");
            _view.remain_img.sprite = CommonUtility.GetSprite(emUIAltas.SuperMarketAtlas, "cs_sz0");
        }
        else
        {
            Dictionary<MoneyType, int> list = SuperMarketTool.CalNum(price);
            //Debug.Log(price + " === price");
            _view.ten_img.sprite = CommonUtility.GetSprite(emUIAltas.SuperMarketAtlas, "cs_sz" + list[MoneyType.TenDigits]);
            _view.one_img.sprite = CommonUtility.GetSprite(emUIAltas.SuperMarketAtlas, "cs_sz" + list[MoneyType.Digits]);
            _view.remain_img.sprite = CommonUtility.GetSprite(emUIAltas.SuperMarketAtlas, "cs_sz" + list[MoneyType.Remain]);
        }
    }

    private void RegisterEvent()
    {
       
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.ReturnMain_Btn_Click, OnReturnClick);
    }

    private void RemoveEvent()
    {       
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.ReturnMain_Btn_Click, OnReturnClick);
    }
    void OnReturnClick(HUIEvent e)
    {
        Debug.Log(" ScanPriceController OnReturnClick == ");
        Input.multiTouchEnabled = true;
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }
}

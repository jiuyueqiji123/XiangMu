﻿using UnityEngine;

public class SuperMarketController : Singleton<SuperMarketController>
{
    private const string _VIEW_PATH = CakeShopResPath.VIEW_DIR + "ReturnForm";
    private ReturnView _view;

    public override void Init()
    {
        base.Init();
        _view = new ReturnView();
    }

    public override void UnInit()
    {
        base.UnInit();
    }

    public void OpenView()
    {
        _view.OpenForm(_VIEW_PATH);
        RegisterEvent();
    }

    public void CloseView()
    {
        _view.CloseForm();
        RemoveEvent();
        //SuperMarketGame.Instance.OnExit();
        SuperMarketGame.DestroyInstance();
    }

    private void RegisterEvent()
    {
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.CakeShop_Btn_Back, ClickBtnBack);
    }

    private void RemoveEvent()
    {
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.CakeShop_Btn_Back, ClickBtnBack);
    }

    private void ClickBtnBack(HUIEvent e)
    {
        Debug.Log("  ClickBtnBack == ");
        RemoveEvent();
        Input.multiTouchEnabled = true;
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);        
    }
}
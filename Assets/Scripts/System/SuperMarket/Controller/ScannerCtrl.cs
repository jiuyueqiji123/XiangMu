﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 扫码器功能
/// </summary>
public class ScannerCtrl : MonoBehaviour, IInputDragHandler, IInputBeginDragHandler, IInputEndDragHandler
{
    Vector3 screenPos;
    Vector3 offsetPos;
    float limitMaxX = -5f;
    float limitMaxY = 0.42f;
    float limitMinX = -6.4f;
    float limitMinY = -0.28f;
    float limitZ = 1.2f;

    //发射射线位置
    Transform rayObject;
    //射线旋转度数
    public float rayDegree = 0;

    private bool isScan = false;
    public bool IsScan
    {
        get;set;
    }

   

    //扫描的条形码
    public Stack<Transform> codeTransforms = new Stack<Transform>();
    //总价格
    private float totalPrice = 0;
    public float TotalPrice
    {
        get;
        set;
    }
    Vector3 orgPos;
    void Awake()
    {
        rayObject = transform.Find("rayObject");
        orgPos = transform.position;
    }
    public void AddBarCode(Transform code)
    {
        codeTransforms.Push(code);
    }

    public void SetColliderEnable(bool Enable)
    {
        codeTransforms.Peek().GetComponent<BoxCollider>().enabled = true;
    }
    public void OnInputBeginDrag(InputEventData eventData)
    {
        ScanCodeController.Instance.EnableRayEffect(true);
        ScanCodeController.Instance.totalCount = 0;
        screenPos = Camera.main.WorldToScreenPoint(transform.position);
        offsetPos = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPos.z));

        MarketMainSceneCtrl.Instance.handClick.gameObject.SetActive(false);
    }

    public void OnInputDrag(InputEventData eventData)
    {
        ScanCodeController.Instance.totalCount = 0;
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPos.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offsetPos;
        // 边界值-5  0.42  1.2    
        float x = curPosition.x > limitMaxX ? limitMaxX : curPosition.x;
        float y = curPosition.y > limitMaxY ? limitMaxY : curPosition.y;
        x = x < limitMinX ? limitMinX : x;
        y = y < limitMinY ? limitMinY : y;
        float z = limitZ;//curPosition.z < limitZ ? limitZ : curPosition.z;
        transform.position = new Vector3(x, y, z);

        if (IsScan && codeTransforms != null && codeTransforms.Count > 0 && !ScanCodeController.Instance.GetTransIsMove())
            ScanCode(codeTransforms.Peek());
    }

    public void OnInputEndDrag(InputEventData eventData)
    {
        ScanCodeController.Instance.EnableRayEffect(false);
        ScanCodeController.Instance.totalCount = 0;
        transform.position = orgPos;
    }
    /// <summary>
    /// 扫码
    /// </summary>
    void ScanCode(Transform scanTrans)
    {
        Vector3 fwd = transform.TransformDirection(transform.forward );//* rayDegree
        //Vector3 fwd = scanTrans.position - rayObject.position;
        RaycastHit hitinfo;
        Debug.DrawLine(rayObject.position, fwd * 10, Color.red); //(scanTrans.position - rayObject.position)
        if (Physics.Raycast(rayObject.position, fwd, out hitinfo, 10)) //scanTrans.position - rayObject.position
        {
            //6.29  91.3  -0.3
            //Debug.Log("   === " + hitinfo.collider.name);
            if (hitinfo.collider.name.Contains(scanTrans.name))
            {
                ScanCodeController.Instance.PlayCodeEffect();
                MarketMainSceneCtrl.Instance.PlayStarEffect(scanTrans.parent.position,1f);
                //Debug.Log("  扫到了码  === " + hitinfo.collider.name);
                SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_603010112);
                //Todo  1 堆栈移除 , 2 禁用 ， 3 移动物品， 4 UI显示
                //1
                codeTransforms.Pop();
                //2
                scanTrans.GetComponent<BoxCollider>().enabled = false;
                IsScan = false;
                //4     ====== mjbTest   
                float tempPrice = SuperMarketGame.Instance.TotalPrice(scanTrans.parent.name);
                TotalPrice += tempPrice;
                ////Debug.Log(TotalPrice + " === price , name  == " + scanTrans.name);
                ScanPriceController.Instance.Refresh(TotalPrice);
                //3 掉落到非扫码区
                ScanCodeController.Instance.ScanFallDown(scanTrans, codeTransforms.Count,()=> {
                    if (codeTransforms.Count == 0)
                    {
                        //结束扫码并转场到找零钱 
                        SuperMarketGame.Instance.SwitchSettlement(TotalPrice);
                    }
                });
                //剩下的往前移动
                Transform[] transArray = new Transform[codeTransforms.Count];
                if (codeTransforms.Count > 0)
                {
                    int index = codeTransforms.Count - 1;
                    foreach (Transform item in codeTransforms)
                    {
                        transArray[index] = item.parent;
                        index--;
                    }
                }
                if (transArray.Length > 0)
                {
                    ScanCodeController.Instance.GoodsMoving(transArray, 0, 1f, () =>
                    {
                        //Debug.Log(" codeTransforms == Complete ");
                        codeTransforms.Peek().GetComponent<BoxCollider>().enabled = true;
                        IsScan = true;
                    });
                }                    
            }
        }
    }
    

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}

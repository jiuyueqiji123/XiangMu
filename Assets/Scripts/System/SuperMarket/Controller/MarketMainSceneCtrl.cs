﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
/// <summary>
/// 主场景管理器
/// </summary>
public class MarketMainSceneCtrl : MonoSingleton<MarketMainSceneCtrl>
{
    Transform leftDoor;
    Transform rightDoor;
    Transform shopCar;

    //是否能 开门  鼠标移动门
    public bool isCanMove = true;
    //购物车震动
    public bool isShakeCar = false;
    float interval = 6f;
    public float timeCount = 0;

    public float strength = 0.1f;

    GameObject successEffect;

    public Transform handClick;

    Vector3 handOrgPos;

    Quaternion handOrgRot;

    Vector3 handOrgScale;

    GameObject tx_502004083;
    protected override void Awake()
    {
        leftDoor = transform.Find("chaoshi/304050106_L");
        rightDoor = transform.Find("chaoshi/304050106_R");
        shopCar = transform.Find("chaoshi/304050105");
        successEffect = transform.Find("effect").gameObject;
        handClick = transform.Find("Hand_Click");
        handOrgPos = handClick.localPosition;
        handOrgRot = handClick.localRotation;
        handOrgScale = handClick.localScale;
    }
    void Start()
    {

    }
    public bool isClickUI = false;
    private void Update()
    {
         
        //不能开门 不能震动车 就不检测
        //if (!isCanMove && !isShakeCar) return;
        if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            //检测是否触摸UI  给ShopItemDragCtrl里拖拽检测使用
            {
#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID)
                if (UnityEngine.EventSystems.EventSystem.current!=null && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
#else
                if (UnityEngine.EventSystems.EventSystem.current!=null && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
#endif
                {
                    //Debug.Log(" ShopItemDragCtrl  当前触摸在UI上");
                    isClickUI = true;
                }
                else
                {
                    //Debug.Log(" ShopItemDragCtrl  当前没有触摸在UI上");
                    isClickUI = false;

                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        /*if (hit.collider.name.Contains("304050106") && isCanMove)
                        {
                            //检测门
                            SuperMarketGame.Instance.SetLediAnim("sayhello1");
                            OpenTheDoor(RoleInSuperMarketCtrl.Instance.WalkInSide);
                            isCanMove = false;
                        }
                        else*/
                        if (hit.collider.name.Contains("304050105") && isShakeCar)
                        {
                            //点击购物车
                            isShakeCar = false;
                            handClick.gameObject.SetActive(false);
                            handClick.localPosition = handOrgPos;
                            handClick.DOKill();
                            AudioManager.Instance.StopSound(AudioManager.Instance.GetAudioClip(SuperMarketResPath.Sound_404050109));
                            SuperMarketGame.Instance.SetLediAnim("idle");
                            //显示角色身边的购物车  
                            SuperMarketGame.Instance.ShowShopCar();
                            //Debug.Log(" === ChooseShopCar === ");
                        }
                        else if (hit.collider.name.Contains("304050202"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050410);
                        }
                        else if (hit.collider.name.Contains("304050207"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050412);
                        }
                        //暂时没有水果的
                        else if (hit.collider.name.Contains("304050101_haibao"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050414);
                        }
                        else if (hit.collider.name.Contains("304050203"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050409);
                        }
                        else if (hit.collider.name.Contains("304050205"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050411);
                        }
                        else if (hit.collider.name.Contains("304050208"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050413);
                        }
                        else if (hit.collider.name.Contains("304050215"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050342);
                        }
                        else if (hit.collider.name.Contains("304050212"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050344);
                        }
                        else if (hit.collider.name.Contains("304050218"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050346);
                        }
                        else if (hit.collider.name.Contains("304050216"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050343);
                        }
                        else if (hit.collider.name.Contains("304050213"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050345);
                        }
                        else if (hit.collider.name.Contains("304050219"))
                        {
                            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050347);
                        }
                    }
                }
            }
            

        }
        if (isShakeCar)
        {
            timeCount += Time.deltaTime;
            if (timeCount >= interval)
            {
                //SuperMarketGame.Instance.SetLediAnim("sayhello1");
                //Invoke("PlaySoundCar", 0.5f);
                //SuperMarketGame.Instance.soundCtrl.PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050109);
                shopCar.DOPunchPosition(new Vector3(1, 0, 1) * strength, 1.8f).SetAutoKill();
                handClick.gameObject.SetActive(true);
                PlayCarHandTween();
                StartCoroutine(TimeUtility.DelayInvoke(2f, () =>
                {
                    handClick.gameObject.SetActive(false);
                }));
                timeCount = 0;
            }

        }
    }
    void PlaySoundCar()
    {
        SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050109);
    }
    void PlayCarHandTween()
    {
        handClick.localPosition = handOrgPos;//new Vector3(-12.82f, 1.147f, 1.502f);
        handClick.localRotation = handOrgRot;
        handClick.localScale = handOrgScale;
        handClick.DOMoveY(handClick.position.y + 0.2f, 0.5f).SetEase(Ease.Linear).SetLoops<Tweener>(-1, LoopType.Yoyo).OnComplete(() =>
        {
            if (!isShakeCar)
                handClick.DOKill();
        });
    }

    public void PlayBuyAreaHandTween(Vector3 pos, Vector3? scale = null)
    {        
        handClick.gameObject.SetActive(true);
        handClick.localRotation = Quaternion.Euler(new Vector3(-90f, 0, 0));
        handClick.localPosition = pos;
        if (scale != null)
            handClick.localScale = (Vector3)scale;
        else
            handClick.localScale = new Vector3(0.008f, 0.008f, 0.008f);

        handClick.DOMoveY(handClick.position.y + 0.2f, 0.5f).SetEase(Ease.Linear).SetLoops<Tweener>(-1, LoopType.Yoyo).OnComplete(() =>
        {             
        });
    }

    public void PlayHorTween(Vector3 startPos, Vector3 endPos, Vector3 rotation, Vector3? scale = null)
    {
        handClick.gameObject.SetActive(true);
        handClick.localRotation = Quaternion.Euler(rotation);
        handClick.localPosition = startPos;
        if (scale != null)
            handClick.localScale = (Vector3)scale;
        else
            handClick.localScale = new Vector3(0.008f, 0.008f, 0.008f);

        handClick.DOMove(endPos, 2f).SetEase(Ease.Linear).SetLoops<Tweener>(-1, LoopType.Restart).OnComplete(() =>
        {
        });
    }

    public void ShakeShopCar()
    {
        shopCar.DOPunchPosition(new Vector3(1, 0, 1) * strength, 1.8f).SetAutoKill();
    }

    public void EnableShopCar(bool isActive)
    {
        shopCar.SetActive(isActive);
    }

    public void PlayStarEffect(Vector3 position, float startSize = 1.2f)
    {
        if (tx_502004083 == null)
        {
            tx_502004083 = Instantiate(ResourceManager.Instance.GetResource("Effect/502004083", typeof(GameObject), enResourceType.Prefab, false, false).m_content as GameObject);
            //tx_502004083.transform.SetParent(parent);
            tx_502004083.transform.position = position;
            //var main = ps.main;
            //main.startSize = startSize;
            tx_502004083.transform.GetChild(0).localScale = new Vector3(startSize, startSize, startSize);
            tx_502004083.SetActive(true);
            /*StartCoroutine(TimeUtility.DelayInvoke(1f, () =>
            {
                tx_502004083.SetActive(false);
            }));*/
            ParticleSystem ps = tx_502004083.transform.GetChild(0).GetComponent<ParticleSystem>();
            if (!ps.isPlaying)
                ps.Play();
        }
        else
        {
            tx_502004083.transform.GetChild(0).localScale = new Vector3(startSize, startSize, startSize);
            tx_502004083.transform.position = position;
            tx_502004083.SetActive(true);
            ParticleSystem ps = tx_502004083.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
            if (!ps.isPlaying)
                ps.Play();
            /*StartCoroutine(TimeUtility.DelayInvoke(1f, () =>
            {
                tx_502004083.SetActive(false);
            }));*/
        }
    }

    public void OpenTheDoor(TweenCallback callback)
    {
        leftDoor.DOLocalMoveX(0.795f, 0.4f).OnComplete(callback);
        rightDoor.DOLocalMoveX(-0.795f, 0.4f);
    }

    public void CloseTheDoor()
    {
        leftDoor.DOLocalMoveX(0, 0.4f);
        rightDoor.DOLocalMoveX(0, 0.4f);
    }

    public void PlaySuccessEffect(bool isActive, Vector3 pos)
    {
        //successEffect.transform.position = pos;
        pos = successEffect.transform.InverseTransformVector(pos);
        successEffect.transform.localPosition = new Vector3(pos.x, successEffect.transform.localPosition.y, pos.z);
        successEffect.SetActive(isActive);
    }

}

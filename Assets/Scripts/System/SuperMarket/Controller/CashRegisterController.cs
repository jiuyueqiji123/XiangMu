﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
/// <summary>
/// 收银台功能
/// </summary>
public class CashRegisterController : MonoSingleton<CashRegisterController>
{
    //价钱每一个位数值
    private Dictionary<MoneyType, int> moneyDic;
    //组合的价钱
    List<DigitsType> moneyList;
    //存储gameobject
    List<GameObject> moneyObjectList;
    //初始化钱的父级point
    Transform moneyPoint;


    private Transform tenPos;
    private Transform fivePos;
    private Transform twoPos;
    private Transform onePos;
    private Transform wmPos;

    List<Vector3> targetPosList;//= new List<Vector3>();

    protected override void Awake()
    {
        moneyPoint = transform.Find("moneyPoint");

        tenPos = transform.Find("tenPos");
        fivePos = transform.Find("fivePos");
        twoPos = transform.Find("twoPos");
        onePos = transform.Find("onePos");
        wmPos = transform.Find("wmPos");
        //==== mjbTest
        //Init(36.5f);
    }
    float interval = 10f;
    public float totalCount = 0f;
    public bool isGuid = false;
    public float offeSetY;
    public float offeSetZ = 0.07f;
    public float offesetR;
    Vector3 startPos;
    Vector3 endPos;
    private void Update()
    {
        if (isGuid)
        {
            totalCount += Time.deltaTime;
            if (totalCount >= interval)
            {
                MarketMainSceneCtrl.Instance.PlayHorTween(new Vector3(startPos.x, startPos.y, startPos.z+ offeSetZ), new Vector3(endPos.x, endPos.y, endPos.z + offeSetZ), new Vector3(offesetR, 0, 0), new Vector3(0.002f, 0.002f, 0.002f));
                StartCoroutine(TimeUtility.DelayInvoke(6f, () =>
                {
                    MarketMainSceneCtrl.Instance.handClick.gameObject.SetActive(false);
                    MarketMainSceneCtrl.Instance.handClick.DOKill();
                }));
                totalCount = 0;
            }
        }

    }
    public void Init(float _price)
    {
        //TODO   放置人民币
        if (_price > 0)
        {
            moneyDic = SuperMarketTool.CalNum(_price);
            moneyList = new List<DigitsType>();
            moneyObjectList = new List<GameObject>();

            targetPosList = new List<Vector3>();

            //通过每个位置上数值排列组合金币
            foreach (MoneyType item in moneyDic.Keys)
            {
                if (moneyDic[item] > 0)
                {
                    switch (item)
                    {
                        case MoneyType.Remain:
                            moneyList.Add(DigitsType.Remain);
                            break;
                        case MoneyType.Digits:
                            int num = moneyDic[item];
                            //Debug.Log(num +" === num ");
                            SuperMarketTool.AnalyDigits(moneyList, num);
                            break;
                        case MoneyType.TenDigits:
                            int count = moneyDic[item];
                            //Debug.Log(count + " === num ");
                            SuperMarketTool.AnalyTenDigits(moneyList, count);
                            break;
                        default:
                            break;
                    }
                }
            }
            //初始化金币
            int itemIndex = 0;
            foreach (DigitsType item in moneyList)
            {
                //Debug.Log((int)item + "  ==== ");
                //边界值 x 0.126  -0.13  y 0.63 - 0.66   z 0  -  0.1
                //float x = 0, y = 0.63f, z = 0f;
                GameObject money = null;
                switch (item)
                {
                    case DigitsType.Remain:
                        money = GameObject.Instantiate(CommonUtility.LoadPrefab(SuperMarketResPath.FiveMao));
                        break;
                    case DigitsType.One:
                        money = GameObject.Instantiate(CommonUtility.LoadPrefab(SuperMarketResPath.OneYuan));
                        break;
                    case DigitsType.Two:
                        money = GameObject.Instantiate(CommonUtility.LoadPrefab(SuperMarketResPath.TwoYuan));
                        break;
                    case DigitsType.Five:
                        money = GameObject.Instantiate(CommonUtility.LoadPrefab(SuperMarketResPath.FiveYuan));
                        break;
                    case DigitsType.Ten:
                        money = GameObject.Instantiate(CommonUtility.LoadPrefab(SuperMarketResPath.TenYuan));
                        break;
                    default:
                        break;
                }
                if (money != null)
                {
                    Vector3 targetPos = new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(0.63f, 0.68f), Random.Range(0f, 0.085f));
                    while (GetTargetPos(targetPos))
                    {
                        targetPos = new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(0.63f, 0.68f), Random.Range(0f, 0.085f));
                    }
                    money.transform.SetParent(moneyPoint);
                    money.transform.localPosition = targetPos;
                    targetPosList.Add(targetPos);
                    money.transform.localRotation = Quaternion.Euler(new Vector3(0, 90, 0));
                    DragMoneyCtrl dm = money.GetComponent<DragMoneyCtrl>();
                    if (dm == null)
                        dm = money.AddComponent<DragMoneyCtrl>();

                    dm.MoneyValue = item;
                    dm.InitColliderPos(tenPos, fivePos, twoPos, onePos, wmPos);
                    moneyObjectList.Add(money);

                    if (itemIndex == moneyList.Count - 1)
                    {
                        //添加引导
                        startPos = FindHightTarget();
                        //Vector3 off = Camera.main.transform.position - targetPos;
                        //Vector3 dragPos = targetPos + off * 0.3f;
                        //startPos = dragPos;
                        Transform guidTrans = fivePos;
                        switch (item)
                        {
                            case DigitsType.Remain:
                                guidTrans = wmPos;
                                break;
                            case DigitsType.One:
                                guidTrans = onePos;
                                break;
                            case DigitsType.Two:
                                guidTrans = twoPos;
                                break;
                            case DigitsType.Five:
                                guidTrans = fivePos;
                                break;
                            case DigitsType.Ten:
                                guidTrans = tenPos;
                                break;
                        }
                        //Vector3 endPosScreen = Camera.main.WorldToScreenPoint(guidTrans.position);
                        float height = guidTrans.GetComponent<BoxCollider>().size.y * 0.2f;
                        endPos = new Vector3(guidTrans.position.x, startPos.y, guidTrans.position.z);//guidTrans.position + guidTrans.up * height;
                        Debug.Log(" start Pos == " + startPos + "   ,  end Pos === " + endPos);
                            //new Vector3(guidTrans.position.x, guidTrans.position.y, guidTrans.position.z);
                    }
                    itemIndex++;
                }
            }
            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_601010109);
            moneyPoint.localPosition = new Vector3(0, 0, -0.5f);
            moneyPoint.DOLocalMoveZ(0, 0.3f).OnComplete(() =>
            {
                isGuid = true;
            });
        }
    }

    Vector3 FindHightTarget()
    {
        float maxY = 0.63f;
        Vector3 tarPos = targetPosList[0];
        foreach (var item in targetPosList)
        {
            float tempVec = item.y;
            //Debug.Log(tempVec + "  ==== tempVec");
            if (tempVec > maxY)
            {
                maxY = tempVec;
                tarPos = item;
                //Debug.Log("  111== ");
            }
        }
        return tarPos;
    }
    /// <summary>
    /// 检测模型是否太近  免得穿帮
    /// </summary>
    /// <param name="checkPos"></param>
    /// <returns></returns>
    bool GetTargetPos(Vector3 checkPos)
    {
        bool isNear = false;
        if (targetPosList != null && targetPosList.Count > 0)
        {
            for (int i = 0; i < targetPosList.Count; i++)
            {
                if (Vector3.Distance(checkPos, targetPosList[i]) < 0.05f)
                {
                    isNear = true;
                    break;
                }
            }
        }
        return isNear;
    }
    /// <summary>
    /// 检测是否整理完成
    /// </summary>
    /// <returns></returns>
    public bool CheckArrange()
    {
        if (moneyPoint.childCount > 0)
            return false;
        else
            return true;
    }

    /// <summary>
    /// 整理完成
    /// </summary>
    public void FinishArrange()
    {
        //TODO  1, 切换到扫码台   2， 重置初始化状态
        Debug.Log("  FinishArrange  ==== ");
        isGuid = false;
        SuperMarketGame.Instance.SwitchScanFromCash();
        OnReset();
    }

    /// <summary>
    /// 重置游戏
    /// </summary>
    public void OnReset()
    {
        //TODO   删除所有moneyObjectList
        if (moneyObjectList != null && moneyObjectList.Count > 0)
        {
            foreach (GameObject item in moneyObjectList)
            {
                Destroy(item);
            }
        }
        gameObject.SetActive(false);
    }
}

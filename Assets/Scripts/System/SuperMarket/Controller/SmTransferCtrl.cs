﻿using UnityEngine;
using System.Collections;

public class SmTransferCtrl : MonoBehaviour
{
    [HideInInspector]
    public float speed = 0.4f;
    float offsetX ;
    Material mat;
    public bool isMove = false;
    // Use this for initialization
    void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (isMove)
        {
            offsetX = offsetX - Time.deltaTime * speed;
            mat.mainTextureOffset = new Vector2(offsetX, 0);
            mat.SetTextureOffset("_MainTex", new Vector2(offsetX, 0));
        }
    }
}

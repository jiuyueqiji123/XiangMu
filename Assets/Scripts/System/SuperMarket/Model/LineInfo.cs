﻿using System.Collections;

public class LineInfo
{
    /// <summary>
    /// 质量
    /// </summary>
    public float mass;
    /// <summary>
    /// 阻力
    /// </summary>
    public float drag;
    /// <summary>
    /// 弹性
    /// </summary>
    public float spring;
    /// <summary>
    /// 弹性阻力
    /// </summary>
    public float damper;
    /// <summary>
    /// 弹性相对于父节点最远距离
    /// </summary>
    public float maxDistance;
    /// <summary>
    /// 是否启用刚体碰撞检测
    /// </summary>
    public bool detectCollisions;
    /// <summary>
    /// 是否使用动力学移动
    /// </summary>
    public bool isKinematic;
    /// <summary>
    /// 是否使用重力
    /// </summary>
    public bool useGravity;

    /// <summary>
    /// 获得线的信息
    /// </summary>
    /// <param name="mass">质量</param>
    /// <param name="drag">阻力</param>
    /// <param name="spring">弹性</param>
    /// <param name="damper">弹性阻力</param>
    /// <param name="maxDistance">弹性相对于父节点最远距离</param>
    /// <param name="detectCollisions">是否启用刚体碰撞检测</param>
    /// <param name="isKinematic">是否使用动力学移动</param>
    /// <param name="useGravity">是否使用重力</param>
    public LineInfo(float mass, float drag, float spring, float damper, float maxDistance, bool detectCollisions, bool isKinematic, bool useGravity)
    {        
        this.mass = mass;
        this.drag = drag;
        this.spring = spring;
        this.detectCollisions = detectCollisions;
        this.isKinematic = isKinematic;
        this.useGravity = useGravity;
        this.damper = damper;
        this.maxDistance = maxDistance;
    }

    static LineInfo defaultInfo;
    public static LineInfo DefaultInfo
    {
        get
        {
            if (defaultInfo == null)
            {
                defaultInfo = new LineInfo(0.003f, 10, 3, 0.4f, 0.05f, false, false, true);
            }
            return defaultInfo;
        }
    }

}

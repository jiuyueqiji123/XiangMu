﻿using System.Collections;
using System.Collections.Generic;
using TableProto;

public class GoodsInfoModel :Singleton<GoodsInfoModel> 
{
    private Dictionary<string, SuperMarketGoodsInfo> itemInfoDic = new Dictionary<string, SuperMarketGoodsInfo>();

    private Dictionary<GoodsType, List<string>> goodsTypeDic = new Dictionary<GoodsType, List<string>>();


    public override void Init()
    {
        //===================== mjbTest
        //TableProtoLoader.Load();
        //=====================
        foreach (var smgi in TableProtoLoader.SuperMarketGoodsInfoDict.Values)
        {
            itemInfoDic.Add(smgi.resid, smgi as SuperMarketGoodsInfo);            

            GoodsType type = (GoodsType)(smgi.type);
            if (!goodsTypeDic.ContainsKey(type))
            {
                goodsTypeDic.Add(type, new List<string>());
            }
            goodsTypeDic[type].Add(smgi.resid);
        }
    }

    public List<string> GetItems(GoodsType type)
    {
        List<string> rets = new List<string>();
        goodsTypeDic.TryGetValue(type, out rets);
        return rets;
    }

    public SuperMarketGoodsInfo GetModelByResId(string resid)
    {
        SuperMarketGoodsInfo smgi = new SuperMarketGoodsInfo();
        itemInfoDic.TryGetValue(resid, out smgi);
        return smgi;
    }
}

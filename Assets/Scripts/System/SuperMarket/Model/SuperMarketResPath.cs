﻿using System;

public sealed class SuperMarketResPath
{
    //Dir
    public const string COMMON_SOUND = "sound/";

    public const string ROOT = "super_market/";

    public const string PREFAB_DIR = ROOT + "main_prefab/";

    public const string MONEY_DIR = ROOT + "money_prefab/";

    public const string SOUND = ROOT + "game_sound/";

    public const string SHPOLIST_DIR = ROOT + "ui_prefab/ShoppingListPanel";

    public const string SCANPRICE_DIR = ROOT + "ui_prefab/ScanPricePanel";

    public const string ADDGOODSUI_DIR = ROOT + "ui_prefab/AddGoodsPanel";

    //Full file path
    public const string SceneObject = PREFAB_DIR + "mainscene";

    public const string ShoopingCar = PREFAB_DIR + "shoppingcar";

    public const string CleanObject = PREFAB_DIR + "clean_game";

    public const string ShopCarObject = PREFAB_DIR + "shopcar_game";

    public const string AddGoodsObject = PREFAB_DIR + "addgoods_game";

    public const string LediObject = "model/model_actor/ledi";

    public const string LediAnimObject = ROOT + "super_market_controller/LediInSuperMarket";

    public const string Vegetables_Dir = ROOT + "vegetables_prefab/group";

    public const string FruitUp_Dir = ROOT + "fruit_prefab/group1";

    public const string FruitDown_Dir = ROOT + "fruit_prefab/group2";

    public const string Snacks_Dir = ROOT + "snacks_prefab/group";

    public const string ShaoMaObject = PREFAB_DIR + "shaoma";

    public const string BarCodeObject = PREFAB_DIR + "barcode";

    public const string TicketObject = PREFAB_DIR + "ticket";

    public const string ShouYinObject = PREFAB_DIR + "shouyin";

    public const string TenYuan = MONEY_DIR + "tenyuan";

    public const string FiveYuan = MONEY_DIR + "fiveyuan";

    public const string TwoYuan = MONEY_DIR + "twoyuan";

    public const string OneYuan = MONEY_DIR + "oneyuan";

    public const string FiveMao = MONEY_DIR + "fivemao";


    ///////soud======================================
    public const string Sound_601010106 = SOUND + "601010106";
    public const string Sound_601010107 = SOUND + "601010107";
    public const string Sound_601010109 = SOUND + "601010109";
    public const string Sound_601010111 = SOUND + "601010111";
    public const string Sound_601010403 = SOUND + "601010403";
    public const string Sound_602020502 = SOUND + "602020502";
    public const string Sound_603010403 = SOUND + "603010403";
    public const string Sound_603010706 = SOUND + "603010706";
    public const string Sound_603010707 = SOUND + "603010707";
    public const string Sound_603010902 = SOUND + "603010902";
    public const string Sound_603020503 = SOUND + "603020503";
    public const string Sound_604010402 = SOUND + "604010402";

    public const string Sound_404050101 = SOUND + "404050101";
    public const string Sound_404050102 = SOUND + "404050102";
    public const string Sound_404050103 = SOUND + "404050103";
    public const string Sound_404050104 = SOUND + "404050104";
    public const string Sound_404050105 = SOUND + "404050105";
    public const string Sound_404050106 = SOUND + "404050106";
    public const string Sound_404050107 = SOUND + "404050107";
    public const string Sound_404050108 = SOUND + "404050108";
    public const string Sound_404050109 = SOUND + "404050109";
    public const string Sound_404050201 = SOUND + "404050201";
    public const string Sound_404050202 = SOUND + "404050202";
    public const string Sound_404050203 = SOUND + "404050203";
    public const string Sound_404050204 = SOUND + "404050204";
    public const string Sound_404050205 = SOUND + "404050205";
    public const string Sound_404050206 = SOUND + "404050206";
    public const string Sound_404050207 = SOUND + "404050207";
    public const string Sound_404050208 = SOUND + "404050208";
    public const string Sound_404050209 = SOUND + "404050209";
    public const string Sound_404050210 = SOUND + "404050210";
    public const string Sound_404050211 = SOUND + "404050211";
    public const string Sound_404050212 = SOUND + "404050212";
    public const string Sound_404050213 = SOUND + "404050213";
    public const string Sound_404050214 = SOUND + "404050214";
    public const string Sound_404050215 = SOUND + "404050215";
    public const string Sound_404050216 = SOUND + "404050216";
    public const string Sound_404050217 = SOUND + "404050217";
    public const string Sound_603010109 = SOUND + "603010109";
    public const string Sound_603010112 = SOUND + "603010112";
    public const string Sound_404050401 = SOUND + "404050401";
    public const string Sound_404050402 = SOUND + "404050402";
    public const string Sound_404050403 = SOUND + "404050403";
    public const string Sound_404050404 = SOUND + "404050404";
    public const string Sound_404050405 = SOUND + "404050405";
    public const string Sound_404050406 = SOUND + "404050406";
    public const string Sound_404050407 = SOUND + "404050407";
    public const string Sound_404050408 = SOUND + "404050408";
    public const string Sound_404050428 = SOUND + "404050428";
    public const string Sound_404050429 = SOUND + "404050429";
    public const string Sound_404050410 = SOUND + "404050410";
    public const string Sound_404050412 = SOUND + "404050412";
    public const string Sound_404050414 = SOUND + "404050414";
    public const string Sound_404050409 = SOUND + "404050409";
    public const string Sound_404050411 = SOUND + "404050411";
    public const string Sound_404050413 = SOUND + "404050413";

    public const string Sound_604050101 = SOUND + "604050101";
    public const string Sound_604050102 = SOUND + "604050102";
    public const string Sound_604050103 = SOUND + "604050103";
    public const string Sound_604050104 = SOUND + "604050104";
    public const string Sound_604050105 = SOUND + "604050105";

    public const string Sound_404050348 = SOUND + "404050348";
    public const string Sound_404050349 = SOUND + "404050349";
    public const string Sound_404050350 = SOUND + "404050350";

    public const string Sound_404050342 = SOUND + "404050342";
    public const string Sound_404050343 = SOUND + "404050343";
    public const string Sound_404050344 = SOUND + "404050344";
    public const string Sound_404050345 = SOUND + "404050345";
    public const string Sound_404050346 = SOUND + "404050346";
    public const string Sound_404050347 = SOUND + "404050347";

    public const string Sound_601010110 = COMMON_SOUND + "601010110";
}

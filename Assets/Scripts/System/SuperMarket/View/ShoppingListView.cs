﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShoppingListView
{
    public HUIFormScript m_Form;
    //点击
    public GameObject BgBtn;

    public Image buy1;
    public Image buy2;
    public Image buy3;
    public Image buy4;
    public Image buy5;
    public Image buy6;

    public GameObject list1_btn;
    public GameObject list2_btn;
    public GameObject list3_btn;
    public GameObject list4_btn;
    public GameObject list5_btn;
    public GameObject list6_btn;


    public GameObject finishState1_obj;
    public GameObject finishState2_obj;
    public GameObject finishState3_obj;
    public GameObject finishState4_obj;
    public GameObject finishState5_obj;
    public GameObject finishState6_obj;

    public GameObject[] finishState_obj;
    

    public GameObject menuList_obj;

    private GameObject return_btn;

    public GameObject way_obj;

    public ScrollRect scroll;

    public GameObject guid_obj;

    public Image guid_img;

    public GameObject finger_obj;
    private Transform guid_pos;

    public void OpenForm()    {
        //HUIManager.Instance.OpenForm(TenPlusModuleAssetPath.m_ViewPath,true)貌似取得不是缓存值
        if (m_Form != null && m_Form.gameObject != null)        {
            if (m_Form != null)
            {
                HUIManager.Instance.CloseForm(m_Form);
                GameObject.DestroyImmediate(m_Form.gameObject);
            }        }        m_Form = HUIManager.Instance.OpenForm(SuperMarketResPath.SHPOLIST_DIR, false);        this.InitForm();    }    public void CloseForm()    {
        HUIManager.Instance.CloseForm(m_Form);
        BgBtn = null;
    }
    public void InitForm()    {        scroll = m_Form.transform.Find("menuList_obj/Pos/scroll").GetComponent<ScrollRect>();        guid_pos = m_Form.transform.Find("menuList_obj/Pos/guid_pos");        BgBtn = m_Form.GetWidget(0);         
        
        buy1 = m_Form.GetWidget(1).GetComponent<Image>();
        buy2 = m_Form.GetWidget(2).GetComponent<Image>();
        buy3 = m_Form.GetWidget(3).GetComponent<Image>();
    

        list1_btn = m_Form.GetWidget(4);
        list2_btn = m_Form.GetWidget(5);
        list3_btn = m_Form.GetWidget(6);

        finishState1_obj = m_Form.GetWidget(7);
        finishState2_obj = m_Form.GetWidget(8);
        finishState3_obj = m_Form.GetWidget(9);

       

        menuList_obj = m_Form.GetWidget(10);
        if (SDKManager.Instance.hasNotchInScreen())
        {
            RectTransform _menu = menuList_obj.transform as RectTransform;
            //_menu.anchoredPosition = new Vector2(180, -44.6f);
            _menu.sizeDelta = new Vector2(500, 767.3f);
        }

        return_btn = m_Form.GetWidget(11);

        way_obj = m_Form.GetWidget(12);

        list4_btn = m_Form.GetWidget(13);
        list5_btn = m_Form.GetWidget(14);
        list6_btn = m_Form.GetWidget(15);

        finishState4_obj = m_Form.GetWidget(16);
        finishState5_obj = m_Form.GetWidget(17);
        finishState6_obj = m_Form.GetWidget(18);

        buy4 = m_Form.GetWidget(19).GetComponent<Image>();
        buy5 = m_Form.GetWidget(20).GetComponent<Image>();
        buy6 = m_Form.GetWidget(21).GetComponent<Image>();

        guid_obj = m_Form.GetWidget(22).gameObject;
        guid_img = guid_obj.transform.GetChild(0).GetComponent<Image>();
        finger_obj = guid_img.transform.GetChild(0).gameObject;

        guid_img.transform.position = guid_pos.position;
        //finger_obj.transform.position = guid_pos.position;

        finishState_obj = new GameObject[6] { finishState1_obj, finishState2_obj, finishState3_obj, finishState4_obj, finishState5_obj, finishState6_obj };

        HUIUtility.SetUIMiniEvent(BgBtn, enUIEventType.Click, enUIEventID.ShopList_Btn_Bg);
        HUIUtility.SetUIMiniEvent(list1_btn, enUIEventType.Click, enUIEventID.MenuList1_Btn);
        HUIUtility.SetUIMiniEvent(list2_btn, enUIEventType.Click, enUIEventID.MenuList2_Btn);
        HUIUtility.SetUIMiniEvent(list3_btn, enUIEventType.Click, enUIEventID.MenuList3_Btn);
        HUIUtility.SetUIMiniEvent(list4_btn, enUIEventType.Click, enUIEventID.MenuList4_Btn);
        HUIUtility.SetUIMiniEvent(list5_btn, enUIEventType.Click, enUIEventID.MenuList5_Btn);
        HUIUtility.SetUIMiniEvent(list6_btn, enUIEventType.Click, enUIEventID.MenuList6_Btn);
        HUIUtility.SetUIMiniEvent(return_btn, enUIEventType.Click, enUIEventID.Return_Btn);
        HUIUtility.SetUIMiniEvent(guid_img.gameObject, enUIEventType.Click, enUIEventID.Guid_Btn);
    }







}

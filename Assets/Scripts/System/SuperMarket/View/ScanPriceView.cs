﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScanPriceView
{
    public HUIFormScript m_Form;
    public Image ten_img;
    public Image one_img;
    public Image remain_img;
    private GameObject return_btn;
    public void OpenForm()
    {
        m_Form = HUIManager.Instance.OpenForm(SuperMarketResPath.SCANPRICE_DIR, false);        this.InitForm();
    }

    public void CloseForm()    {
        HUIManager.Instance.CloseForm(m_Form);
    }
    public void InitForm()    {
        //Debug.Log("InitForm  === ");
        Transform price = m_Form.GetWidget(0).transform;
        ten_img = price.GetChild(0).GetComponent<Image>();
        one_img = price.GetChild(1).GetComponent<Image>();
        remain_img = price.GetChild(2).GetComponent<Image>();
        return_btn = m_Form.GetWidget(1);
        HUIUtility.SetUIMiniEvent(return_btn, enUIEventType.Click, enUIEventID.ReturnMain_Btn_Click);
    }
}

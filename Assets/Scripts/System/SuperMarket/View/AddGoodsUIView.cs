﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AddGoodsUIView  
{
    public HUIFormScript m_Form;
    public Image box1_img;
    public Image box2_img;
    public Image box3_img;
    public Transform HandClick;
    private GameObject return_btn;
    public Transform[] box_imgs;
    public void OpenForm()
    {
        m_Form = HUIManager.Instance.OpenForm(SuperMarketResPath.ADDGOODSUI_DIR, false);        this.InitForm();
    }

    public void CloseForm()    {
        HUIManager.Instance.CloseForm(m_Form);
    }
    public void InitForm()    {
        //Debug.Log("InitForm  === ");        
        box1_img = m_Form.GetWidget(0).GetComponent<Image>();
        box2_img = m_Form.GetWidget(1).GetComponent<Image>();
        box3_img = m_Form.GetWidget(2).GetComponent<Image>();
        box_imgs = new Transform[3];
        box_imgs[0] = box1_img.transform;
        box_imgs[1] = box2_img.transform;
        box_imgs[2] = box3_img.transform;
        return_btn = m_Form.GetWidget(3);
        HandClick = m_Form.transform.Find("Hand_Click");
        //Debug.LogError(HandClick.name + "   ====  HandClick ");
        HUIUtility.SetUIMiniEvent(return_btn, enUIEventType.Click, enUIEventID.Return_Btn);
    }
}

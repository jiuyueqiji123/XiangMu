﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 清洁
/// </summary>
public class DragMop : MonoBehaviour 
{
    public Transform wuzi;
    public Transform wuzi1;
    public Transform wuzi2;
    public float expendTime = 3f;

    private bool isClean = false;

    public bool IsClean
    {
        get;
        set;
    }
    Vector3 _lastMousePos;

    float _totalTime;

    GameAudioSource _gas;

    Transform _dragTarget;

    Vector3 targetScale = new Vector3(0f, 0f, 0.2f);
    // Use this for initialization
    void Start()
    {
        //原始位置  3.452  0  -3.239
        isClean = true;
    }

    #region 使用Input  射线检测
    // Update is called once per frame
    //Vector3 sceenPos;
    //Vector3 offsetPos;
    void Update()
    {
        if (!isClean) return;
        if (Input.GetMouseButtonDown(0))
        {
            if (_dragTarget)
            {
                _dragTarget.GetComponent<CapsuleCollider>().enabled = true;
                _dragTarget = null;
            }
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100, 1 << LayerMask.NameToLayer("Player") | 0 << LayerMask.NameToLayer("Floor") | 0 << LayerMask.NameToLayer("Bottle")) && hit.collider.name.Equals(transform.name))
            {
                Debug.Log(" GetMouseButtonDown ");
                _dragTarget = hit.transform;
                _dragTarget.GetComponent<CapsuleCollider>().enabled = false;
                CleanMarketController.Instance.EnableHand(false);
                //sceenPos = Camera.main.WorldToScreenPoint(_dragTarget.position);
                //offsetPos = _dragTarget.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, sceenPos.z));
            }
            else
            {
                _dragTarget = null;
            }
            _lastMousePos = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            if (_dragTarget == null) return;
            //Debug.Log("_lastMousePos ==="+_lastMousePos + " , Input.mousePosition ===" + Input.mousePosition);
            if (Vector3.Distance(_lastMousePos, Input.mousePosition) > 5f)
            {
                CleanMarketController.Instance._timerstampUpdate();
                //音乐 bgm
                if (_gas == null)
                    _gas = AudioManager.Instance.PlaySound(SuperMarketResPath.Sound_602020502, true, true);

                var distance = Vector3.Distance(_lastMousePos, Input.mousePosition);
                _lastMousePos = Input.mousePosition;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100, 0 << LayerMask.NameToLayer("Player") | 0 << LayerMask.NameToLayer("Floor") | 1 << LayerMask.NameToLayer("Bottle")))
                {
                    CleanMarketController.Instance.PlayCleanEffect(true);

                    _dragTarget.position = hit.point;

                    float delta = _totalTime / expendTime;
                    wuzi1.localScale = Vector3.Lerp(wuzi1.localScale, targetScale, delta);
                    wuzi2.localScale = Vector3.Lerp(wuzi2.localScale, targetScale, delta);
                    wuzi.GetComponent<BoxCollider>().size = Vector3.Lerp(new Vector3(0.7f, 0, 0.7f), new Vector3(0.5f, 0f, 0.5f), delta);
                    _totalTime += distance * 0.0003f; // 变化速度
                    //Debug.Log("  1111 == "+ delta);
                    //if (delta >= 1f)
                    if (Vector3.Distance(wuzi1.localScale, targetScale) < 0.1f && Vector3.Distance(wuzi2.localScale, targetScale) < 0.1f)
                    {
                        //表明擦拭干净  清扫完成，地面出光亮的粒子成功效果。 乐迪：“你真棒，地板擦得亮晶晶。”
                        if (_gas != null)
                        {
                            _gas.Stop();
                            _gas = null;
                        }
                        CleanMarketController.Instance.PlayCleanEffect(false);
                        CleanMarketController.Instance.PlayEffect();
                        isClean = false;
                        SuperMarketGame.Instance.soundCtrl.PlayLeDiVoiceAnimation(SuperMarketResPath.Sound_404050107);
                        SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_604050103);
                        wuzi1.localScale = new Vector3(0, 0, 0);
                        wuzi2.localScale = new Vector3(0, 0, 0);
                        StartCoroutine(TimeUtility.DelayInvoke(3f, () =>
                        {
                            Debug.Log("  结束清扫 ");                          
                            //TODO  播放成功特效   
                            CleanMarketController.Instance.EndGame();
                        }));
                    }
                }
                else if (Physics.Raycast(ray, out hit, 100, 1 << LayerMask.NameToLayer("Floor") | 0 << LayerMask.NameToLayer("Player") | 0 << LayerMask.NameToLayer("Bottle")))
                {
                    //Debug.Log("  2222 ");                    
                    _dragTarget.position = hit.point;
                    //Vector3 curSceenPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, sceenPos.z));
                    //_dragTarget.position = curSceenPos+offsetPos;
                }
            }
            else
            {
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            CleanMarketController.Instance.PlayCleanEffect(false);
            if (_gas != null)
            {
                _gas.Stop();
                _gas = null;
            }
            //if (_dragTarget != null)
                //_dragTarget = null;
          /*
            StartCoroutine(TimeUtility.DelayInvoke(2f, () => {
                if (!_disableCanvas)
                {
                    _canvas.SetActive(true);
                }
            }));    
         */
        }
    }
    #endregion

    #region 接口实现
    public void OnInputBeginDrag(InputEventData eventData)
    {

    }
    public void OnInputDrag(InputEventData eventData)
    {

        /*var targetInScreenPos = Camera.main.WorldToScreenPoint(wuzi.position);
        var mousePos = Input.mousePosition;
        var screenPos = new Vector3(mousePos.x, mousePos.y, targetInScreenPos.z);
        var worldPos = Camera.main.ScreenToWorldPoint(screenPos);
        transform.position = worldPos; */
    }



    public void OnInputEndDrag(InputEventData eventData)
    {

    }
    #endregion
}

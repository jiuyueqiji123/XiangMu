﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using DG.Tweening;
/// <summary>
///  放入金币
/// </summary>
public class DragMoneyCtrl : MonoBehaviour, IInputDragHandler, IInputBeginDragHandler, IInputEndDragHandler
{
    private DigitsType moneyValue;
    /// <summary>
    /// 面值
    /// </summary>
    public DigitsType MoneyValue
    {
        get;
        set;        
    }
    Vector3 screenPos;
    Vector3 offsetPos;
    //原始位置
    Vector3 orgPos;
    //偏移距离
    public float distanceRate = 0.3f;
    
    //射线检测位置
    private Transform tenPos;
    private Transform fivePos;
    private Transform twoPos;
    private Transform onePos;
    private Transform wmPos;

    //是否找到目标
    bool isFindTarget = false;
    void Awake()
    {
        orgPos = transform.position;
    }
    /// <summary>
    /// 初始化碰撞位置
    /// </summary>
    public void InitColliderPos(Transform _tenPos, Transform _fivePos, Transform _twoPos, Transform _onePos, Transform _wmPos)
    {
        tenPos = _tenPos;
        fivePos = _fivePos;
        twoPos = _twoPos;
        onePos = _onePos;
        wmPos = _wmPos;
    }
    public void OnInputBeginDrag(InputEventData eventData)
    {
        Vector3 off = Camera.main.transform.position - transform.position;
        Vector3 dragPos = transform.position + off * distanceRate;

        screenPos = Camera.main.WorldToScreenPoint(dragPos);
        offsetPos = dragPos - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPos.z));

        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPos.z)) + offsetPos;

        isFindTarget = false;
        CashRegisterController.Instance.totalCount = 0;
        MarketMainSceneCtrl.Instance.handClick.gameObject.SetActive(false);
        MarketMainSceneCtrl.Instance.handClick.DOKill();
    }

    public void OnInputDrag(InputEventData eventData)
    {
        if (isFindTarget) return;
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPos.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offsetPos;
        transform.position = new Vector3(curPosition.x, curPosition.y, curPosition.z);
        RaycastHit hitinfo;
        Debug.DrawLine(Camera.main.transform.position, transform.position ,Color.red);
        Vector3 dir = transform.position - Camera.main.transform.position;
        if (Physics.Raycast(Camera.main.transform.position, dir, out hitinfo,100,1<<LayerMask.NameToLayer("Transition")))
        {
            //Debug.Log(" hitinfo.collider.name == " + hitinfo.collider.name);
            if (MoneyValue == DigitsType.Remain && hitinfo.collider.name.Equals(wmPos.name))
            {
                //Debug.Log(" == 找到目标位置了 == " + wmPos.name);
                //TODO 1 Money的Enable为false  不能拖拽和射线检测了， 2 移动到相对应位置  3 是否完成收银台钱的放置
                DoFindTarget(DigitsType.Remain);
            }
            else if (MoneyValue == DigitsType.One && hitinfo.collider.name.Equals(onePos.name))
            {
                //Debug.Log(" == 找到目标位置了 == " + onePos.name);
                DoFindTarget(DigitsType.One);
            }
            else if (MoneyValue == DigitsType.Two && hitinfo.collider.name.Equals(twoPos.name))
            {
                //Debug.Log(" == 找到目标位置了 == " + twoPos.name);
                DoFindTarget(DigitsType.Two);
            }
            else if (MoneyValue == DigitsType.Five && hitinfo.collider.name.Equals(fivePos.name))
            {
                //Debug.Log(" == 找到目标位置了 == " + fivePos.name);
                DoFindTarget(DigitsType.Five);
            }
            else if (MoneyValue == DigitsType.Ten && hitinfo.collider.name.Equals(tenPos.name))
            {
                //Debug.Log(" == 找到目标位置了 == " + tenPos.name);
                DoFindTarget(DigitsType.Ten);
            }
            else
            {
                isFindTarget = false;
            }
        }
    }
    /// <summary>
    /// 找到目标后执行的方法
    /// </summary>
    void DoFindTarget(DigitsType digitsType)
    {
        CashRegisterController.Instance.isGuid = false;
       isFindTarget = true;
        transform.GetComponent<BoxCollider>().enabled = false;
        Vector3 target = transform.localPosition;
        switch (digitsType)
        {
            case DigitsType.Remain:
                transform.SetParent(wmPos);
                // x -0.45 - 0.4   y -0.94  -0.6  z -1.53
                target = new Vector3(Random.Range(-0.45f, 0.4f), Random.Range(-0.94f, -0.6f), -1.53f);
                break;
            case DigitsType.One:
                transform.SetParent(onePos);
                target = new Vector3(Random.Range(-0.45f, 0.4f), Random.Range(-0.94f, -0.6f), -1.53f);
                break;
            case DigitsType.Two:
                transform.SetParent(twoPos);
                target = new Vector3(Random.Range(-0.45f, 0.1f), Random.Range(-0.94f, -0.85f), -0.286f);
                break;
            case DigitsType.Five:
                transform.SetParent(fivePos);
                target = new Vector3(Random.Range(-0.45f, 0.1f), Random.Range(-0.94f, -0.85f), -0.286f);
                break;
            case DigitsType.Ten:
                transform.SetParent(tenPos);
                //x -0.45 - 0.1   y -0.94 - -0.85  z -0.286
                target = new Vector3(Random.Range(-0.45f,0.1f), Random.Range(-0.94f, -0.85f), -0.286f); 
                break;
            default:
                break;
        }
        transform.DOLocalMove(target, 0.3f).OnComplete(()=> {
            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_604050105);
            if (CashRegisterController.Instance.CheckArrange())
            {
                CashRegisterController.Instance.FinishArrange();
            }
        });        
    }

    public void OnInputEndDrag(InputEventData eventData)
    {
        if(!isFindTarget)
            transform.position = orgPos;
    } 

}

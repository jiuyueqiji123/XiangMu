﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
/// <summary>
/// 补货 拖拽
/// </summary>
public class DragAddGoods : MonoBehaviour, IInputDragHandler, IInputBeginDragHandler, IInputEndDragHandler
{
    //是否是补货目标
    public bool isTarget = false;

    bool isFindTarget = false;

    Vector3 screenPos;
    Vector3 offsetPos;

    Vector3 orgPos;
    void Awake()
    {
        BoxCollider2D collider = gameObject.AddComponent<BoxCollider2D>();
        collider.size = new Vector2(0.6f, 0.6f);
        SetShow(false);

        orgPos = transform.position;
    }
    public void SetShow(bool isActive)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(isActive);
        }
    }
     
   
    public void OnInputBeginDrag(InputEventData eventData)
    {
        SetShow(true);
        AudioManager.Instance.StopSound(AudioManager.Instance.GetAudioClip(SuperMarketResPath.Sound_404050205));
        AudioManager.Instance.StopSound(AudioManager.Instance.GetAudioClip(SuperMarketResPath.Sound_404050104));
        AddGoodsController.Instance.HideHand();
        AddGoodsController.Instance.isShakeUI = false;
        screenPos = Camera.main.WorldToScreenPoint(transform.position);
        offsetPos = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPos.z));
        SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_601010106);
    }

    public void OnInputDrag(InputEventData eventData)
    {
        if (isFindTarget) return;
        Vector3 curScreenPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPos.z);
        Vector3 curPos = Camera.main.ScreenToWorldPoint(curScreenPos) + offsetPos;
        transform.position = curPos;
        RaycastHit hitinfo;        
        Vector3 dir = transform.position - Camera.main.transform.position;
        Debug.DrawLine(Camera.main.transform.position, Camera.main.transform.position + dir * 100, Color.red);
        if (Physics.Raycast(Camera.main.transform.position, dir, out hitinfo, 100, 1 << LayerMask.NameToLayer("DragFruit"))  && isTarget)
        {
            if (hitinfo.transform.childCount == 0)
            {
                //Debug.Log(" == 找到目标位置了 == " + hitinfo.transform.name);
                isFindTarget = true;
                transform.SetParent(hitinfo.transform);
                transform.DOLocalMove(Vector3.zero, 0.5f).OnComplete(()=> {
                    SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_601010110);
                    MarketMainSceneCtrl.Instance.PlayStarEffect(new Vector3(transform.position.x, transform.position.y+0.3f, transform.position.z+1));
                    AddGoodsController.Instance.PlayEffect();
                });
                transform.GetComponent<BoxCollider2D>().enabled = false;
            }
        }
    }

    public void OnInputEndDrag(InputEventData eventData)
    {
        if (!isTarget || !isFindTarget)
        {
            SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_603010902);
            if (!isTarget)
                SuperMarketGame.Instance.soundCtrl.PlaySoud(SuperMarketResPath.Sound_404050401);


            transform.DOMove(orgPos, 0.5f).OnComplete(()=> {
                SetShow(false);
            });                        
        }
    }
}

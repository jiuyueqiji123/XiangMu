﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class WayGuide : MonoBehaviour
{
    private Image wayPoint1;
    //private Button wayPoint2;

    public Canvas canvas;

    public float interval = 3f;//0.8f;
    float timeCount = 0;
    // Use this for initialization
    void Awake()
    {
        timeCount = 0f;
        wayPoint1 = gameObject.GetComponent<Image>();//transform.Find("wayPoint1_btn").GetComponent<Image>();
        //wayPoint2 = transform.Find("wayPoint2_btn").GetComponent<Button>();
    }

    void Start()
    {
        //gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        //gameObject.GetComponent<Button>().onClick.AddListener(wayPointClick);
        /*if (wayPoint1 != null)
        {
            wayPoint1.onClick.RemoveAllListeners();
            wayPoint1.onClick.AddListener(wayPointClick);
        }
        if (wayPoint2 != null)
        {
            wayPoint2.onClick.RemoveAllListeners();
            wayPoint2.onClick.AddListener(wayPointClick);
        }*/
        wayPoint1.gameObject.SetActive(true);
        targetAlpha = 0.2f;//wayPoint1.color.a;
        //wayPoint2.gameObject.SetActive(false);
    }

    // Update is called once per frame
    float targetAlpha;
    public bool isChange = false;
    void Update()
    {
        //timeCount += Time.deltaTime;
        //if (timeCount >= interval)
        //{
        //wayPoint1.gameObject.SetActive(wayPoint2.gameObject.activeSelf);
        //wayPoint2.gameObject.SetActive(!wayPoint1.gameObject.activeSelf);
        //timeCount = 0;
        //} 
        if (isChange)
        {
            float alpha = wayPoint1.color.a;
            wayPoint1.color = new Color(1, 1, 1, Mathf.Lerp(alpha, targetAlpha, Time.deltaTime * interval));
            if (Mathf.Abs(wayPoint1.color.a - targetAlpha) < 0.05f)
            {
                if (targetAlpha == 0.2f)
                    targetAlpha = 1;
                else if (targetAlpha == 1)
                {
                    targetAlpha = 0.2f;
                }

            }
        }         
    }

    public void wayPointClick()
    {
        //移动主角位置到当前way位置
        //RectTransformUtility.ScreenPointToWorldPointInRectangle(transform.GetComponent<RectTransform>(),)
        /*ShoppingListController.Instance.WayPointEnable(false);
        Vector3 worldpoint;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(canvas.transform as RectTransform, canvas.worldCamera.WorldToScreenPoint(SuperMarketGame.Instance.superCamera.transform.position), canvas.worldCamera, out worldpoint);
        Debug.Log(" == wayPointClick == " + worldpoint.x);
        float target = SuperMarketGame.Instance.superCamera.transform.position.x - 1.2f;
        SuperMarketGame.Instance.MoveMainCamera(target);
        SuperMarketGame.Instance.WayPointEnable(false);
        SuperMarketGame.Instance.IsCheckOperate = false;*/
    }

}

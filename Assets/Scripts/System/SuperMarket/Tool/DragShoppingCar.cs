﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DragShoppingCar : MonoBehaviour 
{
    //是否整理完毕
    bool isSortOver = false;
    GameAudioSource _gas;
    Vector3 _lastMousePos;

    bool isDrag = false;

    Sequence seqStart;
    // Use this for initialization
    void Start()
    {
        seqStart = DOTween.Sequence();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShakeCar()
    {
        if (isSortOver) return;
        transform.DOPunchPosition(new Vector3(1, 0, 1) * 0.1f, 2f);
    }

    private void OnMouseDown()
    {
        _lastMousePos = Input.mousePosition;       
    }

    private void OnMouseDrag()
    {
        if (isSortOver) return;
        if (Vector3.Distance(_lastMousePos, Input.mousePosition) > 5f  && _lastMousePos.x < Input.mousePosition.x)
        {
            //音乐 bgm
            //if (_gas == null)
            //_gas = AudioManager.Instance.PlaySound(CakeShopResPath.SE_604010208, true, true);

            isDrag = true;
        }
    }
    private void OnMouseUp()
    {
        if (isSortOver) return;
        SortoutCarController.Instance.HideHand();
        SortoutCarController.Instance.totalCount = 0;
        if (Vector3.Distance(_lastMousePos, Input.mousePosition) < 0.1f || isDrag)
            SetPos();
    }

    void SetPos()
    {
        isSortOver = true;
        float x = SortoutCarController.Instance.targetPos.x + (SortoutCarController.Instance.CurSortNum * SortoutCarController.Instance.intervalX);
        //transform.localPosition = new Vector3(x, SortoutCarController.Instance.targetPos.y, SortoutCarController.Instance.targetPos.z);
        //transform.localRotation = Quaternion.identity;
        //Debug.Log(x);
        Vector3 targetPos = new Vector3(x, SortoutCarController.Instance.targetPos.y, SortoutCarController.Instance.targetPos.z);
        seqStart.Append(transform.DOMove(targetPos, 0.3f).SetEase(Ease.Linear));
        seqStart.Append(transform.DORotate(new Vector3(0, 0, 0), 0.4f).OnComplete(() => {
            SortoutCarController.Instance.PlayEffect();
        }));              
        seqStart.Play();
    }

   
}

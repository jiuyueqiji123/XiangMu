﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SuperMarketTool 
{

    public static AudioClip GetAudioClip(string name)
    {
        string path = name;
        Resource res = ResourceManager.Instance.GetResource(path, typeof(AudioClip), enResourceType.Sound);
        if (res.m_content != null)
        {
            return res.m_content as AudioClip;
        }

        return null;
    }
    /// <summary>
    /// 计算价格  支持百一下 两位数
    /// </summary>
    /// <param name="price"></param>
    /// <returns></returns>
    public static Dictionary<MoneyType, int> CalNum(float price, bool isNeedZero = true)
    {
        int tempPrice = Mathf.FloorToInt(price);
        int remain = 0; //零钱  小数点第一位
        if (price.ToString().Contains("."))
            remain = Convert.ToInt32(price.ToString().Substring(price.ToString().LastIndexOf('.') + 1, 1));
        //List<int> list = new List<int>();
        Dictionary<MoneyType, int> dic = new Dictionary<MoneyType, int>();
        dic.Add(MoneyType.Remain, remain);
        //list.Add(remain);
        int index = 0;
        while (tempPrice != 0)
        {
            int pop = tempPrice % 10;
            tempPrice = tempPrice / 10;
            index++;
            dic.Add((MoneyType)index, pop);
            //list.Add(pop);
        }
        //个十位置没有的需要补齐0
        if (isNeedZero)
        {
            if (index < 2)
            {
                for (int i = index + 1; i < 3; i++)
                {
                    dic.Add((MoneyType)i, 0);
                }
            }
        }
        return dic;
    }

    #region ...
    public static List<DigitsType> AnalyDigits(List<DigitsType> pathList, int num)
    {
        int five = num / 5;
        if (five != 0)
            pathList.Add(DigitsType.Five);
        int pop = num % 5;
        // 4,3,2,1,0
        if (pop == 4)
        {
            int n = UnityEngine.Random.Range(1, 5);
            //Debug.Log(" num 为 4的随机值 === " + n);
            if (n == 1)
            {
                pathList.Add(DigitsType.Two);
                pathList.Add(DigitsType.Two);
            }
            else if (n == 2)
            {
                pathList.Add(DigitsType.Two);
                pathList.Add(DigitsType.One);
                pathList.Add(DigitsType.One);
            }
            else if (n == 3)
            {
                pathList.Add(DigitsType.Two);
                pathList.Add(DigitsType.One);
                pathList.Add(DigitsType.Remain);
                pathList.Add(DigitsType.Remain);
            }
            else if (n == 4)
            {
                pathList.Add(DigitsType.One);
                pathList.Add(DigitsType.One);
                pathList.Add(DigitsType.One);
                pathList.Add(DigitsType.Remain);
                pathList.Add(DigitsType.Remain);
            }
        }
        else if (pop == 3)
        {
            int n = UnityEngine.Random.Range(1, 4);
            if (n == 1)
            {
                pathList.Add(DigitsType.Two);
                pathList.Add(DigitsType.One);
            }
            else if (n == 2)
            {
                pathList.Add(DigitsType.Two);
                pathList.Add(DigitsType.Remain);
                pathList.Add(DigitsType.Remain);
            }
            else if (n == 3)
            {
                pathList.Add(DigitsType.One);
                pathList.Add(DigitsType.One);
                pathList.Add(DigitsType.One);
            }
        }
        else if (pop == 2)
        {
            int n = UnityEngine.Random.Range(0, 2);
            if (n == 0)
            {
                pathList.Add(DigitsType.One);
                pathList.Add(DigitsType.One);
            }
            else
            {
                pathList.Add(DigitsType.One);
                pathList.Add(DigitsType.Remain);
                pathList.Add(DigitsType.Remain);
            }

        }
        else if (pop == 1)
        {
            int n = UnityEngine.Random.Range(0, 2);
            if (n == 0)
            {
                pathList.Add(DigitsType.One);
            }
            else
            {
                pathList.Add(DigitsType.Remain);
                pathList.Add(DigitsType.Remain);
            }
        }

        return pathList;
    }

    public static List<DigitsType> AnalyTenDigits(List<DigitsType> pathList, int num)
    {
        //num 可能为1 - 9
        switch (num)
        {
            case 1:
                int m = UnityEngine.Random.Range(0, 4);               
                if (m == 0)
                {
                    pathList.Add(DigitsType.Ten);
                }
                else if (m == 1)
                {
                    pathList.Add(DigitsType.Five);
                    pathList.Add(DigitsType.Five);
                }
                else if (m == 2)
                {
                    pathList.Add(DigitsType.Five);
                    pathList.Add(DigitsType.Two);
                    pathList.Add(DigitsType.Two);
                    pathList.Add(DigitsType.One);
                }
                else if (m == 3)
                {
                    pathList.Add(DigitsType.Five);
                    pathList.Add(DigitsType.Two);
                    pathList.Add(DigitsType.One);
                    pathList.Add(DigitsType.One);
                    pathList.Add(DigitsType.Remain);
                    pathList.Add(DigitsType.Remain);
                }
                break;
            case 2:
                int n = UnityEngine.Random.Range(0, 2);
                //Debug.Log(" num 为 2的随机值 === "+ n);
                if (n == 0)
                {
                    pathList.Add(DigitsType.Ten);
                    pathList.Add(DigitsType.Five);
                    pathList.Add(DigitsType.Five);
                }
                else
                {
                    pathList.Add(DigitsType.Ten);
                    pathList.Add(DigitsType.Five);
                    pathList.Add(DigitsType.Two);
                    pathList.Add(DigitsType.One);
                    pathList.Add(DigitsType.One);
                    pathList.Add(DigitsType.Remain);
                    pathList.Add(DigitsType.Remain);
                }
                break;
            case 3:
                int s = UnityEngine.Random.Range(0, 2);
                if (s == 0)
                {
                    pathList.Add(DigitsType.Ten);
                    pathList.Add(DigitsType.Five);
                    pathList.Add(DigitsType.Five);
                    pathList.Add(DigitsType.Five);
                    pathList.Add(DigitsType.Two);
                    pathList.Add(DigitsType.Two);
                    pathList.Add(DigitsType.One);
                }
                else
                {
                    pathList.Add(DigitsType.Ten);
                    pathList.Add(DigitsType.Ten);
                    pathList.Add(DigitsType.Five);
                    pathList.Add(DigitsType.Two);
                    pathList.Add(DigitsType.One);
                    pathList.Add(DigitsType.One);
                    pathList.Add(DigitsType.Remain);
                    pathList.Add(DigitsType.Remain);
                }
                break;             
            default:
                for (int i = 0; i < num; i++)
                {
                    pathList.Add(DigitsType.Ten);
                }              
                break;
        }

        return pathList;
    }

    #endregion
}

﻿using System;

public enum GameType 
{
    //清洁
    Cleanliness = 0,
    //摆放货物
    PlaceGoods = 1,
    //整理购物车 
    ShoppingCart = 2,
}

public enum GoodsType
{
    Vegetables = 1,
    Fruits = 2,
    Snacks = 3,
    Toy = 4,
    Dessert = 5,
    Supplies = 6,
}

public enum RoleType
{
    ajiasi = 1,
    aisha = 2,
    nuowa = 3,
}
/// <summary>
/// 钱位数
/// </summary>
public enum MoneyType
{
     //小数
     Remain = 0,
     //个位数
     Digits = 1,
     TenDigits = 2,
     HundredsDigit = 3,
     ThousandDigits = 4,    
}
//钱类型 10元 5元 2元 1元 五毛
public enum DigitsType
{
    Remain = 0,
    One = 1,
    Two = 2,
    Five = 3,
    Ten = 4,
}

public enum RoleSoundType
{
    Bye = 0,
    LediBye = 1,
    IWantBuy = 2,
    No = 3,
}
﻿using UnityEngine;
using System.Collections;
using TableProto;
using System.Collections.Generic;

public class MatrixModel : Singleton<MatrixModel>
{
    /// <summary>
    /// levelId 对应的矩阵详情表
    /// </summary>
    private Dictionary<int, List<MatrixLevelInfo>> matrixLelInfoDic = new Dictionary<int, List<MatrixLevelInfo>>();
    public override void Init()
    {
        foreach (MatrixLevelInfo mli in TableProtoLoader.MatrixLevelInfoDict.Values)
        {
            int lelId = mli.levelId;
            //Debug.Log("lelId ==== " + lelId +"   , id === "+ mli.id + "  , columnTitle === "+mli.columnTitle + "  , rowTitle === " + mli.rowTitle);
            if (!matrixLelInfoDic.ContainsKey(lelId))
            {
                matrixLelInfoDic.Add(lelId, new List<MatrixLevelInfo>());
            }
            matrixLelInfoDic[lelId].Add(mli);
        }
        Debug.Log("  matrixLelInfoDic.Count ===  " + matrixLelInfoDic.Count);
    }

    public int GetLevelCount(int type)
    {
        int lel = 0;
        bool tempbol = false;
        foreach (MatrixLevel ml in TableProtoLoader.MatrixLevelDict.Values)
        {
            if (ml.type == type)
            {
                lel++;
                tempbol = true;
            }
            else if (tempbol)
                break;
        }
        return lel;
    }

    /// <summary>
    /// 获取关卡信息
    /// </summary>
    /// <param name="type">类型（数字，推理，图形，加减）</param>
    /// <param name="level">等级</param>
    /// <returns></returns>
    public MatrixLevel GetLevelByLelType(MatrixType type, int level)
    {
        MatrixLevel matrixLevel = null;
        foreach (MatrixLevel ml in TableProtoLoader.MatrixLevelDict.Values)
        {
            if (ml.level == level && ml.type == (int)type)
            {
                matrixLevel = ml;
                break;
            }
        }
        return matrixLevel;
    }

    /// <summary>
    /// 获取结果，根据id,行,列
    /// </summary>
    /// <param name="lelId"></param>
    /// <param name="rolwTitle"></param>
    /// <param name="columnTitle"></param>
    /// <returns></returns>
    public MatrixLevelInfo GetResultByLelIdRowCol(int lelId, string rolwTitle, string columnTitle)
    {
        //string result = "";
        MatrixLevelInfo model = null;
        if (matrixLelInfoDic != null && matrixLelInfoDic.Count > 0)
        {
            foreach (var dic in matrixLelInfoDic)
            {
                if (dic.Key == lelId)
                {
                    List<MatrixLevelInfo> list = dic.Value;
                    if (list != null && list.Count > 0)
                    {
                        foreach (MatrixLevelInfo mli in list)
                        {
                            if (mli.rowTitle == rolwTitle && mli.columnTitle == columnTitle)
                            {
                                //result = mli.resultname;                                 
                                model = mli;
                                break;
                            }
                        }
                    }
                }
            }
        }        
        return model;
    }

    /// <summary>
    /// 随机不重复（随机的都是下标）
    /// </summary>
    /// <param name="count">随机的数量</param>
    /// <param name="countNum">随机的数据源数量 比如在 1-16  里面随机 6个</param>
    /// <returns></returns>
    public int[] RandomTitles(int count, int countNum)
    {
        List<int> array = new List<int>();
        for (int j = 0; j < countNum; j++)
        {
            array.Add(j);
        }
        List<int> newArray = new List<int>();
        for (int i = 0; i < count; i++)
        {
            int index = Random.Range(0, array.Count);
            newArray.Add(array[index]);
            array.RemoveAt(index);
        }
        /*while (newArray.Count >= 3)
        {
            int r = Random.Range(0, 3);
            if (!newArray.Contains(r))
                newArray.Add(r);
            
        }*/
        return newArray.ToArray();
    }

    /// <summary>
    /// 获取标题数据
    /// </summary>
    /// <param name="titles">分号分隔的字符串</param>
    /// <param name="isRandom">是否随机</param>
    /// <param name="count">数量</param>
    public string[] DesignMatrixTitle(string titles, int isRandom, int count)
    {
        string[] titleArray = new string[count];

        string[] oldTitles = titles.Split(';');
        if (isRandom == 1)
        {
            //随机下标
            int[] rRandoms = RandomTitles(count, count);
            for (int i = 0; i < rRandoms.Length; i++)
            {
                titleArray[i] = oldTitles[rRandoms[i]];
            }
        }
        else
            titleArray = oldTitles;

        return titleArray;
    }


    public string GetAtlas(MatrixType type)
    {
        string atlas = MatrixResType.NumAtlas;
        switch (type)
        {
            case MatrixType.Num:
                atlas = MatrixResType.NumAtlas;
                break;
            case MatrixType.Reasoning:
                atlas = MatrixResType.ReasoningAtlas;
                break;
            case MatrixType.Graphics:
                atlas = MatrixResType.GraphicsAtlas;
                break;
            case MatrixType.Addsub:
                atlas = MatrixResType.AddsubAtlas;
                break;
        }
        return atlas;
    }

    public string GetAudio(MatrixType type)
    {
        string audio = "";
        switch (type)
        {
            case MatrixType.Num:
                audio = "403060101";
                break;
            case MatrixType.Graphics:
                audio = "403060201";
                break;
            case MatrixType.Addsub:
                audio = "403060401";
                break;
            case MatrixType.Reasoning:
                audio = "403060301";
                break;
        }
        return audio;
    }

}

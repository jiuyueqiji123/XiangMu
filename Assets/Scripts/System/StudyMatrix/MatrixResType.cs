﻿using System;
public enum MatrixType
{
    Num = 1,
    Graphics = 2,
    Addsub = 3,
    Reasoning = 4,
}
public sealed class MatrixResType 
{
    public const string audioPath = "study_matrix/study_matrix_audio/";
    //public const string COMMON_SOUND = "sound/";
    public const string ROOT = "study_matrix/";


    public const string NumAtlas = "study_matrix/study_matrix_png/";
    public const string ReasoningAtlas = "study_matrix/study_matrix_png/";
    public const string GraphicsAtlas = "study_matrix/study_matrix_png/";
    public const string AddsubAtlas = "study_matrix/study_matrix_png/";
}
 
 

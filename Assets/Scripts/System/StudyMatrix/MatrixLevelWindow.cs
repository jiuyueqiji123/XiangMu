﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MatrixLevelWindow : SingletonBaseWindow<MatrixLevelWindow>
{
    #region ui property
    public Button _return_btn { get; set; }
    public Button _num_btn { get; set; }
    public Button _reasoning_btn { get; set; }
    public Button _graphics_btn { get; set; }
    public Button _addsub_btn { get; set; }
    #endregion

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        _return_btn.onClick.AddListener(OnButtonReturnClick);
        Input.multiTouchEnabled = false;
        AudioManager.Instance.PlayMusic(MatrixResType.audioPath + "603060101", true, true, true);
        AudioManager.Instance.BackgroundMusicVolume = 0.6f;
        _num_btn.transform.localPosition = new Vector3(-1243.15f, -97f, 0);
        _reasoning_btn.transform.localPosition = new Vector3(-1243.15f, -97f, 0);
        _graphics_btn.transform.localPosition = new Vector3(1258.4f, -97f, 0);
        _addsub_btn.transform.localPosition = new Vector3(1258.4f, -97f, 0);
        if (MatrixWindow.Instance.spaceItemList != null && MatrixWindow.Instance.spaceItemList.Count > 0)
        {
            _reasoning_btn.transform.DOLocalMoveX(-206f, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
            {
                _num_btn.transform.DOLocalMoveX(-605f, 0.3f).SetEase(Ease.Linear);
            });

            _graphics_btn.transform.DOLocalMoveX(198f, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
            {
                _addsub_btn.transform.DOLocalMoveX(602f, 0.3f).SetEase(Ease.Linear);
            });
        }
        else
        {

            _reasoning_btn.transform.DOLocalMoveX(-206f, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
            {
                _num_btn.transform.DOLocalMoveX(-605f, 0.3f).SetEase(Ease.Linear);
            });

            _graphics_btn.transform.DOLocalMoveX(198f, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
            {
                _addsub_btn.transform.DOLocalMoveX(602f, 0.3f).SetEase(Ease.Linear);
            });

            this.AddTimerEx(0.8f, () =>
            {
                WindowManager.Instance.OpenWindow(WinNames.EnglishLediAnimationPanel);
                EnglishLediAnimationWindow.Instance.PlayLediTalkAnimation(EnglishLediAnimationWindow.ELediFadeInType.BottomLeft, MatrixResType.audioPath + "403060100", () =>
                {
                });
            });


            /* EnglishLediAnimationWindow.Instance.PlayLediWinAnimation(this.GetAudioNameEx(StudyAudioName.t_32009), true,true, () =>
             {
                 _reasoning_btn.transform.DOLocalMoveX(-206f, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
                 {
                     _num_btn.transform.DOLocalMoveX(-605f, 0.3f).SetEase(Ease.Linear);
                 });

                 _graphics_btn.transform.DOLocalMoveX(198f, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
                 {
                     _addsub_btn.transform.DOLocalMoveX(602f, 0.3f).SetEase(Ease.Linear);
                 });
             });*/
        }
    }

    protected override void OnClose()
    {
        WindowManager.Instance.CloseWindow(WinNames.EnglishLediAnimationPanel);
        _num_btn.transform.localScale = new Vector3(1, 1, 1);
        _reasoning_btn.transform.localScale = new Vector3(1, 1, 1);
        _graphics_btn.transform.localScale = new Vector3(1, 1, 1);
        _addsub_btn.transform.localScale = new Vector3(1, 1, 1);
    }

    protected override void OnUpdate()
    {

    }

    protected override void OnRefresh()
    {
    }

    protected override void AddListeners()
    {
      
        _num_btn.onClick.AddListener(OnNumClick);
        _reasoning_btn.onClick.AddListener(OnReasoningClick);
        _graphics_btn.onClick.AddListener(OnGraphicsClick);
        _addsub_btn.onClick.AddListener(OnAddsubClick);
    }

    protected override void RemoveListensers()
    {
        _num_btn.onClick.RemoveAllListeners();
        _reasoning_btn.onClick.RemoveAllListeners();
        _graphics_btn.onClick.RemoveAllListeners();
        _addsub_btn.onClick.RemoveAllListeners();
    }


    #region uievent
    void OnNumClick()
    {
        WindowManager.Instance.OpenWindow(WinNames.MatrixInfoPanel);
        WindowManager.Instance.CloseWindow(WinNames.MatrixLevelPanel, true);
        int lelCount = MatrixModel.Instance.GetLevelCount((int)MatrixType.Num);
        MatrixWindow.Instance.StartGame(MatrixType.Num, lelCount);
    }

    void OnReasoningClick()
    {
        WindowManager.Instance.OpenWindow(WinNames.MatrixInfoPanel);
        WindowManager.Instance.CloseWindow(WinNames.MatrixLevelPanel, true);
        int lelCount = MatrixModel.Instance.GetLevelCount((int)MatrixType.Reasoning);
        MatrixWindow.Instance.StartGame(MatrixType.Reasoning, lelCount);
    }

    void OnGraphicsClick()
    {
        WindowManager.Instance.OpenWindow(WinNames.MatrixInfoPanel);
        WindowManager.Instance.CloseWindow(WinNames.MatrixLevelPanel, true);
        int lelCount = MatrixModel.Instance.GetLevelCount((int)MatrixType.Graphics);
        MatrixWindow.Instance.StartGame(MatrixType.Graphics, lelCount);
    }

    void OnAddsubClick()
    {
        WindowManager.Instance.OpenWindow(WinNames.MatrixInfoPanel);
        WindowManager.Instance.CloseWindow(WinNames.MatrixLevelPanel, true);
        int lelCount = MatrixModel.Instance.GetLevelCount((int)MatrixType.Addsub);
        MatrixWindow.Instance.StartGame(MatrixType.Addsub, lelCount);
    }
    private void OnButtonReturnClick()
    {
        Debug.Log(" OnButtonReturnClick ==== ");
        if (EasyLoadingEx.IsLoading)
            return;
        _return_btn.onClick.RemoveAllListeners();
        AudioManager.Instance.PauseBackgroudMusic();

        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
        WindowManager.Instance.CloseWindow(WinNames.EnglishLediAnimationPanel);
        TimerManager.Instance.RemoveAllTimer();
        Input.multiTouchEnabled = true;
        WindowManager.Instance.CloseWindow(WinNames.MatrixInfoPanel);
        MatrixWindow.Instance.OnDestroy();
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }



    #endregion
}


﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MatrixSpaceItem : MonoBehaviour
{
    //对应格子从左往右
    public int index;
    public string resultName;
    Image imgBg;
    Image imgDes;
    GameObject goDes;
    /// <summary>
    /// 是否拥有图片了
    /// </summary>
    public bool IsOwnPg { get; set; }
    // Use this for initialization
    void Awake()
    {
        imgBg = transform.GetComponent<Image>();
        imgDes = transform.GetChild(0).GetComponent<Image>();
        goDes = transform.GetChild(0).gameObject;
        IsOwnPg = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetDefaultSpace(MatrixType type, string spriteName, int rotation)
    {
        //Matrix_Num
        IsOwnPg = true;
        string atlas = MatrixModel.Instance.GetAtlas(type);        
        goDes.SetActive(true);
        imgBg.sprite = CommonUtility.GetSprite(emUIAltas.StudyMatrixAtlas, "ljjz-bg03");
        imgBg.SetNativeSize();

        //imgDes.sprite = CommonUtility.GetSprite(atlas, spriteName);
        imgDes.sprite = ResourceManager.Instance.GetResource(atlas + spriteName, typeof(Sprite), enResourceType.Prefab).m_content as Sprite;
        imgDes.SetNativeSize();
        imgDes.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, rotation));
    }

    public void SetItemBg(bool isSpace, bool isEnable = true)
    {
        //Debug.Log("  SetItemBg === "  );
        if (isSpace)
        {
            imgBg.sprite = CommonUtility.GetSprite(emUIAltas.StudyMatrixAtlas, "ljjz-bg04");
            imgBg.SetNativeSize();
        }
        else
        {
            if (isEnable)
            {
                imgBg.color = new Color(1, 1, 1, 1);
                imgBg.sprite = CommonUtility.GetSprite(emUIAltas.StudyMatrixAtlas, "ljjz-bg03");
                imgBg.SetNativeSize();
            }
            else
                imgBg.color = new Color(0, 0, 0, 0);
        }
    }

    public void ResetItem()
    {
        imgBg.color = new Color(1, 1, 1, 1);
        IsOwnPg = false;
        goDes.SetActive(false);
    }
}

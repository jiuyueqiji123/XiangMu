﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using WCBG.ToolsForUnity.Tools;

public class FootballGame : BaseToyBoxGame
{
    FootballEventListen FootBallListen;

    private float yinliang = 1;

    private const string ASSET_PATH = "game_toybox/game_toybox_football/FootballGameRoot";
    public string UI_PATH = "game_toybox/game_toybox_ui/FootBallGameFrom";

    private GameObject GameRoot;
    private AudioClip EnterGameAudio;
    private AudioClip StartGameAudio;
    private AudioClip AbnormalAudio;
    private AudioClip ExitGameAudio;
    private bool IsBallAdmission = false;
    private bool IsLediMove = false;
    private bool IsLediMovePlayAnimitor = false;
    private bool IsPlaying = false;
    private bool IsBackClick = false;
    private bool isFootIn = true;

    private int ClickTime = 0;
    private int ExceptionTime = 0;
    private float Timer = 0f;
    private float LediMoveSpeed = 1;
    private float LediMoveDistance = 0.5f;
    private float Index = 0;
    private float SinSpeed = 0.05f;
    private float MoveYSpeed = 1.2f;
    private float MaxHeight = 100;
    private float MinHeight = 6;
    private float StartPosX = 240f;
    private float StartPosZ = 0f;

    private Transform lediPos;
    private Transform ballLeftPos;
    private Transform ballRightPos;
    public GameObject Football;
    public HUIFormScript From;
    public GameObject Up_Btn;
    private bool Isclick = false;

    ActorAnimatorComponent ActorComponent;

    public override void OnStart(System.Action<bool> callback)
    {
        base.OnStart(callback);
        From = HUIManager.Instance.OpenForm(this.UI_PATH,false);
        Up_Btn = this.From.GetWidget("up_btn");
        HUIUtility.SetUIMiniEvent(this.Up_Btn,enUIEventType.Click,enUIEventID.FootBallGameReturnClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.FootBallGameReturnClick,ExitGame);
        Debug.Log("--------------- FootballGame OnStart!");

        
        CreateGameRoot();
        //FootBallListen = Football.gameObject.GetComponent<FootballEventListen>();
        Camera.main.gameObject.AddComponent<InputPhysicsRaycaster>();
        EnterGameAudio = ResourceManager.Instance.GetResource("game_toybox/game_toybox_sound/401010302", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip;
        StartGameAudio = ResourceManager.Instance.GetResource("game_toybox/game_toybox_sound/401010303", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip;
        AbnormalAudio = ResourceManager.Instance.GetResource("game_toybox/game_toybox_sound/401010305", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip;
        ExitGameAudio = ResourceManager.Instance.GetResource("game_toybox/game_toybox_sound/401010304", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip;
        FEasyInput.AddEvent(EEasyInputType.OnUp,OnFootBallClick);
        actor.TurnFront(() =>
        {
            AudioManager.Instance.PlaySound(EnterGameAudio);
            PlayAnimator("all_talk_02", 0.2f, true);
        });
        TimerManager.Instance.AddTimer(EnterGameAudio.length,()=>
        {
            base.SetDestination(new Vector3(0, 6, -140), () =>
              {
                  Debug.LogError("转转转");
                  lediTransform.DORotate(new Vector3(0, 0, 0), 0.35f);
                  IsBallAdmission = true;
              });
        });




        //this.AddTimerEx(2.6f,()=>
        //{
        //    lediTransform.DORotate(new Vector3(0, 0, 0), 0.35f);
        //});


        //lediTransform.DOMove(lediPos.position, 2);
        //this.AddTimerEx(10f, () => {

        //    OnGameComplete(true);
        //});
    }

    public override void OnExit()
    {
        base.OnExit();
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.FootBallGameReturnClick, ExitGame);
    }

    public override void OnUpdate()
    {
        Debug.LogError("-------------------------------------- GameIsPlaying");
        base.OnUpdate();
        //Debug.LogError(lediTransform.localPosition.z);
        //if (lediTransform.localPosition.z == -140)
        //{
        //    Debug.LogError("----------------- NO");

        //    lediTransform.position += new Vector3(0,0,-0.01f);

        //    //BallAdmission();
        //}

        //Debug.LogError(Timer);
        //if (Input.GetMouseButton(1))
        //{
        //    OnExitGame(false);
        //}
        if (IsPlaying)
        {
            if (Input.GetMouseButtonDown(0) || ClickTime == 15)
            {
                Timer = 0;
                ExceptionTime = 0;
                Debug.Log(Timer + "--------------------------------------------------Click");

            }
            Timer += Time.deltaTime;
            ExceptionHandling(Timer);
        }
        
        

        if (IsBallAdmission)
        {
            FootBallAdmission();
        }

        if (IsLediMove)
        {
            LediMove();
        }
        
        
    }

    public void ExitGame(HUIEvent evt)
    {
        HUIManager.Instance.CloseForm(this.UI_PATH);
        this.OnExitGame(false);
    }



    public void OnExitGame(bool IsCarryOut)
    {
        Timer = 0;
        IsLediMove = false;
        IsBackClick = true;
        TimerManager.Instance.RemoveAllTimer();
        //WindowManager.Instance.CloseWindow(WinNames.FootBallPanel);
        FEasyInput.RemoveEvent(EEasyInputType.OnUp,OnFootBallClick);
        if (IsCarryOut)
        {
            AudioManager.Instance.PlaySound("game_toybox/game_toybox_sound/401010304");
            PlayAnimator("all_praise_p_1", 0.2f, true);

            TimerManager.Instance.AddTimer(AudioManager.Instance.GetAudioClip("game_toybox/game_toybox_sound/401010304").length, () =>
            {
                AudioManager.Instance.PlaySound("game_toybox/game_toybox_sound/601010203");
                PlayAnimator("shoot2", 0.2f, false);
                this.AddTimerEx(0.55f, () =>
                {
                    Football.transform.DOMove(new Vector3(-300, 160, 10), 0.8f);
                });
                this.AddTimerEx(0.8f, () =>
                {

                    base.Agent.enabled = true;
                    IsLediMove = false;

                    //this.SetDestination(new Vector3(0, 2f, -20), () =>
                    //{
                    OnGameComplete(true);
                    HUIManager.Instance.CloseForm(this.UI_PATH);
                    GameObject.Destroy(this.GameRoot);
                    
                    //});

                });

            });
        }
        else
        {
            this.Football.transform.position = new Vector3(-300, 160, 10);
            AudioManager.Instance.PlaySound("game_toybox/game_toybox_sound/401010306");
            this.PlayAnimator("all_praise_p_1", 0.2f, true);
            TimerManager.Instance.AddTimer(AudioManager.Instance.GetAudioClip("game_toybox/game_toybox_sound/401010306").length, () =>
            {
                base.Agent.enabled = true;
                IsLediMove = false;
                //                 this.SetDestination(new Vector3(0, 2f, -20), () =>
                //                 {
                PlayAnimator("idell_p", 0.2f, true);
                PoolObjHandle<ActorRoot> actor = ActorObjManager.Instance.GetActor(ActorNames.LeDi);
                actor.handle.StopAllAnimator();
                OnGameComplete(true);
                HUIManager.Instance.CloseForm(this.UI_PATH);
                GameObject.Destroy(this.GameRoot);
                /*});*/
            });
        }

        
    }

    private void CreateGameRoot()
    {
        GameObject go = Res.LoadObj(ASSET_PATH);

        if (go != null)
        {
            GameRoot = go;
            go.transform.SetParent(toyboxGameRoot);
            lediPos = go.transform.Find("LediPos");
            ballLeftPos = go.transform.Find("FootballLeft");
            ballRightPos = go.transform.Find("FootballRight");
            Football = go.transform.Find("FootBall").gameObject;
        }
    }

    public void ExceptionHandling(float Timer)
    {
        if (Timer >= 5 && ExceptionTime == 0)
        {
            
            
            this.PlayAnimator("all_tal10_zuo", 0.2f, true);
            AudioManager.Instance.PlaySound("game_toybox/game_toybox_sound/401010305");
            this.AddTimerEx(AudioManager.Instance.GetAudioClip("game_toybox/game_toybox_sound/401010305").length,()=>
            {
                this.PlayAnimator("move_crosswise_p_zuo", 0.2f, true);
                IsLediMove = true;
                Timer = 5;
            });
            ExceptionTime++;
        }
        if (Timer >=15 && ExceptionTime == 1)
        {
            IsPlaying = false;
            ExceptionTime = 2;
            OnExitGame(false);
        }
    }

    public void FootBallAdmission()
    {
        if (IsBackClick)
        {
            return;
        }
        //Debug.LogError("111111111111111111111111111");
        if (isFootIn)
        {
            
            isFootIn = false;
        }
        StartPosX -= MoveYSpeed;
        StartPosZ -= MoveYSpeed;
        if (StartPosX <= 5)
        {
            StartPosX = 5f;
            MoveYSpeed = 0;
            Index = 0;
            IsBallAdmission = false;
            AudioManager.Instance.PlaySound(StartGameAudio);
            Football.transform.GetChild(0).GetComponent<Animator>().enabled = false;
            PlayAnimator("all_talk7_p_1", 0.2f, true);
            TimerManager.Instance.AddTimer(StartGameAudio.length + 0.15f,()=>
            {
                base.Agent.enabled = false;
                
                LediKickTheBall();
            });
            
        }
        if (StartPosZ <= -110)
        {
            StartPosZ = -110;
        }
        if (MaxHeight <= 0.25)
        {
            MaxHeight = 0;
            

        }
        Football.transform.position = new Vector3(StartPosX, Mathf.Abs(Mathf.Sin(Index)) * MaxHeight + MinHeight, StartPosZ);
        Index += SinSpeed;

        if (Index >= 3.2)
        {
            AudioManager.Instance.PlaySound(AudioManager.Instance.GetAudioClip("game_toybox/game_toybox_sound/601010201"),1,false,yinliang);
            MaxHeight = MaxHeight * 0.5f;
            SinSpeed += 0.0075f;
            MoveYSpeed -= 0.0001f;
            Index = 0;
            yinliang = 0.5f * yinliang;

        }
    }

//     public void OnInputDown(InputEventData eventData)
//     {
//         OnFootBallClick();
//     }

    public void OnFootBallClick()
    {
        if (Isclick == false)
        {
            return;
        }
        
        Debug.LogError("------------------------------------- onclick");
        Transform football = FTools.GetRaycastHitTargetByMousePoint();
        //AnimatorClipInfo animationforfootball;
        if (football.name != this.Football.name)
        {
            return;
        }
        if (football.name == this.Football.name)
        {
            Isclick = false;
            IsLediMove = false;
            AudioManager.Instance.PlaySound("game_toybox/game_toybox_sound/601010202");
            this.PlayAnimator("move_crosswise_p", 0.2f, true);
            ClickTime++;
            Vector3 FootBallPos = new Vector3(lediTransform.position.x, 6, -110);
            Football.transform.DOMove(FootBallPos, 1f);
            Football.transform.GetChild(0).GetComponent<Animator>().enabled = true;
            //animator.speed = -0.6f;
            Football.transform.GetChild(0).GetComponent<Animator>().Play("down");
            //this.AddTimerEx(1f,()=>
            //{
            //    Football.transform.GetChild(0).GetComponent<Animator>().enabled = false;
            //});
            //Debug.LogError(ClickTime);
            if (ClickTime == 15)
            {
                this.AddTimerEx(0.2f,()=>
                {
                    Football.transform.GetChild(0).GetComponent<Animator>().enabled = false;
                });
                
                OnExitGame(true);
                return;
            }
            this.AddTimerEx(0.2f, () =>
            {

                LediKickTheBall();
            });

        }
        
    }

    public void LediMove()
    {
        //Debug.Log("----------------- LediMove");
        
        //         if (Random.Range(0,1) >= 0.5)
        //         {
        //             i = -i;
        //         }

        if (lediTransform.position.x > 40 || lediTransform.position.x < -40)
        {
            LediMoveDistance = -LediMoveDistance;
        }
        float x = (lediTransform.position.x + LediMoveDistance) * LediMoveSpeed;
        if (IsLediMovePlayAnimitor)
        {
            PlayAnimator("move_crosswise_p_zuo", 0.2f, true);
            IsLediMovePlayAnimitor = false;
        }
        lediTransform.position = new Vector3(x, lediTransform.position.y,lediTransform.position.z);
    }

    public void LediKickTheBall()
    {
        

        this.AddTimerEx(0.2f,()=>
        {
            AudioManager.Instance.StopAllSound();
            AudioManager.Instance.PlaySound("game_toybox/game_toybox_sound/401010232");
            AudioManager.Instance.PlaySound("game_toybox/game_toybox_sound/601010202");
        });
        PlayAnimator("shoot",0.2f,false,(str)=> {
            PlayAnimator("idell_p",0.1f,true);
        });
        this.AddTimerEx(0.5f,()=>
        {
            Football.transform.DOMove(RandomPos(), 1f);
            Football.transform.GetChild(0).GetComponent<Animator>().enabled = true;
            //Football.transform.GetChild(0).GetComponent<Animator>().
            Football.transform.GetChild(0).GetComponent<Animator>().Play("up");
            this.AddTimerEx(1f, () =>
            {
                Football.transform.GetChild(0).GetComponent<Animator>().enabled = false;
                
            });
            this.AddTimerEx(1.2f, () =>
            {
                Isclick = true;

            });
        });
        TimerManager.Instance.AddTimer(0.8f,()=>
        {
            Debug.LogError("---------------------------------------- LeDiMove");
            IsLediMove = true;
            
            IsLediMovePlayAnimitor = true;
        });
        IsPlaying = true;
        //lediTransform.DORotate(new Vector3(0,0,0),0.35f);
    }

    public Vector3 RandomPos()
    {
        float x = 0;
        x = Random.Range(ballLeftPos.position.x,ballRightPos.position.x);
        return new Vector3(x,6,ballRightPos.position.z);
    }
    public void StopAllAnimitor()
    {

    }
}

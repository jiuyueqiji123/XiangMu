﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FootballWindow :  SingletonBaseWindow<FootballWindow>{

	public Button _up_btn { get; set; }

    FootballGame footballgame = new FootballGame();

    protected override void AddListeners()
    {
        base.AddListeners();
        //_up_btn.gameObject.AddComponentEx<UIInputHandler>().OnClick += footballgame.OnExitGame;
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();
        //_up_btn.gameObject.GetComponent<UIInputHandler>().OnClick -= footballgame.OnExitGame;
    }
}

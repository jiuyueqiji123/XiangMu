﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWashEx
{
    bool IsOptBegin { get; }
    void OnUpdate();
    void OnTouchUp();
    void OnHover();
    void OnTouchBegin();
}

public class WashController : MonoSingleton<WashController> {

    public static WashState washState = WashState.Soap;

    public WashBodyGame _washbodyGame;

    public Action washGameFinishedEvent;

    private IWashEx m_currentWashEx;

    public bool _enableInput;

    public AnimatorEx AnimatorLedi
    {
        get { return _washbodyGame.animatorEx; }
    }

    protected override void Init()
    {
        base.Init();

        ChangeState(WashState.Ready);

        FEasyInput.AddEvent(EEasyInputType.OnDown, OnTouchBegin);
        FEasyInput.AddEvent(EEasyInputType.OnHover, OnTouchHover);
        FEasyInput.AddEvent(EEasyInputType.OnUp, OnTouchUp);
        _enableInput = true;
    }

    public void ChangeState(WashState newstate)
    {
        switch (newstate)
        {
            case WashState.None:
                break;
            case WashState.Ready:
                break;
            case WashState.Soap:
                break;
            case WashState.Clean:
                break;
            case WashState.WipeAndDry:
                break;
            case WashState.Finish:
                Debug.Log("清洗完成！");
                if (washGameFinishedEvent != null) washGameFinishedEvent();
                break;
            default:
                break;
        }
        washState = newstate;

        switch (washState)
        {
            case WashState.Soap: m_currentWashEx = SoapEx.Instance; break;
            case WashState.Clean: m_currentWashEx = CleanEx.Instance; break;
            case WashState.WipeAndDry: m_currentWashEx =WipeAndDryEx.Instance; break;
        }
    }

    private void Update()
    {
        if(m_currentWashEx != null)
            m_currentWashEx.OnUpdate();
    }

    private void OnTouchUp()
    {
        //Debug.Log("wash touch up");
        if (m_currentWashEx != null && _enableInput)
            m_currentWashEx.OnTouchUp();
    }

    private void OnTouchHover()
    {
        //Debug.Log("wash touch hover");
        if (m_currentWashEx != null && _enableInput)
            m_currentWashEx.OnHover();
    }

    private void OnTouchBegin()
    {
        //Debug.Log("wash touch begin");
        if (m_currentWashEx != null && _enableInput)
            m_currentWashEx.OnTouchBegin();
    }

    public void PlayExitAnimate()
    {

        WashResController.Instance.CloseAllDragItemInteractive();

        if (washState == WashState.Soap && SoapEx.Instance.IsOptBegin)
        {
            AnimatorLedi.Play(eAnimLediClean.sheke_body_a.ToString()).OnFinished(() => { _washbodyGame.ExitGameAnimation(); });
            SoapEx.Instance.DispearAllFoam();
        }
        else if (washState == WashState.Clean)
        {
            AnimatorLedi.Play(eAnimLediClean.sheke_body_a.ToString()).OnFinished(() => { _washbodyGame.ExitGameAnimation(); });
            SoapEx.Instance.DispearAllFoam();
            WashResController.Instance.CleanWaterEffect();
        }
        else if (washState == WashState.WipeAndDry && WipeAndDryEx.Instance.IsOptBegin)
        {
            AnimatorLedi.Play(eAnimLediClean.sheke_body_a.ToString()).OnFinished(() => { _washbodyGame.ExitGameAnimation(); });
            WashResController.Instance.CleanWaterEffect();
        }
        else
        {
            _washbodyGame.ExitGameAnimation();
        }

    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        SoapEx.DestroyInstance();
        CleanEx.DestroyInstance();
        WipeAndDryEx.DestroyInstance();

        FEasyInput.RemoveEvent(EEasyInputType.OnDown, OnTouchBegin);
        FEasyInput.RemoveEvent(EEasyInputType.OnHover, OnTouchHover);
        FEasyInput.RemoveEvent(EEasyInputType.OnUp, OnTouchUp);
    }

}

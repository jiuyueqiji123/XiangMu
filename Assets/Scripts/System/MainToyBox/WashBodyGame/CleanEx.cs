﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;
using DG.Tweening;

public class CleanEx : Singleton<CleanEx>, IWashEx
{

    public bool isCleanFinished;

    public float waterColumnRadius = 20;

    private AnimatorEx animatorEx;

    private bool m_isOnCleaning = false;

    private int totalFoamCount;

    public bool IsOptBegin
    {
        get
        {
            return true;
        }
    }

    private GameAudioSource cleanAudioEffect;
    public bool IsPlayingEffetAudio
    {
        get
        {
            if (cleanAudioEffect != null)
                return cleanAudioEffect.IsPlaying;
            return false;
        }
        set
        {

            if (value)
            {
                if (cleanAudioEffect == null || !cleanAudioEffect.IsPlaying)
                    cleanAudioEffect = AudioManager.Instance.PlaySound(WashResController.Instance.GetAudioPath(WashResController.audio_effect_penshui),true, true);
            }
            else
            {
                if (cleanAudioEffect != null && cleanAudioEffect.IsPlaying)
                    cleanAudioEffect.Stop();
            }

        }
    }

    public override void Init()
    {
        base.Init();

        animatorEx = WashController.Instance.AnimatorLedi;
        animatorEx.PlaySequence(eAnimLediClean.talk1_a_1.ToString(), WashResController.Instance.GetAudioPath(WashResController.audio_chongshui));

        WashResController.Instance.nozzle.MoveIn();
        isCleanFinished = false;

        totalFoamCount = SoapEx.Instance.FoamList.Count;

    }

    public void OnHover()
    {
        if (WashResController.Instance.nozzle.IsOnDragging)
        {
            DoShower();
        }
    }

    public void OnTouchBegin()
    {
        
    }

    public void OnTouchUp()
    {
        IsPlayingEffetAudio = false;
        if (m_isOnCleaning)
        {
            m_isOnCleaning = false;
            animatorEx.CrossFade(eAnimLediClean.enjoy_a_3.ToString(), .2f);
        }

        if (isCleanFinished)
        {
            WashResController.Instance.nozzle.EnableInteractive(false);
            WashResController.Instance.nozzle.MoveOut();
            TimerManager.Instance.AddTimer(.5f, () => {
                WashController.Instance.ChangeState(WashState.WipeAndDry);
            });
        }
    }

    public void OnUpdate()
    {
        animatorEx.OnUpdate();
    }

    public void DoShower()
    {
        if (isCleanFinished) return;

        List<FoamControl> FoamList = SoapEx.Instance.FoamList;
        Nozzle nozzle = WashResController.Instance.nozzle;
        //Ray ray = new Ray(nozzle.m_nozzlePoint.position, nozzle.m_nozzlePoint.forward);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Wash")))
        {

            if (!m_isOnCleaning)
            {
                IsPlayingEffetAudio = true;
                m_isOnCleaning = true;
                animatorEx.CrossFade(eAnimLediClean.enjoy_a_1.ToString(), .2f);
            }

            Vector2 hitpoint = new Vector2(hit.point.x, hit.point.y);

            int length = FoamList.Count;
            for (int i = 0; i < length; i++)
            {
                Vector2 foamPoint = new Vector2(FoamList[i].transform.position.x, FoamList[i].transform.position.y);
                float distance = Vector2.Distance(hitpoint, foamPoint);
                if (distance < waterColumnRadius)
                {
                    FoamList[i].Shower(0f, SoapEx.Instance);
                }

                //Debug.Log(FoamList.Count);
            }
        }
        else
        {
            if (m_isOnCleaning)
            {
                m_isOnCleaning = false;
                animatorEx.CrossFade(eAnimLediClean.enjoy_a_3.ToString(), .2f);
            }
        }

        //这里不清楚
        if (FoamList.Count <= 0)
        {
            Debug.Log("clean finished-------");
            isCleanFinished = true;
        }

        if (FoamList.Count >= 0)
        {
            float progress = (float)FoamList.Count / totalFoamCount;
            WashResController.Instance.SetWaterDropProgress(1 - progress);
        }
    }

    public override void UnInit()
    {
        base.UnInit();
    }
}

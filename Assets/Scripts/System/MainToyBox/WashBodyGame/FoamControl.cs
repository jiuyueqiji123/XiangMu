﻿using UnityEngine;
using System.Collections;

public class FoamControl : MonoBehaviour {

    [HideInInspector]
    public bool Isliving;
    [HideInInspector]
    public bool Isdying;

    private float DisappearTime = 0.2f;
    private Vector3 FinalScale;
    public float MinScale = 0.65f;
	public float MaxScale = 0.85f;
	public bool NeedBubble = true;
    private float Scale;
    private float time = 0.0f;
    private GameObject Bubble;
	// Use this for initialization
	void Start () 
    {
        transform.localScale = Vector3.zero;
        float x = Random.Range(MinScale, MaxScale);
        FinalScale = new Vector3(x, x, x);
        StartCoroutine(this.Appear());
        Isliving = true;
        Isdying = false;
	}

    public void Shower(float offset, SoapEx control)
    {
        if (!Isdying)
        {
            DisappearTime -= Time.deltaTime;
            if (DisappearTime < 0f)
            {
                StartCoroutine(Disappear(control,offset));
            }
        }
    }


    private IEnumerator Appear()
    {
        float sina;
        while (time < 1.0f)
        {
            time += Time.deltaTime * 5.0f;
            transform.localScale = Vector3.Lerp(Vector3.zero, FinalScale,time);
			//Debug.Log("transform.localScale:" + transform.localScale);
            yield return null;
        }

        time = 0;
        while (time < 12.56637f)
        {
            time += Time.deltaTime * 22.0f;

            sina = Mathf.Sin(time) * 0.015f;
            if(time >  6.283185f)
            {
                sina *= 2;
            }

            transform.localScale = new Vector3(transform.localScale.x + sina, transform.localScale.y + sina, transform.localScale.z + sina);
			//Debug.Log("transform.localScale:" + transform.localScale);
            yield return null;
        }

		//if (Random.Range((float)0f, (float)1f) <= 0.4f && NeedBubble)
  //      {
  //          Bubble = GameObject.Instantiate(SoapEx.Instance.Bubble.gameObject) as GameObject;

  //          Bubble.transform.position = this.transform.position;
  //          Bubble.SetActive(true);
  //      }

        Isliving = false;
    }

    public IEnumerator Grow()
    {
        //float sina;
        Isliving = true;
        time = 0;
        Vector3 startscale = transform.localScale;
        Vector3 finalscale = startscale * 1.1f;

        //if (Bubble != null)
        //{
        //    Bubble.SetActive(false);
        //    Bubble.SetActive(true);
        //}

        while (time < 1.0f)
        {
            time += Time.deltaTime * 5.0f;
            transform.localScale = Vector3.Lerp(startscale, finalscale, time);
            yield return null;
        }

        if (Random.Range((float)0f, (float)1f) <= 0.5f)
        {
            Isliving = false;
        }
    }

    public IEnumerator Disappear(SoapEx showercontrol,float offset)
    { 
        time = 0;
        Isdying = true;
        Vector3 scale = transform.localScale;
        float currenttime = Random.Range((float)0.1f, (float)0.3f) + offset;

        //if (Bubble != null)
        //{
        //    Bubble.SetActive(false);
        //    Bubble.SetActive(true);

        //    BubbleDisappear dis = Bubble.GetComponent<BubbleDisappear>();
        //    dis.StartCoroutine(dis.DelayToDestroy());
        //}

        yield return new WaitForSeconds(currenttime);

        while (time < 1.0f)
        {
            time += Time.deltaTime * 2.4f;
            transform.localScale = Vector3.Lerp(scale, Vector3.zero, time);
            transform.position -= (Vector3) (new Vector3(0f, 0.25f, 0f) * Time.deltaTime);
            yield return null;
        }

        showercontrol.FoamList.Remove(this);
        if (showercontrol.FoamList.Count <= 0)
        {
            //打开角色碰撞器
        }
        Destroy(this.gameObject);
    }

}

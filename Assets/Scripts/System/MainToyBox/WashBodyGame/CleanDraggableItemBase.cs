﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public abstract class CleanDraggableItemBase : MonoBehaviour {

    public Rect m_colliderPlaneRect = new Rect(-100, 100, 200, 100);

    public float lerpRadio = 6f;

    protected Transform m_moveInTrans;
    protected Transform m_moveOutTrans;
    protected Transform m_showTrans;
    protected Vector3 m_showtrans_targetPosition;
    protected Quaternion m_showtrans_targetRotation;
    protected bool m_isOptStateChanged;
    protected bool m_Is_EnterHit;

    public bool IsOnDragging { get; set; }

    protected Vector3 m_last_targetPosition;

    protected Plane plane;
    protected Transform m_player;
    public float offsetOnNormal = .5f;

    protected virtual void Start()
    {
        m_moveInTrans = WashResController.Instance.moveInPoint;
        m_moveOutTrans = WashResController.Instance.moveOutPoint;
        m_player = WashResController.Instance.player;
        IsOnDragging = false;

        transform.position = m_moveInTrans.position;
    }

    protected void Update()
    {
        if (m_isOptStateChanged)
        {
            float distanceZ = Mathf.Abs(transform.position.z - m_showtrans_targetPosition.z);
            float lerpRadio = Time.deltaTime * (this.lerpRadio + (float)offsetOnNormal / (distanceZ + 2));
            transform.position = Vector3.Lerp(transform.position, m_showtrans_targetPosition, lerpRadio);
            transform.rotation = Quaternion.Lerp(transform.rotation, m_showtrans_targetRotation, lerpRadio);
        }
    }

    public void EnableInteractive(bool value)
    {
        GetComponent<Collider>().enabled = value;
        if(Vector3.Distance(transform.position, m_moveOutTrans.position) > 0.2)
            MoveToStart();
    }

    public void MoveIn()
    {
        transform.DOMove(m_showTrans.position, 1f);
    }

    public void MoveToStart()
    {
        transform.DOMove(m_moveInTrans.position, 1f);
    }

    public void MoveOut()
    {
        transform.DOMove(m_moveOutTrans.position, 1f);
    }

    public void MoveBack()
    {
        m_isOptStateChanged = false;
        m_Is_EnterHit = false;
        m_showtrans_targetPosition = m_showTrans.position;
        transform.DORotate(m_showTrans.eulerAngles, .5f);
        transform.DOMove(m_showTrans.position, .5f).OnComplete(() => {
            IsOnDragging = false;
        });
    }

}

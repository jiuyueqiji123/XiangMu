﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;
using DG.Tweening;

public class WipeAndDryEx : Singleton<WipeAndDryEx>, IWashEx
{

    public bool isWipeAndDryFinished;

    private bool isOnWipping = false;

    public float wipeRadius = 20;

    [HideInInspector]
    public Transform ragCollider;

    private float timer;

    private float totalWipeTime = 5f;

    private float wipeProgress;

    private AnimatorEx animatorEx;

    public bool IsOptBegin
    {
        get
        {
            return wipeProgress < 0.95;
        }
    }

    private GameAudioSource wipeAudioEffect;
    public bool IsPlayingEffetAudio
    {
        get
        {
            if (wipeAudioEffect != null)
                return wipeAudioEffect.IsPlaying;
            return false;
        }
        set
        {

            if (value)
            {
                if (wipeAudioEffect == null || !wipeAudioEffect.IsPlaying) wipeAudioEffect = AudioManager.Instance.PlaySound(WashResController.Instance.GetAudioPath(WashResController.audio_effect_cafeiji), true);
            }
            else
            {
                if (wipeAudioEffect != null && wipeAudioEffect.IsPlaying)
                    wipeAudioEffect.Stop();
            }

        }
    }

    public override void Init()
    {
        base.Init();

        WashResController.Instance.rag.MoveIn();

        ragCollider = WashResController.Instance.collider_body.Find("VBBody");
        ragCollider.SetActive(true);
        animatorEx = WashController.Instance.AnimatorLedi;
        animatorEx.PlaySequence(eAnimLediClean.talk1_a_1.ToString(), WashResController.Instance.GetAudioPath(WashResController.audio_cashui));

        isWipeAndDryFinished = false;

    }

    public void OnHover()
    {
        if (WashResController.Instance.rag.IsOnDragging)
        {
            if (isWipeAndDryFinished) return;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Wash")))
            {

                if (!isOnWipping)
                {
                    isOnWipping = true;
                    IsPlayingEffetAudio = true;
                    animatorEx.CrossFade(eAnimLediClean.enjoy_a_1.ToString(), .2f);
                }

                timer += Time.deltaTime;
                wipeProgress = 1 - (timer / totalWipeTime);
                WashResController.Instance.SetWaterDropProgress(wipeProgress);

                if (timer > totalWipeTime)
                {
                    Debug.Log("wipe and dry over------");
                    timer = 0;
                    isWipeAndDryFinished = true;
                }
            }
            else
            {
                if (isOnWipping)
                {
                    IsPlayingEffetAudio = false;
                    isOnWipping = false;
                    animatorEx.CrossFade(eAnimLediClean.enjoy_a_3.ToString(), .2f);
                }
            }
        }
    }

    public void OnTouchBegin()
    {
        
    }

    public void OnTouchUp()
    {
        if (isOnWipping)
        {
            IsPlayingEffetAudio = false;
            isOnWipping = false;
            animatorEx.CrossFade(eAnimLediClean.enjoy_a_3.ToString(), .2f);
        }

        if (isWipeAndDryFinished)
        {
            //停止输入事件

            WindowManager.Instance.CloseWindow(WinNames.LediCleanPanel);
            WashController.Instance._enableInput = false;
            WashResController.Instance.rag.EnableInteractive(false);
            WashResController.Instance.rag.MoveOut();
            AudioManager.Instance.PlaySound(WashResController.Instance.GetAudioPath(WashResController.audio_effect_ganjing));
            WashResController.Instance.wipefinishEffect.SetActive(true);
            TimerManager.Instance.AddTimer(.5f, () => {
                animatorEx.PlaySequence(eAnimLediClean.talk1_a_1.ToString(), WashResController.Instance.GetAudioPath(WashResController.audio_ganxie), () => {

                    ////这里等待时间较长，需要重置一下计时器
                    WashResController.Instance.wipefinishEffect.SetActive(false);
                    EventBus.Instance.BroadCastEvent(EventID.RESET_TIMER);
                    animatorEx.Play(eAnimLediClean.a_t_p_wheel.ToString()).OnFinished(() => {
                        ragCollider.SetActive(false);
                        WashController.Instance.ChangeState(WashState.Finish);
                    });
                });
            });
        }
    }

    public void OnUpdate()
    {
        animatorEx.OnUpdate();
    }

    public override void UnInit()
    {
        base.UnInit();
    }
}

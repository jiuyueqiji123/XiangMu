﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WashResController : Singleton<WashResController> {



    private const string washbody_sound_basepath = "game_toybox/game_toybox_sound/";
    private const string wash_animate_basepath = "game_toybox/game_toybox_anim/";
    private const string washitem_basepath = "game_toybox/washbody/";

    public const string audio_qingjie = "401010307";
    public const string audio_tufeizao = "401010308";
    public const string audio_chongshui = "401010309";
    public const string audio_cashui = "401010310";
    public const string audio_ganxie = "401010311";
    public const string audio_xiaci = "401010312";
    public const string audio_kuaidian = "404020602";

    public const string audio_effect_tufeizao = "601010204";
    public const string audio_effect_penshui = "601010205";
    public const string audio_effect_cafeiji = "601010206";
    public const string audio_effect_ganjing = "601010207";

    public const string effect_xingxing = "502002001";
    public const string effect_huasa = "502002002";

    private const string soap_path = "301010203";
    private const string towel_path = "301010205";
    private const string shower_path = "301010204";
    private const string foam_path = "9235507";
    private const string tb_body = "BodyAnchor";
    private const string tb_swing = "SwingAnchor";

    private const string root = "TBWing";

    private GameObject WashBodyGameRoot;

    public RuntimeAnimatorController m_lediCleanAnimator;

    public Transform collider_root;

    public Transform collider_body;

    public Transform collider_wing;

    public Transform player;

    public Transform soapPoint;

    public Transform nozzlePoint;

    public Transform ragPoint;

    public Transform moveInPoint;

    public Transform moveOutPoint;

    public Transform wipefinishEffect;

    public Nozzle nozzle;

    public Rag rag;

    public Soap soap;

    public GameObject Foam;

    public List<SkinnedMeshRenderer> lediSkin;

    public DOTweenPath dotweenPath;

    public override void Init()
    {
        base.Init();

        WashBodyGameRoot = GetActiveObject("game_toybox", "WashBodyGameRoot");
        Transform root = WashBodyGameRoot.transform.Find("RootObject");
        Transform itemAnchor = root.transform.Find("ItemAnchor");
        nozzle = root.Find("Nozzle").GetComponent<Nozzle>();
        rag = root.Find("Rag").GetComponent<Rag>();
        soap = root.Find("Soap").GetComponent<Soap>();
        wipefinishEffect = root.Find("502002001"); 
        soapPoint = itemAnchor.transform.Find("SoapShowPoint");
        nozzlePoint = itemAnchor.transform.Find("FoamShowPoint");
        ragPoint = itemAnchor.transform.Find("RagShowPoint");
        moveInPoint = itemAnchor.transform.Find("MoveInPoint");
        moveOutPoint = itemAnchor.transform.Find("MoveOutPoint");

        dotweenPath = WashBodyGameRoot.transform.GetComponentInChildren<DOTweenPath>();

        PoolObjHandle<ActorRoot> actor = ActorObjManager.Instance.GetActor(ActorNames.LeDi);
        player = actor.handle.gameObject.transform;
        Transform[] childs = player.GetComponentsInChildren<Transform>();
        lediSkin = new List<SkinnedMeshRenderer>();
        for (int i = 0; i < childs.Length; i++)
        {
            if (childs[i].name == "Bip001 Spine") collider_root = childs[i].transform;
            if (childs[i].name == "101001000_head")
            {
                lediSkin.Add(childs[i].GetComponent<SkinnedMeshRenderer>());
            }
        }

        collider_body = GetActiveObject(tb_body).transform;
        collider_body.SetParent(collider_root);
        collider_body.ResetTransformForLocal();

        collider_wing = GetActiveObject(tb_swing).transform;
        collider_wing.SetParent(collider_root);
        collider_wing.ResetTransformForLocal();

        Foam = Res.LoadObj<GameObject>(washitem_basepath + foam_path);

        m_lediCleanAnimator = Res.LoadObj<RuntimeAnimatorController>(wash_animate_basepath + "lediClean");
    }

    public string GetAudioPath(string name)
    {
        return washbody_sound_basepath + name;
    }

    private GameObject GetActiveObject(string path, string name)
    {
        GameObject temp = Res.LoadObj<GameObject>(FileTools.CombinePath(path,name));
        GameObject target = MonoInstane.Instantiate<GameObject>(temp);
        return target;
    }

    private GameObject GetActiveObject(string name)
    {
        GameObject temp = Res.LoadObj<GameObject>(GetRscFullPath(name));
        GameObject target = MonoInstane.Instantiate<GameObject>(temp);
        return target;
    }

    private string GetRscFullPath(string name)
    {
        return FileTools.CombinePath(washitem_basepath, name);
    }

    public override void UnInit()
    {
        base.UnInit();

        GameObject.Destroy(collider_body.gameObject);
        GameObject.Destroy(collider_wing.gameObject);
    }

    #region 资源操作的一些公共方法

    public void OpenWaterDropEffect()
    {
        if (lediSkin.Count > 0)
        {
            for (int i = 0; i < lediSkin.Count; i++)
            {
                Material mat = lediSkin[i].materials[0];
                mat.SetFloat("_Range", 1);
                Vector2 offset = Random.insideUnitCircle;
                mat.SetVector("_UVOffset", new Vector4(offset.x, offset.y, 0, 0));

            }
        }
    }

    public void SetWaterDropProgress(float value)
    {
        if (lediSkin.Count > 0)
        {
            for (int i = 0; i < lediSkin.Count; i++)
            {
                if (lediSkin[i] != null)
                {
                    Material mat = lediSkin[i].materials[0];
                    mat.SetFloat("_Range", value);
                }
            }
        }
    }


    public void CleanWaterEffect()
    {
        float num = 1;
        DOTween.To(() => num, r => num = r, 0f, 1f).OnUpdate(() => {
            Debug.Log(num);
            SetWaterDropProgress(num);
        });
    }

    public void CloseAllDragItemInteractive()
    {
        soap.EnableInteractive(false);
        rag.EnableInteractive(false);
        nozzle.EnableInteractive(false);
    }

    public void ResetLediBlendShape()
    {
        if (lediSkin.Count > 0)
        {
            for (int i = 0; i < lediSkin.Count; i++)
            {
                if (lediSkin[i] != null)
                {
                    for (int x = 0; x < 24; x++)
                    {
                        (lediSkin[i]).SetBlendShapeWeight(x, 0);
                    }
                 
                }
            }
        }
    }

    #endregion

}


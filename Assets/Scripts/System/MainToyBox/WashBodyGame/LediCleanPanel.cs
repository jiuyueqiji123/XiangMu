﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LediCleanPanel : SingletonBaseWindow<LediCleanPanel> {

    public Button _up_btn { get; set; }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);
    }

    protected override void AddListeners()
    {
        base.AddListeners();

        _up_btn.onClick.AddListener(()=> {
            WashController.Instance.PlayExitAnimate();
            WindowManager.Instance.CloseWindow(WinNames.LediCleanPanel);
        });

    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();
    }
}

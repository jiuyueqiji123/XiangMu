﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class Nozzle : CleanDraggableItemBase,IBeginDragHandler,IDragHandler,IEndDragHandler {

    public GameObject m_effect;

    public Transform m_nozzlePoint;

    protected override void Start()
    {
        base.Start();

        m_showTrans = WashResController.Instance.nozzlePoint;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        IsOnDragging = true;

        m_effect.SetActive(true);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane plane = new Plane(m_showTrans.forward, m_showTrans.position);
        //Plane plane = new Plane(m_showTrans.forward, new Vector3(m_showTrans.position.x, m_showTrans.position.y);
        float distance;
        if (plane.Raycast(ray, out distance))
        {
            Vector3 hitpoint = ray.GetPoint(distance);
            if (!IsOutOfDragfield(hitpoint))
            {
                transform.position = hitpoint;
                //transform.LookAt(m_player);
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        MoveBack();
        m_effect.SetActive(false);
    }

    private bool IsOutOfDragfield(Vector3 point)
    {
        bool value = true;
        Vector3 leftUpPoint = new Vector3(m_colliderPlaneRect.x, m_colliderPlaneRect.y, m_showTrans.position.z);
        Vector3 rightDownPoint = new Vector3(m_colliderPlaneRect.x + m_colliderPlaneRect.width, m_colliderPlaneRect.y - m_colliderPlaneRect.height, m_showTrans.position.z);
        if (point.x >= leftUpPoint.x && point.x < rightDownPoint.x && point.y >= rightDownPoint.y && point.y < leftUpPoint.y)
            value = false;

        return value;
    }


    protected void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        if (m_showTrans != null)
        {
            Vector3 centerPos = new Vector3(m_colliderPlaneRect.x + m_colliderPlaneRect.width / 2, m_colliderPlaneRect.y - m_colliderPlaneRect.height / 2, m_showTrans.position.z);
            Gizmos.DrawWireCube(centerPos, new Vector3(m_colliderPlaneRect.width, m_colliderPlaneRect.height, 0.5f));
        }
    }

}

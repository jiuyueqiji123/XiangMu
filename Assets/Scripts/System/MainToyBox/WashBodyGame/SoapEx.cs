﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;
using DG.Tweening;

public class SoapEx : Singleton<SoapEx>, IWashEx
{

    public List<FoamControl> FoamList;

    public int maxFoamAmount = 20;          //能产生的泡沫的数量上限

    public bool isFoamPlaceFinished;

    private bool isOnSoaping = false;       //是否正在抹肥皂

    private float minDistanceWithFoam= 8;      //两个泡沫间的最小距离    

    private AnimatorEx animatorEx;


    public Transform ragCollider;

    public bool IsOptBegin
    {
        get
        {
            return FoamList.Count > 0;
        }
    }

    private GameAudioSource soapAudioEffect;
    public bool IsPlayingEffetAudio
    {
        get {
            if (soapAudioEffect != null)
                return soapAudioEffect.IsPlaying;
            return false;
        }
        set {

            if (value)
            {
                if (soapAudioEffect == null || !soapAudioEffect.IsPlaying) soapAudioEffect = AudioManager.Instance.PlaySound(WashResController.Instance.GetAudioPath(WashResController.audio_effect_tufeizao), true);
            }
            else
            {
                if (soapAudioEffect != null && soapAudioEffect.IsPlaying)
                    soapAudioEffect.Stop();
            }

        }
    }

    public override void Init()
    {
        base.Init();

        FoamList = new List<FoamControl>();

        isFoamPlaceFinished = false;

        ragCollider = WashResController.Instance.collider_body.Find("VBBody");
        ragCollider.SetActive(false);
        animatorEx = WashController.Instance.AnimatorLedi;
        animatorEx.PlaySequence(eAnimLediClean.talk1_a_1.ToString(), WashResController.Instance.GetAudioPath(WashResController.audio_tufeizao));

        WashResController.Instance.soap.MoveIn();

    }

    public void OnHover()
    {
        if (WashResController.Instance.soap.IsOnDragging)
        {
            if (isFoamPlaceFinished) return;

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Wash")))
            {

                if (!isOnSoaping)
                {
                    isOnSoaping = true;
                    IsPlayingEffetAudio = true;
                    animatorEx.CrossFade(eAnimLediClean.enjoy_a_1.ToString(), .2f);
                }

                if (hit.transform.name.StartsWith("TB") && IsCanPlaceFoam(hit.point))
                {
                    GameObject temp = MonoInstane.Instantiate<GameObject>(WashResController.Instance.Foam);
                    FoamControl foamctr = temp.GetComponent<FoamControl>();
                    temp.transform.position = hit.point;
                    temp.transform.rotation = Quaternion.LookRotation(hit.normal);
                    FoamList.Add(foamctr);
                    if (hit.transform.name.Contains("Body"))
                    {
                        temp.transform.SetParent(WashResController.Instance.collider_body);
                    }
                    else
                    {
                        temp.transform.SetParent(WashResController.Instance.collider_wing);
                    }

                    if (FoamList.Count > maxFoamAmount)
                    {
                        Debug.Log("soap over---");
                        isFoamPlaceFinished = true;
                    }
                }
            }
            else
            {
                if (isOnSoaping)
                {
                    IsPlayingEffetAudio = false;
                    isOnSoaping = false;
                    animatorEx.CrossFade(eAnimLediClean.enjoy_a_3.ToString(), .2f);
                }
            }
        }
    }

    public void OnTouchBegin()
    {
        
    }

    public void OnTouchUp()
    {
        if (isOnSoaping)
        {
            IsPlayingEffetAudio = false;
            isOnSoaping = false;
            animatorEx.CrossFade(eAnimLediClean.enjoy_a_3.ToString(), .2f);

            if (isFoamPlaceFinished)
            {
                WashResController.Instance.soap.EnableInteractive(false);
                WashResController.Instance.soap.MoveOut();
                TimerManager.Instance.AddTimer(.5f, () =>
                {
                    WashController.Instance.ChangeState(WashState.Clean);
                });
            }
            else
            {
                TimerManager.Instance.AddTimer(.2f, () =>
                {
                    animatorEx.PlaySequence(eAnimLediClean.talk2_a_1.ToString(), WashResController.Instance.GetAudioPath(WashResController.audio_kuaidian), null, true);
                });
            }
        }
    }

    public void OnUpdate()
    {
        animatorEx.OnUpdate();
    }

    private bool IsCanPlaceFoam(Vector3 hitpoint)
    {
        if (FoamList.Count > 0)
        {
            if (isFoamPlaceFinished)
            {
                return false;
            }
            else
            {
                bool val = true ;
                foreach (var foam in FoamList)
                {
                    float distance = Vector3.Distance(foam.transform.position, hitpoint);
                    if (distance < minDistanceWithFoam) val = false;
                }
                return val;
            }
        }
        return true;
    }


    public void DispearAllFoam()
    {
        foreach (var foam in FoamList)
        {
            MonoInstane.Instance.StartCoroutine(foam.Disappear(SoapEx.Instance, 0f));
        }
        FoamList.Clear();
    }

    public override void UnInit()
    {
        base.UnInit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class BaseToyBoxGame : IEasyTimer
{

    public bool bTimerCallback
    {
        get { return isPlaying; }
    }


    protected bool isPlaying
    {
        private set;
        get;
    }

    protected Transform toyboxGameRoot
    {
        private set;
        get;
    }
    protected Transform lediTransform
    {
        private set;
        get;
    }
    protected ActorRoot actor
    {
        private set;
        get;
    }

    private System.Action<string> PlayEndAction;
    public Animator animator;
    private int m_currentAnimatorStateCounter;
    private string m_currentStateName;
    private bool m_currentLoop;


    private NavMeshAgent agent;
    private Vector3 movePos;
    private bool isMoveEndStopWalk;
    private bool isMoving;
    private string previousStateName;

    private System.Action moveCallback;
    protected System.Action<bool> completeCallback;


    public void Initialize(Transform gameRoot)
    {
        toyboxGameRoot = gameRoot;

        PoolObjHandle<ActorRoot> actorHandle = ActorObjManager.Instance.GetActor(ActorNames.LeDi);
        actor = actorHandle.handle;
        lediTransform = actorHandle.handle.MyTransform;
        Agent = lediTransform.GetComponent<NavMeshAgent>();
        animator = actor.AnimatorController.Animator;
        
    }


    public virtual void OnStart(System.Action<bool> callback)
    {
        completeCallback = callback;
        isPlaying = true;
        PlayAnimator("idell_p", 0.2f, true);
        AudioManager.Instance.StopAllSound();
    }


    public virtual void OnUpdate() {

        if (!isMoving)
            return;
        if (/*Agent.remainingDistance*/Vector3.Distance(movePos, lediTransform.position) <= this.Agent.radius)
        {
            isMoving = false;
            if (isMoveEndStopWalk)
            {
                PlayAnimator(ActorNames.Ledi_Idle, 0.1f, true);
            }
            if (moveCallback != null)
                moveCallback();
        } 

        CheckAnimatorState();
    }

    private void CheckAnimatorState()
    {
        //Debug.LogError("anim end count: " + this.m_currentAnimatorStateCounter);
        AnimatorStateInfo info = this.animator.GetCurrentAnimatorStateInfo(0);
        if (!this.m_currentLoop && (int)info.normalizedTime >= this.m_currentAnimatorStateCounter)
        {
            Debug.LogError("anim end count: " + this.m_currentAnimatorStateCounter);
            if (PlayEndAction != null)
            {
                PlayEndAction(this.m_currentStateName);
            }
            this.m_currentStateName = null;
            this.m_currentAnimatorStateCounter = 0;

            if (string.IsNullOrEmpty(this.m_currentStateName))
            {
                this.animator.StopPlayback();
            }
        }
    }

    public virtual void OnExit()
    {
        isPlaying = false;
    }

    protected void SetDestination(Vector3 position, System.Action callback = null)
    {
        SetDestination(position, true, callback);
    }
    protected void SetDestination(Vector3 position, bool stopWalk = true, System.Action callback = null)
    {
        isMoveEndStopWalk = stopWalk;
        moveCallback = callback;

        isMoving = true;
        position.y = lediTransform.position.y;
        movePos = position;
        //Debug.LogError("----------:" + Vector3.Distance(position, lediTransform.position));
        this.Agent.SetDestination(position);
        PlayAnimator(ActorNames.Ledi_walk, 0.1f, true);
    }

    protected void PlayAnimator(string name, float fade, bool isLoop, System.Action<string> callback = null)
    {
        m_currentStateName = name;
        m_currentLoop = isLoop;
        PlayEndAction = callback;

        if (fade > 0f)
        {
            this.animator.CrossFade(name, fade);
        }
        else
        {
            this.animator.Play(name);
        }
        m_currentStateName = name;
        m_currentLoop = isLoop;
        this.animator.Update(0f);
        this.animator.Update(0f);    
        this.m_currentAnimatorStateCounter = (int)this.animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    protected void OnGameComplete(bool isComplete)
    {
        SetDestination(new Vector3(0, 0, 0), () =>
           {
               actor.TurnFront(() =>
               {
                   actor.PlayAnimator("idell_p", 0.1f, true);
               });
               if (completeCallback != null)
                   completeCallback(isComplete);
           });

    }
    

    public NavMeshAgent Agent
    {
        get
        {
            return agent;
        }

        set
        {
            agent = value;
        }
    }

}

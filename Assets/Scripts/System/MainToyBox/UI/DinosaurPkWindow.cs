﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class DinosaurPkWindow : SingletonBaseWindow<DinosaurPkWindow>
{
    //public Action backMain_Action;
    public Action<float> Fight_Action;
    #region ui property
    public Button _up_btn { get; set; }
    public Button _click_btn { get; set; }
    public Slider _slider { get; set; }

    public Transform _parent_go { get; set; }
    #endregion
    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);
        Input.multiTouchEnabled = false;

        _parent_go.localPosition = new Vector3(-80, -200f, 0);
        _parent_go.DOLocalMoveY(-76, 0.4f).SetEase(Ease.Linear);

        

    }

    protected override void OnClose()
    {

    }

    protected override void OnRefresh()
    {
    }

    protected override void AddListeners()
    {
        _up_btn.onClick.AddListener(OnUpClick);
        _click_btn.onClick.AddListener(OnFightClick);
    }

    protected override void RemoveListensers()
    {
        _up_btn.onClick.RemoveAllListeners();
        _click_btn.onClick.RemoveAllListeners();
    }


    #region uievent    
    void OnUpClick()
    {
        if (Fight_Action != null)
        {
            _slider.value = 0.96f;
            Fight_Action(-1);
        }
    }
    float interval = 1f;
    public float TimeCount = 0;
    public bool isFight = false;
    public float speed = 1f;
    //float target = 0f;
    protected override void OnUpdate()
    {
        /*
            10.恐龙条以每秒2%的速度挤压乐迪条
            11.点击按钮，乐迪条以3%/每次的速度前进
            12.两人的动作会根据进度条的占比发生变化
            13.当乐迪条占比100%的时候，乐迪胜利
            1)乐迪把恐龙摔出去，或顶到后面，然后乐迪走到中间面对镜头说：“谢谢你帮我一起制服了霸王龙号！”
            2)说话的时候有称赞动作，循环动作
            14.当乐迪条占比为0的时候，乐迪失败
            1)乐迪被恐龙摔出去，或顶到后面，乐迪摔倒，然后恐龙绕场一圈离开，然后乐迪站起来走到中间，面对镜头说：“这次太可惜了！下次一定能制服霸王龙号！”
            2)说话的时候有懊恼动作，循环动作
            15.当有一方失败的时候，游戏结束
         */
        if (isFight)
        {
            TimeCount += Time.deltaTime;
            if (TimeCount >= interval)
            {               
                TimeCount = 0f;
                _slider.value = _slider.value + (1f / 100f * speed);                
                //target = _slider.value + (1f / 100f * speed);
                Debug.Log("  pk ++  " + _slider.value +"  , === "+ (1f / 100f * speed));
                if (_slider.value >= 0.96f)
                {
                    //ledi win
                    Debug.Log("  恐龙 win ==== ");
                    _slider.value = 0.96f;
                    isFight = false;
                }
                if (Fight_Action != null)
                    Fight_Action(_slider.value);
            }
        }
    }
    void OnFightClick()
    {
        if (isFight)
        {
            AudioManager.Instance.StopAllSound();
            AudioManager.Instance.PlaySound("game_toybox/game_toybox_sound/601010211");
            Debug.Log("OnFightClick ====  ");
            if (_slider.value <= 0)
            {
                //my win
                Debug.Log("  my win ==== ");
                return;
            }
            _slider.value = _slider.value - (1.5f / 100f * speed);
            if (_slider.value <= 0)
            {
                _slider.value = 0;
                isFight = false;
            }
            if (Fight_Action != null)
                Fight_Action(_slider.value);
        }
    }
    private void OnButtonReturnClick()
    {
        Debug.Log(" OnButtonReturnClick ==== ");
        if (EasyLoadingEx.IsLoading)
            return;

        TimerManager.Instance.RemoveAllTimer();
        WindowManager.Instance.CloseWindow(WinNames.GuessPanel);
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }



    #endregion


}
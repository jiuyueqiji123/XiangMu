﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class GuessWindow : SingletonBaseWindow<GuessWindow>
{
    public Action<FingerGuessType> callback;
    public Action backMain_Action;
    public bool gamestart = false;
    #region ui property
    public Button _up_btn { get; set; }
    public Button _st_btn { get; set; }
    public Button _jd_btn { get; set; }
    public Button _bu_btn { get; set; }

    public Transform _parent_go { get; set; }
    #endregion

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);
        Input.multiTouchEnabled = false;

        _parent_go.localPosition = new Vector3(0, -200f, 0);
        _parent_go.DOLocalMoveY(0, 0.4f).SetEase(Ease.Linear);

    }

    protected override void OnClose()
    {
      
    }

    protected override void OnUpdate()
    {

    }

    protected override void OnRefresh()
    {
    }

    protected override void AddListeners()
    {
        _up_btn.onClick.AddListener(OnUpClick);
        _st_btn.onClick.AddListener(OnStClick);
        _jd_btn.onClick.AddListener(OnJdClick);
        _bu_btn.onClick.AddListener(OnBuClick);        
    }

    protected override void RemoveListensers()
    {
        _up_btn.onClick.RemoveAllListeners();
        _st_btn.onClick.RemoveAllListeners();
        _jd_btn.onClick.RemoveAllListeners();
        _bu_btn.onClick.RemoveAllListeners();
    }


    #region uievent    
    void OnUpClick()
    {
        if (backMain_Action != null)
            backMain_Action();
    }

    void OnStClick()
    {
        if (callback != null)
        {
            callback(FingerGuessType.St);
            if (gamestart == true)
            {
                _st_btn.GetComponent<Transform>().GetChild(0).SetActive(true);
                gamestart = false;
            }
            
        }
                  
        
    }

    void OnJdClick()
    {
        if (callback != null)
        {
            callback(FingerGuessType.Jd);
            if (gamestart == true)
            {
                _jd_btn.GetComponent<Transform>().GetChild(0).SetActive(true);
                gamestart = false;
            }
            
        }
            
    }

    void OnBuClick()
    {
        if (callback != null)
        {
            callback(FingerGuessType.Bu);
            if (gamestart == true)
            {
                _bu_btn.GetComponent<Transform>().GetChild(0).SetActive(true);
                gamestart = false;
            }
           
        }
    }
    private void OnButtonReturnClick()
    {
        Debug.Log(" OnButtonReturnClick ==== ");
        if (EasyLoadingEx.IsLoading)
            return;

        TimerManager.Instance.RemoveAllTimer();
        WindowManager.Instance.CloseWindow(WinNames.GuessPanel);
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }



    #endregion


}

public enum FingerGuessType
{
    St = 1,
    Jd = 2,
    Bu = 3,
}
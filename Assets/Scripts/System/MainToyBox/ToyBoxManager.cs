﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using WCBG.ToolsForUnity.Tools;

// 玩具箱游戏管理类
public class ToyBoxManager : MonoSingleton<ToyBoxManager>, IEasyTimer
{
    public bool bTimerCallback
    {
        get { return isInit; }
    }

    public bool isWork { get; set; }


    private enum EMainGameType
    {
        Football,       // 踢足球
        CleanLedi,      // 清洁机身
        FingerGuessing, // 猜拳
        PK,             // 恐龙PK
    }

    private const string ASSET_PATH = "game_toybox/ToyboxGameRoot";

    private Transform m_toyboxGameRoot;
    private Animator m_toyboxAnimator;

    private MainMoveClick m_mainMoveClick;
    private BaseToyBoxGame game;

    private bool isInit;
    private bool isGamePlaying;
    private bool isToyboxShow;
    private bool isDoubleClick = false;
    private bool IsPlaying = false;

    private List<Transform> animations = new List<Transform>();
    private int Index = 0;
    private int[] index = new int[] { 0, 1, 2, 3 };

    //protected override void Init()
    //{
    //    base.Init();

    //    Initialize();
    //}

    protected override void OnDestroy()
    {
        base.OnDestroy();

        Dispose();
    }

    private void Update()
    {
        if (game != null)
        {
            game.OnUpdate();
        }

    }


    public void Initialize()
    {
        isWork = true;
        isInit = true;
        if (m_toyboxGameRoot == null)
        {
            m_toyboxGameRoot = Res.LoadObj(ASSET_PATH).transform;
        }
        if (m_mainMoveClick == null)
        {
            m_mainMoveClick = GameObject.FindObjectOfType<MainMoveClick>();
        }
        m_toyboxAnimator = m_toyboxGameRoot.GetChild(0).GetComponent<Animator>();

        FEasyInput.AddEvent(EEasyInputType.OnUp, OnUp);
    }

    void OnUp()
    {
        if (!isWork) return;

        if (EventSystem.current.IsPointerOverGameObject())
            return;

        Transform target = FTools.GetRaycastHitTargetByMousePoint();

        if (target != null)
        {
            switch (target.name)
            {
                case "301010201":
                    OnToyBoxClick();
                    break;
                case "301010202":
                    StartGame(EMainGameType.Football);
                    break;
                case "301010203":
                    StartGame(EMainGameType.CleanLedi);
                    break;
                case "301010206":
                    StartGame(EMainGameType.FingerGuessing);
                    break;
                case "302030402":
                    StartGame(EMainGameType.PK);
                    break;
            }
        }

    }

    private void OnToyBoxClick()
    {

        if (IsPlaying)
        {
            return;
        }
        
        //IsPlaying = true;
        isToyboxShow = !isToyboxShow;
        
        if (isToyboxShow)
        {
            
            OnClickPlayTotBox();
        }
        else
        {
            OnClickBackToyBox();
        }
    }

    private void StartGame(EMainGameType type)
    {
        //Debug.LogError("-----------------------------------");
        if (isGamePlaying)
            return;
        if (IsPlaying)
        {
            return;
        }
        
        isGamePlaying = true;

        switch (type)
        {
            case EMainGameType.Football:
                game = new FootballGame();
                MainController.Instance.CloseButtonRadio();
                break;
            case EMainGameType.CleanLedi:
                game = new WashBodyGame();
                MainController.Instance.CloseButtonRadio();
                break;
            case EMainGameType.FingerGuessing:
                game = new FingerGuessGame();
                MainController.Instance.CloseButtonRadio();
                break;
            case EMainGameType.PK:
                game = new DinosaurPkGame();
                MainController.Instance.CloseButtonRadio();
                break;
        }
        if (game == null) return;
        EnableMainController(false);

        OnToyBoxClick();
        game.Initialize(m_toyboxGameRoot);
        game.OnStart(OnGameComplete);
        IsPlaying = true;
        EventBus.Instance.BroadCastEvent<bool>(EventID.ON_MAIN_GAME_PLAY_OR_EXIT, true);
    }


    public void Dispose()
    {
        isInit = false;
        if (m_toyboxGameRoot != null)
        {
            GameObject.Destroy(m_toyboxGameRoot.gameObject);
            m_toyboxGameRoot = null;
        }
        if (game != null)
        {
            game.OnExit();
            game = null;
        }

        FEasyInput.RemoveEvent(EEasyInputType.OnUp, OnUp);
    }

    private void OnGameComplete(bool isComplete)
    {
        Debug.Log("----------- OnGameComplete!");

        isGamePlaying = false;
        IsPlaying = false;
        game.OnExit();
        game = null;
        EnableMainController(true);
        PoolObjHandle<ActorRoot> actor = ActorObjManager.Instance.GetActor(ActorNames.LeDi);
        actor.handle.PlayAnimator(ActorNames.Ledi_Idle, 0.1f, true);
        MainController.Instance.OpenButtonRadio();
        EventBus.Instance.BroadCastEvent<bool>(EventID.ON_MAIN_GAME_PLAY_OR_EXIT, false);
    }

    private void EnableMainController(bool val)
    {
        PoolObjHandle<ActorRoot> actor = ActorObjManager.Instance.GetActor(ActorNames.LeDi);
        actor.handle.SetPlaying(false);
        actor.handle.ActorCtrl.SetActive(val);
        if (m_mainMoveClick != null)
            m_mainMoveClick.SetActive(val);
        MainAnimatorController.Instance.EnableController(val);
        actor.handle.NavController.EnableComponent(val);
        actor.handle.AnimatorController.ActiveComponent(val);
        if (val)
        {
        }
        else
        {
        }
    }

    public void OnClickPlayTotBox()
    {
        if (isDoubleClick)
        {
            return;
        }
        isDoubleClick = true;
        
        animations.Clear();
        
        foreach (Transform animation in m_toyboxAnimator.gameObject.transform)
        {
            animations.Add(animation);
            
            //Debug.LogError("-------------------------------------------------------- " + animation.name);
        }
        for (Index = 0; Index < animations.Count; Index++)
        {
            animations[index[Index]].GetChild(0).GetChild(0).gameObject.SetActive(true);
            if (Index <= 2)
            {
                animations[index[Index]].GetChild(0).GetChild(0).DOScale(new Vector3(18, 18, 18), 0.1f);
            }
            else
            {
                animations[index[Index]].GetChild(0).GetChild(0).DOScale(new Vector3(50, 50, 50), 0.1f);
            }
            
            if (Index == 3)
            {
                animations.Clear();
                this.AddTimerEx(0.3f, () =>
                {
                    Index = 0;
                    
                });
            }
        }
        this.AddTimerEx(0.1f,()=>
        {
            m_toyboxAnimator.CrossFade("show",0.2f);
            m_toyboxGameRoot.GetChild(1).SetActive(true);
        });
        this.AddTimerEx(0.5f, () =>
        {
            isDoubleClick = false;
            //isGamePlaying = false;
        });
    }

    public void OnClickBackToyBox()
    {
        if (isDoubleClick)
        {
            return;
        }
        isDoubleClick = true;
        animations.Clear();
        foreach (Transform animation in m_toyboxAnimator.gameObject.transform)
        {
            animations.Add(animation);
            
        }
        //Debug.LogError(animations.Count);
        m_toyboxAnimator.CrossFade("back", 0.2f);
        //animations[index[Index]].GetChild(0).GetChild(0).DOScale(new Vector3(0, 0, 0), 0.1f);
        //animations[index[Index]].GetChild(0).GetChild(0).gameObject.SetActive(false);
        this.AddTimerEx(0.23f, () =>
        {
            this.AddTimerEx(0.2f, () =>
            {
                CloseEffect(false);
                m_toyboxGameRoot.GetChild(1).SetActive(false);
                if (Index == 3)
                {

                    Index = 0;
                }
            });
        });
        this.AddTimerEx(0.5f, () =>
        {
            isDoubleClick = false;
            //isGamePlaying = true;
        });
    }

    public void CloseEffect(bool IsOpen)
    {
        if (IsOpen)
        {
            animations[0].GetChild(0).GetChild(0).DOScale(new Vector3(18, 18, 18), 0.1f);
            animations[1].GetChild(0).GetChild(0).DOScale(new Vector3(18, 18, 18), 0.1f);
            animations[2].GetChild(0).GetChild(0).DOScale(new Vector3(18, 18, 18), 0.1f);
            animations[3].GetChild(0).GetChild(0).DOScale(new Vector3(50, 50, 50), 0.1f);
            animations.Clear();
           
        }
        else
        {
            animations[0].GetChild(0).GetChild(0).DOScale(new Vector3(0, 0, 0), 0.1f);
            animations[1].GetChild(0).GetChild(0).DOScale(new Vector3(0, 0, 0), 0.1f);
            animations[2].GetChild(0).GetChild(0).DOScale(new Vector3(0, 0, 0), 0.1f);
            animations[3].GetChild(0).GetChild(0).DOScale(new Vector3(0, 0, 0), 0.1f);
            animations.Clear();
            
        }
    }

}

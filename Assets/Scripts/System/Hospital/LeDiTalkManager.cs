﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class LeDiTalkManager : MonoSingleton<LeDiTalkManager> {

    public Transform _ledi_ActorRoot;

    public AnimatorEx _leAnimator;

    Transform _clapStart;
    Transform _clapEnd;
    Transform _effectParent;

    float _tween_duration = 0.5f;

    protected override void Init()
    {
        base.Init();

        Transform rootObject = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, HospitalConsts.ledi_player_animation_root).transform;
        _clapStart = rootObject.Find("clap_start");
        _clapEnd = rootObject.Find("clap_end");
        _effectParent = rootObject.Find("effect_parent");
        _ledi_ActorRoot = rootObject.Find("LeDi_Talk_Root");
        _leAnimator = new AnimatorEx(_ledi_ActorRoot.gameObject);

    }

    private void Update()
    {
        _leAnimator.OnUpdate();
    }

    public void LediMoveIn(Action callback = null)
    {
        _ledi_ActorRoot.position = _clapStart.position;
        _ledi_ActorRoot.DOMove(_clapEnd.position, _tween_duration).OnComplete(()=> {
            if (callback != null) callback();
        });
    }

    public void LediMoveOut(Action callback = null)
    {
        _ledi_ActorRoot.position = _clapEnd.position;
        _ledi_ActorRoot.DOMove(_clapStart.position, _tween_duration).OnComplete(() => {
            if (callback != null) callback();
        });
    }

    public void Talk(string audioPath,System.Action callback = null, bool removeAll = false)
    {
        _leAnimator.PlaySequence(eHospitalLediAnim.BeginTalk.ToString(), audioPath, callback, removeAll);

    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        GameObject.Destroy(_ledi_ActorRoot);
        _leAnimator = null;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmosDraw : MonoBehaviour {

    public bool isDraw = true;

    public Transform[] pointsDraw;

    private void OnDrawGizmos()
    {
        if (!isDraw) return;

        Gizmos.color = Color.red;
        if(pointsDraw.Length > 0)
            for (int i = 0; i < pointsDraw.Length; i++)
            {
                Gizmos.DrawWireCube(pointsDraw[i].position, Vector3.one * .5f);
            }
    }
}

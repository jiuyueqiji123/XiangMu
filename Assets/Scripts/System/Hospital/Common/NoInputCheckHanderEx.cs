﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoInputCheckHanderEx {

    float _timer;
    float _timeoutDuration;
    bool _isStartTimer;
    System.Action _callback;
    int _loopTimes = 1;

    public NoInputCheckHanderEx(float timeoutDuration, int loopTimes = 1)
    {
        this._isStartTimer = false;
        this._loopTimes = loopTimes;
        this._timeoutDuration = timeoutDuration;
    }

    // Update is called once per frame
    public void OnUpdate () {
        if (_isStartTimer)
        {
            _timer += Time.deltaTime;
            if (_timer > _timeoutDuration)
            {
                if (_loopTimes <= 0)
                {
                    _isStartTimer = false;
                }

                _timer = 0;

                this._loopTimes--;
                Debug.Log("指定事件没有操作");
                if (this._callback != null) this._callback();
            }
        }
    }

    public void AddNoInputHander(System.Action hander)
    {
        this._callback += hander;
    }

    public void RemoveNoInputHander(System.Action hander)
    {
        this._callback -= hander;
    }

    //开始触发无操作检查
    public void StartNoInputTimer(float delay = 0f,bool isCallbackWhenStart = false)
    {
        TimerManager.Instance.AddTimer(delay, () => {
            if (isCallbackWhenStart)
                if (_callback != null) _callback();
            _isStartTimer = true;
        });
    }

    public void StopCheck()
    {
        _isStartTimer = false;
    }

    public void OnDestroy()
    {

    }

}

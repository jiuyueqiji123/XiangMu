﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;
using System;
using DG.Tweening;

public class HospitalMicroWindow : SingletonBaseWindow<HospitalMicroWindow>
{


    #region ui property

    public RectTransform _Mic { get; set; }
    public Button _BtnAudio { get; set; }
    public Transform _Huatong { get; set; }

    #endregion

#if UNITY_EDITOR
    private const float TALK_TIME = 0.5f;
#else
    private const float TALK_TIME = 5f;
#endif


    System.Action<bool> completeCallback;
    private SpriteSequenceFrame m_micComp;

    private float addtime;
    private bool isTalk;

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        m_micComp = _Huatong.GetComponent<SpriteSequenceFrame>();
        m_micComp.Stop();
        addtime = 0;
        isTalk = false;
        MicroManager.Instance.StartRecord();
        MicrophoneAnim(true);
    }

    protected override void OnUpdate()
    {
        if (!isTalk)
            return;

        Debug.Log(addtime);
        addtime += Time.deltaTime;
        if (addtime > TALK_TIME)
        {
            addtime = 0;
            isTalk = false;
            if (completeCallback != null)
                completeCallback(true);
        }
        if (m_micComp != null)
        {
            if (MicroManager.Instance.Volume > 0)
            {
                m_micComp.Play();
            }
            else
            {
                m_micComp.Stop();
            }
        }
    }

    protected override void OnClose()
    {
        base.OnClose();

        MicroManager.Instance.StopRecord();
    }


    #region -------------------------- uievent

    private void OnButtonReturnClick()
    {
        if (completeCallback != null)
        {
            completeCallback(false);
        }
    }

    #endregion

    public void AddEvent(System.Action<bool> callback)
    {
        completeCallback = callback;
        isTalk = true;
        addtime = 0;
    }

    public void MicrophoneAnim(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _Mic.DOAnchorPosY(216, .5f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _Mic.DOAnchorPosY(-126, .5f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }


}

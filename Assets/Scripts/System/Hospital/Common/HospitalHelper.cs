﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalHelper<T> where T:Component {

    public static T GetSafeComponent(GameObject targetObject)
    {
        T t = targetObject.GetComponent<T>();
        if (t == null)
            t = targetObject.AddComponent<T>();
        return t;
    }
}

public class HospitalUtil
{
    public static List<int> RandomUnSame(int amount, int min, int max)
    {
        Random random = new Random();
        List<int> result = new List<int>();
        int temp;
        while (result.Count < amount)
        {
            temp = Random.Range(min, max);
            if (!result.Contains(temp))
            {
                result.Add(temp);
            }
        }
        return result;
    }

    public static void PathInverse(ref Vector3[] path)
    {
        if (path.Length >= 2)
        {
            for (int i = 0; i < path.Length / 2; i++)
            {
                Vector3 temp = path[i];
                path[i] = path[path.Length - i - 1];
                path[path.Length - i - 1] = temp;
            }
        }
    }

    public static Vector3[] PathInverse1(Vector3[] path)
    {
        if (path.Length >= 2)
        {
            Vector3[] newpath = new Vector3[path.Length];
            for (int i = 0; i < newpath.Length; i++)
            {
                newpath[newpath.Length - i - 1] = path[i]; 
            }
            return newpath;
        }

        return null;
    }


    public static void WaitSpeak(System.Action callback)
    {
        WindowManager.Instance.OpenWindow(WinNames.HospitalMicroPanel);
        HospitalMicroWindow.Instance.AddEvent((result) =>
        {
            HospitalMicroWindow.Instance.MicrophoneAnim(false, () => {
                WindowManager.Instance.CloseWindow(WinNames.HospitalMicroPanel);
                if (callback != null) callback();
            });
        });
    }

    static int timerID;
    public  static void PlayAudio(string audioname, System.Action callback = null, bool stopOtherSound = false, float delay = 0f)
    {
        if (stopOtherSound)
            AudioManager.Instance.StopAllSound();
        string path = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, audioname);
        TimerManager.Instance.AddTimer(delay, () => {
            GameAudioSource _audiosource = AudioManager.Instance.PlaySound(path);
            if (callback != null)
            {
                TimerManager.Instance.RemoveTimerSafely(ref timerID);
                timerID = TimerManager.Instance.AddTimer(_audiosource.Length, callback);
            }
        });
    }

    public static GameAudioSource PlayAudio(string audioname, bool loop)
    {
        string path = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, audioname);
        return AudioManager.Instance.PlaySound(path, loop, true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum eHospitalNuowaAnim
{
    s_idle_tooth,       //坐立待机
    walk_tooth,         //难受走路
    walk,
    s_all_talk_tooth_01,    //坐立说话啊
    s_idle_hand,        //双手垂下待机
    Sit_speak_N_01,     //坐着说话
    s_smile,        //消肿完成
    s_smile_idle,   //消肿完成待机
    wave_hand,  //离开
    open_mouth, //张嘴      张开嘴巴，露出牙齿
    op_mouth_idle, //张嘴待机，等待玩家。眼睛不时眨动两下
    close_mouth, //玩家将操作物品拿回，角色闭上嘴巴
    clo_mouth_idle, //闭嘴等待玩家重新操作
    mouth_rotation, //坐立，大头特写，眼珠转动，嘴巴随着牙刷和清洗仪变大（刷牙与清洗时）
    head_tilt,  //头微微倾侧，眼珠转动，等待玩家
    click_feedback, //角色待机时，玩家点击角色，角色会张开嘴巴上下咬合一下牙齿，随后闭上嘴巴。提示玩家继续操作
    clean_success,  //牙齿清洗完成，牙齿洁白，出成功粒子效果。角色嘴巴抿住，表喜悦之情
    walk_stomachache,   //手捂肚子，眉头紧蹙，缓慢走路
    s_idle_stomachache, //手捂肚子，眉头紧蹙，坐立待机
    s_t_stomachache_01,    //手捂肚子，眉头紧蹙，坐立说话
    eat_med_02,        //坐立，张嘴吃药（喂药）
    all_talk_goodbye_01,   //站立，开心道别，说话
    all_talk_refuse_01,    //坐立，神情抗拒，坚决摇头，说话【通用说话】   不吃药不吃药，吃药好苦啊。
    shake_head2,    //坐立，神情抗拒，摇头。
    s_nod,          //坐立，微微点头（加油完成，同意吃药）
    up_chair,       //坐到椅子上
    down_chair,
    med_open_mouse, //吃药张嘴
    med_close_mouse,//吃药闭嘴

}

public enum eHospitalAJiaShaAnim
{
    s_idle_tooth,
    walk_tooth,
    s_all_talk_tooth_01
}

public enum eHospitalAishaAnim
{
    walk_leg_pain, //一只腿红肿，缓慢走路
    cli_chair, //攀爬坐上椅子
    down_chair,//下椅子
    legpain_s_idle, //蹙眉，坐立待机
    all_s_talk_01, //蹙眉，坐立说话
    all_goodbye_talk, //站立，交替跺脚，随即开心说话
    s_idle, //坐立待机
    all_talk_s_fear_01, //面露微微惧色，坐立说话
    all_talk_refuse_01, //神情抗拒，坚决摇头，说话
    shake_head_01, //坐立，神情抗拒，摇头。
    s_nod, //坐立，微微点头（加油完成，同意打针）
    s_happy, //坐立，眼神温暖，神情愉快。【完成贴创可贴，成功反馈】
    all_talk_goodbye_01, //站立，开心道别，说话
    Walke,   //站立走路
}

public enum eHospitalLediAnim
{
    Carry,
    Idle,
    Walk,
    BeginGreet,
    BeginTalk,
}

public enum eHospitalGame
{
    game_Stomachache,
    game_Toothache,
    game_vaccine,
    game_Bruise,
    game_LowFever,
    game_HighFever,
}


enum eStomachachePanelAnim
{
    stomachachepanel_scanner_out,
    stomachachepanel_scanner_in,
}

//冰袋
public enum eIcebag { none, red, green, yellow }


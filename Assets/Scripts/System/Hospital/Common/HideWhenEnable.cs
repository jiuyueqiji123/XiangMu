﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideWhenEnable : MonoBehaviour {

    public float _timeAfterEnableToHide = 1f;

    private void OnEnable()
    {
        TimerManager.Instance.AddTimer(_timeAfterEnableToHide, () => { gameObject.SetActive(false); });
    }
}

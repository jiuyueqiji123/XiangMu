﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalController : MonoSingleton<HospitalController> {

    private Dictionary<string, GameObject> _hospitalObjectCache;

    protected override void Init()
    {
        base.Init();
        _hospitalObjectCache = new Dictionary<string, GameObject>();
    }

    private void Update()
    {
        StomachacheGameManager.Instance.OnUpdate();
        LowFeverController.Instance.OnUpdate();
#if UNITY_EDITOR
        //TimerManager.Instance.Update();
#endif
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }


    #region 记录游戏过程中生成的一些物体

    public void RegistCacheObject(GameObject _obj)
    {
        if (!_hospitalObjectCache.ContainsKey(_obj.name))
            _hospitalObjectCache.Add(_obj.name, _obj);
    }

    public void DestroyCacheObject()
    {
        if(_hospitalObjectCache.Count> 0)
            foreach (var item in _hospitalObjectCache.Values)
                Destroy(item);
        _hospitalObjectCache.Clear();
    }

    public void UnRegistCacheObject(string name)
    {
        if (_hospitalObjectCache.ContainsKey(name))
            _hospitalObjectCache.Remove(name);
    }

    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class QiWuJiClicker : MonoBehaviour,IPointerClickHandler {

    public Action _onPlayFinished;
    int playcount;

    private void Start()
    {
        playcount = 0;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("点击了");
        playcount++;
        if (playcount >= 3)
        {
            if (_onPlayFinished != null) _onPlayFinished();
        }
    }
}

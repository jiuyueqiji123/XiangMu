﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// 药膏物体
/// </summary>
public class DragYaogaoObject : Object3dDragBase {

    Transform _effect;
    Vector3 _effectStartScale;
    float _detumesceneAmount = 4;
    float _detumesceneProgress;
    Camera _cam;

    public override void InitComponet()
    {
        base.InitComponet();
        _cam = VaccineGameManager.Instance._cam;
        _effect = transform.Find("502004099");
        _effectStartScale = _effect.localScale;
        _detumesceneProgress = _detumesceneAmount;
        _loopAudioName = HospitalConsts.audio_bingfu_tumo;
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        //_effect.SetActive(true);
        ActiveLoopAudio(true);
    }

    public override Vector3 Get3DPosition()
    {
        Plane plane = new Plane(_cam.transform.forward * -1, new Vector3(.5f, 1f, -0.96f));
        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public override void OnDrag(PointerEventData eventData)
    {
        base.OnDrag(eventData);

        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Draggable")))
        {
            _targetDragObjectRotation = Quaternion.LookRotation(hit.normal);
            if (hit.transform.name == "HurtCollider")
            {
                _detumesceneProgress -= Time.deltaTime;
                float percent = _detumesceneProgress / _detumesceneAmount;
                _effect.localScale = percent * _effectStartScale;

                if (_detumesceneProgress < 0)
                {
                    gameObject.SetActive(false);
                    Debug.Log("__涂抹伤口完成");
                    VaccineGameManager.Instance.vaccineTreatment.OnPengwuFinished();
                }
            }
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        //_effect.SetActive(false);
        ActiveLoopAudio(false);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;
using DG.Tweening;

public class VaccinePengwuDragItem : UIDrag3DBase {

    public string _modelPath;
    public string _texturePath;
    Camera _cam;
    GameObject _object;
    Transform _effect;
    bool _isCreateYaogao;
    Transform yaogao;
    Vector3 yaogao_startScale;

    float _detumesceneAmount = 2;
    float _detumesceneProgress;
    bool _onPengsa;

    public override void InitComponet()
    {
        base.InitComponet();
        _cam = VaccineGameManager.Instance._cam;
        _detumesceneProgress = _detumesceneAmount;
        _effect = transform.Find("502004098");
        _loopAudioName = HospitalConsts.audio_pengwu;
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        VaccineGameManager.Instance.vaccineTreatment._pengwuObject = _currentDragObject.gameObject;
    }

    public override Vector3 Get3DPosition()
    {
        Plane plane = new Plane(_cam.transform.forward * -1, new Vector3(0.6f, 1f, -0.96f));
        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin,ray.direction,Color.red);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public override void OnDrag(PointerEventData eventData)
    {
        base.OnDrag(eventData);

        DragIdentify _current_dragIdentify = DragComparerManager.Instance._current_dragIdentify;
        if (_current_dragIdentify != null && _current_dragIdentify.ID == "hurtCollider")
        {
            if (!_onPengsa)
            {
                _onPengsa = true;
                //播放碰撒动画，特效
                _effect.SetActive(true);
                ActiveLoopAudio(true);
            }

            if (!_isCreateYaogao)
            {
                _isCreateYaogao = true;

                Transform yaogaoAnchor = _current_dragIdentify.transform.Find("YaogaoAnchor");
                yaogao = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, "Yaogao").transform;
                yaogao.GetComponentInChildren<Collider>().enabled = false;
                yaogao.position = yaogaoAnchor.position;
                yaogao.rotation = yaogaoAnchor.rotation;
                yaogao_startScale = yaogao.localScale;
                yaogao.localScale = Vector3.zero;

                HospitalController.Instance.RegistCacheObject(yaogao.gameObject);
            }

            _detumesceneProgress -= Time.deltaTime;
            float yaogaoPecent =1 - ( _detumesceneProgress / _detumesceneAmount);
            yaogao.localScale = yaogaoPecent * yaogao_startScale;
            if (_detumesceneProgress < 0)
            {
                this.enabled = false;

                HospitalUtil.PlayAudio(HospitalConsts.audio_ledi_tuizhong4);
                Debug.Log("__喷雾完成,生成药膏");
                GetComponentInChildren<Animator>().enabled = false;
                yaogao.GetComponentInChildren<Collider>().enabled = true;
                _currentDragObject.SetActive(false);

            }
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        if (_onPengsa)
        {
            _onPengsa = false;
            _effect.SetActive(false);
            ActiveLoopAudio(false);
        }
    }

    public override Transform _currentDragObject
    {
        get
        {
            if (_object == null)
            {
                _object = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, _modelPath);
                HospitalController.Instance.RegistCacheObject(_object);
                _dragStartRotation = _object.transform.rotation = Quaternion.Euler(-20, 270, 5);
            }
            Texture2D t2d = Res.LoadTexture2D(FileTools.CombinePath(HospitalConsts.game_hospital_texture_BasePath, _texturePath));
            _object.GetComponentInChildren<Renderer>().material.mainTexture = t2d;

            return _object.transform;
        }

        set
        {
            base._currentDragObject = value;
        }
    }



}

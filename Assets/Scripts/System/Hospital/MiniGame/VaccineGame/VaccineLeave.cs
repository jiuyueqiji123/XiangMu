﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaccineLeave : HospitalGameBase
{
    public override void OnEnter()
    {
        WindowManager.Instance.CloseWindow(WinNames.VaccinePanel);
        HospitalResManager.Instance.Game_BruiseRoot.SetActive(false);
        HospitalEventHandler.Instance.BroadCastEvent(HospitalEventID.SMALLGAME_OVER);
    }

    public override void OnLeave()
    {
        
    }

    public override void OnUpdate()
    {
       
    }

    public override void Dispose()
    {
       
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaccineGameManager : Singleton<VaccineGameManager> {


    public enum eVaccineGameState {

        None,
        ItemSelect,   //选药
        Treatment, //治疗
        Leave   //成功告别

    }

    eVaccineGameState state;

    HospitalGameBase _current_stateBase;
    public VaccineItemSelect vaccineItemSelect;
    public VaccineTreatment vaccineTreatment;
    public VaccineLeave vaccineLeave;

    public AnimatorEx _aisha_animatorex;
    public AnimatorEx m_ledi_animatorEx;

    public Camera _cam;

    public override void Init()
    {
        base.Init();

        state = eVaccineGameState.None;
        vaccineItemSelect = new VaccineItemSelect();
        vaccineTreatment = new VaccineTreatment();
        vaccineLeave = new VaccineLeave();

        _cam = HospitalResManager.Instance.Game_BruiseRoot.GetComponentInChildren<Camera>();

        _aisha_animatorex = new AnimatorEx(HospitalResManager.Instance.m_aishaRoot.gameObject);
        m_ledi_animatorEx = new AnimatorEx(HospitalResManager.Instance.m_lediRoot.gameObject);
        _aisha_animatorex.CrossFade(eHospitalAishaAnim.legpain_s_idle.ToString(), .1f);
    }

    public void GameStart()
    {
        HospitalResManager.Instance.Game_BruiseRoot.SetActive(true);

        WindowManager.Instance.CloseWindow(WinNames.ClinicRoomPanel);
        WindowManager.Instance.OpenWindow(WinNames.VaccinePanel);
        ChangeState(eVaccineGameState.ItemSelect);
    }

    public void ChangeState(eVaccineGameState newstate)
    {
        if (_current_stateBase != null && newstate != state)
            _current_stateBase.OnLeave();

        switch (newstate)
        {
            case eVaccineGameState.None:

                break;
            case eVaccineGameState.ItemSelect:
                _current_stateBase = vaccineItemSelect;
                break;
            case eVaccineGameState.Treatment:
                _current_stateBase = vaccineTreatment;
                break;
            case eVaccineGameState.Leave:
                _current_stateBase = vaccineLeave;
                break;
            default:
                break;
        }

        state = newstate;
        _current_stateBase.OnEnter();
    }

    public void OnUpdate()
    {
        if(_current_stateBase != null)
            _current_stateBase.OnUpdate();
        _aisha_animatorex.OnUpdate();
        m_ledi_animatorEx.OnUpdate();
    }

    public override void UnInit()
    {
        base.UnInit();

        vaccineItemSelect.Dispose();
        vaccineTreatment.Dispose();
        vaccineLeave.Dispose();
    }

}

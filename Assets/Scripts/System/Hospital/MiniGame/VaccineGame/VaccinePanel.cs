﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;


public class VaccinePanel : SingletonBaseWindow<VaccinePanel>
{

    #region ui property
    public RectTransform _layout_bingdai { get; set; }
    public RectTransform _layout_pengwu { get; set; }
    public RectTransform _layout_bengdai { get; set; }
    public RectTransform _ItemSelct { get; set; }
    public RectTransform _Xiaozhong { get; set; }
    public RectTransform _Yaogao { get; set; }
    public RectTransform _Chuangkoutie { get; set; }
    public RectTransform _BottomBar { get; set; }
    public Image _DetuForeground { get; set; }
    public RectTransform _DetuProgressbar { get; set; }
    public RectTransform _DraggableImage { get; set; }
    public Button _BtnBack { get; set; }
    public Transform _5020040100 { get; set; }

    #endregion

    protected override void AddListeners()
    {
        base.AddListeners();
        _BtnBack.onClick.AddListener(OnBackBtnClick);
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();
        _BtnBack.onClick.RemoveListener(OnBackBtnClick);
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        DragComparerManager.Instance._currentDragAgent = _DraggableImage;
        XiaozhongAnimate(true);
        ButtomBarAnimate(true);
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        VaccineGameManager.Instance.OnUpdate();
    }

    protected override void OnClose()
    {
        base.OnClose();
    }


    #region UIEvent

    public void XiaozhongAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _Xiaozhong.DOAnchorPosX(-806, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _Xiaozhong.DOAnchorPosX(-554, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    public void YaogaoAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _Yaogao.DOAnchorPosX(-806, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _Yaogao.DOAnchorPosX(-554, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    public void Chuangkoutie(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _Chuangkoutie.DOAnchorPosX(-806, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _Chuangkoutie.DOAnchorPosX(-554, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    public void DetuProgressbarAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _DetuProgressbar.DOAnchorPosX(191, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _DetuProgressbar.DOAnchorPosX(-77, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    public void ButtomBarAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _BottomBar.DOAnchorPosY(119, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _BottomBar.DOAnchorPosY(-103, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    private void OnBackBtnClick()
    {
        WindowManager.Instance.CloseWindow(WinNames.VaccinePanel);
        VaccineGameManager.DestroyInstance();
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }

    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;

/// <summary>
/// 绷带拖拽
/// </summary>
public class VaccineBDDraggableItem : UIDragBase {

    public string _modelname;
    public string _texturename;
    VaccineTreatment vaccineTreatment;

    public override void InitComponet()
    {
        base.InitComponet();
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
    }

    public override void Update()
    {
        base.Update();
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        DragIdentify _current_dragIdentify = DragComparerManager.Instance._current_dragIdentify;
        if (_current_dragIdentify != null && _current_dragIdentify.ID == "hurtCollider")
        {
            Debug.Log("绷带匹配成功");
            Texture2D t2d = Res.LoadTexture2D(FileTools.CombinePath(HospitalConsts.game_hospital_texture_BasePath, _texturename));
            vaccineTreatment = VaccineGameManager.Instance.vaccineTreatment;
            Renderer render = vaccineTreatment._render;
            render.material.mainTexture = t2d;
            _currentDragAgent.SetActive(false);

            vaccineTreatment.OnBengdaiFinished();

        }
        base.OnEndDrag(eventData);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaccineTreatment : HospitalGameBase
{
    enum State {none,bingdai,pengwu,bengdai }   //冰袋，棚屋，绷带
    State _currentState;

    float _detumesceneAmount = 5;
    float _detumesceneProgress;
    Vector3 _camNormalPostion;
    Vector3 _closeUpPostion = new Vector3(0.979f, 0.362f, -1.253f);
    Camera _cam;
    public SkinnedMeshRenderer _render;

    Transform m_aishaRoot;

    public GameObject _bingdaiObject;
    public GameObject _pengwuObject;
    public GameObject _chuangkoutieObject;

    public RectTransform _layout_bingdai;
    public RectTransform layout_pengwu;
    public RectTransform layout_bengdai;

    NoInputCheckHanderEx _tishi;
    float _tick_duration = 15;

    public override void OnEnter()
    {
        //提示
        _tishi = new NoInputCheckHanderEx(_tick_duration, 10000);
        _tishi.AddNoInputHander(() => {

            switch (_currentState)
            {
                case State.none:
                    break;
                case State.bingdai:
                    PlayAudio(HospitalConsts.audio_ledi_tuizhong2);
                    //_layout_bingdai.GetComponentInChildren<Animator>().CrossFade("ShakeZ", .1f);
                    break;
                case State.pengwu:
                    PlayAudio(HospitalConsts.audio_ledi_tuizhong3);
                    //layout_pengwu.GetComponentInChildren<Animator>().CrossFade("ShakeZ", .1f);
                    break;
                case State.bengdai:
                    PlayAudio(HospitalConsts.audio_ledi_tuizhong5);
                    //layout_bengdai.GetComponentInChildren<Animator>().CrossFade("ShakeZ", .1f);
                    break;
                default:
                    break;
            }

        });
        _tishi.StartNoInputTimer();
        _currentState = State.bingdai;

        m_aishaRoot = HospitalResManager.Instance.m_aishaRoot.transform;
        _render = m_aishaRoot.Find("201004000@skin_hospital/304060602").GetComponent<SkinnedMeshRenderer>();

        Transform game_vaccineRoot = HospitalResManager.Instance.Game_BruiseRoot;
        _cam = game_vaccineRoot.GetComponentInChildren<Camera>();
        _camNormalPostion = _cam.transform.position;
        //_cam.transform.localPosition = _closeUpPostion;

        _detumesceneProgress = _detumesceneAmount;

        _layout_bingdai = VaccinePanel.Instance._layout_bingdai;
        layout_pengwu = VaccinePanel.Instance._layout_pengwu;
        layout_bengdai = VaccinePanel.Instance._layout_bengdai;

        //开始第一个治疗
        PlayAudio(HospitalConsts.audio_ledi_tuizhong2);

        _layout_bingdai.GetComponent<VaccineDragItem>().enabled = true;
        _layout_bingdai.GetComponentInChildren<Animator>().CrossFade("UIScale", .1f);
        VaccinePanel.Instance.DetuProgressbarAnimate(true);

        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnBeginDrag);

    }

    public override void OnLeave()
    {
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnBeginDrag);

        _cam.transform.position = _camNormalPostion;
    }

    public override void OnUpdate()
    {
        _tishi.OnUpdate();
    }

    private void OnBeginDrag(DragIdentify identify)
    {
        _tishi.StopCheck();
    }

    //消肿完成
    private void OnBingDaiFinished()
    {
        _currentState = State.pengwu;

        //出现对勾
        _layout_bingdai.Find("GouImage").SetActive(true);
        _layout_bingdai.GetComponent<Animator>().Play("Normal");
        _layout_bingdai.GetComponent<Animator>().enabled = false;
        _layout_bingdai.GetComponent<VaccineDragItem>().enabled = false;
        _bingdaiObject.SetActive(false);
        VaccinePanel.Instance.DetuProgressbarAnimate(false);

        //开始喷雾剂
        PlayAudio(HospitalConsts.audio_ledi_tuizhong3);
        layout_pengwu.GetComponent<VaccinePengwuDragItem>().enabled = true;
        layout_pengwu.GetComponentInChildren<Animator>().CrossFade("UIScale", .1f);
    }

    //涂抹消肿药膏完成
    public void OnPengwuFinished()
    {
        _currentState = State.bengdai;

        layout_pengwu.Find("GouImage").SetActive(true);

        //开始绑绷带
        PlayAudio(HospitalConsts.audio_ledi_tuizhong5);

        VaccineBDDraggableItem vaccineDraggableItem = layout_bengdai.GetComponent<VaccineBDDraggableItem>();
        vaccineDraggableItem.enabled = true;
        vaccineDraggableItem._isAutoBack = true;
        vaccineDraggableItem._userAgent = true;
        layout_pengwu.GetComponent<Animator>().Play("Normal");
        layout_pengwu.GetComponent<Animator>().enabled = false;
        layout_bengdai.GetComponentInChildren<Animator>().CrossFade("UIScale", .1f);
    }

    //绷带完成
    public void OnBengdaiFinished()
    {
        _currentState = State.none;

         layout_bengdai.Find("GouImage").SetActive(true);
        layout_bengdai.GetComponent<Animator>().enabled = false;

        VaccinePanel.Instance._5020040100.SetActive(true);
        PlayAudio(HospitalConsts.audio_tieyaogao,()=> {
            PlayAudio(HospitalConsts.audio_select_correct, () => {
                PlayAudio(HospitalConsts.audio_ledi_zuodebucuo);
            });
        });
        TimerManager.Instance.AddTimer(3f, () => {
            TransitionManager.Instance.StartTransition(() => {
                VaccineGameManager.Instance.ChangeState(VaccineGameManager.eVaccineGameState.Leave);
            }, null);
        });
    }

    //消肿过程
    public void OnXiaoZhongUpdate()
    {
        _tishi.StopCheck();
        _detumesceneProgress -= Time.deltaTime;
        float progress = _detumesceneProgress / _detumesceneAmount;
        if (progress >= 0)
        {
            VaccinePanel.Instance._DetuForeground.fillAmount = progress;
            _render.material.SetFloat("_BlendRadio", progress);
        }
        else
        {
            Debug.Log("消肿完成");
            OnBingDaiFinished();
        }
    }

    public override void Dispose()
    {
       
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VaccineItemSelect : HospitalGameBase
{
    DragIdentify _hover_dragIdentify;
    DragIdentify _dragidentify;
    bool _isItemSelectFinished;
    public RectTransform _layout_bingdai;
    public RectTransform layout_pengwu;
    public RectTransform layout_bengdai;

    public override void OnEnter()
    {
        _layout_bingdai = VaccinePanel.Instance._layout_bingdai;
        layout_pengwu = VaccinePanel.Instance._layout_pengwu;
        layout_bengdai = VaccinePanel.Instance._layout_bengdai;

        PlayAudio(HospitalConsts.audio_ledi_tuizhong1);

        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnScannerDragEnd);
    }

    public override void OnLeave()
    {

        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnScannerDragEnd);
    }

    private void OnScannerDragEnd(DragIdentify identity)
    {
        if (DragComparerManager.Instance.IsMatch(identity))
        {
            _hover_dragIdentify = DragComparerManager.Instance._current_dragIdentify;
            string iD = _hover_dragIdentify.ID;
            VaccineDraggableItem vaccineDraggableItem = identity.GetComponent<VaccineDraggableItem>();
            vaccineDraggableItem.ActiveAgentObject(false);
            string spritename = vaccineDraggableItem._iconname;
            string[] tempStr = vaccineDraggableItem._modelname.Split('|');
            string modelname = tempStr[0];
            string texname = tempStr[1];
            Image _foreImage = null;
            Image _slotImage = null;
            if (iD == "0")
            {
                _foreImage = _layout_bingdai.Find("ForeImage").GetComponent<Image>();
                _slotImage = _layout_bingdai.Find("SlotImage").GetComponent<Image>();

                _layout_bingdai.Find("502004073").SetActive(true);

                VaccineDragItem drag3d = HospitalHelper<VaccineDragItem>.GetSafeComponent(_layout_bingdai.gameObject);
                drag3d._modelPath = modelname;
                drag3d._texturePath = texname;
                drag3d.enabled = false;
                drag3d._isPlayBackAudio = false;

                VaccinePanel.Instance.XiaozhongAnimate(false,()=> { VaccinePanel.Instance.YaogaoAnimate(true); });
            }
            else if (iD == "1")
            {
                _foreImage = layout_pengwu.Find("ForeImage").GetComponent<Image>();
                _slotImage = layout_pengwu.Find("SlotImage").GetComponent<Image>();

                layout_pengwu.Find("502004073").SetActive(true);

                VaccinePengwuDragItem drag3d = HospitalHelper<VaccinePengwuDragItem>.GetSafeComponent(layout_pengwu.gameObject);
                drag3d._modelPath = modelname;
                drag3d._texturePath = texname;
                drag3d.enabled = false;
                drag3d._isPlayBackAudio = false;

                VaccinePanel.Instance.YaogaoAnimate(false, () => { VaccinePanel.Instance.Chuangkoutie(true); });
            }
            else if (iD == "2")
            {
                _foreImage = layout_bengdai.Find("ForeImage").GetComponent<Image>();
                _slotImage = layout_bengdai.Find("SlotImage").GetComponent<Image>();

                layout_bengdai.Find("502004073").SetActive(true);

                BinUIDragMono(modelname, texname, layout_bengdai);
                VaccinePanel.Instance.Chuangkoutie(false);

                _isItemSelectFinished = true;
            }

            //匹配成功音效
            PlayAudio(HospitalConsts.audio_select_correct);

            //关闭匹配检测
            HospitalHelper<HoverUI>.GetSafeComponent(_layout_bingdai.gameObject).enabled = false;
            //设置匹配后的ui显示
            _foreImage.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Hospital, spritename);
            _foreImage.gameObject.SetActive(true);

            if (_isItemSelectFinished)
            {
                TimerManager.Instance.AddTimer(1f, () => {
                    PlayAudio(HospitalConsts.audio_ledi_tui_yaopinzhunbei, () => {
                        VaccineGameManager.Instance.ChangeState(VaccineGameManager.eVaccineGameState.Treatment);
                    });
                });
            }
        }
    }

    private void BinUIDragMono(string modelname, string texname, RectTransform target)
    {
        VaccineBDDraggableItem drag3d = HospitalHelper<VaccineBDDraggableItem>.GetSafeComponent(target.gameObject);
        drag3d._isAutoBack = true;
        drag3d._modelname = modelname;
        drag3d._texturename = texname;
        drag3d.enabled = false;
        drag3d._isPlayBackAudio = false;
    }

    public override void OnUpdate()
    {
       
    }

    //public bool IsMatch(DragIdentify identify, bool use_prefix = false)
    //{
    //    _hover_dragIdentify = DragComparerManager.Instance._current_dragIdentify;
    //    if (_hover_dragIdentify == null || identify == null) return false;
    //    this._dragidentify = identify;
    //    string current_id = _hover_dragIdentify.ID.Substring(0,1);
    //    string target_id = identify.ID.Substring(0,1);
    //    Debug.Log("current_id: " + current_id + " target_id: " + target_id);

    //    return current_id == target_id;
    //}

    public override void Dispose()
    {
       
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;

public class VaccineDraggableItem : UIDragBase {

    public string _iconname;
    public string _modelname;

    public override void InitComponet()
    {
        base.InitComponet();
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
    }

    public override void Update()
    {
        base.Update();
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);

    }

    public void ActiveAgentObject(bool isActive)
    {
        this._currentDragAgent.SetActive(isActive);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;

/// <summary>
/// 冰敷拖拽
/// </summary>
public class DragIceCompress : UIDrag3DBase {

    public string iceBagName;
    Camera _cam;
    bool _isAddObjectDrag;  //是否添加了3d拖拽的脚本

    GameObject _icebag;
    public GameObject GetIceBag()
    {
        if (_icebag == null)
            _icebag = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath,HospitalConsts.icebagPath);
        string path = FileTools.CombinePath(HospitalConsts.game_hospital_texture_BasePath, iceBagName);
        Texture2D tex = Res.LoadTexture2D(path);
        _icebag.GetComponentInChildren<MeshRenderer>().material.mainTexture = tex;

        return _icebag;
    }

    public override void InitComponet()
    {
        base.InitComponet();
        _cam = ToothacheGameManager.Instance._cam;
        _loopAudioName = HospitalConsts.audio_bingfu_tumo;
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        if (!_isAddObjectDrag)
        {
            _isAddObjectDrag = true;

            _currentDragObject.GetComponentInChildren<Collider>().enabled = false;
            DragIceObject _dragiceObject = _currentDragObject.AddComponentEx<DragIceObject>();
            _dragiceObject._isAutoBack = false;
            _dragiceObject.GetComponentInChildren<DragIdentify>().ID = _identyself.ID;
            _dragiceObject.ResetStartDragPosition();
        }

    }

    public override Vector3 Get3DPosition()
    {
        Plane plane = new Plane(_cam.transform.forward * -1, new Vector3(.6f, 1f, -0.96f));
        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public override void OnDrag(PointerEventData eventData)
    {
        base.OnDrag(eventData);

        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Draggable")))
        {
            _targetDragObjectRotation = Quaternion.LookRotation(hit.normal);
            ActiveLoopAudio(true);
        }
        else
        {
            _targetDragObjectRotation = Quaternion.LookRotation(_cam.transform.forward * -1);
            ActiveLoopAudio(false);
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        _currentDragObject.GetComponentInChildren<Collider>().enabled = true;
        this.enabled = false;
        ActiveLoopAudio(false);
    }

    public override Transform _currentDragObject
    {
        get
        {
            Transform transform1 = GetIceBag().transform;
            transform1.LookAt(_cam.transform);
            return transform1;
        }

        set
        {
            base._currentDragObject = value;
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        Destroy(_icebag);
    }


}

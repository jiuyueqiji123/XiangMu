﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;

/// <summary>
/// 冰敷拖拽
/// </summary>
public class DragIceObject : Object3dDragBase {

    public string iceBagName;
    Camera _cam;
    bool _is_tumo_start;
    GameAudioSource _audio_tumo;

    public override void InitComponet()
    {
        base.InitComponet();
        _cam = ToothacheGameManager.Instance._cam;
        _loopAudioName = HospitalConsts.audio_bingfu_tumo;
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);

    }

    public override Vector3 Get3DPosition()
    {
        Plane plane = new Plane(_cam.transform.forward * -1, new Vector3(.6f, 1f, -0.96f));
        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public override void OnDrag(PointerEventData eventData)
    {
        base.OnDrag(eventData);

        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Draggable")))
        {
            _targetDragObjectRotation = Quaternion.LookRotation(hit.normal);
            ActiveLoopAudio(true);
        }
        else
        {
            _targetDragObjectRotation = Quaternion.LookRotation(_cam.transform.forward * -1);
            ActiveLoopAudio(false);
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        ActiveLoopAudio(false);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToothacheLeave : HospitalGameBase
{
    public override void OnEnter()
    {
        WindowManager.Instance.CloseWindow(WinNames.ToothachePanle);
        HospitalResManager.Instance.Game_ToochacheRoot.SetActive(false);
        HospitalEventHandler.Instance.BroadCastEvent(HospitalEventID.SMALLGAME_OVER);
    }

    public override void OnLeave()
    {
        
    }

    public override void OnUpdate()
    {
       
    }

    public override void Dispose()
    {

    }
}

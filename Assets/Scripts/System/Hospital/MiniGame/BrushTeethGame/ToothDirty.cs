﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToothDirty : MonoBehaviour {

    public float _cleanLife = 1f;
    float _timer;
    float _targetTimer;
    float _progress;
    Material _mat;

    private void Start()
    {
        _mat = GetComponentInChildren<SkinnedMeshRenderer>().material;
    }

    private void Update()
    {
        _timer = Mathf.Lerp(_timer, _targetTimer, .5f);
        _progress = _timer / _cleanLife;
        //transform.localScale = Vector3.one * (1 - _progress);
        _mat.color = new Color(_mat.color.r, _mat.color.g, _mat.color.b, (1 - _progress));
        if (_timer > _cleanLife)
        {
            _timer = 0;
            transform.SetActive(false);

            ToothacheGameManager.Instance.brushTeeth.ToothDirtyCleanOverCheck();
        }
    }

    public void OnBrushCleanUpdate()
    {
        _targetTimer += .2f;
       
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;

public class BrushTeeth : HospitalGameBase
{
    public Transform _brushObject;
    public Transform _cleanerObject;

    Vector3 _camNormalPostion;
    Vector3 _closeUpPostion = new Vector3(0.626f,1.073f,-1.237f);
    Camera _cam;
    NoInputCheckHanderEx _tipAudio;
    NoInputCheckHanderEx _hengAudio;
    NoInputCheckHanderEx _shakeTip;
    SkinnedMeshRenderer _toothRender;
    List<Transform> _toothdirtys;
    bool _isOnBrushShake;
    int _toothdirty_count = 7;
    float _tick_duration = 15;
    float _brushteethTime = 5;
    float _cleanteethTime = 5;
    float _timerbrush;
    float _timerclean;
    bool _isbrushStart;
    bool _iscleanStart;
    public bool _isOnOperation; //是否正在刷牙或清洗
    public bool _iscleanfinished;
    public bool _isbrushfinished;
    float _brushProgress;
    float _brushcleanProcess;
    AnimatorEx _nuowa_animatorex;
    Transform m_nuowaRoot;
    Transform m_nuowa_toothache_root;
    Transform m_game_toothache_root;

    public override void OnEnter()
    {
        _toothdirtys = new List<Transform>();
        m_nuowaRoot = HospitalResManager.Instance.m_nuowaRoot;
        m_nuowa_toothache_root = HospitalResManager.Instance.nuowa_toothache_root.transform;
        m_game_toothache_root = HospitalResManager.Instance.Game_ToochacheRoot;
        _toothRender = m_nuowa_toothache_root.Find("201005000@skin_toothache/201005000").GetComponent<SkinnedMeshRenderer>();

        //刷牙提醒
        _tipAudio = new NoInputCheckHanderEx(_tick_duration, 1);
        _tipAudio.AddNoInputHander(() => { PlayAudio(HospitalConsts.audio_ledi_shuaya_tixing); });
        _tipAudio.StartNoInputTimer();

        //刷牙时候的声音
        _hengAudio = new NoInputCheckHanderEx(5, 10000);
        _hengAudio.AddNoInputHander(() => { PlayAudio(HospitalConsts.audio_nuowa_yateng_3); });
        _hengAudio.StartNoInputTimer();

        //摇晃提醒
        _shakeTip = new NoInputCheckHanderEx(_tick_duration, 10000);
        _shakeTip.AddNoInputHander(ShakeTip);
        _shakeTip.StartNoInputTimer();

        IsFinish = false;

        GameObject _brushParent = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, "BrushParent");
        GameObject _cleanerParent = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, "CleanerParent");
        _brushParent.transform.SetParent(HospitalResManager.Instance.Game_ToochacheRoot);
        _cleanerParent.transform.SetParent(HospitalResManager.Instance.Game_ToochacheRoot);
        _brushObject = _brushParent.transform.GetChild(0);
        _cleanerObject = _cleanerParent.transform.GetChild(0);
        HospitalController.Instance.RegistCacheObject(_brushParent);
        HospitalController.Instance.RegistCacheObject(_cleanerParent);
        _brushObject.SetActive(true);
        _cleanerObject.SetActive(true);

        m_nuowaRoot.SetActive(false);
        m_nuowa_toothache_root.SetActive(true);
        m_nuowa_toothache_root.SetTransformFormTarget(m_nuowaRoot);
        _nuowa_animatorex = new AnimatorEx(m_nuowa_toothache_root.gameObject);

        Transform game_ToochacheRoot = HospitalResManager.Instance.Game_ToochacheRoot;
        _cam = game_ToochacheRoot.GetComponentInChildren<Camera>();
        _camNormalPostion = _cam.transform.localPosition;

        _nuowa_animatorex.Play(eHospitalNuowaAnim.open_mouth.ToString());

        _cam.transform.localPosition = _closeUpPostion;

        ToothachePanel.Instance._ToothBrush.SetActive(true);

        PlayAudio(HospitalConsts.audio_ledi_yatongyuanyin,null, false, .5f);

        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnBeginDrag);
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnEndDrag);
    }

    public override void OnLeave()
    {
        m_nuowaRoot.SetActive(true);
        m_nuowa_toothache_root.SetActive(false);

        ToothachePanel.Instance._ToothBrush.SetActive(false);
        _cam.transform.localPosition = _camNormalPostion;

        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnBeginDrag);
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnEndDrag);
    }

    public override void Dispose()
    {
        if (IsEnter)
        {
            OnLeave();
        }
    }

    public override void OnUpdate()
    {
        _hengAudio.OnUpdate();
        _tipAudio.OnUpdate();
        _shakeTip.OnUpdate();
        if(_nuowa_animatorex != null) _nuowa_animatorex.OnUpdate();
        TeethBrushFinishCheck();
    }

    private void ShakeTip()
    {
        _isOnBrushShake = !_isOnBrushShake;
        if (_isOnBrushShake)
        {
            ToothachePanel.Instance._Brush.Play("Shake", 0, .1f);
        }
        else
        {
            ToothachePanel.Instance._Cleaner.Play("Shake", 0, .1f);
        }
    }

    public void ToothDirtyCleanOverCheck()
    {
        _toothdirty_count--;
        if (_toothdirty_count <= 0)
            _iscleanfinished = true;
    }

    private void TeethBrushFinishCheck()
    {
        if (IsFinish) return;

        if (_isbrushfinished && _iscleanfinished)
        {
            Debug.Log("=======刷牙结束");
            Debug.Log("显示洗干净特效！");
            IsFinish = true;
            _brushObject.SetActive(false);
            _cleanerObject.SetActive(false);

            _hengAudio.StopCheck();

            ToothachePanel.Instance._5020040100.SetActive(true);
            _hengAudio.StopCheck();
            PlayAudio(HospitalConsts.audio_select_correct);
            PlayAudio(HospitalConsts.audio_nuowa_yateng_4);
            _nuowa_animatorex.Play(eHospitalNuowaAnim.clean_success.ToString()).OnFinished(() => {
                TransitionManager.Instance.StartTransition(() => {
                    ToothacheGameManager.Instance.ChangeState(ToothacheGameManager.eToothacheGameState.Leave);
                    GameObject.Destroy(m_nuowa_toothache_root.gameObject);
                }, null);
            });
        }
    }

    private void OnBeginDrag(DragIdentify identify)
    {
        _shakeTip.StopCheck();
        _tipAudio.StopCheck();
    }

    private void OnEndDrag(DragIdentify indentify)
    {
       
    }

    public void OnBrushTeeth()
    {
        if (!_isbrushfinished)
        {
            //Debug.Log(_timerbrush);
            _timerbrush += Time.deltaTime;
            _brushProgress = _timerbrush / _brushteethTime;
            _toothRender.materials[1].SetFloat("_BlendRadio", 1 - _brushProgress -.05f);
            _toothRender.materials[2].SetFloat("_BlendRadio", 1 - _brushProgress - .05f);
            if (_timerbrush > _brushteethTime)
            {
                _isbrushfinished = true;
            }
        }
    }

}

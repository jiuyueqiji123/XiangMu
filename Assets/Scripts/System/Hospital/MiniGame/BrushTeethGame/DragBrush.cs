﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;

/// <summary>
/// 牙刷
/// </summary>
public class DragBrush : Object3dDragBase {

    public string _brushPath;
    Camera _cam;
    GameObject _brush;
    Transform _effect;
    bool _isopenPupple;
    Vector3 _lastHitPoint;
    BrushTeeth _brushTeeth;

    public override void InitComponet()
    {
        base.InitComponet();
        _cam = ToothacheGameManager.Instance._cam;
        _effect = transform.Find("502004096");
        _loopAudioName = HospitalConsts.yinxiao_yashuashuaya;
        InvokeRepeating("OnShake", 10f, 10f);
        _brushTeeth = ToothacheGameManager.Instance.brushTeeth;
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        _brushTeeth._isOnOperation = true;
    }

    public override Vector3 Get3DPosition()
    {
        Plane plane = new Plane(_cam.transform.forward * -1, new Vector3(0.4f, 1f, -0.96f));
        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin,ray.direction,Color.red);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public override void OnDrag(PointerEventData eventData)
    {
        base.OnDrag(eventData);

        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Draggable")))
        {
            _targetDragObjectPosition = hit.point;
            _targetDragObjectRotation = Quaternion.LookRotation(hit.normal * -1);
            if (DragComparerManager.Instance.IsMatch(this._identyself, false))
            {
                ToothacheGameManager.Instance.brushTeeth.OnBrushTeeth();

                Vector3 delta = hit.point - _lastHitPoint;
                //Debug.Log(delta);
                if (!_isopenPupple)
                {
                    _isopenPupple = true;
                    _effect.SetActive(true);
                    ActiveLoopAudio(true);
                }
                //if (delta.magnitude > .1f)
                //{

                //}
                _lastHitPoint = hit.point;
            }
        }
        else
        {
            _targetDragObjectRotation = Quaternion.LookRotation(_cam.transform.forward);

            if (_isopenPupple)
            {
                _isopenPupple = false;
                _effect.SetActive(false);
                ActiveLoopAudio(false);
            }
        }

    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        _brushTeeth._isOnOperation = false;

        if (_isopenPupple)
        {
            _isopenPupple = false;
            _effect.SetActive(false);
            ActiveLoopAudio(false);
        }
    }

    protected override void OnShake()
    {
        base.OnShake();
        if (!_isOnTweening && !_onDragging && !_brushTeeth._isOnOperation && !_brushTeeth._isbrushfinished)
        {
            HospitalUtil.PlayAudio(HospitalConsts.audio_huandong);
            transform.parent.GetComponent<Animator>().CrossFade("Shake", .1f);
        }
    }
}

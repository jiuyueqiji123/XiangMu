﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 消肿
/// </summary>
public class Detumescence : HospitalGameBase
{
    float _detumesceneAmount = 5;
    float _detumesceneProgress;
    NoInputCheckHanderEx _tip;
    NoInputCheckHanderEx _tengtong;
    bool _isDetuSelected;
    bool _isOnAudioPlaying;

    public override void OnEnter()
    {
        _tip = new NoInputCheckHanderEx(20, 10000);
        _tip.AddNoInputHander(()=> {
            if (!_isOnAudioPlaying)
            {
                _isOnAudioPlaying = true;
                   PlayAudio(HospitalConsts.audio_ledi_yatong_xiaozhong,()=> { _isOnAudioPlaying = false; });
            }
        });
        _tip.StartNoInputTimer(0f,true);

        _tengtong = new NoInputCheckHanderEx(12, 10000);
        _tengtong.AddNoInputHander(() => {
            if (!_isOnAudioPlaying)
            {
                _isOnAudioPlaying = true;
                PlayAudio(HospitalConsts.audio_nuowa_yateng_1, () => { _isOnAudioPlaying = false; });
            }
        });
        _tengtong.StartNoInputTimer(5f, true);

        IsFinish = false;
        _isDetuSelected = false;
        _detumesceneProgress = _detumesceneAmount;

        ToothachePanel.Instance.RightBarAnimate(true);

        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_POINTDOWN_UI, OnDetumesceneUpdate);
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnBeginDrag);

    }

    public override void OnLeave()
    {

        ToothachePanel.Instance._Detumescence.SetActive(false);

        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_POINTDOWN_UI, OnDetumesceneUpdate);
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnBeginDrag);
    }

    public override void Dispose()
    {
        if (IsEnter)
        {
            OnLeave();
        }
    }

    public override void OnUpdate()
    {
        if(Input.GetKeyDown(KeyCode.X))
            ToothacheGameManager.Instance.ChangeState(ToothacheGameManager.eToothacheGameState.BrushTeeth);

        _tip.OnUpdate();
        _tengtong.OnUpdate();
    }

    private void OnBeginDrag(DragIdentify identify)
    {
        _tip.StopCheck();

        if (!_isDetuSelected)
        {
            _isDetuSelected = true;
            ToothachePanel.Instance.RightBarAnimate(false);
            ToothachePanel.Instance.DetuProgressbarAnimate(true);
        }
    }

    public void OnDetumesceneUpdate(DragIdentify identify)
    {
        if (IsFinish) return;

        if (DragComparerManager.Instance.IsMatch(identify))
        {
            //Debug.Log(_detumesceneProgress);
            _detumesceneProgress -= Time.deltaTime;
            float progress = _detumesceneProgress / _detumesceneAmount;
            if (progress >= 0)
            {
                ToothachePanel.Instance._DetuForeground.fillAmount = progress;
            }
            else
            {
                Debug.Log("消肿完成");
                _tengtong.StopCheck();
                IsFinish = true;
                ToothachePanel.Instance.DetuProgressbarAnimate(false);
                DragComparerManager.Instance._currentDrag3dObject.SetActive(false);
                PlayAudio(HospitalConsts.audio_ledi_zhangzui, () => {
                    TransitionManager.Instance.StartTransition(() => {
                        ToothacheGameManager.Instance.ChangeState(ToothacheGameManager.eToothacheGameState.BrushTeeth);
                    }, null);
                });
            }

        }
    }
}

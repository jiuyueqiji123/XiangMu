﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;


public class ToothachePanel : SingletonBaseWindow<ToothachePanel>
{

    #region ui property
    public RectTransform _Detumescence { get; set; }
    public RectTransform _ToothBrush { get; set; }
    public Animator _Brush { get; set; }
    public Animator _Cleaner { get; set; }
    public Image _DetuForeground { get; set; }
    public RectTransform _DetuProgressbar { get; set; }
    public RectTransform _DraggableImage { get; set; }
    public RectTransform _RightBar { get; set; }
    public RectTransform _RightSubRoot { get; set; }
    public Button _BtnBack { get; set; }
    public Transform _5020040100 { get; set; }

    #endregion

    protected override void AddListeners()
    {
        base.AddListeners();
        _BtnBack.onClick.AddListener(OnBackBtnClick);
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();
        _BtnBack.onClick.RemoveListener(OnBackBtnClick);
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        DragComparerManager.Instance._currentDragAgent = _DraggableImage;
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        ToothacheGameManager.Instance.OnUpdate();
    }

    protected override void OnClose()
    {
        base.OnClose();
    }


    #region UIEvent

    public void RightBarAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _RightBar.DOAnchorPosX(-806, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _RightBar.DOAnchorPosX(-554, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    public void DetuProgressbarAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _DetuProgressbar.DOAnchorPosX(191, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _DetuProgressbar.DOAnchorPosX(-77, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    private void OnBackBtnClick()
    {
        WindowManager.Instance.CloseWindow(WinNames.ToothachePanle);
        ToothacheGameManager.DestroyInstance();
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);

    }

    #endregion
}

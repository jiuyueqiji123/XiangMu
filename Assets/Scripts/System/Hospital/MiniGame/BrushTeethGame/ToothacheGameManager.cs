﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToothacheGameManager : Singleton<ToothacheGameManager> {


    public enum eToothacheGameState {

        None,
        Detumescence,   //消肿
        BrushTeeth, //刷牙
        Leave   //成功告别

    }

    eToothacheGameState state;

    HospitalGameBase _current_stateBase;
    public Detumescence detumescence;
    public BrushTeeth brushTeeth;
    public ToothacheLeave toothacheLeave;

    public AnimatorEx _nuowa_animatorex;
    public AnimatorEx m_ledi_animatorEx;

    public Camera _cam;

    public override void Init()
    {
        base.Init();

        state = eToothacheGameState.None;
        detumescence = new Detumescence();
        brushTeeth = new BrushTeeth();
        toothacheLeave = new ToothacheLeave();

        _cam = HospitalResManager.Instance.Game_ToochacheRoot.GetComponentInChildren<Camera>();

        _nuowa_animatorex = new AnimatorEx(HospitalResManager.Instance.m_nuowaRoot.gameObject);
        m_ledi_animatorEx = new AnimatorEx(HospitalResManager.Instance.m_lediRoot.gameObject);
        _nuowa_animatorex.CrossFade(eHospitalNuowaAnim.s_idle_tooth.ToString(), .1f);
    }

    public void GameStart()
    {
        HospitalResManager.Instance.Game_ToochacheRoot.SetActive(true);

        WindowManager.Instance.CloseWindow(WinNames.ClinicRoomPanel);
        WindowManager.Instance.OpenWindow(WinNames.ToothachePanle);
        ChangeState(eToothacheGameState.Detumescence);
    }

    public void ChangeState(eToothacheGameState newstate)
    {
        if (_current_stateBase != null && newstate != state)
            _current_stateBase.OnLeave();

        switch (newstate)
        {
            case eToothacheGameState.None:

                break;
            case eToothacheGameState.Detumescence:
                _current_stateBase = detumescence;
                break;
            case eToothacheGameState.BrushTeeth:
                _current_stateBase = brushTeeth;
                break;
            case eToothacheGameState.Leave:
                _current_stateBase = toothacheLeave;
                break;
            default:
                break;
        }

        state = newstate;
        _current_stateBase.OnEnter();
    }

    public void OnUpdate()
    {
        if(_current_stateBase != null)
            _current_stateBase.OnUpdate();
        _nuowa_animatorex.OnUpdate();
        m_ledi_animatorEx.OnUpdate();
    }

    public override void UnInit()
    {
        base.UnInit();

        detumescence.Dispose();
        brushTeeth.Dispose();
        toothacheLeave.Dispose();
    }

}

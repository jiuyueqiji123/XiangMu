﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BacteriaGameRootHolder : MonoBehaviour {

    public Transform _backteriaSpawnPointRoot;
    public Transform _drugsSpawnPointRoot;
    public Transform _drugsFinishPointRoot;
    public Transform _cleanEffect;
    public Camera _Camera;

    public List<Transform> _bacteriaSpawnPoints;
    public List<Transform> _drugsSpawnPoints;


    private void Awake()
    {
        foreach (Transform spawnpoint in _backteriaSpawnPointRoot)
        {
            _bacteriaSpawnPoints.Add(spawnpoint);
        }

        foreach (Transform spawnpoint in _drugsSpawnPointRoot)
        {
            _drugsSpawnPoints.Add(spawnpoint);
        }
    }

}

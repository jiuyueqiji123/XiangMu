﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// 语音激励
/// </summary>
public class StomachVoiceExcitation : HospitalGameBase
{
    NoInputCheckHanderEx _scannerShake;
    float _tick_duration = 15;
    Transform _bottombar_subroot;
    Transform _drugeatAnchor;
    HoverUI _mouseAnchorImg;
    HoverUI _stomachAnchorImg;
    DragIdentify _currentDragIdentify;
    public Dictionary<string, StomachDrugSlot> _drugsCompareDic;

    bool _is_talk_finished;
    bool _on_talking;
    bool _is_drug_eatfinished;
    bool _isabsoubed;
    int _drug_eat_amout;

    AnimatorEx _nuowa_animatorex;

    

    public override void OnEnter()
    {
        Debug.Log("语音激励游戏");

        _scannerShake = new NoInputCheckHanderEx(_tick_duration, 10000);
        _scannerShake.AddNoInputHander(()=> { PlayAudio(HospitalConsts.audio_huandong); StomachachePanle.Instance._Scaner.Play("Shake", 0, .1f); });

        _is_talk_finished = false;
        _on_talking = false;
        _is_drug_eatfinished = false;
        _drug_eat_amout = 0;

        //PlayAudio(HospitalConsts.audio_ledi_weiyao_tixing);

        _drugeatAnchor = HospitalResManager.Instance.m_nuowaRoot.Find("201005000@skin_hospital/root/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/DrugEatAnchor");

        _nuowa_animatorex = StomachacheGameManager.Instance._nuowa_animatorex;
        _drugsCompareDic = StomachacheGameManager.Instance.stomachDispen._drugsCompareDic;

        _mouseAnchorImg = StomachachePanle.Instance._MouseAnchorImg;
        _stomachAnchorImg = StomachachePanle.Instance._StomachAnchorImg;
        _bottombar_subroot = DispenPanel.Instance._BottomSubRoot;

        _mouseAnchorImg.gameObject.SetActive(true);

        HoverUI[] hoveruis = _bottombar_subroot.GetComponentsInChildren<HoverUI>();
        for (int i = 0; i < hoveruis.Length; i++)
        {
            hoveruis[i].enabled = false;
            DraggableDrug3D _draggable_drug = HospitalHelper<DraggableDrug3D>.GetSafeComponent(hoveruis[i].gameObject);
            _draggable_drug.enabled = true;
            _draggable_drug._isAutoBack = true;
        }

        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnHoverOverMouse);
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnDrugDragEnd);
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnDrugDragBegin);
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAGGING, OnDrugDrag);

    }

    public void OnFeverEnter()
    {
        _bottombar_subroot = DispenPanel.Instance._BottomSubRoot;

        HoverUI[] hoveruis = _bottombar_subroot.GetComponentsInChildren<HoverUI>();
        for (int i = 0; i < hoveruis.Length; i++)
        {
            hoveruis[i].enabled = false;
            DraggableDrug3D _draggable_drug = HospitalHelper<DraggableDrug3D>.GetSafeComponent(hoveruis[i].gameObject);
            _draggable_drug.enabled = true;
            _draggable_drug._isAutoBack = true;
        }

        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnDrugDragEnd);
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnDrugDragBegin);
    }

    public override void OnLeave()
    {
        if (_mouseAnchorImg == null || _stomachAnchorImg == null) return;
        _mouseAnchorImg.gameObject.SetActive(false);
        _stomachAnchorImg.gameObject.SetActive(false);

        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnHoverOverMouse);
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnDrugDragEnd);
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnDrugDragBegin);
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAGGING, OnDrugDrag);
    }

    public override void Dispose()
    {
        if (IsEnter) OnLeave();
    }

    public override void OnUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Debug.Log("跳过了说话");
            _is_talk_finished = true;

        }
    }

    private void OnHoverOverMouse(DragIdentify identify)
    {
        Debug.Log("鼠标到了嘴巴上面");
        if (!_is_talk_finished)
        {
            if (!_on_talking)
            {
                _on_talking = true;

                string audioPath = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, HospitalConsts.audio_nuowa_buchiyao2);
                //拒绝吃药
                _nuowa_animatorex.PlaySequence(eHospitalNuowaAnim.all_talk_refuse_01.ToString(), audioPath, () => {
                    LeDiTalkManager.Instance.LediMoveIn(() => {
                        audioPath = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, HospitalConsts.audio_ledi_chiyaoguli);
                        //乐迪鼓励
                        LeDiTalkManager.Instance.Talk(audioPath, () => {
                            audioPath = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, HospitalConsts.audio_nuowa_buchiyao1);
                            //诺娃拒绝吃药
                            _nuowa_animatorex.PlaySequence(eHospitalNuowaAnim.all_talk_refuse_01.ToString(), audioPath, () => {
                                //一起加油
                                audioPath = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, HospitalConsts.audio_ledi_jiayou);
                                LeDiTalkManager.Instance.Talk(audioPath, () =>
                                {
                                    audioPath = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, HospitalConsts.audio_ledi_jiayou1);
                                    //加油
                                    LeDiTalkManager.Instance.Talk(audioPath, () => {
                                        //等待回应
                                        HospitalUtil.WaitSpeak(() => {

                                            Debug.Log("角色点头同意");
                                            _nuowa_animatorex.CrossFade(eHospitalNuowaAnim.s_nod.ToString(), .1f);
                                            TimerManager.Instance.AddTimer(1f, () => {

                                                _nuowa_animatorex.CrossFade(eHospitalNuowaAnim.s_idle_stomachache.ToString(), .1f);
                                                LeDiTalkManager.Instance.LediMoveOut();

                                                _on_talking = false;
                                                _is_talk_finished = true;

                                            });

                                        });
                                    });

                                });
                            });
                        });

                    });
                });

            }
        }


        
    }

    private void OnDrugDragBegin(DragIdentify identity)
    {
        if (_scannerShake == null)
        {
            return;
        }
        _scannerShake.StopCheck();

        //拖拽时，角色嘴巴张开
        if(!_on_talking && _is_talk_finished && identity.ID != "scanner" && _nuowa_animatorex != null)
            _nuowa_animatorex.CrossFade(eHospitalNuowaAnim.med_open_mouse.ToString(), .1f);
    }

    private void OnDrugDrag(DragIdentify identify)
    {
        //拖拽扫描仪到诺娃肚子上，触发细菌消灭游戏
        if (identify.ID == "scanner")
        {
            bool ismatch = DragComparerManager.Instance.IsMatch(identify, false);
            if (ismatch)
            {
                if (!_isabsoubed)
                {
                    _isabsoubed = true;

                    Debug.Log("拖到了肚子上，准备开游戏");
                    DraggableScaner _scaner = identify.GetComponent<DraggableScaner>();
                    _scaner._isAutoBack = false;
                    _scaner.enabled = false;
                    _scaner.StartScanEffect();

                    Transform _scannerTrans = _scaner.transform;
                    Transform _StomachAnchorImg = StomachachePanle.Instance._StomachAnchorImg.transform;
                    _scannerTrans.SetTransformFormTarget(_StomachAnchorImg);

                    TimerManager.Instance.AddTimer(2f, () =>
                    {
                        PlayAudio(HospitalConsts.audio_ledi_zhengzaixiaomie,()=> {
                            TransitionManager.Instance.StartTransition(() =>
                            {

                                HospitalResManager.Instance.root.SetActive(false);
                                StomachachePanle.Instance._ScanerPanel.gameObject.SetActive(false);

                                StomachacheGameManager.Instance.ChangeState(StomachacheGameManager.eStamachGameState.Exterminate_bacteria);

                            },null);
                        });
                    });

                }
            }
        }
    }

    private void OnDrugDragEnd(DragIdentify identify)
    {
        //关闭角色嘴巴
        if (!_on_talking && _is_talk_finished && identify.ID != "scanner" && _nuowa_animatorex != null)
            _nuowa_animatorex.CrossFade(eHospitalNuowaAnim.med_close_mouse.ToString(), .1f);

        if (_is_talk_finished)
        {
            _currentDragIdentify = DragComparerManager.Instance._current_dragIdentify;
            //拖拽药片到诺娃嘴边
            if (_currentDragIdentify != null && _currentDragIdentify.ID == "MouseEvt")
            {

                if (!_is_drug_eatfinished)
                {
                    //使3d药片飞进嘴巴里
                    Transform _currentDrag3dObject = DragComparerManager.Instance._currentDrag3dObject;
                    UIDrag3DBase uIDrag3DBase = identify.GetComponent<UIDrag3DBase>();
                    uIDrag3DBase.SetStartDragPosition(_drugeatAnchor.position);
                    uIDrag3DBase.enabled = false;
                    uIDrag3DBase._isPlayBackAudio = false;
                    uIDrag3DBase.SetGraphicsVisble(uIDrag3DBase.transform, false);

                    TimerManager.Instance.AddTimer(1.5f, () => { _currentDrag3dObject.SetActive(false); });
                    identify.transform.Find("ForeImage").SetActive(false);
                    _nuowa_animatorex.CrossFade(eHospitalNuowaAnim.eat_med_02.ToString(), .1f);

                    _drug_eat_amout++;
                    if (_drug_eat_amout >= 3)
                    {
                        Debug.Log("吃完的药片数量： " + _drug_eat_amout);
                        _is_drug_eatfinished = true;
                        _stomachAnchorImg.gameObject.SetActive(true);
                        _mouseAnchorImg.gameObject.SetActive(false);

                        DispenPanel.Instance.BottomBarAnimate(false);
                        StomachachePanle.Instance._ScanerPanel.gameObject.SetActive(true);
                        StomachachePanle.Instance.ScanerAnimate(true, () => {
                            _scannerShake.StartNoInputTimer();
                            PlayAudio(HospitalConsts.audio_ledi_saomiao);
                        });


                    }

                }

            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrugFighter : StomachExterminateItemBase {

    public StomachDrugFighter _drugFighterInfo;
    public SpriteDragBase _dragger;
    Transform _walkEffect;


    // Use this for initialization
    void Start () {

        _dragger = GetComponentInChildren<SpriteDragBase>();
        _walkEffect = transform.Find("502004095");

        Sprites = GetSprites(_drugFighterInfo.m_spritename_idle);
        _spriteAniamtion.AutoPlayDelay = Random.Range(0f, 1f);
        _spriteAniamtion.Loop = true;

        InvokeRepeating("CombatOverCheck", 0f, .5f);

    }
	
	// Update is called once per frame
	void Update () {

        if (_isReadyCombat)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //
            }
        }

	}

    public void OnFighter()
    {
        Sprites = GetSprites(_drugFighterInfo.m_spritename_kill);
        TimerManager.Instance.AddTimer(6f / _spriteAniamtion.FPS, () =>
        {
            Sprites = GetSprites(_drugFighterInfo.m_spritename_idle);
        });
    }

    private void OnTriggerEnter(Collider other)
    {
        string name = other.name;
        if (name.StartsWith("enemy"))
        {
            Bacteria bac = other.GetComponent<Bacteria>();
            if (bac._color == this._color)
            {
                bac.OnColliderDrug(this);

                //停止拖拽
                _dragger._IsEnableDrag = false;
                _isReadyCombat = true;
            }
        }
    }

    private void CombatOverCheck()
    {
        if (!_isCombatOver)
        {
            StomachExterminate stomachExterminate = StomachacheGameManager.Instance.stomachExterminate;
            int amount = stomachExterminate.GetCurrentBacteriaAmount(this._color);
            if (amount == 0)
            {
                _isCombatOver = true;
                transform.SetTransformFormTarget(stomachExterminate._holder._drugsFinishPointRoot);
                transform.position += Vector3.up * amount;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public enum eStomachDrugShape { ellipse, square, circle , triangle }
public enum eStomachDrugColor { red, blue, yellow}
public enum eBacteriaType { type1,type2,type3 }

//药片填充槽
[System.Serializable]
public class StomachDrugSlot
{
    public Transform m_myTransform;
    public eStomachDrugShape m_stomachDrugShape;
    public eStomachDrugColor m_stomachDrugColor;
    public string m_spritename;
    public bool m_is_have_fill; //这个槽是否被对应道具填充

    public void SetOwer(Transform ower)
    {
        this.m_myTransform = ower;
    }

    public StomachDrugSlot(eStomachDrugShape shape, eStomachDrugColor color,GameObject ower = null)
    {
        this.m_stomachDrugShape = shape;
        this.m_stomachDrugColor = color;
        if(ower != null) this.m_myTransform = ower.transform;  
        
        m_spritename = "ywjy_";
        switch (shape)
        {
            case eStomachDrugShape.triangle:
                m_spritename += "sj";
                break;
            case eStomachDrugShape.ellipse:
                m_spritename += "ty";
                break;
            case eStomachDrugShape.circle:
                m_spritename += "yx";
                break;
            case eStomachDrugShape.square:
                m_spritename += "zf";
                break;
            default:
                break;
        }

        switch (color)
        {
            case eStomachDrugColor.red:
                m_spritename += "hongse";
                break;
            case eStomachDrugColor.yellow:
                m_spritename += "huangse";
                break;
            case eStomachDrugColor.blue:
                m_spritename += "lanse";
                break;
            default:
                break;
        }
    }
}

public class StomachDrug
{
    public eStomachDrugShape m_stomachDrugShape;
    public eStomachDrugColor m_stomachDrugColor;
    public string m_spritename;

    public StomachDrug(eStomachDrugShape shape, eStomachDrugColor color)
    {
        this.m_stomachDrugShape = shape;
        this.m_stomachDrugColor = color;

        m_spritename = "yw_";
        switch (shape)
        {
            case eStomachDrugShape.triangle:
                m_spritename += "sj";
                break;
            case eStomachDrugShape.ellipse:
                m_spritename += "ty";
                break;
            case eStomachDrugShape.circle:
                m_spritename += "yx";
                break;
            case eStomachDrugShape.square:
                m_spritename += "zf";
                break;
            default:
                break;
        }

        switch (color)
        {
            case eStomachDrugColor.red:
                m_spritename += "hongse";
                break;
            case eStomachDrugColor.yellow:
                m_spritename += "huangse";
                break;
            case eStomachDrugColor.blue:
                m_spritename += "lanse";
                break;
            default:
                break;
        }
    }
}

/// <summary>
/// 匹配
/// </summary>
public class StomachDispen : HospitalGameBase
{
    public Action _onDispenComplete;

    int randomTimes = 3;
    int shapeIndex;
    int colorIndex;
    int _dipenSuccessAmount;    //配药成功的个数

    RectTransform _BottomBar;
    RectTransform _RightBar;

    public List<StomachDrugSlot> stomachDrugs;
    public Dictionary<string, StomachDrugSlot> _drugsCompareDic;

    public override void OnEnter()
    {
        _dipenSuccessAmount = 0;

        _drugsCompareDic = new Dictionary<string, StomachDrugSlot>();

        WindowManager.Instance.OpenWindow(WinNames.DispenPanel);

        _BottomBar = DispenPanel.Instance._BottomBar as RectTransform;
        _BottomBar.anchoredPosition = new Vector2(_BottomBar.anchoredPosition.x, -95);
        DispenPanel.Instance.BottomBarAnimate(true);
        _BottomBar.SetActive(true);

        _RightBar = DispenPanel.Instance._RightBar as RectTransform;
        _RightBar.anchoredPosition = new Vector2(-554, _RightBar.anchoredPosition.y);
        DispenPanel.Instance.RightBarAnimate(true,()=> {
            PlayAudio(HospitalConsts.audio_ledi_peiyao);
        });
        _RightBar.SetActive(true);

        stomachDrugs = new List<StomachDrugSlot>();
        for (int i = 0; i < randomTimes; i++)
        {
            shapeIndex = UnityEngine.Random.Range(0, 4);

            StomachDrugSlot drug = new StomachDrugSlot((eStomachDrugShape)shapeIndex, (eStomachDrugColor)i);
            stomachDrugs.Add(drug);
        }

        CreateBottomBarItems();

        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnDragMatch);

    }

    public override void OnLeave()
    {
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnDragMatch);
    }

    public override void Dispose()
    {
        if(IsEnter) OnLeave();
    }

    public override void OnUpdate()
    {
       
    }


    public void CreateBottomBarItems()
    {
        //StomachDispen stomachDispen = this;
        List<StomachDrugSlot> stomachDrugs = this.stomachDrugs;
        for (int i = 0; i < stomachDrugs.Count; i++)
        {
            StomachDrugSlot _stomachDrug = stomachDrugs[i];

            GameObject item = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, HospitalConsts.buttombarLayoutItem);
            item.transform.SetParent(DispenPanel.Instance._BottomSubRoot);
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            Transform _child = item.transform.Find("SlotImage");
            item.transform.Find("ForeImage").SetActive(false);
            Image childImg = _child.GetComponent<Image>();
            childImg.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Hospital, _stomachDrug.m_spritename);

            //添加标识
            DragIdentify identy = HospitalHelper<DragIdentify>.GetSafeComponent(item);
            identy.ID = (int)_stomachDrug.m_stomachDrugShape + "_" + (int)_stomachDrug.m_stomachDrugColor;  //形状_颜色
            _stomachDrug.SetOwer(item.transform);

            HoverUI hoverUI = HospitalHelper<HoverUI>.GetSafeComponent(item);

            UIDrag3DBase drugDrag3D = null;
            if (ClinicRoomController.Instance.m_currentHospitalGame == eHospitalGame.game_Stomachache)
            {
                drugDrag3D = HospitalHelper<DraggableDrug3D>.GetSafeComponent(item);
            }
            else
            {
                drugDrag3D = HospitalHelper<DragDurg>.GetSafeComponent(item);
            }

            drugDrag3D._slot = _stomachDrug;
            drugDrag3D.enabled = false;

            _drugsCompareDic.Add(identy.ID, _stomachDrug);
        }

        //填充药片列表
    }

    //当有拖拽行为匹配上时
    private void OnDragMatch(DragIdentify identify)
    {
        if (!_drugsCompareDic.ContainsKey(identify.ID)) return;

        bool ismatch = DragComparerManager.Instance.IsMatch(identify,false);
        DragIdentify _current_dragIdentify = DragComparerManager.Instance._current_dragIdentify;
        if (_current_dragIdentify == null) return;
        if (ismatch && !_drugsCompareDic[identify.ID].m_is_have_fill)
        {

            //Debug.Log(_current_dragIdentify.ID);
            _drugsCompareDic[identify.ID].m_is_have_fill = true;
            string iD = _current_dragIdentify.ID;
            StomachDrugSlot _drugSlot = _drugsCompareDic[iD];
            StomachDrug _drug = new StomachDrug(_drugSlot.m_stomachDrugShape, _drugSlot.m_stomachDrugColor);
            Transform itemTransform = _drugSlot.m_myTransform;
            Transform _itemSlotImage = _drugSlot.m_myTransform.Find("SlotImage");
            Transform itemForeImage = itemTransform.Find("ForeImage");
            Image _itemforgeImage = itemForeImage.GetComponent<Image>();

            //重新生成药丸
            UIDragBase _dragbase = identify.GetComponent<UIDragBase>();
            _dragbase.SetGraphicsVisble(_dragbase.transform, true);
            _dragbase.transform.localScale = Vector3.zero;
            _dragbase.transform.DOScale(Vector3.one, .5f);

            //填充ui前景图
            itemTransform.GetComponent<HoverUI>().enabled = false;
            _itemforgeImage.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Hospital, _drug.m_spritename);
            itemForeImage.SetActive(true);

            //匹配成功的特效
            itemTransform.Find("502004073").SetActive(true);
            PlayAudio(HospitalConsts.audio_select_correct);

            _dipenSuccessAmount++;
            if (_dipenSuccessAmount >= 3)
            {
                Debug.Log("所有药片已经全部配好");
                DispenPanel.Instance.Effect_pipei.SetActive(true);

                DispenPanel.Instance.RightBarAnimate(false);

                if (_onDispenComplete != null) _onDispenComplete();
            }
        }
        else
        {
            Debug.Log("没有匹配上！");
        }
    }

}

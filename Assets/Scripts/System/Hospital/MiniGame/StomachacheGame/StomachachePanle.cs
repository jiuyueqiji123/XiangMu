﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;
using DG.Tweening;


public class StomachachePanle : SingletonBaseWindow<StomachachePanle> {

    #region ui property

    public Animator _Scaner { get; set; }
    public Animator _Scaner_Exterminate { get; set; }
    public RectTransform _ScanerPanel { get; set; }
    public RectTransform _DraggableImage { get; set; }
    public RectTransform _BottomBar { get; set; }
    public RectTransform _RightBar { get; set; }
    public Button _BtnBack { get; set; }
    public RectTransform _BottomSubRoot { get; set; }
    public RectTransform _RightSubRoot { get; set; }
    public HoverUI _MouseAnchorImg { get; set; }
    public HoverUI _StomachAnchorImg { get; set; }
    public Transform Effect_success { get; set; }

    #endregion

    protected override void AddListeners()
    {
        base.AddListeners();
        _BtnBack.onClick.AddListener(OnBackBtnClick);
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();
        _BtnBack.onClick.RemoveListener(OnBackBtnClick);
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        DragComparerManager.Instance._currentDragAgent = _DraggableImage;
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            StomachacheGameManager.Instance.ChangeState(StomachacheGameManager.eStamachGameState.Dispensing);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            StomachacheGameManager.Instance.ChangeState(StomachacheGameManager.eStamachGameState.Exterminate_bacteria);
        }

    }

    protected override void OnClose()
    {
        base.OnClose();
    }


    #region UIEvent

    public void ScanerAnimate(bool isIn,Action callback = null)
    {
        if (isIn)
        {
            _ScanerPanel.DOAnchorPosX(-851, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _ScanerPanel.DOAnchorPosX(-46, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    public void RightBarAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _RightBar.DOAnchorPosX(-806, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _RightBar.DOAnchorPosX(-554, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    public void BottomBarAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _BottomBar.DOAnchorPosY(112, 1f).OnComplete(()=> { if (callback != null) callback(); });
        }
        else
        {
            _BottomBar.DOAnchorPosY(-95, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
       
    }

    private void OnBackBtnClick()
    {
        WindowManager.Instance.CloseWindow(WinNames.StomachachePanel);
        WindowManager.Instance.CloseWindow(WinNames.DispenPanel);
        StomachacheGameManager.DestroyInstance();
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }

    #endregion
}

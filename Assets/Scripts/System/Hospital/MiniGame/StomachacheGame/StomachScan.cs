﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StomachScan : HospitalGameBase
{
    Transform _StomachAnchorImg;
    NoInputCheckHanderEx _scannerShake;
    float _tick_duration = 10;
    int _voiceTipCount;
    bool _isabsoubed;

    public override void OnEnter()
    {
        _voiceTipCount = 0;
        _scannerShake = new NoInputCheckHanderEx(_tick_duration,10000);
        _scannerShake.AddNoInputHander(OnScannerShake);
        _scannerShake.StartNoInputTimer();

        _StomachAnchorImg = StomachachePanle.Instance._StomachAnchorImg.transform;
        _StomachAnchorImg.SetActive(true);
        StomachachePanle.Instance.ScanerAnimate(true,()=> { PlayAudio(HospitalConsts.audio_ledi_saomiaoyi); });

        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAGGING, OnScannerDrag);
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnScannerBeginDrag);
    }

    public override void OnLeave()
    {
        StomachachePanle.Instance._StomachAnchorImg.gameObject.SetActive(false);

        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAGGING, OnScannerDrag);
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_BEGIN, OnScannerBeginDrag);
    }

    public override void Dispose()
    {
        if (IsEnter) OnLeave();
    }

    public override void OnUpdate()
    {
        _scannerShake.OnUpdate();
    }

    private void OnScannerShake()
    {
        PlayAudio(HospitalConsts.audio_huandong);
        StomachachePanle.Instance._Scaner.Play("Shake", 0, .1f);
        _voiceTipCount++;
        if (_voiceTipCount <= 2)
        {
            //Debug.Log("播放画外音： 打开扫描仪，找找病因");
            PlayAudio(HospitalConsts.audio_ledi_saomiaoyi);
        }

    }

    private void OnScannerBeginDrag(DragIdentify indentity)
    {
        _scannerShake.StopCheck();
    }

    private void OnScannerDrag(DragIdentify identity)
    {
        Debug.Log("aaa");
        bool ismatch = DragComparerManager.Instance.IsMatch(identity, false);
        if (ismatch)
        {
            Debug.Log("bbbb");
            if (identity.ID == "scanner")
            {
                Debug.Log("cccc");
                if (!_isabsoubed)
                {
                    Debug.Log("dddd");
                    _isabsoubed = true;

                    DraggableScaner _scaner = identity.GetComponent<DraggableScaner>();
                    _scaner._isAutoBack = false;
                    _scaner.enabled = false;

                    Debug.Log("切换到扫描仪特写");
                    _scaner.StartScanEffect();

                    Transform _scannerTrans = _scaner.transform;
                    _scannerTrans.SetTransformFormTarget(_StomachAnchorImg);
                    Debug.Log("播放特写动画");

                    TimerManager.Instance.AddTimer(2f, () => {
                        PlayAudio(HospitalConsts.audio_ledi_zangdongxi, () =>
                        {
                            Debug.Log("画面转场");
                            TransitionManager.Instance.StartTransition( () => {

                                _StomachAnchorImg.SetActive(false);
                                _scaner.transform.SetActive(false);
                                StomachachePanle.Instance._Scaner_Exterminate.gameObject.SetActive(true);

                                //_scaner._isAutoBack = true;
                                //_scaner.enabled = true;
                                //_scaner.SetRaycastTarget(_scannerTrans, true);
                                //_scaner.SetToDragStartPostion();
                                StomachachePanle.Instance._ScanerPanel.gameObject.SetActive(false);
                                StomachachePanle.Instance.ScanerAnimate(false);

                                StomachacheGameManager.Instance.ChangeState(StomachacheGameManager.eStamachGameState.Dispensing);

                            },null);
                        },true);
                    });

                }
                
            }
        }

    }

}

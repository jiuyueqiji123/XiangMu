﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bacteria : StomachExterminateItemBase
{

    public BacteriaInfo _bacteriaInfo;

    DrugFighter _fighter;
    bool _blockInput;

    // Use this for initialization
    void Start () {
        InvokeRepeating("OnLaughing", 0f, Random.Range(4f,15f));

        Sprites = GetSprites(_bacteriaInfo.m_spritename_idle);
        _spriteAniamtion.AutoPlayDelay = Random.Range(0f, 1f);
        _spriteAniamtion.Loop = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (_isReadyCombat)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (_blockInput) return;
                _blockInput = true;

                //击杀动画
                Sprites = GetSprites(_bacteriaInfo.m_spritename_kill);
                float fightTime = 6f / _spriteAniamtion.FPS;
                TimerManager.Instance.AddTimer(fightTime, () => {
                    _spriteAniamtion.Loop = false;
                    this._fighter._dragger._IsEnableDrag = true;
                    _blockInput = false;
                });

                Debug.Log("刀光-》惨叫-》胜利");
                HospitalUtil.PlayAudio(HospitalConsts.audio_yaowan_gongji, () => {
                    HospitalUtil.PlayAudio(HospitalConsts.audio_bingdu_canjiao,()=> {
                        HospitalUtil.PlayAudio(HospitalConsts.audio_yaowan_victory);
                    });
                });

                this._fighter.OnFighter();
                Invoke("OnKill", fightTime);
            }
        }
    }

    public void OnColliderDrug(DrugFighter drugfighter)
    {
       this._fighter = drugfighter;
        _isReadyCombat = true;
    }

    private void OnLaughing()
    {
        switch (_bacteriaInfo.m_bacteriaColor)
        {
            case eStomachDrugColor.red:
                StomachacheGameManager.Instance.stomachExterminate.PlayAudioSingleton(HospitalConsts.audio_virus_hong_laugh);
                break;
            case eStomachDrugColor.yellow:
                StomachacheGameManager.Instance.stomachExterminate.PlayAudioSingleton(HospitalConsts.audio_virus_huang_laugh);
                break;
            case eStomachDrugColor.blue:
                StomachacheGameManager.Instance.stomachExterminate.PlayAudioSingleton(HospitalConsts.audio_virus_lan_laugh);
                break;
            default:
                break;
        }
    }

    public void OnKill()
    {
        CancelInvoke();
        StomachacheGameManager.Instance.stomachExterminate._currentBacteria.Remove(this);

        //_holder._cleanEffect.SetActive(true);
        //_holder._cleanEffect.position = transform.position;
        Destroy(gameObject);
        //TimerManager.Instance.AddTimer(2f, () => {
        //    Destroy(gameObject);
        //    TimerManager.Instance.AddTimer(.5f, () => { _holder._cleanEffect.SetActive(false); });
        //    Debug.Log("kill");
        //});
    }
}

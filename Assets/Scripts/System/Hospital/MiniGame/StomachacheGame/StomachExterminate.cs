﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StomachExterminateItemBase : MonoBehaviour
{
    public eStomachDrugColor _color;

    protected bool _isReadyCombat = false;
    protected bool _isCombatOver = false;
    protected SpriteAnimation _spriteAniamtion;
    protected BacteriaGameRootHolder _holder;

    private void Awake()
    {
        _spriteAniamtion = GetComponentInChildren<SpriteAnimation>();
        _holder = StomachacheGameManager.Instance.stomachExterminate._holder;
    }

    public virtual List<Sprite> Sprites
    {
        set {
            _spriteAniamtion.SpriteFrames = value;
        }
    }

    public List<Sprite> GetSprites(List<string> spriteNames)
    {
        List<Sprite> sprites = new List<Sprite>();
        for (int i = 0; i < spriteNames.Count; i++)
            sprites.Add(UISpriteManager.Instance.GetSprite(emUIAltas.Hospital, spriteNames[i]));
        return sprites;
    }
}

public class BacteriaInfo
{
    public eBacteriaType m_bacteriaType;
    public eStomachDrugColor m_bacteriaColor;
    public List<string> m_spritename_idle;
    public List<string> m_spritename_kill;

    public BacteriaInfo(eBacteriaType type, eStomachDrugColor color)
    {
        m_spritename_idle = new List<string>();
        m_spritename_kill = new List<string>();

        this.m_bacteriaType = type;
        this.m_bacteriaColor = color;

        string[] m_idleSpriteNames = new string[2];
        string[] m_killSprteNames = new string[2];

        string basePrefx = "";
        switch (m_bacteriaType)
        {
            case eBacteriaType.type1:
                basePrefx = "dh1_";
                break;
            case eBacteriaType.type2:
                basePrefx = "dh2_";
                break;
            case eBacteriaType.type3:
                basePrefx = "dh3_";
                break;
            default:
                break;
        }

        switch (m_bacteriaColor)
        {
            case eStomachDrugColor.red:
                m_killSprteNames[0] = basePrefx + "hongsexijun_01";
                m_killSprteNames[1] = basePrefx + "hongsexijun_02";
                m_idleSpriteNames[0] = "dj" + basePrefx + "hongsexijun_01";
                m_idleSpriteNames[1] = "dj" + basePrefx + "hongsexijun_02";
                break;
            case eStomachDrugColor.yellow:
                m_killSprteNames[0] = basePrefx + "huangsexijun_01";
                m_killSprteNames[1] = basePrefx + "huangsexijun_02";
                m_idleSpriteNames[0] = "dj" + basePrefx + "huangsexijun_01";
                m_idleSpriteNames[1] = "dj" + basePrefx + "huangsexijun_02";
                break;
            case eStomachDrugColor.blue:
                m_killSprteNames[0] = basePrefx + "lansexijun_01";
                m_killSprteNames[1] = basePrefx + "lansexijun_02";
                m_idleSpriteNames[0] = "dj" + basePrefx + "lansexijun_01";
                m_idleSpriteNames[1] = "dj" + basePrefx + "lansexijun_02";
                break;
            default:
                break;
        }

        m_spritename_idle.Add(m_idleSpriteNames[0]);
        m_spritename_idle.Add(m_idleSpriteNames[1]);
        m_spritename_kill.Add(m_killSprteNames[0]);
        m_spritename_kill.Add(m_killSprteNames[1]);
        Debug.Log(m_killSprteNames[0]);
        Debug.Log(m_killSprteNames[1]);
        Debug.Log(m_idleSpriteNames[0]);
        Debug.Log(m_idleSpriteNames[1]);
    }
}

public class StomachDrugFighter
{
    public eStomachDrugShape m_stomachDrugShape;
    public eStomachDrugColor m_stomachDrugColor;
    public List<string> m_spritename_idle;
    public List<string> m_spritename_kill;

    public StomachDrugFighter(eStomachDrugShape shape, eStomachDrugColor color)
    {
        this.m_stomachDrugShape = shape;
        this.m_stomachDrugColor = color;

        m_spritename_idle = new List<string>();
        m_spritename_kill = new List<string>();

        string[] m_idleSpriteNames = new string[2];
        string[] m_killSprteNames = new string[2];

        string basePrefx = "";
        switch (shape)
        {
            case eStomachDrugShape.triangle:
                basePrefx = "dh_sjx";
                break;
            case eStomachDrugShape.ellipse:
                basePrefx = "dh_ty";
                break;
            case eStomachDrugShape.circle:
                basePrefx = "dh_yx";
                break;
            case eStomachDrugShape.square:
                basePrefx = "dh_fx";
                break;
            default:
                break;
        }

        switch (color)
        {
            case eStomachDrugColor.red:
                m_killSprteNames[0] = basePrefx + "hongs_yaowan01";
                m_killSprteNames[1] = basePrefx + "hongs_yaowan02";
                m_idleSpriteNames[0] = "dj" + basePrefx + "hongs_yaowan01";
                m_idleSpriteNames[1] = "dj" + basePrefx + "hongs_yaowan02";
                break;
            case eStomachDrugColor.yellow:
                m_killSprteNames[0] = basePrefx + "huangs_yaowan01";
                m_killSprteNames[1] = basePrefx + "huangs_yaowan02";
                m_idleSpriteNames[0] = "dj" + basePrefx + "huangse_yaowan01";
                m_idleSpriteNames[1] = "dj" + basePrefx + "huangse_yaowan02";
                break;
            case eStomachDrugColor.blue:
                m_killSprteNames[0] = basePrefx + "lans_yaowan01";
                m_killSprteNames[1] = basePrefx + "lans_yaowan02";
                m_idleSpriteNames[0] = "dj" + basePrefx + "lanse_yaowan01";
                m_idleSpriteNames[1] = "dj" + basePrefx + "lanse_yaowan02";
                break;
            default:
                break;
        }

        m_spritename_idle.Add(m_idleSpriteNames[0]);
        m_spritename_idle.Add(m_idleSpriteNames[1]);
        m_spritename_kill.Add(m_killSprteNames[0]);
        m_spritename_kill.Add(m_killSprteNames[1]);
        Debug.Log(m_killSprteNames[0]);
        Debug.Log(m_killSprteNames[1]);
        Debug.Log(m_idleSpriteNames[0]);
        Debug.Log(m_idleSpriteNames[1]);
    }
}

/// <summary>
/// 消灭细菌
/// </summary>
public class StomachExterminate : HospitalGameBase
{
    public BacteriaGameRootHolder _holder;
    NoInputCheckHanderEx _scannerShake;
    float _tick_duration = 15;

    public List<StomachDrugSlot> _stomachDrugs;
    public List<DrugFighter> _currentDrugFighter;
    public List<Bacteria>   _currentBacteria;

    bool _isGameComplete;

    public int GetCurrentBacteriaAmount(eStomachDrugColor drugColor)
    {
        int sum = 0;
        foreach (var bacteria in _currentBacteria)
        {
            if (bacteria._color == drugColor)
                sum++;
        }

        return sum;
    }

    public override void OnEnter()
    {
        HospitalResManager.Instance.root.SetActive(false);
        WindowManager.Instance.CloseWindow(WinNames.DispenPanel);

        _currentDrugFighter = new List<DrugFighter>();
        _currentBacteria = new List<Bacteria>();
        _stomachDrugs = StomachacheGameManager.Instance.stomachDispen.stomachDrugs;
        _holder = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, HospitalConsts.bacteriaGameRoot).GetComponent<BacteriaGameRootHolder>();

        //定时播放提示
        _scannerShake = new NoInputCheckHanderEx(_tick_duration, 10000);
        _scannerShake.AddNoInputHander(PlayRandomTip);
        _scannerShake.StartNoInputTimer(5f,true);


        //生成细菌
        List<int> _indexList = HospitalUtil.RandomUnSame(5, 0, 16);
        foreach (int inex in _indexList)
        {
            eBacteriaType type = (eBacteriaType) Random.Range(0, 3);
            eStomachDrugColor color = (eStomachDrugColor)Random.Range(0, 3);

            BacteriaInfo _backteriainfo = new BacteriaInfo(type, color);
            GameObject _enemyObject = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, HospitalConsts.enemy);
            Bacteria _bacteria = HospitalHelper<Bacteria>.GetSafeComponent(_enemyObject);
            _bacteria._color = color;
            _bacteria._bacteriaInfo = _backteriainfo;
            //_bacteria.Sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Hospital, _backteriainfo.m_spritename_idle);  //设置正常状态下的图片

            Transform _bactariaAnchor = _holder._bacteriaSpawnPoints[inex];
            _enemyObject.transform.SetParent(_bactariaAnchor);
            _enemyObject.transform.SetTransformFormTarget(_bactariaAnchor);

            _currentBacteria.Add(_bacteria);

        }

        //生成药片
        int fighterIndex = 0;
        foreach (var drugslot in _stomachDrugs)
        {
            bool iscontain = false;
            foreach (var enemy in _currentBacteria)
            {
                if (enemy._bacteriaInfo.m_bacteriaColor == drugslot.m_stomachDrugColor)
                    iscontain = true;
            }

            if (!iscontain) continue;

            StomachDrugFighter _fighter = new StomachDrugFighter(drugslot.m_stomachDrugShape, drugslot.m_stomachDrugColor);

            GameObject _drugObject = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, HospitalConsts.drug);

            SpriteDragBase _drag = HospitalHelper<SpriteDragBase>.GetSafeComponent(_drugObject);
            _drag._camera = _holder._Camera;
            _drag._onBeginDrag += OnDrugBeginDrag;
            _drag._onEndDrag += OnDrugEndDrag;

            DrugFighter _drugfighter = HospitalHelper<DrugFighter>.GetSafeComponent(_drugObject);
            _drugfighter._color = _fighter.m_stomachDrugColor;
            _drugfighter._drugFighterInfo = _fighter;
            //_drugfighter.Sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Hospital, _fighter.m_spritename_idle);
            Transform _fighterAnchor = _holder._drugsSpawnPoints[fighterIndex];
            _drugObject.transform.SetParent(_fighterAnchor);
            _drugObject.transform.SetTransformFormTarget(_fighterAnchor);

            _currentDrugFighter.Add(_drugfighter);

            fighterIndex++;
        }
    }

    public override void OnLeave()
    {
        GameObject.Destroy(_holder.gameObject);
    }

    public override void Dispose()
    {
        if (IsEnter) OnLeave();
    }

    public override void OnUpdate()
    {
        if (Input.GetKeyDown(KeyCode.X))
            _currentBacteria.Clear();

        _scannerShake.OnUpdate();
        if (_currentBacteria.Count == 0)
        {
            Debug.Log("exterminate 过关");
            if (!_isGameComplete)
            {
                _isGameComplete = true;

                StomachachePanle.Instance.Effect_success.SetActive(true);
                PlayAudio(HospitalConsts.audio_select_correct);
                TimerManager.Instance.AddTimer(3f, () => {
                    TransitionManager.Instance.StartTransition(() => {
                        StomachacheGameManager.Instance.ChangeState(StomachacheGameManager.eStamachGameState.Leave);
                    }, null);
                });
            }
        }
    }


    private void OnDrugBeginDrag(SpriteDragBase drag)
    {
        foreach (DrugFighter fighter in _currentDrugFighter)
        {
            if(fighter._dragger != drag)
                fighter.GetComponentInChildren<Collider>().enabled = false;
        }
    }


    private void OnDrugEndDrag(SpriteDragBase drag)
    {
        foreach (DrugFighter fighter in _currentDrugFighter)
        {
            if (fighter._dragger != drag)
                fighter.GetComponentInChildren<Collider>().enabled = true;
        }
    }


    private void PlayRandomTip()
    {
        int index = Random.Range(0, 3);
        switch (index)
        {
            case 0: PlayAudioSingleton(HospitalConsts.audio_ledi_lanse_guancha); break;
            case 1: PlayAudioSingleton(HospitalConsts.audio_ledi_huangse_guancha); break;
            case 2: PlayAudioSingleton(HospitalConsts.audio_ledi_hongse_guancha); break;
            default:
                break;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StomachacheGameManager : Singleton<StomachacheGameManager> {


    public enum eStamachGameState {

        None,
        Scan,   //扫描
        Dispensing, //配药
        VoiceExcitation,//语音激励
        Exterminate_bacteria,//消灭细菌
        Leave   //成功告别

    }

    public eStamachGameState state;

    HospitalGameBase _current_stateBase;
    public StomachScan stomachScan;
    public StomachDispen stomachDispen;
    public StomachExterminate stomachExterminate;
    public StomachVoiceExcitation stomachVoiceExcitation;
    public StomachLeave stomachLeave;

    public AnimatorEx _nuowa_animatorex;
    public AnimatorEx m_ledi_animatorEx;

    public Camera _cam;

    public override void Init()
    {
        base.Init();

        state = eStamachGameState.None;
        stomachScan = new StomachScan();
        stomachDispen = new StomachDispen(); stomachDispen._onDispenComplete = () => {
            HospitalUtil.PlayAudio(HospitalConsts.audio_nuowa_chiyao, () => {
                TimerManager.Instance.AddTimer(1f, () => { StomachacheGameManager.Instance.ChangeState(eStamachGameState.VoiceExcitation); });
            });
        };
         stomachExterminate = new StomachExterminate();
        stomachVoiceExcitation = new StomachVoiceExcitation();
        stomachLeave = new StomachLeave();

        _nuowa_animatorex = new AnimatorEx(HospitalResManager.Instance.m_nuowaRoot.gameObject);
        m_ledi_animatorEx = new AnimatorEx(HospitalResManager.Instance.m_lediRoot.gameObject);

        _cam = HospitalResManager.Instance.Game_StomachacheRoot.GetComponentInChildren<Camera>();
    }

    public void GameStart()
    {
        HospitalResManager.Instance.Game_StomachacheRoot.SetActive(true);

        WindowManager.Instance.CloseWindow(WinNames.ClinicRoomPanel);
        WindowManager.Instance.OpenWindow(WinNames.StomachachePanel);
        ChangeState(eStamachGameState.Scan);
    }

    public void ChangeState(eStamachGameState newstate)
    {
        if (_current_stateBase != null && newstate != state)
            _current_stateBase.OnLeave();

        switch (newstate)
        {
            case eStamachGameState.None:

                break;
            case eStamachGameState.Scan:
                _current_stateBase = stomachScan;
                break;
            case eStamachGameState.Dispensing:
                _current_stateBase = stomachDispen;
                break;
            case eStamachGameState.VoiceExcitation:
                _current_stateBase = stomachVoiceExcitation;
                break;
            case eStamachGameState.Exterminate_bacteria:
                _current_stateBase = stomachExterminate;
                break;
            case eStamachGameState.Leave:
                _current_stateBase = stomachLeave;
                break;
            default:
                break;
        }

        state = newstate;
        _current_stateBase.OnEnter();
    }

    public void OnUpdate()
    {
        if(_current_stateBase != null)
            _current_stateBase.OnUpdate();
        _nuowa_animatorex.OnUpdate();
        m_ledi_animatorEx.OnUpdate();
    }

    public override void UnInit()
    {
        base.UnInit();

        stomachScan.Dispose();
        stomachDispen.Dispose();
        stomachExterminate.Dispose();
        stomachVoiceExcitation.Dispose();
        stomachLeave.Dispose();
    }

}

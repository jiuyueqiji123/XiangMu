﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StomachLeave : HospitalGameBase
{
    public override void OnEnter()
    {
        HospitalResManager.Instance.Game_StomachacheRoot.SetActive(false);
        WindowManager.Instance.CloseWindow(WinNames.StomachachePanel);
        HospitalEventHandler.Instance.BroadCastEvent(HospitalEventID.SMALLGAME_OVER);
    }

    public override void OnLeave()
    {
        
    }

    public override void OnUpdate()
    {
       
    }

    public override void Dispose()
    {
       
    }
}

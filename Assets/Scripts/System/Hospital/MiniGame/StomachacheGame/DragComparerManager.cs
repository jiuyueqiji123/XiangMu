﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragComparerManager : Singleton<DragComparerManager> {

    private DragIdentify _last_dragIdentify;
    public DragIdentify _current_dragIdentify;

    public RectTransform _currentDragAgent; //当前拖拽的代理
    public Transform _currentDrag3dObject;  //当前拖拽的3d物体

    public bool IsMatch(DragIdentify identify,bool use_prefix = false)
    {
        if (_current_dragIdentify == null || identify == null) return false;
        string current_id = _current_dragIdentify.ID;
        string target_id = identify.ID;
        //Debug.Log("current_id: " + current_id + " target_id: " + target_id);

        //匹配成功后要将当前全局的indetify置空，放置重复成功匹配的情况
        _last_dragIdentify = _current_dragIdentify;

        return current_id == target_id;
    }



}

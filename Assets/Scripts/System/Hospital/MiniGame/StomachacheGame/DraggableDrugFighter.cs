﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DraggableDrugFighter : SpriteDragBase,IPointerDownHandler {

    Transform _clickEffect;

    private void Start()
    {
        _clickEffect = transform.Find("502004094");
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _clickEffect.SetActive(false);
        _clickEffect.SetActive(true);
    }
}

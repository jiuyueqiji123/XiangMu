﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;


public class DispenPanel : SingletonBaseWindow<DispenPanel> {

    const string anim_shake = "Shake";

    #region ui property

    public RectTransform _DraggableImage { get; set; }
    public RectTransform _BottomBar { get; set; }
    public RectTransform _RightBar { get; set; }
    public Button _BtnBack { get; set; }
    public RectTransform _BottomSubRoot { get; set; }
    public RectTransform _RightSubRoot { get; set; }
    public Transform Effect_pipei { get; set; }

    #endregion

    protected override void AddListeners()
    {
        base.AddListeners();
        _BtnBack.onClick.AddListener(OnBackBtnClick);
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();
        _BtnBack.onClick.RemoveListener(OnBackBtnClick);
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        DragComparerManager.Instance._currentDragAgent = _DraggableImage;
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();

        StomachacheGameManager.Instance.OnUpdate();
    }

    protected override void OnClose()
    {
        base.OnClose();
    }


    #region UIEvent

    public void RightBarAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _RightBar.DOAnchorPosX(-806, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
        else
        {
            _RightBar.DOAnchorPosX(-554, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
    }

    public void BottomBarAnimate(bool isIn, Action callback = null)
    {
        if (isIn)
        {
            _BottomBar.DOAnchorPosY(112, 1f).OnComplete(()=> { if (callback != null) callback(); });
        }
        else
        {
            _BottomBar.DOAnchorPosY(-95, 1f).OnComplete(() => { if (callback != null) callback(); }); ;
        }
       
    }

    private void OnBackBtnClick()
    {
        WindowManager.Instance.CloseWindow(WinNames.StomachachePanel);
        StomachacheGameManager.DestroyInstance();
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }

    #endregion
}

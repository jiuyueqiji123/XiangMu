﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HospitalGameBase {

    public bool IsFinish { get; set; }
    public bool IsEnter { get; set; }

    int timerID = -1;
    GameAudioSource _audiosource;

    public virtual void OnEnter() { IsEnter = true; }
    public abstract void OnUpdate();
    public abstract void OnLeave();
    public virtual void Dispose() { }

    public void PlayAudio(string audioname, System.Action callback = null, bool stopOtherSound = false,float delay = 0f)
    {
        if(stopOtherSound)
            AudioManager.Instance.StopAllSound();
        string path = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, audioname);
        TimerManager.Instance.AddTimer(delay, () => {
            _audiosource = AudioManager.Instance.PlaySound(path);
            if (callback != null)
            {
                TimerManager.Instance.RemoveTimerSafely(ref timerID);
                timerID = TimerManager.Instance.AddTimer(_audiosource.Length, callback);
            }
        });
    }

    public void PlayAudioSingleton(string audioname, System.Action callback = null, float delay = 0f)
    {
        string path = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, audioname);
        TimerManager.Instance.AddTimer(delay, () => {

            if (_audiosource == null || !_audiosource.IsPlaying)
            {
                _audiosource = AudioManager.Instance.PlaySound(path);
                if (callback != null)
                {
                    TimerManager.Instance.RemoveTimerSafely(ref timerID);
                    timerID = TimerManager.Instance.AddTimer(_audiosource.Length, callback);
                }
            }
        });
    }

    public GameAudioSource PlayAudio(string audioname, bool loop)
    {
        string path = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, audioname);
        return AudioManager.Instance.PlaySound(path, loop,true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerticalLayoutExtension : MonoBehaviour {

    public int cellHeight = 150;
    public int spacing;
    public int paddingTop;

    float length;
    RectTransform _rt;
    int _activedChildCount;

    // Use this for initialization
    void Start () {
        RefreshUI();
    }

    public void RefreshUI()
    {
        _rt = GetComponent<RectTransform>();

        var allchilds = _rt.GetComponentsInChildren<DragIdentify>(true);
        _activedChildCount = allchilds.Length;

        for (var i = allchilds.Length - 1; i >= 0; i--)
        {
            var child = allchilds[i];

            if (child.gameObject.activeInHierarchy == false)
            {
                Destroy(child.gameObject);
                _activedChildCount -= 1;
            }
        }

        _rt.sizeDelta = new Vector2(_rt.sizeDelta.x, paddingTop + (cellHeight + spacing) * (_activedChildCount - 1));

    }

}

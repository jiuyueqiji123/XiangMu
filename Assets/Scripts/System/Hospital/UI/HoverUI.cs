﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

/// <summary>
/// 被匹配的物体（当鼠标滑入时，会触发一个滑入事件。在脚本中通过监听这个事件来进行相关事件）
/// </summary>
[RequireComponent(typeof(DragIdentify))]
public class HoverUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    DragIdentify _dragIdentify;
    bool _pointdown;

    private void Awake()
    {
        _dragIdentify = GetComponentInChildren<DragIdentify>();
        _pointdown = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("poin enger");
        //在鼠标没有拖拽行为时，不设置全局的dragidenty
        if (Input.GetMouseButton(0))
        {
            Debug.Log("hover in" + name + " id: " + _dragIdentify.ID);
            DragComparerManager.Instance._current_dragIdentify = _dragIdentify;
            HospitalEventHandler.Instance.BroadCastEvent<DragIdentify>(HospitalEventID.ON_HOVER_UI, this._dragIdentify);
            _pointdown = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("poin exit");
        DragComparerManager.Instance._current_dragIdentify = null;
        _pointdown = false;
    }

    private void Update()
    {
        if (_pointdown)
        {
            HospitalEventHandler.Instance.BroadCastEvent<DragIdentify>(HospitalEventID.ON_POINTDOWN_UI,this._dragIdentify);
        }
    }
}

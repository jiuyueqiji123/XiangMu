﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollRectExtension : MonoBehaviour {

    public enum eArrayDirection { horizontal,vertical }    //滚动的方向

    GridLayoutGroup gridLayoutGroup;

    public eArrayDirection arrayDirction;

    float spacing;
    float length;
    int childCount;

	// Use this for initialization
	void Start () {
        gridLayoutGroup = transform.GetComponentInChildren<GridLayoutGroup>();
        RefreshUI();
    }

    public void RefreshUI()
    {
        childCount = transform.childCount;
        RectTransform rectTransform = gridLayoutGroup.GetComponent<RectTransform>();

        if (arrayDirction == eArrayDirection.horizontal)
        {
            spacing = gridLayoutGroup.spacing.x;
            length = gridLayoutGroup.padding.left + (gridLayoutGroup.cellSize.x + spacing) * childCount;
            rectTransform.sizeDelta = new Vector2(length, rectTransform.sizeDelta.y);
        }
        else
        {
            spacing = gridLayoutGroup.spacing.y;
            length = gridLayoutGroup.padding.top + (gridLayoutGroup.cellSize.y + spacing) * childCount;
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x,length);
        }
    }

}

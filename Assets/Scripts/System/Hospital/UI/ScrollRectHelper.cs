﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollRectHelper : MonoBehaviour {

    public float _scrollSensitity;
    public Vector2 _scrooldelta;
    ScrollRect _scrollrect;

    private void Awake()
    {
        _scrollrect = GetComponent<ScrollRect>();
    }

    public void Update()
    {
        _scrollrect.verticalNormalizedPosition += _scrooldelta.y * .001f * _scrollSensitity;
        _scrooldelta = Vector2.Lerp(_scrooldelta, Vector2.zero, .1f);
    }

}

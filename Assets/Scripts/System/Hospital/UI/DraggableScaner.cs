﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// 扫描仪拖拽
/// </summary>
public class DraggableScaner : UIDragBase {

    public RectTransform _bacteriaMask;
    Transform _ScanEffect;
    bool _isabsoubed;
    GameAudioSource _audio_scanning;

    private void Start()
    {
        _ScanEffect = transform.Find("ScanEffect");
        _ScanEffect.SetActive(false);
    }

    public void StartScanEffect()
    {
        HospitalUtil.PlayAudio(HospitalConsts.audio_saomiao_sound);
        //开启动画
        UGUISpriteAnimation[] anims = _ScanEffect.GetComponentsInChildren<UGUISpriteAnimation>();
        for (int i = 0; i < anims.Length; i++)
        {
            anims[i].AutoPlay = true;
            anims[i].AutoPlayDelay = Random.Range(0f, 1f);
        }
        _ScanEffect.SetActive(true);
        TimerManager.Instance.AddTimer(.5f, StartMaskAnim);
        Invoke("StopScanEffect", 4f);
    }

    public void StopScanEffect()
    {
        _ScanEffect.Find("502004093").SetActive(false);
    }

    private void StartMaskAnim()
    {
        CanvasGroup canvasGroup = _bacteriaMask.GetComponent<CanvasGroup>();
        canvasGroup.alpha = 1;
        _bacteriaMask.sizeDelta = new Vector2(0, 344.3f);
        _bacteriaMask.DOSizeDelta(new Vector2(485.6f, 344.3f),1.3f).SetEase(Ease.Linear).OnComplete(()=> {
            //canvasGroup.DOFade(0, .5f);
        });
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        HospitalUtil.PlayAudio(HospitalConsts.audio_saomiao_work);
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
    }

}

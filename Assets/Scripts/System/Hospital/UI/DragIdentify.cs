﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 用于标识拖拽的物体与被匹配的物体
/// </summary>
public class DragIdentify : MonoBehaviour {

    public string ID;
	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;

/// <summary>
/// 从ui拖出3d的物体
/// </summary>
public class DraggableDrug3D : UIDrag3DBase {

    Camera _cam;

    public override void InitComponet()
    {
        base.InitComponet();
        _cam = StomachacheGameManager.Instance._cam;
    }

    public override Vector3 Get3DPosition()
    {
        Plane plane = new Plane(_cam.transform.forward * -1, new Vector3(0.702f,1f,-0.96f));
        Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public override Transform _currentDragObject
    {
        get
        {
            Transform transform1 = GetDrugPill(_slot.m_stomachDrugShape, _slot.m_stomachDrugColor).transform;
            transform1.LookAt(_cam.transform);
            return transform1;
        }

        set
        {
            base._currentDragObject = value;
        }
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        //SetGraphicsVisble(_rt, false);
    }

    public override void SetGraphicsVisble(Transform target, bool isshow)
    {
        //base.SetGraphicsVisble(target, isshow);
        transform.Find("ForeImage").SetActive(isshow);
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        //SetGraphicsVisble(_rt, false);
    }

}

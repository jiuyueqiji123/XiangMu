﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

/// <summary>
/// 3d物体拖拽
/// </summary>
[RequireComponent(typeof(DragIdentify))]
public abstract class Object3dDragBase : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    public bool _isAutoBack = true;
    public bool _is3dObjectShowAfterBack = true;
    public float _tweenduration = 0.5f;
    public bool _isPlayBackAudio = true;

    protected bool _isOnTweening;
    protected DragIdentify _identyself;
    Vector3 _dragStartPosition;
    Quaternion _dragStartRotation;

    protected Vector3 _targetDragObjectPosition;
    protected Quaternion _targetDragObjectRotation;
    protected bool _onDragging;     //是否输入拖拽状态

    protected string _loopAudioName;
    bool _is_loopaudio_start;
    GameAudioSource _loopAudio;


    protected virtual void Awake()
    {
        _identyself = GetComponentInChildren<DragIdentify>();
        ResetStartDragPosition();
        InitComponet();
    }

    protected virtual void Start()
    { }

    public virtual void InitComponet() { }

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        _onDragging = true;
        //每次拖拽开始，将DragComparerManager的上次匹配的目标清空
        DragComparerManager.Instance._current_dragIdentify = null;
        GetComponentInChildren<Collider>().enabled = false;
        //触发一次拖拽开始的事件
        HospitalEventHandler.Instance.BroadCastEvent<DragIdentify>(HospitalEventID.DRAG_BEGIN, _identyself);

        DragComparerManager.Instance._currentDrag3dObject = transform;
        transform.position = Get3DPosition();

    }

    protected void Update()
    {
        if (_onDragging)
        {
            transform.position = Vector3.Lerp(transform.position, _targetDragObjectPosition, .5f);
            transform.rotation = Quaternion.Lerp(transform.rotation, _targetDragObjectRotation, .5f);
        }

        if (Input.GetKeyDown(KeyCode.X))
            OnShake();
    }

    //设置被拖拽物体的世界坐标位置
    public virtual Vector3 Get3DPosition()
    {
        Plane plane = new Plane(Camera.main.transform.forward * -1, Vector3.zero);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        _targetDragObjectPosition = Get3DPosition();
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        _onDragging = false;
        GetComponentInChildren<Collider>().enabled = true;
        //拖拽结束后，触发一个匹配事件
        HospitalEventHandler.Instance.BroadCastEvent<DragIdentify>(HospitalEventID.DRAG_MATCH, _identyself);

        if (_isAutoBack)
        {
            if (_isPlayBackAudio) HospitalUtil.PlayAudio(HospitalConsts.audio_zidonghuigui);
            _isOnTweening = true;
            transform.DORotate(_dragStartRotation.eulerAngles, _tweenduration);
            transform.DOMove(_dragStartPosition, _tweenduration).OnComplete(() =>
            {
                _isOnTweening = false;

                transform.SetActive(_is3dObjectShowAfterBack);
            });
        }
        //else
        //{
        //    transform.SetActive(false);
        //}


    }

    public void SetToDragStartPostion()
    {
        transform.position = _dragStartPosition;
    }

    public void ResetStartDragPosition()
    {
        _dragStartPosition = transform.position;
        _dragStartRotation = transform.rotation;
    }

    //控制一个循环音效
    protected void ActiveLoopAudio(bool isEnable)
    {
        if (isEnable)
        {
            if (!_is_loopaudio_start)
            {
                _is_loopaudio_start = true;
                _loopAudio = HospitalUtil.PlayAudio(_loopAudioName, true);
            }
        }
        else
        {
            if (_is_loopaudio_start)
            {
                _is_loopaudio_start = false;
                if (_loopAudio != null) _loopAudio.Stop();
            }
        }
       
    }

    protected virtual void OnShake()
    {
        
    }

    private void OnDisable()
    {
        ActiveLoopAudio(false);
    }

    protected virtual void OnDestroy()
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalResManager : Singleton<HospitalResManager>
{
    public Transform Acotors;
    public Transform root;
    public Transform Game_MainRoot;
    public Transform MainCamera;
    public Transform Game_StomachacheRoot;
    public Transform Game_ToochacheRoot;
    public Transform Game_VaccineRoot;
    public Transform Game_BruiseRoot;
    public Transform Game_LowFeverRoot;
    public Transform Game_HighFeverRoot;

    [HideInInspector]
    public Transform m_lediRoot;

    [HideInInspector]
    private Transform aishaRoot;
    GameObject m_aisha_root_clone;
    public GameObject m_aishaRoot
    {
        get
        {
            if (m_aisha_root_clone == null)
            {
                m_aisha_root_clone = GameObject.Instantiate<GameObject>(aishaRoot.gameObject);
                m_aisha_root_clone.transform.SetParent(Acotors);
            }
            return m_aisha_root_clone;
        }
    }

    [HideInInspector]
    public Transform m_ajiasiRoot;

    [HideInInspector]
    public Transform m_anchorPointsRoot;

    [HideInInspector]
    public Transform m_nuowaRoot;

    [HideInInspector]
    private Transform m_nuowa_toothache_root;

    GameObject m_nuowa_toothache_root_clone;
    public GameObject nuowa_toothache_root
    {
        get {
            if (m_nuowa_toothache_root_clone == null)
            {
                m_nuowa_toothache_root_clone = GameObject.Instantiate<GameObject>(m_nuowa_toothache_root.gameObject);
                m_nuowa_toothache_root_clone.transform.SetParent(Acotors);
            }
            return m_nuowa_toothache_root_clone;
        }
    }

    [HideInInspector]
    public Transform m_yizi;

    [HideInInspector]
    public Transform m_door;

    public Transform GetAnchorPoint(string name)
    {
        return m_anchorPointsRoot.Find(name);
    }

    public override void Init()
    {
        base.Init();

        UISpriteManager.Instance.LoadAtlas(emUIAltas.Hospital);

        root = GameObject.Find("Root").transform;

        Game_MainRoot = root.Find("Game_Main");
        Acotors = root.Find("Actors");
        MainCamera = Game_MainRoot.Find("Camera");
        Game_StomachacheRoot = root.Find("Game_Stomachache");
        Game_ToochacheRoot = root.Find("Game_Toochache");
        Game_VaccineRoot = root.Find("Game_Vaccine");
        Game_BruiseRoot = root.Find("Game_Bruise");
        Game_LowFeverRoot = root.Find("Game_LowFever");
        Game_HighFeverRoot = root.Find("Game_HighFever");

        m_door = Game_MainRoot.Find("Door");
        m_yizi = Game_MainRoot.Find("Stool/304060101_yizi");
        m_lediRoot = Acotors.Find("LeDi_Root");
        aishaRoot = Acotors.Find("AiSha_Root");
        m_nuowaRoot = Acotors.Find("Nuowa_Root");
        m_anchorPointsRoot = Acotors.Find("AnchorPoints");
        m_nuowa_toothache_root = Acotors.Find("Nuowa_Toothache_Root");
    }

    public static GameObject GetActiveObject(string path, string name)
    {
        GameObject temp = Res.LoadObj<GameObject>(FileTools.CombinePath(path, name));
        GameObject target = MonoInstane.Instantiate<GameObject>(temp);
        return target;
    }

    public override void UnInit()
    {
        base.UnInit();
    }

    public Sprite GetSprite(string name)
    {
        return UISpriteManager.Instance.GetSprite(emUIAltas.Game_Hospital,name);
    }
}

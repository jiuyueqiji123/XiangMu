﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClinicRoomWindow : SingletonBaseWindow<ClinicRoomWindow> {

    ClinicRoomController _clinicRoomController;

    #region ui property

    public Button _ButtonMiniGame { get; set; }

    public Button _BtnBack { get; set; }

    public Button _BtnStomacache { get; set; }

    public Button _BtnVaccine { get; set; }

    public Button _BtnHighFever { get; set; }

    public Button _BtnLowFever { get; set; }

    public Button _BtnToothache { get; set; }

    public Button _BtnTuizhong { get; set; }


    #endregion

    protected override void AddListeners()
    {
        base.AddListeners();

        _BtnBack.onClick.AddListener(OnBackBtnClick);
        _BtnStomacache.onClick.AddListener(OnStomacacheBtnClick);
        _BtnVaccine.onClick.AddListener(OnBtnVaccineBtnClick);
        _BtnHighFever.onClick.AddListener(OnHighFeverBtnClick);
        _BtnLowFever.onClick.AddListener(OnLowFeverBtnClick);
        _BtnToothache.onClick.AddListener(OnToothacheBtnClick);
        _BtnTuizhong.onClick.AddListener(OnTuizhongBtnClick);
        _ButtonMiniGame.onClick.AddListener(OnMoniGameClickerClick);
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();

        _BtnBack.onClick.RemoveListener(OnBackBtnClick);
        _BtnStomacache.onClick.RemoveListener(OnStomacacheBtnClick);
        _BtnVaccine.onClick.RemoveListener(OnBtnVaccineBtnClick);
        _BtnHighFever.onClick.RemoveListener(OnHighFeverBtnClick);
        _BtnLowFever.onClick.RemoveListener(OnLowFeverBtnClick);
        _BtnToothache.onClick.RemoveListener(OnToothacheBtnClick);
        _BtnTuizhong.onClick.RemoveListener(OnTuizhongBtnClick);
        _ButtonMiniGame.onClick.AddListener(OnMoniGameClickerClick);
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        _ButtonMiniGame.transform.parent.SetActive(false);
        _clinicRoomController = ClinicRoomController.Instance;
        _clinicRoomController._onMiniGameStartReady += OnMiniGameReadyStart;


    }

    protected override void OnClose()
    {
        base.OnClose();
        _clinicRoomController._onMiniGameStartReady -= OnMiniGameReadyStart;
    }

    private void OnMiniGameReadyStart(eHospitalGame gametype)
    {
        string iconName = HospitalConsts.hospitalGameClickIcons[(int)gametype];
        _ButtonMiniGame.image.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Hospital, iconName);
        _ButtonMiniGame.image.SetNativeSize();
        _ButtonMiniGame.transform.parent.SetActive(true);
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        ClinicRoomController.Instance.OnUpdate();
    }


    #region UIEvent

    private void OnBackBtnClick()
    {
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }

    private void OnMoniGameClickerClick()
    {
        _ButtonMiniGame.interactable = false;
        TransitionManager.Instance.StartTransition(() => {
            switch (_clinicRoomController.m_currentHospitalGame)
            {
                case eHospitalGame.game_Stomachache:
                    StomachacheGameManager.DestroyInstance();
                    StomachacheGameManager.Instance.GameStart();
                    break;
                case eHospitalGame.game_Toothache:
                    ToothacheGameManager.DestroyInstance();
                    ToothacheGameManager.Instance.GameStart();
                    break;
                case eHospitalGame.game_vaccine:
                    VaccineGameManager.DestroyInstance();
                    VaccineGameManager.Instance.GameStart();
                    break;
                case eHospitalGame.game_Bruise:
                    InjectionController.Instance.StartGame();
                    break;
                case eHospitalGame.game_LowFever:
                    break;
                case eHospitalGame.game_HighFever:
                    break;
                default:
                    break;
            }
            _ButtonMiniGame.transform.parent.SetActive(false);
            HospitalResManager.Instance.m_lediRoot.SetActive(false);
        },null);
           
    }

    private void OnStomacacheBtnClick()
    {
        _clinicRoomController.StartGame(eHospitalGame.game_Stomachache);
    }

    private void OnBtnVaccineBtnClick()
    {
        _clinicRoomController.StartGame(eHospitalGame.game_Bruise);
    }

    private void OnHighFeverBtnClick()
    {
        _clinicRoomController.StartGame(eHospitalGame.game_HighFever);
    }

    private void OnLowFeverBtnClick()
    {
        _clinicRoomController.StartGame(eHospitalGame.game_LowFever);
    }

    private void OnToothacheBtnClick()
    {
        _clinicRoomController.StartGame(eHospitalGame.game_Toothache);
    }

    private void OnTuizhongBtnClick()
    {
        _clinicRoomController.StartGame(eHospitalGame.game_vaccine);
    }

    #endregion
}

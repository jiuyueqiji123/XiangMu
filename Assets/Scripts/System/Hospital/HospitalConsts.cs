﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalConsts {

    public const string hospitalAudioBasePath = "game_hospital/game_hospital_audio";
    public const string game_hospital_prefab_stomachache_BasePath = "game_hospital/game_hospital_prefab_stomachache";
    public const string game_hospital_texture_BasePath = "game_hospital/game_hospital_texture";
    public const string game_hospital_effect_BasePath = "game_hospital/game_hospital_effect";

    public const string buttombarLayoutItem = "buttombarLayoutItem";

    //肚子痛游戏


    //牙痛游戏
    public const string icebagPath = "icebag";
    public const string brushCleanerPath = "ToothCleanner";
    public const string brushPath = "ToothBrush";

    //药丸战士
    public const string bacteriaGameRoot = "BacteriaGameRoot";
    public const string drug = "drug";
    public const string enemy = "enemy";

    //乐迪说话资源
    public const string ledi_player_animation_root = "PlayerAnimationRoot";

    //主界面小游戏提示气泡
    public static string[] hospitalGameClickIcons = { "LDETYY_dzt_icon" , "LDETYY_yt_icon", "LDETYY_tz_icon", "LDETYY_ym_icon", "LDETYY_ds_icon", "LDETYY_gs_icon" };
    public const string dishao = "LDETYY_ds_icon";  //低烧
    public const string gaoshao = "LDETYY_gs_icon"; //高烧
    public const string duzitong = "LDETYY_dzt_icon";   //肚子痛
    public const string tuizhong = "LDETYY_tz_icon";    //腿肿
    public const string yatong = "LDETYY_yt_icon";    //牙痛
    public const string yimiao = "LDETYY_ym_icon";    //疫苗


    //音效
    public const string audio_select_correct = "601010110"; //物品匹配正确
    public const string audio_door_open = "603020401";  //门开关
    public const string audio_huandong = "604040101";   //扫描仪晃动

    static string[] ledi_zhiliao = { "404060401", "404060402", "404060403", "404060404" };
    public static string audio_ledi_zhiliao {   //开始治疗
        get {
            int index = Random.Range(0, 4);
            return ledi_zhiliao[index];
        }
    }

    public const string audio_bgMusic = "604060100";  //背景音乐
    public const string audio_saomiao_work = "604060101";  //扫描仪开始工作提示音
    public const string audio_saomiao_sound = "604060102";  //扫描仪扫描的声音
    public const string audio_virus_laugh= "604060103";  //病毒邪恶的笑声
    public const string audio_virus_hong_laugh = "604060107";  //红病毒邪恶的笑声
    public const string audio_virus_huang_laugh = "604060109";  //黄色病毒邪恶的笑声
    public const string audio_virus_lan_laugh = "604060108";  //蓝色病毒邪恶的笑声
    public const string audio_yaowan_gongji = "604060104";  //药丸攻击病毒的音效（刀光剑影）
    public const string audio_bingdu_canjiao = "604060105";  //病毒被攻击后啊的一声
    public const string audio_yaowan_victory = "604060106";  //药丸攻击完病毒后胜利的声音
    public const string audio_juese_zoulu = "604060201";  //角色走路音效
    public const string audio_ui_huaru = "604060202";  //UI滑出/滑入的音效
    public const string audio_wuping_xuanze = "604060203";  //点击或选择物品音效
    public const string audio_zidongxifu = "604060204";  //自动吸附音效
    public const string audio_zidonghuigui = "604040105";  //自动归音效
    public const string audio_ui_qiehuan = "604060206";  //UI切换音效
    public const string audio_tuyao = "604060207";  //涂药音效
    public const string audio_pengshui = "604060301";  //喷头喷水的声音
    public const string audio_shuaya = "604060302";  //牙刷刷牙的声音
    public const string audio_tiwenji_di = "604060401";  //体温计滴的一声
    public const string audio_bingfu_tumo = "604060501";  //冰敷/涂抹药膏的效果
    public const string audio_pengwu = "604060502";  //药瓶喷雾，嗤嗤的效果
    public const string audio_tieyaogao = "604060503";  //药膏贴上去的音效

    public const string audio_nuowa_yateng_1 = "404060109";  //哎呦...哎呦...
    public const string audio_nuowa_yateng_2 = "404060110";  //呵呵...不疼了
    public const string audio_nuowa_yateng_3 = "404060111";  //额.....
    public const string audio_nuowa_yateng_4 = "404060112";  //哇...哦...
    public const string audio_aisha_dazhengbuteng = "404060308";  //嘻嘻....打防疫针一点都不疼


    //-----------------配音---------------------------


    public const string audio_ledi_huanying = "404060439";  //hi 这里是乐迪儿童医院
    public const string audio_ledi_xunwen = "404060440";  //你好!感觉哪里不舒服吗？
    public const string audio_ledi_zuodebucuo = "404060441";  //哇哦...做得不错！
    public const string audio_ledi_yimiao_1 = "404060442";  //是的，防疫针可以让我们的身体变得更加强壮！
    public const string audio_ledi_yimiao_2 = "404060443";  //选择消毒药水、注射器和创可贴。
    public const string audio_ledi_yimiao_3 = "404060444";  //先用消毒药水在手臂上消毒。
    public const string audio_ledi_yimiao_4 = "404060445";  //开始给艾莎打防疫针。
    public const string audio_ledi_yimiao_5 = "404060446";  //用创可贴保护伤口。
    public const string audio_ledi_fashao_1 = "404060447";  //用体温枪量一量体温。
    public const string audio_ledi_fashao_2 = "404060448";  //在额头上贴上退热贴来降温。
    public const string audio_ajiasi_chiyao = "404060449";  //哇哦...做的不错，现在给阿佳思吃药
    public const string audio_nuowa_chiyao = "404060450";  //哇哦...做的不错，现在给诺娃吃药。
    public const string audio_ledi_saomiao = "404060451";  //用扫描仪看看肚子里发生了什么事情。
    public const string audio_ledi_zhengzaixiaomie = "404060452";  //看...!是药物在消灭病菌。
    public const string audio_ledi_lanse_guancha = "404060453";  //仔细观察，蓝色的药丸消灭哪一种细菌呢？
    public const string audio_ledi_hongse_guancha = "404060454";  //仔细观察，红色的药丸消灭哪一种细菌呢？
    public const string audio_ledi_huangse_guancha = "404060455";  //仔细观察，黄色的药丸消灭哪一种细菌呢？
    public const string audio_ledi_shuaya_cehngzan = "404060456";  //哇哦...做的不错，把诺娃的牙齿刷干净了。
    public const string audio_ledi_tuizhong1 = "404060457";  //选择冰袋、消肿药和纱带。
    public const string audio_ledi_tuizhong2 = "404060458";  //先用冰袋给伤口消肿。
    public const string audio_ledi_tuizhong3 = "404060459";  //在伤口上喷消肿药。
    public const string audio_ledi_tuizhong4 = "404060460";  //均匀的涂抹消肿药。
    public const string audio_ledi_tuizhong5 = "404060461";  //缠上纱带保护伤口。





    public const string audio_ledi_zhiliao_duzi = "404060401"; //别担心，我们马上开始治疗。
    public const string audio_ledi_zhiliao_yimiao = "404060402"; //别着急，我们马上开始治疗。
    public const string audio_ledi_zhiliao_fashao = "404060403"; //看起来像是发烧了，我们马上开始治疗。
    public const string audio_ledi_zhiliao_yatong = "404060404"; //哎呀，你看起来很不舒服。我们马上开始治疗。

    public const string audio_ledi_saomiaoyi = "404060405"; //打开扫描仪，找找病因。
    public const string audio_ledi_zangdongxi = "404060406";    //诺娃用脏手吃了脏东西，把细菌带到了肚子里，所以才会肚子疼。
    public const string audio_ledi_peiyao = "404060407";    //快来给他配药吧。
    public const string audio_ledi_chiyaoguli = "404060408";    //药丸好像小糖豆，咕嘟咕嘟吞下肚。
    public const string audio_ledi_jiayou = "404060409";    //小朋友，我们一起来给他加加油吧。
    public const string audio_ledi_jiayou1 = "404060410";   //跟我一起说“你可以的，加油。”
    public const string audio_ledi_duzitixing = "404060411";    //小朋友记住啦，饭前便后要洗手，拒绝细菌跑进肚。

    public const string audio_ledi_dazheng_anfu = "404060412";  //针头尖尖不可怕，放轻松伸出手，就像蚊子咬了口。
    public const string audio_ledi_dazhengguli = "404060413";   //跟我一起大声说：“勇敢勇敢。”
    public const string audio_ledi_dazheng_jiaodao = "404060414";   //小朋友，打防疫针可以预防疾病，保护我们的身体健康！

    public const string audio_ledi_yatong_xiaozhong = "404060415";  //诺娃的脸肿的太厉害了，先给他消肿。
    public const string audio_ledi_zhangzui = "404060416";      //太好了，消肿拉。请张开嘴巴，我们来检查看看吧。
    public const string audio_ledi_yatongyuanyin = "404060417"; //诺娃吃了太多糖，又不爱刷牙，牙齿都坏掉拉。
    public const string audio_ledi_shuaya_tixing = "404060418"; //牙齿好脏呀，快给他洗洗吧。
    public const string audio_ledi_shuaya_jiaodao = "404060419"; //少吃糖果早晚刷牙，做个讲卫生的好孩子哟。

    public const string audio_ledi_dishao_tixing = "404060423"; //发低烧拉，赶快给他降温。
    public const string audio_ledi_dishao_jiaodao = "404060424"; //小朋友，发低烧时要多喝热水多休息，让爸爸妈妈给你洗洗温水澡。

    public const string audio_ledi_gaoshao_tixing1 = "404060428"; //哎呀不好，发高烧拉。
    public const string audio_ledi_gaoshao_tixing2 = "404060429"; //还是有点发烧呢，给他配点退烧药吧。
    public const string audio_ledi_weiyao_tixing = "404060430"; //快给他喂药吧。
    public const string audio_ledi_gaoshao_jiaodao = "404060431"; //小朋友，发烧了一定要马上告诉爸爸妈妈哦。

    //腿肿
    public const string audio_ledi_tui_zhiliao = "404060432"; //啊，腿又红又肿，要赶紧治疗才行。
    public const string audio_ledi_tui_yaopinzhunbei = "404060433"; //药品准备好啦。
    public const string audio_ledi_bingkuaixiaozhong = "404060434"; //用冰块消消肿。
    public const string audio_ledi_tuxiaozhongyao = "404060435"; //给病人涂上消肿药。
    public const string audio_ledi_bangbengdai = "404060436"; //给病人贴上纱带。。
    public const string audio_ledi_tui_jiaodao = "404060437"; //追赶打闹易受伤，玩耍时千万要注意。。

    //肚子疼
    public const string audio_nuowa_duziteng = "404060101"; //哎呦，哎呦，肚子好疼啊，快帮我看看吧。
    public const string audio_nuowa_yateng = "404060102"; //哎呀哎呀，疼死了，我的牙齿好疼，医生快给我看看。
    public const string audio_nuowa_buchiyao1 = "404060103"; //不要不要，我不要吃药。
    public const string audio_nuowa_buchiyao2 = "404060104"; //不吃药不吃药，吃药好苦啊。
    public const string audio_nuowa_duzi_ganji = "404060105"; //谢谢乐迪，我再也不用脏手吃东西了。
    public const string audio_nuowa_ya_fanxing = "404060106"; //以后我会记得刷牙，再也不吃那么多糖了，牙齿坏了可真难受。
    public const string audio_nuowa_ya_tixing = "404060106"; //啊哦（玩家清洗牙齿时，停下待机语音）
    public const string audio_nuowa_shuaya_fankui = "404060107"; //呜~（玩家清洗牙齿时，语音反馈）

    //打疫苗
    public const string audio_aisha_liuganyimiao = "404060301"; //乐迪医生，我来打流感疫苗。
    public const string audio_aisha_yilaodayimiao = "404060302"; //乐迪医生，我来打乙脑疫苗，老师说打防疫针可以预防生病。
    public const string audio_aisha_budazheng = "404060303"; //我不要打针了，不要打针了，打针太疼了。
    public const string audio_aisha_budazheng1 = "404060304"; //不打针不打针，我不打针了。
    public const string audio_aisha_dazhenbuteng = "404060305"; //嘻嘻，打针好像真的没那么疼。
    public const string audio_aisha_shuaidao = "404060306"; //我和小伙伴玩捉迷藏时不小心摔倒了，呜呜呜。
    public const string audio_aisha_ganxie = "404060307"; //太好了，我的腿不疼了。

    //特效
    public const string effect_select_correct = "502004073"; //选择正确的药丸等，自动吸附到位置时的特效
    public const string effect_wancheng_zhiliao = "502004100";  //完成治疗的星星特效
    public const string effect_tumo_yaogao = "502004099";   //拖动药膏特效
    public const string effect_yaoping_pengsa = "502004098";    //药瓶喷洒药水特效
    public const string effect_xiya_pengshui = "502004097"; //洗牙喷水特效
    public const string effect_shuaya_paomo = "502004096";  //刷牙起泡泡特效
    public const string effect_yaowan_xingzou = "502004095";    //药丸行走特效
    public const string effect_dianji_yaowan = "502004094";    //点击药丸特效


    //音效
    public const string yinxiao_kaishisaomiao = "604060101";    //扫描仪开始工作提示音
    public const string yinxiao_shaomiaoyiwork = "604060102";   //扫描仪扫描的声音
    public const string yinxiao_bingduxiao = "604060103";       //病毒邪恶的笑声
    public const string yinxiao_gongjibingdu = "604060104";     //药丸攻击病毒的音效（刀光剑影）
    public const string yinxiao_bingducanjiao = "604060105";    //病毒被攻击后啊的一声
    public const string yinxiao_yaowanshengli = "604060106";    //药丸攻击完病毒后胜利的声音
    public const string yinxiao_juesezoulu = "604060201";       //角色走路音效
    public const string yinxiao_uihuaru = "604060202";          //UI滑出/滑入的音效
    public const string yinxiao_dianji = "604060203";           //点击或选择物品音效
    public const string yinxiao_zidongxifu = "604060204";       //自动吸附音效
    public const string yinxiao_zidonghuigui = "604060205";     //自动归音效
    public const string yinxiao_uiqiehuan = "604060206";        //UI切换音效
    public const string yinxiao_tuyao = "604060207";            //涂药音效
    public const string yinxiao_pentoupenshui = "604060301";    //喷头喷水的声音
    public const string yinxiao_yashuashuaya = "604060302";     //牙刷刷牙的声音
    public const string yinxiao_tuwenji = "604060401";          //体温计滴的一声
    public const string yinxiao_bingfutuyao = "604060501";      //冰敷/涂抹药膏的效果
    public const string yinxiao_yaopinpenwu = "604060502";      //药瓶喷雾，嗤嗤的效果
    public const string yinxiao_yaogaotiehao = "604060503";     //药膏贴上去的音效

    //背景音乐
    public const string BackMusic = "604060100";
}

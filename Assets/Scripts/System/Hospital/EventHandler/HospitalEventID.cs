﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HospitalEventID {

    //拖拽
    public const string DRAG_BEGIN = "DRAG_BEGIN";  //开始拖拽UI
    public const string DRAGGING = "DRAGGING";  //正在拖拽
    public const string DRAG_MATCH = "DRAG_MATCH";  //拖拽匹配是否成功
    public const string ON_HOVER_UI = "ON_HOVER_UI";  //当鼠标滑过监听的ui
    public const string ON_POINTDOWN_UI = "ON_POINTDOWN_UI";  //当鼠标滑过切按住

    //公共
    public const string SMALLGAME_FORCEOVER = "SMALLGAME_FORCEOVER";    //小游戏强制结束
    public const string SMALLGAME_START = "SMALLGAME_START";    //小游戏开始
    public const string SMALLGAME_OVER = "SMALLGAME_OVER";      //小游戏结束

}

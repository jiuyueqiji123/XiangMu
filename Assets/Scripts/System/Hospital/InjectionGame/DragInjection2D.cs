﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(DragIdentify))]
public class DragInjection2D : UIDragBase {

    public Transform TuiShaoTie;
    public Transform tuishaotie;
    Texture CktTexture;

    //public LayerMask laymask;
    [HideInInspector]
    public bool IsCarryOut = false;
    [HideInInspector]
    public bool IsDragging = true;
    public override void OnBeginDrag(PointerEventData eventData)
    {

        DragComparerManager.Instance._currentDragAgent = InjectionWindow.Instance._Agter.GetComponent<RectTransform>();
        TuiShaoTie = this.transform.GetChild(1);
        base.OnBeginDrag(eventData);
        IsCarryOut = true;
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnInJectionCKT);
       

    }

    //public override Vector3 Get3DPosition()
    //{

    //    Plane plane = new Plane(HospitalResManager.Instance.Game_VaccineRoot.GetChild(0).GetComponent<Camera>().transform.forward * -1 + new Vector3(10f, 0, 0), Vector3.zero);
    //    Ray ray = HospitalResManager.Instance.Game_VaccineRoot.GetChild(0).GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
    //    float dis;
    //    if (plane.Raycast(ray, out dis))
    //    {
    //        Vector3 point = ray.GetPoint(dis);
    //        return point;
    //    }

    //    return Vector3.zero;
    //}

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        if (IsCarryOut)
        {
            //this.gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
            //AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/604040105");
        }
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnInJectionCKT);



    }


    public void OnInJectionCKT(DragIdentify identify)
    {
        Debug.LogError("------------KTISCarryOut--------------------");
        if (!IsCarryOut && !DragComparerManager.Instance.IsMatch(this.gameObject.GetComponent<DragIdentify>()))
        {
            return;
        }
        
        IsCarryOut = false;
        AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/601010110");
        Sprite sprite = this.transform.GetChild(0).GetComponent<Image>().sprite;
        CktTexture =ResourceManager.Instance.GetResource(InjectionConsts.Path + InjectionController.Instance.CKTTextureReplace(sprite.name),typeof(Texture),enResourceType.UIPrefab,false).m_content as Texture;
        InjectionWindow.Instance.chuankoutie.GetComponent<Renderer>().material.mainTexture = CktTexture ;
        InjectionWindow.Instance._chuangkoutie.GetComponent<Image>().sprite = this.transform.GetComponent<Image>().sprite;
        InjectionWindow.Instance._chuangkoutie.GetComponent<Image>().SetNativeSize();
        InjectionWindow.Instance._chuangkoutie.rotation = Quaternion.Euler(0,0,0);
        InjectionController.Instance.Model.chuangkoutieOver = true;
        InjectionWindow.Instance._chuangkoutie.Find("502004073").SetActive(true);
        TimerManager.Instance.AddTimer(0.8f, () =>
        {
            Destroy(InjectionWindow.Instance._chuangkoutie.Find("502004073").gameObject);
        });
        InjectionWindow.Instance._Agter.SetActive(false);
        if (InjectionController.Instance.Model.zhentongOver && InjectionController.Instance.Model.xiaoyanyaoOver && InjectionController.Instance.Model.chuangkoutieOver)
        {
            Debug.LogError("---------- 匹配完成 -----------");
            InjectionWindow.Instance._RightBarCkt.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-580, 0), 0.5f);
            
            //HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnCarryOutSyringe);
            InjectionWindow.Instance._xiaoyanyao.GetComponent<DragInjection>().enabled = true;
            InjectionWindow.Instance._xiaoyanyao.GetComponent<DragIdentify>().ID = "1005";
            
            HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnInJectionCKT);
            InjectionWindow.Instance._xiaoyanyao.GetComponent<BreathingEffect>().enabled = true;
            AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ledi_zuodebucuo);
            InjectionWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ledi_zuodebucuo).length + 1.5f, ()=>
            {
                AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060438");
                InjectionWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060438").length + 1.5f, () =>
                {
                    InjectionWindow.Instance._yuyinjili.SetActive(true);
                    AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ledi_yimiao_3);
                    HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, InjectionController.Instance.OnHoverOverMouse);
                });

            });
            
        }

    }
     

}

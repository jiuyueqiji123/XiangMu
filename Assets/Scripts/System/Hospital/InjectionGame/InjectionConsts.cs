﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InjectionConsts{

    public const string Path = "game_hospital/game_hospital_texture/";

    public const string xiaoyanyao_red = "304060301-1";
    public const string xiaoyanyao_purple = "304060301-2";
    public const string xiaoyanyao_blue = "304060301-3";

    public const string mianqian_red = "304060302_red";
    public const string mianqian_blue = "304060302_blue";
    public const string mianqian_purple = "304060302_purple";

    public const string shoubi_red = "304060301_red";
    public const string shoubi_blue = "304060301_blue";
    public const string shoubi_purple = "304060301_purple";

    public const string shoubi_red000 = "304060301_red000";
    public const string shoubi_blue000 = "304060301_blue000";
    public const string shoubi_purple000 = "304060301_purple000";

    public const string chuangkoutie_red = "304060304-1";
    public const string chuangkoutie_orange = "304060304-2";
    public const string chuangkoutie_yellow = "304060304-3";
    public const string chuangkoutie_greed = "304060304-4";
    public const string chuangkoutie_blue = "304060304-5";
    public const string chuangkoutie_purple = "304060304-6";
}

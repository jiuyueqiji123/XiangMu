﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(DragIdentify))]
public class DragInjection : UIDrag3DBase {

    public Transform TuiShaoTie;
    public Transform tuishaotie;

    //public LayerMask laymask;
    [HideInInspector]
    public bool IsCarryOut = false;
    [HideInInspector]
    public bool IsDragging = true;
    public override void OnBeginDrag(PointerEventData eventData)
    {

        base._currentDragObject = this.transform.Find("xiaoyanyao1");
        if (base._currentDragObject == null)
        {
            base._currentDragObject = this.transform.Find("zhentong1");
        }
        TuiShaoTie = this.transform.GetChild(1);
        //InjectionWindow.Instance.DraggingObj = this.gameObject;
        //HospitalResManager.Instance.Game_LowFeverRoot.GetChild(2).Find("Etou");
        //Debug.LogError(HospitalResManager.Instance.Game_LowFeverRoot.GetChild(2).Find("201003000@skin/root/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head/Etou").name);
        base.OnBeginDrag(eventData);
        //Debug.LogError("------------------------------------XXXXXXXXXXXXXXXX");
        //_isOnDragging = true;
        //this.gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color(1,1,1,0);
        IsCarryOut = true;
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnCarryOutSyringe);
        //_currentDragAgent = this.gameObject.GetComponent<RectTransform>();
        

    }

    public override Vector3 Get3DPosition()
    {

        Plane plane = new Plane(HospitalResManager.Instance.Game_VaccineRoot.GetChild(0).GetComponent<Camera>().transform.forward * -1 + new Vector3(10f, 0, 0), Vector3.zero);
        Ray ray = HospitalResManager.Instance.Game_VaccineRoot.GetChild(0).GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        if (IsCarryOut)
        {
            //this.gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
        }
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnCarryOutSyringe);
    }

    public void OnCarryOutSyringe(DragIdentify identify)
    {
        if (!IsCarryOut || DragComparerManager.Instance.IsMatch(this.gameObject.GetComponent<DragIdentify>()) == false)
        {
            
            return;
        }
        Debug.LogError("-------------SyringeIsCarryOut--------------------");
        
        if (identify.ID == "1002")
        {
            IsCarryOut = false;
            Sprite sprite = this.transform.GetChild(0).GetComponent<Image>().sprite;
            Debug.LogError(sprite.name);
            foreach (Transform item in InjectionWindow.Instance._xiaoyanyao)
            {
                item.SetActive(false);
            }
            //Debug.LogError(InjectionWindow.Instance._xiaoyanyao.gameObject);
            Color color = new Color(1,1,1,0);
            InjectionWindow.Instance._xiaoyanyao.gameObject.GetComponent<Image>().color = color;
            InjectionWindow.Instance._xiaoyanyao.transform.Find(sprite.name).SetActive(true);
            
            if (InjectionWindow.Instance._xiaoyanyao.Find("xiaoyanyao1") != null)
            {
                Destroy(InjectionWindow.Instance._xiaoyanyao.Find("xiaoyanyao1").gameObject);
            }
            Transform xiaoyanyao = Instantiate(_currentDragObject);
            xiaoyanyao.gameObject.name = "xiaoyanyao1";
            xiaoyanyao.SetParent(InjectionWindow.Instance._xiaoyanyao);
            xiaoyanyao.localScale = new Vector3(10,10,10);
            base._currentDragObject.SetActive(false);
            InjectionController.Instance.Model.xiaoyanyaoOver = true;
            InjectionWindow.Instance._RightBarXyy.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-580,0),0.5f);
            TimerManager.Instance.AddTimer(0.51f,()=>
            {
                InjectionWindow.Instance._RightBarZt.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-840, 0), 0.5f);
            });
        }
        if (identify.ID == "1001")
        {
            IsCarryOut = false;
            Sprite sprite = this.transform.GetChild(0).GetComponent<Image>().sprite;
            Debug.LogError(sprite.name);
            foreach (Transform item in InjectionWindow.Instance._zhentong)
            {
                item.SetActive(false);
            }
            InjectionWindow.Instance._zhentong.GetComponent<Image>().color = new Color(1, 1, 1, 0);
            InjectionWindow.Instance._zhentong.transform.Find(sprite.name).SetActive(true);
            
            if (InjectionWindow.Instance._xiaoyanyao.Find("zhentong1") != null)
            {
                Destroy(InjectionWindow.Instance._xiaoyanyao.Find("zhentong1").gameObject);
            }
            Transform zhentong = Instantiate(_currentDragObject);
            zhentong.gameObject.name = "zhentong1";
            zhentong.SetParent(InjectionWindow.Instance._zhentong);
            zhentong.localScale = new Vector3(10, 10, 10);
            base._currentDragObject.SetActive(false);
            InjectionController.Instance.Model.zhentongOver = true;
            InjectionWindow.Instance._RightBarZt.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-580, 0), 0.5f);
            TimerManager.Instance.AddTimer(0.51f, () =>
            {
                InjectionWindow.Instance._RightBarCkt.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-840, 0), 0.5f);
            });

        }
        
        if (InjectionController.Instance.Model.zhentongOver && InjectionController.Instance.Model.xiaoyanyaoOver && InjectionController.Instance.Model.chuangkoutieOver)
        {
            Debug.LogError("---------- 匹配完成 -----------");
            InjectionWindow.Instance._RightBarCkt.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-580, 0), 0.5f);
            HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI,InjectionController.Instance.OnHoverOverMouse);
            HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnCarryOutSyringe);
            InjectionWindow.Instance._xiaoyanyao.GetComponent<DragInjection>().enabled = true;
            InjectionWindow.Instance._xiaoyanyao.GetComponent<DragIdentify>().ID = "1005";
            InjectionWindow.Instance._yuyinjili.SetActive(true);
            HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnCarryOutSyringe);
        }

    }

    

    
    
    

}

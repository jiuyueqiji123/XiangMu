﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InjectionWindow : SingletonBaseWindow<InjectionWindow> {

    public Transform _RightScrollRect { get; set; }
    public Transform _RightBarXyy { get; set; }
    public Transform _RightBarZt { get; set; }
    public Transform _RightBarCkt { get; set; }
    public Transform _Agter { get; set; }
    public Transform _xiaoyanyao { get; set; }
    public Transform _zhentong { get; set; }
    public Transform _chuangkoutie { get; set; }
    public Transform _yuyinjili { get; set; }
//    public Transform _mianqianClick { get; set; }
    public Transform _zhusheClick { get; set; }
    public Transform _Btn_Back { get; set; }
    public GameObject DraggingObj;

    public Transform xiaoyanyao;
    public Transform mianqian;
    public Transform zhentong;
    public Transform chuankoutie;
    public Transform shoubi;
    public Transform shuazi;

    public bool IsXyyClick = false;
    public bool IsZhanXyy = false;
    public bool IsChaYao = false;
    public bool IsZhenTongMove = false;
    public bool IsZhentongAnimaPlay = false;

    private Draw m_draw;

    //public InjectionModel model;
    protected override void AddListeners()
    {
        base.AddListeners();
        _xiaoyanyao.AddComponentEx<UIInputHandler>().OnClick += XiaoYanAnimator;
        //_mianqianClick.AddComponentEx<UIInputHandler>().OnClick += MianQianAnimator;
        _zhentong.AddComponentEx<UIInputHandler>().OnClick += ZhenTongAnimator;
        _zhusheClick.AddComponentEx<UIInputHandler>().OnClick += ZhenTongAnimator;
        _chuangkoutie.AddComponentEx<UIInputHandler>().OnClick += OnClickCktAnimator;
        _Btn_Back.AddComponentEx<UIInputHandler>().OnClick += OnExitGame;
        _xiaoyanyao.GetComponent<UIInputHandler>().enabled = false;
        //_mianqianClick.GetComponent<UIInputHandler>().enabled = false;
        _zhentong.GetComponent<UIInputHandler>().enabled = false;
        _zhusheClick.GetComponent<UIInputHandler>().enabled = false;
        _chuangkoutie.GetComponent<UIInputHandler>().enabled = false;
        chuankoutie = HospitalResManager.Instance.Game_VaccineRoot.Find("Role Root/201004000@skin/root/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 L Clavicle/Bip001 L UpperArm/304060304");
        shoubi = HospitalResManager.Instance.Game_VaccineRoot.Find("Role Root/201004000@skin/root/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 L Clavicle/Bip001 L UpperArm/304060301_blue000");
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();
        _xiaoyanyao.AddComponentEx<UIInputHandler>().OnClick -= XiaoYanAnimator;
       // _mianqianClick.AddComponentEx<UIInputHandler>().OnClick -= MianQianAnimator;
        _zhentong.AddComponentEx<UIInputHandler>().OnClick -= ZhenTongAnimator;
        _zhusheClick.AddComponentEx<UIInputHandler>().OnClick -= ZhenTongAnimator;
        _chuangkoutie.AddComponentEx<UIInputHandler>().OnClick -= OnClickCktAnimator;
        _Btn_Back.AddComponentEx<UIInputHandler>().OnClick -= OnExitGame;


    }

    protected override void OnUpdate()
    {
        
        base.OnUpdate();
        InjectionController.Instance.AiSha_AnimatorEx.OnUpdate();
        
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);
        //TransitionManager.Instance.StartTransition(null, null);
        _RightBarXyy.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-840,0),0.5f);
        WindowManager.Instance.CloseWindow(WinNames.ClinicRoomPanel);
        AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ledi_yimiao_2);
    }

    protected override void OnClose()
    {
        base.OnClose();
        IsXyyClick = false;
        IsZhanXyy = false;
        IsChaYao = false;
        IsZhenTongMove = false;
        IsZhentongAnimaPlay = false;
    }

    public void OnExitGame()
    {
        WindowManager.Instance.CloseWindow(WinNames.InjectionPanle);
        InjectionController.DestroyInstance();
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
        
    }

    public void XiaoYanAnimator()
    {
        if (IsXyyClick)
        {
            return;
        }
        IsXyyClick = true;
        InjectionWindow.Instance._xiaoyanyao.GetComponent<BreathingEffect>().suofangObj.transform.localScale = new Vector3(1,1,1);
        InjectionWindow.Instance._xiaoyanyao.GetComponent<BreathingEffect>().enabled = false;
        Debug.LogError("----------xiaoyanEnter--------------");
        xiaoyanyao =  _xiaoyanyao.Find("xiaoyanyao1");
        //xiaoyanyao.localPosition = new Vector3(184.528f,-8766.133f,4585.17f);
        mianqian = xiaoyanyao.GetChild(0);
        shuazi = mianqian.GetChild(0);
        xiaoyanyao.SetActive(true);
        mianqian.SetActive(true);
        xiaoyanyao.localScale = new Vector3(5,5,5);
        xiaoyanyao.DOMove(HospitalResManager.Instance.Game_VaccineRoot.Find("XYYBOX").position,0.8f);
        xiaoyanyao.DORotate(new Vector3(0,80.94f,0),0.8f);
        this.AddTimerEx(0.8f,()=>
        {
            MianQianAnimator();
            //_mianqianClick.GetComponent<UIInputHandler>().enabled = true;
        });

    }

    public void MianQianAnimator()
    {
        Debug.LogError("-------------mianqiananimatorEntor---------------");
        if (!IsZhanXyy)
        {
            IsZhanXyy = true;
            mianqian.DOLocalMove(new Vector3(-0.001f, 0.082f, -0.004f), 0.3f);
            mianqian.DOLocalRotate(new Vector3(0, 0, 180), 0.3f);
            this.AddTimerEx(0.3f, () =>
             {
                 mianqian.DOLocalMove(new Vector3(-0.001f, 0.045f, -0.004f), 0.3f);
                 this.AddTimerEx(0.3f, () =>
                 {
                     mianqian.DOLocalMove(new Vector3(-0.001f, 0.082f, -0.004f), 0.3f);
                     this.AddTimerEx(0.3f, () =>
                     {
                         mianqian.DOLocalMove(new Vector3(-0.001f, 0.045f, -0.004f), 0.3f);
                         this.AddTimerEx(0.3f, () =>
                         {
                             mianqian.DOLocalMove(new Vector3(-0.001f, 0.15f, 0.017f), 0.4f);
                             



                             this.AddTimerEx(0.41f, () =>
                             {

                                 //MianQianAnimator();
                                 mianqian.AddComponentEx<DragIdentify>();
                                 mianqian.GetComponent<DragIdentify>().ID = "2001";
                                 mianqian.AddComponentEx<Dragmianqian>();

                                 mianqian.DOLocalMove(new Vector3(-0.133f, 0.004f, 0.017f), 0.4f);
                                 mianqian.DOLocalRotate(new Vector3(180, 0, 210), 0.4f);



                                 this.AddTimerEx(1f, () =>
                                {

                                    //MianQianAnimator();
                                    mianqian.AddComponentEx<DragIdentify>();
                                    mianqian.GetComponent<DragIdentify>().ID = "2001";
                                    mianqian.AddComponentEx<Dragmianqian>();

                                    //CarryoutMianqian();
                                });


                             });
                             this.AddTimerEx(1f, () =>
                            {
                                mianqian.SetParent(_xiaoyanyao);
                                xiaoyanyao.DOLocalMove(HospitalResManager.Instance.Game_VaccineRoot.Find("GoBack").position, 0.6f);
                            });


                             //CarryoutMianqian();
                         });



                     });

                 });

             });

        }
    




    }

    public void CarryoutMianqian()
    {
        InjectionWindow.Instance._xiaoyanyao.Find("Carryout").SetActive(true);
        mianqian.SetActive(false);
        _yuyinjili.SetActive(false);
       // _mianqianClick.SetActive(false);
        shoubi.SetActive(true);
        _zhentong.GetComponent<UIInputHandler>().enabled = true;
        //_zhentong.GetComponent<DragInjection>().enabled = true;
        _zhentong.GetComponent<DragIdentify>().ID = "1006";
        _xiaoyanyao.GetComponent<DragInjection>().enabled = false;
    }

    public void ZhenTongAnimator()
    {
        Debug.LogError("------------------ZhenTongAnimatorEnter---------------------");
        if (!IsZhenTongMove)
        {
            InjectionWindow.Instance._zhentong.GetComponent<BreathingEffect>().suofangObj.transform.localScale = new Vector3(1,1,1);
            InjectionWindow.Instance._zhentong.GetComponent<BreathingEffect>().enabled = false;
            IsZhenTongMove = true;
            zhentong = _zhentong.Find("zhentong1");
            zhentong.SetActive(true);
            zhentong.localScale = new Vector3(0.1f,0.1f,0.1f);
            zhentong.localPosition = new Vector3(-6.03f,-9066.84f,-4739.03f);
            zhentong.DOLocalMove(new Vector3(-8.54f, -9065.53f, -4739.73f), 1f);
            zhentong.DOLocalRotate(new Vector3(-182.67f,-101.73f,66.89f),1f);
            
            _zhentong.GetComponent<DragInjection>().enabled = false;
            _zhentong.GetComponent<UIInputHandler>().enabled = false;
            this.AddTimerEx(1f,()=>
            {
                
                IsZhentongAnimaPlay = true;
                //_mianqianClick.SetActive(false);
                _zhusheClick.GetComponent<UIInputHandler>().enabled = true;
                _zhusheClick.SetActive(true);
            });
            
        }
        if (IsZhentongAnimaPlay)
        {
            IsZhentongAnimaPlay = false;
            
            Debug.LogError("--------------PlayZhusheAnimator--------------");
            zhentong.GetComponent<Animator>().enabled = true;
            this.AddTimerEx(1.5f,()=>
            {
                zhentong.DOLocalMove(new Vector3(-6.03f, -9066.84f, -4739.03f), 1f);
                _zhusheClick.SetActive(false);
                _chuangkoutie.GetComponent<UIInputHandler>().enabled = true;
                InjectionWindow.Instance._chuangkoutie.GetComponent<BreathingEffect>().enabled = true;
                this.AddTimerEx(1f,()=>
                {
                    this._zhentong.Find("Carryout").SetActive(true);
                    AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ledi_yimiao_5);
                });
            });
            
           
        }

    }

    public void OnClickCktAnimator()
    {
        Debug.LogError("---------------CktAnimatorEnter-----------------");
        InjectionWindow.Instance._chuangkoutie.localScale = new Vector3(1,1,1);
        InjectionWindow.Instance._chuangkoutie.GetComponent<BreathingEffect>().enabled = false;
        _chuangkoutie.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-422,73),0.8f);
        _chuangkoutie.DOLocalRotate(new Vector3(10.961f, 0.566f, 156.377f),0.8f);
        _chuangkoutie.DOScale(new Vector3(0.6421643f, 0.6421643f, 0.6421643f), 0.8f);
        this.AddTimerEx(0.81f,()=>
        {
            
            this.AddTimerEx(0.31f, () =>
            {
                _chuangkoutie.SetActive(false);
                HospitalResManager.Instance.Game_VaccineRoot.Find("Role Root/201004000@skin/root/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 L Clavicle/Bip001 L UpperArm/304060304").SetActive(true);
                _chuangkoutie.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,200);
                _chuangkoutie.localScale = new Vector3(1,1,1);
                _chuangkoutie.localRotation = Quaternion.Euler(0,0,90);
                _chuangkoutie.SetActive(true);
                //_chuangkoutie.Find("Carryout").SetActive(true);
                this.AddTimerEx(0.8f,()=>
                {
                    ExitGame();
                });
                
            });
        });
    }


    public void ExitGame()
    {
        HospitalResManager.Instance.Game_VaccineRoot.SetActive(false);
        HospitalResManager.Instance.Game_VaccineRoot.Find("Role Root/201004000@skin/root/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 L Clavicle/Bip001 L UpperArm/304060304").SetActive(false);
        m_draw = HospitalResManager.Instance.Game_VaccineRoot.Find("Role Root/201004000@skin/root/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 L Clavicle/Bip001 L UpperArm/304060301_blue000").GetComponent<Draw>();
        m_draw.Reset();
        HospitalResManager.Instance.Game_VaccineRoot.Find("Role Root/201004000@skin").GetComponent<Animator>().enabled = true;
        HospitalResManager.Instance.Game_VaccineRoot.Find("Camera").localPosition = new Vector3(1.81f,0.848f,-1.297f);
        HospitalResManager.Instance.Game_VaccineRoot.Find("Camera").localRotation = Quaternion.Euler(3.296f,-87.079f,0.168f);
        HospitalResManager.Instance.Acotors.SetActive(true);
        WindowManager.Instance.CloseWindow(WinNames.InjectionPanle);
        //HospitalEventHandler.Instance.ClearAllEvents();
        InjectionController.DestroyInstance();
        WindowManager.Instance.OpenWindow(WinNames.ClinicRoomPanel);
        ClinicRoomController.Instance.OnSmallGameExit();
    }

    public bool RandomNumber()
    {
        int i;
        i = Random.Range(1, 10);
        Debug.LogError(i);
        if (i >= 5)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;
using DG.Tweening;


public class Dragmianqian : MonoBehaviour, IInputUpHandler {

    private Vector3 _TargetScreenSpace;
    private Vector3 _TargetWorldSpace;
    private Transform _battery_transform;
    private Vector3 _MouseScreenSpace;
    private Vector3 _Offset;
    private Camera camera;


    void Awake()
    {
        _battery_transform = transform;
        camera = HospitalResManager.Instance.Game_VaccineRoot.Find("Camera").GetComponent<Camera>();
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, InjectionController.Instance.mianqianjishu);

    }

    public virtual void OnInputUp(InputEventData eventData)
    {
        Debug.LogError("inputup---------------------");
        if (InjectionController.Instance.IsMianqianGameCarryout)
        {
            return;
        }
        this.gameObject.transform.DOLocalMove(new Vector3(182.7207f, -9065.923f, -4737.778f),0.5f);
    }


    IEnumerator OnMouseDown()

    {

        _TargetScreenSpace = camera.WorldToScreenPoint(_battery_transform.position);

        _MouseScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _TargetScreenSpace.z);

        _Offset = _battery_transform.position - camera.ScreenToWorldPoint(_MouseScreenSpace);

        while (Input.GetMouseButton(0))
        {

            _MouseScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _TargetScreenSpace.z);

            _TargetWorldSpace = camera.ScreenToWorldPoint(_MouseScreenSpace) + _Offset;

            _battery_transform.position = _TargetWorldSpace;

            yield return new WaitForFixedUpdate();
        }
    }





}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class InjectionController : Singleton<InjectionController>
{

    public bool _is_talk_finished;
    public bool _on_talking;
    public int MianQianJishu = 0;

    public InjectionModel Model;

    public Transform AiSha;

    public  AnimatorEx AiSha_AnimatorEx;

    public Texture MianqianTexture;

    public bool IsMianqianGameCarryout = false;

    private bool IsOver = false;

    public override void Init()
    {
        
        _is_talk_finished = false;
        AiSha_AnimatorEx = new AnimatorEx(HospitalResManager.Instance.Game_VaccineRoot.GetChild(2).GetChild(0).gameObject);
        AiSha = HospitalResManager.Instance.Game_VaccineRoot.GetChild(2).GetChild(0);
        Model = new InjectionModel();
        Model.xiaoyanyaoOver = false;
        Model.zhentongOver = false;
        Model.chuangkoutieOver = false;
        base.Init();
    }

    public override void UnInit()
    {
        base.UnInit();
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, InjectionController.Instance.mianqianjishu);
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, InjectionController.Instance.OnHoverOverMouse);
    }


    public void StartGame()
    {
        WindowManager.Instance.OpenWindow(WinNames.InjectionPanle);

        HospitalResManager.Instance.Acotors.SetActive(false);
        HospitalResManager.Instance.Game_MainRoot.SetActive(false);
        HospitalResManager.Instance.Game_VaccineRoot.SetActive(true);
    }


    public void OnHoverOverMouse(DragIdentify identify)
    {
        Debug.Log("鼠标到了嘴巴上面");
        if (!_is_talk_finished)
        {
            Debug.LogError("----------语音激励游戏-------------");
            if (!_on_talking)
            {
                _on_talking = true;
                InjectionWindow.Instance._xiaoyanyao.GetComponent<BreathingEffect>().suofangObj.transform.localScale = new Vector3(1,1,1);
                InjectionWindow.Instance._xiaoyanyao.GetComponent<BreathingEffect>().enabled = false;
                string audioPath = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, HospitalConsts.audio_nuowa_buchiyao2);
                //拒绝吃药
                AiSha_AnimatorEx.PlaySequence("all_talk_refuse_01", "game_hospital/game_hospital_audio/404060303", () =>
                {
                    AiSha_AnimatorEx.CrossFade("s_idle", .1f);
                    LeDiTalkManager.Instance.LediMoveIn(() =>
                    {
                        audioPath = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, HospitalConsts.audio_ledi_dazheng_anfu);
                            //乐迪鼓励
                            LeDiTalkManager.Instance.Talk(audioPath, () =>
                        {
                            audioPath = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, HospitalConsts.audio_nuowa_buchiyao1);
                                //爱莎拒绝打针
                                AiSha_AnimatorEx.PlaySequence("all_talk_refuse_01", "game_hospital/game_hospital_audio/404060304", () =>
                            {
                                AiSha_AnimatorEx.CrossFade("s_idle", .1f);
                                //一起加油
                                audioPath = FileTools.CombinePath(HospitalConsts.hospitalAudioBasePath, HospitalConsts.audio_ledi_dazhengguli);
                                LeDiTalkManager.Instance.Talk(audioPath, () =>
                                {

                                    HospitalUtil.WaitSpeak(() =>
                                    {

                                        Debug.Log("角色点头同意");
                                        AiSha_AnimatorEx.CrossFade("s_nod", .25f, () =>
                                        {
                                            AiSha_AnimatorEx.CrossFade("s_idle", .25f);
                                        });
                                        LeDiTalkManager.Instance.LediMoveOut();
                                        _on_talking = false;
                                        _is_talk_finished = true;
                                        TimerManager.Instance.AddTimer(0.5f, () =>
                                        {
                                            TransitionManager.Instance.StartTransition(null, ()=> 
                                            {
                                                HospitalResManager.Instance.Game_VaccineRoot.Find("Camera").DOLocalMove(new Vector3(-0.4f, 0.682f, -0.8f), 0.01f);
                                                HospitalResManager.Instance.Game_VaccineRoot.Find("Camera").rotation = Quaternion.Euler(3.296f, -120.836f, 0.168f);
                                                AiSha.GetComponent<Animator>().enabled = false;
                                                InjectionWindow.Instance._xiaoyanyao.GetComponent<DragInjection>().enabled = false;
                                                InjectionWindow.Instance._xiaoyanyao.GetComponent<UIInputHandler>().enabled = true;
                                                InjectionWindow.Instance._xiaoyanyao.GetComponent<BreathingEffect>().enabled = true;
                                                InjectionWindow.Instance.shoubi.SetActive(true);
                                                AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ledi_yimiao_3);
                                            });
                                            
                                        });


                                    });


                                });
                            });
                        });

                    });
                });
            }


        }

    }

    public string mianqianTextureReplace(string TextureName)
    {
        string Texturename;
        switch (TextureName)
        {
            case "xyys_hongse":
                Texturename = InjectionConsts.mianqian_red;
                return Texturename;
            case "xyys_lanse":
                Texturename = InjectionConsts.mianqian_blue;
                return Texturename;
            case "xyys_zise":
                Texturename = InjectionConsts.mianqian_purple;
                return Texturename;
        }
        return null;
    }

    public string shoubiTextureReplace(string TextureName)
    {
        string Texturename;
        switch (TextureName)
        {
            case "xyys_hongse":
                Texturename = InjectionConsts.shoubi_red000;
                return Texturename;
            case "xyys_lanse":
                Texturename = InjectionConsts.shoubi_blue000;
                return Texturename;
            case "xyys_zise":
                Texturename = InjectionConsts.shoubi_purple000;
                return Texturename;
        }
        return null;
    }

    public string CKTTextureReplace(string TextureName)
    {
        string Texturename;
        switch (TextureName)
        {
            case "ckt_01":
                Texturename = InjectionConsts.chuangkoutie_purple;
                return Texturename;
            case "ckt_02":
                Texturename = InjectionConsts.chuangkoutie_blue;
                return Texturename;
            case "ckt_03":
                Texturename = InjectionConsts.chuangkoutie_greed;
                return Texturename;
            case "ckt_04":
                Texturename = InjectionConsts.chuangkoutie_yellow;
                return Texturename;
            case "ckt_05":
                Texturename = InjectionConsts.chuangkoutie_orange;
                return Texturename;
            case "ckt_06":
                Texturename = InjectionConsts.chuangkoutie_red;
                return Texturename;
        }
        return null;
    }

    public void mianqianjishu(DragIdentify dragIdentify)
    {
        if (DragComparerManager.Instance.IsMatch(InjectionWindow.Instance.mianqian.GetComponent<DragIdentify>()) == false || IsOver)
        {
            return;
        }
        MianQianJishu++;

        //Debug.LogError(MianQianJishu);
        if (MianQianJishu >= 15)
        {
            IsOver = true;
            this.IsMianqianGameCarryout = true;
            InjectionWindow.Instance.CarryoutMianqian();
            InjectionWindow.Instance._zhentong.GetComponent<BreathingEffect>().enabled = true;
            AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" +HospitalConsts.audio_ledi_yimiao_4);

        }
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreathingEffect : MonoBehaviour {

    private float speed = 0.35f;
    private float m_Scale = 1;
    public GameObject suofangObj;

    void Start()
    {
        foreach (Transform item in this.transform)
        {
            if (item.gameObject.activeSelf)
            {
                suofangObj = item.gameObject;
                return;
            }
            if (this.gameObject.name == "chuangkoutie")
            {
                suofangObj = this.gameObject;
            }
        }
    }

    void Update ()
    {
        m_Scale -= Time.deltaTime * speed;
        suofangObj.GetComponent<RectTransform>().localScale = new Vector3(m_Scale, m_Scale, m_Scale);
        if (m_Scale <= 0.9f || m_Scale >= 1.1f)
        {
            speed = -speed;
        }
	}
}

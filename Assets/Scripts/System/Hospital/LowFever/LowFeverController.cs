﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LowFeverController : Singleton<LowFeverController> {

    public LowFeverModel Model;
    public StomachDispen highDispen;
    public StomachVoiceExcitation stomchvoice;
    public Transform GameRoot;
    public Transform AJiaSi;
    public Transform Durg;
    public DragTiwenqiang drag;
    public Cinemachine.CinemachineDollyCart ActorMove;
    public bool IsPlay = true;
    public bool IsPlaying = true;
    public int GameID;

    public override void Init()
    {
        base.Init();
        Model = new LowFeverModel();
        highDispen = new StomachDispen();
        stomchvoice = new StomachVoiceExcitation();
        highDispen._onDispenComplete = this.TakingMedicine;
        drag = HospitalResManager.Instance.Game_LowFeverRoot.Find("TiWenQiang").GetComponent<DragTiwenqiang>();



    }

    public override void UnInit()
    {
        base.UnInit();
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, drag.OnCarryOut);


        stomchvoice.OnLeave();

    }

    public void StartGame(int GameID)
    {
        //this.GameRoot = HospitalResManager.Instance.Game_LowFeverRoot;

       this.GameID = GameID;
        WindowManager.Instance.OpenWindow(WinNames.LowFeventPanel);
        HospitalResManager.Instance.Acotors.Find("AJiasi_Root").SetActive(true);
        PlayAnimation();
        //LowFeverWindow.Instance.OnStartGame(GameID,this.Model.LowFeverTemperatures);
    }

    public void OnUpdate()
    {
       
        if (Input.GetKeyDown(KeyCode.Alpha2) && IsPlaying)
        {
            StartGame(1);
            IsPlaying = false;
        }
        if (Input.GetKeyDown(KeyCode.Alpha8) && IsPlaying)
        {
            StartGame(2);
            IsPlaying = false;
        }

        if (ActorMove != null)
        {
            //Debug.LogError(AJiaSi.rotation.y);
            if (AJiaSi.rotation.y >= 0.46f && IsPlay)
            {
                PlayUIGame();
                HospitalResManager.Instance.m_door.GetComponent<Animator>().Play("doorClose");
                AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_door_open);
                IsPlay = false;
            }
        }
    }

    public void PlayAnimation()
    {
        HospitalResManager.Instance.m_door.GetComponent<Animator>().Play("doorOpen");
        AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_door_open);
        AJiaSi = HospitalResManager.Instance.Acotors.Find("AJiasi_Root");
        ActorMove = AJiaSi.GetComponent<Cinemachine.CinemachineDollyCart>();
        if (this.GameID == 1)
        {
            AJiaSi.GetChild(0).GetComponent<Animator>().Play("walk_lowfever");
        }
        else
        {
            AJiaSi.GetChild(0).GetComponent<Animator>().Play("walk_high_fever");
        }
        ActorMove.enabled = true;

    }

    public void PlayUIGame()
    {
        
        AJiaSi.GetChild(0).GetComponent<Animator>().Play("cli_chair");
        
        
        ActorMove.enabled = false;
        TimerManager.Instance.RemoveAllTimer();
        TimerManager.Instance.AddTimer(1f, () =>
        {
            AJiaSi.DOMove(new Vector3(0.162f, 0.094f, -1.122f), 1f);

        });
        AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ledi_xunwen);
        LowFeverWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ledi_xunwen).length,()=>
        {
            if (this.GameID == 1)
            {
                LowFeverWindow.Instance.AddTimerEx(1f, () =>
                {

                    
                    TimerManager.Instance.AddTimer(1f, () => {
                        AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060201");
                        AJiaSi.GetChild(0).GetComponent<Animator>().Play("talk_sit_lowfever_01");
                        LowFeverWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060201").length, () =>
                        {
                            LowFeverWindow.Instance._LowFevelIc.SetActive(true);
                            AJiaSi.GetChild(0).GetComponent<Animator>().Play("talk_sit_lowfever_03");
                            AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060403");
                            HospitalResManager.Instance.m_lediRoot.GetChild(0).GetComponent<Animator>().Play("Talking");
                            LowFeverWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060403").length, () =>
                            {
                                LowFeverWindow.Instance._ClickImage.SetActive(true);
                                HospitalResManager.Instance.m_lediRoot.GetChild(0).GetComponent<Animator>().Play("EndTalk");
                            });
                        });
                    });
                });
            }
            else
            {
                LowFeverWindow.Instance.AddTimerEx(1f, () =>
                {
                    
                    LowFeverWindow.Instance.AddTimerEx(1f, () => {
                        AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060202");
                        AJiaSi.GetChild(0).GetComponent<Animator>().Play("all_talk_highfever_01");
                        LowFeverWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060202").length, () =>
                        {
                            LowFeverWindow.Instance._HighFevelIc.SetActive(true);
                            AJiaSi.GetChild(0).GetComponent<Animator>().Play("all_talk_highfever_03");
                            AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060404");
                            HospitalResManager.Instance.m_lediRoot.GetChild(0).GetComponent<Animator>().Play("Talking");
                            LowFeverWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060404").length, () =>
                            {
                                LowFeverWindow.Instance._ClickImage.SetActive(true);
                                HospitalResManager.Instance.m_lediRoot.GetChild(0).GetComponent<Animator>().Play("EndTalk");
                            });
                        });
                    });
                });
            }
        });


    }

    public void GameCarryOut()
    {
        HospitalResManager.Instance.Game_MainRoot.SetActive(true);
        HospitalResManager.Instance.Acotors.SetActive(true);
        HospitalResManager.Instance.Game_LowFeverRoot.SetActive(false);
        AJiaSi.position = new Vector3(-0.693f,0,-0.98f);
        AJiaSi.rotation = Quaternion.Euler(0.391f,9.51f,-0.415f);
        AJiaSi.GetChild(0).GetComponent<Animator>().Play("talk");
        if (GameID == 1)
        {
            AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060203");
            LowFeverWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060203").length, () =>
            {
                AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060424");
                HospitalResManager.Instance.m_lediRoot.GetChild(0).GetComponent<Animator>().Play("Talking");
                LowFeverWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060424").length, () =>
                {
                    AjiasiGoBack();
                    HospitalResManager.Instance.m_lediRoot.GetChild(0).GetComponent<Animator>().Play("EndTalk");
                });
            });
        }
        if (GameID == 2)
        {
            
            AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060204");
            LowFeverWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060204").length, () =>
            {
                AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060431");
                HospitalResManager.Instance.m_lediRoot.GetChild(0).GetComponent<Animator>().Play("Talking");
                LowFeverWindow.Instance.AddTimerEx(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060431").length, () =>
                {
                    AjiasiGoBack();
                    HospitalResManager.Instance.m_lediRoot.GetChild(0).GetComponent<Animator>().Play("EndTalk");
                    
                });
            });
        }
        
    }

    public void AjiasiGoBack()
    {
       
        LowFeverWindow.Instance.AddTimerEx(1f,()=>
        {
            AJiaSi.DORotate(new Vector3(0,180,0),0.3f);
            AJiaSi.GetChild(0).GetComponent<Animator>().Play("walk");
            HospitalResManager.Instance.m_door.GetComponent<Animator>().Play("doorOpen");
            LowFeverWindow.Instance.AddTimerEx(0.4f, () =>
            {
                AJiaSi.DOMove(new Vector3(-0.7f, 0, -4.637f), 4f);
            });
        });
        
        LowFeverWindow.Instance.AddTimerEx(4.5f,()=>
        {
            HospitalResManager.Instance.m_door.GetComponent<Animator>().Play("doorClose");
            AJiaSi.GetComponent<Cinemachine.CinemachineDollyCart>().m_Position = 0;
            WindowManager.Instance.CloseWindow(WinNames.LowFeventPanel);
            WindowManager.Instance.OpenWindow(WinNames.ClinicRoomPanel);
            IsPlay = true;
            IsPlaying = true;
            HospitalResManager.Instance.Acotors.Find("AJiasi_Root").SetActive(false);
            LowFeverWindow.Instance.IsPlaying = false;
            
            ClinicRoomController.Instance.OnSmallGameExit();
        });
    }

    public void TakingMedicine()
    {
        WindowManager.Instance.OpenWindow(WinNames.DispenPanel);
        //WindowManager.Instance.OpenWindow(WinNames.StomachachePanel);
        stomchvoice.OnFeverEnter();
        AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ajiasi_chiyao);
        Durg = DispenPanel.Instance._BottomSubRoot;
        foreach (Transform item in Durg)
        {
            item.DestroyComponentEx<DraggableDrug3D>();
            item.AddComponentEx<DragDurg>().enabled = true;
            //item.GetComponent<DragIdentify>().ID = "10002";
        }
        //WindowManager.Instance.CloseWindow(WinNames.StomachachePanel);
    }

    public LowFeverModel lowfevermodel()
    {
        return this.Model;
    }

}

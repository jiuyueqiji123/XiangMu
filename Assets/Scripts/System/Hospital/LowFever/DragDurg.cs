﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
[RequireComponent(typeof(DragIdentify))]
public class DragDurg : UIDrag3DBase {

    Camera _cam;
    Transform ajiasi;
    bool Carryout = false;

    public override void InitComponet()
    {
        base.InitComponet();
        _cam = HospitalResManager.Instance.Game_LowFeverRoot.GetChild(0).GetComponent<Camera>();
        Debug.LogError("---------------AjiaSiAnimatorPlay--------------------");
        
    }
    

    protected override void Awake()
    {
        base.Awake();
        //this.GetComponent<DragIdentify>().ID = "10002";
        ajiasi = HospitalResManager.Instance.Game_LowFeverRoot.Find("Role Root/201003000@skin");
       // OnAnimatorPlay();


    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        this.gameObject.GetComponent<DragIdentify>().ID = "10000";
        ajiasi.GetComponent<Animator>().Play("eat_med_01");
        Debug.LogError(LowFeverController.Instance.lowfevermodel().EatMedTime); 
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, this.OnAnimatorPlay);
        Debug.LogError("-------------------------IsDrag-----------------------------");

    }

    public override Transform _currentDragObject
    {
        get
        {
            Transform transform1 = GetDrugPill(_slot.m_stomachDrugShape, _slot.m_stomachDrugColor).transform;
            transform1.LookAt(HospitalResManager.Instance.Game_LowFeverRoot.GetChild(0).GetComponent<Camera>().transform);
            return transform1;
        }

        set
        {
            base._currentDragObject = value;
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        ajiasi.gameObject.GetComponent<Animator>().Play("eat_med_03");
    }

    

    public override Vector3 Get3DPosition()
    {
        Plane plane = new Plane(HospitalResManager.Instance.Game_LowFeverRoot.GetChild(0).GetComponent<Camera>().transform.forward * -1 + new Vector3(10f, 0, 0), Vector3.zero);
        Ray ray = HospitalResManager.Instance.Game_LowFeverRoot.GetChild(0).GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public void OnAnimatorPlay(DragIdentify identify)
    {
        
        //Debug.LogError(LowFeverController.Instance.AJiaSi.name);
        if (this.Carryout)
        {
            return;
        }
        Debug.LogError("--------------PlayMedAnimator---------");
        this.Carryout = true;
        TimerManager.Instance.AddTimer(1f,()=>
        {
            ajiasi.gameObject.GetComponent<Animator>().Play("eat_med_03");
        });
        
        this.enabled = false;
        this.transform.GetChild(1).SetActive(false);
        _currentDragObject.DOLocalMove(new Vector3(-0.021f,0.9f,-1.26f),0.2f);
        _currentDragObject.DOScale(new Vector3(0,0,0),1f);
        LowFeverController.Instance.lowfevermodel().EatMedTime++;
        if (LowFeverController.Instance.lowfevermodel().EatMedTime == 3)
        {
            LowFeverController.Instance.lowfevermodel().EatMedTime = 0;
            TimerManager.Instance.AddTimer(3f,()=>
            {
                LowFeverController.Instance.GameCarryOut();
                LowFeverWindow.Instance._LifeGroup.SetActive(false);
                LowFeverWindow.Instance._DragCoolingSticker.SetActive(false);

                HospitalResManager.Instance.Game_MainRoot.SetActive(true);
                HospitalResManager.Instance.Acotors.SetActive(true);
                HospitalResManager.Instance.Game_LowFeverRoot.SetActive(false);
                //WindowManager.Instance.CloseWindow(WinNames.LowFeventPanel);
                WindowManager.Instance.CloseWindow(WinNames.DispenPanel);
                HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI,OnAnimatorPlay);
            });
            
        }
    }
    
}

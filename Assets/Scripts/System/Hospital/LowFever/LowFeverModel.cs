﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowFeverModel{

    public int RandomNumber;
    public int EatMedTime = 0;

    public List<string> LowFeverTemperatures = new List<string>() { "38°" ,"38.2°", "38.3°" };
    public List<string> HighFeverTemperatures = new List<string>() { "39°", "39.6°", "39.8°" };

    public List<string> LowFevelSound = new List<string>() { "404060420", "404060421", "404060422" };
    public List<string> HighFevelSound = new List<string>() { "404060425", "404060426", "404060427" };

}

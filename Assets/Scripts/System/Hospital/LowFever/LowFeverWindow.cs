﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LowFeverWindow : SingletonBaseWindow<LowFeverWindow> {

    

    public Transform _LifeGroup { get; set; }
    public Transform _FollowingGroup { get; set; }
    //public Transform _Temperature { get; set; }
    public Transform _Temperature_qipao { get; set; }
    public Transform _WenDuJi { get; set; }
    public Transform _DragCoolingSticker { get; set; }
    public Transform _LowFevelIc { get; set; }
    public Transform _HighFevelIc { get; set; }
    public Transform _ClickImage { get; set; }
    //public Transform _TiWenQiang { get; set; }
    public Transform _Btn_Back { get; set; }

    public LowFeverEventListen Lowfevereventlisten;
    public TemperatureEventListen temperatureeventlisten;
    //public bool IsCarryOut = false;
    public bool IsPlaying = false;

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);
        DragComparerManager.Instance._currentDragAgent = _DragCoolingSticker.GetComponent<RectTransform>();
        WindowManager.Instance.CloseWindow(WinNames.ClinicRoomPanel);
        

    }

    protected override void AddListeners()
    {
        base.AddListeners();
        //temperatureeventlisten = _Temperature.AddComponentEx<TemperatureEventListen>();
        _ClickImage.AddComponentEx<UIInputHandler>().OnClick += OnClickUIGameEnter;
        _Btn_Back.AddComponentEx<UIInputHandler>().OnClick += OnExitGame;
        //_Temperature.AddComponentEx<UIInputHandler>().OnDown += temperatureeventlisten.OnInputDown;
        //_Temperature.GetComponent<UIInputHandler>().OnEnter += temperatureeventlisten.OnInputEnter;


    }


    protected override void RemoveListensers()
    {
        base.RemoveListensers();
        //_Temperature.GetComponent<UIInputHandler>().OnDown -= temperatureeventlisten.OnInputDown;
        //_Temperature.GetComponent<UIInputHandler>().OnEnter -= temperatureeventlisten.OnInputEnter;
        _ClickImage.AddComponentEx<UIInputHandler>().OnClick -= OnClickUIGameEnter;
        _Btn_Back.AddComponentEx<UIInputHandler>().OnClick -= OnExitGame;
       // HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, _Temperature.GetComponent<DragTemperature>().OnCarryOut);
    }


    protected override void OnUpdate()
    {
        base.OnUpdate();
        //if (_Temperature.localPosition.x >= -1153 && _Temperature.localPosition.x <= -850)
        //{
        //    IsCarryOut = true;
        //}
        //if (temperatureeventlisten == null)
        //{
        //    return;
        //}
        //Debug.LogError(temperatureeventlisten.main_Camera.ScreenToWorldPoint(Input.mousePosition));
    }


    public void OnExitGame()
    {
        WindowManager.Instance.CloseWindow(WinNames.LowFeventPanel);
        WindowManager.Instance.CloseWindow(WinNames.DispenPanel);
        LowFeverController.DestroyInstance();
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
        
    }


    public void OnStartGame(int GameId, List<string> temperatures)
    {
        Debug.LogError(GameId);
        switch (GameId)
        {
            case 1: BodyTemperature(temperatures);
                break;
            case 2:
                BodyTemperature(temperatures);
                break;
        }
            
    }

    public int RandomNumber(float Rang)
    {
       LowFeverController.Instance.Model.RandomNumber = (int)Mathf.Floor(Random.Range(0, Rang));
        return LowFeverController.Instance.Model.RandomNumber;
    }


    public void BodyTemperature(List<string> temperatures)
    {
        HospitalResManager.Instance.Game_LowFeverRoot.Find("TiWenQiang").AddComponentEx<DragTiwenqiang>();
        HospitalResManager.Instance.Game_LowFeverRoot.Find("TiWenQiang").GetComponent<BoxCollider>().enabled = true;
        HospitalResManager.Instance.Game_LowFeverRoot.Find("TiWenQiang").localPosition = new Vector3(0.017f,0.817f,-0.001f);
        HospitalResManager.Instance.Game_LowFeverRoot.Find("TiWenQiang").SetActive(true);
        HospitalResManager.Instance.Game_LowFeverRoot.Find("TiWenQiang").DOLocalMove(new Vector3(0.017f,0.817f,-0.44f),0.5f);
        AudioManager.Instance.PlaySound(HospitalConsts.hospitalAudioBasePath + "/" + HospitalConsts.audio_ledi_fashao_1);
       // _Temperature.GetComponent<RectTransform>().DOAnchorPos3D(new Vector3(-103,0,0),0.8f);
        _Temperature_qipao.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = temperatures[RandomNumber(temperatures.Count)];
    }

    

    public void PlayAnimator()
    {

    }



    public void PasteGame()
    {
        _LifeGroup.GetComponent<RectTransform>().DOAnchorPos3D(new Vector3(-141.5f, 0, 0),0.5f);
        //_LifeGroup.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
        //_LifeGroup.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
        //_LifeGroup.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(689.5f, 0, 0);
    }

    public void PasteGameCarryOut(GameObject tuiretie, GameObject etouCollider)
    {
        tuiretie.transform.DORotate(new Vector3(0, 0, 35), 0.2f);
        this.AddTimerEx(0.2f, () =>
         {
             tuiretie.transform.DOLocalMove(Camera.main.WorldToScreenPoint(etouCollider.transform.position) + new Vector3(0, 30, 0), 0.3f);
         });
        this.AddTimerEx(0.5f, () =>
         {
             tuiretie.transform.DOLocalMove(Camera.main.WorldToScreenPoint(etouCollider.transform.position), 0.1f);
         });

    }

    public void CoolingSticker()
    {

    }


    public void OnClickUIGameEnter()
    {

        if (IsPlaying)
        {
            return;
        }
        IsPlaying = true;
        TransitionManager.Instance.StartTransition(null, () =>
        {
            Transform GameRoot;
            _ClickImage.SetActive(false);
            _LowFevelIc.SetActive(false);
            _HighFevelIc.SetActive(false);
            GameRoot = HospitalResManager.Instance.Game_LowFeverRoot;
            HospitalResManager.Instance.Game_MainRoot.SetActive(false);
            HospitalResManager.Instance.Acotors.SetActive(false);
            GameRoot.SetActive(true);
            if (LowFeverController.Instance.GameID == 1)
            {
                LowFeverWindow.Instance.OnStartGame(LowFeverController.Instance.GameID, LowFeverController.Instance.Model.LowFeverTemperatures);
            }
            else
            {
                LowFeverWindow.Instance.OnStartGame(LowFeverController.Instance.GameID, LowFeverController.Instance.Model.HighFeverTemperatures);
            }
        });

        
    }

    public void OnGameCarryOut()
    {
        Transform GameRoot;

        GameRoot = HospitalResManager.Instance.Game_LowFeverRoot;
        _WenDuJi.SetActive(false);
        if (LowFeverController.Instance.GameID ==1)
        {
            LowFeverController.Instance.GameCarryOut();
            _LifeGroup.SetActive(false);
            _DragCoolingSticker.SetActive(false);
            HospitalResManager.Instance.Game_LowFeverRoot.Find("TiWenQiang").GetComponent<BoxCollider>().enabled = false;
            //HospitalResManager.Instance.Game_LowFeverRoot.Find("TiWenQiang").GetComponent<DragTiwenqiang>().StopCoroutine();
            HospitalResManager.Instance.Game_MainRoot.SetActive(true);
            HospitalResManager.Instance.Acotors.SetActive(true);
            GameRoot.SetActive(false);
        }
        else
        {
            _LifeGroup.SetActive(false);
            LowFeverController.Instance.highDispen.OnEnter();
        }
        
        

    }

}

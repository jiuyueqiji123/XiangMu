﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(DragIdentify))]
public class DragCoolingSticker : UIDrag3DBase {

    public Transform TuiShaoTie;
    public Transform tuishaotie;
    

    //public LayerMask laymask;
    [HideInInspector]
    public bool IsCarryOut = false;
    [HideInInspector]
    public bool IsDragging = true;
    public override void OnBeginDrag(PointerEventData eventData)
    {

        base._currentDragObject = this.transform.GetChild(0);
        TuiShaoTie = this.transform.GetChild(0);
        //HospitalResManager.Instance.Game_LowFeverRoot.GetChild(2).Find("Etou");
        //Debug.LogError(HospitalResManager.Instance.Game_LowFeverRoot.GetChild(2).Find("201003000@skin/root/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head/Etou").name);
        base.OnBeginDrag(eventData);
        //Debug.LogError("------------------------------------XXXXXXXXXXXXXXXX");
        //_isOnDragging = true;
        this.gameObject.GetComponent<Image>().color = new Color(1,1,1,0);
        IsCarryOut = true;
        LowFeverWindow.Instance._WenDuJi.SetActive(true);
        LowFeverWindow.Instance._WenDuJi.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1400,0),0.5f);
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnCarryOutCoolingSticker);
        //_currentDragAgent = this.gameObject.GetComponent<RectTransform>();
        

    }

    public override Vector3 Get3DPosition()
    {

        Plane plane = new Plane(HospitalResManager.Instance.Game_LowFeverRoot.GetChild(0).GetComponent<Camera>().transform.forward * -1 + new Vector3(10f, 0, 0), Vector3.zero);
        Ray ray = HospitalResManager.Instance.Game_LowFeverRoot.GetChild(0).GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        
        if (IsCarryOut)
        {
            this.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            TuiShaoTie = null;
        }

    }

    public void OnCarryOutCoolingSticker(DragIdentify identify)
    {
        if (!IsCarryOut)
        {
            
            return;
        }
        this.enabled = false;
        IsCarryOut = false;
        //_isOnDragging = false;
        IsDragging = false;
        //Debug.LogError("11111111111111111111111111111111111111111111111");
        StartCoroutine("CarryOutCoolingSticker");

    }

    IEnumerator CarryOutCoolingSticker()
    {
        
        LowFeverWindow.Instance._DragCoolingSticker.GetComponent<RectTransform>().DOAnchorPos(new Vector2(25,130),0.4f);
        LowFeverWindow.Instance._LifeGroup.GetComponent<RectTransform>().DOAnchorPos3D(new Vector3(141.5f, 0, 0), 0.5f);
        tuishaotie = Instantiate(TuiShaoTie);
        tuishaotie.gameObject.SetActive(false);
        tuishaotie.SetParent(HospitalResManager.Instance.Game_LowFeverRoot.GetChild(2).Find("201003000@skin/root/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head/Etou"));
        tuishaotie.localPosition = new Vector3(-0.9f, 6.5f, -0.29f);
        tuishaotie.localRotation = Quaternion.Euler(-109, 70, 20);
        tuishaotie.localScale = new Vector3(57,57,57);
        this.transform.GetChild(0).DOLocalMove(new Vector3(-718.5f,-9651.89f,-4741.93f),0.2f);
        yield return new WaitForSeconds(0.4f);
        tuishaotie.SetActive(true);
        TuiShaoTie.SetActive(false);
       
        
        LowFeverWindow.Instance._WenDuJi.SetActive(true);

        //LowFeverWindow.Instance._WenDuJi.GetChild(0).DOScale(new Vector3(1,0,1),8f);-600;-210
        LowFeverWindow.Instance._WenDuJi.GetChild(1).GetComponent<RectTransform>().DOAnchorPos3D(new Vector3(0,-630),8f);
        LowFeverWindow.Instance._WenDuJi.GetChild(1).GetChild(0).GetComponent<RectTransform>().DOAnchorPos3D(new Vector3(0, 420), 7.8f);

        yield return new WaitForSeconds(8f);
        DestroyObject(TuiShaoTie.gameObject);
        Destroy(tuishaotie.gameObject);
        LowFeverWindow.Instance.OnGameCarryOut();
        
    }

    
    
    

}

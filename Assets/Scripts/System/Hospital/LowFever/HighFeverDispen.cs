﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;



/// <summary>
/// 匹配
/// </summary>
public class HighFeverDispen : HospitalGameBase
{
    int randomTimes = 3;
    int shapeIndex;
    int colorIndex;
    int _dipenSuccessAmount;    //配药成功的个数

    RectTransform _BottomBar;
    RectTransform _RightBar;

    public List<StomachDrugSlot> stomachDrugs;
    public Dictionary<string, StomachDrugSlot> _drugsCompareDic;


    public override void OnEnter()
    {
        _dipenSuccessAmount = 0;

        _drugsCompareDic = new Dictionary<string, StomachDrugSlot>();

        _BottomBar = StomachachePanle.Instance._BottomBar as RectTransform;
        _BottomBar.anchoredPosition = new Vector2(_BottomBar.anchoredPosition.x, -95);
        StomachachePanle.Instance.BottomBarAnimate(true);
        _BottomBar.SetActive(true);

        _RightBar = StomachachePanle.Instance._RightBar as RectTransform;
        _RightBar.anchoredPosition = new Vector2(-554, _RightBar.anchoredPosition.y);
        StomachachePanle.Instance.RightBarAnimate(true);
        _RightBar.SetActive(true);

        stomachDrugs = new List<StomachDrugSlot>();
        for (int i = 0; i < randomTimes; i++)
        {
            shapeIndex = Random.Range(0, 4);

            StomachDrugSlot drug = new StomachDrugSlot((eStomachDrugShape)shapeIndex, (eStomachDrugColor)i);
            stomachDrugs.Add(drug);
        }

        CreateBottomBarItems();

        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnDragMatch);

    }

    public override void OnLeave()
    {
        HospitalEventHandler.Instance.RemoveEventHandler<DragIdentify>(HospitalEventID.DRAG_MATCH, OnDragMatch);
    }

    public override void OnUpdate()
    {

    }


    public void CreateBottomBarItems()
    {
        StomachDispen stomachDispen = StomachacheGameManager.Instance.stomachDispen;
        List<StomachDrugSlot> stomachDrugs = stomachDispen.stomachDrugs;
        for (int i = 0; i < stomachDrugs.Count; i++)
        {
            StomachDrugSlot _stomachDrug = stomachDrugs[i];

            GameObject item = HospitalResManager.GetActiveObject(HospitalConsts.game_hospital_prefab_stomachache_BasePath, HospitalConsts.buttombarLayoutItem);
            item.transform.SetParent(StomachachePanle.Instance._BottomSubRoot);
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            Transform _child = item.transform.Find("SlotImage");
            item.transform.Find("ForeImage").SetActive(false);
            Image childImg = _child.GetComponent<Image>();
            childImg.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Hospital, _stomachDrug.m_spritename);
            //childImg.SetNativeSize();

            //添加标识
            DragIdentify identy = HospitalHelper<DragIdentify>.GetSafeComponent(item);
            identy.ID = i + "_" + (int)_stomachDrug.m_stomachDrugShape + "_" + (int)_stomachDrug.m_stomachDrugColor;  //形状_颜色
            _stomachDrug.SetOwer(item.transform);

            HoverUI hoverUI = HospitalHelper<HoverUI>.GetSafeComponent(item);

            DraggableDrug3D drugDrag3D = HospitalHelper<DraggableDrug3D>.GetSafeComponent(item);
            drugDrag3D._slot = _stomachDrug;
            drugDrag3D.enabled = false;

            _drugsCompareDic.Add(identy.ID, _stomachDrug);
        }

        //填充药片列表
    }

    //当有拖拽行为匹配上时
    private void OnDragMatch(DragIdentify identify)
    {
        bool ismatch = DragComparerManager.Instance.IsMatch(identify);
        DragIdentify _current_dragIdentify = DragComparerManager.Instance._current_dragIdentify;
        if (_current_dragIdentify == null) return;
        if (ismatch)
        {

            //Debug.Log(_current_dragIdentify.ID);
            string iD = _current_dragIdentify.ID;
            StomachDrugSlot _drugSlot = _drugsCompareDic[iD];
            StomachDrug _drug = new StomachDrug(_drugSlot.m_stomachDrugShape, _drugSlot.m_stomachDrugColor);
            Transform itemTransform = _drugSlot.m_myTransform;
            Transform _itemSlotImage = _drugSlot.m_myTransform.Find("SlotImage");
            Transform itemForeImage = itemTransform.Find("ForeImage");
            Image _itemforgeImage = itemForeImage.GetComponent<Image>();

            itemTransform.GetComponent<HoverUI>().enabled = false;
            _itemforgeImage.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Hospital, _drug.m_spritename);
            //_itemforgeImage.SetNativeSize();
            itemForeImage.SetActive(true);

            _dipenSuccessAmount++;
            if (_dipenSuccessAmount >= 3)
            {
                Debug.Log("所有药片已经全部配好");

                StomachachePanle.Instance.RightBarAnimate(false);

                StomachacheGameManager.Instance.ChangeState(StomachacheGameManager.eStamachGameState.VoiceExcitation);
            }
        }
        else
        {
            Debug.Log("没有匹配上！");
        }
    }

}

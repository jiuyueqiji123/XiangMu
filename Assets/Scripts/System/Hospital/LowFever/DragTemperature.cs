﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using WCBG.ToolsForUnity.Tools;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(DragIdentify))]
public class DragTemperature: Object3dDragBase {

    //public LayerMask laymask;
    [HideInInspector]
    public bool IsCarryOut = false;
    [HideInInspector]
    public bool IsDragging = true;
    public override void OnBeginDrag(PointerEventData eventData)
    {

        //base._currentDragObject = LowFeverWindow.Instance._TiWenQiang;
        base.OnBeginDrag(eventData);
        //Debug.LogError("------------------------------------XXXXXXXXXXXXXXXX");
        //_isOnDragging = true;
        this.gameObject.GetComponent<Image>().color = new Color(1,1,1,0);
        IsCarryOut = true;
        HospitalEventHandler.Instance.AddEventHandler<DragIdentify>(HospitalEventID.ON_HOVER_UI, OnCarryOut);
        
        //Debug.LogError(_currentDragObject);
        //LowFeverWindow.Instance._Temperature.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
        //LowFeverWindow.Instance._Temperature.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);


    }

    //public override void Update()
    //{
    //    if (!IsDragging)
    //    {
    //        Debug.LogError("----------------------------------------------1");
    //        return;

    //    }
    //    base.Update();
    //}

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        IsDragging = false;
        //_isOnDragging = false;
        if (IsCarryOut)
        {
            
            //LowFeverWindow.Instance._Temperature.GetComponent<RectTransform>().anchorMax = new Vector2(1f, 0.5f);
            //LowFeverWindow.Instance._Temperature.GetComponent<RectTransform>().anchorMin = new Vector2(1f, 0.5f);
            //LowFeverWindow.Instance._Temperature.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(-103,0,0);
        }
        //IsCarryOut = false;
        
        //Transform _hitTrans = FTools.GetRaycastHitTargetByMousePoint( LayerMask.NameToLayer("Player") );
        //if (_hitTrans && _hitTrans.name == "Etou")
        //{
        //    Debug.Log("拖到了身上");
        //}

    }

    //protected override void OnTweenBackFinished()
    //{
    //    base.OnTweenBackFinished();
    //    this.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
    //}

    public void OnCarryOut(DragIdentify identify)
    {
        //Debug.LogError("1111111111111111111111111111111111111111111111111111111111-------------------------------");
        if (!IsCarryOut)
        {
            return;
        }
        this.enabled = false;
        IsCarryOut = false;
        //_isOnDragging = false;
        IsDragging = false;
        StartCoroutine("CarryOut");
        
    }

    public override Vector3 Get3DPosition()
    {
       
        Plane plane = new Plane(HospitalResManager.Instance.Game_LowFeverRoot.GetChild(0).GetComponent<Camera>().transform.forward * -1 , new Vector3(0.017f,0.817f,-1.3f));
        Ray ray = HospitalResManager.Instance.Game_LowFeverRoot.GetChild(0).GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        float dis;
        if (plane.Raycast(ray, out dis))
        {
            Vector3 point = ray.GetPoint(dis);
            return point;
        }

        return Vector3.zero;
    }

    IEnumerator CarryOut()
    {
        this.gameObject.transform.DOLocalMove(new Vector3(70, 70, 0), 0.3f);
        //LowFeverWindow.Instance._TiWenQiang.DOLocalMove(new Vector3(0,-9450.79f,-4741.67f),0.3f);
        yield return new WaitForSeconds(2f);
        //Debug.LogError("------------------------------为什么不进来？");
        this.gameObject.transform.DOLocalMove(new Vector3(400, 0, 0), 0.5f);
        //LowFeverWindow.Instance._TiWenQiang.DOLocalMove(new Vector3(0, -9452.28f, -4738.71f), 0.3f);
        yield return new WaitForSeconds(0.5f);
        LowFeverWindow.Instance._Temperature_qipao.SetActive(true);
        //this.enabled = true;
        if (LowFeverController.Instance.GameID == 1)
        {
            AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/" + LowFeverController.Instance.Model.LowFevelSound[LowFeverController.Instance.Model.RandomNumber]);
            yield return new WaitForSeconds(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/" + LowFeverController.Instance.Model.LowFevelSound[LowFeverController.Instance.Model.RandomNumber]).length);
            AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060423");
            yield return new WaitForSeconds(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060423").length);
        }
        else
        {
            AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/" + LowFeverController.Instance.Model.HighFevelSound[LowFeverController.Instance.Model.RandomNumber]);
            yield return new WaitForSeconds(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/" + LowFeverController.Instance.Model.HighFevelSound[LowFeverController.Instance.Model.RandomNumber]).length);
            AudioManager.Instance.PlaySound("game_hospital/game_hospital_audio/404060428");
            yield return new WaitForSeconds(AudioManager.Instance.GetAudioClip("game_hospital/game_hospital_audio/404060428").length);
        }
        
        //LowFeverWindow.Instance._Temperature.gameObject.SetActive(false);
        //LowFeverWindow.Instance._TiWenQiang.SetActive(false);
        LowFeverWindow.Instance._Temperature_qipao.SetActive(false);
        LowFeverWindow.Instance.PasteGame();
    }

    
    
    

}

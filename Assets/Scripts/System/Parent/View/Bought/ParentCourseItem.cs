﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;
using UnityEngine.UI;

public class ParentCourseItem : MonoBehaviour {

    MainItemInfo info;
    Cmd.CourseInfo cInfo;

    public Image Icon;
    public Text NameText;
    public Text PriceText;
    //public Text EnergyNumText;
    public GameObject BuyBtn;
    public GameObject BoughtObj;
    //public GameObject Type4Obj;

    public void SetInfo(MainItemInfo info)
    {
        this.info = info;
        this.cInfo = PayManager.Instance.GetNetInfo((int)this.info.id);

        Icon.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Main, info.image);
        NameText.text = info.name;
        int price;
        if (this.cInfo != null)
        {
            price = this.cInfo.price;
            NameText.text = this.cInfo.name;
            SetBtnState();
        }
        else
        {
            price = this.info.price;
            BuyBtn.SetActive(true);
            BoughtObj.SetActive(false);
        }
        float fprice = price / 100f;
        PriceText.text = fprice.ToString() + "元";
        //if (this.info.type == 4)
        //{
        //    Type4Obj.SetActive(true);
        //    EnergyNumText.text = info.scene_int.ToString();
        //}
        //else
        //{
        //    Type4Obj.SetActive(false);
        //}
        
    }

    private void SetBtnState()
    {
        BuyBtn.SetActive(this.cInfo.is_buy == 2);
        BoughtObj.SetActive(this.cInfo.is_buy == 1);
    }

	public void BuyBtnClick()
    {
        //Debug.LogError(this.info.id + "  ===== id");
        PayManager.Instance.Pay((int)this.info.id, (id, suc) => {
            Debug.Log(id + "," + suc);
            if (id == (int)this.info.id && suc)
            {
                //购买成功
                if (this.cInfo != null)
                {
                    this.cInfo.is_buy = 1;
                    SetBtnState();
                }
                if (this.info.type == 4)
                {
                    //加能量
                    //ExpressDataManager.Instance.AddPower(this.info.scene_int);
                }
            }
        }, false);
        /*PayManager.Instance.Pay((int)this.info.id, (id, suc) => {
            Debug.Log(id + "," + suc);
            if (id == (int)this.info.id && suc)
            {
                //购买成功
                if (this.cInfo != null)
                {
                    this.cInfo.is_buy = 1;
                    SetBtnState();
                }
                if (this.info.type == 4)
                {
                    //加能量
                }
            }
        });*/
    }
}

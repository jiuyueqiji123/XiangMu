﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cmd;

public class ParentRestView : UITabTarget {

    public UIButtonToggle RestToggle;

    public Text RestStartText;
    public Text RestEndText;

    public GameObject RestTimeObj;
    public GameObject RestStartObj;
    public GameObject RestEndObj;

    public ToggleGroup RestStartToggleGroup;
    public ToggleGroup RestEndToggleGroup;
    public ToggleGroup PlayTimeToggleGroup;

    public List<Toggle> RestStartToggles = new List<Toggle>();
    public List<Toggle> RestEndToggles = new List<Toggle>();
    public List<Toggle> PlayTimeToggles = new List<Toggle>();

    private BabyInfo info;

    public override void Init()
    {
        base.Init();
        this.info = UserManager.Instance.GetBabyInfo();
        ReflashRestToggle();
        ReflashRestStartTime();
        ReflashRestEndTime();
        ReflashRestTime();
    }

    public override void Show()
    {
        base.Show();
        RestToggle.ValueChangeAction = OnRestToggleChange;
    }

    public override void Hide()
    {
        base.Hide();
        RestToggle.ValueChangeAction = null;
    }

    #region view
    private void ReflashRestToggle()
    {
        RestToggle.Set(this.info.rest_switch == 1);
    }

    private void ReflashRestStartTime()
    {
        RestStartText.text = this.info.rest_start_time;
    }

    private void ReflashRestEndTime()
    {
        RestEndText.text = this.info.rest_end_time;
    }

    private void ReflashRestTime()
    {
        PlayTimeToggleGroup.SetAllTogglesOff();
        int index = ParentModel.PlayTimeList.IndexOf(this.info.play_finite_time);
        if (index >= 0)
        {
            PlayTimeToggles[index].isOn = true;
        }
    }
    #endregion

    #region Click
    public void RestStartClick()
    {
        RestTimeObj.SetActive(false);
        RestStartObj.SetActive(true);
        RestEndObj.SetActive(false);
        
        int index = ParentModel.RestStartList.IndexOf(this.info.rest_start_time);
        if (index >= 0)
        {
            RestStartToggles[index].isOn = true;
        }
        else
        {
            RestStartToggleGroup.SetAllTogglesOff();
        }
        
    }

    public void RestEndClick()
    {
        RestTimeObj.SetActive(false);
        RestEndObj.SetActive(true);
        RestStartObj.SetActive(false);
        
        int index = ParentModel.RestEndList.IndexOf(this.info.rest_end_time);
        if (index >= 0)
        {
            RestEndToggles[index].isOn = true;
        }
        else
        {
            RestEndToggleGroup.SetAllTogglesOff();
        }
        
    }

    public void RestStartCancelClick()
    {
        RestTimeObj.SetActive(true);
        RestStartObj.SetActive(false);
    }

    public void RestStartOkClick()
    {
        if (RestStartToggleGroup.AnyTogglesOn())
        {
            foreach(Toggle toggle in RestStartToggleGroup.ActiveToggles())
            {
                int t = GeneralUtils.ForceInt(toggle.name);
                this.info.rest_start_time = ParentModel.RestStartList[t - 1];
                ReflashRestStartTime();
                break;
            }
            RestStartCancelClick();
        }
    }

    public void RestEndCancelClick()
    {
        RestTimeObj.SetActive(true);
        RestEndObj.SetActive(false);
    }

    public void RestEndOkClick()
    {
        if (RestEndToggleGroup.AnyTogglesOn())
        {
            foreach (Toggle toggle in RestEndToggleGroup.ActiveToggles())
            {
                int t = GeneralUtils.ForceInt(toggle.name);
                this.info.rest_end_time = ParentModel.RestEndList[t - 1];
                ReflashRestEndTime();
                break;
            }
            RestEndCancelClick();
        }
    }

    public void RestTimeToggleClick(Toggle toggle)
    {
        if (toggle.isOn)
        {
            int t = GeneralUtils.ForceInt(toggle.name);
            this.info.play_finite_time = t;
        }
    }
    #endregion

    #region event
    private void OnRestToggleChange(bool val)
    {
        int t = val ? 1 : 2;
        this.info.rest_switch = t;
    }
    #endregion

}

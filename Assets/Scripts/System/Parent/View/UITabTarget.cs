﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITabTarget : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void Init()
    {

    }

    public virtual void Show()
    {
        if(!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
    }

    public virtual void Hide()
    {
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
    }


}

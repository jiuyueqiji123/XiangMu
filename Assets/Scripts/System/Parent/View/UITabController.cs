﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITabController : MonoBehaviour {

    public List<UITabObject> Tabs = new List<UITabObject>();

    private int currentIndex;

    public void Init(int defaultTab)
    {
        for(int i=0; i<Tabs.Count; i++)
        {
            Tabs[i].SetIndex(this, i);
        }
        currentIndex = defaultTab;
        Tabs[currentIndex].SetSelect(true);

#if SUPER_MY
        Tabs[Tabs.Count - 1].gameObject.SetActive(SDKManager.Instance.IsShowGZH());
#endif
    }

    public void ClickTab(int index)
    {
        if(index != currentIndex)
        {
            Tabs[currentIndex].SetSelect(false);
            currentIndex = index;
            Tabs[currentIndex].SetSelect(true);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UITabObject : MonoBehaviour, IPointerClickHandler
{

    public GameObject UnSelect;

    public GameObject Select;

    public UITabTarget Target;

    public Text Text;

    private int index;

    private UITabController tabController;

    public Color selectColor = new Color32(161, 107, 74, 255);
    public Color unselectColor = new Color32(169, 142, 48, 255);

    public void SetIndex(UITabController tabController, int i)
    {
        this.tabController = tabController;
        this.index = i;
        Init();
    }

    private void Init()
    {
        UnSelect.SetActive(true);
        Select.SetActive(false);
        ChangeTextColor(false);
        Target.Init();
    }

    public void SetSelect(bool enable)
    {
        UnSelect.SetActive(!enable);
        Select.SetActive(enable);
        ChangeTextColor(enable);
        if (enable)
        {
            Target.Show();
        }
        else
        {
            Target.Hide();
        }
    }

    private void ChangeTextColor(bool enable)
    {
        if (enable)
        {
            Text.color = selectColor;
        }
        else
        {
            Text.color = unselectColor;
        }
        
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        this.tabController.ClickTab(this.index);
    }
}
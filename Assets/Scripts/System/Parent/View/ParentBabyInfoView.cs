﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cmd;

public class ParentBabyInfoView : UITabTarget {

    public InputField InputFieldName;
    public Text DesNameText;

    public Text AgeText;
    public Text SexText;

    public GameObject SetAgeBtnObj;
    public GameObject SetSexBtnObj;

    public GameObject SetAgePanel;
    public GameObject SetSexPanel;

    public ToggleGroup AgeToggleGroup;
    public ToggleGroup SexToggleGroup;

    public List<Toggle> AgeToggles = new List<Toggle>();
    public List<Toggle> SexToggles = new List<Toggle>();

    private BabyInfo info;

    public override void Init()
    {
        base.Init();
        info = UserManager.Instance.GetBabyInfo();
        ReflashName();
        ReflashAge();
        ReflashSex();
    }

    public override void Show()
    {
        base.Show();
    }

    public override void Hide()
    {
        base.Hide();
    }

    #region view
    private void ReflashName()
    {
        string name = EncryptUtils.Base64Decrypt(this.info.name);
        InputFieldName.text = name;
        DesNameText.text = name + "，你好啊！魔力乐学欢迎你的到来。";
    }

    private void ReflashAge()
    {
        string age;
        int a =this.info.age_range;
        if (a > 0 && a <= ParentModel.AgeList.Count)
        {
            age = ParentModel.AgeList[a-1];
        }
        else
        {
            age = "年龄设置";
        }
        AgeText.text = age;
    }

    private void ReflashSex()
    {
        string sex = "性别设置";
        int a = this.info.gender;
        if (a > 0 && a <= ParentModel.SexList.Count)
        {
            sex = ParentModel.SexList[a-1];
        }
        else
        {
            sex = "性别设置";
        }
        SexText.text = sex;
    }
    #endregion

    #region Click
    public void ChangeNameEnd(string name)
    {
        if (ParentController.Instance.CheckName(name))
        {
            this.info.name = EncryptUtils.Base64Encrypt(name);
            ReflashName();
        } 
        else
        {
            InputFieldName.text = "";
        }
    }

    public void ChangeAgeBtnClick()
    {
        SetAgeBtnObj.SetActive(false);
        SetSexBtnObj.SetActive(false);
        SetAgePanel.SetActive(true);
       
        int a = this.info.age_range;
        if (a > 0 && a <= ParentModel.AgeList.Count)
        {
            AgeToggles[a - 1].isOn = true;
        }
        else
        {
            AgeToggleGroup.SetAllTogglesOff();
        }
        
    }

    public void ChangeSexBtnClick()
    {
        SetAgeBtnObj.SetActive(false);
        SetSexBtnObj.SetActive(false);
        SetSexPanel.SetActive(true);

        int a = this.info.gender;
        if (a > 0 && a <= ParentModel.SexList.Count)
        {
            SexToggles[a - 1].isOn = true;
        } 
        else
        {
            SexToggleGroup.SetAllTogglesOff();
        }
    }

    public void ChangeAgeCancelBtnClick()
    {
        SetAgeBtnObj.SetActive(true);
        SetSexBtnObj.SetActive(true);
        SetAgePanel.SetActive(false);
    }

    public void ChangeAgeOKBtnClick()
    {
        if (AgeToggleGroup.AnyTogglesOn())
        {
            foreach(Toggle toggle in AgeToggleGroup.ActiveToggles())
            {
                this.info.age_range = GeneralUtils.ForceInt(toggle.name);
                ReflashAge();
                break;
            }
            ChangeAgeCancelBtnClick();
        }
    }

    public void ChangeSexCancelBtnClick()
    {
        SetAgeBtnObj.SetActive(true);
        SetSexBtnObj.SetActive(true);
        SetSexPanel.SetActive(false);
    }

    public void ChangeSexOKBtnClick()
    {
        if (SexToggleGroup.AnyTogglesOn())
        {
            foreach (Toggle toggle in SexToggleGroup.ActiveToggles())
            {
                this.info.gender = GeneralUtils.ForceInt(toggle.name);
                ReflashSex();
                break;
            }
            ChangeSexCancelBtnClick();
        }
    }
    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ParentViewTab
{
    Acount,
    BabyInfo,
    Rest,
    Bought,
    Download,
    Communication,
}

public class ParentView  {

    private const string VIEW_PATH = "ui/ui_parent/ParentForm";

    private HUIFormScript form;

    private UITabController tabController;

    private GameObject BackBtnObj;

    public void OpenForm(ParentViewTab tab)
    {
        this.form = HUIManager.Instance.OpenForm(VIEW_PATH, false);
        this.tabController = this.form.transform.Find("Panel").GetComponent<UITabController>();
        this.tabController.Init((int)tab);

        this.BackBtnObj = this.form.transform.Find("ButtonBack").gameObject;
        HUIUtility.SetUIMiniEvent(this.BackBtnObj, enUIEventType.Click, enUIEventID.Parent_Close_Btn);
    }

    public void CloseForm()
    {
        HUIManager.Instance.CloseForm(form);
    }
}

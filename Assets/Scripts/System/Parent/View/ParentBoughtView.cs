﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class ParentBoughtView : UITabTarget {

    private const string Item_Path = "ui/ui_parent/CourseItem";

    public RectTransform context;

    public override void Init()
    {
        base.Init();

        GameObject go = ResourceManager.Instance.GetResource(Item_Path, typeof(GameObject), enResourceType.UIPrefab).m_content as GameObject;
        List<MainItemInfo> items = PayManager.Instance.GetItems();
        int count = items.Count;
        int row = (count + 1) / 2;
        context.sizeDelta = new Vector2(context.sizeDelta.x, 220f * row + 15f * (row - 1));
        for (int i=0; i<count; i++)
        {
            GameObject itemObj = GameObject.Instantiate(go);
            HUIUtility.SetParent(itemObj.transform, context);
            itemObj.name = "item_" + (i + 1);
            itemObj.GetComponent<ParentCourseItem>().SetInfo(items[i]);
        }
    }

    public override void Show()
    {
        base.Show();
    }

    public override void Hide()
    {
        base.Hide();
    }

    #region Click
    
    #endregion

}

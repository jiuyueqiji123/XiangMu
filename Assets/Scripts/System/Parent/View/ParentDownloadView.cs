﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParentDownloadView : UITabTarget {

    public UITabController tabController;

    public List<ParentDownHandler> DownHandlers = new List<ParentDownHandler>();

    private const string TogglePath = "ui/ui_parent/ToggleDownLoad";

    private Queue<ParentDownToggle> toggleObjs = new Queue<ParentDownToggle>();

    private List<ParentDownToggle> toggleUseList = new List<ParentDownToggle>();

    private List<ParentDownToggle> delToggleList = new List<ParentDownToggle>();

    private List<uint> selectId = new List<uint>();

    private GameObject toggleObj;

    public Color TextSelectColor;

    public Color TextUnSelectColor;

    public Text TextTime;

    public Text TextSize;

    public Text TextDes;

    private bool orderByTime;

    private long allSize;

    private long delSize;

    public override void Init()
    {
        base.Init();
        delToggleList.Clear();
        selectId.Clear();
        tabController.Init((int)emMainType.Adventure);
        InitToggles();
        OrderTimeBtnClick(false);
        CalculateAllSize();
        ChangeDes();
    }

    public override void Show()
    {
        base.Show();
    }

    public override void Hide()
    {
        base.Hide();
    }

    private void InitToggles()
    {
        toggleObj = ResourceManager.Instance.GetResource(TogglePath, typeof(GameObject), enResourceType.UIPrefab).m_content as GameObject;
        for(int i=0; i<10; i++)
        {
            AddToggle();
        }
    }

    private void AddToggle()
    {
        GameObject go = Instantiate(toggleObj);
        go.transform.SetParent(this.transform);
        go.SetActive(false);
        toggleObjs.Enqueue(go.GetComponent<ParentDownToggle>());
    }

    public ParentDownToggle GetToggle()
    {
        if (toggleObjs.Count == 0)
        {
            AddToggle();
        }
        ParentDownToggle toggle = toggleObjs.Dequeue();
        toggleUseList.Add(toggle);
        return toggle;
    }

    public void RecycleAllToggle()
    {
        foreach(ParentDownToggle toggle in toggleUseList)
        {
            toggle.transform.SetParent(this.transform);
            toggle.gameObject.SetActive(false);
            toggleObjs.Enqueue(toggle);
        }
        toggleUseList.Clear();
    }

    private void OnDestroy()
    {
        toggleObjs.Clear();
    }

    #region Click
    public void OrderSizeBtnClick(bool recycle = true)
    {
        if (!orderByTime)
            return;
        orderByTime = false;
        TextSize.color = TextSelectColor;
        TextTime.color = TextUnSelectColor;
        DownloadItemManager.Instance.OrderItemsBySize();
        if (recycle)
        {
            RecycleAllToggle();
        }
        InitToggleItems();
    }

    public void OrderTimeBtnClick(bool recycle = true)
    {
        if (orderByTime)
            return;
        orderByTime = true;
        TextSize.color = TextUnSelectColor;
        TextTime.color = TextSelectColor ;
        DownloadItemManager.Instance.OrderItemsByTime();
        if (recycle)
        {
            RecycleAllToggle();
        }
        InitToggleItems();
    }

    private void InitToggleItems()
    {
        foreach (ParentDownToggle toggle in delToggleList)
        {
            selectId.Add(toggle.Data.Id);
        }
        int index = 0;
        for (int i = 0; i < DownHandlers.Count; i++)
        {
            index = i;
            if (i == 3)
            {
                index = 4; //英语的id为5
            }
            DownHandlers[i].Init(this, DownloadItemManager.Instance.Items((emMainType)index));
        }
        selectId.Clear();
    }

    private void CalculateAllSize()
    {
        allSize = 0;
        foreach (ParentDownToggle toggle in toggleUseList)
        {
            allSize += toggle.Data.Size;
        }
    }

    private void CalculateDelSize()
    {
        delSize = 0;
        foreach (ParentDownToggle toggle in delToggleList)
        {
            delSize += toggle.Data.Size;
        }
    }

    public void ToggleValueChange(ParentDownToggle toggle)
    {
        bool frag = false;
        if (toggle.DelToggle.isOn)
        {
            delToggleList.Add(toggle);
            frag = true;
        }
        else
        {
            if (delToggleList.Contains(toggle))
            {
                delToggleList.Remove(toggle);
                frag = true;
            }
        }
        if (frag)
        {
            CalculateDelSize();
            ChangeDes();
        }
    }

    private void ChangeDes()
    {
        this.TextDes.text = "共" + GeneralUtils.SizeFormat(GeneralUtils.SizeBToMB(allSize)) + "M，将删除" + GeneralUtils.SizeFormat(GeneralUtils.SizeBToMB(delSize)) + "M" ;
    }

    public void DeleteItem(ParentDownToggle toggle)
    {
        DownloadItemManager.Instance.DelDownloadItem(toggle.Data);
        if (delToggleList.Contains(toggle))
        {
            delToggleList.Remove(toggle);
        }
        RecycleAllToggle();
        InitToggleItems();
        CalculateAllSize();
        CalculateDelSize();
        ChangeDes();
    }

    public bool IsDeleteSelect(uint id)
    {
        return selectId.Contains(id);
    }

    public void DeleteBtnClick()
    {
        foreach (ParentDownToggle toggle in delToggleList)
        {
            DownloadItemManager.Instance.DelDownloadItem(toggle.Data);
        }
        delToggleList.Clear();
        RecycleAllToggle();
        InitToggleItems();
        CalculateAllSize();
        CalculateDelSize();
        ChangeDes();
        DownloadItemManager.Instance.Save();
    }
    #endregion

}

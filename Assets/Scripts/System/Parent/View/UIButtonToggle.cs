﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIButtonToggle : MonoBehaviour, IPointerClickHandler
{
    public bool IsOn;
    public GameObject OnObj;
    public GameObject OffObj;
    public Transform Trigger;
    public Action<bool> ValueChangeAction;

    public void Set(bool ison)
    {
        //Debug.LogError(name + "," + ison);
        this.IsOn = ison;
        Reflash();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        IsOn = !IsOn;
        Reflash();
        if(ValueChangeAction != null)
        {
            ValueChangeAction(IsOn);
        }
    }

    private void Reflash()
    {
        OnObj.SetActive(IsOn);
        OffObj.SetActive(!IsOn);
        if (IsOn)
        {
            Trigger.localPosition = new Vector3(80f, 0f, 0f);
        }
        else
        {
            Trigger.localPosition = new Vector3(-80f, 0f, 0f);
        }
    }
}

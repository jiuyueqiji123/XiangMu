﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ParentAccountView : UITabTarget {

    public GameObject AccountObj;

    public GameObject LoginObj;

    public GameObject LogOutObj;

    public Text WechatName;

    public Text PhoneNum;

    public Text CodeTip;

    public Text CodeCountDown;

    public InputField PhoneInput;

    public InputField CodeInput;

    private bool loginState;

    private string phone;

    

    public override void Init()
    {
        base.Init();
        ReflashLogoutBtn();
        ReflashWechat();
        ReflashPhone();
        ChangeCodeTip("");

        AccountObj.SetActive(true);
        LoginObj.SetActive(false);
    }

    public override void Show()
    {
        base.Show();
        Debug.Log(emUserInfo.WechatName.ToString());
        ChangeCodeVerity();
        EventBus.Instance.AddEventHandler(EventID.USER_INFO_CHANGE + emUserInfo.WechatName.ToString(), this.OnWechatNameChange);
        EventBus.Instance.AddEventHandler(EventID.USER_INFO_CHANGE + emUserInfo.Phone.ToString(), this.OnPhoneChange);
        EventBus.Instance.AddEventHandler(EventID.PHONE_LOGIN_SUC, this.OnPhoneLoginSuc);
        EventBus.Instance.AddEventHandler<string>(EventID.PHONE_CODE_VARIFY, this.OnPhoneVarigyFail);
        EventBus.Instance.AddEventHandler(EventID.VERITY_COUNT_DOWN, this.OnVerityCountDown);
    }

    public override void Hide()
    {
        base.Hide();
        EventBus.Instance.RemoveEventHandler(EventID.USER_INFO_CHANGE + emUserInfo.WechatName.ToString(), this.OnWechatNameChange);
        EventBus.Instance.RemoveEventHandler(EventID.USER_INFO_CHANGE + emUserInfo.Phone.ToString(), this.OnPhoneChange);
        EventBus.Instance.RemoveEventHandler(EventID.PHONE_LOGIN_SUC, this.OnPhoneLoginSuc);
        EventBus.Instance.RemoveEventHandler<string>(EventID.PHONE_CODE_VARIFY, this.OnPhoneVarigyFail);
        EventBus.Instance.RemoveEventHandler(EventID.VERITY_COUNT_DOWN, this.OnVerityCountDown);
    }

    #region event
    private void OnWechatNameChange()
    {
        LogOutObj.SetActive(true);
        loginState = true;
        ReflashWechat();
    }

    private void OnPhoneChange()
    {
        LogOutObj.SetActive(true);
        loginState = true;
        ReflashPhone();
    }

    private void OnPhoneLoginSuc()
    {
        BackLoginClick();
        OnPhoneChange();
    }

    private void OnPhoneVarigyFail(string msg)
    {
        ChangeCodeTip(msg);
    }

    private void OnVerityCountDown()
    {
        ChangeCodeVerity();
    }
    #endregion

    #region view
    private void ReflashWechat()
    {
        string wechatName = UserManager.Instance.GetInfo(emUserInfo.WechatName);
        if (string.IsNullOrEmpty(wechatName) || !loginState)
        {
            WechatName.text = "";
        }
        else
        {
            WechatName.text = wechatName;
        }
    }

    private void ReflashPhone()
    {
        string phone = UserManager.Instance.GetInfo(emUserInfo.Phone);
        if (string.IsNullOrEmpty(phone) || !loginState)
        {
            PhoneNum.text = "未绑定 >";
        }
        else
        {
            PhoneNum.text = phone;
        }
    }

    private void ReflashLogoutBtn()
    {
        if (UserManager.Instance.HasInfo(emUserInfo.OpenUUID) || UserManager.Instance.HasInfo(emUserInfo.Phone))
        {
            LogOutObj.SetActive(true);
            loginState = true;
        } 
        else
        {
            LogOutObj.SetActive(false);
            loginState = false;
        }
    }

    private void ChangeCodeTip(string msg)
    {
        this.CodeTip.text = msg;
    }

    public void ChangeCodeCountDown(string msg)
    {
        this.CodeCountDown.text = msg;
    }

    private void ChangeCodeVerity()
    {
        ParentController controller = ParentController.Instance;
        if (controller.CountNum <= 0)
        {
            ChangeCodeCountDown("获取验证码");
        }
        else
        {
            ChangeCodeCountDown(controller.CountNum + "秒后重试");
        }
    }
    #endregion

    #region click
    public void WechatClick()
    {
        if (UserManager.Instance.HasInfo(emUserInfo.OpenUUID) && loginState)
            return;
        //调用微信登录
        SDKManager.Instance.WXLogin();
        
    }

    public void PhoneClick()
    {
        if (UserManager.Instance.HasInfo(emUserInfo.Phone) && loginState)
        {
            return;
        }
        AccountObj.SetActive(false);
        LoginObj.SetActive(true);
    }

    public void LogoutClick()
    {
        LogOutObj.SetActive(false);
        loginState = false;
        ReflashWechat();
        ReflashPhone();
    }

    public void GetCodeClick()
    {
        if (!ParentController.Instance.IsCountDone)
        {
            return;
        }
        string p = PhoneInput.text;
        if(System.Text.RegularExpressions.Regex.IsMatch(p, @"^[1]+\d{10}"))
        {
            phone = p;
        } else
        {
            phone = string.Empty;
        }
        if (!string.IsNullOrEmpty(phone))
        {
            SDKManager.Instance.GetVerityCode(phone);
            ParentController.Instance.StartVerityCount();
            ChangeCodeVerity();
        } 
        else
        {
            ChangeCodeTip("手机号不正确！");
        }
    }

    public void LoginClick()
    {
        if (!string.IsNullOrEmpty(phone))
        {
            SDKManager.Instance.AuthVerityCode(phone, CodeInput.text);
        }
    }

    public void BackLoginClick()
    {
        AccountObj.SetActive(true);
        LoginObj.SetActive(false);
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParentDownHandler : MonoBehaviour {

    public RectTransform content;

    private ParentDownloadView view;

    private VerticalLayoutGroup group;

	public void Init(ParentDownloadView v, List<DownloadItemData> datas)
    {
        this.view = v;
        content.sizeDelta = new Vector2(content.sizeDelta.x, 136f * datas.Count + 15f * (datas.Count - 1));
        for(int i=0; i<datas.Count; i++)
        {
            ParentDownToggle toggle = this.view.GetToggle();
            HUIUtility.SetParent(toggle.transform, this.content);
            toggle.name = "Toggle_" + (i + 1);
            toggle.Init(v, datas[i]);
            toggle.gameObject.SetActive(true);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;

public class ParentDownToggle : MonoBehaviour {

    public Toggle DelToggle;
    public Image Icon;
    public Text NearUseText;
    public Text SizeText;
    public Text NameText;

    private ParentDownloadView view;
    private DownloadItemData data;

    public void Init(ParentDownloadView v, DownloadItemData d)
    {
        this.view = v;
        this.data = d;
        DelToggle.isOn = this.view.IsDeleteSelect(this.data.Id);
        MainItemInfo info = MainController.Instance.GetItem(d.Id);
        if (info != null) {
            Icon.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.Main, info.image);
            NameText.text = info.name;
        }
        SizeText.text = GeneralUtils.SizeFormat(GeneralUtils.SizeBToMB(d.Size)) + "M";
        NearUseText.text = TimeUtils.ToDayAgo(d.UseTime) + "天前";
    }

    public void ToggleChange()
    {
        this.view.ToggleValueChange(this);
    }

    public DownloadItemData Data
    {
        get
        {
            return this.data;
        }
    }

    public void DeleteBtnClick()
    {
        this.view.DeleteItem(this);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class ParentController : Singleton<ParentController> {

    private ParentView view;

    private int countDownSeq = -1;

    private int countDownNum = 0;

    public override void Init()
    {
        base.Init();
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Parent_Close_Btn, this.OnCloseBtn);
        this.view = new ParentView();
    }

    public override void UnInit()
    {
        base.UnInit();
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Parent_Close_Btn, this.OnCloseBtn);
    }

    public void OpenView(ParentViewTab tab)
    {
        PayManager.Instance.GetCourse();
        this.view.OpenForm(tab);
        MainAnimatorController.Instance.OpenScrennUI();
    }

    public void CloseView()
    {
        this.view.CloseForm();
        UserManager.Instance.UploadBabyInfo();
        MainAnimatorController.Instance.CloseScreenUI();
    }

    private void OnCloseBtn(HUIEvent evt)
    {
        this.CloseView();
    }

    public bool CheckName(string name)
    {
        Dictionary<uint, SensitiveWord> dict = TableProtoLoader.SensitiveWordDict;
        foreach(SensitiveWord n in dict.Values)
        {
            if (name.IndexOf(n.word) > -1)
            {
                return false;
            }
        }
        return true;
    }

    public bool IsCountDone
    {
        get
        {
            return this.countDownNum <= 0;
        }
    }

    public int CountNum
    {
        get
        {
            return this.countDownNum;
        }
    }

    public void StartVerityCount()
    {
        this.countDownNum = 60;
        this.countDownSeq = TimerManager.Instance.AddTimer(1000, 60, VerityCountSeq);
    }

    private void VerityCountSeq(int seq)
    {
        this.countDownNum--;
        EventBus.Instance.BroadCastEvent(EventID.VERITY_COUNT_DOWN);
    }
}

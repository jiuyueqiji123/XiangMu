﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentModel {

    public static List<string> AgeList = new List<string>()
    {
        "1—2周岁", "2—3周岁", "3—4周岁", "4—5周岁", "5—6周岁", "6+ 周岁", 
    };

    public static List<string> SexList = new List<string>()
    {
        "小王子", "小公主", 
    };

    public static List<int> PlayTimeList = new List<int>()
    {
        15, 30, 45, 60,
    };

    public static List<string> RestStartList = new List<string>()
    {
        "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00",
    };

    public static List<string> RestEndList = new List<string>()
    {
        "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00",
    };
}

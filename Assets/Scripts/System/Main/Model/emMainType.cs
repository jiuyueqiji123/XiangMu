﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum emMainType {
    Adventure,
    Game,
    StudyMath,
    Express,
    StudyEnglish,
    Paint,
}

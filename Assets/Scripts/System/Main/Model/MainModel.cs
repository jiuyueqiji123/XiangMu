﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

/// <summary>
/// 数据保存和处理，数据变化发消息
/// </summary>
public class MainModel
{

    public static List<string> ItemChildImages = new List<string>()
    {
        "冒险", "基地", "数学", "", "英语", ""
    };

    private Dictionary<emMainType, List<MainItemInfo>> itemInfos = new Dictionary<emMainType, List<MainItemInfo>>();

    public MainModel()
    {
        foreach(MainItemInfo info in TableProtoLoader.MainItemInfoDict.Values)
        {

#if UNITY_IOS
            if (info.show_ios == 0)
            {
                continue;
            }
#else
            if (info.show == 0)
            {
                continue;
            }
#endif

            emMainType type = (emMainType)(info.type - 1);
            if (!itemInfos.ContainsKey(type))
            {
                itemInfos.Add(type, new List<MainItemInfo>());
            }
            itemInfos[type].Add(info);
        }
        OrderItems();
    }

    private void OrderItems()
    {
        foreach (List<MainItemInfo> items in itemInfos.Values)
        {
            items.Sort(delegate (MainItemInfo x, MainItemInfo y)
            {
                return x.index.CompareTo(y.index);
            });
        }
    }

    public List<MainItemInfo> GetItems(emMainType type)
    {
        List<MainItemInfo> rets = new List<MainItemInfo>();
        itemInfos.TryGetValue(type, out rets);
        return rets;
    }

    public MainItemInfo GetItemInfo(uint id)
    {
        MainItemInfo info;
        if (TableProtoLoader.MainItemInfoDict.TryGetValue(id, out info))
        {
            return info;
        }
        return null;
    }
}

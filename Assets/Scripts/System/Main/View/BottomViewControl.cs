﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomViewControl : MonoBehaviour {

    public RectTransform Panel;

    public void Show()
    {
        Panel.DOAnchorPosY(0f, 0.5f).SetEase(Ease.Flash);
    }

    public void Hide()
    {
        Panel.DOAnchorPosY(-250f, 0.5f);
    }

    public void Init()
    {
        Panel.anchoredPosition = Vector3.zero;
    }


}

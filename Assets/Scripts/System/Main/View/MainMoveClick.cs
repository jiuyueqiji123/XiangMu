﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainMoveClick : MonoBehaviour, IPointerClickHandler
{
    private const float MAX = 150f;
    private float ratio = 0.5626f;
    private float minRatio = 0.4618f;
    private float maxCanClick = MAX;

    public bool isActive = true;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!isActive)
            return;

        Vector3 pos = eventData.pointerCurrentRaycast.worldPosition;
        PoolObjHandle<ActorRoot> actor = ActorObjManager.Instance.GetActor(ActorNames.LeDi);
        //Debug.LogError(pos.x);
        //Debug.LogError(pos.z);
        //Debug.LogError(LimitXPosition(pos).x);
        actor.handle.ClickFloorPosition(LimitXPosition(pos));
    }

    public void SetActive(bool val)
    {
        isActive = val;
    }

    // Use this for initialization
    void Start () {
        ratio = Framework.Instance.ScreenRatio;
        maxCanClick = (1 - ratio) / (1f - minRatio) * MAX;
        //Debug.LogError(ratio);
        //Debug.LogError(maxCanClick);
	}

    private Vector3 LimitXPosition(Vector3 pos)
    {
        if (pos.x > 0f && pos.x > maxCanClick)
        {
            pos.x = maxCanClick - pos.z/3f;
        }
        else if(pos.x < 0f && pos.x < -maxCanClick)
        {
            pos.x = -maxCanClick + pos.z/3f;
        }
        return pos;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

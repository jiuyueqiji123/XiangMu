﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockAllGiftControl : MonoBehaviour {

    public GameObject Img_Text;
    public Button button_unlock;

    private void Start()
    {
        Img_Text.SetActive(SDKManager.Instance.isGiftText());
    }

    private void OnEnable()
    {
        button_unlock.onClick.AddListener(OnUnlockButtonClick);
    }

    private void OnDisable()
    {
        button_unlock.onClick.RemoveListener(OnUnlockButtonClick);
    }

    public void OnUnlockButtonClick()
    {
        gameObject.SetActive(false);

        //解锁后续操作
        Debug.LogError("购买礼包");
    }
}

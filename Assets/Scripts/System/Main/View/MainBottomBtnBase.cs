﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBottomBtnBase : MonoBehaviour {

	protected GameObject Back;

	protected virtual void Awake() {
		Back = transform.Find ("Back").gameObject;
		CloseChild ();
	}

	public virtual void OpenChild() {
		Back.SetActive (true);
	}

	public virtual void CloseChild() {
		Back.SetActive (false);
	}
}

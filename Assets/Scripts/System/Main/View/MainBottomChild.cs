﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;

public class MainBottomChild : MonoBehaviour {

    public RectTransform Panel;

    public RectTransform Content;

    public Text ItemImage;

    public ScrollRect scroll;

    public RectTransform viewPort;

    private List<MainItemChild> Childs = new List<MainItemChild>();

    private HorizontalLayoutGroup layout;

    private bool isViewPortDown;

    private void Awake()
    {
        layout = Content.GetComponent<HorizontalLayoutGroup>();
    }

    private void MainBottomChild_OnDown()
    {
        isViewPortDown = true;
    }

    public void InitPosition()
    {
        //Panel.anchoredPosition = new Vector2(0f, -375f);
    }

    public void Show(emMainType type)
    {
        gameObject.SetActive(true);
        ItemImage.text = MainModel.ItemChildImages[(int)type];
        InitChild();
        List<MainItemInfo> tables = MainController.Instance.GetItems(type);
        int num = tables.Count;
        
        Content.sizeDelta = new Vector2(483f * num + 76f * (num), Content.sizeDelta.y);
        for (int i=0; i< Childs.Count; i++)
        {
            if (i < num)
            {
                Childs[i].gameObject.SetActive(true);
                Childs[i].Init(tables[i]);
            }
            else
            {
                Childs[i].gameObject.SetActive(false);
            }
        }
        
        if (num > 3)
        {
            layout.childAlignment = TextAnchor.MiddleLeft;
            scroll.horizontalNormalizedPosition = 1;
            StartCoroutine(Fade());
        }
        else
        {
            layout.childAlignment = TextAnchor.MiddleCenter;
        }
        //Debug.LogError(layout.childAlignment);
        //Panel.DOAnchorPosY(-45f, 0.5f).SetEase(Ease.Flash);
    }

    IEnumerator Fade()
    {
        isViewPortDown = false;
        yield return new WaitForSeconds(0.5f);

        viewPort.AddComponentEx<UIInputHandler>().OnDown -= MainBottomChild_OnDown;
        viewPort.AddComponentEx<UIInputHandler>().OnDown += MainBottomChild_OnDown;
        float val = 1;
        while (true)
        {
            //if (preVal != scroll.horizontalNormalizedPosition)
            //    yield break;

            val -= Time.deltaTime * 2;
            if (val <= 0)
            {
                val = 0;
                isViewPortDown = true;
            }
            scroll.horizontalNormalizedPosition = val;
            if (isViewPortDown)
            {
                viewPort.AddComponentEx<UIInputHandler>().OnDown -= MainBottomChild_OnDown;
                viewPort.DestroyComponentEx<UIInputHandler>();
                yield break;
            }
            yield return null;
        }

    }

    private void InitChild()
    {
        if (Childs.Count == 0)
        {
            GameObject go = ResourceManager.Instance.GetResource("main_controller/main_controller_ui/MainItemChild", typeof(GameObject), enResourceType.UIPrefab).m_content as GameObject;
            for (int i = 0; i < 8; i++)
            {
                GameObject item = Instantiate(go);
                HUIUtility.SetParent(item.transform, Content);
                item.name = "item_" + i;
                Childs.Add(item.GetComponent<MainItemChild>());
            }
        }
    }

    public void Hide()
    {
        //Panel.DOAnchorPosY(-375f, 0.5f);
        gameObject.SetActive(false);
    }

    public void HideBtnClick()
    {
        CommonSound.PlayClick();
        MainController.Instance.ChildBtnHile();
    }

}

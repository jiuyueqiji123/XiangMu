﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enMainViewWidget {
	Button_Adventure,
	Button_Game,
	Button_StudyMath,
	Button_Express,
	Button_Parent,
    Button_StudyEnglish,
    Button_Paint,
    Button_Gift,
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// View用来管理窗口或者其它视图的显示，监听用户输入，然后执行Controller中的逻辑。
/// Controller可以改变view的显示
/// view最好不要引用model对象
/// </summary>
public class MainView {

	private const string VIEW_PATH = "main_controller/main_controller_ui/MainForm";

	private HUIFormScript form;

	private GameObject buttonAdventure;

	private GameObject buttonGame;

	private GameObject buttonStudyMath;

    private GameObject buttonStudyEnglish;

    private GameObject buttonPaint;

    private GameObject buttonExpress;

	private GameObject buttonAR;

	private GameObject buttonParent;

    private GameObject buttonRadio;

	private BottomViewControl BottomControl;

    private MainBottomChild BottomChild;

    private GameObject buttonGift;

    //private GameObject buttonWalk;

    //private GameObject buttonStand;

    //private Text textGold;

    //private GameObject buttonChangeGold;

    public void OpenForm() {
		form = HUIManager.Instance.OpenForm (VIEW_PATH, true);
		this.InitForm ();
	}

	public void CloseForm() {
		HUIManager.Instance.CloseForm (form);
		buttonAdventure = null;
	}

	private void InitForm() {
		buttonAdventure = form.GetWidget ((int)enMainViewWidget.Button_Adventure);
		HUIUtility.SetUIMiniEvent (buttonAdventure, enUIEventType.Click, enUIEventID.Main_Btn_Adventure_Click);

		//this.textGold = form.GetWidget ((int)enMainViewWidget.Text_Gold).GetComponent<Text> ();

		//this.buttonChangeGold = form.GetWidget ((int)enMainViewWidget.Button_AddGold);
		//HUIUtility.SetUIMiniEvent (buttonChangeGold, enUIEventType.Click, enUIEventID.Main_Btn_GoldChange_Click);

		this.buttonGame = form.GetWidget ((int)enMainViewWidget.Button_Game);
		HUIUtility.SetUIMiniEvent (buttonGame, enUIEventType.Click, enUIEventID.Main_Btn_Game_Click);

        this.buttonGift = form.GetWidget((int)enMainViewWidget.Button_Gift);
        HUIUtility.SetUIMiniEvent(buttonGift, enUIEventType.Click, enUIEventID.Main_Btn_Gift_Click);

        this.buttonStudyMath = form.GetWidget ((int)enMainViewWidget.Button_StudyMath);
		HUIUtility.SetUIMiniEvent (buttonStudyMath, enUIEventType.Click, enUIEventID.Main_Btn_Study_Math_Click);

        this.buttonStudyEnglish = form.GetWidget((int)enMainViewWidget.Button_StudyEnglish);
        HUIUtility.SetUIMiniEvent(buttonStudyEnglish, enUIEventType.Click, enUIEventID.Main_Btn_Study_English_Click);

        this.buttonPaint = form.GetWidget((int)enMainViewWidget.Button_Paint);
        HUIUtility.SetUIMiniEvent(buttonPaint, enUIEventType.Click, enUIEventID.Main_Btn_Paint_Click);

        this.buttonExpress = form.GetWidget ((int)enMainViewWidget.Button_Express);
		HUIUtility.SetUIMiniEvent (buttonExpress, enUIEventType.Click, enUIEventID.Main_Btn_Express_Click);

        //this.buttonAR = form.GetWidget ((int)enMainViewWidget.Button_AR);
        //HUIUtility.SetUIMiniEvent (buttonAR, enUIEventType.Click, enUIEventID.Main_Btn_AR_Click);

        this.buttonRadio = form.GetWidget("Button_Radio");
        HUIUtility.SetUIMiniEvent(buttonRadio, enUIEventType.Click, enUIEventID.Main_Btn_Radio_Click);
#if UNITY_IOS
        buttonRadio.gameObject.SetActive(false);
#endif

        this.buttonParent = form.GetWidget ((int)enMainViewWidget.Button_Parent);
		HUIUtility.SetUIMiniEvent (buttonParent, enUIEventType.Click, enUIEventID.Main_Btn_Parent_Click);

        this.buttonPaint = form.GetWidget((int)enMainViewWidget.Button_Paint);
        HUIUtility.SetUIMiniEvent(buttonPaint, enUIEventType.Click, enUIEventID.Main_Btn_Paint_Click);

        BottomControl = form.transform.Find("PanelBottom").GetComponent<BottomViewControl>();
        BottomControl.Init();

        BottomChild = form.transform.Find("PanelBottomChild").GetComponent<MainBottomChild>();
        BottomChild.InitPosition();

        //this.buttonWalk = form.transform.Find ("Button_Walk").gameObject;
        //HUIUtility.SetUIMiniEvent (buttonWalk, enUIEventType.Click, enUIEventID.Main_Btn_Walk_Click);

        //this.buttonStand = form.transform.Find ("Button_Stand").gameObject;
        //HUIUtility.SetUIMiniEvent (buttonStand, enUIEventType.Click, enUIEventID.Main_Btn_Stand_Click);

        //Image t = form.transform.Find ("Image_altas_test1").GetComponent<Image>();
        //t.sprite = UISpriteManager.Instance.GetSprite (emUIAltas.Main, "303020101_red_a_5");
    }

    public void BlockInput()
    {
        GraphicRaycaster gr =  this.form.GetGraphicRaycaster();
        if (gr!=null)
        {
            gr.enabled = false;
        }
    }

	public void ClickBottomBtn(emMainType type) {
        //BottomControl.Hide();
        ToyBoxManager.Instance.isWork = false;
        BottomChild.Show(type);
	}

    public void BottomControlBack()
    {
        //BottomControl.Show();
        ToyBoxManager.Instance.isWork = true;
        BottomChild.Hide();
    }

    public void BottomControlHideAndShowArrow()
    {
        BottomControl.Hide();
    }

    public void ButtomControlShowAndHideArrow()
    {
        BottomControl.Show();
    }

    public void CloseButtonRadio()
    {
        this.buttonRadio.SetActive(false);
    }

    public void OpenButtonRadio()
    {
        this.buttonRadio.SetActive(true);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 需要发射线的摄像头需要绑定InputPhysicsRaycaster或者InputPhysics2DRaycaster
/// 当前脚本需要绑定到有collider的对象上
/// </summary>
public class TestInput : MonoBehaviour, IInputDownHandler, IInputUpHandler, IInputEnterHandler, IInputExitHandler, IInputClickHandler, IInputDragHandler, IInputBeginDragHandler, IInputEndDragHandler {

	public virtual void OnInputDown (InputEventData eventData) {
		Debug.LogWarning (gameObject.name +  " OnInputDown");

        //EventBus.Instance.BroadCastEvent(EventID.ON_SCENEGAMEBODY_CLICK, int.Parse(gameObject.name));
	}

	public virtual void OnInputUp (InputEventData eventData) {
		Debug.LogWarning (gameObject.name +  " OnInputUp");
	}

	public virtual void OnInputEnter (InputEventData eventData) {
		Debug.LogWarning (gameObject.name +  " OnInputEnter");
	}

	public virtual void OnInputExit (InputEventData eventData) {
		Debug.LogWarning (gameObject.name +  " OnInputExit");
	}

	public virtual void OnInputClick (InputEventData eventData) {
		Debug.LogWarning (gameObject.name +  " OnInputClick");
	}

	public virtual void OnInputDrag (InputEventData eventData) {
		Debug.LogWarning (gameObject.name +  " OnInputDrag");
        //Debug.LogError (eventData.Delta);
        //Debug.LogError (eventData.pressEventCamera.ScreenToWorldPoint(eventData.Position));

        //Vector3 delta = new Vector3 (-eventData.Delta.x, eventData.Delta.y, 0f);
        //transform.position += delta * 0.1f;

        this.gameObject.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y,0);

	}

	public virtual void OnInputBeginDrag (InputEventData eventData) {
		Debug.LogWarning (gameObject.name +  " OnInputBeginDrag");
	}

	public virtual void OnInputEndDrag (InputEventData eventData) {
		Debug.LogWarning (gameObject.name +  " OnInputEndDrag");
	}
}

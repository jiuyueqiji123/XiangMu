﻿using System.Collections;
using System.Collections.Generic;
using TableProto;
using UnityEngine;

/// <summary>
/// 单例，实例化view和model，负责更新view，监听model的变化，处理service的变化。
/// 
/// </summary>
public class MainController : Singleton<MainController> {

	private MainView view;

	private MainModel model;

	private UserService service;

    private bool bLockInput;

    public bool LockInput
    {
        set
        {
            this.bLockInput = value;
        }
    }

	public override void Init ()
	{
		base.Init ();
		this.view = new MainView ();
		this.model = new MainModel ();
		this.service = new UserService ();

        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Express_Click, this.OnBtnExpressClick);
        EventBus.Instance.AddEventHandler(EventID.ON_EXPRESS_ANIM_FINISHED, OnExpressAnimFinished);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Adventure_Click, this.OnBtnAdventureClick);
		HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Game_Click, this.OnBtnGameClick);
		HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Study_Math_Click, this.OnBtnStudyMathClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Study_English_Click, this.OnBtnStudyEnglishClick);
		HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_AR_Click, this.OnBtnARClick);
		HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Parent_Click, this.OnBtnParentClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Radio_Click, this.OnBtnRadioClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Walk_Click, this.OnBtnWalkClick);
		HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Stand_Click, this.OnBtnStandClick);
		HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Item_Child_Click, this.OnBtnItemChildClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Paint_Click, this.OnBtnPaintClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Main_Btn_Gift_Click, this.OnBtnGiftClick);
        EventBus.Instance.AddEventHandler<bool>(EventID.ON_MAIN_GAME_PLAY_OR_EXIT, OnMainGamePlayOrExit);
    }

	public override void UnInit ()
	{
		base.UnInit ();

        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Express_Click, this.OnBtnExpressClick);
        EventBus.Instance.RemoveEventHandler(EventID.ON_EXPRESS_ANIM_FINISHED, OnExpressAnimFinished);
        HUIEventManager.Instance.RemoveUIEventListener (enUIEventID.Main_Btn_Adventure_Click, this.OnBtnAdventureClick);
		HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Game_Click, this.OnBtnGameClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Study_Math_Click, this.OnBtnStudyMathClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Study_English_Click, this.OnBtnStudyEnglishClick);
		HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_AR_Click, this.OnBtnARClick);
		HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Parent_Click, this.OnBtnParentClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Radio_Click, this.OnBtnRadioClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Walk_Click, this.OnBtnWalkClick);
		HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Stand_Click, this.OnBtnStandClick);
		HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Item_Child_Click, this.OnBtnItemChildClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Paint_Click, this.OnBtnPaintClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Main_Btn_Gift_Click, this.OnBtnGiftClick);
        EventBus.Instance.RemoveEventHandler<bool>(EventID.ON_MAIN_GAME_PLAY_OR_EXIT, OnMainGamePlayOrExit);
    }

	public void OpenView() {
		this.view.OpenForm ();
        bLockInput = false;

        NewbieManager.Instance.CheckStartTime (NewbieStartTimeType.EnterMain);
	}

    public void CloseView()
    {
        this.view.CloseForm();
    }

    private void BlockInput()
    {
        this.view.BlockInput();
    }

    public void CloseButtonRadio()
    {
        this.view.CloseButtonRadio();
    }
    public void OpenButtonRadio()
    {
        this.view.OpenButtonRadio();
    }

    #region uievent

    private void OnBtnExpressClick(HUIEvent evt)
    {
        Debug.LogError("点击快递");
        if (bLockInput)
        {
            return;
        }
        SDKManager.Instance.Event(UmengEvent.MainExpress);
        BlockInput();
        ToyBoxManager.Instance.isWork = false;
        //RestManager.Instance.PauseCheck();
        //EventBus.Instance.BroadCastEvent(EventID.MAIN_GO_OTHER_VIEW);
        EventBus.Instance.BroadCastEvent(EventID.ON_EXPRESS_BTN_CLICKED);
    }

    private void OnExpressAnimFinished()
    {
        Debug.Log("快递动画播放完了");
        
        GameStateManager.Instance.GotoState(GameStateName.EXPRESS_STATE);
    }

	private void OnBtnAdventureClick(HUIEvent evt) {
        
		Debug.LogError ("点击冒险");
        SDKManager.Instance.Event(UmengEvent.MainAdventure);
        //GameStateManager.Instance.GotoState(GameStateName.MAO_XIAN_STATE);
        this.view.ClickBottomBtn(emMainType.Adventure);
    }

	private void OnBtnItemChildClick(HUIEvent evt) {
        if (bLockInput)
        {
            return;
        }
        EventBus.Instance.BroadCastEvent(EventID.MAIN_GO_OTHER_VIEW);
        //RestManager.Instance.PauseCheck();
        BlockInput();

        stUIEventParams param = evt.m_eventParams;
        MainItemInfo info = this.model.GetItemInfo((uint)param.argInt);
        if (info != null)
        {
            bLockInput = true;
            SDKManager.Instance.Event(info.umeng_event);
            if (info.type == (int)emMainType.Adventure + 1)
            {
                MaoXianManager.Instance.SetName(info.scene_int, info.umeng_level);
                GameStateManager.Instance.GotoState(info.scene);
            }
            else if (info.type == (int)emMainType.Game + 1)
            {
                GameStateManager.Instance.GotoState(info.scene);
            }
            else if (info.type == (int)emMainType.StudyMath + 1)
            {
                 GameStateManager.Instance.GotoState(info.scene);
            }
            else if (info.type == (int)emMainType.StudyEnglish + 1)
            {
                GameStateManager.Instance.GotoState(info.scene);
            }
        }
        
//		ModuleAssetState state = ModuleAssetManager.Instance.ModuleAsset [name];
//		if (state.IsUnziping) {
//			return;
//		}
//		if (!state.IsDownload) {
//			ModuleAssetManager.Instance.UnZipModule (name, AdventureDoneHandle);
//		} else {
//			AdventureDoneHandle (name);
//		}
	}

	private void AdventureDoneHandle(string name) {
		/*
		ModuleAssetState state = ModuleAssetManager.Instance.ModuleAsset [name];
		if (!state.IsReadInfo) {
			ModuleAssetManager.Instance.LoadResInfo (name);
		}
		MaoXianManager.Instance.SetName (name);
		GameStateManager.Instance.GotoState(GameStateName.MAO_XIAN_STATE);
        */
	}

	private void OnBtnGameClick(HUIEvent evt) {
        Debug.LogError ("点击基地");
        SDKManager.Instance.Event(UmengEvent.MainGame);
        this.view.ClickBottomBtn(emMainType.Game);
        //SceneLoader.Instance.LoadScene("XianGuo", FreshFruitController.Instance.view.OpenView);
	}

    private void OnBtnStudyMathClick(HUIEvent evt)
    {
        Debug.LogError("点击数学");
        SDKManager.Instance.Event(UmengEvent.MainStudyMath);
        this.view.ClickBottomBtn(emMainType.StudyMath);
    }

    private void OnBtnStudyEnglishClick(HUIEvent evt)
    {
        Debug.LogError("点击英语");
        SDKManager.Instance.Event(UmengEvent.MainStudyEnglish);
        this.view.ClickBottomBtn(emMainType.StudyEnglish);
    }

    private void OnBtnPaintClick(HUIEvent evt)
    {
        Debug.LogError("点击画画");
        SDKManager.Instance.Event(UmengEvent.MainPaint);
        GameStateManager.Instance.GotoState(GameStateName.PAINT_STATE);
        //this.view.ClickBottomBtn(emMainType.Paint);
    }

    private void OnBtnGiftClick(HUIEvent evt)
    {
        Debug.LogError("点击礼包");
        PopPanelManager.Instance.AddPopPanel(UnlockGiftBagController.Instance);
        
    }

    private void OnBtnRadioClick(HUIEvent evt) {
		Debug.Log ("点击电台");
        //SDKManager.Instance.Event(UmengEvent.MainRadio);
        //this.view.ClickBottomBtn(emMainType.Radio);
        //RadioSDKManager.Instance.InitRadio ();
        //RadioSDKManager.Instance.Play ("http://test.superwings.soulgame.mobi/public/radio/Hotel California.mp3");
        SDKManager.Instance.Event(UmengEvent.MainRadio);
        GameStateManager.Instance.GotoState(GameStateName.Radio_State);
    }

    private void OnBtnARClick(HUIEvent evt) {
		//Debug.LogError ("点击AR");
		//this.view.ClickBottomBtn(3);
	}

	private void OnBtnParentClick(HUIEvent evt) {
        if (bLockInput)
        {
            return;
        }
        EventBus.Instance.BroadCastEvent(EventID.MAIN_GO_OTHER_VIEW);
        SettingManager.Instance.OpenView();
        /*
        VerifyController.Instance.OpenView(new System.Action(() =>
        {
            ParentController.Instance.OpenView(ParentViewTab.Acount);
        }));
        */
        //NoticeManager.Instance.ShowOkCancelTip("测试测试测试测试测试测", ()=> { Debug.LogError(1); });
        //TransitionManager.Instance.StartTransition(null, null);
        //LuaManager.Instance.CallFunction ("FormManager", "OpenNoticeForm");
    }

	private void OnBtnWalkClick(HUIEvent evt) {
		Debug.LogError ("走");
		PoolObjHandle<ActorRoot> actor = ActorObjManager.Instance.GetActor (101001000);
		actor.handle.PlayAnimator ("Walk", 0f, true);

		AudioManager.Instance.PlayMusic (SoundNames.BackgroundMusic, true);
	}

	private void OnBtnStandClick(HUIEvent evt) {
		Debug.LogError ("站");
		PoolObjHandle<ActorRoot> actor = ActorObjManager.Instance.GetActor (101001000);
		actor.handle.PlayAnimator ("Stand", 0f, true);

		AudioManager.Instance.PlaySound (SoundNames.TestSound);
    }

    private void OnMainGamePlayOrExit(bool isPlay)
    {
        if (isPlay)
        {
            view.BottomControlHideAndShowArrow();
        }
        else
        {
            view.ButtomControlShowAndHideArrow();
        }
    }

    #endregion

    #region event
    public void ChildBtnHile()
    {
        this.view.BottomControlBack();
    }
    #endregion

    public List<MainItemInfo> GetItems(emMainType type)
    {
        return this.model.GetItems(type);
    }

    public MainItemInfo GetItem(uint id)
    {
        return this.model.GetItemInfo(id);
    }
}

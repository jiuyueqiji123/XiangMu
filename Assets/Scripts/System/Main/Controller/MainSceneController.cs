﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DynamicShadowProjector;

public class MainSceneController : Singleton<MainSceneController> {

	private GameObject mainObject;
    private DrawTargetObject targetObject;

	private Vector3 drawPosition = new Vector3(110f, 194f, -43f);
    private Quaternion drawRot = Quaternion.Euler(new Vector3(49.16f, -70.681f, 0f));

	public void InitScene() {
        mainObject = SceneObjManger.Instance.CreateSceneObject (SceneObjManger.ScenePath(SceneObjManger.MAIN_CONTROLLER));
        targetObject = SceneObjManger.Instance.CreateSceneObject(SceneObjManger.MAIN_SHADOW, drawPosition, drawRot).GetComponent<DrawTargetObject>();

    }

    public void SetDrawTarget(Transform transform)
    {
        targetObject.target = transform;
    }

	public void ExitScene() {
		SceneObjManger.Instance.RecycleGameObject (mainObject);
		SceneObjManger.Instance.RecycleGameObject (targetObject.gameObject);
	}
}

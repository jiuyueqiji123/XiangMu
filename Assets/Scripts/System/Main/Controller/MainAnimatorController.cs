﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class MainAnimatorController : Singleton<MainAnimatorController> {

    private Dictionary<uint, MainAnimation> animTable;

    //测试
    private uint start = 1001;

    private int seq = -1;

    /// <summary>
    /// 关闭自动开始
    /// </summary>
    private bool closeStart = false;
    private bool isEnable = true;

    public override void Init()
    {
        base.Init();
        animTable = TableProtoLoader.MainAnimationDict;
    }

    public List<MainAnimation.act_attr> GetActs()
    {
        //return animTable[1018].act_table;
        int num = animTable.Count;
        uint ran = (uint)Random.Range(0, num);
        while (animTable[start + ran].enable == 0)
        {
            ran = (uint)Random.Range(0, num);
        }
        return animTable[start + ran].act_table;
        //AddStart();
        //while (animTable[start].enable == 0)
        //{
        //    AddStart();
        //}
        //return animTable[1014].act_table;
        //return animTable[start].act_table;
    }

    private void AddStart()
    {
        start++;
        if (!animTable.ContainsKey(start))
        {
            start = 1001;
        }
    }

    public void StartAutoPlay()
    {
        if (!isEnable)
            return;
        //Debug.Log("StartAutoPlay");
        if (seq < 0)
        {
            Debug.Log("StartAutoPlay");
            seq = TimerManager.Instance.AddTimer(10000, 1, this.OnTimePlay);
        }
    }

    public void StopAutoPlay()
    {
        Debug.Log("StopAutoPlay: " + seq);
        if (seq >= 0)
        {
            TimerManager.Instance.RemoveTimer(seq);
            seq = -1;
        }
    }

    public void InMainRoom()
    {
        StartAutoPlay();
    }

    public void ExitMainRoom()
    {
        StopAutoPlay();
    }

    public void OpenScrennUI()
    {
        StopAutoPlay();
    }

    public void CloseScreenUI()
    {
        StartAutoPlay();
    }

    public void EnableController(bool val)
    {
        isEnable = val;
        if (isEnable)
            StartAutoPlay();
        else
            StopAutoPlay();
    }

    private void OnTimePlay(int seq)
    {
        if (!isEnable)
            return;
        PoolObjHandle<ActorRoot> actor = ActorObjManager.Instance.GetActor(ActorNames.LeDi);
        actor.handle.MainStateClick();
    }
 }

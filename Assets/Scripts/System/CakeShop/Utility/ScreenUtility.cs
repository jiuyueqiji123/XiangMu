﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ScreenUtility {

	public static float AspectRatio {
        get {
            var ratio = (float)Screen.width / Screen.height;
            return ratio;
        }
    }

    public static bool IsWideScreen {
        get {
            if (AspectRatio <= 1.5f) {
                return false;
            } else {
                return true;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public sealed class TimeUtility {

    public static IEnumerator DelayInvoke(float second, Action callback) {
        yield return new WaitForSeconds(second);

        if (callback != null) {
            callback();
        }
    }

    public static IEnumerator WaitForEndOfFrame(Action callback) {
        yield return new WaitForEndOfFrame();

        if (callback != null) {
            callback();
        }
    }
}

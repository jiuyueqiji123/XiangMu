﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PhysicsUtility {

    public static void ApplyExplosionForce(Vector3 explosionPos, float radius, float power, float upwardsModifier) {
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);

        foreach (Collider hit in colliders) {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(power, explosionPos, radius, upwardsModifier);
        }
    }
}

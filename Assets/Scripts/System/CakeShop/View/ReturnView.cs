﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// View用来管理窗口或者其它视图的显示，监听用户输入，然后执行Controller中的逻辑。
/// Controller可以改变view的显示
/// view最好不要引用model对象
/// </summary>
public class ReturnView {

	private HUIFormScript _huiForm;
	private GameObject _btnBack;


	public void OpenForm(string path) {
		_huiForm = HUIManager.Instance.OpenForm(path, false);
		InitForm();
	}

	public void CloseForm() {
		HUIManager.Instance.CloseForm(_huiForm);
	}

    private void InitForm() {
		_btnBack = _huiForm.GetWidget((int)ReturnViewWidget.BtnBack);
		HUIUtility.SetUIMiniEvent(_btnBack, enUIEventType.Click, enUIEventID.CakeShop_Btn_Back);
	}
}


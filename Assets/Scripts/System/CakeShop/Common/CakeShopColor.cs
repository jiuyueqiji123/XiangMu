﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class CakeShopColor {

    public static Color Blueberry {
        get { return new Color32(232, 31, 235, 255); }
    }

    public static Color Orange {
        get { return new Color32(235, 186, 31, 255); }
    }

    public static Color Apple {
        get { return new Color32(198, 235, 31, 255); }
    }

    public static Color Rose {
        get { return new Color32(235, 31, 76, 255); }
    }

    public static Color Strawberry {
        get { return new Color32(235, 37, 31, 255); }
    }

    public static Color Pineapple {
        get { return new Color32(231, 235, 31, 255); }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class CakeShopResPath {
    // Dir
    public const string AUDIO_DIR = "game_cake/game_cake_audio/";
    public const string PREFAB_DIR = "game_cake/game_cake_prefab/";
    public const string TEXTURE_DIR = "game_cake/game_cake_texture/";
    public const string VIEW_DIR = "game_cake/game_cake_view/";
    public const string ATLAS_DIR = "game_cake/game_cake_atlas/";
    public const string TIMELINE_DIR = "game_cake/game_cake_timeline/";
    public const string SOUND_BG = "sound_bg/";

    // Full file path
    public const string TL_LEVEL_1 = TIMELINE_DIR + "Level_1_Timeline";
    public const string TL_LEVEL_2 = TIMELINE_DIR + "Level_2_Timeline";
    public const string TL_LEVEL_3 = TIMELINE_DIR + "Level_3_Timeline";
    public const string TL_LEVEL_4 = TIMELINE_DIR + "Level_4_Timeline";
    public const string TL_LEVEL_5 = TIMELINE_DIR + "Level_5_Timeline";

    public const string TEX_COLORFUL_GREEN = TEXTURE_DIR + "304020410_green";
    public const string TEX_COLORFUL_HUANGLV = TEXTURE_DIR + "304020410_huanglv";
    public const string TEX_COLORFUL_ORANGE = TEXTURE_DIR + "304020410_orange";
    public const string TEX_COLORFUL_QIANHUANGBAI = TEXTURE_DIR + "304020410_qianhuangbai";
    public const string TEX_COLORFUL_RED = TEXTURE_DIR + "304020410_red";
    public const string TEX_COLORFUL_WHITE = TEXTURE_DIR + "304020410_white";
    public const string TEX_COLORFUL_YELLOW = TEXTURE_DIR + "304020410_yellow";

    public const string PARTICLE_CHOCOLATE = PREFAB_DIR + "Particle_Chocolate";
    public const string PARTICLE_COLORFUL = PREFAB_DIR + "Particle_Colorful";

    public const string CAKE_BODY = PREFAB_DIR + "Cake_Body";
    public const string CAKE_CHRISTMAS = PREFAB_DIR + "Cake_Christmas";
    public const string CAKE_FRUIT = PREFAB_DIR + "Cake_Fruit";
    public const string CAKE_RAINBOW = PREFAB_DIR + "Cake_Rainbow";
    public const string CAKE_MICAI_BEFORE = PREFAB_DIR + "Cake_MiCai_Before";
    public const string CAKE_MICAI_AFTER = PREFAB_DIR + "Cake_MiCai_After";
    public const string CAKE_YINGHUA_BEFORE = PREFAB_DIR + "Cake_YingHua_Before";
    public const string CAKE_YINGHUA_AFTER = PREFAB_DIR + "Cake_YingHua_After";
    public const string CAKE_PAOFU = PREFAB_DIR + "Cake_PaoFu";
    public const string CAKE_JAM = PREFAB_DIR + "Jam";


    public const string MACARON_PINK = PREFAB_DIR + "Macaron_Pink";
    public const string MACARON_BLUE = PREFAB_DIR + "Macaron_Blue";
    public const string MACARON_BROWN = PREFAB_DIR + "Macaron_Brown";

    public const string CREAM_1 = PREFAB_DIR + "Cream_1";
    public const string CREAM_2 = PREFAB_DIR + "Cream_2";
    public const string CREAM_3 = PREFAB_DIR + "Cream_3";

    public const string CIRCLE_CREAM_1 = PREFAB_DIR + "Circle_Cream_1";
    public const string CIRCLE_CREAM_2 = PREFAB_DIR + "Circle_Cream_2";
    public const string CIRCLE_CREAM_3 = PREFAB_DIR + "Circle_Cream_3";

    public const string SQUARE_CREAM_1 = PREFAB_DIR + "Square_Cream_1";
    public const string SQUARE_CREAM_2 = PREFAB_DIR + "Square_Cream_2";
    public const string SQUARE_CREAM_3 = PREFAB_DIR + "Square_Cream_3";

    public const string CREAM_WHITE = TEXTURE_DIR + "304010341_01";
    public const string CREAM_PINK = TEXTURE_DIR + "304010341_03";
    public const string CREAM_BLUE = TEXTURE_DIR + "304010341_04";
    public const string CREAM_ORANGE = TEXTURE_DIR + "304010341_02";

    public const string PAOFU_WHITE = TEXTURE_DIR + "304010315_01";
    public const string PAOFU_PINK = TEXTURE_DIR + "304010315_02";
    public const string PAOFU_BLUE = TEXTURE_DIR + "304010315_03";
    public const string PAOFU_ORANGE = TEXTURE_DIR + "304010315_04";

    public const string CAKE_ATLAS = ATLAS_DIR + "CakeAtlas";

    public const string TEX_JAM_BLUEBERRY = TEXTURE_DIR + "304010434";
    public const string TEX_JAM_ORANGE = TEXTURE_DIR + "304010435";
    public const string TEX_JAM_APPLE = TEXTURE_DIR + "304010436";
    public const string TEX_JAM_ROSE = TEXTURE_DIR + "304010437";
    public const string TEX_JAM_STRAWBERRY = TEXTURE_DIR + "304010438";
    public const string TEX_JAM_PINEAPPLE = TEXTURE_DIR + "304010439";

    public const string TEX_COMMON_0 = TEXTURE_DIR + "304010422";
    public const string TEX_COMMON_1 = TEXTURE_DIR + "304010423";
    public const string TEX_MICAI_0 = TEXTURE_DIR + "304010424";
    public const string TEX_MICAI_1 = TEXTURE_DIR + "304010425";
    public const string TEX_YINGHUA_0 = TEXTURE_DIR + "304010426";
    public const string TEX_YINGHUA_1 = TEXTURE_DIR + "304010427";

    public const string BGM_604010101 = SOUND_BG + "604010101";

    public const string LEVEL_1 = PREFAB_DIR + "Level_1_Opening";
    public const string LEVEL_2 = PREFAB_DIR + "Level_2_Making";
    public const string LEVEL_3 = PREFAB_DIR + "Level_3_Baking";
    public const string LEVEL_4 = PREFAB_DIR + "Level_4_Decorating";
    public const string LEVEL_5 = PREFAB_DIR + "Level_5_Eating";

    public const string LeDi_404010101 = AUDIO_DIR + "404010101";
    public const string LeDi_404010102 = AUDIO_DIR + "404010102";
    public const string LeDi_404010103 = AUDIO_DIR + "404010103";

    public const string DuoDuo_404010104 = AUDIO_DIR + "404010104";
    public const string DuoDuo_404010105 = AUDIO_DIR + "404010105";
    public const string DuoDuo_404010106 = AUDIO_DIR + "404010106";
    public const string DuoDuo_404010107 = AUDIO_DIR + "404010107";
    public const string DuoDuo_404010108 = AUDIO_DIR + "404010108";
    public const string DuoDuo_404010109 = AUDIO_DIR + "404010109";

    public const string XiaoQing_404010110 = AUDIO_DIR + "404010110";
    public const string XiaoQing_404010111 = AUDIO_DIR + "404010111";
    public const string XiaoQing_404010112 = AUDIO_DIR + "404010112";
    public const string XiaoQing_404010113 = AUDIO_DIR + "404010113";
    public const string XiaoQing_404010114 = AUDIO_DIR + "404010114";
    public const string XiaoQing_404010115 = AUDIO_DIR + "404010115";

    public const string XiaoAi_404010116 = AUDIO_DIR + "404010116";
    public const string XiaoAi_404010117 = AUDIO_DIR + "404010117";
    public const string XiaoAi_404010118 = AUDIO_DIR + "404010118";
    public const string XiaoAi_404010119 = AUDIO_DIR + "404010119";
    public const string XiaoAi_404010120 = AUDIO_DIR + "404010120";
    public const string XiaoAi_404010121 = AUDIO_DIR + "404010121";

    public const string LeDi_404010122 = AUDIO_DIR + "404010122";
    public const string LeDi_404010122a = AUDIO_DIR + "404010122a";
    public const string LeDi_404010122b = AUDIO_DIR + "404010122b";
    public const string LeDi_404010122c = AUDIO_DIR + "404010122c";
    public const string LeDi_404010122d = AUDIO_DIR + "404010122d";
    public const string LeDi_404010122e = AUDIO_DIR + "404010122e";
    public const string LeDi_404010122f = AUDIO_DIR + "404010122f";
    public const string LeDi_404010123 = AUDIO_DIR + "404010123";

    public const string LeDi_404010201 = AUDIO_DIR + "404010201";
    public const string LeDi_404010202 = AUDIO_DIR + "404010202";
    public const string LeDi_404010203 = AUDIO_DIR + "404010203";
    public const string LeDi_404010204 = AUDIO_DIR + "404010204";
    public const string LeDi_404010205 = AUDIO_DIR + "404010205";

    public const string LeDi_404010301a = AUDIO_DIR + "404010301a";
    public const string LeDi_404010301b = AUDIO_DIR + "404010301b";
    public const string LeDi_404010302 = AUDIO_DIR + "404010302";
    public const string LeDi_404010303 = AUDIO_DIR + "404010303";
    public const string LeDi_404010304b = AUDIO_DIR + "404010304b";
    public const string LeDi_404010305 = AUDIO_DIR + "404010305";
    public const string LeDi_404010306 = AUDIO_DIR + "404010306";
    public const string LeDi_404010307 = AUDIO_DIR + "404010307";
    public const string LeDi_404010308 = AUDIO_DIR + "404010308";

    public const string LeDi_404010401 = AUDIO_DIR + "404010401";
    public const string LeDi_404010402 = AUDIO_DIR + "404010402";
    public const string LeDi_404010403 = AUDIO_DIR + "404010403";
    public const string LeDi_404010404 = AUDIO_DIR + "404010404";
    public const string LeDi_404010405 = AUDIO_DIR + "404010405";
    public const string LeDi_404010406 = AUDIO_DIR + "404010406";

    public const string LeDi_404010501 = AUDIO_DIR + "404010501";
    public const string LeDi_404010502 = AUDIO_DIR + "404010502";
    public const string LeDi_404010503 = AUDIO_DIR + "404010503";
    public const string LeDi_404010504 = AUDIO_DIR + "404010504";

    public const string LeDi_404010601 = AUDIO_DIR + "404010601";
    public const string LeDi_404010602 = AUDIO_DIR + "404010602";
    public const string LeDi_404010603 = AUDIO_DIR + "404010603";
    public const string LeDi_404010604 = AUDIO_DIR + "404010604";
    public const string LeDi_404010605 = AUDIO_DIR + "404010605";

    public const string DuoDuo_404010607 = AUDIO_DIR + "404010607";
    public const string DuoDuo_404010608 = AUDIO_DIR + "404010608";
    public const string DuoDuo_404010609 = AUDIO_DIR + "404010609";
    public const string DuoDuo_404010610 = AUDIO_DIR + "404010610";
    public const string DuoDuo_404010611 = AUDIO_DIR + "404010611";

    public const string XiaoQing_404010612 = AUDIO_DIR + "404010612";
    public const string XiaoQing_404010613 = AUDIO_DIR + "404010613";
    public const string XiaoQing_404010619 = AUDIO_DIR + "404010619";
    public const string XiaoQing_404010620 = AUDIO_DIR + "404010620";
    public const string XiaoQing_404010621 = AUDIO_DIR + "404010621";

    public const string XiaoAi_404010614 = AUDIO_DIR + "404010614";
    public const string XiaoAi_404010615 = AUDIO_DIR + "404010615";
    public const string XiaoAi_404010616 = AUDIO_DIR + "404010616";
    public const string XiaoAi_404010617 = AUDIO_DIR + "404010617";
    public const string XiaoAi_404010618 = AUDIO_DIR + "404010618";

    public const string SE_604010201 = AUDIO_DIR + "604010201";
    public const string SE_604010202 = AUDIO_DIR + "604010202";
    public const string SE_604010203 = AUDIO_DIR + "604010203";
    public const string SE_604010204 = AUDIO_DIR + "604010204";
    public const string SE_604010205 = AUDIO_DIR + "604010205";
    public const string SE_604010208 = AUDIO_DIR + "604010208";
    public const string SE_604020304 = AUDIO_DIR + "604020304";
    public const string SE_604010306 = AUDIO_DIR + "604010306";
    public const string SE_604010402 = AUDIO_DIR + "604010402";
    public const string SE_604010502 = AUDIO_DIR + "604010502";
    public const string SE_604010503 = AUDIO_DIR + "604010503";
}

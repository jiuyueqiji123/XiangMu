﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class HandController : MonoBehaviour {
    public enum HandState {
        Move,
        Click,
    }

    public HandState initState = HandState.Move;

    Animator _animator;

	void Start() {
        _animator = GetComponent<Animator>();
        SwitchStateTo(initState);
	}
	
	public void SwitchStateTo(HandState state) {
        switch (state) {
            case HandState.Move:
                _animator.speed = 0f;
                break;
            case HandState.Click:
                _animator.speed = 1f;
                break;
        }
    }
}

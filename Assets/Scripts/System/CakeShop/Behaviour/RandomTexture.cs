﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTexture : MonoBehaviour {
    [SerializeField]
    Texture2D[] _textures;

	void Start() {
        if (_textures == null || _textures.Length < 1) {
            return;
        }

        var index = Random.Range(0, _textures.Length);
        GetComponent<Renderer>().material.mainTexture = _textures[index];
	}
}

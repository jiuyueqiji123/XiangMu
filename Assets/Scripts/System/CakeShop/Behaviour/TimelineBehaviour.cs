﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[RequireComponent(typeof(PlayableDirector))]
public class TimelineBehaviour : MonoBehaviour {
    public PlayableDirector playableDirector;

    protected virtual void Start() {
        if (playableDirector == null) {
            playableDirector = GetComponent<PlayableDirector>();
        }
    }

    public virtual void PauseTimeline(float duration) {
        playableDirector.Pause();
        playableDirector.time += duration;
    }

    public virtual void PlayTimeline() {
         playableDirector.Evaluate();
         playableDirector.Resume();
    }

    public bool IsTimelinePlaying() {
        return playableDirector.state.Equals(PlayState.Playing);
    }

    public void StopTimeline() {
        playableDirector.Stop();
    }
}

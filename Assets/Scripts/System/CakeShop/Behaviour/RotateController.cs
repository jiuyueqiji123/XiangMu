﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateController : MonoBehaviour {
    [SerializeField]
    Vector3 _rotateAround;

    Transform _thisTransform;

    void Start() {
        _thisTransform = transform;
    }

    void Update() {
        _thisTransform.Rotate(_rotateAround);
    }
}

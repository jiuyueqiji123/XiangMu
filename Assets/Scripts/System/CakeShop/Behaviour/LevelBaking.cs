﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class LevelBaking : TimelineBehaviour {

    [SerializeField]
    Camera _camera1;

    [SerializeField]
    Camera _camera2;

    [SerializeField]
    GameObject _fluid;

    [SerializeField]
    GameObject _buttonEffect;

    [SerializeField]
    GameObject _overEffect;

    [SerializeField]
    GameObject[] _afters;

    [SerializeField]
    GameObject _glove;

    [SerializeField]
    GameObject _starEffect;

    [SerializeField]
    Transform _cream;

    [SerializeField]
    Transform _bowl;

    [SerializeField]
    GameObject _hand;

    [SerializeField]
    GameObject _canvas;

    [SerializeField]
    GameObject _hotGasEffect;

    [SerializeField]
    BoxCollider[] _colliders;

    Transform _thisTransform;
    bool _showingGlove;
    float _lastTime;
    float _interval = 7f;

    protected override void Start() {
        base.Start();

        _thisTransform = GetComponent<Transform>();

        if (_camera1 == null) {
            _camera1 = _thisTransform.Find("Camera_1").GetComponent<Camera>();
        }

        if (_camera2 == null) {
            _camera2 = _thisTransform.Find("Camera_2").GetComponent<Camera>();
        }

        if (ScreenUtility.IsWideScreen == false) {
            _camera1.fieldOfView = 26f;
        }
    }

    void Update() {
        if (_showingGlove && (Time.time - _lastTime) > _interval) {
            var audio = AudioManager.Instance.PlaySound(CakeShopResPath.LeDi_404010308);
            _lastTime = Time.time + audio.Length;
        }
    }

    public void PlayVoice(int state) {
        switch (state) {
            case 1:
                var length = AudioManager.Instance.PlaySound(CakeShopResPath.LeDi_404010301a).Length;
                StartCoroutine(TimeUtility.DelayInvoke(length, () => {
                    if (CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.PaoFu)) {
                        AudioManager.Instance.PlaySound(CakeShopResPath.LeDi_404010304b);
                    } else {
                        AudioManager.Instance.PlaySound(CakeShopResPath.LeDi_404010301b);
                    }
                }));
                break;
            case 2:
                if (CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.PaoFu)) {
                    AudioManager.Instance.PlaySound(CakeShopResPath.LeDi_404010305);
                } else {  
                    AudioManager.Instance.PlaySound(CakeShopResPath.LeDi_404010302);
                }
                break;
            case 3:
                if (CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.PaoFu)) {
                    AudioManager.Instance.PlaySound(CakeShopResPath.LeDi_404010307);
                } else {
                    AudioManager.Instance.PlaySound(CakeShopResPath.LeDi_404010306);
                }

                _hotGasEffect.SetActive(true);
                break;
        }
    }

    public void ActiveCanvas() {
        _canvas.SetActive(true);
        _thisTransform.GetComponentInChildren<DragMove>().enabled = true;
    }

    public void ActiveHand(bool show) {
        _hand.SetActive(show);

        if (show && CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.PaoFu)) {
            MakePuff.CanTrigger = true;
        }
    }

    int _currentCollidersIndex = -1;

    public void SwitchCollider(int index) {
        _currentCollidersIndex = index;

        for (var i = 0; i < _colliders.Length; i++) {
            if (i == _currentCollidersIndex) {
                _colliders[i].enabled = true;
            } else {
                _colliders[i].enabled = false;
            }
        }
    }

    public void MoveHand() {
        ActiveHand(true);
        _hand.transform.localEulerAngles = new Vector3(120f, 0f, 0f);

        var hc = _hand.GetComponent<HandController>();
        hc.SwitchStateTo(HandController.HandState.Move);

        var trs = _hand.GetComponent<TRSController>();
        trs.position.form = new Vector3(-168f, -155f, 65f);
        trs.position.to = new Vector3(-168f, -100f, 65f);
        trs.position.loop = true;
    }

    public void MoveCream() {
        if (CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.PaoFu)) {
            _fluid.transform.parent.SetActive(false);
            _cream.DOLocalMove(new Vector3(-175f, -122f, 65f), 1f);
            _hand.transform.localPosition = new Vector3(-180f, -135f, 70f);
        } else {
            _bowl.DOLocalMove(new Vector3(-190f, -125f, 64.5f), 1f);
        }
    }

    public void SkipTime(float duration) {
        playableDirector.time += duration;
    }

    public void SkipTimeForLeDi(float duration) {
        AudioManager.Instance.PlaySound(CakeShopResPath.LeDi_404010303);
        playableDirector.time += duration;
    }

    public void SkipTimeForPaoFu(float duration) {
        if (CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.PaoFu)) {
            playableDirector.time += duration;
        }
    }

    public void OnBowlPour() {
        _bowl.DOLocalMove(new Vector3(-176f, -125f, 78f), 0.5f).OnComplete(() => _bowl.DOLocalRotate(Vector3.up * 70f, 0.5f));
    }

    public void OnBowlLeave() {
        _bowl.DOLocalMove(new Vector3(-176f, -125f, 120f), 1f);
    }

    public void ShowGlove(bool isShow) {
        if (_glove) {
            _glove.SetActive(isShow);

            if (isShow) {
                PauseTimeline(0.5f);
                _showingGlove = true;
                _lastTime = Time.time;
            } else if (_starEffect) {
                StartCoroutine(TimeUtility.DelayInvoke(1.5f, () => {
                    AudioManager.Instance.PlaySound(CakeShopResPath.SE_604020304);
                    _starEffect.SetActive(true);
                }));
            }
        }
    }

    public void OvenEffect(bool isShow) {
        if (_overEffect) {
            _overEffect.SetActive(isShow);
        }

        if (_afters != null && isShow) {
            foreach (var go in _afters) {
                go.SetActive(true);
            }
        }
    }

    public void ButtonEffect(bool isShow) {
        if (_buttonEffect) {
            _buttonEffect.SetActive(isShow);          
        }
    }

    public void StartPourFluid() {
        if (_fluid) {
            _fluid.SetActive(true);
        }
    }

    public void StopPourFluid() {
        if (_fluid) {
            _fluid.SetActive(false);
        }

        OnBowlLeave();
    }

    public void SwitchCamera() {
        _camera2.gameObject.SetActive(true);
        _camera1.gameObject.SetActive(false);

        AudioManager.Instance.StopAllSound();
        PlayTimeline();
    }

    public override void PauseTimeline(float duration) {
        base.PauseTimeline(duration);

        // 防止跳帧完成前碰撞被提前关闭
        SwitchCollider(_currentCollidersIndex);
    }

    public override void PlayTimeline() {
        base.PlayTimeline();
        _showingGlove = false;
    }

    public void OnEnd() {
        CakeShopManager.Instance.ToNextLevel(CakeShopManager.LevelState.Level_4_Decorating);
    }
}

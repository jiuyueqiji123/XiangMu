﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using DG.Tweening;

public class LevelEating : TimelineBehaviour {
    public static Transform cake;

    [SerializeField]
    Camera _camera;

    [SerializeField]
    Transform _parent;

    [SerializeField]
    Transform _roleParent;

    [SerializeField]
    GameObject _leDi;

    [SerializeField]
    GameObject _clickEffect;

    [SerializeField]
    GameObject _endEffect;

    GameObject _currentRole;
    
    protected override void Start() {
        base.Start();

        if (_camera == null) {
            _camera = GetComponentInChildren<Camera>();
        }

        if (ScreenUtility.IsWideScreen == false) {
            _camera.fieldOfView = 38f;
        }

        ShowGuest();

        if (cake) {
            cake.SetParent(_parent);
            cake.localPosition = new Vector3(0f, 7f, -2f);
            cake.localEulerAngles = Vector3.zero;
            cake.localScale = Vector3.one;
            cake = null;
        }
    }

    public void ShowGuest() {
        var roleName = CakeShopManager.Instance.CurrentGuestType.ToString();
        var path = CakeShopResPath.PREFAB_DIR + roleName;
        _currentRole = CommonUtility.InstantiateFrom(path, _roleParent).gameObject;

        foreach (var playableBinding in playableDirector.playableAsset.outputs) {
            if (playableBinding.streamName.Equals(roleName)) {
                playableDirector.SetGenericBinding(playableBinding.sourceObject, _currentRole);
                break;
            }
        }
    }

    public void ActiveEffect(bool value) {
        _clickEffect.SetActive(value);
    }

    public void PlayVoice() {
        AudioManager.Instance.StopAllSound();

        var index = Random.Range(0, 2);

        switch (index) {
            case 0:
                RolePraise();
                break;
            case 1:
                RoleWant();
                break;
        }
    }
    
    public void RolePraise() {
        var index = Random.Range(0, 2);
        var voices = new string[2];

        switch (CakeShopManager.Instance.CurrentGuestType) {
            case GuestType.DuoDuo:
                voices[0] = CakeShopResPath.DuoDuo_404010607;
                voices[1] = CakeShopResPath.DuoDuo_404010607;
                break;
            case GuestType.XiaoAi:
                voices[0] = CakeShopResPath.XiaoAi_404010614;
                voices[1] = CakeShopResPath.XiaoAi_404010615;
                break;
            case GuestType.XiaoQing:
                voices[0] = CakeShopResPath.XiaoQing_404010612;
                voices[1] = CakeShopResPath.XiaoQing_404010613;
                break;
        }

        var animator = _currentRole.GetComponent<Animator>();
        animator.CrossFade("Talk", 0.5f);
        StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(voices[index]).Length, () => {
            animator.CrossFade("Sit", 0.5f);
            LeDiThanks();
        }));
    }

    public void LeDiThanks() {
        var index = Random.Range(0, 2);
        var voices = new string[2];
        voices[0] = CakeShopResPath.LeDi_404010602;
        voices[1] = CakeShopResPath.LeDi_404010603;
        AudioManager.Instance.PlaySound(voices[index]);

        var animator = _leDi.GetComponent<Animator>();
        animator.CrossFade("Talk", 0.5f);
        StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(voices[index]).Length, () => {
            animator.CrossFade("Idle", 0.5f);
            PlayTimeline();
        }));
    }

    public void RoleWant() {
        var index = Random.Range(0, 2);
        var voices = new string[2];

        switch (CakeShopManager.Instance.CurrentGuestType) {
            case GuestType.DuoDuo:
                voices[0] = CakeShopResPath.DuoDuo_404010608;
                voices[1] = CakeShopResPath.DuoDuo_404010609;
                break;
            case GuestType.XiaoAi:
                voices[0] = CakeShopResPath.XiaoAi_404010616;
                voices[1] = CakeShopResPath.XiaoAi_404010617;
                break;
            case GuestType.XiaoQing:
                voices[0] = CakeShopResPath.XiaoQing_404010619;
                voices[1] = CakeShopResPath.XiaoQing_404010620;
                break;
        }

        var animator = _currentRole.GetComponent<Animator>();
        animator.CrossFade("Talk", 0.5f);
        StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(voices[index]).Length, () => {
            animator.CrossFade("Sit", 0.5f);
            LeDiPersuade();
        }));
    }

    public void LeDiPersuade() {
        var index = Random.Range(0, 2);
        var voices = new string[2];
        voices[0] = CakeShopResPath.LeDi_404010604;
        voices[1] = CakeShopResPath.LeDi_404010605;
        AudioManager.Instance.PlaySound(voices[index]);

        var animator = _leDi.GetComponent<Animator>();
        animator.CrossFade("Talk", 0.5f);
        StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(voices[index]).Length, () => {
            animator.CrossFade("Idle", 0.5f);
            RoleLeave();
        }));
    }

    public void RoleLeave() {
        var index = Random.Range(0, 2);
        var voices = new string[2];

        switch (CakeShopManager.Instance.CurrentGuestType) {
            case GuestType.DuoDuo:
                voices[0] = CakeShopResPath.DuoDuo_404010610;
                voices[1] = CakeShopResPath.DuoDuo_404010611;
                break;
            case GuestType.XiaoAi:
                voices[0] = CakeShopResPath.XiaoAi_404010618;
                voices[1] = CakeShopResPath.XiaoAi_404010618;
                break;
            case GuestType.XiaoQing:
                voices[0] = CakeShopResPath.XiaoQing_404010621;
                voices[1] = CakeShopResPath.XiaoQing_404010621;
                break;
        }

        var animator = _currentRole.GetComponent<Animator>();
        animator.CrossFade("Talk", 0.5f);
        StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(voices[index]).Length, () => {
            animator.CrossFade("Sit", 0.5f);
            PlayTimeline();
        }));
    }

    public void MoveCake() {
        _parent.parent.SetActive(true);
        _parent.parent.DOLocalMove(new Vector3(25f, -6.5f, 230f), 1f);
    }

    public void EatCake(float value) {
        if (_parent.childCount < 1) {
            return;
        }

        if (!AudioManager.Instance.IsPlayingSound()) {
            if (CakeShopManager.Instance.CurrentGuestType.Equals(GuestType.DuoDuo)) {
                AudioManager.Instance.PlaySound(CakeShopResPath.SE_604010502);
            } else {
                AudioManager.Instance.PlaySound(CakeShopResPath.SE_604010503);
            }
        }

        if (CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.PaoFu)) {
            var puffParent = _parent.GetChild(0).GetChild(0);
            if (puffParent.childCount > 0) {
                Destroy(puffParent.GetChild(puffParent.childCount - 1).gameObject);
            }
        } else {
            _parent.GetChild(0).localScale = Vector3.one * value;
        }
    }

    public void ActiveEndEffect() {
        _endEffect.SetActive(true);
    }

    public override void PauseTimeline(float duration) {
        base.PauseTimeline(duration);
    }

    public override void PlayTimeline() {
        base.PlayTimeline();
    }

    public void OnEnd() {
        CakeShopManager.Instance.OnExit();
        CakeShopManager.Instance.ToNextLevel(CakeShopManager.LevelState.Level_1_Opening);
    }
}

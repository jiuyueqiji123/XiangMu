﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.U2D;
using UnityEngine.Playables;

public class LevelDecorating : TimelineBehaviour {
    public Transform parent;

    [SerializeField]
    RectTransform _button;

    [SerializeField]
    RectTransform _background;

    [SerializeField]
    GameObject _fireworksEffect;

    [SerializeField]
    GameObject _handMove;

    [SerializeField]
    GameObject _handClick;

    [SerializeField]
    RectTransform[] _panels;

    [SerializeField]
    Transform[] _colliders;

    int _panelIndex;

    public Transform CurrentCreamModel { get; private set; }
    public CreamColor CurrentCreamColor { get; set; }


    protected override void Start() {
        InitCakeBody();
        _lastShowHandClickTime = Time.time;
    }

    float _lastShowHandClickTime;
    float _interval = 7f;
    bool _disableHandClick;
    bool _disableHandMove;
    Coroutine _coroutine;

    void Update() {
        if (_panels[0].gameObject.activeInHierarchy && !_disableHandClick) {
            if ((Time.time - _lastShowHandClickTime) > _interval) {
                _handClick.SetActive(true);
            }
        } else if (_panels[1].gameObject.activeInHierarchy && !_disableHandMove) {
            if (_coroutine == null) {
                _coroutine = StartCoroutine(TimeUtility.DelayInvoke(1.5f, () => {
                    if (!_disableHandMove) {
                        _handMove.SetActive(true);
                    }
                }));
            }
        }
    }

    public void InitPanel() {
        switch (CakeShopManager.Instance.CurrentCakeType) {
            case CakeType.PaoFu:
            case CakeType.MiCai:
            case CakeType.YingHua:
                _panelIndex = 2;
                _panels[0].SetActive(false);
                StartCoroutine(TimeUtility.DelayInvoke(1.5f, () => {
                    if (!_disableHandMove) {
                        _handMove.SetActive(true);
                    }
                }));
                break;
            default:
                _panelIndex = 0;
                break;
        }

        _background.DOAnchorPosX(0f, 0.5f);
        _panels[_panelIndex].SetActive(true);
        _panels[_panelIndex].DOAnchorPosX(0f, 0.5f);
        _lastShowHandClickTime = Time.time;
    }

    public void SwitchPanel() {
        if (_panels == null || _panels.Length < 1) {
            Debug.LogError(_panels[0]);
            return;
        }

        var length = _panels.Length;

        if (CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.PaoFu)) {
            length -= 1;
        }

        if (length >= 2) {
            if ((length - _panelIndex) > 1) {
                _panels[_panelIndex].DOAnchorPosX(300f, 0.5f).OnComplete(() => _panels[_panelIndex].SetActive(false));
                _panels[_panelIndex + 1].SetActive(true);
                _panels[_panelIndex + 1].DOAnchorPosX(0f, 0.5f).OnComplete(() => {
                    _panelIndex += 1;

                    if (_panelIndex == (length - 1)) {
                        switch (CakeShopManager.Instance.CurrentCakeType) {
                            case CakeType.MiCai:
                                SwitchCakeBody("MiCai");
                                break;
                            case CakeType.YingHua:
                                SwitchCakeBody("YingHua");
                                break;
                        }
                    }
                });
                return;
            }
        }

        _button.SetActive(false);
        _background.DOAnchorPosX(300f, 0.5f).OnComplete(() => _background.SetActive(false));
        _panels[_panelIndex].DOAnchorPosX(300f, 0.5f).OnComplete(() => {        
            _panels[_panelIndex].SetActive(false);
            PlayTimeline();
        });
    }

    public void PlayFireworksEffect() {
        if (_fireworksEffect) {
            _fireworksEffect.SetActive(true);
        }

        LevelEating.cake = Instantiate(transform.Find("Cake"), null);
    }

    public void SwitchCollider() {
        if (_colliders == null || _colliders.Length != 3) {
            Debug.LogError("Collider error...");
            return;
        }

        _colliders[0].SetActive(false);

        if (!CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.PaoFu)) {
            _colliders[1].SetActive(true);
        }
    }

    public void SelectCakeBody(GameObject go) {
        SwitchCakeBody(go.name);
        _handClick.SetActive(false);
        _disableHandClick = true;
    }

    public void SwitchCakeBody(string cakeName) {
        if (parent.childCount > 0) {
            for (var i = 0; i < parent.childCount; i++) {
                Destroy(parent.GetChild(i).gameObject);
            }
        }

        string path = "";

        switch (cakeName) {
            case "Christmas":
                CakeShopManager.Instance.CurrentCakeType = CakeType.ShengDan;
                path = CakeShopResPath.CAKE_CHRISTMAS;
                break;
            case "Fruit":
                CakeShopManager.Instance.CurrentCakeType = CakeType.ShuiGuo;
                path = CakeShopResPath.CAKE_FRUIT;
                break;
            case "Rainbow":
                CakeShopManager.Instance.CurrentCakeType = CakeType.CaiHong;
                path = CakeShopResPath.CAKE_RAINBOW;
                break;
            case "MiCai":
                path = CakeShopResPath.CAKE_MICAI_AFTER;
                _colliders[1].SetActive(false);
                _colliders[2].SetActive(true);
                break;
            case "YingHua":
                path = CakeShopResPath.CAKE_YINGHUA_AFTER;
                _colliders[1].SetActive(false);
                break;
            case "PaoFu":
                
                break;
        }

        var t = CommonUtility.InstantiateFrom(path, parent);
        ActiveButton();
        ChangeTexture(t);
    }

    void ChangeTexture(Transform t) {
        if (!CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.MiCai)
            && !CakeShopManager.Instance.CurrentCakeType.Equals(CakeType.YingHua)) {
            return;
        }

        var mat = t.GetComponent<Renderer>().material;

        switch (CurrentCreamColor) {
            case CreamColor.White:
                mat.mainTexture = CommonUtility.LoadTexture(CakeShopResPath.PAOFU_WHITE);
                break;
            case CreamColor.Pink:
                mat.mainTexture = CommonUtility.LoadTexture(CakeShopResPath.PAOFU_PINK);
                break;
            case CreamColor.Blue:
                mat.mainTexture = CommonUtility.LoadTexture(CakeShopResPath.PAOFU_BLUE);
                break;
            case CreamColor.Orange:
                mat.mainTexture = CommonUtility.LoadTexture(CakeShopResPath.PAOFU_ORANGE);
                break;
        }

        ResourceManager.Instance.UnloadUnusedAssets();
    }

    public void InitCakeBody() {
        string path = "";

        switch (CakeShopManager.Instance.CurrentCakeType) {
            case CakeType.MiCai:
                path = CakeShopResPath.CAKE_MICAI_BEFORE;
                break;
            case CakeType.YingHua:
                path = CakeShopResPath.CAKE_YINGHUA_BEFORE;
                break;
            case CakeType.PaoFu:
                path = CakeShopResPath.CAKE_PAOFU;
                break;
            default:
                path = CakeShopResPath.CAKE_BODY;
                break;
        }
      
        CommonUtility.InstantiateFrom(path, parent);
    }

    public void SwitchCream() {
        var isCircleCream = true;

        switch (CakeShopManager.Instance.CurrentCakeType) {
            case CakeType.ShengDan:
            case CakeType.ShuiGuo:
            case CakeType.CaiHong:
                isCircleCream = true;
                break;
            default:
                isCircleCream = false;
                break;
        }

        var index = Random.Range(0, 3);
        var path = "";

        switch (index) {
            case 0:
                path = isCircleCream ? CakeShopResPath.CIRCLE_CREAM_1 : CakeShopResPath.SQUARE_CREAM_1;
                break;
            case 1:
                path = isCircleCream ? CakeShopResPath.CIRCLE_CREAM_2 : CakeShopResPath.SQUARE_CREAM_2;
                break;
            case 2:
                path = isCircleCream ? CakeShopResPath.CIRCLE_CREAM_3 : CakeShopResPath.SQUARE_CREAM_3;
                break;
        }

        CurrentCreamModel = CommonUtility.InstantiateFrom(path, parent);
    }

    Transform _jam;

    public void SelectJam(string jamName) {
        var path = string.Empty;

        switch (jamName) {
            case "Jam_Blueberry":
                path = CakeShopResPath.TEX_JAM_BLUEBERRY;
                break;
            case "Jam_Orange":
                path = CakeShopResPath.TEX_JAM_ORANGE;
                break;
            case "Jam_Apple":
                path = CakeShopResPath.TEX_JAM_APPLE;
                break;
            case "Jam_Rose":
                path = CakeShopResPath.TEX_JAM_ROSE;
                break;
            case "Jam_Strawberry":
                path = CakeShopResPath.TEX_JAM_STRAWBERRY;
                break;
            case "Jam_Pineapple":
                path = CakeShopResPath.TEX_JAM_PINEAPPLE;
                break;
        }

        if (_jam == null) {
            _jam = CommonUtility.InstantiateFrom(CakeShopResPath.CAKE_JAM, parent);
        }
        
        _jam.GetComponent<Renderer>().material.mainTexture = CommonUtility.LoadTexture(path);
        ResourceManager.Instance.UnloadUnusedAssets();
        DisableHandMove();
        ActiveButton();
    }

    public void ActiveButton() {
        if (!_button.gameObject.activeInHierarchy) {
            _button.SetActive(true);
            _handMove.SetActive(false);
            _handClick.SetActive(false);
        }
    }

    public void DisableHandMove() {
        _disableHandMove = true;
        _handMove.SetActive(false);
    }

    public override void PauseTimeline(float duration) {
        base.PauseTimeline(duration);
    }

    public override void PlayTimeline() {
        base.PlayTimeline();
    }

    public void OnEnd() {
        CakeShopManager.Instance.ToNextLevel(CakeShopManager.LevelState.Level_5_Eating);
    }
}

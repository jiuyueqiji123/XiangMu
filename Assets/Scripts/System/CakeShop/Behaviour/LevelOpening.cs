﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class LevelOpening : TimelineBehaviour {

    [SerializeField]
    Camera _camera;

    [SerializeField]
    Transform _guests;

    [SerializeField]
    RectTransform _canvas;

    [SerializeField]
    GameObject _effectGuest;

    [SerializeField]
    GameObject _effectLeDi;

    [SerializeField]
    BoxCollider _colliderLeDi;

    string[] _voicesDuoDuo = {
        CakeShopResPath.DuoDuo_404010104,  // 圣诞蛋糕
        CakeShopResPath.DuoDuo_404010105,  // 彩虹蛋糕
        CakeShopResPath.DuoDuo_404010106,  // 水果蛋糕
        CakeShopResPath.DuoDuo_404010107,  // 迷彩蛋糕
        CakeShopResPath.DuoDuo_404010108,  // 樱花蛋糕
        CakeShopResPath.DuoDuo_404010109   // 泡芙蛋糕
    };

    string[] _voicesXiaoAi = {
        CakeShopResPath.XiaoAi_404010116,  // 圣诞蛋糕
        CakeShopResPath.XiaoAi_404010117,  // 彩虹蛋糕
        CakeShopResPath.XiaoAi_404010118,  // 水果蛋糕
        CakeShopResPath.XiaoAi_404010119,  // 迷彩蛋糕
        CakeShopResPath.XiaoAi_404010120,  // 樱花蛋糕
        CakeShopResPath.XiaoAi_404010121   // 泡芙蛋糕
    };

    string[] _voicesXiaoQing = {
        CakeShopResPath.XiaoQing_404010110,  // 圣诞蛋糕
        CakeShopResPath.XiaoQing_404010111,  // 彩虹蛋糕
        CakeShopResPath.XiaoQing_404010112,  // 水果蛋糕
        CakeShopResPath.XiaoQing_404010113,  // 迷彩蛋糕
        CakeShopResPath.XiaoQing_404010114,  // 樱花蛋糕
        CakeShopResPath.XiaoQing_404010115   // 泡芙蛋糕
    };

    protected override void Start() {
        base.Start();

        if (_camera == null) {
            _camera = GetComponentInChildren<Camera>();
        }

        if (ScreenUtility.IsWideScreen == false) {
            _camera.fieldOfView = 38f;
        }
    }

    Animator _currentGuestAnimator;
    int _guestCount = 3;

    public void ShowGuest() {
        var guestType = (GuestType)Random.Range(0, _guestCount);
        CakeShopManager.Instance.CurrentGuestType = guestType;

        var guestRole = CommonUtility.InstantiateFrom(CakeShopResPath.PREFAB_DIR + guestType.ToString(), _guests);

        foreach (var playableBinding in playableDirector.playableAsset.outputs) {
            if (playableBinding.streamName.Equals(guestType.ToString())) {
                _currentGuestAnimator = guestRole.GetComponent<Animator>();
                playableDirector.SetGenericBinding(playableBinding.sourceObject, _currentGuestAnimator.gameObject);
                break;
            }
        }
    }

    public void PlayVoice(float duration) {
        PlayVoice(CakeShopManager.Instance.CurrentGuestType, duration);
    }

    public void PlayVoice(GuestType type, float duration) {
        PauseTimeline(duration);

        var index = Random.Range(0, 6);
        CakeShopManager.Instance.CurrentCakeType = (CakeType)index;
        _currentGuestAnimator.CrossFadeInFixedTime("Talk", 0.5f);

        var ledi = transform.Find("LeDi").GetChild(0);
        ledi.SetActive(false);
        ledi.localScale = Vector3.one * 1.2f;
        ledi.SetActive(true);

        switch (type) {
            case GuestType.DuoDuo:
                StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(_voicesDuoDuo[index]).Length, StopVoice));
                break;
            case GuestType.XiaoAi:
                StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(_voicesXiaoAi[index]).Length, StopVoice));
                break;
            case GuestType.XiaoQing:
                StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(_voicesXiaoQing[index]).Length, StopVoice));
                break;
            default:
                StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(_voicesDuoDuo[index]).Length, StopVoice));
                break;
        }

        var spriteName = "a_icon03";

        switch (index) {
            case 0:  // 圣诞
                SDKManager.Instance.Event(UmengEvent.CakeShengDan);
                SDKManager.Instance.StartLevel(UmengLevel.CakeShengDan);
                break;
            case 1:  // 彩虹
                spriteName = "a_icon05";
                SDKManager.Instance.Event(UmengEvent.CakeCaiHong);
                SDKManager.Instance.StartLevel(UmengLevel.CakeCaiHong);
                break;
            case 2:  // 水果
                spriteName = "a_icon06";
                SDKManager.Instance.Event(UmengEvent.CakeShuiGuo);
                SDKManager.Instance.StartLevel(UmengLevel.CakeShuiGuo);
                break;
            case 3:  // 迷彩
                spriteName = "a_icon01";
                SDKManager.Instance.Event(UmengEvent.CakeMicai);
                SDKManager.Instance.StartLevel(UmengLevel.CakeMicai);
                break;
            case 4:  // 樱花
                spriteName = "a_icon02";
                SDKManager.Instance.Event(UmengEvent.CakeYinghua);
                SDKManager.Instance.StartLevel(UmengLevel.CakeYinghua);
                break;
            case 5:  // 泡芙
                spriteName = "a_icon04";
                SDKManager.Instance.Event(UmengEvent.CakePaofu);
                SDKManager.Instance.StartLevel(UmengLevel.CakePaofu);
                break;
        }
  
        _canvas.SetActive(true);
        var sprite = CommonUtility.GetCakeSprite(spriteName);
        _canvas.GetChild(0).GetChild(0).GetComponent<Image>().sprite = sprite;
        ActiveEffectGuest(true);
    }

    void StopVoice() {
        _currentGuestAnimator.CrossFadeInFixedTime("Sit", 0.5f);
        StartCoroutine(TimeUtility.DelayInvoke(0.5f, PlayTimeline));
    }

    public void ActiveEffectGuest(bool isActived) {
        _effectGuest.SetActive(isActived);
        _canvas.GetChild(0).GetComponent<Button>().enabled = isActived;
    }

    public void ActiveEffectLeDi(float duration) {
        PauseTimeline(duration);
        _effectLeDi.SetActive(true);   
    }

    double _gotoTime;

    public void EnableLeDiCollider(bool isEnabled) {
        _colliderLeDi.enabled = isEnabled;

        if (isEnabled) {
            _gotoTime = playableDirector.time;
            PauseTimeline(0.5f);
        } else {
            playableDirector.time = _gotoTime;
        }
    }

    public void DisableEffectLeDi() {
        _effectLeDi.SetActive(false);
    }

    public override void PauseTimeline(float duration) {
        base.PauseTimeline(duration);
    }

    public override void PlayTimeline() {
        base.PlayTimeline();
    }

    public void OnEnd() {
        AudioManager.Instance.StopAllSound();
        ActiveEffectGuest(false);
        CakeShopManager.Instance.ToNextLevel(CakeShopManager.LevelState.Level_2_Making);
    }
}

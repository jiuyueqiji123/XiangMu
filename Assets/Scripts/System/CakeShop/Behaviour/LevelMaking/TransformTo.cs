﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformTo : MonoBehaviour {

    public Transform[] froms;
    public Transform[] tos;

    void Start() {
        for (var i = 0; i < froms.Length; i++) {
            froms[i].DOMove(tos[i].position, 1f);
            froms[i].DORotateQuaternion(tos[i].rotation, 1f);
            froms[i].DOScale(tos[i].localScale, 1f);
            froms[i].SetParent(tos[i].parent);
        }
    }
}

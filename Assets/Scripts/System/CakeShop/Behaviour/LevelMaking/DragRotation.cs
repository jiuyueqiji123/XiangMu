﻿using DG.Tweening;
using UnityEngine;

public class DragRotation : MonoBehaviour {
    public Transform jiaoBanQi;
    public Transform mix;
    public Transform effect;
    public Camera targetCamera;
    public float expendTime = 6f;

    Transform _thisTransform;
    float _totalTime;
    Vector3 _lastMousePos;
    LevelMaking _levelMaking;
    bool _disableCanvas;

    void Start() {
        _thisTransform = GetComponent<Transform>();
        _levelMaking = GetComponentInParent<LevelMaking>();
    }

    Vector2 _lastScreenPos;

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            _lastMousePos = Input.mousePosition;
            _lastScreenPos = Input.mousePosition;
            _levelMaking.ActiveCanvas(false);
        } else if (Input.GetMouseButton(0)) {
            _levelMaking.ActiveCanvas(false);

            if (Vector3.Distance(_lastMousePos, Input.mousePosition) > 5f) {
                if (!AudioManager.Instance.IsPlayingSound() && enabled) {
                    AudioManager.Instance.PlaySound(CakeShopResPath.SE_604010202);
                }
                
                var distance = Vector3.Distance(_lastMousePos, Input.mousePosition);
                _lastMousePos = Input.mousePosition;
                Ray ray = targetCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit)) {
                    jiaoBanQi.position = hit.point;
                } else {
                    var screenPos = targetCamera.WorldToScreenPoint(jiaoBanQi.position);
                    var mousePos = Input.mousePosition;
                    var worldPos = targetCamera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, screenPos.z));
                    var finalPos = new Vector3(worldPos.x, jiaoBanQi.position.y, worldPos.z);
                    var direction = _thisTransform.position - finalPos;
                    RaycastHit hit2;

                    if (Physics.Raycast(finalPos, direction, out hit2)) {
                        jiaoBanQi.position = hit2.point;
                    }
                }

                var delta = (_totalTime / expendTime);
                mix.localPosition = Vector3.Lerp(Vector3.forward * 6f, Vector3.forward * 12f, delta);
                mix.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 1.1f, delta);

                var isClockwise = CommonUtility.IsClockwise(Input.mousePosition, ref _lastScreenPos);
                effect.localEulerAngles += Vector3.forward * (isClockwise ? 1f : -1f) * distance;

                _totalTime += distance * 0.0005f;
                var thirdTime = expendTime / 3f;

                if (_totalTime >= thirdTime && _totalTime < (thirdTime + 0.1f)) {
                    mix.GetComponent<Renderer>().material.mainTexture = _levelMaking.SwitchTexture(0);
                } else if (_totalTime >= (thirdTime * 2) && _totalTime < (thirdTime * 2 + 0.1f)) {
                    mix.GetComponent<Renderer>().material.mainTexture = _levelMaking.SwitchTexture(1);
                }

                if (delta > 1f) {
                    jiaoBanQi.DOLocalMoveZ(100f, 0.5f);
                    enabled = false;
                    _levelMaking.ActiveCanvas(false);
                    _disableCanvas = true;

                    StartCoroutine(TimeUtility.DelayInvoke(2f, () => {
                        _levelMaking.OnEnd();
                    }));
                }
            }
        } else if (Input.GetMouseButtonUp(0)) {
            AudioManager.Instance.StopAllSound();
            StartCoroutine(TimeUtility.DelayInvoke(2f, () => {
                if (!_disableCanvas) {
                    _levelMaking.ActiveCanvas(true);
                }
            }));
        }
    }
}

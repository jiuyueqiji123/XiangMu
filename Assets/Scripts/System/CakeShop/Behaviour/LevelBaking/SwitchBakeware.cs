﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchBakeware : MonoBehaviour {
    public enum BakewareType {
        Square,
        Circle,
    }

    [SerializeField]
    BakewareType _bakewareType = BakewareType.Circle;
	
	void Start() {
		switch (CakeShopManager.Instance.CurrentCakeType) {
            case CakeType.ShengDan:
            case CakeType.ShuiGuo:
            case CakeType.CaiHong:
                gameObject.SetActive(_bakewareType.Equals(BakewareType.Circle));
                break;
            case CakeType.MiCai:
            case CakeType.YingHua:
            case CakeType.PaoFu:
                gameObject.SetActive(_bakewareType.Equals(BakewareType.Square));
                break;
            default:
                gameObject.SetActive(false);
                break;
        }
	}
	
}

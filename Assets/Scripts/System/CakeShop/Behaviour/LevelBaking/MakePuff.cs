﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MakePuff : MonoBehaviour {
    public static bool CanTrigger { get; set; }

    public GameObject effect;
    public Transform[] puffs;

    int _nextIndex = 1;
    Transform _thisTransform;

    void Start() {
        _thisTransform = transform;
    }

    void OnMouseDown() {
        if (!CanTrigger) {
            return;
        }

        CanTrigger = false;

        if (_nextIndex == 1) {
            GetComponentInParent<LevelBaking>().ActiveHand(false);
        }

        puffs[_nextIndex - 1].SetActive(true);
        effect.SetActive(true);
        AudioManager.Instance.PlaySound(CakeShopResPath.SE_604010306);

        StartCoroutine(TimeUtility.DelayInvoke(1f, () => {
            effect.SetActive(false);

            if (_nextIndex >= puffs.Length) {
                _thisTransform.DOLocalMoveZ(100f, 1f);
                GetComponentInParent<LevelBaking>().PlayTimeline();
                return;
            }

            var pos = puffs[_nextIndex].position;
            _thisTransform.DOMove(new Vector3(pos.x, _thisTransform.position.y, pos.z), 0.5f).OnComplete(() => {
                CanTrigger = true;
                _nextIndex += 1;
            });
        }));
    }
}

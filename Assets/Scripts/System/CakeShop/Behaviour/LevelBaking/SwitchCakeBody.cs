﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCakeBody : MonoBehaviour {
    [SerializeField]
    CakeType _cakeType = CakeType.MiCai;
	
	void Start() {
		switch (CakeShopManager.Instance.CurrentCakeType) {
            case CakeType.MiCai:
                gameObject.SetActive(_cakeType.Equals(CakeType.MiCai));
                break;
            case CakeType.YingHua:
                gameObject.SetActive(_cakeType.Equals(CakeType.YingHua));
                break;
            case CakeType.PaoFu:
                gameObject.SetActive(_cakeType.Equals(CakeType.PaoFu));
                break;
            default:
                gameObject.SetActive(false);
                break;
        }
	}
	
}

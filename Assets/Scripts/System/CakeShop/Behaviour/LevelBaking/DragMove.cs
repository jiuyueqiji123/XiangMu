﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragMove : MonoBehaviour {
    public Transform butter;
    public Transform oil;
    public Camera targetCamera;
    public float expendTime = 5f;
    public float scaleEndValue = 1f;
    public GameObject _canvas;

    Transform _thisTransform;
    float _totalTime;
    Vector3 _lastMousePos;
    LevelBaking _levelBaking;
    bool _disableCanvas;
    GameAudioSource _gas;

    void Start() {
        _thisTransform = GetComponent<Transform>();
        _levelBaking = GetComponentInParent<LevelBaking>();
    }

    void Update() {
        if (_disableCanvas) {
            return;
        }

        if (Input.GetMouseButtonDown(0)) {
            _lastMousePos = Input.mousePosition;
            _canvas.SetActive(false);
        } else if (Input.GetMouseButton(0)) {
            _canvas.SetActive(false);

            if (Vector3.Distance(_lastMousePos, Input.mousePosition) > 5f) {
                if (_gas == null) {
                    _gas = AudioManager.Instance.PlaySound(CakeShopResPath.SE_604010208, true, true);
                }

                var distance = Vector3.Distance(_lastMousePos, Input.mousePosition);
                _lastMousePos = Input.mousePosition;
                Ray ray = targetCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit)) {
                    butter.position = hit.point;
                } else {
                    var screenPos = targetCamera.WorldToScreenPoint(butter.position);
                    var mousePos = Input.mousePosition;
                    var worldPos = targetCamera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, screenPos.z));
                    var finalPos = new Vector3(worldPos.x, butter.position.y, worldPos.z);
                    var direction = _thisTransform.position - finalPos;
                    RaycastHit hit2;

                    if (Physics.Raycast(finalPos, direction, out hit2)) {
                        butter.position = hit2.point;
                    }
                }

                var delta = _totalTime / expendTime;
                butter.localScale = Vector3.Lerp(Vector3.one, new Vector3(0f, 0f, 0.5f), delta);
                oil.localScale = Vector3.Lerp(new Vector3(0.2f, 0.2f, 1f), new Vector3(scaleEndValue, scaleEndValue, 1f), delta);
                _totalTime += distance * 0.0005f; // 变化速度

                if (delta >= 1f) {
                    _disableCanvas = true;

                    StartCoroutine(TimeUtility.DelayInvoke(1f, () => {
                        _levelBaking.SwitchCamera();
                        _levelBaking.PlayTimeline();
                    }));
                }
            }
        } else if (Input.GetMouseButtonUp(0)) {
            if (_gas != null) {
                _gas.Stop();
                _gas = null;
            }
            
            StartCoroutine(TimeUtility.DelayInvoke(2f, () => {
                if (!_disableCanvas) {
                    _canvas.SetActive(true);
                }
            }));
        }
    }
}

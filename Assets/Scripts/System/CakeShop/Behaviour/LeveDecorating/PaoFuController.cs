﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PaoFuController : MonoBehaviour {
    public static bool IsShowButton {
        get { return _count >= 4; }
    }

    static int _count;

    [SerializeField]
    Transform _up;

    [SerializeField]
    Transform _cream;

    [SerializeField]
    Vector3 _upEndPosition;

    [SerializeField]
    Vector3 _upEndRotation;

    [SerializeField]
    Vector3 _upEndScale;

    Material _mat;

    void Start() {
        _mat = _cream.GetComponent<Renderer>().material;
    }

    public void OnEnter() {
        _up.DOLocalMoveZ(9f, 0.5f);
    }

    public void OnExit() {
        if (_cream.gameObject.activeInHierarchy) {
            _up.DOLocalMove(_upEndPosition, 0.5f);
            _up.DOLocalRotate(_upEndRotation, 0.5f);
            _up.DOScale(_upEndScale, 0.5f);
        } else {
            _up.DOLocalMoveZ(3f, 0.5f);
        }
    }

    public void ShowCream(CreamColor color, Action callback) {
        _cream.SetActive(false);

        switch (color) {
            case CreamColor.White:
                _mat.mainTexture = CommonUtility.LoadTexture(CakeShopResPath.PAOFU_WHITE);
                break;
            case CreamColor.Pink:
                _mat.mainTexture = CommonUtility.LoadTexture(CakeShopResPath.PAOFU_PINK);
                break;
            case CreamColor.Blue:
                _mat.mainTexture = CommonUtility.LoadTexture(CakeShopResPath.PAOFU_BLUE);
                break;
            case CreamColor.Orange:
                _mat.mainTexture = CommonUtility.LoadTexture(CakeShopResPath.PAOFU_ORANGE);
                break;
        }

        _cream.SetActive(true);
        ResourceManager.Instance.UnloadUnusedAssets();

        StartCoroutine(TimeUtility.DelayInvoke(0.5f, () => {
            _count++;
            OnExit();

            if (callback != null) {
                callback();
            }
        }));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchDecoration : MonoBehaviour {
    [SerializeField]
    GameObject[] objs;

    List<string> _commons = new List<string>(new string[] {
        "CandyUmbrella", "Christmas", "Snowman", "Snow",
        "Cherry", "Strawberry", "Carambola", "Pitaya", "Grape", "Apple",
        "EggRoll", "CandySunflower", "Leaf"
    });
    List<string> _micais = new List<string>(new string[] { "Chocolate", "Colorful" });
    List<string> _yinghuas = new List<string>(new string[] { "Macaron_Pink", "Macaron_Blue", "Macaron_Brown" });
    List<string> _paofus = new List<string>();

    void Start() {
        var list = new List<string>();

        switch (CakeShopManager.Instance.CurrentCakeType) {
            case CakeType.ShengDan:
            case CakeType.ShuiGuo:
            case CakeType.CaiHong:
                list = _commons;
                break;
            case CakeType.MiCai:
                list = _micais;
                break;
            case CakeType.YingHua:
                list = _yinghuas;
                break;
            case CakeType.PaoFu:
                list = _paofus;
                break;
        }

        foreach (var item in objs) {
            item.SetActive(list.Contains(item.name));
        }
    }
}

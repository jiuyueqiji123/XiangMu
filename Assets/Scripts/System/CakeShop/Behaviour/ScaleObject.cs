﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ScaleObject : MonoBehaviour {
    public float startValue = 1f;
    public float endVlaue = 0.7f;
    public float duration = 0.5f;
    public bool loop;
    public bool enableGrow;
    public float growTime = 0.1f;

    Transform _thisTransform;

	void Start() {
        if (enableGrow) {           
            _thisTransform.DOScale(startValue, growTime).OnComplete(OnEnable);
        } else {
            OnEnable();
        }
    }

    void OnEnable() {
        _thisTransform = transform;

        if (loop) {
            StartCoroutine(TimeUtility.DelayInvoke(1f, Play));
        } else {
            PlayOneTime();
        }
    }

    void PlayOneTime() {
        _thisTransform.DOScale(Vector3.one * endVlaue, duration);
    }

    void Play() {
        _thisTransform.DOScale(Vector3.one * endVlaue, duration).OnComplete(Rewind);
    }

    void Rewind() {
        _thisTransform.DOScale(Vector3.one * startValue, duration).OnComplete(Play);
    }

    void OnDisable() {
        _thisTransform.localScale = Vector3.one * startValue;
        _thisTransform.DOKill();
        StopAllCoroutines();
    }
}

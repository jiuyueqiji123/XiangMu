﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickTrigger : MonoBehaviour {

	void OnMouseDown() {
        switch (CakeShopManager.Instance.CurrentLevelState) {
            case CakeShopManager.LevelState.Level_1_Opening:
                var levelOpening = GetComponentInParent<LevelOpening>();

                if (!levelOpening.IsTimelinePlaying()) {
                    levelOpening.PlayTimeline();
                    levelOpening.DisableEffectLeDi();

                    // 防止二次触发
                    GetComponent<Collider>().enabled = false;
                }
                break;
            case CakeShopManager.LevelState.Level_2_Making:
                var levelMaking = GetComponentInParent<LevelMaking>();

                if (!levelMaking.IsTimelinePlaying()) {
                    levelMaking.PlayTimeline();
                    levelMaking.PlayEggAnimation(transform);
                    levelMaking.StopTips();

                    // 防止二次触发
                    GetComponent<Collider>().enabled = false;
                }
                break;
            case CakeShopManager.LevelState.Level_3_Baking:
                var levelBaking = GetComponentInParent<LevelBaking>();

                if (!levelBaking.IsTimelinePlaying()) {
                    levelBaking.PlayTimeline();
                    levelBaking.ActiveHand(false);

                    // 防止二次触发
                    GetComponent<Collider>().enabled = false;
                }
                break;
            case CakeShopManager.LevelState.Level_4_Decorating:

                // 防止二次触发
                GetComponent<Collider>().enabled = false;
                break;
            case CakeShopManager.LevelState.Level_5_Eating:
                var levelEating = GetComponentInParent<LevelEating>();

                if (!levelEating.IsTimelinePlaying()) {
                    levelEating.PlayTimeline();
                    levelEating.ActiveEffect(false);

                    // 防止二次触发
                    GetComponent<Collider>().enabled = false;
                }
                break;
        }
    }
}

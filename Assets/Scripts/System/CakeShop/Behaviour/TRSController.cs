﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class TRSController : MonoBehaviour {
    public float duration = 0.5f;
    public TweenPosition position;
    public TweenRotation rotation;
    public TweenScale scale;

    Transform _thisTransform;

	void Start() {
        _thisTransform = transform;
	}
	
	void Update() {
		if (position.loop) {
            position.loop = false;
            LoopTweenPosition(() => position.loop = true);
        }

        if (rotation.loop) {
            rotation.loop = false;
            LoopTweenRotation(() => rotation.loop = true);
        }

        if (scale.loop) {
            scale.loop = false;
            LoopTweenScale(() => scale.loop = true);
        }
	}

    public void StayOnEnd() {
        _thisTransform.DOKill();
        _thisTransform.DOLocalMove(position.to, 0f);
    }

    public void StayOnStart() {
        _thisTransform.DOKill();
        _thisTransform.DOLocalMove(position.form, 0f);
    }

    public void PlayTweenPosition(bool isReverse = false) {
        if (isReverse) {
            _thisTransform.DOLocalMove(position.form, duration);
        } else {
            _thisTransform.DOLocalMove(position.to, duration);
        }
    }

    public void LoopTweenPosition(TweenCallback callback, bool isReverse = false) {
        if (isReverse) {
            _thisTransform.DOLocalMove(position.to, 0f);
            _thisTransform.DOLocalMove(position.form, duration).OnComplete(callback);
        } else {
            _thisTransform.DOLocalMove(position.form, 0f);
            _thisTransform.DOLocalMove(position.to, duration).OnComplete(callback);
        }
    }

    public void PlayTweenRotation(bool isReverse = false) {
        if (isReverse) {
            _thisTransform.DOLocalRotate(rotation.form, duration);
        } else {
            _thisTransform.DOLocalRotate(rotation.to, duration);
        }
    }

    public void LoopTweenRotation(TweenCallback callback, bool isReverse = false) {
        if (isReverse) {
            _thisTransform.DOLocalRotate(rotation.to, 0f);
            _thisTransform.DOLocalRotate(rotation.form, duration).OnComplete(callback);
        } else {
            _thisTransform.DOLocalRotate(rotation.form, 0f);
            _thisTransform.DOLocalRotate(rotation.to, duration).OnComplete(callback);
        }
    }

    public void PlayTweenScale(bool isReverse = false) {
        if (isReverse) {
            _thisTransform.DOScale(scale.form, duration);
        } else {
            _thisTransform.DOScale(scale.to, duration);
        }
    }

    public void LoopTweenScale(TweenCallback callback, bool isReverse = false) {
        if (isReverse) {
            _thisTransform.DOScale(scale.to, 0f);
            _thisTransform.DOScale(scale.form, duration).OnComplete(callback);
        } else {
            _thisTransform.DOScale(scale.form, 0f);
            _thisTransform.DOScale(scale.to, duration).OnComplete(callback);
        }
    }
}

[System.Serializable]
public class TweenPosition {
    public bool loop;
    public Vector3 form;
    public Vector3 to;
}

[System.Serializable]
public class TweenRotation {
    public bool loop;
    public Vector3 form;
    public Vector3 to;
}

[System.Serializable]
public class TweenScale {
    public bool loop;
    public Vector3 form;
    public Vector3 to;
}

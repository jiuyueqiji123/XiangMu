﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CJFX.CakeShop {
    public class UpdateManager : MonoSingleton<UpdateManager> {
        List<IUpdate> _updateList = new List<IUpdate>();

        void Update() {
            for (var i = 0; i < _updateList.Count; i++) {
                _updateList[i].OnUpdate();
            }
        }

        public void Add(IUpdate call) {
            if (!_updateList.Contains(call)) {
                _updateList.Add(call);
            }
        }

        public void Remove(IUpdate call) {
            if (_updateList.Contains(call)) {
                _updateList.Remove(call);
            }
        }
    }
}
﻿namespace CJFX.CakeShop {
    public interface IUpdate {
        void OnUpdate();
    }
}

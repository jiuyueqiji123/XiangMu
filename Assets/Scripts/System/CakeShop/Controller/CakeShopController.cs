﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CakeShopController : Singleton<CakeShopController> {
    private const string _VIEW_PATH = CakeShopResPath.VIEW_DIR + "ReturnForm";
    private ReturnView _view;

    public override void Init() {
        base.Init();
        _view = new ReturnView();
    }

    public override void UnInit() {
        base.UnInit();
    }

    public void OpenView() {
        _view.OpenForm(_VIEW_PATH);
        RegisterEvent();
        Input.multiTouchEnabled = false;
    }

    public void CloseView() {
        _view.CloseForm();
        RemoveEvent();
        CakeShopManager.Instance.OnExit();
        CakeShopManager.DestroyInstance();
        Input.multiTouchEnabled = true;
    }

    private void RegisterEvent() {
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.CakeShop_Btn_Back, ClickBtnBack);
    }

    private void RemoveEvent() {
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.CakeShop_Btn_Back, ClickBtnBack);
    }

    private void ClickBtnBack(HUIEvent e) {
        CakeShopManager.Instance.ToLastLevel();
    }
}

﻿/// <summary>
/// 蛋糕种类
/// </summary>
public enum CakeType {
    // 圣诞
    ShengDan = 0,

    // 彩虹
    CaiHong = 1,

    // 水果
    ShuiGuo = 2,

    // 迷彩
    MiCai = 3,

    // 樱花
    YingHua = 4,

    // 泡芙
	PaoFu = 5,
}

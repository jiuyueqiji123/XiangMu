﻿/// <summary>
/// 客人类型：小爱、小青、多多
/// </summary>
public enum GuestType {
    XiaoAi = 0,
	XiaoQing = 1,
    DuoDuo = 2,
}

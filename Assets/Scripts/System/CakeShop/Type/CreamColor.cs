﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CreamColor {
    White,
    Pink,
    Blue,
    Orange,
}

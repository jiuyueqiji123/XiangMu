﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.U2D;

public class CakeShopManager : MonoSingleton<CakeShopManager> {
    public enum LevelState {
        Level_1_Opening,
        Level_2_Making,
        Level_3_Baking,
        Level_4_Decorating,
        Level_5_Eating,
    }

    [SerializeField]
    GameObject _currentLevelObject;

    [SerializeField]
    LevelState _currentLevelState = LevelState.Level_1_Opening;

    [SerializeField]
    CakeType _currentCakeType = CakeType.ShengDan;

    [SerializeField]
    GuestType _currentGuestType = GuestType.DuoDuo;

    public GuestType CurrentGuestType {
        get { return _currentGuestType; }
        set { _currentGuestType = value; }
    }

    public CakeType CurrentCakeType {
        get { return _currentCakeType; }
        set { _currentCakeType = value; }
    }

    public LevelState CurrentLevelState {
        get { return _currentLevelState; }
        private set { _currentLevelState = value; }
    }

    protected override void Awake() {
        base.Awake();

        if (_currentLevelObject == null) {
            PlayableAsset playableAsset;
            LoadLevelStart(LevelState.Level_1_Opening, out playableAsset);
            LoadLevelEnd(playableAsset);
        } else {
            PlayBGM();
            _currentLevelObject.GetComponent<PlayableDirector>().Play();
        }
    }

    void DestroyLevel() {
        if (_currentLevelObject) {
            Destroy(_currentLevelObject);
            _currentLevelObject = null;
        }
    }

    public void ToNextLevel(LevelState nextLevel) {
        PlayableAsset playableAsset = null;

        TransitionManager.Instance.StartTransition(() => {
            LoadLevelStart(nextLevel, out playableAsset);
        }, 
        () => {
            LoadLevelEnd(playableAsset);
        });
    }

    void LoadLevelStart(LevelState nextLevel, out PlayableAsset playableAsset) {
        PauseBGM();
        DestroyLevel();
        CurrentLevelState = nextLevel;
        playableAsset = null;

        switch (nextLevel) {
            case LevelState.Level_1_Opening:
                _currentLevelObject = Instantiate(CommonUtility.LoadPrefab(CakeShopResPath.LEVEL_1));
                playableAsset = CommonUtility.LoadTimeline(CakeShopResPath.TL_LEVEL_1);
                break;
            case LevelState.Level_2_Making:
                _currentLevelObject = Instantiate(CommonUtility.LoadPrefab(CakeShopResPath.LEVEL_2));
                playableAsset = CommonUtility.LoadTimeline(CakeShopResPath.TL_LEVEL_2);
                break;
            case LevelState.Level_3_Baking:
                _currentLevelObject = Instantiate(CommonUtility.LoadPrefab(CakeShopResPath.LEVEL_3));
                playableAsset = CommonUtility.LoadTimeline(CakeShopResPath.TL_LEVEL_3);
                break;
            case LevelState.Level_4_Decorating:
                _currentLevelObject = Instantiate(CommonUtility.LoadPrefab(CakeShopResPath.LEVEL_4));
                playableAsset = CommonUtility.LoadTimeline(CakeShopResPath.TL_LEVEL_4);
                break;
            case LevelState.Level_5_Eating:
                _currentLevelObject = Instantiate(CommonUtility.LoadPrefab(CakeShopResPath.LEVEL_5));
                playableAsset = CommonUtility.LoadTimeline(CakeShopResPath.TL_LEVEL_5);
                break;
        }
    }

    void LoadLevelEnd(PlayableAsset playableAsset) {
        PlayBGM();

        var pd = _currentLevelObject.GetComponent<PlayableDirector>();
        pd.playableAsset = playableAsset;
        pd.Play();
    }

    public void PlayBGM() {
        AudioManager.Instance.PlayMusic(CakeShopResPath.BGM_604010101, true, true);
    }

    public void PauseBGM() {
        AudioManager.Instance.PauseBackgroudMusic();
    } 

    public void StopBGM() {
        AudioManager.Instance.StopBackgroundMusic();
    }

    public void OnExit() {
        switch (CurrentCakeType) {
            case CakeType.ShengDan:
                SDKManager.Instance.FinishLevel(UmengLevel.CakeShengDan);
                break;
            case CakeType.CaiHong:
                SDKManager.Instance.FinishLevel(UmengLevel.CakeCaiHong);
                break;
            case CakeType.ShuiGuo:
                SDKManager.Instance.FinishLevel(UmengLevel.CakeShuiGuo);
                break;
            case CakeType.MiCai:
                SDKManager.Instance.FinishLevel(UmengLevel.CakeMicai);
                break;
            case CakeType.YingHua:
                SDKManager.Instance.FinishLevel(UmengLevel.CakeYinghua);
                break;
            case CakeType.PaoFu:
                SDKManager.Instance.FinishLevel(UmengLevel.CakePaofu);
                break;
        }
    }

    bool _isHandling = false;

    public void ToLastLevel() {
        if (_isHandling) {
            return;
        }

        _isHandling = true;

        if (CurrentLevelState == LevelState.Level_1_Opening) {
            GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
        } else {
            ToNextLevel(LevelState.Level_1_Opening);
        }

        StartCoroutine(TimeUtility.DelayInvoke(1f, () => _isHandling = false));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PuzzleGaneController : ReceiverBaseMono,IEasyLoading,IEasyEffect{


    private Transform GameRoot;

    private Camera GameCamera;

    private Transform Cheshen;

    public Transform chelun;

    public enum CheLun
    {
        zuoqian,
        youqian,
        zuohou,
        youhou,
        daoxiang,
    }

    protected override void Initialize()
    {
        base.Initialize();
        GameRoot = this.gameObject.transform;
        GameCamera = GameRoot.Find("Camera").GetComponent<Camera>();
        Cheshen = GameRoot.Find("cheshen");
        chelun = GameRoot.Find("lunzi");
        GameCamera.enabled = false;


    }

    protected override void OnGameStart()
    {
        base.OnGameStart();
        GameCamera.enabled = true;
        
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }




    public void CheshenRotate(CheLun cheLun)
    {
        switch (cheLun)
        {
            case CheLun.zuoqian:
                Cheshen.DORotate(new Vector3(-3.388f, -2.63f, -30.25f),0.5f);
                break;
            case CheLun.zuohou:
                Cheshen.DORotate(new Vector3(12.133f, 4.804f, 41.765f), 0.5f);
                break;
            case CheLun.youqian:
                Cheshen.DORotate(new Vector3(8.737f, -0.169f, -135.905f), 0.5f);
                break;
            case CheLun.youhou:
                Cheshen.DORotate(new Vector3(-12.111f, -0.507f, -217.286f), 0.5f);
                break;
            case CheLun.daoxiang:
                Cheshen.DORotate(new Vector3(0, 45f, -90f), 0.5f);
                break;
        }
    }
}

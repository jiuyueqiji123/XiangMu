﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyGameWindow : SingletonBaseWindow<FlyGameWindow> {


    public Transform _xunzhang { get; set; }

    Transform xunzhangclone;
    Transform xunzhangparent;
    public float Timer = 0;

    protected override void AddListeners()
    {
        base.AddListeners();
    }


    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);
        xunzhangclone = _xunzhang;
        xunzhangparent = xunzhangclone.parent;
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        Timer += Time.deltaTime;
        if (Timer >= 1)
        {
            
            
            
            xunzhangclone = GameObject.Instantiate(xunzhangclone);

            


            xunzhangclone.SetParent(xunzhangparent);

            
            xunzhangclone.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(Random.Range(-650,650),583,0);
            xunzhangclone.localScale = new Vector3(1,1,1);
            Timer = 0;
        }

    }

    protected override void OnClose()
    {
        base.OnClose();
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();
    }

}

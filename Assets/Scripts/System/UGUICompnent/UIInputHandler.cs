﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIInputHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    public event System.Action OnClick;
    public event System.Action OnDown;
    public event System.Action OnUp;
    public event System.Action OnEnter;
    public event System.Action OnExit;
    //public event System.Action<PointerEventData> OnClick;
    //public event System.Action<PointerEventData> OnDown;
    //public event System.Action<PointerEventData> OnUp;
    //public event System.Action<PointerEventData> OnEnter;
    //public event System.Action<PointerEventData> OnExit;


    public void OnPointerClick(PointerEventData eventData)
    {
        //Debug.LogError("OnPointerClick");
        if (OnClick != null)
            OnClick();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //Debug.LogError("OnPointerDown");
        if (OnDown != null)
            OnDown();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.LogError("OnPointerEnter");
        if (OnEnter != null)
            OnEnter();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.LogError("OnPointerExit");
        if (OnExit != null)
            OnExit();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //Debug.LogError("OnPointerUp");
        if (OnUp != null)
            OnUp();
    }
}

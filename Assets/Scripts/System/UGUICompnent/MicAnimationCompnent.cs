﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MicAnimationCompnent : MonoBehaviour {


    public Sprite[] bgSpriteArray;
    public Sprite[] volumeSpriteArray;

    public float bgIntervalTime = 0.3f;

    private float m_bgIntervalAddTime = 0.1f;
    private float m_volumeRate;

    private int m_bgIndex;
    private int m_volumeIndex;

    private Image m_img;
    private Image m_volumeImg;

    [Range(0, 1)]
    public float volume;

	// Use this for initialization
	void Start () {

        m_img = GetComponent<Image>();
        m_volumeImg = transform.GetChild(0).GetComponent<Image>();
        m_volumeRate = 0.9f / volumeSpriteArray.Length;
    }
	
	// Update is called once per frame
	void Update () {

        m_bgIntervalAddTime += Time.deltaTime;
        if (m_bgIntervalAddTime > bgIntervalTime)
        {
            m_bgIntervalAddTime = 0;
            ChangeBgImage();
        }

        UpdateVolumeImage();
    }

    public void UpdateVolume(float value)
    {
        volume = value;
    }

    private void ChangeBgImage()
    {
        if (bgSpriteArray.Length == 0)
            return;


        if (m_bgIndex > bgSpriteArray.Length - 1)
        {
            m_bgIndex = 0;
        }
        m_img.sprite = bgSpriteArray[m_bgIndex];
        m_bgIndex++;
    }

    private void UpdateVolumeImage()
    {
        if (volume < 0.1f)
        {
            m_volumeImg.gameObject.SetActive(false);
            return;
        }
        m_volumeImg.gameObject.SetActive(true);
        m_volumeIndex = (int)((volume - 0.1f) / m_volumeRate);

        if (m_volumeIndex > volumeSpriteArray.Length - 1)
            m_volumeIndex = volumeSpriteArray.Length - 1;
        m_volumeImg.sprite = volumeSpriteArray[m_volumeIndex];
    }

}

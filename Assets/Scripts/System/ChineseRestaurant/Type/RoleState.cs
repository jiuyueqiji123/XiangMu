﻿public enum RoleState {
    OrderDishes,
    ServingDishes,
    EatUpDishes,
}

﻿public enum CondimentName {
    Salt,
    Sauce,
    Chicken,
    Shallot,
    Pepper,
    Garlic,
}

﻿public enum FoodName {
    Bacon,
    Carrot,
    Cucumber,
	Fish,
    GreenPepper,
    GreenVegetable,
    Ham,
    Pork,
    Shiitake,
    Shrimp,
    Tofu,
}

﻿using UnityEngine;

public class UpdateableComponent : MonoBehaviour, IUpdateable {

    void Start() {
        GameLogic.Instance.RegisterUpdateableObject(this);
        OnInitialize();
    }

    void OnDestroy() {
        if (GameLogic.HasInstance()) {
            GameLogic.Instance.DeregisterUpdateableObject(this);
        }

        OnRelease();
    }

    public virtual void OnUpdate(float dt) {

    }

    /// <summary>
    /// Derived classes should override this method for initialization code, and NOT reimplement Start()
    /// </summary>
    protected virtual void OnInitialize() {

    }

    protected virtual void OnRelease() {

    }
}

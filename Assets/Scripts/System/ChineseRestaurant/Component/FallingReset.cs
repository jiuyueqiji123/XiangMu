﻿using UnityEngine;

public class FallingReset : MonoBehaviour {
    public Transform target;
    public float height = 0.5f;

    void OnTriggerExit(Collider other) {
        if (other.attachedRigidbody) {
            other.attachedRigidbody.velocity = Vector3.zero;

            if (target) {
                other.attachedRigidbody.position = target.position + (target.up * height);
            } else {
                var t = other.transform.parent;
                other.attachedRigidbody.position = t.position + (t.up * height);
            }
        }
    }
}

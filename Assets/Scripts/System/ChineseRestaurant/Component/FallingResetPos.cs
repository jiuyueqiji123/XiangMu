﻿using UnityEngine;

public class FallingResetPos : MonoBehaviour {
    public float height = 0.5f;

    void OnTriggerEnter(Collider other) {
        if (other.transform.parent && other.transform.parent.name == "Falling Reset Layer") {
            var rigidbody = transform.GetComponent<Rigidbody>();
            rigidbody.velocity = Vector3.zero;
            var parent = transform.parent;
            rigidbody.position = parent.position + (parent.up * height);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemFate : MonoBehaviour {
    ParticleSystem _ps;

	void Start() {
        _ps = GetComponentInChildren<ParticleSystem>();
	}
	
	void Update() {
        if (!_ps.IsAlive()) {
            Destroy(gameObject);
        }
	}
}

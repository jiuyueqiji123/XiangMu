﻿using UnityEngine;
using DG.Tweening;

public class ShakeFX : MonoBehaviour {
    public float duration = 1f;
    public Vector3 strength = new Vector3(0f, 30f, 0f);
    public int vibrato = 10;

	void Start() {
        transform.DOShakeRotation(duration, strength, vibrato).OnComplete(() => Destroy(this));
    }
	
}

﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

public class DragFood : EventBehaviour {
    Transform _transform;
    BoxCollider _collider;
    Vector3 _originPosition;
    float _planeDistance = 0.25f;
    bool _hasPlayed;

    static Transform _canvas;
    static Camera _camera;
    static Vector3 _canvasScreenPos;

    void Start() {
        _transform = GetComponent<Transform>();
        _collider = GetComponent<BoxCollider>();
        _originPosition = _transform.position;
    }

    public override void OnBeginDrag(PointerEventData eventData) {
        _collider.enabled = false;

        if (_canvas == null) {
            _canvas = LevelSecond.Instance.canvasSelect;
            _camera = _canvas.GetComponent<Canvas>().worldCamera;
            _canvasScreenPos = _camera.WorldToScreenPoint(_canvas.position);

            AudioManager.Instance.StopAllSound();
        }

        if (!_hasPlayed) {
            // name 的格式为：Parent_Shiitake(Clone)，即{0}_{1}(Clone)
            // 为了获得 {1} 需要进行如下拆分：
            var objName = name.Split('_')[1].Split('(')[0];
            var foodName = (FoodName)System.Enum.Parse(typeof(FoodName), objName);
            var path = ChineseRestaurantManager.Instance.GetVoicePathFrom(foodName);
            AudioManager.Instance.PlaySound(path);
            _hasPlayed = true;
        }
    }

    public override void OnDrag(PointerEventData eventData) {
        _transform.position = _camera.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, _canvasScreenPos.z - _planeDistance));
    }

    public override void OnEndDrag(PointerEventData eventData) {
        var go = eventData.pointerCurrentRaycast.gameObject;

        if (go) {
            var image = go.transform.parent.GetComponent<Image>();

            if (image) {
                var spriteName = image.sprite.name;
                spriteName = spriteName.Substring(0, spriteName.LastIndexOf('_'));
                var foodName = ChineseRestaurantManager.Instance.GetFoodNameFrom(spriteName).ToString();
 
                if (_transform.name.Contains(foodName)) {
                    AudioManager.Instance.PlaySound(ChineseRestaurantResPath.SFX_SELECT_CORRECT);
                    _transform.DOMove(image.rectTransform.position, 0.1f).OnComplete(() => {
                        var vfx = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.VFX_SELECT_CORRECT, null);
                        vfx.position = go.transform.position + new Vector3(0.02f, 0f,0.1f);

                        image.sprite = CommonUtility.GetChineseRestaurantSprite(spriteName);
                        go.GetComponent<Image>().raycastTarget = false;

                        _transform.localScale = Vector3.zero;
                        _transform.position = _originPosition;
                        _transform.DOScale(Vector3.one, 0.5f).OnComplete(() => _collider.enabled = true);

                        LevelSecond.Instance.FoodMaterialCount -= 1;

                        if (LevelSecond.Instance.FoodMaterialCount <= 0) {
                            LevelSecond.Instance.ToStateCut();
                        }
                    });
                    return;
                }
            }
        }

        AudioManager.Instance.PlaySound(ChineseRestaurantResPath.SFX_SELECT_ERROR);
        _transform.DOMove(_originPosition, 0.5f).OnComplete(() => _collider.enabled = true);
    }
}

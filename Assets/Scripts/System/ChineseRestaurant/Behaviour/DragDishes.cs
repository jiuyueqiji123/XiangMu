﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class DragDishes : EventBehaviour {

    Transform _transform;
    bool _hasDragged;
    bool _canDestroy;
    RoleAnimation _roleAnimation;
    Vector3 _originPos;
    LevelFirst _levelFirst;
    float _handTipInterval = 8f;
    float _lastTime;

    void Start() {
        _transform = GetComponent<Transform>(); 
        _roleAnimation = FindObjectOfType<RoleAnimation>();
        _originPos = _transform.position;
        _levelFirst = FindObjectOfType<LevelFirst>();
        _lastTime = Time.time;
    }

    void Update() {
        if (_hasDragged == false && (Time.time - _lastTime) >= _handTipInterval) {
            _levelFirst.hand.SetActive(true);
            _lastTime = Time.time;
        }
    }

    public override void OnPointerClick(PointerEventData eventData) {
        if (_canDestroy) {
            Hide();
        }
    }

    public override void Hide() {
        _transform.DOScale(Vector3.zero, 0.3f).OnComplete(() => Destroy(gameObject));
    }

    public override void OnDrag(PointerEventData eventData) {
        if (_hasDragged) {
            return;
        }

        _hasDragged = true;
        _levelFirst.hand.SetActive(false);

        _transform.DOLocalMoveZ(-0.25f, 0.5f).OnComplete(() => {
            float eatDuration = 0f;
            float startTime = Time.time;
            Transform child = _transform.GetChild(0);
            GameAudioSource gas = null;
            var currentDishName = ChineseRestaurantManager.Instance.currentDishName;

            // 第一次吃掉菜的三分之一
            StartCoroutine(TimeUtility.DelayInvoke(0.5f, () => {
                if (currentDishName == DishName.QingCaiDouFuTang) {
                    child.localPosition -= Vector3.up * 0.04f;
                    child.localScale = Vector3.one;
                } else {
                    child.localScale = (_transform.localScale / 3) * 2;
                }

                gas = AudioManager.Instance.PlaySound(ChineseRestaurantResPath.SFX_EAT, true, true);
            }));

            _roleAnimation.Eat().OnFinished(() => {
                eatDuration = Time.time - startTime;

                // 第二次吃掉菜的二分之一
                StartCoroutine(TimeUtility.DelayInvoke(0.5f, () => {
                    if (currentDishName == DishName.QingCaiDouFuTang) {
                        child.localPosition -= Vector3.up * 0.04f;
                        child.localScale = Vector3.one * 0.8f;
                    } else {
                        child.localScale /= 2;
                    }
                }));

                StartCoroutine(TimeUtility.DelayInvoke(eatDuration, () => {
                    // 第三次吃完剩余的菜
                    StartCoroutine(TimeUtility.DelayInvoke(0.5f, () => {
                        if (currentDishName == DishName.QingCaiDouFuTang) {
                            child.localPosition -= Vector3.up * 0.04f;
                            child.localScale = Vector3.zero;
                        } else {
                            child.localScale = Vector3.zero;
                        }
                    }));

                    StartCoroutine(TimeUtility.DelayInvoke(eatDuration, () => {
                        if (gas != null) {
                            gas.Stop();
                        }

                        _canDestroy = true;
                        _transform.DOMove(_originPos, 0.5f);

                        _roleAnimation.Praise(ChineseRestaurantManager.Instance.GetVoicePathFrom(RoleState.EatUpDishes), () => {
                            _roleAnimation.Pay(ChineseRestaurantResPath.SFX_PAY).OnFinished(() => _levelFirst.cameraAnimation.Reverse().OnFinished(() => {
                                _levelFirst.Leave();
                            }));

                            var mc = FindObjectOfType<MoneyController>();

                            if (mc) {
                                mc.Show();
                            }
                        });
                    }));
                }));
            });
        });
    }
}

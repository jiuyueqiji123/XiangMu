﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFX : MonoBehaviour {
    DragCondiment _dc;

    void Start() {
        _dc = GetComponentInParent<DragCondiment>();
    }

    void OnTriggerEnter(Collider other) {
        if (other.name == "Collider") {
            _dc.SetFallingState(true);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.name == "Collider") {
            _dc.SetFallingState(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TwinkleFX : MonoBehaviour {
    public float duration = 1f;
    public float endValue = 0f;

    Material _mat;

    void Awake() {
        _mat = transform.GetComponent<Renderer>().material;
    }

	void OnEnable() {
        _mat.DOFade(1f, 0).OnComplete(() => _mat.DOFade(1f, duration / 2).OnComplete(() => _mat.DOFade(endValue, duration).OnComplete(OnEnable)));
	}

    void OnDisable() {
        _mat.DOKill();
    }
}

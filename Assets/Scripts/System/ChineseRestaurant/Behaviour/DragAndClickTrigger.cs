﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class DragAndClickTrigger : EventBehaviour {
    public static bool IsDraging {
        get;
        private set;
    }

    Transform _target;
    Transform _transform;
    Vector3 _originPosition;
    Vector3 _endPos;
    Vector3 _targetScreenPos;
    Camera _camera;
    Rigidbody _rigidbody;
    Collider _collider;
    float _height = 0.2f;
    bool _isMoveToTarget;

    public static bool IsMoving {
        get; private set;
    }

    void Start() {
        _transform = GetComponent<Transform>();
        _originPosition = _transform.position;
        _target = _transform.root.Find("Cutting Board");
        _camera = _transform.root.GetComponentInChildren<Camera>();
        _endPos = _target.position + (Vector3.up * _height);
        _targetScreenPos = _camera.WorldToScreenPoint(_endPos);
        _rigidbody = GetComponent<Rigidbody>();
        _collider = GetComponent<Collider>();
        IsMoving = false;
    }

    void OnTriggerEnter(Collider collider) {
        if (collider.transform.Equals(_target)) {
            _isMoveToTarget = true;
        }
    }

    void OnTriggerExit(Collider collider) {
        if (collider.transform.Equals(_target)) {
            _isMoveToTarget = false;
        }
    }

    public override void OnBeginDrag(PointerEventData eventData) {
        if (SlicedTarget.Targets.Count > 0 || IsMoving) {
            return;
        }

        IsDraging = true;
        _target.GetComponent<Collider>().enabled = true;
        TargetSlicer.Instance.IsIgnoreSlice = true;
    }

    public override void OnDrag(PointerEventData eventData) {
        if (SlicedTarget.Targets.Count > 0 || IsMoving) {
            return;
        }

        _transform.position = _camera.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, _targetScreenPos.z));
    }

    public override void OnEndDrag(PointerEventData eventData) {
        if (SlicedTarget.Targets.Count > 0 || IsMoving) {
            return;
        }

        IsDraging = false;
        _target.GetComponent<Collider>().enabled = false;
        TargetSlicer.Instance.IsIgnoreSlice = false;

        if (_isMoveToTarget && SlicedTarget.Targets.Count < 1 && IsMoving == false) {
            AudioManager.Instance.PlaySound(ChineseRestaurantResPath.SFX_FOOD_DRAG);
            _transform.DOMove(_endPos, 0.5f).OnComplete(OnFinished);
            IsMoving = true;
        } else {
            _transform.DOMove(_originPosition, 0.5f);
        }
    }

    public override void OnPointerClick(PointerEventData eventData) {
        if (IsDraging == false && SlicedTarget.Targets.Count < 1 && IsMoving == false) {
            IsMoving = true;
            AudioManager.Instance.PlaySound(ChineseRestaurantResPath.SFX_FOOD_DRAG);
            _transform.DOMove(_endPos, 0.5f).OnComplete(OnFinished);
        }
    }

    void OnFinished() {
        IsMoving = false;
        TargetSlicer.Instance.IsIgnoreSlice = false;

        var shakeFX = GetComponent<ShakeFX>();

        if (shakeFX) {
            Destroy(shakeFX);
        }

        // 启用刀光效果
        TargetSlicer.Instance.EnableTrail(true);

        if (_transform.name.StartsWith("Parent_Shiitake")) {
            while (_transform.childCount > 0) {
                var child = _transform.GetChild(0);
                child.SetParent(_transform.parent);
                SlicedTarget.AttachTo(child.gameObject);
                child.GetComponent<Rigidbody>().isKinematic = false;
                child.GetComponent<Collider>().isTrigger = false;
            }

            Destroy(_transform.gameObject);
        } else if (_transform.name.Equals("GreenVegetable Root")) {
            Destroy(_rigidbody);
            Destroy(this);
            TargetSlicer.Instance.IsIgnoreSlice = true;

            _transform.DOMove(new Vector3(1.377f, 1.462f, 1.64f), 0.5f);
            _transform.DOLocalRotate(Vector3.up * -90f, 0.5f).OnComplete(() => _transform.gameObject.AddComponent<GreenVegetableController>());

            var rbs = _transform.GetComponentsInChildren<Rigidbody>();

            foreach (var rb in rbs) {
                if (!rb.transform.Equals(_transform)) {
                    SlicedTarget.AttachTo(rb.gameObject);
                    rb.GetComponent<Collider>().isTrigger = false;
                }
            }
        } else {
            _rigidbody.isKinematic = false;
            _collider.isTrigger = false;
            SlicedTarget.AttachTo(gameObject);

            var fishController = _transform.GetComponentInChildren<FishController>();

            if (fishController) {
                fishController.enabled = true;
                TargetSlicer.Instance.IsIgnoreSlice = true;
            }

            Destroy(this);
        }

        TargetSlicer.Instance.enabled = true;
    }
}

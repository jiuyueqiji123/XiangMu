﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashFX : MonoBehaviour {
    public enum SplashType {
        Oil,
        Water,
    }

    public SplashType type;

    public static bool ShowSplash {
        get; set;
    }

    Collider _collider;

    void Start() {
        _collider = GetComponent<Collider>();
    }

    void OnTriggerEnter(Collider collider) {
        if (!ShowSplash) {
            return;
        }

        switch (type) {
            case SplashType.Oil:
                var oil = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.VFX_SPLASH_OIL, null);
                oil.position = collider.transform.position;
                break;
            case SplashType.Water:
                var water = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.VFX_SPLASH_WATER, null);
                water.position = collider.transform.position;
                break;
        }

        ShowSplash = false;
        _collider.enabled = false;
    }
}

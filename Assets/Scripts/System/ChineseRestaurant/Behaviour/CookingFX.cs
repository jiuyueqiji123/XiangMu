﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookingFX : MonoBehaviour {
    Renderer _renderer;

	void Start() {
        _renderer = GetComponent<Renderer>();
        ChineseRestaurantManager.Instance.onClickSwitch += Cooking;

        if (SwitchController.IsOn) {
            Cooking(true);
        }
	}
	
	void OnDestroy() {
        ChineseRestaurantManager.Instance.onClickSwitch -= Cooking;
	}

    void Cooking(bool onOff) {
        if (onOff) {
            InvokeRepeating("Blending", 2f, 2f);
        } else {
            CancelInvoke();
        }
    }

    void Blending() {
        if (_renderer.material.HasProperty("_BlendValue")) {
            var bv = _renderer.material.GetFloat("_BlendValue");

            if (bv < 1f) {
                var value = bv + 0.1f;
                _renderer.material.SetFloat("_BlendValue", bv + 0.1f);

                if (value >= 1f) {
                    LevelSecond.Instance.ShowButtonVFX();
                }
            }
        }
    }
}

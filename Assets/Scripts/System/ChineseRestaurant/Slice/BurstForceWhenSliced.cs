﻿using UnityEngine;

public class BurstForceWhenSliced : AbstractSliceHandler {
    public float burstForce = 0.5f;

	public override void handleSlice(GameObject[] results) {
		Vector3[] centers = new Vector3[results.Length];

        for (int i = 0; i < results.Length; i++) {
            centers[i] = results[i].GetComponent<Collider>().bounds.center;
        }
		
		Vector3 center = Vector3.zero;

        for (int i = 0; i < centers.Length; i++) {
            center += centers[i];
        }

		center /= centers.Length;

        for (int i = 0; i < results.Length; i++) {
            var rb = results[i].GetComponent<Rigidbody>();

            if (rb != null) {
                Vector3 v = centers[i] - center;
                v.Normalize();
                v *= burstForce;
                rb.AddForce(v, ForceMode.Impulse);
            }
        }
    }
}

﻿using UnityEngine;

public class PlaySoundWhenSliced : AbstractSliceHandler {

	public override void handleSlice(GameObject[] results) {
        AudioManager.Instance.StopAllSound();
        AudioManager.Instance.PlaySound(ChineseRestaurantResPath.SFX_FOOD_SLICE);
    }
}

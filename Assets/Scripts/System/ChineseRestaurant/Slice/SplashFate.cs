﻿using UnityEngine;
using DG.Tweening;

public class SplashFate : MonoBehaviour {
    public float lifetime = 3f;

    Material _mat;

	void Start() {
        _mat = GetComponent<Renderer>().material;
        _mat.mainTexture = CommonUtility.LoadTexture(ChineseRestaurantResPath.TEXTURE_DIR + "Splash_" + Random.Range(0, 5));
        Destroy(gameObject, lifetime);
        Invoke("Fadeout", lifetime * 0.5f);
	}

    void Fadeout() {
        GetComponent<Renderer>().material.DOFade(0f, lifetime * 0.5f);
    }

    public void SetColor(Color color) {
        if (_mat) {
            _mat.color = color;
        } else {
            GetComponent<Renderer>().material.color = color;
        }
    }
}

﻿using UnityEngine;

public class SplashWhenSliced : AbstractSliceHandler {
    static Transform splashRoot;

    public Color splashColor = Color.white;

    void Start() {
        if (splashRoot == null) {
            splashRoot = transform.root.Find("Splash Root");
        }
    }

	public override void handleSlice(GameObject[] results) {
		Vector3[] centers = new Vector3[results.Length];

        for (int i = 0; i < results.Length; i++) {
            centers[i] = results[i].GetComponent<Collider>().bounds.center;
        }
		
		Vector3 center = Vector3.zero;

        for (int i = 0; i < centers.Length; i++) {
            center += centers[i];
        }

		center /= centers.Length;
        var t = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Splash", splashRoot);
        t.position = center;
        var localPos = t.localPosition;
        t.localPosition = new Vector3(localPos.x, 0f, localPos.z);
        t.GetComponent<SplashFate>().SetColor(splashColor);
    }
}

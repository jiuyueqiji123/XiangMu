﻿using UnityEngine;
using UnityEngine.EventSystems;

public class EventBehaviour : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IPointerEnterHandler, IShowHide {
    public virtual void OnBeginDrag(PointerEventData eventData) {

    }

    public virtual void OnDrag(PointerEventData eventData) {

    }

    public virtual void OnEndDrag(PointerEventData eventData) {

    }

    public virtual void OnPointerClick(PointerEventData eventData) {

    }

    public virtual void OnPointerEnter(PointerEventData eventData) {

    }

    public virtual void Show() {

    }

    public virtual void Hide() {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ChineseRestaurantResPath {
    // Dir
    public const string AUDIO_DIR    = "game_restaurant_zh/game_restaurant_zh_audio/";
    public const string PREFAB_DIR   = "game_restaurant_zh/game_restaurant_zh_prefab/";
    public const string TEXTURE_DIR  = "game_restaurant_zh/game_restaurant_zh_texture/";
    public const string VIEW_DIR     = "game_restaurant_zh/game_restaurant_zh_view/";
    public const string TIMELINE_DIR = "game_restaurant_zh/game_restaurant_zh_timeline/";
    public const string MATERIAL_DIR = "game_restaurant_zh/game_restaurant_zh_material/";
    public const string EFFECT_DIR   = "Effect/";
    public const string SOUND_BG = "sound_bg/";

    // Path
    public const string TRAIL = PREFAB_DIR + "Trail";
    public const string PAN = PREFAB_DIR + "Pan";
    public const string STOCKPOT = PREFAB_DIR + "Stockpot";

    public const string VFX_SUCCESS = EFFECT_DIR + "502004090";
    public const string VFX_SELECT_CORRECT = PREFAB_DIR + "VFX_SelectCorrect";
    public const string VFX_SWITCH_TIPS = PREFAB_DIR + "VFX_SwitchTips";
    public const string VFX_SWITCH_SHINE = PREFAB_DIR + "VFX_SwitchShine";
    public const string VFX_SPLASH_OIL = PREFAB_DIR + "VFX_OilSplash";
    public const string VFX_SPLASH_WATER = PREFAB_DIR + "VFX_WaterSplash";
    public const string VFX_INDUCTION_COOKING_HEAT = EFFECT_DIR + "502004091";

    public const string SFX_BGM = SOUND_BG + "604010101";
    public const string SFX_DOOR_WOOD = AUDIO_DIR + "603020401";
    public const string SFX_DOOR_ICEBOX = AUDIO_DIR + "604020201";
    public const string SFX_MENU_CLOSE = AUDIO_DIR + "604040103";
    public const string SFX_MENU_PAGE_CLICK = AUDIO_DIR + "604020202";
    public const string SFX_MENU_FLIP = AUDIO_DIR + "604040102";
    public const string SFX_MENU_SHAKE = AUDIO_DIR + "604040101";
    public const string SFX_SHAKE_ICEBOX = AUDIO_DIR + "602010203";
    public const string SFX_SELECT_CORRECT = AUDIO_DIR + "601010110";
    public const string SFX_SELECT_ERROR = AUDIO_DIR + "604040104";
    public const string SFX_FOOD_SLICE = AUDIO_DIR + "604020501";
    public const string SFX_FOOD_DRAG = AUDIO_DIR + "602010405";
    public const string SFX_SWITCH = AUDIO_DIR + "604040106";
    public const string SFX_FOOD_FALLING_WATER = AUDIO_DIR + "603010802";
    public const string SFX_CHICKEN_SALT = AUDIO_DIR + "604010204";
    public const string SFX_CONDIMENT_FALLING_WATER = AUDIO_DIR + "603010203";
    public const string SFX_BUTTON_CONFIRM = AUDIO_DIR + "603010903";
    public const string SFX_PAY = AUDIO_DIR + "601010109";
    public const string SFX_SERVING_DISHES = AUDIO_DIR +  "604010501";
    public const string SFX_SUCCESS = AUDIO_DIR + "604040114";
    public const string SFX_GAME_DONE = AUDIO_DIR + "603010706";
    public const string SFX_OIL_HEAT = AUDIO_DIR + "604040108";
    public const string SFX_FOOD_FALLING_OIL = AUDIO_DIR + "604040109";
    public const string SFX_PAN_SHAKE = AUDIO_DIR + "604040111";
    public const string SFX_WATER_HEAT = AUDIO_DIR + "604040113";
    public const string SFX_EAT = AUDIO_DIR + "604010502";
    public const string SFX_PUSH_CONDIMENT = AUDIO_DIR + "604040115";

    public const string LEDI_404040101 = AUDIO_DIR + "404040101";
    public const string LEDI_404040102 = AUDIO_DIR + "404040102";
    public const string LEDI_404040103 = AUDIO_DIR + "404040103";
    public const string LEDI_404040104 = AUDIO_DIR + "404040104";
    public const string LEDI_404040105 = AUDIO_DIR + "404040105";
    public const string LEDI_404040106 = AUDIO_DIR + "404040106";
    public const string LEDI_404040107 = AUDIO_DIR + "404040107";
    public const string LEDI_404040108 = AUDIO_DIR + "404040108";
    public const string LEDI_404040205 = AUDIO_DIR + "404040205";
    public const string LEDI_404040206 = AUDIO_DIR + "404040206";
    public const string LEDI_404040426 = AUDIO_DIR + "404040426";
    public const string LEDI_404040427 = AUDIO_DIR + "404040427";
    public const string LEDI_404040428 = AUDIO_DIR + "404040428";
    public const string LEDI_404040301 = AUDIO_DIR + "404040301";

    public const string AISHA_404040404 = AUDIO_DIR + "404040404";
    public const string AISHA_404040405 = AUDIO_DIR + "404040405";
    public const string AISHA_404040406 = AUDIO_DIR + "404040406";
    public const string AISHA_404040407 = AUDIO_DIR + "404040407";
    public const string AISHA_404040418 = AUDIO_DIR + "404040418";
    public const string AISHA_404040419 = AUDIO_DIR + "404040419";
    public const string AISHA_404040421 = AUDIO_DIR + "404040421";

    public const string NUOWA_404040408 = AUDIO_DIR + "404040408";
    public const string NUOWA_404040409 = AUDIO_DIR + "404040409";
    public const string NUOWA_404040410 = AUDIO_DIR + "404040410";
    public const string NUOWA_404040411 = AUDIO_DIR + "404040411";
    public const string NUOWA_404040413 = AUDIO_DIR + "404040413";
    public const string NUOWA_404040414 = AUDIO_DIR + "404040414";
    public const string NUOWA_404040417 = AUDIO_DIR + "404040417";

    public const string AJIASI_404040401 = AUDIO_DIR + "404040401";
    public const string AJIASI_404040402 = AUDIO_DIR + "404040402";
    public const string AJIASI_404040403 = AUDIO_DIR + "404040403";
    public const string AJIASI_404040412 = AUDIO_DIR + "404040412";
    public const string AJIASI_404040422 = AUDIO_DIR + "404040422";
    public const string AJIASI_404040423 = AUDIO_DIR + "404040423";
    public const string AJIASI_404040425 = AUDIO_DIR + "404040425";

    public const string VOICE_SELECT_STATE_ENTER = AUDIO_DIR + "404040201";
    public const string VOICE_SELECT_STATE_EXIT = AUDIO_DIR + "404040202";
    public const string VOICE_CUT_STATE_EXIT_COOKING = AUDIO_DIR + "404040203";
    public const string VOICE_CUT_STATE_EXIT_SOUP = AUDIO_DIR + "404040204";

    public const string VOICE_FOOD_NAME_PORK = AUDIO_DIR + "404040309";
    public const string VOICE_FOOD_NAME_SHRIMP = AUDIO_DIR + "404040310";
    public const string VOICE_FOOD_NAME_FISH = AUDIO_DIR + "404040311";
    public const string VOICE_FOOD_NAME_HAM = AUDIO_DIR + "404040312";
    public const string VOICE_FOOD_NAME_BACON = AUDIO_DIR + "404040313";
    public const string VOICE_FOOD_NAME_CARROT = AUDIO_DIR + "404040314";
    public const string VOICE_FOOD_NAME_CUCUMBER = AUDIO_DIR + "404040315";
    public const string VOICE_FOOD_NAME_GREEN_PEPPER = AUDIO_DIR + "404040316";
    public const string VOICE_FOOD_NAME_SHIITAKE = AUDIO_DIR + "404040317";
    public const string VOICE_FOOD_NAME_GREEN_VEGETABLE = AUDIO_DIR + "404040318";
    public const string VOICE_FOOD_NAME_TOFU = AUDIO_DIR + "404040319";

    public const string VOICE_CONDIMENT_SALT = AUDIO_DIR + "404040320";
    public const string VOICE_CONDIMENT_SAUCE = AUDIO_DIR + "404040321";
    public const string VOICE_CONDIMENT_CHICKEN = AUDIO_DIR + "404040322";
    public const string VOICE_CONDIMENT_SHALLOT = AUDIO_DIR + "404040323";
    public const string VOICE_CONDIMENT_GARLIC = AUDIO_DIR + "404040324";
    public const string VOICE_CONDIMENT_PEPPER = AUDIO_DIR + "404040325";

    public const string DISH_NAME_HGHT_FINISH = AUDIO_DIR + "404040307";
    public const string DISH_NAME_QJPG_FINISH = AUDIO_DIR + "404040304";
    public const string DISH_NAME_DPR_FINISH = AUDIO_DIR + "404040303";
    public const string DISH_NAME_HSY_FINISH = AUDIO_DIR + "404040306";
    public const string DISH_NAME_QCXR_FINISH = AUDIO_DIR + "404040305";
    public const string DISH_NAME_QCDFT_FINISH = AUDIO_DIR + "404040308";
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeDiAnimation : AnimationBehaviour {

	public AnimationBehaviour Idle() {
        return SetTrigger("ToIdle");
    }

    public void Walk() {
        SetTrigger("ToWalk");
    }

    public void Carry() {
        SetTrigger("ToCarry");
    }

    public void Greet(string voicePath, Action callback = null) {
        PlaySequence("BeginGreet", voicePath, callback);
    }

    public void Talk(string voicePath, Action callback = null) {
        PlaySequence("BeginTalk", voicePath, callback);
    }
}

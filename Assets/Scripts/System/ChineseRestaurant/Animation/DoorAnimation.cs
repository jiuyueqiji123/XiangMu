﻿public class DoorAnimation : AnimationBehaviour {

    public AnimationBehaviour Open(string sfxPath = null) {
        return Play("Open", sfxPath);
    }

    public AnimationBehaviour Close(string sfxPath = null) {
        return Play("Close", sfxPath);
    }

    public AnimationBehaviour Shake(string sfxPath) {
        return Play("Shake", sfxPath);
    }

    public AnimationBehaviour Idle() {
        return Play("Idle");
    }
}

/**
The MIT License (MIT)

Copyright (c) 2014 Paul Hayes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 **/

using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace AnimatorStateMachineUtil {
    [RequireComponent(typeof(Animator))]
    public class AnimatorStateMachineUtil : MonoBehaviour {
        public bool autoUpdate;

        public Animator Animator {
            get {
                return _animator;
            }
        }

        protected Animator _animator;
        protected Lookup<int, Action> _stateHashToUpdateMethod;
        protected Lookup<int, Action> _stateHashToEnterMethod;
        protected Lookup<int, Action> _stateHashToExitMethod;
        protected Dictionary<int, string> _hashToAnimString;
        protected int[] _lastStateLayers;

        void Awake() {
            _animator = GetComponent<Animator>();
            _lastStateLayers = new int[_animator.layerCount];

            DiscoverStateMethods();
        }

        void Update() {
            if (autoUpdate) {
                StateMachineUpdate();
            }
        }

        void OnValidate() {
            DiscoverStateMethods();
        }

        public void StateMachineUpdate() {
            for (int layer = 0; layer < _lastStateLayers.Length; layer++) {
                int _lastState = _lastStateLayers[layer];
                int stateId = _animator.GetCurrentAnimatorStateInfo(layer).fullPathHash;

                if (_lastState != stateId) {
                    if (_stateHashToExitMethod.Contains(_lastState)) {
                        foreach (Action action in _stateHashToExitMethod[_lastState]) {
                            action.Invoke();
                        }
                    }

                    if (_stateHashToEnterMethod.Contains(stateId)) {
                        foreach (Action action in _stateHashToEnterMethod[stateId]) {
                            action.Invoke();
                        }
                    }
                }

                if (_stateHashToUpdateMethod.Contains(stateId)) {
                    foreach (Action action in _stateHashToUpdateMethod[stateId]) {
                        action.Invoke();
                    }
                }

                _lastStateLayers[layer] = stateId;
            }
        }

        void DiscoverStateMethods() {
            _hashToAnimString = new Dictionary<int, string>();
            var components = gameObject.GetComponents<MonoBehaviour>();

            var enterStateMethods = new List<StateMethod>();
            var updateStateMethods = new List<StateMethod>();
            var exitStateMethods = new List<StateMethod>();

            foreach (var component in components) {
                if (component == null) {
                    continue;
                }

                var type = component.GetType();
                var methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.InvokeMethod);

                foreach (var method in methods) {
                    object[] attributes;

                    attributes = method.GetCustomAttributes(typeof(StateUpdateMethod), true);
                    foreach (StateUpdateMethod attribute in attributes) {
                        var parameters = method.GetParameters();

                        if (parameters.Length == 0) {
                            updateStateMethods.Add(CreateStateMethod(attribute.state, method, component));
                        }
                    }

                    attributes = method.GetCustomAttributes(typeof(StateEnterMethod), true);
                    foreach (StateEnterMethod attribute in attributes) {
                        var parameters = method.GetParameters();

                        if (parameters.Length == 0) {
                            enterStateMethods.Add(CreateStateMethod(attribute.state, method, component));
                        }
                    }

                    attributes = method.GetCustomAttributes(typeof(StateExitMethod), true);
                    foreach (StateExitMethod attribute in attributes) {
                        var parameters = method.GetParameters();

                        if (parameters.Length == 0) {
                            exitStateMethods.Add(CreateStateMethod(attribute.state, method, component));
                        }
                    }
                }
            }

            _stateHashToUpdateMethod = (Lookup<int, Action>)updateStateMethods.ToLookup(p => p.stateHash, p => p.method);
            _stateHashToEnterMethod = (Lookup<int, Action>)enterStateMethods.ToLookup(p => p.stateHash, p => p.method);
            _stateHashToExitMethod = (Lookup<int, Action>)exitStateMethods.ToLookup(p => p.stateHash, p => p.method);
        }

        StateMethod CreateStateMethod(string state, MethodInfo method, MonoBehaviour component) {
            int stateHash = Animator.StringToHash(state);
            _hashToAnimString[stateHash] = state;
            var stateMethod = new StateMethod();
            stateMethod.stateHash = stateHash;
            stateMethod.method = () => {
                method.Invoke(component, null);
            };
            return stateMethod;
        }
    }


    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class StateUpdateMethod : Attribute {
        public string state;

        public StateUpdateMethod(string state) {
            this.state = state;
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class StateEnterMethod : Attribute {
        public string state;

        public StateEnterMethod(string state) {
            this.state = state;
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class StateExitMethod : Attribute {
        public string state;

        public StateExitMethod(string state) {
            this.state = state;
        }
    }

    public class StateMethod {
        public int stateHash;
        public Action method;
    }
}


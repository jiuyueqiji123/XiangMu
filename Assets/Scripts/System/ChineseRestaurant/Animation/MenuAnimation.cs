﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class MenuAnimation : AnimationBehaviour {
    enum FlipState {
        Normal,
        First,
        Second,
    }

    FlipState _flipState = FlipState.Normal;

    public AnimationBehaviour Open() {
        return Play("Open");
    }

    public AnimationBehaviour Close(string sfxPath) {
        return Play("Close", sfxPath);
    }

    public AnimationBehaviour Shake() {
        return Play("Shake");
    }

    public AnimationBehaviour Idle() {
        return Play("Idle");
    }

    public void ButtonShowHide(Transform left, Transform right) {
        switch (_flipState) {
            case FlipState.Normal:
                left.SetActive(false);
                right.SetActive(true);
                break;
            case FlipState.First:
                left.SetActive(true);
                right.SetActive(true);
                break;
            case FlipState.Second:
                left.SetActive(true);
                right.SetActive(false);
                break;
        }
    }

    public void ClickButtonRight(Transform left, Transform right) {
        switch (_flipState) {
            case FlipState.Normal:
                Play("Forward_1");
                _flipState = FlipState.First;
                break;
            case FlipState.First:
                Play("Forward_2");
                _flipState = FlipState.Second;
                break;
        }

        ButtonShowHide(left, right);
    }

    public void ClickButtonLeft(Transform left, Transform right) {
        switch (_flipState) {
            case FlipState.First:
                Play("Reverse_1");
                _flipState = FlipState.Normal;
                break;
            case FlipState.Second:
                Play("Reverse_2");
                _flipState = FlipState.First;
                break;
        }

        ButtonShowHide(left, right);
    }

    public DishName? ClickPageLeft(MeshRenderer render) {
        render.enabled = true;

        switch (_flipState) {
            case FlipState.Normal:
                return ChineseRestaurantManager.Instance.currentDishName = DishName.HuangGuaHuoTui;
            case FlipState.First:
                return ChineseRestaurantManager.Instance.currentDishName = DishName.DongPoRou;
            case FlipState.Second:
                return ChineseRestaurantManager.Instance.currentDishName = DishName.HongShaoYu;
            default:
                return null;
        }
    }

    public DishName? ClickPageRight(MeshRenderer render) {
        render.enabled = true;

        switch (_flipState) {
            case FlipState.Normal:
                return ChineseRestaurantManager.Instance.currentDishName = DishName.QingChaoXiaRen;
            case FlipState.First:
                return ChineseRestaurantManager.Instance.currentDishName = DishName.QingJiaoPeiGen;
            case FlipState.Second:
                return ChineseRestaurantManager.Instance.currentDishName = DishName.QingCaiDouFuTang;
            default:
                return null;
        }
    }
}

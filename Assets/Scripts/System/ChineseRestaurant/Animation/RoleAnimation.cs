﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class RoleAnimation : AnimationBehaviour {

    public AnimationBehaviour Idle() {
        return Play("Idle");
    }

    public void Walk() {
        SetTrigger("ToWalk");
    }

    public AnimationBehaviour Eat() {
        return SetTrigger("ToEat");
    }

    public AnimationBehaviour Sit() {
        return Play("Sit");
    }

    public AnimationBehaviour SitIdle() {
        return Play("Sit_Idle");
    }

    public AnimationBehaviour SitDown() {
        return SetTrigger("ToSitDown");
    }

    public AnimationBehaviour Nod(string voicePath, bool imediate) {
        AudioManager.Instance.PlaySound(voicePath);

        if (imediate) {
            return Play("Nod");
        } else {
            return SetTrigger("ToNod");
        }
    }

    public void Praise(string voicePath, Action callback = null) {
        PlaySequence("BeginPraise", voicePath, callback);
    }

    public AnimationBehaviour Pay(string sfxPath) {
        return SetTrigger("ToPay", sfxPath);
    }
}

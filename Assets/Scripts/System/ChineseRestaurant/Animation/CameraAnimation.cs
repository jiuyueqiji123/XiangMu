﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : AnimationBehaviour {
    
    public AnimationBehaviour Forward() {
        return Play("Forward");
    }

    public AnimationBehaviour Reverse() {
        return Play("Reverse");
    }
}

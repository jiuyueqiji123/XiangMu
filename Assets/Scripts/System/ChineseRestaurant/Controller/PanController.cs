﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class PanController : EventBehaviour {
    public Transform vfxRoot;
    public ParticleSystem[] vfxs;
    public Renderer oilRenderer;
    public GameObject colliderObj;
    public GameObject tips;
    public GameObject hand;

    Transform _transform;
    PanAnimation _panAnim;
    bool _isDraging;
    bool _isShaking;
    float _height = 5f;

    void Start() {
        _transform = GetComponent<Transform>();
        _panAnim = GetComponentInParent<PanAnimation>();
        ChineseRestaurantManager.Instance.onClickSwitch += EnableVFX;
        InvokeRepeating("ShowHideTips", 1f, 1f);
    }

    void OnDestroy() {
        ChineseRestaurantManager.Instance.onClickSwitch -= EnableVFX;
    }

    void EnableVFX(bool isEnabled) {
        if (isEnabled) {
            vfxRoot.SetActive(false);
            vfxRoot.SetActive(true);
        }

        foreach (var vfx in vfxs) {
            var emit = vfx.emission;
            emit.enabled = isEnabled;
        }
    }

    public override void OnPointerClick(PointerEventData eventData) {
        if (_isDraging) {
            return;
        }

        Shake();
    }

    void Shake() {
        if (_isShaking) {
            return;
        }

        ShowHideTips();
        colliderObj.SetActive(true);
        _isShaking = true;
        _panAnim.Shake().OnFinished(() => {
            _isShaking = false;
            colliderObj.SetActive(false);
        });

        for (var i = 0; i < _transform.childCount; i++) {
            var child = _transform.GetChild(i);

            if (child.name != "Collider") {
                var rb = child.GetComponent<Rigidbody>();
                var force = Random.Range(2.5f, 3.5f);
                rb.AddForce(Vector3.up * force, ForceMode.Impulse);
            }
        }

        AudioManager.Instance.PlaySound(ChineseRestaurantResPath.SFX_PAN_SHAKE);
    }

    public override void OnBeginDrag(PointerEventData eventData) {
        _isDraging = true;
        Shake();
    }

    public override void OnEndDrag(PointerEventData eventData) {
        _isDraging = false;
    }

    public void SetOilColor(bool isBlending) {
        if (isBlending) {
            oilRenderer.material.DOColor(new Color32(43, 5, 1, 255), 3f);
        } else {
            oilRenderer.material.DOPause();
        }
    }

    bool _hasShownTips;
    bool _hasShownHand;

    public void ShowHideTips() {
        if (_hasShownHand || _hasShownTips) {           
            hand.SetActive(false);
            tips.SetActive(false);
            return;
        }

        var val = Random.Range(0, 2);

        if (val == 0) {
            ShowHideUI();
        } else {
            ShowHideHand();
        }
    }

    void ShowHideUI() {
        if (_hasShownTips || DishController.IsDraging) {
            StopAllCoroutines();
            tips.SetActive(false);
            return;
        }

        var dp = FindObjectOfType<DragPlate>();

        if (SwitchController.IsOn == false || dp != null) {
            return;
        }

        StartCoroutine(TimeUtility.DelayInvoke(5f, () => {
            tips.SetActive(true);
            _hasShownTips = true;
            CancelInvoke();
            StopAllCoroutines();
        }));
    }

    void ShowHideHand() {
        if (_hasShownHand || _isShaking) {
            StopAllCoroutines();
            hand.SetActive(false);
            return;
        }

        var dp = FindObjectOfType<DragPlate>();

        if (SwitchController.IsOn == false || dp != null) {
            return;
        }

        StartCoroutine(TimeUtility.DelayInvoke(5f, () => {
            hand.SetActive(true);
            _hasShownHand = true;
            CancelInvoke();
            StopAllCoroutines();
        }));
    }
}

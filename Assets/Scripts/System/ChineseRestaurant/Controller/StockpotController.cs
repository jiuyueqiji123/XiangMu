﻿using UnityEngine;
using DG.Tweening;

public class StockpotController : EventBehaviour {
    public Transform vfxBubble;
    public Transform vfxFire;
    public ParticleSystem[] vfxs;
    public Renderer waterRenderer;
    public GameObject tips;

    bool _hasShownTips;

    void Start() {
        ChineseRestaurantManager.Instance.onClickSwitch += EnableVFX;
        InvokeRepeating("ShowHideTips", 1f, 1f);
    }

    void OnDestroy() {
        ChineseRestaurantManager.Instance.onClickSwitch -= EnableVFX;
    }

    public void ShowHideTips() {
        if (_hasShownTips || DishController.IsDraging) {
            StopAllCoroutines();
            tips.SetActive(false);
            return;
        }

        var dp = FindObjectOfType<DragPlate>();

        if (SwitchController.IsOn == false || dp != null) {
            return;
        }

        StartCoroutine(TimeUtility.DelayInvoke(5f, () => {
            tips.SetActive(true);
            _hasShownTips = true;
            CancelInvoke();
        }));
    }

    void EnableVFX(bool isEnabled) {
        if (isEnabled) {
            vfxBubble.SetActive(false);
            vfxFire.SetActive(false);
            vfxBubble.SetActive(true);
            vfxFire.SetActive(true);
        }

        foreach (var vfx in vfxs) {
            var emit = vfx.emission;
            emit.enabled = isEnabled;
        }
    }

    public void SetWaterColor(bool isBlending) {
        if (isBlending) {
            waterRenderer.material.DOColor(new Color32(246, 255, 210, 255), 3f);
        } else {
            waterRenderer.material.DOPause();
        }
    }
}

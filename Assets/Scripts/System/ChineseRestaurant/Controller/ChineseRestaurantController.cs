﻿public class ChineseRestaurantController : Singleton<ChineseRestaurantController> {
    const string _VIEW_PATH = ChineseRestaurantResPath.VIEW_DIR + "ReturnForm";
    ReturnView _view;

    public override void Init() {
        base.Init();
        _view = new ReturnView();
    }

    public override void UnInit() {
        base.UnInit();
    }

    public void OpenView() {
        _view.OpenForm(_VIEW_PATH);
        RegisterEvent();
    }

    public void CloseView() {
        _view.CloseForm();
        RemoveEvent();
        ChineseRestaurantManager.DestroyInstance();
    }

    private void RegisterEvent() {
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.CakeShop_Btn_Back, ClickBtnBack);
    }

    private void RemoveEvent() {
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.CakeShop_Btn_Back, ClickBtnBack);
    }

    public void ClickBtnBack(HUIEvent e) {
        ChineseRestaurantManager.Instance.ToLastLevel();
    }
}

﻿using UnityEngine;
using UnityEngine.EventSystems;

public class FishController : UpdateableComponent, IPointerEnterHandler {
    Transform _child;
    Material _childMat;
    int _cutCount = 3;
    bool _canSlice = true;

    public bool IsDone {
        get {
            return _cutCount <= 0;
        }
    }

    protected override void OnInitialize() {
        _child = transform.GetChild(0);
        _child.SetActive(true);
        _childMat = _child.GetComponent<Renderer>().material;       
        TargetSlicer.Instance.onIgnoreSliceCallback += OnSliced;
    }

    protected override void OnRelease() {
        if (TargetSlicer.HasInstance()) {
            TargetSlicer.Instance.onIgnoreSliceCallback -= OnSliced;
        }
    }
  
    public override void OnUpdate(float dt) {
        if (_canSlice == false && Input.GetMouseButtonUp(0)) {
            _canSlice = true;
        }
    }

    bool _isDraging;

    public void OnPointerEnter(PointerEventData eventData) {
        if (_isDraging == false) {
            _isDraging = true;
        }
    }

    void OnSliced() {
        if (_canSlice && _childMat && _isDraging) {
            _isDraging = false;
            AudioManager.Instance.PlaySound(ChineseRestaurantResPath.SFX_FOOD_SLICE);
            TargetSlicer.Instance.HideHandTips();

            switch (_cutCount) {
                case 3:
                    _childMat.SetFloat("_Cutoff", 0.5f);
                    break;
                case 2:
                    _childMat.SetFloat("_Cutoff", 0.4f);
                    break;
                case 1:
                    Destroy(_child.gameObject);
                    StartCoroutine(TimeUtility.DelayInvoke(1f, () => {
                        TargetSlicer.Instance.sampleCount = 1;
                    }));
                    break;
            }

            _cutCount -= 1;
            _canSlice = false;
        }
    }
}

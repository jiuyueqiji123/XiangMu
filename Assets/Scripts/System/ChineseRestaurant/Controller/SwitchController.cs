﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class SwitchController : EventBehaviour {
    Transform _transform;
    Vector3 _startRot;
    Vector3 _endRot;
    float _angle = -90f;
    
    public static bool IsOn {
        get; private set;
    }

	void Start() {
        _transform = GetComponent<Transform>();
        _startRot = _transform.localEulerAngles;

        _transform.Rotate(Vector3.up * _angle, Space.Self);
        _endRot = _transform.localEulerAngles;
        _transform.localEulerAngles = _startRot;

        IsOn = false;
        SetSwitchState(IsOn);
    }

    void OnDestroy() {
        if (_heatSFX != null) {
            _heatSFX.Stop();
            _heatSFX = null;
        }

        if (_inductionCookingHeat) {
            Destroy(_inductionCookingHeat.gameObject);
        }
    }

    ForceController _forceController;
    float _powerLimit = 10f;
    Transform _inductionCookingHeat;
    GameAudioSource _heatSFX;

    public override void OnPointerClick(PointerEventData eventData) {
        IsOn = !IsOn;
        SetSwitchState(IsOn);
        AudioManager.Instance.PlaySound(ChineseRestaurantResPath.SFX_SWITCH);

        if (_forceController == null) {
            _forceController = FindObjectOfType<ForceController>();

            if (_forceController == null) {
                return;
            }
        }

        if (_transform.childCount > 0) {
            for (var i = _transform.childCount - 1; i >= 0; i--) {
                Destroy(_transform.GetChild(i).gameObject);
            }
        }

        if (IsOn) {
            var vfx = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.VFX_SWITCH_SHINE, _transform);
            var sfxPath = "";

            if (ChineseRestaurantManager.Instance.currentDishName == DishName.QingCaiDouFuTang) {
                _forceController.upwardsModifier = 1f;
                _forceController.randomOnCenter = true;
                ChineseRestaurantManager.Instance.EnableFloater(false);
                var ps = vfx.GetComponentInChildren<ParticleSystem>();
                var main = ps.main;
                main.startSize = 0.25f;
                var psr = vfx.GetComponentInChildren<ParticleSystemRenderer>();
                psr.renderMode = ParticleSystemRenderMode.Billboard;

                sfxPath = ChineseRestaurantResPath.SFX_WATER_HEAT;
            } else {
                _forceController.randomOnCenter = false;
                vfx.localPosition += Vector3.up * -0.02f;

                if (_inductionCookingHeat == null) {
                    _inductionCookingHeat = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.VFX_INDUCTION_COOKING_HEAT, null);
                    _inductionCookingHeat.position = new Vector3(-0.6621439f, 1.02f, 1.508208f);
                }

                sfxPath = ChineseRestaurantResPath.SFX_OIL_HEAT;
            }

            InvokeRepeating("IncreasePower", 1f, 2f);

            if (_heatSFX == null) {
                _heatSFX = AudioManager.Instance.PlaySound(sfxPath, true, true);

                if (sfxPath == ChineseRestaurantResPath.SFX_OIL_HEAT) {
                    _heatSFX.Volume = 0.5f;
                }
            }
        } else {
            if (ChineseRestaurantManager.Instance.currentDishName == DishName.QingCaiDouFuTang) {
                ChineseRestaurantManager.Instance.EnableFloater(true);
            } else {
                if (_inductionCookingHeat) {
                    Destroy(_inductionCookingHeat.gameObject);
                }
            }

            CancelInvoke();
            _forceController.power = 0f;

            if (_heatSFX != null) {
                _heatSFX.Volume = 1f;
                _heatSFX.Stop();
                _heatSFX = null;
            }
        }

        if (ChineseRestaurantManager.Instance.onClickSwitch != null) {
            ChineseRestaurantManager.Instance.onClickSwitch(IsOn);
        }
    }

    void IncreasePower() {
        if (_forceController.power < _powerLimit && !ForceController.isDraging) {
            _forceController.power += 1f;
        }
    }

    void SetSwitchState(bool isOn) {
        var rot = isOn ? _endRot : _startRot;
        _transform.DORotate(rot, 0.3f);
    }
}

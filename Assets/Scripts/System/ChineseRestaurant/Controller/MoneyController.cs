﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoneyController : EventBehaviour {
    Transform _transform;
    Vector3 _originPos;

	void Start() {
        _transform = GetComponent<Transform>();
        _originPos = _transform.position;
	}

    public override void OnPointerClick(PointerEventData eventData) {
        Hide();
    }

    public override void Hide() {
        _transform.DOScale(0f, 0.3f).OnComplete(() => _transform.position = _originPos);
    }

    public override void Show() {
        var delay = (ChineseRestaurantManager.Instance.currentRoleName == RoleName.NuoWa) ? 1f : 0.6f;
        StartCoroutine(TimeUtility.DelayInvoke(delay, () => {
            _transform.DOScale(2f, 0.5f);
            _transform.DOLocalMove(new Vector3(-0.4f, 0f, -0.3f), 0.5f);
        }));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateController : MonoBehaviour {

    Transform _transform;

	void Start() {
        _transform = GetComponent<Transform>();
    }

    public void SpawnPlateAndFood() {
        var foodNames = ChineseRestaurantManager.Instance.GetFoodMaterialFromCurrentDishName();

        switch (foodNames.Length) {
            case 1:
                var p = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Plate_Center", _transform);
                _transform.localPosition = Vector3.right * 0.1f;
                var t = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Parent_" + foodNames[0].ToString(), p);
                ResetChild(t, p);
                break;
            case 2:
                var p1 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Plate_Up", _transform);
                var p2 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Plate_Down", _transform);
                _transform.localPosition = Vector3.right * -0.2f;
                var t1 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Parent_" + foodNames[0].ToString(), p1);
                var t2 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Parent_" + foodNames[1].ToString(), p2);
                ResetChild(t1, p1);
                ResetChild(t2, p2);
                break;
            case 3:
                var p3 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Plate_Up", _transform);
                var p4 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Plate_Center", _transform);
                var p5 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Plate_Down", _transform);
                var t3 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Parent_" + foodNames[0].ToString(), p3);
                var t4 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Parent_" + foodNames[1].ToString(), p4);
                var t5 = CommonUtility.InstantiateFrom(ChineseRestaurantResPath.PREFAB_DIR + "Parent_" + foodNames[2].ToString(), p5);
                ResetChild(t3, p3);
                ResetChild(t4, p4);
                ResetChild(t5, p5);
                break;
        }
    }

    void ResetChild(Transform t, Transform parent) {
        if (t.name.StartsWith("Parent_Shiitake")) {
            Destroy(t.GetComponent<DragFood>());
            t.gameObject.AddComponent<Rigidbody>().isKinematic = true;
            t.gameObject.AddComponent<DragAndClickTrigger>();
            return;
        }

        while (t.childCount > 0) {
            var child = t.GetChild(0);
            child.SetParent(parent);

            if (t.name.StartsWith("Parent_Shrimp")) {
                child.GetComponent<Rigidbody>().isKinematic = false;
                child.GetComponent<Collider>().material = ChineseRestaurantManager.Instance.PhysicMat;
                child.gameObject.AddComponent<ShrimpController>();
            } else {
                child.localPosition = Vector3.up * 0.03f;
                child.localEulerAngles = Vector3.zero;
                child.GetComponent<DragAndClickTrigger>().enabled = true;
            }
        }

        Destroy(t.gameObject);
    }

}

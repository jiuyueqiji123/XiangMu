﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShrimpController : EventBehaviour {
    static List<ShrimpController> _slist = new List<ShrimpController>();

    void Start() {
        _slist.Add(this);
    }

    public static void Destroy() {
        for (var i = _slist.Count - 1; i >= 0; i--) {
            Destroy(_slist[i]);
        }
    }

    bool _isDraging;
    GameAudioSource _gas;

    public override void OnBeginDrag(PointerEventData eventData) {
        _isDraging = true;

        if (_gas != null) {
            AudioManager.Instance.StopSound(_gas);
        }
       
        _gas = AudioManager.Instance.PlaySound(ChineseRestaurantResPath.LEDI_404040206);
    }

    public override void OnEndDrag(PointerEventData eventData) {
        _isDraging = false;
    }

    public override void OnPointerClick(PointerEventData eventData) {
        if (!_isDraging) {
            if (_gas != null) {
                AudioManager.Instance.StopSound(_gas);
            }

            _gas = AudioManager.Instance.PlaySound(ChineseRestaurantResPath.LEDI_404040206);
        }
    }
}

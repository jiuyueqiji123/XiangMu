﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class EventListen : MonoBehaviour , IInputDownHandler , IInputDragHandler , IInputUpHandler , IInputClickHandler{

    SubtractionModel model;
    SubtractionView View;
    [HideInInspector]
    public bool IsDisPlay = false;
    
    //[HideInInspector]
    private Vector3 StartPos = new Vector3();
    //[HideInInspector]
    private float Start_speed;
    private RotateAndAccelerate RandA;
    [HideInInspector]
    public bool isDrag = false;
    
    public Color color;
    [HideInInspector]
    public bool IsBack = false;
    [HideInInspector]
    public bool IsClick = false;


    private BoxCollider2D Collider;


    void Awake()
    {
        model = SubtractionController.Instance.GetModel();
        View = SubtractionController.Instance.GetView();
        Collider = this.gameObject.GetComponent<BoxCollider2D>();
        RandA = this.gameObject.GetComponent<RotateAndAccelerate>();


    }

    

    void Update()
    {
       
        if (IsBack)
        {
            this.gameObject.transform.localPosition = Vector3.MoveTowards(this.gameObject.transform.localPosition,this.RandA.Animal_Pos, 4000f * Time.deltaTime);
            if (this.gameObject.transform.localPosition == this.RandA.Animal_Pos)
            {
                this.RandA.enabled = true;
                IsBack = false;
                //this.model.IsRotate = true;
            }
        }
        
        if (model.m_IsPufferFish)
        {
            if (IsDisPlay && model.alpha >=1)
            {
                return;
            }
            foreach (Renderer FishRenderer in SubtractionController.Instance.GetMaterial(this.gameObject.transform.GetChild(1).gameObject))
            {
                color = FishRenderer.material.color;
                color.a = model.alpha;
                FishRenderer.material.color = color;
                if (model.alpha >= 1)
                {
                    IsDisPlay = true;
                }
                else
                {
                    IsDisPlay = false;
                }
            }
            foreach (Renderer FishRenderer in SubtractionController.Instance.GetMaterial(this.gameObject.transform.GetChild(2).gameObject))
            {
                color = FishRenderer.material.color;
                color.a = 0;
                FishRenderer.material.color = color;
                if (model.alpha >= 1)
                {
                    IsDisPlay = true;
                }
                else
                {
                    IsDisPlay = false;
                }
            }
        }
        else
        {
            foreach (Renderer FishRenderer in SubtractionController.Instance.GetMaterial(this.gameObject))
            {
                color = FishRenderer.material.color;
                color.a = model.alpha;
                FishRenderer.material.color = color;
                if (model.alpha >= 1)
                {
                    IsDisPlay = true;
                }
                else
                {
                    IsDisPlay = false;
                }
            }
        }
        if (this.gameObject.transform.localScale.x * RandA.speed > 0)
        {
            this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x * -1, this.gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);
            if (model.Isshuimu)
            {
                if (this.gameObject.transform.rotation.eulerAngles.z > 180)
                {
                    this.gameObject.transform.rotation = Quaternion.Euler(0, 180, 20);
                    return;
                }

                if (this.gameObject.transform.rotation.eulerAngles.z < 180)
                {
                    Debug.LogError("这里这里这里！！！");
                    this.gameObject.transform.rotation = Quaternion.Euler(0, 180, -20);
                    return;
                }
            }

        }

    }

    public void Recovery()
    {
        foreach (Renderer FishRenderer in SubtractionController.Instance.GetMaterial(this.gameObject.transform.GetChild(1).gameObject))
        {
            color = FishRenderer.material.color;
            color.a = 0;
            FishRenderer.material.color = color;
        }
        foreach (Renderer FishRenderer in SubtractionController.Instance.GetMaterial(this.gameObject.transform.GetChild(2).gameObject))
        {
            color = FishRenderer.material.color;
            color.a = 0;
            FishRenderer.material.color = color;
        }
        TimerManager.Instance.AddTimer(0.01f,()=>
        {
            foreach (Renderer FishRenderer in SubtractionController.Instance.GetMaterial(this.gameObject.transform.GetChild(1).gameObject))
            {
                color = FishRenderer.material.color;
                color.a = 1;
                FishRenderer.material.color = color;
            }
        });
       // this.gameObject.transform.GetChild(2).GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
       // this.gameObject.transform.GetChild(1).GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);
    }


    public virtual void OnInputClick(InputEventData eventData)
    {
        if (!this.isDrag)
        {
            if (model.m_IsPufferFish)
            {
                foreach (Renderer FishRenderer in SubtractionController.Instance.GetMaterial(this.gameObject.transform.GetChild(1).gameObject))
                {
                    color = FishRenderer.material.color;
                    color.a = 0;
                    FishRenderer.material.color = color;
                }
                foreach (Renderer FishRenderer in SubtractionController.Instance.GetMaterial(this.gameObject.transform.GetChild(2).gameObject))
                {
                    color = FishRenderer.material.color;
                    color.a = 0;
                    FishRenderer.material.color = color;
                }
                TimerManager.Instance.AddTimer(0.01f, () =>
                {
                    foreach (Renderer FishRenderer in SubtractionController.Instance.GetMaterial(this.gameObject.transform.GetChild(2).gameObject))
                    {
                        color = FishRenderer.material.color;
                        color.a = 1;
                        FishRenderer.material.color = color;
                    }
                });
                Invoke("Recovery",4f);
            }
        }
        this.isDrag = false;
    }

    public virtual void OnInputDown(InputEventData eventData)
    {




        //Debug.LogError("1111111111111111111111111");
        // model = new SubtractionModel();
        this.model.IsRotate = false;
        IsClick = true;
        AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/603050103",typeof(AudioClip),enResourceType.Sound,false).m_content as AudioClip);
        this.gameObject.transform.localScale = this.gameObject.transform.localScale * 1.2f;
        this.Collider.size = new Vector2(this.Collider.size.x / 1.6f, this.Collider.size.y / 1.6f);
        StartPos = this.gameObject.transform.localPosition;
        RandA = this.gameObject.GetComponent<RotateAndAccelerate>();
        //if (model.m_IsPufferFish)
        //{
        //    this.gameObject.transform.GetChild(1).SetActive(false);
        //    this.gameObject.transform.GetChild(2).SetActive(true);
        //}
        if (RandA.speed > 0)
        {
            if (model.Isshuimu)
            {
                Start_speed = 50;
            }
            else
            {
                Start_speed = 70;
            }
            
        }
        if (RandA.speed < 0)
        {
            if (model.Isshuimu)
            {
                Start_speed = -50;
            }
            else
            {
                Start_speed = -70;
            }
        }
        RandA.speed = 0f;
        RandA.IsRotate = false;
        RandA.IsOpenEffect = false;
    }

    public virtual void OnInputDrag(InputEventData eventData)
    {
        RandA.IsDrag = true;
        RandA.IsRotate = false;
        RandA.IsOpenEffect = false;
        this.gameObject.transform.position = new Vector3(View.ui_camera.ScreenToWorldPoint(Input.mousePosition).x, View.ui_camera.ScreenToWorldPoint(Input.mousePosition).y, RandA.Animal_Pos.z);
        this.gameObject.transform.localPosition = new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, RandA.Animal_Pos.z);
        //Debug.LogError(this.gameObject.GetComponent<RectTransform>().anchoredPosition);
        isDrag = true;
        
        
    }

    public virtual void OnInputUp(InputEventData eventData)
    {
        //if (RandA.IsContact)
        //{
        
        this.gameObject.transform.localScale = this.gameObject.transform.localScale / 1.2f;
        this.Collider.size = new Vector2(this.Collider.size.x * 1.6f, this.Collider.size.y * 1.6f);
        this.model.IsRotate = true;
        if (model.m_IsPufferFish)
        {
            //this.gameObject.transform.GetChild(2).SetActive(false);
            //this.gameObject.transform.GetChild(1).SetActive(true);
        }


        //this.gameObject.transform.localPosition = Vector3.MoveTowards(this.gameObject.transform.localPosition,new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z),100f*Time.deltaTime);
        if (RandA.IsContact)
        {
            AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/603050104", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip);
            RandA.Animal_Pos = this.gameObject.transform.localPosition;
        }
        else
        {
            AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/603050105", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip);
            this.RandA.enabled = false;
            IsBack = true;
            //if (this.model.FishTankRongQi.Count != 0)
            //{
            //    Debug.LogError("this.model.FishTankRongQi.Remove(this.gameObject);");
            //    Debug.LogError(this.model.FishTankRongQi.Count);
            //    this.model.FishTankRongQi.Remove(this.gameObject);
            //}
        }
        if (!PlayerPrefs.HasKey("SubtractionKey"))
        {
            if (this.model.FishTankRongQi == null)
            {
                return;
            }
            if (this.model.FishTankRongQi.Count == this.model.subtraction)
            {

                this.View.hand_click1.SetActive(true);
                this.View.hand_click1.GetComponent<RectTransform>().DOMove(this.View.CarryOutButton.GetComponent<RectTransform>().position + new Vector3(10, -8, -10), 0.3f);
                SubtractionController.Instance.IsGuiDe = false;
                this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>().enabled = false;
            }
        }
        // Debug.LogError(model.completiontimes);
        if (this.model.completiontimes == 5)
        {
            return;
        }

        RandA.speed = this.Start_speed;

        RandA.IsDrag = false;
        

        //}
    }

    

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubtractionModel {

    public AudioClip StartAudio_1;

    public AudioClip StartAudio_2;

    public AudioClip StartAudio_3;

    public AudioClip ErrorAudio_1;
    public AudioClip ErrorAudio_2;
    public AudioClip CorrectAudio;
    public AudioClip ChaAudio;

    public bool IsRotate = false;

    public bool IsShalite = false;

    public bool IsWugui = false;

    public bool IsPlayAudio = false;

    public bool IsGameEnter = false;

    private int Subtracted;
    private int Subtraction;
    private int CompletionTimes = 0;
    private int Difference;
    private float Rang = 10f;
    private float Random;
    private float Alpha;
    private float Speed = 300f;
    private float ScaleSpeed = 3f;
    private bool IsGoBig = false;
    public bool IsCarryOut_Button = false;

    public bool RandAIsMove = true;


    private bool FadeOut = false;

    private bool FadeIn = false;

    private bool IsPufferFish = false;

    private bool IsAudioPlay = true;

    private bool IsFormula = false;

    private bool IsFormualBack = false;

    private bool IsDown = true;

    private bool XunZhangMove = false;

    private bool IsMoveUp = false;

    private bool LeDiMoveIN = false;

    private bool LeDiMoveOut = false;

    private bool LeDiJZMoveIn = false;

    private bool isshuimu = false;

    private float FormualRotate = 90;

    private AudioClip StatrAudio;

    [HideInInspector]
    public List<Vector3> Start_Pos = new List<Vector3>();

    [HideInInspector]
    public List<GameObject> FishTankRongQi = new List<GameObject>();

    [HideInInspector]
    public List<GameObject> Animals = new List<GameObject>();

    [HideInInspector]
    public List<Texture2D> Fish = new List<Texture2D>();

    [HideInInspector]
    public List<GameObject> Xunzhangdi = new List<GameObject>();

    [HideInInspector]
    public List<GameObject> FishPrefab = new List<GameObject>();

    private GameObject shalite;

    private GameObject haigui;

    private GameObject hetun;

    private GameObject shuimu;

    private GameObject fish;

    private GameObject m_Camera;

    private AudioClip AudioSubtraced;

    private AudioClip AudioSubtraction;

    private AudioClip Minus;

    private AudioClip EqualSign;

    private AudioClip Cha;






    public int subtracted
    {
        get { return Subtracted; }
        set { Subtracted = value; }
    }

    public int subtraction
    {
        get
        {
            return Subtraction;
        }

        set
        {
            Subtraction = value;
        }
    }

    public int completiontimes
    {
        get
        {
            return CompletionTimes;
        }

        set
        {
            CompletionTimes = value;
        }
    }

    public int difference
    {
        get
        {
            return Difference;
        }

        set
        {
            Difference = value;
        }
    }

    public float random
    {
        get
        {
            return Random;
        }

        set
        {
            Random = value;
        }
    }

    public float rang
    {
        get
        {
            return Rang;
        }
    }

    public float alpha
    {
        get
        {
            return Alpha;
        }

        set
        {
            Alpha = value;
        }
    }

    public bool Fadein
    {
        get
        {
            return FadeIn;
        }

        set
        {
            FadeIn = value;
        }
    }

    public bool Fadeout
    {
        get
        {
            return FadeOut;
        }

        set
        {
            FadeOut = value;
        }
    }

    public GameObject Camera
    {
        get
        {
            return m_Camera;
        }

        set
        {
            m_Camera = value;
        }
    }

    public bool m_IsPufferFish
    {
        get
        {
            return IsPufferFish;
        }

        set
        {
            IsPufferFish = value;
        }
    }

    public AudioClip My_AudioSubtraced
    {
        get
        {
            return AudioSubtraced;
        }

        set
        {
            AudioSubtraced = value;
        }
    }

    public AudioClip My_AudioSubtraction
    {
        get
        {
            return AudioSubtraction;
        }

        set
        {
            AudioSubtraction = value;
        }
    }

    public AudioClip My_Minus
    {
        get
        {
            return Minus;
        }

        set
        {
            Minus = value;
        }
    }

    public AudioClip My_EqualSign
    {
        get
        {
            return EqualSign;
        }

        set
        {
            EqualSign = value;
        }
    }

    public AudioClip My_Cha
    {
        get
        {
            return Cha;
        }

        set
        {
            Cha = value;
        }
    }

    public bool IsAudioplay
    {
        get
        {
            return IsAudioPlay;
        }

        set
        {
            IsAudioPlay = value;
        }
    }

    public float speed
    {
        get
        {
            return Speed;
        }

        set
        {
            Speed = value;
        }
    }

    public bool isDown
    {
        get
        {
            return IsDown;
        }

        set
        {
            IsDown = value;
        }
    }

    public bool XunZhangmove
    {
        get
        {
            return XunZhangMove;
        }

        set
        {
            XunZhangMove = value;
        }
    }

    public float Scalespeed
    {
        get
        {
            return ScaleSpeed;
        }

        set
        {
            ScaleSpeed = value;
        }
    }

    public bool isMoveUp
    {
        get
        {
            return IsMoveUp;
        }

        set
        {
            IsMoveUp = value;
        }
    }

    public AudioClip Statraudio
    {
        get
        {
            return StatrAudio;
        }

        set
        {
            StatrAudio = value;
        }
    }

    public bool LeDimoveIn
    {
        get
        {
            return LeDiMoveIN;
        }

        set
        {
            LeDiMoveIN = value;
        }
    }

    public bool LeDiMoveout
    {
        get
        {
            return LeDiMoveOut;
        }

        set
        {
            LeDiMoveOut = value;
        }
    }

    public bool LeDiJZMovein
    {
        get
        {
            return LeDiJZMoveIn;
        }

        set
        {
            LeDiJZMoveIn = value;
        }
    }

    public GameObject Shalite
    {
        get
        {
            return shalite;
        }

        set
        {
            shalite = value;
        }
    }

    public GameObject Haigui
    {
        get
        {
            return haigui;
        }

        set
        {
            haigui = value;
        }
    }

    public GameObject Hetun
    {
        get
        {
            return hetun;
        }

        set
        {
            hetun = value;
        }
    }

    public GameObject Shuimu
    {
        get
        {
            return shuimu;
        }

        set
        {
            shuimu = value;
        }
    }

    public GameObject M_Fish
    {
        get
        {
            return fish;
        }

        set
        {
            fish = value;
        }
    }

    public bool Isshuimu
    {
        get
        {
            return isshuimu;
        }

        set
        {
            isshuimu = value;
        }
    }

    public bool Isgobig
    {
        get
        {
            return IsGoBig;
        }

        set
        {
            IsGoBig = value;
        }
    }

    public bool Isformula
    {
        get
        {
            return IsFormula;
        }

        set
        {
            IsFormula = value;
        }
    }

    public float Formualrotate
    {
        get
        {
            return FormualRotate;
        }

        set
        {
            FormualRotate = value;
        }
    }

    public bool IsFormualback
    {
        get
        {
            return IsFormualBack;
        }

        set
        {
            IsFormualBack = value;
        }
    }
}
    


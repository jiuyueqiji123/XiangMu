﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishType{

    public static string SHALITE = "shalite";
    public static string HETUN = "hetun";
    public static string HAIGUI = "haigui";
    public static string SHUIMU = "shuimu";
}

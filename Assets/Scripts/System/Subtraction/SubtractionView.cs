﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubtractionView {

    private const string PATH_SUB = "study_subtraction/study_subtraced_ugui/StudySubtractionFrom";

    private HUIFormScript From;

    public GameObject hand_click1;

    private GameObject BtnBack;

    private GameObject Subtraction;

    private GameObject SubtractionText;

    private GameObject SubtracedText;

    private GameObject Current_Subtraced;

    private GameObject BigFishTank;

    private GameObject FishTank;

    private GameObject zhouzi;

    private GameObject Formula_Subtraced_Text;

    private GameObject Formula_Subtraction_Text;

    private GameObject Cha;

    private GameObject Components;

    public GameObject CarryOutButton;

    private Camera UI_Camera;

    private GameObject Subtraced;

    private GameObject XunZhangDi;

    private GameObject LeDi;

    private GameObject jizhang;

    private GameObject Formualperents;

    private GameObject Mask;

    private GameObject XunZhang;

    public GameObject XunZhangtubiao;

    public GameObject CarryOutButtonMask;

    public GameObject hand_click;

    public GameObject bianjie1;

    public GameObject bianjie2;

    public GameObject bianjie3;

    private bool IsMoveUp = true;

    private bool IsMoveDown = false;

    private bool UpOrDown = true;

    private float Scale_Button = 1.1f;

    public float Scale_speed = 0.25f;


    public void OpenFrom()
    {
        From = HUIManager.Instance.OpenForm (PATH_SUB,false);
        //HUIManager.Instance.OpenForm("study_subtraction/study_subtraced_ugui/StudySubtractionMaskFrom",false);
        UI_Camera = From.gameObject.GetComponent<Canvas>().worldCamera;
        //this.From.gameObject.layer = 10;
        //Debug.LogError(UI_Camera.name);
        UI_Camera.gameObject.AddComponent<InputPhysics2DRaycaster>();

        this.CarryOutButton = this.From.GetWidget("CarryOutButton");
        HUIUtility.SetUIMiniEvent(this.CarryOutButton,enUIEventType.Click,enUIEventID.Subtraction_CarryOut);

        this.BtnBack =  this.From.GetWidget("ButtonBack");
        HUIUtility.SetUIMiniEvent(this.BtnBack,enUIEventType.Click,enUIEventID.Subtraction_BtnBack);

        this.BtnBack = this.From.GetWidget("jizhang");
        HUIUtility.SetUIMiniEvent(this.BtnBack, enUIEventType.Click, enUIEventID.Subtraction_JiZhang);

        this.CarryOutButtonMask = this.From.GetWidget("CarryOutButtonMask");

        this.Subtraction = this.From.GetWidget("Subtraction");

        this.SubtracedText = this.From.GetWidget("SubtracedText");

        this.SubtractionText = this.From.GetWidget("SubtractionText");

        this.BigFishTank = this.From.GetWidget("BigFishTank");

        this.FishTank = this.From.GetWidget("FishTank");

        this.zhouzi = this.From.GetWidget("zhouzi");

        this.Components = this.From.GetWidget("Components");

        this.Formula_Subtraced_Text = this.From.GetWidget("Formula_Subtraced");

        this.Formula_Subtraction_Text = this.From.GetWidget("Formula_Subtraction");

        this.Cha = this.From.GetWidget("cha");

        this.XunZhangDi = this.From.GetWidget("XunZhangDi");

        this.XunZhang = this.From.GetWidget("xunzhang");

        this.XunZhangtubiao = this.From.GetWidget("XunZhangtubiao");

        this.LeDi = this.From.GetWidget("LeDi");
        this.LeDi.transform.localPosition = new Vector3(-1200,-560,0);

        this.jizhang = this.From.GetWidget("jizhang");

        Mask = this.From.GetWidget("Mask");

        this.Formualperents = this.From.GetWidget("Formualparents");

        this.hand_click = this.From.GetWidget("hand_click");

        this.hand_click1 = this.From.GetWidget("hand_click1");

        this.bianjie1 = this.From.GetWidget("bianjie1");

        this.bianjie2 = this.From.GetWidget("bianjie2");

        this.bianjie3 = this.From.GetWidget("bianjie3");
    }

    //public void Star()
    //{
    //    Components.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0f, 0f, 0f);

    //}

    public void Close()
    {
        HUIManager.Instance.CloseForm(PATH_SUB);
    }


    public List<GameObject> Activation(int Subtraced,List<GameObject> Animals,List<Vector3> start_pos,Texture2D Fish)
    {
        Sprite fish;
        int i = 0;
        fish = Sprite.Create(Fish,new Rect(0,0,Fish.width,Fish.height),new Vector2(0,0));
        foreach (Transform subtraced in this.Subtraction.transform)
        {
            if (subtraced.gameObject.name == Subtraced.ToString())
            {

                
                foreach (Transform Animal in subtraced)
                {
                    Animals.Add(Animal.gameObject);
                    Debug.LogError(Animals[i]);
                    Debug.LogError(start_pos.Count);
                    start_pos.Add(Animals[i].GetComponent<RectTransform>().anchoredPosition3D);
                    Animal.GetComponent<Image>().sprite = fish;
                    //Animal.gameObject.GetComponent<Image>().color = new Color(255f, 255f, 255f, 0f);
                    i++;
                }
                this.Subtraced = subtraced.gameObject;
                subtraced.gameObject.SetActive(true);
                Current_Subtraced = subtraced.gameObject;
                return Animals;                
            }
        }
        return null;
    }

    

    public void XunZhangActivation(List<GameObject> XunZhangdi)
    {
        foreach (Transform xunzhangdi in this.XunZhangDi.transform)
        {
            XunZhangdi.Add(xunzhangdi.gameObject);
        }
    }

    public void OpenJZButton()
    {
        jizhang.gameObject.SetActive(true);
    }


    public void FadeInAndFadeOut(float Alpha)
    {
        if (Alpha <= 1 || Alpha >= 0)
        {
            //if (Current_Subtraced == null)
            //{
            //    return;
            //}
            //Current_Subtraced.GetComponent<CanvasGroup>().alpha = Alpha;
            if (SubtracedText == null)
            {
                return;
            }
            SubtracedText.GetComponent<CanvasGroup>().alpha = Alpha;
            SubtractionText.GetComponent<CanvasGroup>().alpha = Alpha;
        }
        return;
    }

    public void MoveUp(float Speed)
    {
        //Components.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0f, 0f, 0f);

        

        if (IsmoveUp())
        {                      
            Components.GetComponent<RectTransform>().anchoredPosition += new Vector2(0, Time.deltaTime * Speed);
            
        }

        
       
    }

    public void MoveDown(float Speed)
    {
       // Components.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0f,234f,0f);
        
        if (IsmoveDown())
        {
            Components.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0, Time.deltaTime * Speed);
        }
    }


    public void closetubiao()
    {
        this.XunZhangtubiao.SetActive(true);
        this.XunZhangDi.SetActive(false);
    }


    public void AssignmentText(string Subtraced,string Subtraction)
    {
        this.SubtracedText.GetComponent<Text>().text = Subtraced;
        this.SubtractionText.GetComponent<Text>().text = Subtraction;
    }
    
    public bool IsmoveUp()
    {
        //Debug.LogError(Components.GetComponent<RectTransform>().anchoredPosition3D.y);
        if (Components.GetComponent<RectTransform>().anchoredPosition3D.y >= 176)
        {
            //Debug.LogError(Components.GetComponent<RectTransform>().anchoredPosition3D.y + "111111111");
            
            IsMoveUp = false;

            return IsMoveUp;
        }
        return IsMoveUp;
    }

    public bool IsmoveDown()
    {
        //if (IsAuidoPlay)
        //{
        //Debug.LogError(Components.GetComponent<RectTransform>().anchoredPosition3D.y);
        if (Components == null)
        {
            return false;
        }
        if (Components.GetComponent<RectTransform>().anchoredPosition3D.y <= 0)
        {
            // Debug.LogError(Components.GetComponent<RectTransform>().anchoredPosition3D.y + "111111111");
            IsMoveDown = false;
            return IsMoveDown;
        }
        //}

        return IsMoveDown;
    }


    public void AssignmentFormula(int Subtraced,int Subtraction)
    {
        this.Formula_Subtraced_Text.GetComponent<Text>().text = Subtraced.ToString();
        this.Formula_Subtraction_Text.GetComponent<Text>().text = Subtraction.ToString();
        this.Cha.GetComponent<Text>().text = (Subtraced - Subtraction).ToString();
    }


    public void ButtonController(List<GameObject> FishRongQi)
    {
        if (CarryOutButton == null)
        {
            return;
        }
        if (FishRongQi.Count >= 0)
        {
            this.CarryOutButton.SetActive(true);
        }
        if (this.CarryOutButton.activeSelf && FishRongQi.Count == 0)
        {
            this.CarryOutButtonMask.SetActive(true);
        }
        else
        {
            this.CarryOutButtonMask.SetActive(false);
        }
        if (!this.CarryOutButtonMask.activeSelf)
        {
            Scale_Button += Time.deltaTime * Scale_speed;
            this.CarryOutButton.GetComponent<RectTransform>().localScale = new Vector3(Scale_Button, Scale_Button, Scale_Button);
            if (Scale_Button >= 1.1f || Scale_Button <= 0.9f)
            {
                Scale_speed = -Scale_speed;
            }
        }
        else
        {
            this.CarryOutButton.transform.localScale = new Vector3(1,1,1);
        }
    }

    public void SetButtonSclea()
    {
        this.CarryOutButtonMask.SetActive(true);
        this.CarryOutButton.transform.localScale = new Vector3(1, 1, 1);
    }

    //public void XunzhangMove()
    //{

    //}





    //public void ImageAssignment(List<GameObject> Animals)
    //{
    //    foreach (GameObject Fish in Animals)
    //    {
    //        Fish.GetComponent<Image>()
    //    }
    //}

    public Camera ui_camera
    {
        get
        {
            return UI_Camera;
        }

    }

    public bool m_IsMoveUp
    {
        get
        {
            return IsMoveUp;
        }

        set
        {
            IsMoveUp = value;
        }
    }
    public bool m_IsMoveDown
    {
        get
        {
            return IsMoveDown;
        }

        set
        {
            IsMoveDown = value;
        }
    }

    public GameObject My_Components
    {
        get
        {
            return Components;
        }

        set
        {
            Components = value;
        }
    }

    public GameObject subtraced
    {
        get
        {
            return Subtraced;
        }

        set
        {
            Subtraced = value;
        }
    }

    public GameObject Ledi
    {
        get
        {
            return LeDi;
        }

        set
        {
            LeDi = value;
        }
    }

    public GameObject subtraction
    {
        get
        {
            return Subtraction;
        }

        set
        {
            Subtraction = value;
        }
    }

    public GameObject mask
    {
        get
        {
            return Mask;
        }

        set
        {
            Mask = value;
        }
    }

    public GameObject Jizhang
    {
        get
        {
            return jizhang;
        }

        set
        {
            jizhang = value;
        }
    }

    public GameObject FormualPerents
    {
        get
        {
            return Formualperents;
        }

        set
        {
            Formualperents = value;
        }
    }

    public GameObject xunzhang
    {
        get
        {
            return XunZhang;
        }

        set
        {
            XunZhang = value;
        }
    }
}

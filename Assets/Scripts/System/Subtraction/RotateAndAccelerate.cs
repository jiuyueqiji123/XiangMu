﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAndAccelerate : MonoBehaviour {

    SubtractionModel Model;
    SubtractionView View;
    EventListen Eventlisten;
    [HideInInspector]
    public float speed;
    [HideInInspector]
    public bool IsRotate = false;
    [HideInInspector]
    public bool IsContact = false;
    [HideInInspector]
    public bool IsDrag = false;
    [HideInInspector]
    public Vector3 Animal_Pos;
    [HideInInspector]
    public bool IsOpenEffect = false;

   

    private float Animal_Pos_x = 0;

    private Transform Rect;

	// Use this for initialization
	void Start () {
        
        Model = SubtractionController.Instance.GetModel();
        View = SubtractionController.Instance.GetView();
        Eventlisten = this.gameObject.GetComponent<EventListen>();
        Rect = this.gameObject.GetComponent<Transform>();
        if (Model.Isshuimu)
        {
            speed = -50;
        }
        else
        {
            speed = -70;
        }
        
        Animal_Pos_x = this.gameObject.transform.localPosition.x;
        Animal_Pos = new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z);
        StartMove();

        //Debug.LogError(this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
        //y -= Time.deltaTime*10;
        if (Model.RandAIsMove)
        {
            Move();
            M_Rotate(Random_Rotate(IsRotate));
        }
        Accelerate(Random_Accelerate());
        OpenEffect(Random_OpenEffect(IsOpenEffect));
        //Debug.LogError("==============================================" + this.Model.FishTankRongQi[0].transform.position.y + "------------------ X");
        if (this.gameObject.transform.localPosition.y <= this.View.bianjie3.transform.position.y && (this.gameObject.transform.position.x <= -27.5f || this.gameObject.transform.position.x >= 27.5f) && this.Model.IsRotate)
        {
            Debug.LogError("111111111111111111");
            M_Rotate(true);
        }
        //SubtractionController.Instance.Update();
        //SubtractionController.Instance.OnCarryOut();

    }

    public void M_Rotate(bool Isrotate)
    {

        //IsRotate = Random_Rotate(IsRotate);
        if (Isrotate)
        {
            //Debug.LogError(Isrotate);
            //Rect.Rotate(new Vector3(0, 180, 0));
            //Debug.LogError(this.gameObject.GetComponent<RectTransform>());
            //Debug.LogError(Model.Isshuimu);
            speed = -speed;
            Rect.localScale = new Vector3(Rect.localScale.x * -1, Rect.localScale.y, Rect.localScale.z);
            this.gameObject.transform.GetChild(0).localScale = new Vector3(-1 * this.gameObject.transform.GetChild(0).localScale.x, 1 * this.gameObject.transform.GetChild(0).localScale.y, 1 * this.gameObject.transform.GetChild(0).localScale.z);
           
            IsRotate = false;
            this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.gameObject.GetComponent<BoxCollider2D>().size.x * 0.95f, this.gameObject.GetComponent<BoxCollider2D>().size.y * 0.95f);
            TimerManager.Instance.AddTimer(0.2f, () =>
             {
                 if (this == null)
                 {
                     return;
                 }
                 this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.gameObject.GetComponent<BoxCollider2D>().size.x / 0.95f, this.gameObject.GetComponent<BoxCollider2D>().size.y / 0.95f);
             });
            if (Model.Isshuimu)
            {
                if (this.gameObject.transform.rotation.eulerAngles.z > 180)
                {
                    this.gameObject.transform.rotation = Quaternion.Euler(0, 180, 20);
                    return;
                }

                if (this.gameObject.transform.rotation.eulerAngles.z < 180)
                {
                    Debug.LogError("这里这里这里！！！");
                    this.gameObject.transform.rotation = Quaternion.Euler(0, 180, -20);
                    return;
                }
            }
        }
    }


    public void OpenEffect(bool IsOpenEffect)
    {
        if (IsOpenEffect)
        {
            StartCoroutine(OpenEffect());
            
        }
    }

    //public void 

    public void Move()
    {
        if (!IsDrag)
        {
            if (IsContact)
            {
                Animal_Pos = new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z);
            }
            
            Animal_Pos += Vector3.right * Time.deltaTime * speed;
            this.gameObject.transform.localPosition = Animal_Pos;
        }
    }

    public bool Random_Rotate(bool isRotate)
    {
        float i = Random.Range(1f, 10f);
        //Debug.LogError(i);
        if (i >= 9.997f)
        {
            isRotate = true;
            return  isRotate;
        }
        return false;
    }


    public bool Random_OpenEffect(bool isRotate)
    {
        float i = Random.Range(1f, 10f);
        if (speed > 70 || Model.Fadein || Model.Fadeout || Model.alpha < 1 || this.gameObject.transform.localScale.y > 1)
        {
            i = 0;
            return false;
        }
        //Debug.LogError(i);
        if (i >= 9.990f)
        {
            AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/603050102", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip);
            isRotate = true;
            return isRotate;
        }
        return false;
    }

    public bool Random_Accelerate()
    {
        float m_random = Random.Range(0f,1f);

        if (m_random >= 0.999f)
        {
            return true;
        }
        return false;
    }

    public void Accelerate(bool IsAccelerate)
    {
        if (IsAccelerate)
        {
            speed *= 2f;
            Invoke("Recovery", 1f);
        }

        
    }

    public void Recovery()
    {
        speed /= 2f;
    }
    void OnTriggerExit2D(Collider2D other)
    {
        //if (!IsDrag)
        //{
        //    IsRotate = true;
        //    M_Rotate(IsRotate);
        //}

        IsContact = false;
        if (((other.gameObject.name == "FishTank") && IsDrag))
        {
            
            if (SubtractionController.Instance.FishTankRongQi.Count == 0)
            {
                return;
            }
            else
            {
                SubtractionController.Instance.RemAnimal(this.gameObject);
            }
            //Debug.LogError(SubtractionController.Instance.FishTankRongQi.Count);
        }

        
        //Debug.LogError(model.FishTankRongQi);
    }

    void OnTriggerStay2D(Collider2D other)
    {

        // Debug.LogError(other.gameObject.name);
        if ((other.name == "FishTank" || other.gameObject.name == "BigFishTank") && Eventlisten.isDrag)
        {
            IsContact = true;
        }

        if (other.gameObject.name == "bianjie" && !Eventlisten.IsClick)
        {
            if (Model.m_IsPufferFish)
            {
                return;
            }
            M_Rotate(true);
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "bianjie" && !IsDrag && !Eventlisten.IsBack)
        {
            IsRotate = true;
            M_Rotate(IsRotate);
        }


        if (other.gameObject.name == "FishTank")
        {
            //Debug.LogError("222222222222222");
            if (SubtractionController.Instance.FishTankRongQi.Count == 0)
            {
                SubtractionController.Instance.AddAnimal(this.gameObject);
                Debug.LogError(this.gameObject.name);
                //Debug.LogError(SubtractionController.Instance.FishTankRongQi.Count);
                return;
            }
            foreach (GameObject Fish in SubtractionController.Instance.FishTankRongQi)
            {
                if (Fish == this.gameObject)
                {
                    return;
                }
            }
            SubtractionController.Instance.AddAnimal(this.gameObject);
            Debug.LogError(this.gameObject.name);
            //Debug.LogError(SubtractionController.Instance.FishTankRongQi.Count);
        }
        if (other.gameObject.name == "BigFishTank")
        {
            if (SubtractionController.Instance.FishTankRongQi.Count == 0)
            {
                return;
            }
            else
            {
                SubtractionController.Instance.RemAnimal(this.gameObject);
            }
        }

    }

    public void StartMove()
    {
        float i = Random.Range(0f,1f);
        if (i < 0.5)
        {
            speed = -speed;
            this.gameObject.transform.localScale = new Vector3(-1 * this.gameObject.transform.localScale.x, 1, 1);
            if (Model.Isshuimu)
            {
                if (this.gameObject.transform.rotation.eulerAngles.z > 180)
                {
                    this.gameObject.transform.rotation = Quaternion.Euler(0, 180, 20);
                    return;
                }

                if (this.gameObject.transform.rotation.eulerAngles.z < 180)
                {
                    Debug.LogError("这里这里这里！！！");
                    this.gameObject.transform.rotation = Quaternion.Euler(0, 180, -20);
                    return;
                }
            }
            
            

        }

    }

    IEnumerator OpenEffect()
    {
        this.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        if (Model.Isshuimu)
        {
            yield return new WaitForSeconds(2.5f);
        }
        else
        {
            yield return new WaitForSeconds(1f);
        }
        this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SubtractionController : MonoSingleton<SubtractionController>, IEasyTimer
{

    public bool bTimerCallback
    {
        get { return isGamePlaying; }
    }


    private  SubtractionModel Model;
    private  SubtractionView View;
    private Color color;
    [HideInInspector]
    public bool IsGuiDe = false;
    private float Timer = 0;
    private bool IsMove = true;
    private int EnterGameTime = 0;
    private float Alpha = 0;
    private bool IsFadeIn = false;
    private bool IsfadeOut = false;
    private bool isGamePlaying = false;

    protected override void Awake()
    {
        base.Awake();
        GetFish();
        ActivationImage();
        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Five);
        AudioManager.Instance.PlayMusic("study_subtraction/study_subtraced_sound/603050101",true,false);
        Input.multiTouchEnabled = false;
    }


    protected override void Init()
    {
        base.Init();
        
        Model = new SubtractionModel();
        View = new SubtractionView();
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Subtraction_CarryOut,OnClickCarryOut);
        
        EventBus.Instance.AddEventHandler(EventID.CARRY_OUT_ANIMALGAME, OnCarryOut);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Subtraction_JiZhang, OnCarryOutGame);
    }


    protected override void OnDestroy()
    {
        base.OnDestroy();
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Subtraction_CarryOut, OnClickCarryOut);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Subtraction_BtnBack, OnExitGame);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Subtraction_JiZhang, OnCarryOutGame);
        EventBus.Instance.RemoveEventHandler(EventID.CARRY_OUT_ANIMALGAME, OnCarryOut);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Timer = 0;
            Alpha = 0;
        }
        
        if (!Model.IsCarryOut_Button)
        {
            this.View.ButtonController(Model.FishTankRongQi);
        }

        if (!PlayerPrefs.HasKey("SubtractionKey") && IsGuiDe)
        {
            //Debug.LogError(Alpha);
            if (IsFadeIn)
            {
                Alpha += Time.deltaTime;
                if (Alpha >= 1)
                {
                    IsFadeIn = false;
                }
            }
            if (IsfadeOut)
            {
                Alpha -= Time.deltaTime *2;
                if (Alpha <= 0)
                {
                    IsfadeOut = false;
                }
            }
            Timer += Time.deltaTime;
            this.View.hand_click.transform.GetChild(0).gameObject.GetComponent<CanvasGroup>().alpha = this.Alpha * 0.6f;
            this.View.hand_click.transform.GetChild(1).gameObject.GetComponent<CanvasGroup>().alpha = this.Alpha;
            if (Timer >= 5f && IsMove)
            {
                Debug.LogError("=====================   IsMove");
                IsFadeIn = true;
                View.hand_click.SetActive(true);

                this.AddTimerEx(1f, ()=>
                {
                    View.hand_click.transform.DOLocalMove(new Vector3(0, -280, 0), 1.5f);
                });
               
                IsMove = false;
                
            }
            if (Timer >= 5f && View.hand_click.transform.localPosition.y <= -279)
            {
                IsfadeOut = true;
                Debug.LogError("      =========================================== IsMoveTrue");
                this.AddTimerEx(0.5f, ()=>
                {
                    //View.hand_click.SetActive(false);
                    Alpha = 0;
                    View.hand_click.transform.DOLocalMove(new Vector3(0, 150, 0f), 0.2f);
                });
                Debug.LogError(View.hand_click.transform.localPosition + "=============================");
                Timer = 0;
                
                IsMove = true;
                //IsGuiDe = false;
            }
        }
        

        //Debug.LogError(Model.FishTankRongQi.Count);
        if (Model.Fadein)
        {
            Model.alpha += Time.deltaTime * 1f;
            this.View.FadeInAndFadeOut(Model.alpha);
            if (Model.alpha >= 1)
            {
                Model.Fadein = false;
            }
        }
        if (Model.Fadeout)
        {
            Model.alpha -= Time.deltaTime * 1f;
            this.View.FadeInAndFadeOut(Model.alpha);
            if (Model.alpha <= 0)
            {
                Model.Fadeout = false;
            }
        }
        if (this.Model.LeDimoveIn)
        {
            this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D = Vector3.MoveTowards(this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D, new Vector3(-560, 0, -200), 2000f * Time.deltaTime);
            //Debug.LogError(this.View.Ledi.transform.localPosition);
            if (this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D == new Vector3(-560, 0, -200))
            {
                this.Model.LeDimoveIn = false;
            }
        }
        if (this.Model.LeDiMoveout)
        {
            //Debug.LogError("11111111111111");
            this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D = Vector3.MoveTowards(this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D, new Vector3(-1200, 0, -200), 2000f * Time.deltaTime);
            if (this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D == new Vector3(-1200, 0, -200))
            {
                //Debug.LogError("11111111111111");
                this.Model.LeDiMoveout = false;
                this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0f, -1260, -200);
            }
        }
        //Debug.LogWarning(Model.subtraction);
        if (this.Model.isMoveUp)
        {
            EventBus.Instance.BroadCastEvent(EventID.CARRY_OUT_ANIMALGAME);
            if (this.View.My_Components.GetComponent<RectTransform>().anchoredPosition.y > 176f)
            {
                this.Model.Isformula = true;
            }
            if (this.View.My_Components.GetComponent<RectTransform>().anchoredPosition.y > 176f && this.Model.isDown && Model.IsPlayAudio)
            {
                OnAgainGame();
                this.Model.isDown = false;
            }

        }
        if (this.View.m_IsMoveDown)
        {
            this.View.MoveDown(this.Model.speed);
        }
        if (Model.Isgobig)
        {
            this.View.xunzhang.transform.localScale += Time.deltaTime * new Vector3(1,1,1)*5;
            if (this.View.xunzhang.transform.localScale.y >= 2.5)
            {
                
                this.Model.Isgobig = false;
            }
        }
        if (this.Model.XunZhangmove)
        {
            if (this.View.xunzhang == null)
            {
                return;
            }
            if (this.View.xunzhang.transform.localScale.y <= 1)
            {
                this.Model.Scalespeed = 0f;
            }
            XunzhangMove(this.Model.Xunzhangdi[this.Model.completiontimes].transform.position, this.Model.Scalespeed);
            if (this.View.xunzhang.transform.position == this.Model.Xunzhangdi[this.Model.completiontimes].transform.position)
            {
                AdsManager.Instance.ShowAds(AdsNameConst.Ads_Xunzhang);
                this.Model.Xunzhangdi[this.Model.completiontimes].transform.GetChild(0).gameObject.SetActive(true);
                Debug.LogError(this.Model.completiontimes);
                this.View.xunzhang.transform.localPosition = new Vector3(0, 0, 0);
                this.View.xunzhang.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                this.View.xunzhang.SetActive(false);
                this.Model.XunZhangmove = false;
            }


        }

        if (this.Model.LeDiJZMovein)
        {
            if (View.Ledi == null)
            {
                return;
            }
            this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D = Vector3.MoveTowards(this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D, new Vector3(0f, 230, -200f), 2000f * Time.deltaTime);
            if (this.View.Ledi.GetComponent<RectTransform>().anchoredPosition3D == new Vector3(0, 230, -200))
            {
                
                this.Model.LeDiJZMovein = false;

            }
        }

        if (Model.Isformula)
        {
            this.RotateFormual(300);
        }
        //Debug.LogError("1111111111111");
        if (Model.IsFormualback)
        {
            RotateFormualBack(300);
        }
        if (this.Model.FishTankRongQi != null)
        {
            if (this.Model.FishTankRongQi.Count != this.Model.subtraction && IsGuiDe)
            {
                if (this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>() == null)
                {
                    return;
                }
                this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>().enabled = true;
            }
        }
        

        


        //this.View.MoveDown(this.Model.IsAudioplay,this.Model.speed);
        //if (this.View.My_Components.GetComponent<RectTransform>().anchoredPosition.y >= 235f)
        //{
        //    StartCoroutine(AudioPlay());
        //}

    }
    public void RotateFormual(float speed)
    {
        if (this.View.FormualPerents == null)
        {
            return;
        }
        this.Model.Formualrotate -= Time.deltaTime * speed;
        if (this.Model.Formualrotate <= 0)
        {
            this.Model.Formualrotate = 0;
            speed = 0;
            Model.Isformula = false;
            
        }
        foreach (Transform Formual in this.View.FormualPerents.transform)
        {
            //Debug.LogError(this.Model.Formualrotate);
            Formual.localRotation = Quaternion.Euler(new Vector3(0, this.Model.Formualrotate, 0));
        }

    }
    public void RotateFormualBack(float speed)
    {
        if (this.View.FormualPerents == null)
        {
            return;
        }
        this.Model.Formualrotate += Time.deltaTime * speed;
        if (this.Model.Formualrotate >= 90)
        {
            this.Model.Formualrotate = 90;
            speed = 0;
            Model.IsFormualback = false;

        }
        foreach (Transform Formual in this.View.FormualPerents.transform)
        {
            //Debug.LogError(this.Model.Formualrotate);
            Formual.localRotation = Quaternion.Euler(new Vector3(0, this.Model.Formualrotate, 0));
        }

    }
    public void ActivationImage()
    {
        Model.Fish.Add((ResourceManager.Instance.GetResource("Study_Subtraction/shaliteyu", typeof(Texture2D), enResourceType.Prefab).m_content as Texture2D));
        Model.Fish.Add((ResourceManager.Instance.GetResource("Study_Subtraction/shuimu", typeof(Texture2D), enResourceType.Prefab).m_content as Texture2D));
        Model.Fish.Add((ResourceManager.Instance.GetResource("Study_Subtraction/wugui", typeof(Texture2D), enResourceType.Prefab).m_content as Texture2D));
        Model.Fish.Add((ResourceManager.Instance.GetResource("Study_Subtraction/xiaohaitun", typeof(Texture2D), enResourceType.Prefab).m_content as Texture2D));
    }


    public void EnterGame()
    {
        //MainController.Instance.CloseView();
        isGamePlaying = true;
        //this.View.Star();
        this.View.OpenFrom();
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Subtraction_BtnBack, OnExitGame);
        StartCoroutine("LeDiAudioPlay");
    }

    public void OnCarryOutGame(HUIEvent evt)
    {
        //AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("sound/601010111",typeof(AudioClip),enResourceType.Sound,false).m_content as AudioClip);
        AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/603020207", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip);
        this.View.Jizhang.transform.GetChild(1).gameObject.SetActive(true);
        this.View.Jizhang.transform.GetChild(0).gameObject.SetActive(false);

        Invoke("OnExitGame", 2f);
        //WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        //MedalWindow.Instance.ShowXunzhang(1, true, MedalWindow.EXZCountEnum.Five, () => {
        //    Debug.
        //});
    }


    public void OnExitGame()
    {
        StopAllCoroutines();
        //SceneLoader.Instance.LoadScene(SceneLoader.MainScene);
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }

    public void OnExitGame(HUIEvent evt)
    {
        StopAllCoroutines();
        //SceneLoader.Instance.LoadScene(SceneLoader.MainScene);
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Subtraction_BtnBack, OnExitGame);
    }

    public void ExitGame()
    {
        StopAllCoroutines();
        this.View.Ledi.transform.GetChild(3).gameObject.SetActive(false); ;
        ReductionGame();
        this.View.Close();
        this.Model.completiontimes = 0;
        this.IsGuiDe = false;
        this.View.Ledi.transform.localPosition = new Vector3(-1200, -560, -100);
        this.Model.Isshuimu = false;
        this.View.Ledi.transform.GetChild(1).gameObject.SetActive(false);
        isGamePlaying = false;
        Destroy(this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>());
        
    }

    public void RandomNum()
    {
        Model.random = Random.Range(1, Model.rang);
        Model.subtracted = (int)Mathf.Ceil(Model.random);

        Model.random = Random.Range(1, (float)Model.subtracted);
        Model.subtraction = (int)Mathf.FloorToInt(Model.random);

    }

    public void Initialization_Animal()
    {
        AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/603050106", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip);
        this.Activation(Model.subtracted,this.Model.Animals,this.Model.Start_Pos,Model.FishPrefab[RandomImage()]);
        this.View.XunZhangActivation(this.Model.Xunzhangdi);
        this.View.AssignmentFormula(this.Model.subtracted,this.Model.subtraction);
        for (int i = 0; i < this.Model.Animals.Count; i++)
        {
            this.Model.Animals[i].AddComponent<RotateAndAccelerate>();
            this.Model.Animals[i].AddComponent<EventListen>();
            if (Model.m_IsPufferFish)
            {
                this.Model.Animals[i].transform.GetChild(1).GetComponent<Animator>().Play("Take 001", 0, 0.1f * i);
                
            }
            else
            {
                this.Model.Animals[i].GetComponent<Animator>().Play("Take 001", 0, 0.1f * i);
            }
            
        }
    }

    public List<GameObject> Activation(int Subtraced, List<GameObject> Animals, List<Vector3> start_pos, GameObject Fish)
    {
        for (int i = 0; i < Subtraced; i++)
        {
            this.Model.M_Fish = Instantiate(Fish);
            Animals.Add(this.Model.M_Fish);
            HUIUtility.SetParent(this.Model.M_Fish.transform, this.View.subtraction.transform.GetChild(0));
            //Fish.transform.parent = this.View.subtraced.transform.GetChild(0);
            this.Model.M_Fish.transform.localPosition = new Vector3(Random.Range(-330f,330f),Random.Range(-80f,30f), -100f-(i-1)*20f);
            if (Model.m_IsPufferFish)
            {
                this.Model.M_Fish.transform.localRotation = Quaternion.Euler(0, 0, 0);
            }
            else if (Model.Isshuimu)
            {
                this.Model.M_Fish.transform.localRotation = Quaternion.Euler(0, 180, -20);
            }
            else
            {
                this.Model.M_Fish.transform.localRotation = Quaternion.Euler(0, 180, 0);
            }
            start_pos.Add(Fish.transform.localPosition);
        }
        return Animals;
    }

    public void RemoveAnimals()
    {
        foreach (GameObject animal in this.Model.Animals)
        {
            Destroy(animal);
        }
    }



    public void Reduction(List<GameObject> Animals, List<Vector3> start_pos)
    {
        int i = 0;
        foreach (GameObject animal in Animals)
        {
            animal.transform.localPosition = start_pos[i];
            Destroy(animal.GetComponent<RotateAndAccelerate>());
            Destroy(animal.GetComponent<EventListen>());
            animal.transform.rotation = Quaternion.Euler(0f,-180f,animal.transform.localRotation.z);
            
            i++;
        }
        Animals.Clear();
    }

    public void ReductionGame()
    {
        //foreach (Transform Formual in this.View.FormualPerents.transform)
        //{
        //    Formual.localRotation = Quaternion.Euler(new Vector3(0,90,0));
        //}
        //this.Model.Formualrotate = 90f;
        this.Model.FishTankRongQi.Clear();
        this.Model.Animals.Clear();
        this.Model.Xunzhangdi.Clear();
        this.Model.Start_Pos.Clear();
        this.Model.isMoveUp = false;
        this.View.m_IsMoveUp = true;
        this.View.m_IsMoveDown = true;
        this.Model.IsPlayAudio = false;
        this.Model.alpha = 0;
        this.Model.isDown = true;
        this.Model.Scalespeed = 3f;
        this.Model.m_IsPufferFish = false;
        this.Model.Isshuimu = false;
        this.Model.IsCarryOut_Button = false;

    }

    public void PlayAgain()
    {
        RandomNum();
        this.View.AssignmentText(Model.subtracted.ToString(), Model.subtraction.ToString());
        Initialization_Animal();
        SetFormalAudio();
        this.Model.alpha = 0;
        this.IsGuiDe = false;
        this.View.xunzhang.transform.localScale = new Vector3(0.1f,0.1f,0.1f);
        this.Model.FishTankRongQi.Clear();
        Model.Fadein = true;

        //if (Model.m_IsPufferFish)
        //{
        //    foreach (GameObject fish in Model.Animals)
        //    {
        //        foreach (Renderer FishRenderer in GetMaterial(fish.transform.GetChild(2).gameObject))
        //        {
        //            color = FishRenderer.material.color;
        //            color.a = 0;
        //            FishRenderer.material.color = color;
        //        }
        //    }
        //}
        if (this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>() == null)
        {
            this.View.ui_camera.gameObject.AddComponent<InputPhysics2DRaycaster>();
        }
        else
        {
            this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>().enabled = true;
        }
        
    }

    public int RandomImage()
    {
        if (Model.completiontimes == 0)
        {
            Model.IsShalite = true;
            return 0;
        }

        float i = Random.Range(0f, 4f);
        //i = 2;
        if ((int)Mathf.FloorToInt(i) == 0)
        {
            Model.IsShalite = true;
        }
        else
        {
            Model.IsShalite = false;
        }
        if ((int)Mathf.FloorToInt(i) == 1)
        {
           // Debug.LogError("对的对的!");
            Model.Isshuimu = true;
        }
        else
        {
           // Debug.LogError("快看这里！！！");
            Model.Isshuimu = false;
        }
        if ((int)Mathf.FloorToInt(i) == 2)
        {
            Model.m_IsPufferFish = true;
        }
        else
        {
            Model.m_IsPufferFish = false;
        }
        if((int)Mathf.FloorToInt(i) == 3)
        {
            Model.IsWugui = true;
        }
        else
        {
            Model.IsWugui = false;
        }
        return (int)Mathf.FloorToInt(i);

    }


    public void OnCarryOut()
    {

        this.View.MoveUp(this.Model.speed);
        
    }

    public void OnAgainGame()
    {

        //this.View.MoveDown();
        StartCoroutine("AudioPlay");

       

        


    }

    public void AddAnimal(GameObject Animal)
    {
        this.Model.FishTankRongQi.Add(Animal);
    }


    public void RemAnimal(GameObject Animal)
    {
        this.Model.FishTankRongQi.Remove(Animal);
    }

    

    public List<GameObject> FishTankRongQi {

        get { return Model.FishTankRongQi; }
    }

    public SubtractionModel GetModel()
    {
        return this.Model;
    }

    public SubtractionView GetView()
    {
        return this.View;
    }


    

    public void SetFormalAudio()
    {
        this.Model.StartAudio_1 = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/403050116", typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        this.Model.StartAudio_2 = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/" + FishType() + Model.subtraction.ToString(), typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        if (Model.IsShalite || Model.m_IsPufferFish)
        {
            this.Model.StartAudio_3 = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/403050118", typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        }
        if (Model.IsWugui || Model.Isshuimu)
        {
            this.Model.StartAudio_3 = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/403050117", typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        }
        if (this.Model.subtracted >= 5)
        {
            this.Model.My_AudioSubtraced = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/4030501" + (this.Model.subtracted + 5).ToString(), typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        }
        else
        {
            this.Model.My_AudioSubtraced = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/40305010" + (this.Model.subtracted + 5).ToString(), typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        }
        if (this.Model.subtraction >= 5)
        {
            this.Model.My_AudioSubtraction = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/4030501" + (this.Model.subtraction + 5).ToString(), typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        }
        else
        {
            this.Model.My_AudioSubtraction = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/40305010" + (this.Model.subtraction + 5).ToString(), typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        }
        this.Model.My_Minus = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/403050102", typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        this.Model.My_EqualSign = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/403050103", typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        if ((this.Model.subtracted - this.Model.subtraction) >= 5)
        {
            this.Model.My_Cha = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/4030501" + ((this.Model.subtracted - this.Model.subtraction) + 5).ToString(), typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        }
        else
        {
            this.Model.My_Cha = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/40305010" + (this.Model.subtracted - this.Model.subtraction + 5).ToString(), typeof(AudioClip), enResourceType.Prefab, false).m_content as AudioClip;
        }
        Model.ErrorAudio_1 = AudioManager.Instance.GetAudioClip("study_subtraction/study_subtraced_sound/403140115");
        Model.ErrorAudio_2 = AudioManager.Instance.GetAudioClip("study_subtraction/study_subtraced_sound/403140116");
        Model.CorrectAudio = AudioManager.Instance.GetAudioClip("study_subtraction/study_subtraced_sound/403050119");
        Model.ChaAudio = AudioManager.Instance.GetAudioClip("study_subtraction/study_subtraced_sound/" + SubtractionController.Instance.FishType() + (Model.subtracted - Model.subtraction).ToString());

    }


    public string FishType()
    {
        if (Model.IsShalite)
        {
            return "40305015";
        }

        if (Model.Isshuimu)
        {
            return "40305013";
        }
        if (Model.m_IsPufferFish)
        {
            return "40305014";
        }
        if (Model.IsWugui)
        {
            return "40305012";
        }
        return null;
    }


    public void XunzhangMove(Vector3 End_Pos,float xunzhangScaleSpeed)
    {
        this.View.xunzhang.transform.position = Vector3.MoveTowards(this.View.xunzhang.transform.position,End_Pos,180 * Time.deltaTime);
        this.View.xunzhang.transform.localScale -= new Vector3(1, 1, 1) * xunzhangScaleSpeed * Time.deltaTime;
    }


    public List<Renderer> GetMaterial(GameObject Fish)
    {
        List<Renderer> FishComponent = new List<Renderer>();
        foreach (Transform FishComponents in Fish.transform)
        {
            if (FishComponents.gameObject.GetComponent<Renderer>() != null)
            {
                FishComponent.Add(FishComponents.gameObject.GetComponent<Renderer>());
            }
        }
        return FishComponent;
    }


    public void OnClickCarryOut(HUIEvent evt)
    {
        this.View.hand_click1.SetActive(false);
        if (!Model.IsGameEnter)
        {
            return;
        }
        if (this.Model.FishTankRongQi.Count == this.Model.subtraction)
        {
            Debug.LogError("------------------------------------ " + this.Model.FishTankRongQi.Count);
            Model.IsCarryOut_Button = true;
            this.View.SetButtonSclea();
            AudioManager.Instance.StopAllSound();
            AudioManager.Instance.PlaySound(Model.CorrectAudio);
            this.Model.isMoveUp = true;
            this.Model.IsGameEnter = false;
            this.AddTimerEx(Model.CorrectAudio.length, ()=>
            {
                AudioManager.Instance.PlaySound(Model.ChaAudio);
                this.AddTimerEx(Model.ChaAudio.length,()=>
                {
                    
                    Model.IsPlayAudio = true;
                });
            });
            if (this.EnterGameTime == 0 && !PlayerPrefs.HasKey("SubtractionKey"))
            {
                PlayerPrefs.SetInt("SubtractionKey", 10);
                EnterGameTime++;
            }
            
            this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>().enabled = false;
            if (this.Model.completiontimes == 5)
            {
                return;
            }
        }
        else
        {
            List<GameObject> Fish = Model.FishTankRongQi;

            AudioManager.Instance.StopAllSound();
            if (Random.Range(0,1) > 0.5f)
            {
                AudioManager.Instance.PlaySound(Model.ErrorAudio_1);
            }
            else
            {
                AudioManager.Instance.PlaySound(Model.ErrorAudio_2);
            }
            Debug.LogError("========================================= " + this.Model.FishTankRongQi.Count);
            //foreach (GameObject fish in FishTankRongQi)
            //{
            
            //}
            for (int i = 0; i < Fish.Count; i++)
            {
                Debug.LogError("========================================= " + Model.FishTankRongQi[i].name + i);
                Debug.LogError("========================================= " + Fish[i].transform.localPosition + i);
                //Fish[i].transform.localPosition = new Vector3(Random.Range(-330f, 330f), Random.Range(-80f, 30f), Random.Range(-10, -20) * 10);
                Model.RandAIsMove = false;
                Vector3 randompos = new Vector3(Random.Range(-330f, 330f), Random.Range(-80f, 30f),Random.Range(-10, -20) * 10);
                Fish[i].transform.DOLocalMove(randompos,0.01f);
                Fish[i].GetComponent<RotateAndAccelerate>().Animal_Pos = randompos;
                this.AddTimerEx(0.1f,()=>
                {

                    Model.RandAIsMove = true;
                });
                
                
                Debug.LogError("========================================= " + Fish[i].transform.localPosition + i);
            }
            FishTankRongQi.Clear();
            
            
            //Fish.Clear();
        }
    }


    public void GetFish()
    {
        this.Model.Shalite = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_prefab/shaliteyu", typeof(GameObject), enResourceType.Prefab, false).m_content as GameObject;
        this.Model.FishPrefab.Add(this.Model.Shalite);
        this.Model.Shuimu = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_prefab/shuimu", typeof(GameObject), enResourceType.Prefab, false).m_content as GameObject;
        this.Model.FishPrefab.Add(this.Model.Shuimu);
        this.Model.Hetun = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_prefab/hetun", typeof(GameObject), enResourceType.Prefab, false).m_content as GameObject;
        this.Model.FishPrefab.Add(this.Model.Hetun);
        this.Model.Haigui = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_prefab/wugui", typeof(GameObject), enResourceType.Prefab, false).m_content as GameObject;
        this.Model.FishPrefab.Add(this.Model.Haigui);
    }





    IEnumerator AudioPlay()
    {
       
        Debug.LogError("start");
        //AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/403050104", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip);
        
        yield return new WaitForSeconds(0.3f/*(ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/403050104", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip).length*/);
        
        AudioManager.Instance.PlaySound(this.Model.My_AudioSubtraced);

        yield return new WaitForSeconds(this.Model.My_AudioSubtraced.length);

        AudioManager.Instance.PlaySound(this.Model.My_Minus);

        yield return new WaitForSeconds(this.Model.My_Minus.length);

        AudioManager.Instance.PlaySound(this.Model.My_AudioSubtraction);

        yield return new WaitForSeconds(this.Model.My_AudioSubtraction.length);

        AudioManager.Instance.PlaySound(this.Model.My_EqualSign);

        yield return new WaitForSeconds(this.Model.My_EqualSign.length);

        AudioManager.Instance.PlaySound(this.Model.My_Cha);

        yield return new WaitForSeconds(this.Model.My_Cha.length);
        //this.Model.Isgobig = true;
        //Debug.LogError("111111111111111111111111");

        //this.View.xunzhang.SetActive(true);
        //AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("sound/601010110",typeof(AudioClip),enResourceType.Sound,false).m_content as AudioClip);
        //AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource());
        yield return new WaitForSeconds(1.6f);
        this.Model.completiontimes++;
        MedalWindow.Instance.ShowXunzhang(this.Model.completiontimes, true, MedalWindow.EXZCountEnum.Five, null);
        this.Model.IsFormualback = true;

        yield return new WaitForSeconds(1f);


        this.Model.IsAudioplay = false;
        this.Model.Fadeout = true;
        if (this.Model.completiontimes <= 5)
        {
            this.View.m_IsMoveDown = true;
        }

        //RemoveAnimals(this.Model.Animals);

        yield return new WaitForSeconds(1f);

        

       // yield return new WaitForSeconds(0.5f);

       
        if (this.Model.completiontimes == 5)
        {
            this.View.mask.SetActive(true);
            this.Model.LeDiJZMovein = true;
            this.View.Ledi.transform.GetChild(2).gameObject.SetActive(true);
            //this.View.closetubiao();
            AudioManager.Instance.PlaySound(ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/403050104", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip);
            yield return new WaitForSeconds(3.5f);
            this.View.OpenJZButton();

            this.View.Ledi.transform.GetChild(2).gameObject.SetActive(false);
            this.View.Ledi.transform.GetChild(1).gameObject.SetActive(true);

            StopCoroutine("AudioPlay");
            Debug.Log("12312312313123123123123213213123123");
        }
        
        yield return new WaitForSeconds(1f);

        

        if (this.Model.completiontimes < 5)
        {
            Debug.Log("1111111111111111111111111111111111111111111111111111111111");
            RemoveAnimals();
            Reduction(this.Model.Animals, this.Model.Start_Pos);
            //this.View.subtraced.SetActive(false);
            ReductionGame();
            PlayAgain();

            this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>().enabled = false;
            yield return new WaitForSeconds(0.3f);
            AudioManager.Instance.PlaySound(Model.StartAudio_1);
            this.AddTimerEx(Model.StartAudio_1.length,()=>
            {
                AudioManager.Instance.PlaySound(Model.StartAudio_2);
                this.AddTimerEx(Model.StartAudio_2.length,()=>
                {
                    AudioManager.Instance.PlaySound(Model.StartAudio_3);
                    this.AddTimerEx(Model.StartAudio_3.length,()=>
                    {
                        this.Model.IsGameEnter = true;
                        this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>().enabled = true;
                    });
                    
                });
            });
        }

        
    }

    IEnumerator LeDiAudioPlay()
    {
        this.View.Ledi.transform.GetChild(0).gameObject.SetActive(true);
        this.Model.LeDimoveIn = true;
        PlayAgain();
        this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>().enabled = false;
        yield return new WaitForSeconds(0.5f);

        
        AudioManager.Instance.PlaySound(this.Model.StartAudio_1);

        yield return new WaitForSeconds(this.Model.StartAudio_1.length);

        AudioManager.Instance.PlaySound(this.Model.StartAudio_2);

        yield return new WaitForSeconds(this.Model.StartAudio_2.length);

        AudioManager.Instance.PlaySound(this.Model.StartAudio_3);

        yield return new WaitForSeconds(this.Model.StartAudio_3.length);

        this.Model.LeDiMoveout = true;
        this.Model.IsGameEnter = true;
        this.View.ui_camera.gameObject.GetComponent<InputPhysics2DRaycaster>().enabled = true;

        yield return new WaitForSeconds(1.5f);

        IsGuiDe = true;
        this.View.Ledi.transform.GetChild(0).gameObject.SetActive(false);
       

    }

}

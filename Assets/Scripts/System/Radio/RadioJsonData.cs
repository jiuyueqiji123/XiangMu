﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RadioJsonData {
	public int AudioID;
	public string audioBgUrl;
	public string audioMainTitle;
	public string audioSubTitle;
	public string audioUrl;
}

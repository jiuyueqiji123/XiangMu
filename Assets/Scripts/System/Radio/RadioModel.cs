﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJson;

public class RadioModel {

	private string radioUrl = "http://test.superwings.soulgame.mobi/public/radio/";

	private List<RadioJsonData> datas = new List<RadioJsonData>();

	private Dictionary<int, RadioJsonData> dataDicts = new Dictionary<int, RadioJsonData> ();

	private int currentIndex;

	private int currentPlayProgress;

	private int currentLoadingProgress;

	private int currentAudioLength;

	public int CurrentIndex {
		get {
			return currentIndex;
		}
		private set {
			this.currentIndex = value;
			this.currentLoadingProgress = 0;
			this.currentPlayProgress = 0;
			this.currentAudioLength = 0;
		}
	}

	public int CurrentPlayProgress {
		get {
			return currentPlayProgress;
		}
	}

	public int CurrentLoadingProgress {
		get {
			return currentLoadingProgress;
		}
	}

	public int CurrentAudioLength {
		get {
			return currentAudioLength;
		}
	}

	public string CurName {
		get {
			return dataDicts [currentIndex].audioMainTitle;
		}
	}

	public float CurLoadingValue {
		get {
			return currentLoadingProgress / 100f;
		}
	}

	public float CurPlayValue {
		get {
			return (float)currentPlayProgress / (float)currentAudioLength;
		}
	}

	public RadioModel() {
		RadioJsonData data1 = new RadioJsonData () {
			AudioID = 0,
			audioBgUrl = "",
			audioMainTitle = "加州旅馆",
			audioSubTitle = "老鹰乐队",
			audioUrl = radioUrl + "Hotel California.mp3",
		};
		datas.Add (data1);
		dataDicts.Add (0, data1);
		RadioJsonData data2 = new RadioJsonData () {
			AudioID = 1,
			audioBgUrl = "",
			audioMainTitle = "Yellow",
			audioSubTitle = "Yellow",
			audioUrl = radioUrl + "Yellow.mp3",
		};
		datas.Add (data2);
		dataDicts.Add (1, data2);
		RadioJsonData data3 = new RadioJsonData () {
			AudioID = 2,
			audioBgUrl = "",
			audioMainTitle = "Lost Stars",
			audioSubTitle = "Lost",
			audioUrl = radioUrl + "Lost Stars.mp3",
		};
		datas.Add (data3);
		dataDicts.Add (2, data3);
		this.CurrentIndex = 0;
	}

	public string GetJsonData() {
		return SimpleJson.SimpleJson.SerializeObject (this.datas);
	}

	public void Next() {
		this.CurrentIndex++;
		if (this.CurrentIndex == this.datas.Count) {
			this.CurrentIndex = 0;
		}
	}

	public void Pre() {
		this.CurrentIndex--;
		if (this.CurrentIndex > 0) {
			this.CurrentIndex = this.datas.Count - 1;
		}
	}

	public void ReflashProgress(string json) {
		RadioJsonProgress pro = SimpleJson.SimpleJson.DeserializeObject<RadioJsonProgress> (json);
		if (pro != null) {
			this.currentPlayProgress = pro.curAudioPlayProgress;
			this.currentLoadingProgress = pro.curAudioLoadingPercent;
		}
	}

	public void ReflashInfo(string json) {
		RadioJsonInfo info = SimpleJson.SimpleJson.DeserializeObject<RadioJsonInfo> (json);
		if (info != null) {
			CurrentIndex = info.audioID;
			currentAudioLength = info.audioTotalTime;
		}
	}

}

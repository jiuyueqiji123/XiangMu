﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RadioJsonProgress {

	public int curAudioLoadingPercent;

	public int curAudioPlayProgress;
}

﻿using System.Collections;using System.Collections.Generic;using System.Text.RegularExpressions;using UnityEngine;using UnityEngine.UI;using Radio;[RequireComponent(typeof(Image))][RequireComponent(typeof(Mask))][RequireComponent(typeof(ScrollRect))]public class RadioTextScroll : MonoBehaviour{    protected ScrollRect scroll;    protected RectTransform text_rect;    protected Text text;    protected string cache;    public float speed = 0.2f;    private void Awake()
    {
        scroll = GetComponent<ScrollRect>();
        text_rect = transform.GetChild(0) as RectTransform;
        text = text_rect.GetComponent<Text>();
    }    public void Inject(string injectStr)
    {
        //injectStr += "。。。。。。。。。";
        int count1 = Regex.Matches(injectStr, RadioClassHelper.Get(RegexEnum.number)).Count;//数字
        int count2 = Regex.Matches(injectStr, RadioClassHelper.Get(RegexEnum.character)).Count;//字母
        int count3 = Regex.Matches(injectStr, RadioClassHelper.Get(RegexEnum.chinese)).Count;//汉字
        int count4 = Regex.Matches(injectStr, RadioClassHelper.Get(RegexEnum.englishInterpunction)).Count;//英文标点符号
        int count5 = Regex.Matches(injectStr, RadioClassHelper.Get(RegexEnum.chineseInterpunction)).Count;//中文标点符号
        //Debug.Log("字符串长度:" + injectStr.Length + " 数字:" + count1 + " 字母:" + count2 + " 汉字:" + count3 + " 英文标点:" + count4 +" 中文标点:"+count5);
        cache = injectStr ;
        text.text = cache;

        text_rect.anchorMin = new Vector2(0, 0.5f);
        text_rect.anchorMax = new Vector2(0, 0.5f);
        text_rect.sizeDelta = new Vector2((count3 * 2 + count5 * 2 + count1 + count2 + count4 * 0.5f + 2) * 0.5f * text.fontSize, text_rect.rect.height);
        text_rect.anchoredPosition = new Vector2(text_rect.anchoredPosition.x, 0);

        scroll.horizontalNormalizedPosition = 0;
        ismove = canMove();
    }    private void OnEnable()
    {
        ismove = canMove();
    }    private void OnDisable()
    {
        ismove = false;
        text.text = "";
        cache = null;
    }    protected bool canMove()
    {
        if (string.IsNullOrEmpty(cache))
            return false;

        if (text_rect.rect.width <= (this.transform as RectTransform).rect.width)
            return false;

        return true;
    }    bool ismove = false;    private void Update()
    {
        if (ismove)
        {
            if (scroll.horizontalNormalizedPosition >= 1.0f)
                scroll.horizontalNormalizedPosition = 0f;

            scroll.horizontalNormalizedPosition = scroll.horizontalNormalizedPosition + speed * Time.deltaTime;
        }
    }}
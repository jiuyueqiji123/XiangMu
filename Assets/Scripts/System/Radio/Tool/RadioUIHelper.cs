﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadioUIHelper
{
    public static void SetImg(Image component, string localPath, Rect rect, Texture2D defaultTex)
    {
        //Debug.Log(localPath);
        if (!SetImg(component, localPath, rect))
        {
            SetImg(component, defaultTex, rect);
        }
    }

    public static bool SetImg(Image component, string localPath, Rect rect)
    {
        string md5 = FileTools.GetMD5FromString(localPath);
        if (RadioDataPool.Instance.textures.ContainsKey(md5))
        {
            //赋值
            SetImg(component, RadioDataPool.Instance.textures[md5], rect);
            return true;
        }
        else
        {
            if (FileTools.IsFileExist(localPath))
            {
                byte[] texBytes = FileTools.ReadFile(localPath);
                if (texBytes != null)
                {
                    Texture2D texture = new Texture2D(Mathf.CeilToInt(rect.width), Mathf.CeilToInt(rect.height));
                    texture.LoadImage(texBytes);
                    texture.Apply();
                    //Debug.Log(localPath);
                    try
                    {
                        SetImg(component, texture, rect);
                    }
                    catch (Exception e)
                    {

                    }
                    RadioDataPool.Instance.textures[md5] = texture;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }

    public static void SetImg(Image component, Texture2D texture, Rect rect)
    {
        component.sprite = Sprite.Create(texture, new Rect(0, 0, rect.width, rect.height), Vector2.one * 0.5f,100,1,SpriteMeshType.FullRect);
    }

    public static Texture2D GaussianBlur(Texture2D texture)
    {
        Color old;
        int h = texture.height;
        int w = texture.width;
        float[,,] inputPicture = new float[3, w, h];
        for (int i = 0; i < w; i++)
            for (int j = 0; j < h; j++)
            {
                old = texture.GetPixel(i, j);
                inputPicture[0, i, j] = old.r;
                inputPicture[1, i, j] = old.g;
                inputPicture[2, i, j] = old.b;
            }

        int[,,] outputPicture = new int[3, w, h];
        Texture2D newtex = new Texture2D(w, h);

        for (int i = 0; i < w; i++)
            for (int j = 0; j < h; j++)
            {
                float r = 0f;
                float g = 0f;
                float b = 0f;

                for (int m = 0; m < 5; m++)
                    for (int n = 0; n < 5; n++)
                    {
                        int row = i - 2 + m;
                        int col = j - 2 + n;

                        //越界处理
                        row = row < 0 ? 0 : row;
                        col = col < 0 ? 0 : col;
                        row = row >= w ? w - 1 : row;
                        col = col >= h ? h - 1 : col;

                        r += gaussionRect[m, n] * inputPicture[0, row, col];
                        g += gaussionRect[m, n] * inputPicture[1, row, col];
                        b += gaussionRect[m, n] * inputPicture[2, row, col];
                    }

                newtex.SetPixel(i, j, new Color(r, g, b));
            }

        newtex.Apply();
        
        return newtex;
    }

    protected static float gaussionDelta = 300f;
    protected static float[,] gaussionRect = new float[5, 5]
    {
        { 1/gaussionDelta,  4/gaussionDelta,    7/gaussionDelta,    4/gaussionDelta,  1/gaussionDelta},
        { 4/gaussionDelta,  16/gaussionDelta,   26/gaussionDelta,   16/gaussionDelta, 4/gaussionDelta},
        { 7/gaussionDelta,  26/gaussionDelta,   41/gaussionDelta,   26/gaussionDelta, 7/gaussionDelta},
        { 4/gaussionDelta,  16/gaussionDelta,   26/gaussionDelta,   16/gaussionDelta, 4/gaussionDelta},
        { 1/gaussionDelta,  4/gaussionDelta,    7/gaussionDelta,    4/gaussionDelta,  1/gaussionDelta}
    };
}
﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEngine.UI;[RequireComponent(typeof(Image))]public class UIFrameAnimation : MonoBehaviour
{    public FrameAnimationType frameAnimationType;    [SerializeField]    protected Sprite[] sprites;        public Texture2D texture;        public string texturePath;        [Tooltip("每张图片间隔")]    [Range(0.01f,10f)]    public float interval = 0.01f;        public bool loop = true;    private Image _img;    private float timer = 0f;    private int index = 0;    private bool caniplay = true;    private Sprite[] array;    private void Awake()
    {
        _img = GetComponent<Image>();
        _loadSprites();
        
    }        void _loadSprites()
    {
        if (texture != null)
        {
            if (array == null)
            {
                array = Resources.LoadAll<Sprite>(texturePath);
            }
        }
    }    Sprite[] avator;    private void FixedUpdate()
    {
        if (frameAnimationType == FrameAnimationType.MultipleSprite)
        {
            if (array == null) _loadSprites();
            if (!caniplay || _img == null) return;
            avator = array;
        }
        else
        {
            if (sprites == null || !caniplay || _img==null) return;
            avator = sprites;
        }

        doit();
    }    void doit()
    {
        if (avator == null) return;
        timer += Time.fixedDeltaTime;
        if (timer >= interval)
        {
            timer = 0;
            index %= avator.Length;
            _img.sprite = avator[index];
            if (index == avator.Length - 1)
                caniplay = loop;
            index++;
        }
    }}public enum FrameAnimationType
{
    MultipleSprite,
    SpriteList,
}
﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using System.Text.RegularExpressions;namespace Radio
{
    public static class RadioClassHelper
    {
        public const int one_minite = 60;
        public const int one_hour = 3600;
        public const int one_day = 86400;

        public static List<RadioDownLoadItem> Deserialize(this string str)
        {
            List<RadioDownLoadItem> list = new List<RadioDownLoadItem>();
            string[] array = str.Split('|');
            for (int i=0;i<array.Length;i++)
            {
                RadioDownLoadItem tmp = new RadioDownLoadItem();
                string[] tmpStrArray = array[i].Split(',');
                tmp.itemId      = tmpStrArray[0];
                tmp.coverUrl    = tmpStrArray[1];
                list.Add(tmp);
            }
            return list;
        }

        public static string Clock(int seconds)
        {
            int s = 0;
            int m = 0;
            int h = 0;
            int d = 0;
            if(seconds>one_day) d = seconds / one_day;
            if(seconds>one_hour) h = (seconds - d * one_day) / one_hour;
            if(seconds>one_minite) m = (seconds - d * one_day - h * one_hour) / one_minite;
            s = seconds - d * one_day - h * one_hour - m * one_minite;
            string str = "";
            if (d > 0)
                str += string.Format("{0:D2}:", d);
            if (h > 0)
                str += string.Format("{0:D2}:", h);
            str += string.Format("{0:D2}:{1:D2}", m, s);
            return str;
        }

        public static string Get(RegexEnum regexEnum)
        {
            switch(regexEnum)
            {
                case RegexEnum.number:
                    return @"\d";
                case RegexEnum.character:
                    return @"(?i)[a-z]";
                case RegexEnum.chinese:
                    return @"[\u4e00-\u9fa5]";
                case RegexEnum.englishInterpunction:
                    return @"[~!@#\$%\^&\*\(\)\+=\|\\\}\]\{\[:;<,>\?\/.""' ]";
                case RegexEnum.chineseInterpunction:
                    return @"[：；,\？“”‘’。 ]";
            }
            return @"";
        }
    }

    public enum RegexEnum
    {
        number,
        character,
        chinese,
        englishInterpunction,
        chineseInterpunction,
    }
}
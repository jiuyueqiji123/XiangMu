﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UGUITest : MonoBehaviour
{

    [ContextMenu("显示")]
    void Test1()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        Debug.Log("anchoredPos:" + rectTransform.anchoredPosition);
        Debug.Log("rect:" + rectTransform.rect);
        Debug.Log("sizeDelta:" + rectTransform.sizeDelta);
        Debug.Log("offsetMax:" + rectTransform.offsetMax);
        Debug.Log("offsetMin:" + rectTransform.offsetMin);
        Debug.Log("persistDataPath:" + Application.persistentDataPath);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class ScrollRectPatch : MonoBehaviour
{
    protected ScrollRect scrollRect;
    protected RectTransform viewRect;
    protected RectTransform contentRect;

    protected List<RectTransform> childList = new List<RectTransform>();
    private void Awake()
    {
        scrollRect = GetComponent<ScrollRect>();
        viewRect = scrollRect.viewport;
        contentRect = scrollRect.content;
    }

    public static float horizontal = 0f;
    private void LateUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            horizontal = Input.GetAxis("Mouse X");
            return;
        }
        if (scrollRect.velocity != Vector2.zero)
        {
            HUIEventManager.Instance.DispatchUIEvent(enUIEventID.Radio_Scroll_Move, new stUIEventParams { argFloat = contentRect.anchoredPosition.x });
        }

    }
}

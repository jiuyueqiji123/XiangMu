﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Toggle))]
public class TogglePatch : MonoBehaviour
{
    protected Toggle toggle;

    private void Awake()
    {
        toggle = GetComponent<Toggle>();
        Set();
    }

    public List<UIObjShowState> uilist;

    public void Set()
    {
        if (uilist == null) return;
        for (int i=0;i<uilist.Count;i++)
        {
            if (uilist[i] != null && uilist[i].content != null)
            {
                uilist[i].content.SetActive(!(uilist[i].isActive^toggle.isOn));
            }
        }
    }
}

[System.Serializable]
public class UIObjShowState
{
    public GameObject content;
    public bool isActive;
}
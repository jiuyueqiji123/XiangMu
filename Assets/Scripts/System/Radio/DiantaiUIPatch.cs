﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiantaiUIPatch : MonoSingleton<DiantaiUIPatch>
{
    public void LoadMainUI(DiantaiUIView diantaiUIView,int index)
    {
        StopAllCoroutines();
        StartCoroutine(LoadContentUI(diantaiUIView,index));
    }

    IEnumerator LoadContentUI(DiantaiUIView diantaiUIView, int index)
    {
        yield return null;
        diantaiUIView.contentInstanceIDList = new List<int>();
        diantaiUIView.needSetImgcontentItem.Clear();
        if (diantaiUIView.contentGameobjectList == null) diantaiUIView.contentGameobjectList = new List<GameObject>();
        diantaiUIView.contentList = RadioDataPool.Instance.GetViewData(diantaiUIView.tabList[index].tag);
        int len1 = diantaiUIView.contentGameobjectList.Count;
        int len2 = diantaiUIView.contentList.Count;
        int sum = Mathf.Max(len1, len2);

        diantaiUIView.AssembleUILayoutByData(len2);
        for (int i = 0; i < sum; i++)
        {
            if (i % 4 == 0)
                yield return null;
            if (i >= len2)
            {
                if (i >= len1)
                {
                    //
                }
                else
                {
                    diantaiUIView.contentGameobjectList[i].SetActive(false);
                }
            }
            else
            {
                if (i >= len1)
                {
                    GameObject tmp = GameObject.Instantiate(diantaiUIView.template_content_item0);
                    tmp.SetActive(true);
                    tmp.transform.SetParent(diantaiUIView.template_content_item0.transform.parent, false);

                    diantaiUIView.contentGameobjectList.Add(tmp);

                    diantaiUIView.UpdateContentItem(tmp, diantaiUIView.contentList[i]);
                    diantaiUIView.contentInstanceIDList.Add(tmp.GetInstanceID());
                }
                else
                {
                    diantaiUIView.contentGameobjectList[i].SetActive(true);
                    diantaiUIView.UpdateContentItem(diantaiUIView.contentGameobjectList[i], diantaiUIView.contentList[i]);
                    diantaiUIView.contentInstanceIDList.Add(diantaiUIView.contentGameobjectList[i].GetInstanceID());
                }
            }
        }


        diantaiUIView.template_content_item0.transform.parent.GetComponent<GridLayoutGroup>().SetLayoutHorizontal();

        diantaiUIView.InitSplitPage();
    }
}

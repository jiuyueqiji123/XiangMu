﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RadioJsonInfo {
	public int audioID;
	public int audioTotalTime; 
}

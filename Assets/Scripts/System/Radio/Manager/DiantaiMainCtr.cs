﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiantaiMainCtr:Singleton<DiantaiMainCtr>
{
    #region 变量
    /// <summary>
    /// 电台功能模组组装状态
    /// </summary>
    private RadioModuleEnum moduleAssembleState;

    /// <summary>
    /// 电台功能UI
    /// </summary>
    private DiantaiUIView view;
    
    #endregion

    #region 初始化
    public override void Init()
    {
        base.Init();

        if (view == null) view = new DiantaiUIView();
        Register();
        ReportModuleState(RadioModuleEnum.UIViewComponent);
    }

    public void ReportModuleState(RadioModuleEnum state)
    {
        moduleAssembleState |= state;

        if (state == RadioModuleEnum.UIBasicComponent)
            ShowUI();
        
        if ((moduleAssembleState & RadioModuleEnum.DefaultConfig) == RadioModuleEnum.DefaultConfig)
        {
            if (RadioDataPool.Instance.localConfig.needSDK && RadioDataPool.Instance.localConfig.needServer)
            {
                
                if (moduleAssembleState ==
                    (RadioModuleEnum.AssembleData | RadioModuleEnum.ConnectServer | RadioModuleEnum.DefaultConfig
                    | RadioModuleEnum.SDK_2_FM | RadioModuleEnum.UIBasicComponent | RadioModuleEnum.UIViewComponent))
                {
                    UpdateData();
                }
            }

            if ((!RadioDataPool.Instance.localConfig.needSDK) && (!RadioDataPool.Instance.localConfig.needServer))
            {
                if (moduleAssembleState ==
                    (RadioModuleEnum.AssembleData | RadioModuleEnum.DefaultConfig
                     | RadioModuleEnum.UIBasicComponent | RadioModuleEnum.UIViewComponent))
                {
                    UpdateData();
                }
            }
        }
        Debug.Log(moduleAssembleState);
    }

    public void UpdateData()
    {
        view.UpdateData();
    }

    protected void ShowUI()
    {
        Debug.Log("UIShow");
        view.OpenForm();
        //UpdateData();
    }

    public void RefreshUI()
    {
        view.RefreshUI();
    }

    protected void Register()
    {
        Input.multiTouchEnabled = false;
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Radio_Btn_Exit, Exit);
    }

    protected void RemoveLisenter()
    {
        //Input.multiTouchEnabled = true;
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Radio_Btn_Exit, Exit);
    }

    protected void ClearData()
    {
        moduleAssembleState = 0;
        view = null;
        RadioDataPool.Instance.Release();
        //RadioSDKManager.Instance.Destroy();
        RadioSDKManager.DestroyInstance();
    }
    #endregion

    #region 外部调用

    public void RadioPopFailed()
    {
        view.OnPopFailed();
    }

    #endregion

    #region 交互
    public void Exit(HUIEvent hUIEvent)
    {
        //RadioSDKManager.Instance.Pause();
        view.Clear();
        Input.multiTouchEnabled = true;
        ClearData();
        TimerManager.Instance.RemoveAllTimer();
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }
    #endregion
}

public enum RadioModuleEnum
{
    UIBasicComponent=1,
    DefaultConfig=2,
    SDK_2_FM=4,
    ConnectServer=8,
    AssembleData=16,
    UIViewComponent=32,
}
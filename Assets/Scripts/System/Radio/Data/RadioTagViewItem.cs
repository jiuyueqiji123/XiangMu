﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 电台分类数据描述
/// </summary>
[System.Serializable]
public class RadioTagViewItem: IEquatable<RadioTagViewItem>, IComparable<RadioTagViewItem>
{
    /// <summary>
    /// 数据分类
    /// </summary>
    public RadioTagViewState state;

    public string nameStr;

    /// <summary>
    /// 该分类是否显示
    /// </summary>
    public bool isShow;

    /// <summary>
    /// 加载时是否聚焦在此分类[默认选中]
    /// </summary>
    public bool hoverThisWhenLoad;

    /// <summary>
    /// 当前分类索引
    /// </summary>
    public int tag;

    /// <summary>
    /// 剧集数量
    /// </summary>
    public int diversityNums;

    /// <summary>
    /// 该分类标题
    /// </summary>
    public string tagDesStr;

    /// <summary>
    /// 封面图片
    /// </summary>
    public string imgUrl;

    /// <summary>
    /// 默认封面图片
    /// </summary>
    public Texture2D defaultTex;

    /// <summary>
    /// 排序
    /// </summary>
    public int order;

    public string localImgPath
    {
        get
        {
            if (imgUrl == string.Empty || imgUrl == "") return "";            string fileName = tag.ToString();            string fileType = imgUrl.Split('.')[imgUrl.Split('.').Length - 1];            return RadioDataPool.fileSavePath + fileName + "." + fileType;
        }
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(state).Append("|");
        sb.Append(isShow).Append("|");
        sb.Append(hoverThisWhenLoad).Append("|");
        sb.Append(tag).Append("|");
        sb.Append(diversityNums).Append("|");
        sb.Append(tagDesStr).Append("|");
        sb.Append(imgUrl).Append("|");
        sb.Append(order);
        return sb.ToString();
    }

    public RadioTagViewItem Clone()
    {
        RadioTagViewItem item = new RadioTagViewItem();
        item.defaultTex = this.defaultTex;
        item.diversityNums = this.diversityNums;
        item.hoverThisWhenLoad = this.hoverThisWhenLoad;
        item.imgUrl = this.imgUrl;
        item.isShow = this.isShow;
        item.state = this.state;
        item.tag = this.tag;
        item.tagDesStr = this.tagDesStr;
        item.order = this.order;
        item.nameStr = this.nameStr;
        return item;
    }
    

    public bool Equals(RadioTagViewItem other)
    {
        if (other == null) return false;
        if (this.defaultTex != other.defaultTex) return false;
        if (this.diversityNums != other.diversityNums) return false;
        if (this.hoverThisWhenLoad != other.hoverThisWhenLoad) return false;
        if (this.imgUrl != other.imgUrl) return false;
        if (this.isShow != other.isShow) return false;
        if (this.state != other.state) return false;
        if (this.tag != other.tag) return false;
        if (this.tagDesStr != other.tagDesStr) return false;
        if (this.order != other.order) return false;
        return true;
    }

    public int CompareTo(RadioTagViewItem other)
    {
        return this.order.CompareTo(other.order);
    }
}


/// <summary>
/// 电台分类数据状态
/// </summary>
public enum RadioTagViewState
{
    rainBallFM,     //彩虹儿童fm
    recommend,      //编辑推荐
    normal,         //其他
}
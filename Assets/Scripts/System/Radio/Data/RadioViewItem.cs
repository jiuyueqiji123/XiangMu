﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>
/// 每一个分集的数据描述
/// </summary>
[System.Serializable]
public class RadioViewItem : IEquatable<RadioViewItem>, IComparable<RadioViewItem>
{
    /// <summary>
    /// 归类
    /// </summary>
    public int leadershipTag
    {
        get;
        set;
    }
    /// <summary>
    /// 所属归类下第几集
    /// </summary>
    public int episodeTag
    {
        get;
        set;
    }

    public string nameStr;

    /// <summary>
    /// 是否分级
    /// </summary>
    public bool isDiversity;

    /// <summary>
    /// 有分集的话，分集数量
    /// </summary>
    public int diversityNum;

    /// <summary>
    /// 分集索引:上中下...
    /// </summary>
    public int diversityTag;

    /// <summary>
    /// 本集描述
    /// </summary>
    public string desStr;

    /// <summary>
    /// 封面图路径[待议]
    /// </summary>
    public string imgUrl;

    /// <summary>
    /// 音频文件路径[待议]
    /// </summary>
    public string audioUrl;

    /// <summary>
    /// 当前剧情 显示 状态.
    /// </summary>
    public int itemState
    {
        get;
        set;
    }

    /// <summary>
    /// 排序
    /// </summary>
    public int order
    {
        get;
        set;
    }

    protected string m_imglocal;
    public string imgLocalPath
    {
        get
        {
            if (string.IsNullOrEmpty(m_imglocal) || m_imglocal == "")
            {
                if (imgUrl == string.Empty || imgUrl == "") return "";
                string fileName = leadershipTag + "_" + episodeTag;
                string fileType = imgUrl.Split('.')[imgUrl.Split('.').Length - 1];
                return RadioDataPool.fileSavePath + fileName + "." + fileType;
            }
            else
                return m_imglocal;
        }
        set
        {
            m_imglocal = value;
        }
    }

    protected string m_audioLocal;
    public string audioLocalPath
    {
        get
        {
            if (string.IsNullOrEmpty(m_audioLocal) || m_audioLocal == "")
            {
                if (audioUrl == string.Empty || audioUrl == "") return "";
                string fileName = leadershipTag + "_" + episodeTag;
                string fileType = audioUrl.Split('.')[audioUrl.Split('.').Length - 1];
                return RadioDataPool.fileSavePath + fileName + "." + fileType;
            }
            else
                return m_audioLocal;
        }
        set
        {
            m_audioLocal = value;
        }
    }

    public int CompareTo(RadioViewItem other)
    {
        return this.order.CompareTo(other.order);
    }

    public bool Equals(RadioViewItem other)
    {
        if(this.audioLocalPath!=other.audioLocalPath)return false;
        if (this.audioUrl != other.audioUrl) return false;
        if (this.desStr != other.desStr) return false;
        if (this.diversityNum != other.diversityNum) return false;
        if (this.diversityTag != other.diversityTag) return false;
        if (this.episodeTag != other.episodeTag) return false;
        if (this.imgUrl != other.imgUrl) return false;
        if (this.isDiversity != other.isDiversity) return false;
        if (this.itemState != other.itemState) return false;
        if (this.leadershipTag != other.leadershipTag) return false;
        if (this.order != other.order) return false;
        return true;
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("剧本:").Append(leadershipTag).Append(" ");
        sb.Append("第").Append(episodeTag).Append("集 ");
        sb.Append("order:").Append(order);
        return sb.ToString();
    }
}

public enum RadioItemState
{
    needMoney = 0,      //需要购买
    free = 1,               //免费
    limitfree = 2,          //限免
}

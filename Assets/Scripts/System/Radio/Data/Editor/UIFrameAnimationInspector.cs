﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEditor;[CustomEditor(typeof(UIFrameAnimation))]public class UIFrameAnimationInspector : Editor{    protected SerializedObject thisObj;    protected SerializedProperty frameAnimationType;    protected SerializedProperty interval;    protected SerializedProperty loop;    protected SerializedProperty spriteList;

    protected SerializedProperty texture;    protected SerializedProperty texturePath;        protected UIFrameAnimation script;    private void OnEnable()    {        thisObj = new SerializedObject(this.target);

        interval = thisObj.FindProperty("interval");        loop = thisObj.FindProperty("loop");        spriteList = thisObj.FindProperty("sprites");        texture = thisObj.FindProperty("texture");        texturePath = thisObj.FindProperty("texturePath");    }    public override void OnInspectorGUI()    {        script = (UIFrameAnimation)this.target;

        EditorGUI.BeginChangeCheck();        script.frameAnimationType = (FrameAnimationType)EditorGUILayout.EnumPopup("DataType", script.frameAnimationType);        if (script.frameAnimationType == FrameAnimationType.SpriteList)
        {
            EditorGUILayout.PropertyField(spriteList, true);
        }        else
        {
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(texture);
            if (script.texture != null)
            {
                string tmp = AssetDatabase.GetAssetPath(script.texture);
                tmp = tmp.Replace("Assets/Resources/", "");
                tmp = tmp.Split('.')[0];
                script.texturePath = tmp;
            }
            else
                script.texturePath = "";

            GUI.enabled = false;
            EditorGUILayout.LabelField("texture_path", script.texturePath);
            GUI.enabled = true;
        }        EditorGUILayout.PropertyField(interval);        EditorGUILayout.PropertyField(loop);        if (EditorGUI.EndChangeCheck())
        {
            thisObj.ApplyModifiedProperties();
        }    }}
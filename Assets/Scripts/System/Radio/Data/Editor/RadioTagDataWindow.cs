﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEditor;[ExecuteInEditMode]public class RadioTagDataWindow : EditorWindow{    [MenuItem("Tools/Radio/PageEdit")]    static void Excute()    {        RadioTagDataWindow thisWindow =  (RadioTagDataWindow)EditorWindow.GetWindow(typeof(RadioTagDataWindow),  false, "电台",false);        thisWindow.InitData();        thisWindow.autoRepaintOnSceneChange = true;        thisWindow.Show();        thisWindow.Focus();    }    RadioTagDataWindow()    {        this.titleContent = GUIContent.none;    }    public RadioTagDataConfig data;    public const string dataPath = "Assets/Resources/Radio/radio_data/";    public const string dataFileName = "RadioTagDataConfig.asset";    public void InitData()    {        data = AssetDatabase.LoadAssetAtPath<RadioTagDataConfig>(dataPath + dataFileName);
        foldOutSwitch = new List<bool>();        if (data != null&&data.viewItemList!=null)
        {
            for (int i = 0; i < data.viewItemList.Count; i++)
            {
                foldOutSwitch.Add(false);
            }
        }    }    protected Vector2 beginScroll;    protected List<bool> foldOutSwitch;    private void OnGUI()    {        GUILayout.Space(10);        GUI.skin.label.fontSize = 20;        GUI.skin.label.alignment = TextAnchor.MiddleCenter;        GUILayout.Label("电台分类信息默认配置");        GUILayout.Space(10);        GUI.skin.label.fontSize = 14;        GUI.skin.label.alignment = TextAnchor.MiddleRight;                if (data == null)            data = ScriptableObject.CreateInstance<RadioTagDataConfig>();        beginScroll = GUILayout.BeginScrollView(beginScroll);        data.isShieldFMPushFunc = EditorGUILayout.Toggle("是否屏蔽彩虹儿童FM推送:", data.isShieldFMPushFunc);        data.needSDK = EditorGUILayout.Toggle("需要SDK:", data.needSDK);        data.needServer = EditorGUILayout.Toggle("需要Server:", data.needServer);        if (GUILayout.Button("添加一个电台数据展示分类",GUILayout.MaxWidth(300)))        {            if (data.viewItemList == null)
            {
                data.viewItemList = new List<RadioTagViewItem>();
            }            data.viewItemList.Add(new RadioTagViewItem());            foldOutSwitch.Add(false);        }                if (data.viewItemList != null && data.viewItemList.Count > 0)        {            for (int i = 0; i < data.viewItemList.Count; i++)            {                RadioTagViewItem item = data.viewItemList[i];                foldOutSwitch[i] = EditorGUILayout.Foldout(foldOutSwitch[i], item.nameStr);                if (foldOutSwitch[i])
                {
                    GUILayout.BeginVertical();
                    GUI.skin.label.alignment = TextAnchor.MiddleRight;

                    GUILayout.BeginHorizontal();
                    item.nameStr = EditorGUILayout.TextField("分类标题:", item.nameStr, GUILayout.MaxWidth(300));
                    if (GUILayout.Button("移除此分类", GUILayout.MaxWidth(150)))
                    {
                        foldOutSwitch.RemoveAt(i);
                        data.viewItemList.RemoveAt(i--);
                        continue;
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    item.hoverThisWhenLoad = EditorGUILayout.Toggle("默认选中此分类？:", item.hoverThisWhenLoad);
                    item.isShow = EditorGUILayout.Toggle("是否显示:", item.isShow);
                    item.state = (RadioTagViewState)EditorGUILayout.EnumPopup("类型:", item.state, GUILayout.ExpandWidth(true));
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    item.order = EditorGUILayout.IntField("排序索引:", item.order);
                    item.diversityNums = EditorGUILayout.IntField("剧集数量:", item.diversityNums);
                    item.imgUrl = EditorGUILayout.TextField("图片路径:", item.imgUrl);
                    GUILayout.EndHorizontal();
                    item.defaultTex = (Texture2D)EditorGUILayout.ObjectField("选默认图", item.defaultTex, typeof(Texture2D), true);
                    GUILayout.EndVertical();
                }            }        }        GUILayout.EndScrollView();        GUILayout.FlexibleSpace();        if (GUILayout.Button("Save"))        {            Save();        }    }    void Save()    {        AssetDatabase.SaveAssets();        AssetDatabase.Refresh();        EditorUtility.SetDirty(data);        ShowNotification(new GUIContent("已保存o..O"));        //Close();    }}
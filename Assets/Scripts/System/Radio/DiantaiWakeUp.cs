﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cmd;

[RequireComponent(typeof(Camera))]
public class DiantaiWakeUp : MonoBehaviour
{
    //分部
    //1.默认配置
    //2.SDK确认是否推送FM
    //3.链接服务器获取具体信息
    //4.组装数据
    //5.展示UI
    private void Awake()
    {
        Debug.LogWarning("<color=green>进入电台场景Oo.</color>");
        DiantaiMainCtr.Instance.Init();

        //0.
        TryCheckComponent();
        //1.
        TryGetData();
        /*
#if UNITY_EDITOR
        if (RadioDataPool.Instance.localConfig.needSDK && RadioDataPool.Instance.localConfig.needServer)
        {
            //2.
            TryConnectSDK();

            //3.
            HUIEventManager.Instance.DispatchUIEvent(enUIEventID.Radio_Net_Loading, new stUIEventParams { argInt = 0 });
            RadioDataPool.Instance.TryGetServerData();
        }
        else
        {
            //4.
            AssembleData();
        }
#else
            //2.
            TryConnectSDK();
           
            //3.
            HUIEventManager.Instance.DispatchUIEvent(enUIEventID.Radio_Net_Loading,new stUIEventParams { argInt=0});
            RadioDataPool.Instance.TryGetServerData();
#endif
*/

    }

    void TryCheckComponent()
    {
        //if (GetComponent<AudioListener>() == null) this.gameObject.AddComponent<AudioListener>();
        if (GetComponent<InputPhysicsRaycaster>() == null) this.gameObject.AddComponent<InputPhysicsRaycaster>();
        DiantaiMainCtr.Instance.ReportModuleState(RadioModuleEnum.UIBasicComponent);
    }

    void TryGetData()
    {
        RadioDataPool.Instance.LocalConfigLoad();
        DiantaiMainCtr.Instance.ReportModuleState(RadioModuleEnum.DefaultConfig);
    }

    void TryConnectSDK()
    {
        RadioDataPool.Instance.ConnectSDK();
        DiantaiMainCtr.Instance.ReportModuleState(RadioModuleEnum.SDK_2_FM);
    }
    
    void AssembleData()
    {
        RadioDataPool.Instance.AssembleData();
        DiantaiMainCtr.Instance.ReportModuleState(RadioModuleEnum.AssembleData);
    }
    

    private void OnDestroy()
    {
        Debug.LogWarning("<color=green>退出电台场景.oO</color>");
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadioLoad : Singleton<RadioLoad> {


    public GameObject panel;
    private RectTransform rect;

	// Use this for initialization
	void Start () {
        rect = panel.GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x,0f); 
	}


    public override void Init()
    {
        base.Init();

        HUIEventManager.Instance.AddUIEventListener(enUIEventID.CaiHongFM_Click,this.OnCaiHongFMClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Recommend_Click,this.OnRecommendClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.TypeOne_Click,this.OnTypeOneClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.TypeTwo_Click,this.OnTypeTwoClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.TypeThree_Click,this.OnTypeThreeClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.TypeFour_Click, this.OnTypeFourClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.TypeFive_Click, this.OnTypeFiveClick);
    }

    public override void UnInit()
    {
        base.UnInit();
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.CaiHongFM_Click, this.OnCaiHongFMClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Recommend_Click, this.OnRecommendClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.TypeOne_Click, this.OnTypeOneClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.TypeTwo_Click, this.OnTypeTwoClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.TypeThree_Click, this.OnTypeThreeClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.TypeFour_Click, this.OnTypeFourClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.TypeFive_Click, this.OnTypeFiveClick);
    }


    public void OnCaiHongFMClick(HUIEvent evt)
    {

    }

    public void OnRecommendClick(HUIEvent evt)
    {

    }

    public void OnTypeOneClick(HUIEvent evt)
    {

    }
    public void OnTypeTwoClick(HUIEvent evt)
    {

    }
    public void OnTypeThreeClick(HUIEvent evt)
    {

    }
    public void OnTypeFourClick(HUIEvent evt)
    {

    }
    public void OnTypeFiveClick(HUIEvent evt)
    {

    }




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FMIndroduceCtr : MonoBehaviour {

    //public Button buttonClose;
    public Button buttonDownload;
    public RawImage radioTexture;
    public GameObject panel_main;
    public GameObject panel_errtext;

    // Use this for initialization
    public void Init () {

        Debug.Log("indroduce init");
        panel_errtext.SetActive(false);
        panel_main.SetActive(false);

        string localpath = FileTools.CombinePath(GiftBagResManager.Instance.ResPath, FileTools.GetFullName(GiftBagResManager.url_popImage));
        if (FileTools.IsFileExist(localpath))
        {
            Debug.LogError("fileExist: " + localpath);
            GiftBagResManager.Instance.LoadTexture(localpath, (tex) =>
            {
                this.radioTexture.texture = tex;
                panel_main.SetActive(true);
            });
        }
        else
        {
            panel_errtext.SetActive(true);
            gameObject.GetComponent<Button>().onClick.AddListener(() => { DiantaiMainCtr.Instance.RadioPopFailed(); });
        }

        buttonDownload.onClick.AddListener(OnDownloadBtnClick);

    }

    private void OnDestroy()
    {
        buttonDownload.onClick.RemoveAllListeners();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnDownloadBtnClick()
    {
        Debug.Log("下载");
        //gameObject.SetActive(false);
        DiantaiMainCtr.Instance.RadioPopFailed();
        SDKManager.Instance.NativeDownload(GiftBagResManager.url_apk);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum emRadioViewWidget {
	PlayBtn,
	NextBtn,
	PreBtn,
	BackBtn,
	LoadingText,
	PlayText,
	AllText,
	ExitBtn,
	NameText,
	SliderPlay,
	SliderLoading,
}

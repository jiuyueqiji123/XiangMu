﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadioView  {

	private const string FORM_PATH = "radio/radio_ui/RadioForm";

    private const string FORM_MAIN_PATH = "radio/radio_ui/RadioMainForm";

	private HUIFormScript form;

	private GameObject PlayBtnObj;

	private GameObject NextBtnObj;

	private GameObject PreBtnObj;

	private GameObject BackBtnObj;

    private GameObject CaiHongBtn;

    private GameObject RecommendBtn;

    private GameObject TypeOneBtn;

    private GameObject TypeTwoBtn;

    private GameObject TypeThreeBtn;

    private GameObject TypeFourBtn;

    private GameObject TypeFiveBtn;

    private Text LoadingText;
	private Text PlayText;
	private Text AllText;
	private Text NameText;

	private GameObject ExitBtnObj;
	private Slider PlaySlider;
	private Slider LoadingSlider;

	public void Open() {
		this.form = HUIManager.Instance.OpenForm (FORM_PATH, false);

		this.PlayBtnObj = form.GetWidget ((int)emRadioViewWidget.PlayBtn);
		HUIUtility.SetUIMiniEvent (this.PlayBtnObj, enUIEventType.Click, enUIEventID.Radio_Btn_Play);

		this.NextBtnObj = form.GetWidget ((int)emRadioViewWidget.NextBtn);
		HUIUtility.SetUIMiniEvent (this.NextBtnObj, enUIEventType.Click, enUIEventID.Radio_Btn_Next);

		this.PreBtnObj = form.GetWidget ((int)emRadioViewWidget.PreBtn);
		HUIUtility.SetUIMiniEvent (this.PreBtnObj, enUIEventType.Click, enUIEventID.Radio_Btn_Pre);

		this.BackBtnObj = form.GetWidget ((int)emRadioViewWidget.BackBtn);
		HUIUtility.SetUIMiniEvent (this.BackBtnObj, enUIEventType.Click, enUIEventID.Radio_Btn_Back);

		this.ExitBtnObj = form.GetWidget ((int)emRadioViewWidget.ExitBtn);
		HUIUtility.SetUIMiniEvent (this.ExitBtnObj, enUIEventType.Click, enUIEventID.Radio_Btn_Exit);

		this.LoadingText = form.GetWidget ((int)emRadioViewWidget.LoadingText).GetComponent<Text> ();
		this.PlayText = form.GetWidget ((int)emRadioViewWidget.PlayText).GetComponent<Text> ();
		this.AllText = form.GetWidget ((int)emRadioViewWidget.AllText).GetComponent<Text> ();
		this.NameText = form.GetWidget ((int)emRadioViewWidget.NameText).GetComponent<Text> ();

		this.PlaySlider = form.GetWidget ((int)emRadioViewWidget.SliderPlay).GetComponent<Slider> ();
		this.LoadingSlider = form.GetWidget ((int)emRadioViewWidget.SliderLoading).GetComponent<Slider> ();

		this.PlaySlider.onValueChanged.AddListener (PlaySliderValueChange);
	}

    public void OpenRadioMain()
    {
        this.form = HUIManager.Instance.OpenForm(FORM_MAIN_PATH,false);

        this.CaiHongBtn = form.GetWidget("CaiHongButton");
        HUIUtility.SetUIMiniEvent(this.CaiHongBtn,enUIEventType.Click,enUIEventID.CaiHongFM_Click);

        this.RecommendBtn = form.GetWidget("RecommendButton");
        HUIUtility.SetUIMiniEvent(this.RecommendBtn,enUIEventType.Click,enUIEventID.Recommend_Click);

        this.TypeOneBtn = form.GetWidget("TypeOneButton");
        HUIUtility.SetUIMiniEvent(this.TypeOneBtn,enUIEventType.Click,enUIEventID.TypeOne_Click);

        this.TypeTwoBtn = form.GetWidget("TypeTwoButton");
        HUIUtility.SetUIMiniEvent(this.TypeTwoBtn, enUIEventType.Click, enUIEventID.TypeTwo_Click);

        this.TypeThreeBtn = form.GetWidget("TypeThreeButton");
        HUIUtility.SetUIMiniEvent(this.TypeThreeBtn, enUIEventType.Click, enUIEventID.TypeThree_Click);

        this.TypeFourBtn = form.GetWidget("TypeFourButton");
        HUIUtility.SetUIMiniEvent(this.TypeFourBtn, enUIEventType.Click, enUIEventID.TypeFour_Click);

        this.TypeFiveBtn = form.GetWidget("TypeFiveButton");
        HUIUtility.SetUIMiniEvent(this.TypeFiveBtn, enUIEventType.Click, enUIEventID.TypeFive_Click);

    }


    public void Close(string Path)
    {
        HUIManager.Instance.CloseForm(Path);
    }


	public void Close() {
		HUIManager.Instance.CloseForm (FORM_PATH);
	}

	public void SetPlaySlider(bool enable) {
		if (this.PlaySlider.enabled == enable) {
			return;
		}
		this.PlaySlider.enabled = enable;
	}

	public void ChangePlayBtn(bool playing) {
		Text text = this.PlayBtnObj.transform.Find ("Text").GetComponent<Text> ();
		if (text) {
			text.text = playing ? "暂停" : "播放";
		}
	}

	public void ChangeAudio(string name, int time) {
		this.NameText.text = name;
		this.AllText.text = time.ToString ();
	}

	public void ChangeProgressText(int loading, int play) {
		this.LoadingText.text = loading.ToString ();
		this.PlayText.text = play.ToString ();
	}

	public void ChangeSlider(float loading, float play) {
		this.PlaySlider.value = play;
		this.LoadingSlider.value = loading;
	}

	private void PlaySliderValueChange(float v) {
		Debug.Log (v);
		this.PlaySlider.value = v;
		HUIEventManager.Instance.DispatchUIEvent(enUIEventID.Radio_Slider_Value_Change, new stUIEventParams() {argFloat = v});
	}
}

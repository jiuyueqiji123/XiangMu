﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioController : Singleton<RadioController> {

	private RadioView view;

	private RadioModel model;

	public override void Init ()
	{
		base.Init ();
		this.view = new RadioView ();
		this.model = new RadioModel ();

		HUIEventManager.Instance.AddUIEventListener (enUIEventID.Radio_Btn_Play, this.OnPlayClick);
		HUIEventManager.Instance.AddUIEventListener (enUIEventID.Radio_Btn_Next, this.OnNextClick);
		HUIEventManager.Instance.AddUIEventListener (enUIEventID.Radio_Btn_Pre, this.OnPreClick);
		HUIEventManager.Instance.AddUIEventListener (enUIEventID.Radio_Btn_Back, this.OnBackClick);
		HUIEventManager.Instance.AddUIEventListener (enUIEventID.Radio_Btn_Exit, this.OnExitClick);
		HUIEventManager.Instance.AddUIEventListener (enUIEventID.Radio_Slider_Value_Change, this.OnSliderValueChange);

		EventBus.Instance.AddEventHandler(EventID.GAME_QUIT, this.OnGameExit);
		EventBus.Instance.AddEventHandler(EventID.GAME_RESUME, this.OnGameResume);
	}

	public override void UnInit ()
	{
		base.UnInit ();
		HUIEventManager.Instance.RemoveUIEventListener (enUIEventID.Radio_Btn_Play, this.OnPlayClick);
		HUIEventManager.Instance.RemoveUIEventListener (enUIEventID.Radio_Btn_Next, this.OnNextClick);
		HUIEventManager.Instance.RemoveUIEventListener (enUIEventID.Radio_Btn_Pre, this.OnPreClick);
		HUIEventManager.Instance.RemoveUIEventListener (enUIEventID.Radio_Btn_Back, this.OnBackClick);
		HUIEventManager.Instance.RemoveUIEventListener (enUIEventID.Radio_Btn_Exit, this.OnExitClick);
		HUIEventManager.Instance.RemoveUIEventListener (enUIEventID.Radio_Slider_Value_Change, this.OnSliderValueChange);

		EventBus.Instance.RemoveEventHandler(EventID.GAME_QUIT, this.OnGameExit);
		EventBus.Instance.RemoveEventHandler(EventID.GAME_RESUME, this.OnGameResume);
	}

	public void EnterRadio() {
		//MainController.Instance.CloseView ();
		string json = this.model.GetJsonData ();
		Debug.Log (json);
		RadioSDKManager.Instance.InitRadio (json);
		this.view.Open ();
		ReflashPlayBtn ();
		ReflashNameAndTime ();
	}

    public void EnterRadioMain()
    {
        MainController.Instance.CloseView();
        SceneLoader.Instance.LoadScene(SceneLoader.RadioScene, null);
        this.view.OpenRadioMain();
    }

	public void ExitRadio() {
		this.view.Close ();
		MainController.Instance.OpenView ();
	}

	#region view
	private void ReflashPlayBtn() {
		bool playing = RadioSDKManager.Instance.IsPlaying ();
		this.view.ChangePlayBtn (playing);
	}

	private void ReflashProgress() {
		this.view.ChangeProgressText (this.model.CurrentLoadingProgress, this.model.CurrentPlayProgress);
		this.view.ChangeSlider (this.model.CurLoadingValue, this.model.CurPlayValue);
	}

	private void ReflashNameAndTime() {
		this.view.ChangeAudio (this.model.CurName, this.model.CurrentAudioLength);
	}
	#endregion

	#region ui event
	private void OnPlayClick(HUIEvent evt) {
		bool playing = RadioSDKManager.Instance.IsPlaying ();
		if (playing) {
			RadioSDKManager.Instance.Pause ();
		} else {
			RadioSDKManager.Instance.Play (this.model.CurrentIndex, this.model.CurrentPlayProgress);
		}
		this.view.ChangePlayBtn (!playing);
	}

	private void OnNextClick(HUIEvent evt) {
		this.model.Next ();
		RadioSDKManager.Instance.Next ();
	}

	private void OnPreClick(HUIEvent evt) {
		this.model.Pre ();
		RadioSDKManager.Instance.Pre ();
	}

	private void OnBackClick(HUIEvent evt) {
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
        this.ExitRadio ();
	}

	private void OnExitClick(HUIEvent evt) {
		Framework.Instance.QuitGame ();
	}

	private void OnSliderValueChange(HUIEvent evt) {
		//TimerManager.Instance.RemoveTimer (new Timer.OnTimeUpHandler(this.GetPlayProgress));

		if (RadioSDKManager.Instance.IsPlaying ()) {
			float pro = evt.m_eventParams.argFloat;
			int len = (int)(this.model.CurrentAudioLength * pro);
			RadioSDKManager.Instance.Play (this.model.CurrentIndex, len);
		}

	}

    
	#endregion

	#region event
	private void OnGameExit() {
		RadioSDKManager.Instance.Destroy ();
	}

	private void OnGameResume() {
		SetAudioInfo (RadioSDKManager.Instance.GetInfo ());
	}
	#endregion

	private void GetPlayProgress(int seq) {
		this.model.ReflashProgress (RadioSDKManager.Instance.GetProgress ());
		if (this.model.CurrentAudioLength > 0) {
			this.view.SetPlaySlider (true);
		}
		ReflashProgress ();
	}

	public void SetAudioInfo(string info) {
		Debug.Log ("Radio Info Change");
		this.model.ReflashInfo (info);
		ReflashNameAndTime ();
		GetPlayProgress (0);
		TimerManager.Instance.RemoveTimer (new Timer.OnTimeUpHandler(this.GetPlayProgress));
		TimerManager.Instance.AddTimer (1000, 0, new Timer.OnTimeUpHandler(this.GetPlayProgress));
	}
}

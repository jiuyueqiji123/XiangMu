﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEngine.UI;public class DiantaiUIBuy : MonoBehaviour
{
    #region component
    Button btn_close;
    Text text_title;
    Text text_subtitle;
    Button btn_download;
    Button btn_buy;
    #endregion

    #region data
    protected int goods_id;


    #endregion
    #region cache
    protected bool hasInitialized = false;
    #endregion    private void OnEnable()
    {
        if(!hasInitialized)
        _initilatize();
        _register();
        _updateData();
    }    protected void _initilatize()
    {
        btn_close = transform.Find("close_btn").GetComponent<Button>();
        text_title = transform.Find("Title_txt").GetComponent<Text>();
        text_subtitle = transform.Find("SubTitle_txt").GetComponent<Text>();
        btn_download = transform.Find("download_btn").GetComponent<Button>();
        btn_buy = transform.Find("buy_btn").GetComponent<Button>();

        hasInitialized = true;
    }

    protected void _register()
    {
        btn_buy.onClick.AddListener(buy);
    }


    protected void _unRegister()
    {
        btn_buy.onClick.RemoveAllListeners();
        
    }


    protected void buy()
    {
        PayManager.Instance.Pay(goods_id,_callback);

        //
        //
        //
        //RadioDataPool.Instance.BuyAlbumSuccess(goods_id);
        //DiantaiMainCtr.Instance.RefreshUI();
        this.gameObject.SetActive(false);
        Debug.Log("fresh");
    }    protected void _callback(int a, bool b)
    {
        Debug.Log(string.Format("a:{0} b:{1}",a,b));

        if (b)
            RadioDataPool.Instance.BuyAlbumSuccess(goods_id);

        DiantaiMainCtr.Instance.RefreshUI();
        
    }    protected void _updateData()
    {
        //TableProto.MainItemInfo itemInfo = PayManager.Instance.GetItemById(this.goods_id);
        //string text = text_subtitle.text;
        //text.Replace("28", itemInfo.price.ToString());
        //text_subtitle.text = text;
    }    private void OnDisable()
    {
        _unRegister();
        goods_id = 0;
    }    public void InjectData(int goodid)
    {
        this.goods_id = goodid;
        
    }}
﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using System;using System.Text;

public class RadioResDownload:MonoBehaviour{    private void Awake()    {        DontDestroyOnLoad(gameObject);    }    protected float interval = 2f;    protected float timer = 0f;    protected Action<float> onProgress;    protected Action onFinish;    protected Action<string> OnError;    protected List<RadioDownLoadItem> list;    public bool started = false;    public int downloadingFilesNum = 0;    public int finishedFilesNum = 0;    public float progress = 0f;    public void Set(List<RadioDownLoadItem> needDownloadList, Action<float> progressFunc,Action finishFunc,Action<string> errorFunc)    {        if (list == null) list = new List<RadioDownLoadItem>();        list.AddRange(needDownloadList);        onProgress = progressFunc;        onFinish = finishFunc;        OnError = errorFunc;        DownloadBegin();    }    void DownloadBegin()
    {
        FileTools.CreateDirectory(RadioDataPool.fileSavePath);
        Debug.Log(RadioDataPool.fileSavePath);
        for (int i = 0; i < list.Count; i++)
        {
            DownloadTask task = new DownloadTask();
            RadioDownLoadItem item = list[i];
            task.URL = item.coverUrl;
            task.SavePath = item.localCoverFileUrl;
            task.GetHashCode();
            task.OnProgress = new DownloadTask.OnProgressDelegate(delegate(long totalsize,long nowsize) 
            {
                task.DownloadedSize = nowsize;
                task.Size = totalsize;
                if (onProgress != null)
                    onProgress(task.DownloadedSize/task.Size);
            });
            task.OnError = new DownloadTask.OnErrorDelegate(delegate(string errStr) 
            {
                if (OnError != null) OnError(errStr);
            });
            task.OnSuccess = new DownloadTask.OnSuccessDelegate(delegate () 
            {
                if (onFinish != null)
                    onFinish();
            });
            DownloadManager.Instance.AddTask(task);
        }
    }    [ContextMenu("测试下载")]    void Test()    {        if (Application.isPlaying)        {            Debug.Log("o.o");            RadioDownLoadItem t1 = new RadioDownLoadItem();            t1.itemId = "1";            t1.coverUrl = "http://192.168.1.37/file/download/AlphaTopBottom.mp4";            RadioDownLoadItem t2 = new RadioDownLoadItem();            t2.itemId = "2";            t2.coverUrl = "http://192.168.1.37/file/download/BigBuckBunny_360p30.mp3";            RadioDownLoadItem t3 = new RadioDownLoadItem();            t3.itemId = "3";            t3.coverUrl = "http://192.168.1.37/file/download/SampleSubtitles.srt";
            RadioDownLoadItem t4 = new RadioDownLoadItem();            t4.itemId = "4";            t4.coverUrl = "http://192.168.1.37/file/download/极乐净土720p.mp4";
            RadioDownLoadItem t5 = new RadioDownLoadItem();            t5.itemId = "5";            t5.coverUrl = "http://192.168.1.37/file/download/极乐净土1080p.mp4";            List<RadioDownLoadItem> rlist = new List<RadioDownLoadItem>();            rlist.Add(t1);            rlist.Add(t2);            rlist.Add(t3);            rlist.Add(t4);            rlist.Add(t5);            Set(rlist,                 (p) => { Debug.Log(p); },                 () => { Debug.Log("finished"); },                (s)=> { Debug.Log("error: " + s); });        }    }}public class RadioDownLoadItem{    /// <summary>    /// ID规则 TaGID_Index    /// </summary>    public string itemId;    /// <summary>    /// 下载链接    /// </summary>    public string coverUrl;    /// <summary>    /// MD5值    /// </summary>    public string coverMD5;    public string localCoverFileUrl    {        get        {            return RadioDataPool.fileSavePath + localCoverFileName;        }    }    public string localCoverFileName    {        get        {            if (coverUrl == string.Empty || coverUrl == "") return "";            string fileName = itemId;            string fileType = coverUrl.Split('.')[coverUrl.Split('.').Length - 1];            return fileName + "." + fileType;        }    }


    /// <summary>
    /// 音频路径
    /// </summary>    public string audioUrl;

    public string localAudioFileUrl    {        get        {            return RadioDataPool.fileSavePath + localAudioFileName;        }    }    public string localAudioFileName    {        get        {            if (audioUrl == string.Empty || audioUrl == "") return "";            string fileName = itemId;            string fileType = audioUrl.Split('.')[audioUrl.Split('.').Length - 1];            return fileName + "." + fileType;        }    }    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(itemId).Append(",");
        sb.Append(coverUrl).Append(",");
        sb.Append(audioUrl).Append(",");
        return sb.ToString();
    }    public string GetDownloadKeyString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(itemId).Append(",");
        sb.Append(coverUrl);
        return sb.ToString();
    }}
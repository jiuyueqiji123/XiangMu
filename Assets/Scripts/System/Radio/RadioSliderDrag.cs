﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RadioSliderDrag : MonoBehaviour, IEndDragHandler,IBeginDragHandler,IPointerClickHandler {
    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("slider begin drag.");
        HUIEventManager.Instance.DispatchUIEvent(enUIEventID.Radio_Slider_DragBegin);
    }

    public void OnEndDrag(PointerEventData data)
	{
        Debug.Log("slider end drag.");
        HUIEventManager.Instance.DispatchUIEvent(enUIEventID.Radio_Slider_DragEnd);
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        HUIEventManager.Instance.DispatchUIEvent(enUIEventID.Radio_Slider_Click,
            new stUIEventParams { argFloat = eventData.position.x, argFloat2 = eventData.position.y });
    }
}

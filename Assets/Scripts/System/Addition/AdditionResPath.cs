﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class AdditionResPath {
    // Dir
    public const string AUDIO_DIR = "study_addition/study_addition_audio/";
    public const string PREFAB_DIR = "study_addition/study_addition_prefab/";
    public const string TEXTURE_DIR = "study_addition/study_addition_texture/";
    public const string VIEW_DIR = "study_addition/study_addition_view/";
    public const string TIMELINE_DIR = "study_addition/study_addition_timeline/";

    // Path
    public const string TL_ADDITION = TIMELINE_DIR + "Addition_Timeline";

    public const string ADDITION_MANAGER = PREFAB_DIR + "AdditionManager";
    public const string CAR_AND_BOX = PREFAB_DIR + "CarAndBox";
    public const string SCENE_1 = PREFAB_DIR + "Scene_1";
    public const string SCENE_2 = PREFAB_DIR + "Scene_2";
    public const string SHEEP = PREFAB_DIR + "Sheep";

    public const string VOICE_SELECT_CORRECT = AUDIO_DIR + "402010302";
    public const string VOICE_NUM_1 = AUDIO_DIR + "403020202";
    public const string VOICE_NUM_2 = AUDIO_DIR + "403020203";
    public const string VOICE_NUM_3 = AUDIO_DIR + "403020204";
    public const string VOICE_NUM_4 = AUDIO_DIR + "403020205";
    public const string VOICE_NUM_5 = AUDIO_DIR + "403020206";
    public const string VOICE_NUM_6 = AUDIO_DIR + "403020207";
    public const string VOICE_NUM_7 = AUDIO_DIR + "403020208";
    public const string VOICE_NUM_8 = AUDIO_DIR + "403020209";
    public const string VOICE_NUM_9 = AUDIO_DIR + "403020210";

    public const string VOICE_OPERATOR_PLUS = AUDIO_DIR + "403030208";
    public const string VOICE_OPERATOR_EQUAL = AUDIO_DIR + "403030209";

    public const string VOICE_SHEEP_SPEAK = AUDIO_DIR + "603030101";

    // SE: Sound Effect
    public const string SE_SHOW_HIDE = AUDIO_DIR + "603030102";
    public const string SE_ERROR = AUDIO_DIR + "603030303";
    public const string SE_SHERA = AUDIO_DIR + "603030202";
    public const string SE_WOOL_MOVE = AUDIO_DIR + "603030201";
    public const string SE_ADSORPTION = AUDIO_DIR + "604030102";
    public const string SE_CAR_MOVE = AUDIO_DIR + "603030301";
    public const string SE_CAR_STOP = AUDIO_DIR + "603030302";
    public const string SE_STAR = AUDIO_DIR + "601010110";
    public const string SE_CAR_CLICK = AUDIO_DIR + "603030304";
    public const string SE_BOX_MOVE = AUDIO_DIR + "603030305";

    // TEX: Texture
    public const string TEX_SHEEP_GRAY = TEXTURE_DIR + "303030201_gray";

    // BGM
    public const string BGM_601010112 = AUDIO_DIR + "601010112";
}

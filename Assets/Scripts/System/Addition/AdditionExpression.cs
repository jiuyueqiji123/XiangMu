﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionExpression {

    public int LeftValue { get; private set; }
    public int RightValue { get; private set; }
    public int Sum { get; private set; }

    public AdditionExpression(int leftValue, int rightValue, int sum) {
        LeftValue = leftValue;
        RightValue = rightValue;
        Sum = sum;
    }
    
    public static AdditionExpression CreateFromSum(int sum) {
        var leftValue = Random.Range(1, sum);
        var rightValue = sum - leftValue;
        return new AdditionExpression(leftValue, rightValue, sum);
    }

    public static AdditionExpression RandomWithinTen() {
        var sum = Random.Range(2, 10);
        return CreateFromSum(sum);
    }

}

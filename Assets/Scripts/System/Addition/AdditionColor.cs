﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class AdditionColor {

    /// <summary>
    /// 米黄色
    /// </summary>
	public static Color Beige {
        get { return new Color32(255, 255, 235, 255); }
    }

    /// <summary>
    /// 浅蓝色
    /// </summary>
    public static Color BabyBlue {
        get { return new Color32(22, 164, 217, 255); }
    }
}

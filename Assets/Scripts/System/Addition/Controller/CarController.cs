﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {
    public Renderer leftRender;
    public Renderer rightRender;

    Animator _animator;

    void Start() {
        _animator = GetComponent<Animator>();
    }

    void OnMouseDown() {
        StopAllCoroutines();
        AudioManager.Instance.StopAllSound();
        AudioManager.Instance.PlaySound(AdditionResPath.SE_CAR_CLICK);
        ToAnimTouch();

        var ae = AdditionManager.CurrentAE;
        StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(AdditionManager.Instance.GetNumVoicePath(ae.LeftValue)).Length, () => {
            StartCoroutine(TimeUtility.DelayInvoke(AudioManager.Instance.PlaySound(AdditionResPath.VOICE_OPERATOR_PLUS).Length, () => {
                AudioManager.Instance.PlaySound(AdditionManager.Instance.GetNumVoicePath(ae.RightValue));
            }));
        }));
    }

    public void SetLeftValue(int num) {
        leftRender.material.mainTexture = CommonUtility.LoadTexture(AdditionResPath.TEXTURE_DIR + num);
    }

    public void SetRightValue(int num) {
        rightRender.material.mainTexture = CommonUtility.LoadTexture(AdditionResPath.TEXTURE_DIR + num);
    }

    public void ToAnimMove() {
        _animator.CrossFade("Move", 0.1f);
    }
    public void ToAnimStop() {
        _animator.CrossFade("Stop", 0.1f);
    }

    public void ToAnimTouch() {
        _animator.Play("Touch", -1, 0f);
    }
}

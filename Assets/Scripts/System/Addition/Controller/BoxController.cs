﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BoxController : MonoBehaviour {

    public Renderer sumRender;
    public Transform target;

    Vector3 _originPosition;
    int _sum;
    Animator _animator;

    void Start() {
        _originPosition = transform.localPosition;
        _animator = GetComponent<Animator>();
    }

    public void SetSumValue(int num) {
        sumRender.material.mainTexture = CommonUtility.LoadTexture(AdditionResPath.TEXTURE_DIR + num);
        _sum = num;
    }

    public void Reset() {
        transform.localPosition = _originPosition;
        transform.localScale = Vector3.one;
    }

    void OnMouseDown() {
        if (_sum.Equals(AdditionManager.CurrentAE.Sum)) {
            AudioManager.Instance.PlaySound(AdditionResPath.SE_BOX_MOVE);
            transform.DOMove(target.position, 0.5f);
            transform.DOScale(Vector3.zero, 0.5f);
            StartCoroutine(TimeUtility.DelayInvoke(0.5f, AdditionManager.Instance.HideCarAndBox));
        } else {
            ToAnimError();
            AudioManager.Instance.PlaySound(AdditionResPath.SE_ERROR);
        }
    }

    public void ToAnimError() {
        _animator.Play("Error", -1, 0f);
    }
}

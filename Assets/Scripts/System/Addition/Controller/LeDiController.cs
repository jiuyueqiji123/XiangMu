﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeDiController : MonoBehaviour {
    public GameObject cameraForIphone;
    public GameObject cameraForIpad;
    public GameObject effect;

	void Start() {
        if (cameraForIphone == null) {
            cameraForIphone = transform.Find("Camera").gameObject;
        }

        if (cameraForIpad == null) {
            cameraForIpad = transform.Find("Camera_iPad").gameObject;
        }

        cameraForIphone.SetActive(ScreenUtility.IsWideScreen);
        cameraForIpad.SetActive(!ScreenUtility.IsWideScreen);
    }

    public void ActiveEffect(bool isActived) {
        effect.SetActive(isActived);
    }

    public void OnClap() {
        ActiveEffect(false);
        AdditionManager.Instance.PlayTimeline();
    }

}

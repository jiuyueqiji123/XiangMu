﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionController : Singleton<AdditionController> {
    private const string _VIEW_PATH = AdditionResPath.VIEW_DIR + "ReturnForm";
    private ReturnView _view;

    public override void Init() {
        base.Init();
        _view = new ReturnView();
    }

    public override void UnInit() {
        base.UnInit();
    }

    public void OpenView() {
        _view.OpenForm(_VIEW_PATH);
        RegisterEvent();
        CommonUtility.InstantiateFrom(AdditionResPath.ADDITION_MANAGER, null);
        Input.multiTouchEnabled = false;
    }

    public void CloseView() {
        _view.CloseForm();
        RemoveEvent();
        AdditionManager.DestroyInstance();
        Input.multiTouchEnabled = true;
    }

    private void RegisterEvent() {
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.CakeShop_Btn_Back, ClickBtnBack);
    }

    private void RemoveEvent() {
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.CakeShop_Btn_Back, ClickBtnBack);
    }

    public void ClickBtnBack(HUIEvent e) {
        AdditionManager.Instance.OnClickBackButton();
    }
}

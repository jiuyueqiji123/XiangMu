﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class DragBoxUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    static int _total = 3;
    static RectTransform _currentDragTarget;
    static List<DragBoxUI> _allDragBoxUI;
    static bool _isFillLeft;
    static bool _isFillRight;

    Vector3 _originPos;
    Canvas _canvas;
    Animator _animator;
    float _interval = 5f;

    void Awake() {
        if (_allDragBoxUI == null) {
            _allDragBoxUI = new List<DragBoxUI>();
        }
    }

    void Start() {
        _originPos = GetComponent<RectTransform>().anchoredPosition;
        _canvas = GetComponentInParent<Canvas>();
        _animator = GetComponent<Animator>();
        _allDragBoxUI.Add(this);
    }

    void OnDestroy() {
        if (_allDragBoxUI != null) {
            _allDragBoxUI.Clear();
            _allDragBoxUI = null;
        }
    }

    public void OnBeginDrag(PointerEventData eventData) {
        if (_currentDragTarget) {
            _currentDragTarget.GetComponent<Image>().raycastTarget = true;
            _currentDragTarget = null;
        }

        _currentDragTarget = GetComponent<RectTransform>();
        _currentDragTarget.GetComponent<Image>().raycastTarget = false;
        PlayAllAnimNormal();
    }

    public void OnDrag(PointerEventData eventData) {
        Vector3 pos;

        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(_canvas.transform as RectTransform, eventData.position, _canvas.worldCamera, out pos)) {
            _currentDragTarget.position = pos;
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        var go = eventData.pointerCurrentRaycast.gameObject;

        if (go == null) {
            _currentDragTarget.DOAnchorPos(_originPos, 0.5f);
            _currentDragTarget.GetComponent<Image>().raycastTarget = true;
            return;
        }

        var endPos = go.GetComponent<RectTransform>().position;
        var value = int.Parse(GetActiveChildName());

        if (!_isFillLeft && value.Equals(AdditionManager.CurrentAE.LeftValue) && go.name.Equals("Wireframe Left")) {
            AudioManager.Instance.PlaySound(AdditionResPath.SE_ADSORPTION);
            _currentDragTarget.DOMove(endPos, 0.1f).OnComplete(SetTotal);
            _isFillLeft = true;
        } else if (!_isFillRight && value.Equals(AdditionManager.CurrentAE.RightValue) && go.name.Equals("Wireframe Right")) {
            AudioManager.Instance.PlaySound(AdditionResPath.SE_ADSORPTION);
            _currentDragTarget.DOMove(endPos, 0.1f).OnComplete(SetTotal);
            _isFillRight = true;
        } else if (value.Equals(AdditionManager.CurrentAE.Sum) && go.name.Equals("Wireframe Sum")) {
            AudioManager.Instance.PlaySound(AdditionResPath.SE_ADSORPTION);
            _currentDragTarget.DOMove(endPos, 0.1f).OnComplete(SetTotal);
        } else {
            _currentDragTarget.DOAnchorPos(_originPos, 0.5f);
            _currentDragTarget.GetComponent<Image>().raycastTarget = true;
        }

        _currentDragTarget = null;
        PlayAllAnimShake(false);
    }

    public void PlayAnimNormal() {
        _animator.Play("Normal", -1, 0f);
    }

    public void PlayAnimShake() {
        _animator.Play("Shake", -1, 0f);
    }

    public void PlayAnimShakeRepeating() {
        InvokeRepeating("PlayAnimShake", _interval, _interval);
    }

    public static void PlayAllAnimShake(bool isImmediate = true) {
        foreach (var dbui in _allDragBoxUI) {
            if (dbui.GetComponent<Image>().raycastTarget) {
                if (isImmediate) {
                    dbui.PlayAnimShake();
                }
                
                dbui.PlayAnimShakeRepeating();
            }
        }
    }

    public static void PlayAllAnimNormal() {
        foreach (var dbui in _allDragBoxUI) {
            dbui.PlayAnimNormal();
            dbui.CancelInvoke();
        }
    }

    void SetTotal() {
        _total -= 1;

        if (_total <= 0) {
            AdditionManager.Instance.OnDragComplete();
            _total = 3;
            _isFillLeft = false;
            _isFillRight = false;
        }
    }

    string GetActiveChildName() {
        foreach (RectTransform child in _currentDragTarget) {
            if (child.gameObject.activeInHierarchy) {
                return child.name;
            }
        }

        return null;
    }

}

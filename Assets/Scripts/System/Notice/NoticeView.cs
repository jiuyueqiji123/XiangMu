﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoticeView  {

    private const string Path = "ui/ui_notice/NoticeForm";

    private HUIFormScript formScript;

    private Text contentText;

    private GameObject CancelObj;

    private GameObject OkObj;

    private GameObject CloseObj;

    public void Open(string content)
    {
        this.formScript = HUIManager.Instance.OpenForm(Path, true);
        this.contentText = this.formScript.GetWidget((int)EmNoticeWidget.ContentText).GetComponent<Text>();
        this.CancelObj = this.formScript.GetWidget((int)EmNoticeWidget.CancelBtn);
        this.OkObj = this.formScript.GetWidget((int)EmNoticeWidget.OkBtn);
        this.CloseObj = this.formScript.GetWidget((int)EmNoticeWidget.CloseBtn);
        HUIUtility.SetUIMiniEvent(CancelObj, enUIEventType.Click, enUIEventID.Notice_Btn_Cancel_Click);
        HUIUtility.SetUIMiniEvent(OkObj, enUIEventType.Click, enUIEventID.Notice_Btn_Ok_Click);
        HUIUtility.SetUIMiniEvent(CloseObj, enUIEventType.Click, enUIEventID.Notice_Btn_Close_Click);

        this.contentText.text = content;
    }

    public void ShowNullBtn()
    {
        this.CancelObj.SetActive(false);
        this.OkObj.SetActive(false);
    }

    public void ShowOneBtn()
    {
        this.CancelObj.SetActive(false);
        this.OkObj.SetActive(true);
        Vector3 pos = this.OkObj.transform.localPosition;
        this.OkObj.transform.localPosition = new Vector3(0f, pos.y, pos.z);
    }

    public void ShowTwoBtn()
    {
        this.CancelObj.SetActive(true);
        this.OkObj.SetActive(true);
        Vector3 pos = this.OkObj.transform.localPosition;
        this.OkObj.transform.localPosition = new Vector3(170f, pos.y, pos.z);
    }

    public void Close()
    {
        HUIManager.Instance.CloseForm(this.formScript);
        this.contentText = null;
        this.CancelObj = null;
        this.OkObj = null;
        this.CloseObj = null;
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class NoticeManager : Singleton<NoticeManager>
{ 
    private NoticeView view;

    private Action closeAction;
    private Action cancelAction;
    private Action okAction;

    public override void Init()
    {
        base.Init();
        this.view = new NoticeView();
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Notice_Btn_Cancel_Click, this.OnBtnCancelClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Notice_Btn_Close_Click, this.OnBtnCloseClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Notice_Btn_Ok_Click, this.OnBtnOkClick);
    }

    public override void UnInit()
    {
        base.UnInit();
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Notice_Btn_Cancel_Click, this.OnBtnCancelClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Notice_Btn_Close_Click, this.OnBtnCloseClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Notice_Btn_Ok_Click, this.OnBtnOkClick);
    }

    public void ShowTip(string content, Action closeAction = null)
    {
        this.view.Open(content);
        this.view.ShowNullBtn();
        this.closeAction = closeAction;
        this.okAction = null;
        this.cancelAction = null;
    }

    public void ShowOkTip(string content, Action okAction = null, Action closeAction = null)
    {
        this.view.Open(content);
        this.view.ShowOneBtn();
        this.closeAction = closeAction;
        this.okAction = okAction;
        this.cancelAction = null;
    }

    public void ShowOkCancelTip(string content, Action okAction = null, Action cancelAction = null, Action closeAction = null)
    {
        this.view.Open(content);
        this.view.ShowTwoBtn();
        this.closeAction = closeAction;
        this.okAction = okAction;
        this.cancelAction = cancelAction;
    }

    public void CloseTip()
    {
        this.view.Close();
    }

    private void OnBtnCloseClick(HUIEvent evt)
    {
        this.view.Close();
        if (closeAction != null)
        {
            closeAction();
        }
    }

    private void OnBtnCancelClick(HUIEvent evt)
    {
        this.view.Close();
        if (cancelAction != null)
        {
            cancelAction();
        }
    }

    private void OnBtnOkClick(HUIEvent evt)
    {
        this.view.Close();
        if (okAction != null)
        {
            okAction();
        }
    }
}

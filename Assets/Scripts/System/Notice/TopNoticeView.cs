﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopNoticeView {

    private const string Path = "ui/ui_notice/TopNoticeForm";

    private GameObject form;

    private bool isInit;

    private TopNoticePanel panel;

    

    public void Open(string content)
    {
        if (this.form == null)
        {
            GameObject go = ResourceManager.Instance.GetResource(Path, typeof(GameObject), enResourceType.ScenePrefab).m_content as GameObject;
            this.form = GameObject.Instantiate(go);
            GameObject.DontDestroyOnLoad(this.form);
            this.Init();
        }
        this.form.SetActive(true);
        this.panel.Init(content);
    }

    public void ShowNullBtn()
    {
        this.panel.ShowNullBtn();
    }

    public void ShowOneBtn()
    {
        this.panel.ShowOneBtn();
    }

    public void ShowTwoBtn()
    {
        this.panel.ShowTwoBtn();
    }

    private void Init()
    {
        if (!isInit)
        {
            isInit = true;
            this.panel = this.form.transform.Find("Panel").GetComponent<TopNoticePanel>();
        }
    }

    public void Close()
    {
        if (this.form != null && this.form.activeSelf)
        {
            this.form.SetActive(false);
        }
    }
}

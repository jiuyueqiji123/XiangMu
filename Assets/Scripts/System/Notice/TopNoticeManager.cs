﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopNoticeManager : Singleton<TopNoticeManager> {

    private TopNoticeView view;

    private Action closeAction;
    private Action cancelAction;
    private Action okAction;

    public override void Init()
    {
        base.Init();
        this.view = new TopNoticeView();
    }

    public void ShowTip(string content, Action closeAction = null)
    {
        this.view.Open(content);
        this.view.ShowNullBtn();
        this.closeAction = closeAction;
        this.okAction = null;
        this.cancelAction = null;
    }

    public void ShowOkTip(string content, Action okAction = null, Action closeAction = null)
    {
        this.view.Open(content);
        this.view.ShowOneBtn();
        this.closeAction = closeAction;
        this.okAction = okAction;
        this.cancelAction = null;
    }

    public void ShowOkCancelTip(string content, Action okAction = null, Action cancelAction = null, Action closeAction = null)
    {
        this.view.Open(content);
        this.view.ShowTwoBtn();
        this.closeAction = closeAction;
        this.okAction = okAction;
        this.cancelAction = cancelAction;
    }

    public void OnBtnCloseClick()
    {
        this.view.Close();
        if (closeAction != null)
        {
            closeAction();
        }
    }

    public void OnBtnCancelClick()
    {
        this.view.Close();
        if (cancelAction != null)
        {
            cancelAction();
        }
    }

    public void OnBtnOkClick()
    {
        this.view.Close();
        if (okAction != null)
        {
            okAction();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopNoticePanel : MonoBehaviour {

    public Text ContentText;

    public GameObject CancelObj;

    public GameObject OkObj;

    public GameObject CloseObj;

    public void Init(string cont)
    {
        ContentText.text = cont;
    }

    public void ShowNullBtn()
    {
        this.CancelObj.SetActive(false);
        this.OkObj.SetActive(false);
    }

    public void ShowOneBtn()
    {
        this.CancelObj.SetActive(false);
        this.OkObj.SetActive(true);
        Vector3 pos = this.OkObj.transform.localPosition;
        this.OkObj.transform.localPosition = new Vector3(0f, pos.y, pos.z);
    }

    public void ShowTwoBtn()
    {
        this.CancelObj.SetActive(true);
        this.OkObj.SetActive(true);
        Vector3 pos = this.OkObj.transform.localPosition;
        this.OkObj.transform.localPosition = new Vector3(170f, pos.y, pos.z);
    }

    public void OkBtnClick()
    {
        TopNoticeManager.Instance.OnBtnOkClick();
    }

    public void CancelBtnClick()
    {
        TopNoticeManager.Instance.OnBtnCancelClick();
    }

    public void CloseBtnClick()
    {
        TopNoticeManager.Instance.OnBtnCloseClick();
    }
}

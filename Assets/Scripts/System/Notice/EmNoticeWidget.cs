﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EmNoticeWidget {
    ContentText,
    CancelBtn,
    OkBtn,
    CloseBtn,
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnglishColorManager : MonoSingleton<EnglishColorManager>, IEasyLoading
{
    private const string studyPhonicPath = "study_phonics/study_phonics_sound/";

    EnglishColorController gameController = new EnglishColorController();


    private bool isLockInput;



    void Update()
    {
        if (gameController != null)
            gameController.OnUpdate();
    }

    public void StartGame()
    {
        AudioManager.Instance.PlayMusic(studyPhonicPath + "603010801", true, true, true);
        LoadSelectScene();
        AddEvent();
    }

    public void ExitGame()
    {
        DestroySelectScene();
        RemoveEvent();
    }

    public void StopCurrentGame()
    {
        OnGameComplete(false);
    }

    private void AddEvent()
    {
        EventBus.Instance.AddEventHandler(EventID.ON_ENGLISH_WINDOW_BTN_RETURN_CLICK, OnEnglishWindowReturnClick);
    }

    private void RemoveEvent()
    {
        EventBus.Instance.RemoveEventHandler(EventID.ON_ENGLISH_WINDOW_BTN_RETURN_CLICK, OnEnglishWindowReturnClick);
    }

    private void OnEnglishWindowReturnClick()
    {
        if (isLockInput)
            return;
        isLockInput = true;
        GameStateManager.Instance.GotoState(GameStateName.STUDY_ENGLISH_COLOR_STATE);
    }

    private void LoadSelectScene()
    {
        isLockInput = false;
        WindowManager.Instance.OpenWindow(WinNames.EnglishColorMainWindow);
        EnglishColorMainWindow.Instance.SelectGame(OnSelectGameComplete);
    }

    private void DestroySelectScene()
    {
        WindowManager.Instance.CloseWindow(WinNames.EnglishColorMainWindow);
    }

    private void OnSelectGameComplete(EEnglishColorGameType type)
    {
        if (EasyLoadingEx.IsLoading)
            return;

        isLockInput = true;
        this.StartLoadingAndEndEx(() =>
        {
            WindowManager.Instance.CloseWindow(WinNames.EnglishColorMainWindow, true);
            gameController.StartGame(type, OnGameComplete);
        });
    }

    private void OnGameComplete(bool isComplete)
    {
        this.StartLoadingAndEndEx(() =>
        {
            gameController.ExitGame();
            LoadSelectScene();
        });
    }



}

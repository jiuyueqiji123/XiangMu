﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BslActor : MonoBehaviour {

    public enum State { idle,cuowu}
    string[] idle_spritenames = { "daiji_01", "daiji_02", "daiji_03", "daiji_04" };
    string[] cuowu_spritenames = { "cuowufanghui_01", "cuowufanghui_02", "cuowufanghui_03" };

    private List<Sprite> _idleSprites;
    private List<Sprite> _cuowuSprites;

    UGUISpriteAnimation _spriteAnimation;

    private void Start()
    {
        _idleSprites = new List<Sprite>();
        _cuowuSprites = new List<Sprite>();

        for (int i = 0; i < idle_spritenames.Length; i++)
            _idleSprites.Add(UISpriteManager.Instance.GetSprite(emUIAltas.English_Colours, idle_spritenames[i]));
        for (int i = 0; i < cuowu_spritenames.Length; i++)
            _cuowuSprites.Add(UISpriteManager.Instance.GetSprite(emUIAltas.English_Colours, cuowu_spritenames[i]));

        _spriteAnimation = GetComponent<UGUISpriteAnimation>();
        ChangeState(State.idle);
    }

    public void ChangeState(BslActor.State state)
    {
        switch (state)
        {
            case State.idle:

                _spriteAnimation.SpriteFrames = _idleSprites;

                break;
            case State.cuowu:

                _spriteAnimation.SpriteFrames = _cuowuSprites;

                break;
            default:
                break;
        }
    }
}

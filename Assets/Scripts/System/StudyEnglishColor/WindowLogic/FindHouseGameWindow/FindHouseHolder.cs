﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using WCBG.ToolsForUnity.Extension;
using TableProto;

public class FindHouseHolder : MonoBehaviour, IEasyTimer
{

    public bool bTimerCallback
    {
        get { return this == null ? false : true; }
    }


    public RectTransform m_doorPoint;
    public Image m_houseImg;
    public Image m_bslImg;
    public Image m_textBg;
    public Text m_wordText;

    public bool isComplete
    {
        get;
        private set;
    }

    public string word
    {
        get;
        private set;
    }

    public Vector3 doorPos
    {
        get { return m_doorPoint.position; }
    }

    private System.Action<FindHouseHolder> clickCallback;


    void Awake()
    {
        m_doorPoint = transform.Find("doorPoint") as RectTransform;
        m_houseImg = transform.Find("houseImg").GetComponent<Image>();
        m_bslImg = transform.Find("bslImg").GetComponent<Image>();
        m_textBg = transform.Find("textBg").GetComponent<Image>();
        m_wordText = transform.Find("wordText").GetComponent<Text>();

        m_houseImg.GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public void Initialize(string wordText, string bslImg, string houseImg, Color color, System.Action<FindHouseHolder> clickCallback)
    {
        this.clickCallback = clickCallback;
        gameObject.SetActive(true);
        m_bslImg.gameObject.SetActive(false);

        word = wordText;

        m_wordText.color = color;
        m_wordText.text = wordText;
        m_bslImg.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.English_Colours, bslImg);
        m_houseImg.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.English_Colours, houseImg);

        isComplete = false;
        ActiveTextState(false);

    }

    public void ScaleIn()
    {
        transform.localScale = Vector3.zero;
        transform.DOScale(1, 0.5f);
    }

    public void PlayCompleteAnim(System.Action callback)
    {
        if (isComplete)
            return;
        isComplete = true;

        m_bslImg.gameObject.SetActive(true);
        m_bslImg.transform.localScale = Vector3.zero;
        m_bslImg.transform.DOScale(1, 0.3f);
        this.AddTimerEx(0.3f, () => {

            ActiveTextState(true, callback);
        });
    }

    void ActiveTextState(bool val, System.Action callback = null)
    {
        m_textBg.gameObject.SetActive(val);
        m_wordText.gameObject.SetActive(val);
        if (val)
        {
            StartCoroutine(Fade(callback));
        }
    }

    private void OnClick()
    {
        if (isComplete)
            return;
        if (clickCallback != null)
            clickCallback(this);
    }

    IEnumerator Fade(System.Action callback = null)
    {
        float a = 0;
        Color col = Color.white;
        while (a < 1)
        {
            col = m_textBg.color;
            col.a = a;
            m_textBg.color = col;
            col = m_wordText.color;
            col.a = a;
            m_wordText.color = col;

            a += Time.deltaTime * 0.5f;
            yield return null;
        }

        col = m_textBg.color;
        col.a = 1;
        m_textBg.color = col;
        col = m_wordText.color;
        col.a = 1;
        m_wordText.color = col;

        if (callback != null)
            callback();
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;
using DG.Tweening;


public class EnglishColorMainWindow : SingletonBaseWindow<EnglishColorMainWindow>
{

    #region ui property

    public Button _Btn_Return { get; set; }
    public Button _Btn_Game1 { get; set; }
    public Button _Btn_Game2 { get; set; }
    public Button _Btn_Game3 { get; set; }
    public Transform _BtnArr { get; set; }
    public Transform _BtnArr_Start { get; set; }
    public Transform _BtnArr_End { get; set; }
    public Transform _BlockPanel { get; set; }


    #endregion


    private bool isLockInput;

    private System.Action<EEnglishColorGameType> completeCallback;



    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);
        _BlockPanel.SetActive(false);
    }

    protected override void OnRefresh()
    {
        base.OnRefresh();

        StudyGameProgressInfo info = LocalDataManager.Instance.Load<StudyGameProgressInfo>(ELocalDataType.StudyEnglishColours);
        //info.gameProgress = 2;
        _Btn_Game2.transform.parent.SetActive(info.gameProgress > 0);
        _Btn_Game3.transform.parent.SetActive(info.gameProgress > 1);

        _BtnArr.position = _BtnArr_Start.position;

        this.AddTimerEx(1.5f, () =>
        {
            _BtnArr.DOMove(_BtnArr_End.position, 0.5f);
            this.PlayAudioEx(StudyAudioName.t_41001);
        });
    }

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
        _Btn_Game1.onClick.AddListener(OnBtnGame1Click);
        _Btn_Game2.onClick.AddListener(OnBtnGame2Click);
        _Btn_Game3.onClick.AddListener(OnBtnGame3Click);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
        _Btn_Game1.onClick.RemoveAllListeners();
        _Btn_Game2.onClick.RemoveAllListeners();
        _Btn_Game3.onClick.RemoveAllListeners();
    }
    


    #region uievent

    private void OnButtonReturnClick()
    {
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }


    private void OnBtnGame1Click()
    {
        if (isLockInput)
            return;
        isLockInput = true;
        _BlockPanel.SetActive(true);

        if (completeCallback != null)
        {
            completeCallback(EEnglishColorGameType.FillColor);
        }

        this.PlayAudioEx(StudyAudioName.t_41002);
    }

    private void OnBtnGame2Click()
    {
        if (isLockInput)
            return;
        isLockInput = true;
        _BlockPanel.SetActive(true);

        if (completeCallback != null)
        {
            completeCallback(EEnglishColorGameType.FillLetter);
        }
        this.PlayAudioEx(StudyAudioName.t_41002);
    }

    private void OnBtnGame3Click()
    {
        if (isLockInput)
            return;
        isLockInput = true;
        _BlockPanel.SetActive(true);

        if (completeCallback != null)
        {
            completeCallback(EEnglishColorGameType.FindHouse);
        }
        this.PlayAudioEx(StudyAudioName.t_41002);
    }

    #endregion



    public void SelectGame(System.Action<EEnglishColorGameType> callback)
    {
        completeCallback = callback;
        isLockInput = false;


    }





}

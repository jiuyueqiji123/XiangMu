﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class FillColorFlow : InitiativeBaseFlow
{



    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        List<EnglishColoursInfo> infoList = new List<EnglishColoursInfo>();

        uint index = 10001;
        while (true)
        {
            if (!TableProtoLoader.EnglishColoursInfoDict.ContainsKey(index))
            {
                break;
            }
            //RegisterPart(new FillColorPart(TableProtoLoader.EnglishColoursInfoDict[index]));
            infoList.Add(TableProtoLoader.EnglishColoursInfoDict[index]);
            index++;
        }

        while (infoList.Count > 0)
        {
            int rd = Random.Range(0, infoList.Count);
            EnglishColoursInfo info = infoList[rd];
            infoList.RemoveAt(rd);
            RegisterPart(new FillColorPart(info));
        }

        WindowManager.Instance.OpenWindow(WinNames.FillColorGameWindow);
        WindowManager.Instance.OpenWindow(WinNames.EnglishLediAnimationPanel, EWindowLayerEnum.PopupLayer);

        MedalWindow.Instance.ShowXunzhang(0, false, machine.partCount);
        StartPart(0, true, OnGamePartCallback, OnGameAllPartCallback);
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();

    }

    protected void OnGamePartCallback(bool isComplete, params object[] paramArray)
    {
        //if (isComplete)
            //SaveGameProgress(EEnglishColorGameType.FillColor);
    }

    protected void OnGameAllPartCallback(bool isComplete)
    {
        if (isComplete)
            SaveGameProgress(EEnglishColorGameType.FillColor);
        OnFlowComplete(isComplete);
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        WindowManager.Instance.CloseWindow(WinNames.FillColorGameWindow);
    }

    protected void SaveGameProgress(EEnglishColorGameType gameProgress)
    {
        StudyGameProgressInfo info = LocalDataManager.Instance.Load<StudyGameProgressInfo>(ELocalDataType.StudyEnglishColours);

        if (info.gameProgress > (int)gameProgress)
            return;
        info.gameProgress = (int)gameProgress;
        LocalDataManager.Instance.Save(ELocalDataType.StudyEnglishColours, info);
    }



}

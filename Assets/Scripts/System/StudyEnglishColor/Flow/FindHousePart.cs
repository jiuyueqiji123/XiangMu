﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class FindHousePart : InitiativeBaseFlow
{

    private EnglishColoursInfo[] m_info;
    public FindHousePart(EnglishColoursInfo[] info)
    {
        m_info = info;
    }

    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        //Debug.LogError(m_info.word);

        FindHouseGameWindow.Instance.StartGame(m_info, (levelIndex, callback)=> {

            MedalWindow.Instance.ShowXunzhang(flowIndex * m_info.Length + levelIndex + 1, true, callback);

        }, (isComplete) =>
        {
            if (isComplete)
            {
                MedalWindow.Instance.ShowXunzhang((flowIndex + 1) * m_info.Length, true, () =>
                {
                    OnFlowComplete(isComplete);
                });
            }
            else
            {
                OnFlowComplete(isComplete);
            }
        });
    }


}

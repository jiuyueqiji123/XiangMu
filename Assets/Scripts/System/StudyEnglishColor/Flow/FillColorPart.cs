﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class FillColorPart : InitiativeBaseFlow
{

    private EnglishColoursInfo m_info;
    public FillColorPart(EnglishColoursInfo info)
    {
        m_info = info;
    }

    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        FillColorGameWindow.Instance.StartGame(m_info, (isComplete) =>
        {
            if (isComplete)
            {
                MedalWindow.Instance.ShowXunzhang(flowIndex + 1, true, () =>
                {
                    OnFlowComplete(isComplete);
                });
            }
            else
            {
                OnFlowComplete(isComplete);
            }
        });
    }
    





}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class FillLetterPart : InitiativeBaseFlow
{

    private EnglishColoursInfo m_info;
    public FillLetterPart(EnglishColoursInfo info)
    {
        m_info = info;
    }

    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        FillLetterGameWindow.Instance.StartGame(m_info, (isComplete) =>
        {
            if (isComplete)
            {
                MedalWindow.Instance.ShowXunzhang(flowIndex + 1, true, () =>
                {
                    OnFlowComplete(isComplete);
                });
            }
            else
            {
                OnFlowComplete(isComplete);
            }
        });
    }


}

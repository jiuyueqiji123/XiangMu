﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TableProto;

public class FindHouseFlow : InitiativeBaseFlow
{
    
    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        uint index = 30001;
        List<EnglishColoursInfo> infoList = new List<EnglishColoursInfo>();
        while (true)
        {
            if (!TableProtoLoader.EnglishColoursInfoDict.ContainsKey(index))
            {
                break;
            }
            infoList.Add(TableProtoLoader.EnglishColoursInfoDict[index]);
            index++;
        }

        int count = infoList.Count / 4;
        for (int i = 0; i < count; i++)
        {
            EnglishColoursInfo[] arr = new EnglishColoursInfo[4];
            for (int j = 0; j < arr.Length; j++)
            {
                if (infoList.Count == 0)
                    break;
                int rd = Random.Range(0, infoList.Count);
                arr[j] = infoList[rd];
                infoList.RemoveAt(rd);
            }

            RegisterPart(new FindHousePart(arr));
        }


        WindowManager.Instance.OpenWindow(WinNames.FindHouseGameWindow);
        WindowManager.Instance.OpenWindow(WinNames.EnglishLediAnimationPanel, EWindowLayerEnum.PopupLayer);

        MedalWindow.Instance.ShowXunzhang(0, false, count * 4);
        StartPart(0, true, null, OnGameAllPartCallback);
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();

    }

    protected void OnGamePartCallback(bool isComplete, params object[] paramArray)
    {

    }

    protected void OnGameAllPartCallback(bool isComplete)
    {
        if (isComplete)
            SaveGameProgress(EEnglishColorGameType.FindHouse);
        OnFlowComplete(isComplete);
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        WindowManager.Instance.CloseWindow(WinNames.FindHouseGameWindow);
    }
    protected void SaveGameProgress(EEnglishColorGameType gameProgress)
    {
        StudyGameProgressInfo info = LocalDataManager.Instance.Load<StudyGameProgressInfo>(ELocalDataType.StudyEnglishColours);

        if (info.gameProgress > (int)gameProgress)
            return;
        info.gameProgress = (int)gameProgress;
        LocalDataManager.Instance.Save(ELocalDataType.StudyEnglishColours, info);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillLetterFlow : InitiativeBaseFlow
{
    
    protected override void OnFlowStart()
    {
        base.OnFlowStart();

        uint index = 20001;
        while (true)
        {
            if (!TableProtoLoader.EnglishColoursInfoDict.ContainsKey(index))
            {
                break;
            }
            RegisterPart(new FillLetterPart(TableProtoLoader.EnglishColoursInfoDict[index]));
            index++;
        }

        WindowManager.Instance.OpenWindow(WinNames.FillLetterGameWindow);
        WindowManager.Instance.OpenWindow(WinNames.EnglishLediAnimationPanel, EWindowLayerEnum.PopupLayer);

        MedalWindow.Instance.ShowXunzhang(0, false, machine.partCount);
        StartPart(0, true, null, OnGameAllPartCallback);
    }

    protected override void OnFlowUpdate()
    {
        base.OnFlowUpdate();

    }

    protected void OnGamePartCallback(bool isComplete, params object[] paramArray)
    {

    }

    protected void OnGameAllPartCallback(bool isComplete)
    {
        if (isComplete)
            SaveGameProgress(EEnglishColorGameType.FillLetter);
        OnFlowComplete(isComplete);
    }

    protected override void OnFlowExit()
    {
        base.OnFlowExit();

        WindowManager.Instance.CloseWindow(WinNames.FillLetterGameWindow);
    }

    protected void SaveGameProgress(EEnglishColorGameType gameProgress)
    {
        StudyGameProgressInfo info = LocalDataManager.Instance.Load<StudyGameProgressInfo>(ELocalDataType.StudyEnglishColours);

        if (info.gameProgress > (int)gameProgress)
            return;
        info.gameProgress = (int)gameProgress;
        LocalDataManager.Instance.Save(ELocalDataType.StudyEnglishColours, info);
    }

}

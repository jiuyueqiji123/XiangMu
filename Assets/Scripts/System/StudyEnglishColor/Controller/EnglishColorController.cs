﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnglishColorController : IEasyLoading,IEasyAudio
{



    protected StudyGameMachine gameMachine = new StudyGameMachine();



    private bool isGamePlaying;
    private System.Action<bool> completeCallback;


    public void OnUpdate()
    {
        if (gameMachine != null)
            gameMachine.UpdateGamePart();
        if (Input.GetKeyDown(KeyCode.N)) OnGamePartCallback(true);
    }


    public void StartGame(EEnglishColorGameType gameType, System.Action<bool> callback)
    {
        completeCallback = callback;

        WindowManager.Instance.OpenWindow(WinNames.MedalPanel, EWindowLayerEnum.PopupLayer);
        WindowManager.Instance.OpenWindow(WinNames.EnglishLediAnimationPanel, EWindowLayerEnum.PopupLayer);

        gameMachine.RegisterGamePart(new FillColorFlow());
        gameMachine.RegisterGamePart(new FillLetterFlow());
        gameMachine.RegisterGamePart(new FindHouseFlow());

        gameMachine.StartGamePart((int)(gameType - 1), OnGameAllPartCallback, OnGamePartCallback);
    }

    protected void OnGamePartCallback(bool isComplete, params object[] paramArray)
    {
        Debug.Log("iscomplete: " + isComplete);
        EnglishLediAnimationWindow.Instance.PlayLediWinAnimation(this.GetAudioNameEx(StudyAudioName.t_32009), this.GetAudioNameEx(StudyAudioName.t_32005), false, false, () =>
        {
            TimerManager.Instance.AddTimer(2f, () => {
                WindowManager.Instance.CloseWindow(WinNames.EnglishLediAnimationPanel);
                this.StartLoadingAndEndEx(() =>
                {
                    gameMachine.StopCurrentGamePart();
                    gameMachine.StartNextPart();
                });
            });
        });
    }

    protected void OnGameAllPartCallback(bool isComplete)
    {
        EnglishLediAnimationWindow.Instance.PlayLediWinAnimation(this.GetAudioNameEx(StudyAudioName.t_32009), this.GetAudioNameEx(StudyAudioName.t_32005), false, false, () =>
        {
            TimerManager.Instance.AddTimer(2f, () =>
            {
                WindowManager.Instance.CloseWindow(WinNames.EnglishLediAnimationPanel);
                if (completeCallback != null)
                {
                    completeCallback(isComplete);
                }
            });
        });
    }


    public void ExitGame()
    {
        gameMachine.StopAllGamePart();
        WindowManager.Instance.CloseWindow(WinNames.MedalPanel);
    }

    protected void SaveGameProgress(EEnglishColorGameType gameProgress)
    {
        StudyGameProgressInfo info = LocalDataManager.Instance.Load<StudyGameProgressInfo>(ELocalDataType.StudyEnglishColours);

        if (info.gameProgress > (int)gameProgress)
            return;
        info.gameProgress = (int)gameProgress;
        LocalDataManager.Instance.Save(ELocalDataType.StudyEnglishColours, info);
    }

}

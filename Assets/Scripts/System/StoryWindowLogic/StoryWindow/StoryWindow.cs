﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;

public class StoryWindow : SingletonBaseWindow<StoryWindow>
{


    #region ui property
    
    public Button _Btn_Return { get; set; }

    #endregion

    private bool isLockInput;


    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        isLockInput = false;
    }

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
    }



    #region -------------------------- uievent

    private void OnButtonReturnClick()
    {
        //if (EasyLoadingEx.IsLoading)
        //    return;
        if (isLockInput)
            return;
        isLockInput = true;

        //MaoXianManager.Instance.ExitMaoXian();
        MaoXianManager.Instance.GotoMainState();
    }

    #endregion


}

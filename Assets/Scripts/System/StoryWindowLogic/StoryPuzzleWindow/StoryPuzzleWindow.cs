﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;

public class StoryPuzzleWindow : SingletonBaseWindow<StoryPuzzleWindow>
{


    #region ui property

    public Transform _BtnArray { get; set; }
    public Transform _EffectPos { get; set; }

    #endregion


    private const string audioBasePath = "story_danmai/story_danmai_audio/";
    private const string audioTrue = "402010302";
    private const string audioFalse = "402010303";
    private const string audioComplete = "402010110";

    private const string effectComplete = "Effect/502002032";

    private System.Action<bool> completeCallback;

    private PuzzleHolder[] m_puzzleArray;
    private bool isPlayAudioFalse;
    
    
    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);


        m_puzzleArray = _BtnArray.GetComponentsInChildren<PuzzleHolder>();
        foreach (PuzzleHolder holder in m_puzzleArray)
        {
            holder.OnDragTrue += Holder_OnDragTrue;
            holder.OnDragFalse += Holder_OnDragFalse;
        }
        SetRandomPos();

    }

    protected override void OnClose()
    {
        base.OnClose();

        foreach (PuzzleHolder holder in m_puzzleArray)
        {
            holder.OnDragTrue -= Holder_OnDragTrue;
            holder.OnDragFalse -= Holder_OnDragFalse;
        }
        completeCallback = null;
        isPlayAudioFalse = false;
    }


    private void Holder_OnDragFalse()
    {
        PlayAudioFalse();
    }

    private void Holder_OnDragTrue()
    {
        bool isComplete = true;
        foreach (PuzzleHolder holder in m_puzzleArray)
        {
            if (!holder.IsComplete)
            {
                isComplete = false;
                break;
            }
        }
        if (isComplete)
        {
            PlayAudioComplete();
            this.PlayEffectEx(effectComplete, _EffectPos, 5, EGameLayerMask.UI);
            this.AddTimerEx(3f, () =>
            {
                if (completeCallback != null)
                    completeCallback(true);
            });
        }
        else
        {
            PlayAudioTrue();
        }

    }


    #region -------------------------- uievent

    private void OnButtonReturnClick()
    {

    }

    #endregion






    public void StartGame(System.Action<bool> callback)
    {
        completeCallback = callback;
        isPlayAudioFalse = false;
    }



    private void SetRandomPos()
    {

        for (int i = 0; i < m_puzzleArray.Length; i++)
        {
            m_puzzleArray[i].transform.SetSiblingIndex(Random.Range(0, m_puzzleArray.Length));
        }

    }


    private void PlayAudioTrue()
    {
        this.PlayAudioEx(audioBasePath + audioTrue);
    }
    private void PlayAudioFalse()
    {
        if (isPlayAudioFalse)
            return;

        isPlayAudioFalse = true;
        GameAudioSource audio = this.PlayAudioEx(audioBasePath + audioFalse);
        this.AddTimerEx(audio.Length, () => {
            isPlayAudioFalse = false;
        });
    }
    private void PlayAudioComplete()
    {
        this.PlayAudioEx(audioBasePath + audioComplete);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WCBG.ToolsForUnity.Tools;
using DG.Tweening;

public class PuzzleHolder : MonoBehaviour, IEasyEffect
{


    private const float CHECK_DISTANCE = 10;
    private const float FOLLOW_SPEED = 200;
    private const string EFFECT_PATH = "Effect/";

    public float MAX = 50;

    public Transform fixedTarget;   // 修补目标
    public Transform effectPoint;
    public string fixedEffect;

    private bool _IsComplete;
    public bool IsComplete { get { return _IsComplete; } }

    private bool isHover;
    private Transform m_puzzle;

    public event System.Action OnDragTrue;
    public event System.Action OnDragFalse;

    // Use this for initialization
    void Start() {

        ShowLinePuzzle();
        _IsComplete = false;
        m_puzzle = transform.GetChild(0);
        gameObject.AddComponentEx<UIInputHandler>().OnDown += PuzzleHolder_OnDown;
        gameObject.AddComponentEx<UIInputHandler>().OnUp += PuzzleHolder_OnUp;
    }

    // Update is called once per frame
    void Update()
    {
        if (isHover)
        {
            Vector3 to = FUGUITool.ScreenPointToUGUIPoint(WindowManager.Instance.canvas, Input.mousePosition);
            if (m_puzzle != null)
            {
                if (Vector3.Distance(m_puzzle.position, to) < 5f)
                {
                    m_puzzle.position = to;
                }
                else
                {
                    Vector3 dir = to - m_puzzle.position;
                    float dis = Vector3.Distance(m_puzzle.position, to);
                    m_puzzle.position += dir.normalized * Time.deltaTime * FOLLOW_SPEED * (1 + dis / MAX);
                }
                //if (Vector3.Distance(m_puzzle.position, fixedTarget.position) < CHECK_DISTANCE)
                //{
                //    MoveComplete();
                //    return;
                //}
            }
            if (Input.GetMouseButtonUp(0))
            {
                isHover = false;
                MoveBack();
            }
        }
    }

    private void PuzzleHolder_OnUp()
    {
        if (m_puzzle != null)
        {
            if (Vector3.Distance(m_puzzle.position, fixedTarget.position) < CHECK_DISTANCE)
            {
                MoveComplete();
                return;
            }
        }
        isHover = false;
        MoveBack();
    }

    private void PuzzleHolder_OnDown()
    {
        //Debug.LogError("PuzzleHolder_OnDown");
        if (_IsComplete)
            return;
        isHover = true;
        m_puzzle.DOKill();
        m_puzzle.SetParent(transform.parent.parent);
    }

    private void MoveComplete()
    {
        isHover = false;
        _IsComplete = true;
        gameObject.AddComponentEx<UIInputHandler>().OnDown -= PuzzleHolder_OnDown;
        gameObject.AddComponentEx<UIInputHandler>().OnUp -= PuzzleHolder_OnUp;
        GetComponent<Button>().enabled = false;

        float duration = 0.2f;
        m_puzzle.DOMove(fixedTarget.position, duration).SetEase(Ease.Linear);
        m_puzzle.DOScale(fixedTarget.localScale, duration).SetEase(Ease.Linear).OnComplete(()=> {

            m_puzzle.SetParent(transform);
            m_puzzle.SetActive(false);
            ShowPuzzle();
        });

        if (OnDragTrue != null)
            OnDragTrue();
    }

    private void ShowPuzzle()
    {
        if (fixedTarget != null && fixedTarget.childCount > 1)
        {
            fixedTarget.GetChild(0).SetActive(false);
            fixedTarget.GetChild(1).SetActive(true);

            this.PlayEffectEx(EFFECT_PATH + fixedEffect, effectPoint, 5, EGameLayerMask.UI);
        }
    }

    private void ShowLinePuzzle()
    {
        if (fixedTarget != null && fixedTarget.childCount > 1)
        {
            fixedTarget.GetChild(0).SetActive(true);
            fixedTarget.GetChild(1).SetActive(false);
        }
    }

    private void MoveBack()
    {
        if (m_puzzle != null)
        {
            m_puzzle.SetParent(transform);
            m_puzzle.DOLocalMove(Vector3.zero, 0.3f);
        }
        if (OnDragFalse != null)
            OnDragFalse();
    }



}

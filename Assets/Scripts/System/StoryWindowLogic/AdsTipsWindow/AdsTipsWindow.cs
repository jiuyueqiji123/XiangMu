﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;

public class AdsTipsWindow : SingletonBaseWindow<AdsTipsWindow>
{


    #region ui property
    
    public Button _Btn_Return { get; set; }

    #endregion




    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
        EventBus.Instance.AddEventHandler<string>(EventID.ADS_DONE_CALLBACK, OnAdsCallback);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
        EventBus.Instance.RemoveEventHandler<string>(EventID.ADS_DONE_CALLBACK, OnAdsCallback);
    }


    private void OnAdsCallback(string adsName)
    {

    }


    #region -------------------------- uievent

    private void OnButtonReturnClick()
    {

    }
    

    #endregion


}

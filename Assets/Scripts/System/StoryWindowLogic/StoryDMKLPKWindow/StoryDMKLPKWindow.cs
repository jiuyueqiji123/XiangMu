﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;

public class StoryDMKLPKWindow : SingletonBaseWindow<StoryDMKLPKWindow>
{


    private const float UPDATE_SPEED = 0.3f;


    #region ui property

    public Image _Ledi_Bar { get; set; }
    public Image _Konglong_Bar { get; set; }

    #endregion


    private bool isUpdateLediBar;
    private bool isUpdateKonglongBar;

    private float lediBarValue;
    private float konglongBarValue;

    private float lediBarCurrentValue;
    private float konglongCurrentValue;

    private float lediImgWidth;
    private float longlongImgWidth;


    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        lediBarValue = konglongBarValue = lediBarCurrentValue = konglongCurrentValue = 1;

        lediImgWidth = _Ledi_Bar.rectTransform.sizeDelta.x;
        longlongImgWidth = _Konglong_Bar.rectTransform.sizeDelta.x;
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();

        //if (Input.GetMouseButtonDown(0))
        //{
        //    UpdateLediBar(lediBarCurrentValue - 1 / 3f);
        //}
        //if (Input.GetMouseButtonDown(1))
        //{
        //    UpdateKonglongBar(konglongCurrentValue - 1 / 3f);
        //}

        if (isUpdateLediBar)
        {
            float speed = lediBarCurrentValue - lediBarValue > 0 ? (lediBarCurrentValue - lediBarValue) * 10 : 1;
            lediBarCurrentValue -= Time.deltaTime * UPDATE_SPEED * speed;
            if (lediBarCurrentValue <= lediBarValue)
            {
                isUpdateLediBar = false;
                lediBarCurrentValue = lediBarValue;
            }
            _Ledi_Bar.rectTransform.sizeDelta = new Vector2(lediImgWidth * lediBarCurrentValue, _Ledi_Bar.rectTransform.sizeDelta.y);
        }
        if (isUpdateKonglongBar)
        {
            float speed = konglongCurrentValue - konglongBarValue > 0 ? (konglongCurrentValue - konglongBarValue) * 10 : 1;
            konglongCurrentValue -= Time.deltaTime * UPDATE_SPEED * speed;
            if (konglongCurrentValue <= konglongBarValue)
            {
                isUpdateKonglongBar = false;
                konglongCurrentValue = konglongBarValue;
            }
            _Konglong_Bar.rectTransform.sizeDelta = new Vector2(longlongImgWidth * konglongCurrentValue, _Konglong_Bar.rectTransform.sizeDelta.y);
        }
    }

    protected override void OnClose()
    {
        base.OnClose();
        
    }


    #region -------------------------- uievent

    //private void OnButtonReturnClick()
    //{

    //}

    #endregion

    public void UpdateLediBar(float val)
    {
        lediBarValue = val;
        isUpdateLediBar = true;
    }

    public void UpdateKonglongBar(float val)
    {
        konglongBarValue = val;
        isUpdateKonglongBar = true;
    }






}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;

public class StoryMicroWindow : SingletonBaseWindow<StoryMicroWindow>
{


    #region ui property
    
    public Button _Btn_Audio { get; set; }
    public Transform _AudioAnimation { get; set; }

    #endregion

#if UNITY_EDITOR
    private const float TALK_TIME = 0.5f;
#else
    private const float TALK_TIME = 5f;
#endif


    System.Action<bool> completeCallback;
    private MicAnimationCompnent m_micComp;

    private float addtime;
    private bool isTalk;

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);
        
        m_micComp = _AudioAnimation.GetComponent<MicAnimationCompnent>();
        addtime = 0;
        isTalk = false;
        MicroManager.Instance.StartRecord();
    }

    protected override void OnUpdate()
    {
        if (!isTalk)
            return;

        addtime += Time.deltaTime;
        if (addtime > TALK_TIME)
        {
            addtime = 0;
            isTalk = false;
            if (completeCallback != null)
                completeCallback(true);
        }
        if (m_micComp != null)
        {
            m_micComp.volume = MicroManager.Instance.Volume;
        }
    }

    protected override void OnClose()
    {
        base.OnClose();

        MicroManager.Instance.StopRecord();
    }


#region -------------------------- uievent

    private void OnButtonReturnClick()
    {
        if (completeCallback != null)
        {
            completeCallback(false);
        }
    }

#endregion

    public void AddEvent(System.Action<bool> callback)
    {
        completeCallback = callback;
        isTalk = true;
        addtime = 0;
    }


}

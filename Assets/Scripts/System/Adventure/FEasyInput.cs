﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EEasyInputType
{
    OnDown,
    OnHover,
    OnUp,
}
public class FEasyInput : MonoSingleton<FEasyInput>
{

    private event System.Action OnDown;
    private event System.Action OnHover;
    private event System.Action OnUp;


    private void Update()
    {
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (OnDown != null)
                    OnDown();
            }
            if (Input.GetMouseButton(0))
            {
                if (OnHover != null)
                    OnHover();
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (OnUp != null)
                    OnUp();
            }
        }
        else
        {
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    if (OnDown != null)
                        OnDown();
                }
                if (Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    if (OnHover != null)
                        OnHover();
                }
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    if (OnUp != null)
                        OnUp();
                }
            }
        }
    }


    public static void AddEvent(EEasyInputType type, System.Action action)
    {
        if (action == null)
            return;
        switch (type)
        {
            case EEasyInputType.OnDown:
                FEasyInput.Instance.OnDown += action;
                break;
            case EEasyInputType.OnHover:
                FEasyInput.Instance.OnHover += action;
                break;
            case EEasyInputType.OnUp:
                FEasyInput.Instance.OnUp += action;
                break;
        }
    }

    public static void RemoveEvent(EEasyInputType type, System.Action action)
    {
        if (action == null || !FEasyInput.HasInstance())
            return;

        switch (type)
        {
            case EEasyInputType.OnDown:
                FEasyInput.Instance.OnDown -= action;
                break;
            case EEasyInputType.OnHover:
                FEasyInput.Instance.OnHover -= action;
                break;
            case EEasyInputType.OnUp:
                FEasyInput.Instance.OnUp -= action;
                break;
        }
    }



}

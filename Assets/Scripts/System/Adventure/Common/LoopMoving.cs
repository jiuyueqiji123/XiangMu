﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopMoving : MonoBehaviour {


    public bool playOnStart;

    public Transform target1;
    public Transform target2;

    public float onceTime = 5;

    private float time1;
    private float time2;

    private bool isEnable;

    private Vector3 startPoint;
    private Vector3 moveRate;

	// Use this for initialization
	void Start () {
        if (playOnStart)
            isEnable = true;

        startPoint = target2.position;
        moveRate = (target1.position - target2.position) * 2;
        time1 = onceTime / 2;
	}
	
	// Update is called once per frame
	void Update () {

        if (isEnable)
        {
            time1 += Time.deltaTime;
            time2 += Time.deltaTime;

            if (time1 > onceTime)
            {
                time1 = time2 - onceTime / 2;
            }
            if (time2 > onceTime)
            {
                time2 = time1 - onceTime / 2;
            }

            target1.position = startPoint + moveRate * time1 / onceTime;
            target2.position = startPoint + moveRate * time2 / onceTime;
        }
	}

    public void SetEnable(bool val)
    {
        isEnable = val;
    }


}

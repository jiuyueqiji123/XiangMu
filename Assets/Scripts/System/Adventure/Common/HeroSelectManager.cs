﻿using UnityEngine;
using System.Collections;
using TableProto;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class HeroSelectManager : MonoSingleton<HeroSelectManager> , IEasyTimer
{

    public bool bTimerCallback
    {
        get { return this == null ? false : true; }
    }

    private const string ASSET_PATH = "common_asset/common_asset_story/HeroSelectRoot";

    public enum EHeroType
    {
        Mili = 0,
        Xiaoai = 1,
        Duoduo = 2,
        Xiaoqing = 3,
        Baojinzhang = 4,
    }


    /// <summary>  
    /// 第一次按下的位置  
    /// </summary>  
    private Vector2 first = Vector2.zero;
    /// <summary>  
    /// 鼠标的拖拽位置（第二次的位置）  
    /// </summary>  
    private Vector2 second = Vector2.zero;

    private bool isDown = false;

    private float timer;
    public float offsetTime = 0.1f;

    public int count = 0;

    private bool choicedown = false;

    private GameObject xiaoai;
    private GameObject xiaoqing;
    private GameObject duoduo;
    private GameObject mili;
    private GameObject ledi;

    public Transform heroParent;

    public Transform m_effect;
    public Transform m_lookpoint;
    public Transform m_milipoint;
    public Transform m_xiaoaipos;
    public Transform m_xiaoqingpos;
    public Transform m_milipos;
    public Transform m_duoduopos;
    public Transform m_ledipos;
    public Transform m_zhuanpan;

    public Button m_button;


    private bool isDrag=false;
    private bool isLeftClick = false;
    private bool isRightClick = false;
    private bool isRotate = false;
    private bool isInit;

    private Action action;
    private int heroID = 0;//0.米莉，1.小爱, 2.多多，3.小青，4.包警长


    /// <summary>  
    /// 旋转的角度  
    /// </summary>  
    private float angle =72f;

    private GameAudioSource m_feixiaAudio;

    

    private void Update()
    {
        xiaoai.transform.LookAt(m_lookpoint);
        m_milipos.LookAt(m_milipoint);
        xiaoqing.transform.LookAt(m_lookpoint);
        ledi.transform.LookAt(m_lookpoint);
        duoduo.transform.LookAt(m_lookpoint);
    }

    public void Initialize()
    {
        LoadObj();

        Input.multiTouchEnabled = false;
        choicedown = false;
        m_effect.SetActive(false);

        AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401010201");
        //info = TableProtoLoader.TimelineInfoDict[(uint)TimelineManager.juqingID];
        xiaoai = GameObject.Instantiate(ResourceManager.Instance.GetResource(Itempathinfo.xiaoaipath, null, enResourceType.Prefab, false, false).m_content as GameObject, m_xiaoaipos);
        xiaoqing = GameObject.Instantiate(ResourceManager.Instance.GetResource(Itempathinfo.xiaoqingpath, null, enResourceType.Prefab, false, false).m_content as GameObject, m_xiaoqingpos);
        mili = GameObject.Instantiate(ResourceManager.Instance.GetResource(Itempathinfo.milipath, null, enResourceType.Prefab, false, false).m_content as GameObject, m_milipos);
        ledi = GameObject.Instantiate(ResourceManager.Instance.GetResource(Itempathinfo.ledipath, null, enResourceType.Prefab, false, false).m_content as GameObject, m_ledipos);
        duoduo = GameObject.Instantiate(ResourceManager.Instance.GetResource(Itempathinfo.duoduopath, null, enResourceType.Prefab, false, false).m_content as GameObject, m_duoduopos);

        xiaoai.transform.localPosition = Vector3.zero;
        xiaoqing.transform.localPosition = Vector3.zero;
        mili.transform.localPosition = Vector3.zero;
        ledi.transform.localPosition = Vector3.zero;
        duoduo.transform.localPosition = Vector3.zero;

        OpenAllAnimation();
    }

    private void LoadObj()
    {
        GameObject go = Res.LoadObj(ASSET_PATH);
        if (go == null)
            return;
        heroParent = go.transform;
        heroParent.SetParent(transform);

        m_effect = heroParent.FindChildEx("502002007");
        m_lookpoint = heroParent.Find("ChoiceHero/LookPoint");
        m_milipoint = heroParent.Find("ChoiceHero/milipoint");

        m_xiaoaipos = heroParent.Find("ChoiceHero/zhuanpan/xiaoaipos");
        m_xiaoqingpos = heroParent.Find("ChoiceHero/zhuanpan/xiaoqingpos");
        m_milipos = heroParent.Find("ChoiceHero/zhuanpan/milipos");
        m_duoduopos = heroParent.Find("ChoiceHero/zhuanpan/duoduopos");
        m_ledipos = heroParent.Find("ChoiceHero/zhuanpan/ledipos");

        m_zhuanpan = heroParent.Find("ChoiceHero/zhuanpan");

        m_button = heroParent.Find("ChoiceHero/Button").GetComponent<Button>();
        m_button.onClick.AddListener(OnChoiceHeroDown);

    }

    public void RequstHeroSelect(EHeroType type, Action callback)
    {
        action = callback;
        heroID = (int)type;
    }



    void OnGUI()
    {
        if (choicedown)
        {
            return;
        }
        if (isLeftClick || isRightClick)
        {
            return;
        }
        if (isRotate)
        {
            return;
        }
        if (Event.current.type == EventType.MouseDown)
        {
            //记录鼠标按下的位置 　　  
            first = Event.current.mousePosition;


        }
        if (Event.current.type == EventType.MouseDrag)
        {
            timer += Time.deltaTime;
            second = Event.current.mousePosition;
            //记录鼠标拖动的位置 　　
            if (isDrag == false)
            {
                if (timer > offsetTime)
                {


                    if (second.x < first.x - 10)
                    {
                        isDrag = true;
                        //拖动的位置的x坐标比按下的位置的x坐标小时,响应向左事件 　　  
                        //zhuanpan.transform.Rotate(Vector3.up, angle);
                        isRotate = true;
                        m_zhuanpan.DOLocalRotate(new Vector3(0f, angle, 0f), 0.3f, RotateMode.LocalAxisAdd).OnComplete(this.SetIsDrag);

                        count++;
                        if (count > 4)
                            count = 0;
                        PlayHeroSound();
                        AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "601010402");
                    }
                    if (second.x > first.x + 10)
                    {
                        isDrag = true;
                        //拖动的位置的x坐标比按下的位置的x坐标大时,响应向右事件 　　  
                        //zhuanpan.transform.Rotate(Vector3.down, angle);
                        isRotate = true;
                        m_zhuanpan.DOLocalRotate(new Vector3(0f, -angle, 0f), 0.3f, RotateMode.LocalAxisAdd).OnComplete(this.SetIsDrag);
                        count--;
                        if (count < 0)
                            count = 4;
                        PlayHeroSound();
                        AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "601010402");
                    }
                    first = second;
                    timer = 0;
                }
            }
        }
    }

    public void OnChoiceHeroDown() {
        Input.multiTouchEnabled = false;
        OpenAllAnimation();
        AudioManager.Instance.StopAllSound();
        AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "601010403");
        if (count == heroID)
        {
            m_button.gameObject.SetActive(false);
            choicedown = true;
            m_effect.SetActive(true);

            if (m_feixiaAudio != null)
            {
                m_feixiaAudio.Stop();
                m_feixiaAudio = null;
            }
            GameAudioSource audio = null;
            switch (count)
            {
                case 2:
                    //Debug.Log("我是多多，我已经准备好创造奇迹了！");
                    audio = AudioManager.Instance.PlaySound("story_danmai/story_danmai_audio/402030601");
                    break;
            }
            this.AddTimerEx(audio ==  null? 0 : audio.Length, () => {

                action();
            });
        }
        else
        {
            AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401010208");
        }
    }


    void PlayHeroSound()
    {
        CancelInvoke();
        AudioManager.Instance.StopAllSound();
        switch (count)
        {
            case 0:
                Debug.Log("我是米莉，来自外太空的明星！");
                OpenAllAnimation();
                m_feixiaAudio = AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401010203");
                mili.transform.Find("114001000@Skin").GetComponent<Animator>().CrossFade("fly_as", 0.3f);
                Invoke("OpenAllAnimation", 2.7f);
                break;
            case 1:
                Debug.Log("我是小爱，我就是救援飞侠！");
                OpenAllAnimation();
                xiaoai.transform.Find("102001000@skin").GetComponent<Animator>().CrossFade("fly_as", 0.3f);
                m_feixiaAudio = AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401010202");
                Invoke("OpenAllAnimation", 2.7f);
                break;
            case 2:
                Debug.Log("我是多多，我已经准备好创造奇迹了！");
                OpenAllAnimation();
                duoduo.transform.Find("103001000@skin").GetComponent<Animator>().CrossFade("fly_as", 0.3f);
                m_feixiaAudio = AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401010204");
                Invoke("OpenAllAnimation", 3.5f);
                break;
            case 3:
                Debug.Log("我是小青，听说你需要一个潜水天才！");
                OpenAllAnimation();
                xiaoqing.transform.Find("105001000@skin").GetComponent<Animator>().CrossFade("fly_as", 0.3f);
                m_feixiaAudio = AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401010205");
                Invoke("OpenAllAnimation", 3.0f);
                break;
            case 4:
                Debug.Log("我是包警长，不论什么问题，都在我掌控！");
                OpenAllAnimation();
                ledi.transform.Find("101001000@skin").GetComponent<Animator>().CrossFade("fly_as", 0.3f);
                m_feixiaAudio = AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401010133");
                Invoke("OpenAllAnimation", 3.2f);
                break;
        }

        this.AddTimerEx(m_feixiaAudio.Length, () => {

            m_feixiaAudio = null;
        });

    }
   


    public void OnButtonRightClick() {
        if (choicedown)
        {
            return;
        }
        if (isRightClick)
        {
            return;
        }
        if(isRotate) {
            return;
        }
     
            isRightClick = true;
        isRotate = true;
        m_zhuanpan.DOLocalRotate(new Vector3(0f, -angle, 0f), 0.3f, RotateMode.LocalAxisAdd).OnComplete(this.SetIsRightClick);
            count--;
            if (count < 0)
                count = 4;
            PlayHeroSound();
            AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "601010402");
        
    }

   public void OnButtonLeftClick() {
        if (choicedown)
        {
            return;
        }
        if (isLeftClick)
        {
            return;
        }
        if (isRotate)
        {
            return;
        }
        isLeftClick = true;
        isRotate = true;
        m_zhuanpan.DOLocalRotate(new Vector3(0f, angle, 0f), 0.3f, RotateMode.LocalAxisAdd).OnComplete(this.SetIsLeftClick);
            count++;
            if (count > 4)
                count = 0;
            PlayHeroSound();
            AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "601010402");
        
    }

    private void StopAllAnimation() {
        xiaoai.transform.Find("102001000@skin").GetComponent<Animator>().CrossFade("fly_a", 0.3f);
        duoduo.transform.Find("103001000@skin").GetComponent<Animator>().CrossFade("fly_a", 0.3f);
        xiaoqing.transform.Find("105001000@skin").GetComponent<Animator>().CrossFade("fly_a", 0.3f);
        ledi.transform.Find("101001000@skin").GetComponent<Animator>().CrossFade("fly_at", 0.3f);
        mili.transform.Find("114001000@Skin").GetComponent<Animator>().CrossFade("fly_a", 0.3f);

    }

    private void OpenAllAnimation() {
        xiaoai.transform.Find("102001000@skin").GetComponent<Animator>().CrossFade("fly_at", 0.3f);
        xiaoqing.transform.Find("105001000@skin").GetComponent<Animator>().CrossFade("fly_at", 0.3f);
        ledi.transform.Find("101001000@skin").GetComponent<Animator>().CrossFade("fly_at", 0.3f);
        duoduo.transform.Find("103001000@skin").GetComponent<Animator>().CrossFade("fly_at", 0.3f);
        mili.transform.Find("114001000@Skin").GetComponent<Animator>().CrossFade("fly_a", 0.3f);
    }

    private void SetIsDrag() {
        isDrag = false;
        isRotate = false;
    }

    private void SetIsRightClick() {
        isRightClick = false;
        isRotate = false;
    }

    private void SetIsLeftClick() {
        isLeftClick = false;
        isRotate = false;
    }




    public void Dispose()
    {
        DestroyInstance();
    }






}


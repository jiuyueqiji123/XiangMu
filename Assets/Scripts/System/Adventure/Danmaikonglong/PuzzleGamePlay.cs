﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleGamePlay : ReceiverBaseMono, IEasyLoading
{

    //public string nextTimeline;


    public string[] hideObjNameArray;

    protected override void Initialize()
    {
        SetPuzzleGameCameraActive(false);
    }

    protected override void OnGameStart()
    {
        Debug.Log("-----------------   Start puzzle game!");

        this.StartLoadingAndEndEx(() =>
        {
            SetPuzzleGameCameraActive(true);
            WindowManager.Instance.OpenWindow(WinNames.StoryPuzzlePanel);
            HideSceneObj();
            StoryPuzzleWindow.Instance.StartGame(OnPuzzleGameComplete);
        });
    }

    // 完成拼图游戏
    private void OnPuzzleGameComplete(bool isComplete)
    {
        Debug.Log("-----------------   OnPuzzleGameComplete! result:" + isComplete);

        if (isComplete)
        {
            this.StartLoadingAndEndEx(() =>
            {
                WindowManager.Instance.CloseWindow(WinNames.StoryPuzzlePanel);
                //FTimelineDispatch.DestroyTimeline(TimelineID);
                ResumeTimeline();
                SetPuzzleGameCameraActive(false);
                ShowSceneObj();
            });
        }
    }

    private void ShowSceneObj()
    {
        GameObject go = FTimelineDispatch.GetTimelineObj(TimelineID);
        DynamicShadowProjector.DrawTargetObject dt = null;
        if (go != null && hideObjNameArray != null)
        {
            Transform target = go.transform;
            foreach (string name in hideObjNameArray)
            {
                Transform t = target.Find(name);
                if (t != null)
                {
                    t.SetActive(true);
                    DynamicShadowProjector.DrawTargetObject _dt = t.GetComponent<DynamicShadowProjector.DrawTargetObject>();
                    if (_dt != null)
                        dt = _dt;
                }
            }
        }
        if (dt != null)
        {
            this.AddTimerEx(0.1f, () => {
                dt.RefreshShaowProjecter();
            });
        }
    }

    private void HideSceneObj()
    {
        GameObject go = FTimelineDispatch.GetTimelineObj(TimelineID);
        if (go != null && hideObjNameArray != null)
        {
            Transform target = go.transform;
            
            foreach (string name in hideObjNameArray)
            {
                Transform t = target.Find(name);
                if (t != null) t.SetActive(false);

            }
        }
    }

    private void SetPuzzleGameCameraActive(bool value)
    {
        Camera cam = transform.GetComponentInChildren<Camera>(true);
        if (cam != null)
        {
            cam.gameObject.SetActive(value);
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        WindowManager.Instance.CloseWindow(WinNames.StoryPuzzlePanel);
    }



}

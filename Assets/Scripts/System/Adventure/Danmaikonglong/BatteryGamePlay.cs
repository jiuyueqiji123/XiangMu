﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using WCBG.ToolsForUnity.Tools;

public class BatteryGamePlay : ReceiverBaseMono, IEasyLoading, IEasyEffect
{

    private const float CHECK_DISTANCE = 0.5f;

    private const string audioBasePath = "story_danmai/story_danmai_audio/";
    private const string audioStart = "402030405";
    private const string audioComplete = "402010110";
    private const string audioBattery = "602030301"; // 放电池音效
    private const string audioCici = "602030302"; // 电流通电，滋滋

    private const string BATTERY_EFFECT = "Effect/502002033";

    public string konglongName;
    public string dianchiName;

    public string[] hideObjNameArray;

    public Transform battery;


    private Vector3 m_batteryStartPos;
    private Vector3 m_batteryOffset;

    private bool isHover;
    private bool isComplete;

    private Camera cam;

    private Transform _BatteryParent;
    private Transform BatteryParent
    {
        get
        {
            if (_BatteryParent == null)
            {
                GameObject go = FTimelineDispatch.GetTimelineObj(TimelineID);
                if (go != null)
                {
                    Transform kl = go.transform.FindChildEx(konglongName);
                    if (kl != null)
                    {
                        _BatteryParent = kl.FindChildEx(dianchiName);
                    }
                }
            }
            return _BatteryParent;
        }
    }

    void Awake()
    {
        cam = transform.GetComponentInChildren<Camera>(true);
    }

    void Start()
    {
        transform.SetAllChildActive(false);

        m_batteryStartPos = battery.position;

        FEasyInput.AddEvent(EEasyInputType.OnDown, FEasyInput_OnDown);
        FEasyInput.AddEvent(EEasyInputType.OnHover, FEasyInput_OnHover);
        FEasyInput.AddEvent(EEasyInputType.OnUp, FEasyInput_OnUp);
    }

    private void FEasyInput_OnDown()
    {
        Transform target = FTools.GetRaycastHitTarget(cam, Input.mousePosition, 999999);
        if (target != null && target == battery)
        {
            isHover = true;
            battery.SetColliderActive<BoxCollider>(false);

            Vector3 outPoint = Vector3.zero;
            if (FTools.GetRaycastHitPoint(cam, Input.mousePosition, 999999, out outPoint))
            {
                m_batteryOffset = battery.position - outPoint;
            }
            else
            {
                m_batteryOffset = outPoint;
            }
        }
    }

    private void FEasyInput_OnHover()
    {
        if (!isHover || isComplete)
            return;
        Vector3 outPoint = Vector3.zero;
        if (FTools.GetRaycastHitPoint(cam, Input.mousePosition, 999999, out outPoint))
        {
            battery.position = outPoint + m_batteryOffset;
        }
    }

    private void FEasyInput_OnUp()
    {
        isHover = false;
        if (BatteryParent != null)
        {
            if (Vector3.Distance(battery.position, BatteryParent.position) < CHECK_DISTANCE)
            {
                isComplete = true;
                battery.SetParent(BatteryParent);
                battery.DOLocalMove(Vector3.zero, 0.5f);
                battery.DOLocalRotate(Vector3.zero, 0.5f);


                GameAudioSource audio = PlayAudio(audioBattery);

                this.AddTimerEx(audio.Length, () =>
                {

                    GameAudioSource audio2 = PlayAudio(audioCici);
                    this.PlayEffectEx(BATTERY_EFFECT, battery, 5);

                    this.AddTimerEx(audio2.Length + 1, () =>
                    {
                        this.AddTimerEx(1f, () =>
                        {
                            PlayAudio(audioComplete);
                        });
                        this.AddTimerEx(3f, OnGameComplete);
                    });
                });
            }
        }
        if (!isComplete)
        {
            battery.DOMove(m_batteryStartPos, 0.3f);
            battery.SetColliderActive<BoxCollider>(true);
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        FEasyInput.RemoveEvent(EEasyInputType.OnDown, FEasyInput_OnDown);
        FEasyInput.RemoveEvent(EEasyInputType.OnHover, FEasyInput_OnHover);
        FEasyInput.RemoveEvent(EEasyInputType.OnUp, FEasyInput_OnUp);
    }

    protected override void Initialize()
    {
        SetCameraActive(false);
    }

    protected override void OnGameStart()
    {
        Debug.Log("-----------------   Start battery game!");

        this.StartLoadingAndEndEx(() =>
        {
            transform.SetAllChildActive(true);
            SetCameraActive(true);
            HideSceneObj();

            PlayAudio(audioStart);
        });
    }

    // 完成拼图游戏
    private void OnGameComplete()
    {
        Debug.Log("-----------------  OnGameComplete");

        this.StartLoadingAndEndEx(() =>
        {
            ShowSceneObj();
            ResumeTimeline();
            SetCameraActive(false);
        });
    }

    private void ShowSceneObj()
    {
        GameObject go = FTimelineDispatch.GetTimelineObj(TimelineID);
        if (go != null && hideObjNameArray != null)
        {
            Transform target = go.transform;
            
            foreach (string name in hideObjNameArray)
            {
                Transform t = target.Find(name);
                if (t != null) t.SetActive(true);

            }
        }
    }

    private void HideSceneObj()
    {
        GameObject go = FTimelineDispatch.GetTimelineObj(TimelineID);
        if (go != null && hideObjNameArray != null)
        {
            Transform target = go.transform;
            
            foreach (string name in hideObjNameArray)
            {
                Transform t = target.Find(name);
                if (t != null) t.SetActive(false);

            }
        }
    }

    private void SetCameraActive(bool value)
    {
        if (cam != null)
        {
            cam.gameObject.SetActive(value);
        }
    }


    //private void PlayAudioStart()
    //{
    //    this.PlayAudioEx(audioBasePath + audioStart);
    //}
    //private void PlayAudioComplete()
    //{
    //    this.PlayAudioEx(audioBasePath + audioComplete);
    //}
    private GameAudioSource PlayAudio(string audioName)
    {
        return this.PlayAudioEx(audioBasePath + audioName);
    }

}

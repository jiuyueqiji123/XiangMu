﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHolder : BaseTrackHolder
{

    public float force = 500;
    public float rudius = 10;

    public Transform explosionPoint;
    public Transform explosionParent;
    public Transform wallNormal;
    public Transform wallBreak;


    //protected override void Start()
    //{
    //    base.Start();

    //}


    protected override void OnPlay()
    {
        base.OnPlay();

        Explosion();
    }


    void Explosion()
    {
        wallNormal.SetActive(false);
        wallBreak.SetActive(true);
        for (int i = 0; i < explosionParent.childCount; i++)
        {
            Transform target = explosionParent.GetChild(i);
            Rigidbody rb = target.AddComponentEx<Rigidbody>();
            rb.AddExplosionForce(force, explosionPoint.position, rudius);
        }
    }

}

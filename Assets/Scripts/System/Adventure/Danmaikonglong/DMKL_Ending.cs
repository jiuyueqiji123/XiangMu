﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DMKL_Ending : ReceiverBaseMono, IEasyLoading
{




    protected override void Initialize()
    {
        base.Initialize();


        TimelinePlayer.Instance.PauseAndStopAnimator(TimelineID);

        this.AddTimerEx(1f, () => {

            this.StartLoadingAndEndEx(() =>
            {
                GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
            });
        });
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Chaokugongju : BaseTrackHolder
{


    private const string jiqirenPath = "story_danmai/story_danmai_prefab/Jiqiren";
    private const string duoduoPath = "story_danmai/story_danmai_prefab/duoduo";

    private readonly string audioBasePath = "story_danmai/story_danmai_audio/";
    private readonly string audioShow = "602030404";    //大机器出现
    private readonly string audioHeti = "602030405";    // 合体

    public string parentName;
    public string nodeName;

    public Transform box;
    //public Transform jiqiren;
    public Transform uiEffect;
    public Transform moveBg;
    public Transform jiqirenParent;

    public Animator jiqirenShowAnimator;


    protected override void Start()
    {
        base.Start();

        box.SetActive(false);
        //jiqiren.SetActive(false);
        uiEffect.SetActive(false);
        moveBg.SetActive(false);

    }

    protected override void AddListeners()
    {

        EventBus.Instance.AddEventHandler<string>(EventID.ON_TIMELINE_CLICK_EVENT, OnTimelineClickEvent);
    }

    protected override void RemoveListeners()
    {

        EventBus.Instance.RemoveEventHandler<string>(EventID.ON_TIMELINE_CLICK_EVENT, OnTimelineClickEvent);
    }

    protected override void OnPlay()
    {
        base.OnPlay();

        switch (clipInfo.eventID)
        {
            case "30005_show":

                GameObject go = FTimelineDispatch.GetTimelineObj(TimelineID);
                if (go != null)
                {
                    Transform target = go.transform.Find(parentName);
                    if (target != null)
                    {
                        box.SetActive(true);
                        Transform node = target.FindChildEx(nodeName);
                        box.SetParent(node);
                        box.ResetTransformForLocal();
                    }
                }

                break;

            case "30005_tuoli":

                    box.SetParent(transform);

                break;
            case "30005_refresh":

                GameObject go1 = FTimelineDispatch.GetTimelineObj(TimelineID);
                if (go1 != null)
                {
                    Transform target = go1.transform.Find("30005_ShadowProjecter");
                    if (target != null)
                    {
                        target.GetComponent<DynamicShadowProjector.DrawTargetObject>().RefreshShaowProjecter();
                    }
                }
                break;
        }
    }

    private void OnTimelineClickEvent(string eventID)
    {
        switch (eventID)
        {
            case "30005_chaokugongju":

                box.SetActive(false);
                StartCoroutine(ShowJiqiren());
                break;
        }
    }

    private IEnumerator ShowJiqiren()
    {
        uiEffect.SetActive(true);

        Image img = uiEffect.GetComponent<Image>();
        img.color = new Color(1, 1, 1, 0);
        float val = 0;
        while (val<1)
        {
            val += Time.deltaTime;
            img.color = new Color(1, 1, 1, val);
            yield return null;
        }
        img.color = new Color(1, 1, 1, 1);

        GameObject jiqirenObj = CreateJiqiren();
        jiqirenObj.SetActive(false);
        yield return new WaitForSeconds(1);

        moveBg.SetActive(true);
        moveBg.GetComponentInChildren<LoopMoving>().SetEnable(true);

        val = 1;
        while (val > 0)
        {
            val -= Time.deltaTime * 2;
            img.color = new Color(1, 1, 1, val);
            yield return null;
        }
        uiEffect.SetActive(false);
        jiqirenObj.SetActive(true);

        jiqirenShowAnimator.Play("Show");

        PlayAudio(audioShow);

        yield return new WaitForSeconds(2.5f);
        GameObject duoduo = CreateDuoduo();
        duoduo.transform.SetParent(jiqirenObj.transform.FindChildEx("mount"));
        duoduo.transform.ResetTransformForLocal();
        duoduo.transform.localPosition = new Vector3(0, 10, 0);
        duoduo.transform.GetChild(0).GetComponent<Animator>().CrossFade("sit_control", 0.1f);
        duoduo.transform.DOLocalMove(Vector3.zero, 1.0f);

        yield return new WaitForSeconds(0.5f);

        PlayAudio(audioHeti);

        yield return new WaitForSeconds(1.5f);

        ResumeTimeline();

        yield return new WaitForSeconds(0.5f);

        GameObject go = FTimelineDispatch.GetTimelineObj(TimelineID);
        if (go != null)
        {
            Transform target = go.transform.Find("30005_ShadowProjecter");
            if (target != null)
            {
                target.GetComponent<DynamicShadowProjector.DrawTargetObject>().RefreshShaowProjecter();
            }
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).SetActive(false);
        }

    }

    private GameObject CreateJiqiren()
    {
        GameObject go = Res.LoadObj(jiqirenPath);
        if (go != null)
        {
            go.transform.SetParent(jiqirenParent);
            go.transform.localPosition = new Vector3(0, 0, -0.44f);
            go.transform.localRotation = Quaternion.Euler(0, -180, 0);
        }
        return go;
    }

    private GameObject CreateDuoduo()
    {
        GameObject go = Res.LoadObj(duoduoPath);
        return go;
    }

    private GameAudioSource PlayAudio(string audioName)
    {
        return this.PlayAudioEx(audioBasePath + audioName);
    }


}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GallaryDragItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    public RectTransform canvas;          //得到canvas的ugui坐标
    private RectTransform imgRect;        //得到图片的ugui坐标
    Vector2 offset = new Vector3();    //用来得到鼠标和图片的差值

    void Start()
    {
        canvas = WindowManager.Instance.canvas.GetComponent<RectTransform>();
        imgRect = transform.parent.GetComponent<RectTransform>();

    }

    //当鼠标拖动时调用   对应接口 IDragHandler
    public void OnDrag(PointerEventData eventData)
    {
        Vector2 mouseDrag = new Vector2(eventData.position.x, Screen.height / 2);   //当鼠标拖动时的屏幕坐标
        Vector2 uguiPos = new Vector2();   //用来接收转换后的拖动坐标
        bool isRect = RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas, mouseDrag, eventData.enterEventCamera, out uguiPos);

        if (isRect)
        {
            imgRect.anchoredPosition = offset + uguiPos;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        offset = Vector2.zero;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Vector2 mouseDown = eventData.position;    //记录鼠标按下时的屏幕坐标
        Vector2 mouseUguiPos = new Vector2();   //定义一个接收返回的ugui坐标
        bool isRect = RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas, mouseDown, eventData.enterEventCamera, out mouseUguiPos);
        if (isRect)   //如果在
        {
            offset = new Vector2(imgRect.anchoredPosition.x - mouseUguiPos.x, 0);
        }
    }

    private void Update()
    {
        ////Vector3 lmin = 
        //RectTransform selfTrans = GetComponent<RectTransform>();
        //if (selfTrans.position.x <= DragTest.intance.anchorLeft.x + 1)
        //{
        //    if (DragTest.intance.currentMoveDir == DragTest.ViewMoveDir.left)
        //    {
        //        Debug.Log("left");
        //        transform.position = DragTest.intance.vr;
        //        DragTest.intance.ResetParent();
        //    }

        //}
        //else if (selfTrans.position.x >= DragTest.intance.anchorRight.x - 1)
        //{
        //    if (DragTest.intance.currentMoveDir == DragTest.ViewMoveDir.right)
        //    {
        //        Debug.Log("right");
        //        transform.position = DragTest.intance.vl;
        //        DragTest.intance.ResetParent();
        //    }
        //}
    }
}

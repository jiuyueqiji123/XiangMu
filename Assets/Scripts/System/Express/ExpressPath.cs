﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpressPath {

    private const string expressBasePath = "express/";

    private const string expressEarthPath = "prefab/model/Earth";
	
    public static string EarthPath { get { return expressBasePath + expressEarthPath; } }

    //资源


    public const string gridlayoutUIItem = "express/express_ui/GridLayoutItem";

    public const string PageViewPageItem = "express/express_ui/PageViewPageItem";
}

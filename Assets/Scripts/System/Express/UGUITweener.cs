﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

/*
 * /////////////////
 * 苏继兵
 * ////////////////
 */
//https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/UI/ScriptBindings/RectTransformUtility.cs

public class UGUITweener : Singleton<UGUITweener>
{

    private static Canvas _canvas;
    private  static Canvas Canvas
    {
        get
        {
            if (_canvas == null)
                _canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
            return _canvas;
        }
    }

    /// <summary>
    /// 屏幕坐标-》canvas坐标
    /// </summary>
    /// <param name="cvs"></param>
    /// <param name="ScreenPos"></param>
    /// <param name="RectTransPos"></param>
    /// <returns></returns>
    public static bool GetRectPosForScreenPos(Canvas cvs, Vector2 ScreenPos, out Vector2 RectTransPos)
    {

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(cvs.transform as RectTransform, ScreenPos, cvs.worldCamera, out RectTransPos))
        {
            return true;
        }

        return false;

    }

    /// <summary>
    /// 世界坐标-》canvas坐标
    /// </summary>
    /// <param name="cvs"></param>
    /// <param name="worldPos"></param>
    /// <param name="RectTransPos"></param>
    /// <returns></returns>
    public static bool GetRectPosForWorldPos(Canvas cvs, Vector3 worldPos, out Vector2 RectTransPos)
    {
        Vector2 ScreenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 finalScreenPos = new Vector2(ScreenPos.x, ScreenPos.y);

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(cvs.transform as RectTransform, finalScreenPos, cvs.worldCamera, out RectTransPos))
        {
            return true;
        }

        return false;

    }

    /// <summary>
    /// 其它层级ui坐标-》Canvas根节点anchorPosition
    /// </summary>
    /// <param name="cvs"></param>
    /// <param name="rect"></param>
    /// <returns></returns>
    public static Vector2 GetCanvasAnchorLocalPosition(Canvas cvs,RectTransform rect)
    {
        Vector2 localpos = cvs.GetComponent<RectTransform>().InverseTransformPoint(rect.position);
        Debug.Log(localpos);
        return localpos;
    }

    /// <summary>
    /// UI移动到制定屏幕位置
    /// </summary>
    /// <param name="touchPoint"></param>
    /// <param name="target"></param>
    /// <param name="dur"></param>
    /// <param name="callback"></param>
    public void MoveToWithScreenPos(Canvas cvs, Vector3 touchPoint, RectTransform target, float dur, TweenCallback callback = null)
    {
        Canvas cv = cvs==null ? Canvas : cvs;

        Vector2 localpos;
        if (GetRectPosForScreenPos(cv, touchPoint, out localpos))
        {
            target.DOAnchorPos(localpos, dur).OnComplete(callback);
        }
        else
        {
            Debug.Log("不包含");
        }
    }

    public static Vector3 GetCanvasLocalPos(Canvas cvs, RectTransform target)
    {
        Canvas cv = cvs == null ? Canvas : cvs;

        return cv.transform.InverseTransformPoint(target.position);
    }

    public static bool GetCanvaLocalPos(Canvas cvs, RectTransform target,Camera cam, out Vector2 localPosition)
    {
        Canvas cv = cvs == null ? Canvas : cvs;
        Camera camera = cam == null ? Camera.main : cam;
        Vector2 _localpos;

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(cv.transform as RectTransform, camera.WorldToScreenPoint(target.position), camera, out _localpos))
        {
            localPosition = _localpos;
            return true;
        }

        localPosition = Vector2.zero;
        return false;
    }
}

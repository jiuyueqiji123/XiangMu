﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ExpressSetting {

    public int _energy;           //拥有的能量数量
    public string _time_bonus;   //计时器

    public ExpressSetting(int energy, string time_bonus)
    {
        this._energy = energy;
        this._time_bonus = time_bonus;
    }
}

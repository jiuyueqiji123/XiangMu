﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PhotoShowPanel : SingletonBaseWindow<PhotoShowPanel>
{

    #region ui property

    public RectTransform _PhotoParent { get; set; }

    public RawImage _PhotoTexture { get; set; }

    public Text _text_PhotoName { get; set; }

    public Transform _Streamereffect { get; set; }

    public Button _Panel { get; set; }

    private GameAudioSource _currentAudioSource;

    #endregion

    private bool isOnTweeen;

    private int timeseq;

    protected override void AddListeners()
    {
        base.AddListeners();

        _Panel.onClick.AddListener(OnClolseBtnClick);
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();

        _Panel.onClick.RemoveAllListeners();
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
    }

    private void OnClolseBtnClick()
    {
        _Panel.interactable = false;
        CommonSound.PlayClick();
        TimerManager.Instance.RemoveAllTimer();
        PanelTweenClose();
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        isOnTweeen = false;

        _Panel.interactable = true;

        gameObject.SetActive(true);

        ExpressImageLocalLoadRuturnArg arg = paramArray[0] as ExpressImageLocalLoadRuturnArg;
        _PhotoTexture.texture = arg._tex;
        _text_PhotoName.text = arg._texname;

        AudioClip clip = ExpressResManager.GetAudioClip(ExpressConsts.audio_capture);
        AudioClip indroduceClip = ExpressResManager.GetAudioClip(arg._id.ToString());
        AudioManager.Instance.PlaySound(clip);

        TimerManager.Instance.AddTimer(2.1f, () => {

            _Streamereffect.SetActive(true);

            _currentAudioSource = AudioManager.Instance.PlaySound(indroduceClip);

        });

        timeseq = TimerManager.Instance.AddTimer(indroduceClip.length + 2.1f, ()=>
        {
            PanelTweenClose();

        });
    }

    private void PanelTweenClose()
    {
        if (!isOnTweeen)
        {
            isOnTweeen = true;

            AudioManager.Instance.StopSound(_currentAudioSource);

            gameObject.GetComponent<Animator>().enabled = false;
            _PhotoParent.DOAnchorPos(GallaryPanel.m_current_ui_startposition, 0.8f);
            _PhotoParent.DOScale(.1f, 0.8f);
            _PhotoParent.GetComponent<CanvasGroup>().DOFade(0f, 0.8f);

            _Streamereffect.SetActive(false);
            TimerManager.Instance.AddTimer(ExpressConsts.dur_closeUI, () =>
            {
                WindowManager.Instance.CloseWindow(WinNames.PhotoShowPanel);
                isOnTweeen = false;

            });
        }
        

    }
}

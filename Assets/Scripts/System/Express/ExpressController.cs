﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExpressController : Singleton<ExpressController> {

    float ui_cam_origindepth;

    private ExpressPara _expressParam;
    public ExpressPara ExpressParam
    {
        get {
            if (_expressParam == null)
                _expressParam = GameObject.FindObjectOfType<ExpressPara>();
            return _expressParam;
        }
    }

    GameObject _earth;
    public GameObject Earth
    {
        get
        {
            if (_earth == null)
            {
                _earth = GameObject.Find("Earth");
            }
            return _earth;

        }
    }

    private Animator _lediAnimator;
    public Animator LediAnimator
    {
        get {
            if (_lediAnimator == null)
            {
                GameObject ledi_prefab = ResourceManager.Instance.GetResource(ExpressConsts.actor_ledi, typeof(GameObject), enResourceType.Prefab).m_content as GameObject;
                GameObject target = GameObject.Instantiate<GameObject>(ledi_prefab);
                _lediAnimator = target.GetComponent<Animator>();
                Resources.UnloadUnusedAssets();
            }
            return _lediAnimator;
                
        }
    }

    public override void Init()
    {
        base.Init();

        //确保主ui层级在本ui的上面
        ui_cam_origindepth = WindowManager.Instance.camera.depth;
        WindowManager.Instance.camera.depth = ExpressConsts.ui_express_camdepth;

        if (UserManager.Instance.UserId != 0)
        {
            WindowManager.Instance.OpenWindow(WinNames.StartPanel);

            ExpressDataManager.Instance.InitServerData(() => {

                Debug.Log("获取数据成功");

            }, () =>
            {
                Debug.LogError("从服务器获取数据失败");
                NoticeManager.Instance.ShowTip("该功能需要联网，请打开网络!", BackToMainState);
                GetUIAnimatorEx(WinNames.StartPanel).CrossFade(eExpressUIAnimState.StartPaneOut.ToString(), ExpressConsts.dur_crossfade_openUI);

            });
           
        }
        else
        {
            Debug.LogError("用户登录失败！");
            NoticeManager.Instance.ShowTip("该功能需要联网，请打开网络!", BackToMainState);
            GetUIAnimatorEx(WinNames.StartPanel).CrossFade(eExpressUIAnimState.StartPaneOut.ToString(), ExpressConsts.dur_crossfade_openUI);
        }

    }

    public override void UnInit()
    {
        base.UnInit();
        WindowManager.Instance.camera.depth = ui_cam_origindepth;

        ExpressDataManager.DestroyInstance();
        ExpressResManager.DestroyInstance();
        ExpressWebService.DestroyInstance();
        TimerManager.Instance.RemoveAllTimer();
       
    }

    public AnimatorExtension GetUIAnimatorEx(string windowName)
    {
        switch (windowName)
        {
            case WinNames.StartPanel:

                if (StartPanel.Instance.gameObject != null)
                    return new AnimatorExtension(StartPanel.Instance.gameObject);


                break;
            case WinNames.GallaryPanel:

                if (GallaryPanel.Instance.gameObject != null)
                    return new AnimatorExtension(GallaryPanel.Instance.gameObject);

                break;
            case WinNames.WarningPanel:

                if (WarningPanel.Instance.gameObject != null)
                    return new AnimatorExtension(WarningPanel.Instance.gameObject);
                break;
            case WinNames.PhotoShowPanel:

                if (PhotoShowPanel.Instance.gameObject != null)
                    return new AnimatorExtension(PhotoShowPanel.Instance.gameObject);

                break;

            case WinNames.GallayPictureShowPanel:

                if (GallayPictureShowPanel.Instance.gameObject != null)
                    return new AnimatorExtension(GallayPictureShowPanel.Instance.gameObject);

                break;
            default:
                break;
        }

        return null;
    }

    private void BackToMainState()
    {
        WindowManager.Instance.CloseWindow(WinNames.StartPanel);
        GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }

    public void OnTimerFinished()
    {

    }

    public void OnTimerStart()
    {

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulateDataWriter : MonoBehaviour {

    public TotalGallayImageData galaryData;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            string jsonstr = JsonUtility.ToJson(galaryData);
            PlayerPrefs.SetString(ExpressConsts.key_TotalGallaryData, jsonstr);
            Debug.Log(jsonstr);
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            ExpressDataManager.CreateInstance();
        }

        //if (Input.GetKeyDown(KeyCode.X))
        //{
        //    ExpressSetting setting = new ExpressSetting();
        //    setting._energy = 2;
        //    TimerSetting tsetting = new TimerSetting();
        //    //tsetting.currentTimerTimeRemain = 1000;
        //    tsetting.TimerStartTime = STool.GetCurrentTimeWithSec();
        //    tsetting.isOnTimer = true;

        //    string j = JsonUtility.ToJson(setting);
        //    PlayerPrefs.SetString(ExpressConsts.key_ExpressSetting, j);
        //}

        if (Input.GetKeyDown(KeyCode.C))
            PlayerPrefs.DeleteAll();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Continent
{
    Asia,
    Africa,
    Europe,
    NorthAmerica,
    Australia,
    SouthAmerica,
}

//相册本地信息存档
public class TotalGallayLocalData
{
    public uint lastNewAddID = ExpressConsts.defaultUID; 
    public List<GallayLocalData> m_tatal_gallaylocaldata = new List<GallayLocalData>();
}

public class GallayLocalData
{
    public uint m_id;
    public bool m_isNewadd;
}

[System.Serializable]
public class GallayImageData {

    public uint _id;          //id
    public string _url;         //图片路径
    public Continent _continent;   //大洲
    public string _region;      //区域
    public string _imagename;   //图片名称
    public bool isNewAdded;     //是否是新加入还未查看的

}

[System.Serializable]
public class TotalGallayImageData
{
    public uint lastNewaddPicture;  //上一张新获得的图片
    public bool haveNewAdd;   //有没有是不是新加入图片
    public int totalPhotoNum;   //总的照片数量
    public List<GallayImageData> _totalGallayData;

    public TotalGallayImageData(List<GallayImageData> data,bool haveNewPhotoAdd,int totalCount, uint lastNewaddPicture = ExpressConsts.defaultUID)
    {
        _totalGallayData = data;
        haveNewAdd = haveNewPhotoAdd;
        totalPhotoNum = totalCount;
        this.lastNewaddPicture = lastNewaddPicture;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;



/// <summary>
/// 图片本地加载的参数
/// </summary>
public class ExpressImageLocalloadArg
{
    public string _name;
    public uint _id;  
    public string _localpath; 

    public ExpressImageLocalloadArg(uint id, string localPath,string name)
    {
        this._id = id;
        this._localpath = localPath;
        this._name = name;
    }
}

/// <summary>
/// 图片本地加载返回参数
/// </summary>
public class ExpressImageLocalLoadRuturnArg
{
    public Texture2D _tex;
    public uint _id;
    public string _texname;

    public ExpressImageLocalLoadRuturnArg(Texture2D tex, uint id,string name)
    {
        this._tex = tex;
        this._id = id;
        this._texname = name;
    }
}

public class ExpressResManager : Singleton<ExpressResManager> {

    private const string expressPhotoBasePath = "express/gallaryTexture/";

    public override void Init()
    {
        base.Init();
    }

    public override void UnInit()
    {
        base.UnInit();
    }

    public static AudioClip GetAudioClip(string name)
    {
        string path = ExpressConsts.audio_basepath + name;
        Resource res = ResourceManager.Instance.GetResource(path, typeof(AudioClip), enResourceType.Sound);
        if (res.m_content != null)
        {
            return res.m_content as AudioClip;
        }

        return null;
    }

    /// <summary>
    /// 加载本地图片
    /// </summary>
    /// <returns></returns>
    public IEnumerator LoadTexture(ExpressImageLocalloadArg loadArg, Action<ExpressImageLocalLoadRuturnArg> callback, Action onfail)
    {
        string localPath = expressPhotoBasePath + loadArg._localpath;
        //Debug.LogFormat("localpath: {0}", localPath);
        Texture2D t2d = CommonUtility.LoadTexture(localPath);
        if (t2d == null)
        {
            if (onfail != null) onfail();
        }

        ExpressImageLocalLoadRuturnArg returnArg = new ExpressImageLocalLoadRuturnArg(t2d, loadArg._id, loadArg._name);
        if (callback != null) callback(returnArg);
        yield break;
    }

    private void UpdateUIProgress(string count, float progress)
    {
        Debug.Log(count + progress);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eExpressUIAnimState {

    UIAnimStay,
    StartPaneOut,
    StartPanelIn

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpressWindowBase : SingletonBaseWindow<ExpressWindowBase>
{
    public ExpressWindowBase()
    {
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        gameObject.SetActive(true);

        OnUIAnimateIn();
    }

    protected override void OnClose()
    {
        base.OnClose();

        OnUIAnimateOut();

    }

    protected virtual void OnUIAnimateIn() { }
    protected virtual void OnUIAnimateOut() { }
}

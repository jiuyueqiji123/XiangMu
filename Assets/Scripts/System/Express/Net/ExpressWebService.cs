﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExpressWebService : Singleton<ExpressWebService>
{

    private DeliveryDataService delivertyService { get; set; }

    private StartTravelService startTraveService { get; set; }

    private BuyPowerService buyPowerService { get; set; }

    /// <summary>
    /// 网路是否连通
    /// </summary>
    public bool IsNetworkAccess
    {
        get
        {
            return Application.internetReachability != NetworkReachability.NotReachable;
        }

    }

    public override void Init()
    {
        base.Init();

        delivertyService = new DeliveryDataService();
        startTraveService = new StartTravelService();
        buyPowerService = new BuyPowerService();

    }

    public override void UnInit()
    {
        base.UnInit();

        delivertyService = null;
        startTraveService = null;
        buyPowerService = null;
    }

    /// <summary>
    /// 从服务器购买能量测试接口
    /// </summary>
    public void AddEnegy(int num)
    {
        //MonoInstane.Instance.StartCoroutine(buyPowerService.AddEnergy());
        this.buyPowerService.AddEnergy(num, (data) =>
        {
            if (data.status != Cmd.Status.FAILURE)
            {
                ExpressDataManager.Instance.AddPower(num);
                WindowManager.Instance.OpenWindow(WinNames.StartPanel);
            }
            else
            {

            }
        },
        (fail)=>{
           
        });
    }

    /// <summary>
    /// 获取相册信息
    /// </summary>
    public void GetDeliveryData( Action<Cmd.ReturnDeliveryData> callback,Action failcallback)
    {
        if (IsNetworkAccess)
        {
            delivertyService.DeliveryData((data) => {

                if (data.status != Cmd.Status.FAILURE)
                {
                    if (callback != null) callback(data);
                }
                else
                {
                    Debug.LogError(data.info);
                    if (failcallback != null) failcallback();
                    NoticeManager.Instance.ShowTip(data.info);
                }

            },(fail)=> {
                if (failcallback != null) failcallback();
            });
        }
        else
        {
            if (failcallback != null) failcallback();
        }
    }

    /// <summary>
    /// 获取旅行照片
    /// </summary>
    /// <param name="callback"></param>
    public void StartTravel(Action<Cmd.TakePhoto> callback, Action failcallback)
    {
        if (IsNetworkAccess)
        {
            startTraveService.StartTravel((data) => {

                if (data.status != Cmd.Status.FAILURE)
                {
                    if (callback != null) callback(data);
                }
                else
                {
                    Debug.LogError(data.info);
                    if (failcallback != null) failcallback();
                    NoticeManager.Instance.ShowTip(data.info);
                }

            },(fail)=>
            {
                if (failcallback != null) failcallback();
            });
        }
        else
        {
            if (failcallback != null) failcallback();
        }
    }

}

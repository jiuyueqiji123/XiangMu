﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BuyPowerService : ExpressServiceBase
{

    protected override string Url
    {
        get
        {
            return "delivery/add_energy";
        }
    }


    public void AddEnergy(int num, Action<Cmd.ReturnAddEnergy> callback, HttpTask.FailCallbackDelegate connectfail)
    {
        HttpTask task = ClassObjPool<HttpTask>.Get();
        Cmd.AddEnergy info = new Cmd.AddEnergy();
        info.id = UserManager.Instance.UserId;
        info.num = num;
        byte[] dataB = Protocol.ProtoBufSerialize<Cmd.AddEnergy>(info);
        task.SetData(dataB, delegate (byte[] data) {
            Cmd.ReturnAddEnergy ret = Protocol.ProtoBufDeserialize<Cmd.ReturnAddEnergy>(data);
            if (callback != null) callback(ret);
        }, connectfail);

        base.Send(task);
    }
}

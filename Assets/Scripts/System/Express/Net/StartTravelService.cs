﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TableProto;

public class StartTravelService : ExpressServiceBase
{


    protected override string Url
    {
        get
        {
            return "delivery/travel";
        }
    }


    /// <summary>
    /// 获取旅行照片
    /// </summary>
    /// <param name="callback"></param>
    public void StartTravel(Action<Cmd.TakePhoto> callback,HttpTask.FailCallbackDelegate connectfail)
    {
#if UNITY_EDITOR

        Dictionary<uint, TableProto.ExpressPhotosInfo> tableProto = TableProtoLoader.ExpressPhotosInfoDict;
        List<uint> ks = new List<uint>();
        foreach (var item in tableProto.Keys)
        {
            ks.Add(item);
        }
        int index = UnityEngine.Random.Range(0, ks.Count);
        ExpressPhotosInfo ex = tableProto[ks[index]];

        Cmd.TakePhoto takePhotodata = new Cmd.TakePhoto();
        Cmd.PhotoInfo photoInfo = new Cmd.PhotoInfo();
        photoInfo.area_type = (int)ex.area_type;
        photoInfo.photo_id = ex.id.ToString();
        photoInfo.photo_url = ex.photo_url;
        photoInfo.title = ex.title;
        //photoInfo.area_type = 5;
        //photoInfo.photo_id = "503005037";
        //photoInfo.photo_url = "S04E16.png";
        //photoInfo.title = "克罗地亚";

        takePhotodata.photo_info = photoInfo;
        takePhotodata.status = Cmd.Status.SUCCESS;
        takePhotodata.info = "success";

        if (callback != null) callback(takePhotodata);
#else
         HttpTask task = ClassObjPool<HttpTask>.Get();
        Cmd.Travel info = new Cmd.Travel();
        info.id = UserManager.Instance.UserId;
        Debug.LogFormat("InitDeliverydata userid: {0}", info.id);
        byte[] dataB = Protocol.ProtoBufSerialize<Cmd.Travel>(info);
        task.SetData(dataB, delegate (byte[] data) {

            Cmd.TakePhoto takePhotodata = Protocol.ProtoBufDeserialize<Cmd.TakePhoto>(data);
            if (callback != null) callback(takePhotodata);

        }, connectfail);

        base.Send(task);
#endif

    }
}

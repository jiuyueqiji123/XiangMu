﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DeliveryDataService : ExpressServiceBase
{

    protected override string Url
    {
        get
        {
            return "delivery/init_data";
        }
    }


    /// <summary>
    /// 获取相册信息
    /// </summary>
    public void DeliveryData(Action<Cmd.ReturnDeliveryData> callback,HttpTask.FailCallbackDelegate connectfail)
    {
        HttpTask task = ClassObjPool<HttpTask>.Get();
        Cmd.InitDeliveryData info = new Cmd.InitDeliveryData();
        info.id = UserManager.Instance.UserId;
        Debug.LogFormat("InitDeliverydata userid: {0}", info.id);
        byte[] dataB = Protocol.ProtoBufSerialize<Cmd.InitDeliveryData>(info);
        task.SetData(dataB, delegate (byte[] data) {

            Cmd.ReturnDeliveryData deliverydataReturn = Protocol.ProtoBufDeserialize<Cmd.ReturnDeliveryData>(data);
            if (callback != null) callback(deliverydataReturn);

        }, connectfail);

        base.Send(task);
    }
}

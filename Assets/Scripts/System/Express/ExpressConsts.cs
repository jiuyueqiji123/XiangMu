﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpressConsts {

    public static bool forDebug = false;

    public const float ui_express_camdepth = 8;

    //playerpref key
    public const string key_first_photo = "key_first_photo";


    //audio
    public const string audio_basepath = "express/audio/";
    public const string common_audio_basepath = "sound/";
    public const string audio_test = "test";                    //飞行地点测试的声音
    public const string audio_earth_rotate = "605000001";       //地球旋转的声音
    public const string audio_capture = "605000002";            //拍照声音
    public const string audio_click = "601010106";            //点击声音

    public const string key_TotalGallaryData = "key_TotalGallaryData";

    public const string key_PowerAmount = "key_PowerAmount";

    public const string key_ExpressSetting = "key_ExpressSetting";


    //animator

    public const string animator_start_travel = "express/101001000/101001000";

    public const string actor_ledi = "express/101001000/ledi";

    public const float dur_actor_ledifly = 1.8f;

    public const float dur_earth_move = .5f;

    public const float dur_earth_rotate = 2f;

    public const float dur_crossfade_openUI = .1f;

    public const float dur_crossfade_closeUI = .1f;

    public const float dur_openUI = 1f;

    public const float dur_closeUI = 1f;

    public const float timeout_showPhoto = 5f;      //travel照片下载的超时时间

    //power

    public const int powerCost_perTravel = 1;       //每次旅行消耗的能量


    //powerbuy

    public const uint pay1id = 4001;

    //pageview
    public const uint defaultUID = 99999;

}

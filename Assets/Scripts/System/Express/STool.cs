﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class STool {

    //获取当前时间的总秒数
    public static long GetCurrentTimeWithSec()
    {
        long currentTicks = DateTime.Now.Ticks;
        DateTime dtFrom = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        return (currentTicks - dtFrom.Ticks) / 10000000;
    }

    public static long GetTimeWithSec(DateTime time)
    {
        long currentTicks = time.Ticks;
        DateTime dtFrom = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        return (currentTicks - dtFrom.Ticks) / 10000000;
    }

    /// <summary>
    /// 16进制颜色转Color
    /// </summary>
    public static Color HexStrToColor(string colorStr)
    {
        if (string.IsNullOrEmpty(colorStr))
        {
            return new Color();
        }
        int colorInt = int.Parse(colorStr, System.Globalization.NumberStyles.AllowHexSpecifier);
        return IntToColor(colorInt);
    }

    private static Color IntToColor(int colorInt)
    {
        float basenum = 255;

        int b = 0xFF & colorInt;
        int g = 0xFF00 & colorInt;
        g >>= 8;
        int r = 0xFF0000 & colorInt;
        r >>= 16;
        return new Color((float)r / basenum, (float)g / basenum, (float)b / basenum, 1);

    }

}

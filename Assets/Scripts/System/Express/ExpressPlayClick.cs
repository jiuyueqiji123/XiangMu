﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpressPlayClick : MonoBehaviour {

    private Button _bt;

	void Start () {
        _bt = GetComponent<Button>();
        _bt.onClick.AddListener(() => {
            CommonSound.PlayClick();
        });
	}
}

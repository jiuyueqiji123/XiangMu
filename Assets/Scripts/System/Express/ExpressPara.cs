﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ExpressPara : MonoBehaviour {

    //相册
    public Color indexNormalColorForGalary = Color.white;

    public Color indexSelectedColorForGalary = Color.white;

    public static DOTweenPath GetDoTweenPath(string id)
    {
        GameObject pathObj = GameObject.Find(string.Format("Path{0}", id));
        if (pathObj != null)
        {
            return pathObj.GetComponent<DOTweenPath>();
        }
        else
        {
            Debug.LogError("没有找到dotween路径！" + id);
            return null;
        }
    }
}

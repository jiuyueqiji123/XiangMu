﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EarthMono : MonoBehaviour {

    public float default_rotatespeed = 2f;
    public float travel_rotatespeed = 5f;
    public float radio_lerp = .085f;
    private float current_rotatespeed;
    private float target_rotatespeed;

    private bool _ontravel;
    public bool OnTravel
    {
        get {
            return this._ontravel;
        }
        set {
            this._ontravel = value;
            target_rotatespeed = value ? travel_rotatespeed : default_rotatespeed;
           

        }
    }

    public GameObject[] arkitecture;

    private Vector3 startPostion;

    public Animator EarthAniamator
    {
        get { return GetComponent<Animator>(); }
    }

    private void Start()
    {
        startPostion = transform.position;
        ResetEarthArkitecture();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            //OnTravel = true;
            OpenEarthArkitectureAnimate();
            FarwawyFromCam();
            StartTravel();
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            OnTravel = false;
            CloseToCam();
            CancelTravel();
        }

    }

    private void FixedUpdate()
    {
        current_rotatespeed = Mathf.Lerp(current_rotatespeed, target_rotatespeed, radio_lerp);
        transform.Rotate(new Vector3(0, current_rotatespeed * 0.01656738f, 0), Space.World);
    }

    #region Animate

    public void StartTravel()
    {
        OnTravel = true;
    }

    public void CancelTravel()
    {
        OnTravel = false;
        //这里没有设为vector3.zero是因为地球旋转太快，设为0，停下时会刚好转到地球的背面
        transform.localEulerAngles = new Vector3(0, 180, 0);

    }

    public void FarwawyFromCam()
    {
        transform.DOMove(new Vector3(startPostion.x, startPostion.y - .3f, startPostion.z - .3f), ExpressConsts.dur_earth_move);
    }

    public void CloseToCam()
    {
        transform.DOMove(startPostion, ExpressConsts.dur_earth_move);
    }

    #endregion

    public void OpenEarthArkitectureAnimate()
    {
        for (int i = 0; i < arkitecture.Length; i++)
            arkitecture[i].SetActive(true);
        EarthAniamator.CrossFade("GuoChang_02", 0f);
    }

    public void ResetEarthArkitecture()
    {
        for (int i = 0; i < arkitecture.Length; i++)
            arkitecture[i].SetActive(false);
        EarthAniamator.CrossFade("GuoChang_02_back", 0f);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExpressWindowOpenCallbackPara  {

    public Action _onOkEvent;
    public Action _onCancelEvent;
    public Action _onNoEvent;
    public Action _onCloseEvent;

    public ExpressWindowOpenCallbackPara(Action okevent, Action canceEvent, Action noEvent, Action closeEvent)
    {
        this._onCancelEvent = canceEvent;
        this._onOkEvent = okevent;
        this._onNoEvent = noEvent;
        this._onCloseEvent = closeEvent;
    }

}

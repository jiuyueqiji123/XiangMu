﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GallayPictureShowPanelOpenPara
{
    public Sprite _sprite;
    public string _name;
    public uint _pic_id;
    public Vector3 _anchor_startPoint;
}

public class GallayPictureShowPanel : SingletonBaseWindow<GallayPictureShowPanel> {

    private const float m_tween_dur = .5f;

    #region ui_properties

    public Button _Btn_CloseGallaryPictureShow { get; set; }

    public Image _Texture { get; set; }

    public Text _text_PhotoName { get; set; }

    public RectTransform _GallaryPictureShow { get; set; }

    private GameAudioSource _currentAudioSource;

    #endregion

    private Vector3 m_anchor_startPoint;

    protected override void AddListeners()
    {
        base.AddListeners();

        _Btn_CloseGallaryPictureShow.onClick.AddListener(OnCloseBtnClicked);
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();

        _Btn_CloseGallaryPictureShow.onClick.RemoveListener(OnCloseBtnClicked);
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        _Btn_CloseGallaryPictureShow.interactable = true;

        GallayPictureShowPanelOpenPara para = paramArray[0] as GallayPictureShowPanelOpenPara;

        Sprite sp = para._sprite;
        _Texture.sprite = sp;

        _text_PhotoName.text = para._name;

        uint pic_id = para._pic_id;

        m_anchor_startPoint = para._anchor_startPoint;


        gameObject.SetActive(true);

        AudioClip clip = ExpressResManager.GetAudioClip(pic_id.ToString());
        _currentAudioSource = AudioManager.Instance.PlaySound(clip);
        //移动至屏幕中央并放大
        Vector2 rectpos;
        UGUITweener.GetRectPosForScreenPos(WindowManager.Instance.canvas, new Vector2(Screen.width / 2, Screen.height / 2), out rectpos);
        _GallaryPictureShow.anchoredPosition = m_anchor_startPoint;
        _GallaryPictureShow.localScale = Vector2.one * .1f;
        _GallaryPictureShow.GetComponent<CanvasGroup>().alpha = 0;
        _GallaryPictureShow.GetComponent<CanvasGroup>().DOFade(1f, m_tween_dur);
        _GallaryPictureShow.DOAnchorPos(rectpos, m_tween_dur);
        _GallaryPictureShow.DOScale(1f, m_tween_dur);

    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        if (Input.GetMouseButtonDown(0))
        {
            OnCloseBtnClicked();
        }
    }

    #region UIevent

    private void OnCloseBtnClicked()
    {
        _Btn_CloseGallaryPictureShow.interactable = false;
        AudioManager.Instance.StopSound(_currentAudioSource);
     
        _GallaryPictureShow.GetComponent<CanvasGroup>().DOFade(0f, m_tween_dur);
        _GallaryPictureShow.DOAnchorPos(m_anchor_startPoint, m_tween_dur);
        _GallaryPictureShow.DOScale(.1f, m_tween_dur);
        TimerManager.Instance.AddTimer(m_tween_dur, () => {
            WindowManager.Instance.CloseWindow(WinNames.GallayPictureShowPanel);
        });
        
    }

    #endregion

}


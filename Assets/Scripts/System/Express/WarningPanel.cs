﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class WarningPanel : SingletonBaseWindow<WarningPanel>
{

    private Action _onCloseEvent;

    #region ui property

    public Button _Btn_close { get; set; }

    public Text _Text_warning { get; set; }

    #endregion


    protected override void AddListeners()
    {
        base.AddListeners();

        _Btn_close.onClick.AddListener(OnCloseBtnClicked);
    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();

        _Btn_close.onClick.RemoveListener(OnCloseBtnClicked);
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        _Text_warning.text = paramArray[0] as string;

        if(paramArray.Length > 1 && paramArray[1] != null)
            _onCloseEvent = (paramArray[1] as ExpressWindowOpenCallbackPara)._onCloseEvent;
    }

    protected override void OnClose()
    {
        base.OnClose();

        gameObject.SetActive(false);
    }


    #region UIEvent

    private void OnCloseBtnClicked()
    {
        WindowManager.Instance.CloseWindow(WinNames.WarningPanel);

        if (_onCloseEvent != null) _onCloseEvent();
    }

    #endregion
}

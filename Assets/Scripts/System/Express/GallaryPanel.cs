﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GallaryPanel : SingletonBaseWindow<GallaryPanel>
{
    public static Vector3 m_current_ui_startposition;

    #region ui property

    public Button _Btn_Back { get; set; }
    public Button _Btn_LastPage { get; set; }
    public Button _Btn_NextPage { get; set; }

    public Text _TextAmount { get; set; }

    public PageView _PageView { get; set; }

    public Transform _Panel_gallary { get; set; }

    public Transform _PageIndex { get; set; }

    public Text _Text_NoGallaryInfo { get; set; }

    #region 大洲按钮

    public Toggle _ToggleAfrica { get; set; }

    public Toggle _ToggleAsia { get; set; }

    public Toggle _ToggleEurope { get; set; }

    public Toggle _ToggleAustralia { get; set; }

    public Toggle _ToggleSouthAmerica { get; set; }

    public Toggle _ToggleNorthAmerica { get; set; }


    #endregion

    #endregion

    private Continent _lastContinent = Continent.Africa; 

    public Dictionary<Continent, List<GallayImageData>> GallayInfoDicIndexWithContinent { get; private set; }

    private bool _isChangeContentWithClick = true;  //切换大洲是否由UI操作发起（点击大洲按钮）


    protected override void AddListeners()
    {
        base.AddListeners();

        _Btn_Back.onClick.AddListener(OnBackBtnClicked);
        _Btn_LastPage.onClick.AddListener(OnlastPageClicked);
        _Btn_NextPage.onClick.AddListener(OnNextpageClicked);

        _ToggleAfrica.onValueChanged.AddListener((b) => { OnContinentBtnClicked(b,Continent.Africa); });
        _ToggleAsia.onValueChanged.AddListener((b) => { OnContinentBtnClicked(b, Continent.Asia); });
        _ToggleEurope.onValueChanged.AddListener((b) => { OnContinentBtnClicked(b, Continent.Europe); });
        _ToggleAustralia.onValueChanged.AddListener((b) => { OnContinentBtnClicked(b, Continent.Australia); });
        _ToggleSouthAmerica.onValueChanged.AddListener((b) => { OnContinentBtnClicked(b, Continent.SouthAmerica); });
        _ToggleNorthAmerica.onValueChanged.AddListener((b) => { OnContinentBtnClicked(b, Continent.NorthAmerica); });

    }

    protected override void RemoveListensers()
    {
        base.RemoveListensers();

        _Btn_Back.onClick.RemoveListener(OnBackBtnClicked);
        _Btn_LastPage.onClick.RemoveListener(OnlastPageClicked);
        _Btn_NextPage.onClick.RemoveListener(OnNextpageClicked);

        _ToggleAfrica.onValueChanged.RemoveAllListeners();
        _ToggleAsia.onValueChanged.RemoveAllListeners();
        _ToggleEurope.onValueChanged.RemoveAllListeners();
        _ToggleAustralia.onValueChanged.RemoveAllListeners();
        _ToggleSouthAmerica.onValueChanged.RemoveAllListeners();
        _ToggleNorthAmerica.onValueChanged.RemoveAllListeners();
    }

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        _TextAmount.text = string.Format("{0}/{1}", ExpressDataManager.Instance.TotalGallayGettedCount, ExpressDataManager.Instance.TotalGallaryCount);


        if (GallayInfoDicIndexWithContinent != null)
        {
            if (ExpressDataManager.Instance.TotalGallayInfo.lastNewaddPicture == ExpressConsts.defaultUID)
            {
                IndexGallaryinfoWithContinent();
            }
        }
        else
        {
            IndexGallaryinfoWithContinent();
        }

        if (ExpressDataManager.Instance.TotalGallayInfo.lastNewaddPicture == ExpressConsts.defaultUID)
        {
            if (!GallayInfoDicIndexWithContinent.ContainsKey(Continent.Asia))
                GallayInfoDicIndexWithContinent.Add(Continent.Asia, new List<GallayImageData>());
            //打开面板时默认打开非洲页面
            _PageView.Init(GallayInfoDicIndexWithContinent[Continent.Asia], Continent.Asia);
            ChangeContinent(Continent.Asia);
        }
        else
        {

            uint id = ExpressDataManager.Instance.TotalGallayInfo.lastNewaddPicture;
            _isChangeContentWithClick = false;
            GallayImageData img = ExpressDataManager.Instance.TotalGallayInfoDic[id];
            ChangeContinent(img._continent, img._id);
            ExpressDataManager.Instance.TotalGallayInfo.lastNewaddPicture = ExpressConsts.defaultUID;
            _isChangeContentWithClick = true;

        }
    }

    private void IndexGallaryinfoWithContinent()
    {
        GallayInfoDicIndexWithContinent = new Dictionary<Continent, List<GallayImageData>>();
        foreach (var item in ExpressDataManager.Instance.TotalGallayInfoDic.Values)
        {
            if (!GallayInfoDicIndexWithContinent.ContainsKey(item._continent))
            {
                List<GallayImageData> imagelist = new List<GallayImageData>();
                imagelist.Add(item);
                GallayInfoDicIndexWithContinent.Add(item._continent, imagelist);
            }
            else
            {
                GallayInfoDicIndexWithContinent[item._continent].Add(item);
            }
        }
    }

    protected override void OnClose()
    {
        base.OnClose();

        gameObject.SetActive(false);

    }

    protected override void OnUpdate()
    {
        base.OnUpdate();

        if (Input.GetKeyDown(KeyCode.H))
        {
            _isChangeContentWithClick = false;
            ChangeContinent(Continent.Asia,1);
            _isChangeContentWithClick = true;

        }
    }


    #region Event


    public void OnPageIndexChange(int index)
    {
        TimerManager.Instance.AddTimer(.1f, () => {
            int childCount = _PageIndex.childCount;
            for (int i = 0; i < childCount - 1; i++)
            {
                if (i == index)
                {
                    _PageIndex.GetChild(i + 1).GetComponent<Image>().color = ExpressController.Instance.ExpressParam.indexSelectedColorForGalary;
                }
                else
                {
                    _PageIndex.GetChild(i + 1).GetComponent<Image>().color = ExpressController.Instance.ExpressParam.indexNormalColorForGalary;
                }
            }
        });
    }

    public void ChangeContinent(Continent continent,uint defaultuid = ExpressConsts.defaultUID)
    {
        if (continent == _lastContinent)
        {
            Debug.Log("切换的同一个洲");
            //return;
        }

        if (!GallayInfoDicIndexWithContinent.ContainsKey(continent))
            GallayInfoDicIndexWithContinent.Add(continent, new List<GallayImageData>());
        _PageView.Init(GallayInfoDicIndexWithContinent[continent],continent, defaultuid);

        switch (continent)
        {
            case Continent.Africa:
                _ToggleAfrica.isOn = true;
                break;
            case Continent.Asia:
                _ToggleAsia.isOn = true;
                break;
            case Continent.Europe:
                _ToggleEurope.isOn = true;
                break;
            case Continent.Australia:
                _ToggleAustralia.isOn = true;
                break;
            case Continent.SouthAmerica:
                _ToggleSouthAmerica.isOn = true;
                break;
            case Continent.NorthAmerica:
                _ToggleNorthAmerica.isOn = true;
                break;
            default:
                break;
        }

        _lastContinent = continent;

    }

    //大洲照片页面往右翻结束
    public void OnAContinentPageRightOver(Continent continent)
    {
        continent++;
        if ((int)continent > 5)
        {
            continent = (Continent)0;

        }

        ChangeContinent(continent);
    }


    //往左翻结束
    public void OnAContinentPageLeftOver(Continent continent)
    {
        continent--;
        if ((int)continent < 0)
        {
            continent = (Continent)5;

        }

        ChangeContinent(continent);
    }



    #endregion


    #region UIEvent

    public void CreatePageIndexUI(int count)
    {
        foreach (Transform child in _PageIndex.transform)
            if(child.name != "Template")
                GameObject.Destroy(child.gameObject);

        GameObject template = _PageIndex.transform.GetChild(0).gameObject;
        template.SetActive(false);

        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                GameObject indexUIObj = GameObject.Instantiate<GameObject>(template);
                indexUIObj.transform.SetParent(template.transform.parent, false);
                indexUIObj.SetActive(true);
            }
        }
        
    }

    private void OnContinentBtnClicked(bool value, Continent continent)
    {
        if (value == true && _isChangeContentWithClick)
        {
            CommonSound.PlayClick();
            ChangeContinent(continent);
        }
    }

    private void OnBackBtnClicked()
    {
        WindowManager.Instance.CloseWindow(WinNames.GallaryPanel,false);
        ExpressController.Instance.GetUIAnimatorEx(WinNames.StartPanel).CrossFade(eExpressUIAnimState.StartPanelIn.ToString(), ExpressConsts.dur_crossfade_openUI);
        System.GC.Collect();
        Resources.UnloadUnusedAssets();
    }

    private void OnlastPageClicked()
    {
        _PageView.LastPage();
    }

    private void OnNextpageClicked()
    {
        _PageView.Nextpage();
    }

    #endregion
}

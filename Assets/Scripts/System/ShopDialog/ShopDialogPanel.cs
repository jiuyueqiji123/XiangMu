﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TableProto;
using System;

public class ShopDialogPanel : MonoBehaviour
{
    public Text title_txt;
    public Text desc_txt;
    public Text payPrice_txt;
    public Image bg;
    private int id;

    private string TexturePath = "texture/shopbuy/";
    MainItemInfo info;
    public void Init(int id)
    {
        this.id = id;
        info = PayManager.Instance.GetItemById(id);
        //title_txt.text = info.name;
        Cmd.CourseInfo cInfo = PayManager.Instance.GetNetInfo((int)this.info.id);
        int price;
        if (cInfo != null)
        {
            //title_txt.text = cInfo.name;
#if UNITY_IOS
            price = this.info.price_ios;
#else
            price = cInfo.price;
#endif
        }
        else
        {
#if UNITY_IOS
            price = this.info.price_ios;
#else
            price = this.info.price;
#endif
        }
        float fprice = price / 100f;
        payPrice_txt.text = fprice.ToString() + "元购买";

        //desc_txt.text = info.desc;

        //Debug.Log("<color=green>"+TexturePath + info.shop_bg +"</color>");
        bg.sprite = ResourceManager.Instance.GetResource(TexturePath + info.shop_bg, typeof(Sprite), enResourceType.Prefab).m_content as Sprite; 
    }

    public void ShopPayClick()
    {
        Debug.Log(" ShopPayClick ===  " + info.id);
        
        PayManager.Instance.GetOrder(this.id);
        /*
        PayManager.Instance.Pay((int)this.info.id, (id, suc) => {
            Debug.Log(id + "," + suc);
            //if (action !=null)
            //    action(id, suc);
            CloseClick();
        });
        */
    }

    public void CloseClick()
    {
        EventBus.Instance.BroadCastEvent(EventID.CANCEL_PAY);
        ShopDialogController.Instance.CloseView();
    }
}

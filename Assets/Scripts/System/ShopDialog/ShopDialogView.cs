﻿using System;
using TableProto;
using UnityEngine;

public class ShopDialogView  
{

    private const string VIEW_PATH = "ui/ui_parent/ShopDialogForm";

    public GameObject form;

    private bool isInit = false;

    private ShopDialogPanel panel;

    public void OpenForm(int id)
    {
        if (this.form == null)
        {
            GameObject go = ResourceManager.Instance.GetResource(VIEW_PATH, typeof(GameObject), enResourceType.ScenePrefab).m_content as GameObject;
            this.form = GameObject.Instantiate(go);
            GameObject.DontDestroyOnLoad(this.form);
        }
        EventBus.Instance.AddEventHandler(EventID.PAY_BUY_SUC, OnBuySuc);
        //this.form = HUIManager.Instance.OpenForm(VIEW_PATH, true);
        this.form.SetActive(true);
        this.Init(id);
    }

    public void CloseForm()
    {
        isInit = false;
        EventBus.Instance.RemoveEventHandler(EventID.PAY_BUY_SUC, OnBuySuc);
        this.form.SetActive(false);
#if UNITY_EDITOR

#elif UNITY_ANDROID
        PayManager.Instance.ShowAds();
#endif
        //HUIManager.Instance.CloseForm(form);
    }

    private void OnBuySuc()
    {
        this.CloseForm();
    }

    private void Init(int id)
    {
        if (!isInit)
        {
            isInit = true;
            this.panel = this.form.transform.Find("Panel").GetComponent<ShopDialogPanel>();
        }
        this.panel.Init(id);
    }
}
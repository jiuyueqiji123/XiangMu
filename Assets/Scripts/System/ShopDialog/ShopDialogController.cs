﻿using UnityEngine;
using System.Collections;
using System;
using TableProto;

public class ShopDialogController : Singleton<ShopDialogController>
{
    public ShopDialogView view;

    public override void Init()
    {
        base.Init();
        this.view = new ShopDialogView();
    }

    public override void UnInit()
    {
        base.UnInit();
    }

    public void OpenView(int id)
    {
        this.view.OpenForm(id);
    }

    public void CloseView()
    {
        this.view.CloseForm();
    }
}

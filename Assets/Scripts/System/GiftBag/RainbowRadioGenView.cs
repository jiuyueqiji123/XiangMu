﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// View用来管理窗口或者其它视图的显示，监听用户输入，然后执行Controller中的逻辑。
/// Controller可以改变view的显示
/// view最好不要引用model对象
/// </summary>
public class RainbowRadioGenView
{
    private const string VIEW_PATH = "rainbow_radio_gen/rainbow_radio_gen_ui/RainbowRadioGenForm";

    private HUIFormScript form;

    public GameObject buttonClose;

    public GameObject buttonDownload;

    public RawImage radioTexture;

    public bool IsVisible
    {
        set { this.form.SetActive(value); }
    }

    public void OpenForm()
    {
        form = HUIManager.Instance.OpenForm(VIEW_PATH, true);
        IsVisible = false;
        this.InitForm();
    }

    public void CloseForm()
    {
        HUIManager.Instance.CloseForm(form);
    }

    public void InitForm()
    {
        Transform formTrans = this.form.transform;

        this.buttonClose = form.GetWidget((int)enRainRadioGenWidget.Btn_Close);
        HUIUtility.SetUIMiniEvent(buttonClose, enUIEventType.Click, enUIEventID.RainbowRadioGen_Button_Close);
        this.buttonDownload = form.GetWidget((int)enRainRadioGenWidget.Btn_Download);
        HUIUtility.SetUIMiniEvent(buttonDownload, enUIEventType.Click, enUIEventID.RainbowRadioGen_Button_Download);

        radioTexture = formTrans.Find("Panel/BG").GetComponent<RawImage>();
    }

    public void BlockInput()
    {
        GraphicRaycaster gr = this.form.GetGraphicRaycaster();
        if (gr != null)
        {
            gr.enabled = false;
        }
    }
}

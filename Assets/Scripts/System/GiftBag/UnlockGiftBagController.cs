﻿using System.Collections;
using System.Collections.Generic;
using TableProto;
using UnityEngine;
using System;

/// <summary>
/// 单例，实例化view和model，负责更新view，监听model的变化，处理service的变化。
/// 
/// </summary>
public class UnlockGiftBagController : Singleton<UnlockGiftBagController>,IPopPanel {

    const int unlockgift_id = 10001;

    Action _finishcallback;

	private UnlockGiftBagView view;

    MainItemInfo info;

	private UserService service;

    private bool bLockInput;

    public bool LockInput
    {
        set
        {
            this.bLockInput = value;
        }
    }

	public override void Init ()
	{
		base.Init ();
        this.view = new UnlockGiftBagView();
		this.service = new UserService ();

        info = TableProtoLoader.MainItemInfoDict[unlockgift_id];

        HUIEventManager.Instance.AddUIEventListener(enUIEventID.UnlockBag_Button_Buy, this.OnBuyBtnClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.UnlockBag_Button_Close, this.CloseView);
    }

	public override void UnInit ()
	{
		base.UnInit ();

        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.UnlockBag_Button_Buy, this.OnBuyBtnClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.UnlockBag_Button_Close, this.CloseView);
    }

	public void OpenView(Action finishcallback = null) {

        if (PayManager.Instance.IsBuyItem(unlockgift_id))
        {
            CloseView();
            return;
        }

        this._finishcallback = finishcallback;
		this.view.OpenForm ();
        bLockInput = false;

        view.text_img.SetActive(SDKManager.Instance.isGiftText());
        view.buttonBuyText.text = info.price + "元购买";
    }

    public void CloseView(HUIEvent evt = null)
    {
        this.view.CloseForm();
        if (_finishcallback != null) _finishcallback();
    }

    private void BlockInput()
    {
        this.view.BlockInput();
    }


    #region uievent

    private void OnBuyBtnClick(HUIEvent evt = null)
    {
        Debug.Log("购买");
        PayManager.Instance.PayDirectly(unlockgift_id, (id, suc) => {
            if (suc)
            {
                //购买成功
                Debug.LogError("购买成功");
                CloseView();
            }
        }, true, null);

    }

    #endregion

    #region event

    #endregion

}

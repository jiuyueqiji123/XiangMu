﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// View用来管理窗口或者其它视图的显示，监听用户输入，然后执行Controller中的逻辑。
/// Controller可以改变view的显示
/// view最好不要引用model对象
/// </summary>
public class UnlockGiftBagView
{

    private const string VIEW_PATH = "unlockallgift_bag/unlockallgift_bag_ui/UnlockAllGiftForm";

    private HUIFormScript form;

    public GameObject buttonClose;

    public GameObject buttonBuy;

    public Text buttonBuyText;

    public GameObject text_img;

    public void OpenForm()
    {
        form = HUIManager.Instance.OpenForm(VIEW_PATH, true);
        this.InitForm();
    }

    public void CloseForm()
    {
        HUIManager.Instance.CloseForm(form);
    }

    private void InitForm()
    {
        Transform formTrans = this.form.transform;

        this.buttonBuy = form.GetWidget((int)enUnlockBagWidget.Btn_Buy);
        HUIUtility.SetUIMiniEvent(buttonBuy, enUIEventType.Click, enUIEventID.UnlockBag_Button_Buy);
        this.buttonClose = form.GetWidget((int)enUnlockBagWidget.Btn_Close);
        HUIUtility.SetUIMiniEvent(buttonClose, enUIEventType.Click, enUIEventID.UnlockBag_Button_Close);

        buttonBuyText = buttonBuy.transform.GetComponentInChildren<Text>();
        text_img = formTrans.Find("PanelGift/Img_text").gameObject;
    }

    public void BlockInput()
    {
        GraphicRaycaster gr = this.form.GetGraphicRaycaster();
        if (gr != null)
        {
            gr.enabled = false;
        }
    }
   
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPopPanel
{
    void OpenView(System.Action callback = null);
    void CloseView(HUIEvent evt = null);
}

public class PopPanelManager : MonoSingleton<PopPanelManager> {

    private List<IPopPanel> _poppanels;
    bool isPopStart = false;

    protected override void Init()
    {
        base.Init();
        _poppanels = new List<IPopPanel>();
    }

    public void AddPopPanel(IPopPanel poppanel)
    {
        _poppanels.Add(poppanel);
    }

    public void RemovePopPanel(IPopPanel poppanel)
    {
        if (_poppanels.Contains(poppanel))
            _poppanels.Remove(poppanel);
    }

    private void Update()
    {
        if (_poppanels.Count > 0)
        {
            if (!isPopStart)
            {
                IPopPanel pop = _poppanels[0];
                pop.OpenView(OnPopPanelClose);
                _poppanels.RemoveAt(0);

                isPopStart = true;
            }
        }
    }

    private void OnPopPanelClose()
    {
        TimerManager.Instance.AddTimer(.3f, () => {
            isPopStart = false;
        });
    }
}

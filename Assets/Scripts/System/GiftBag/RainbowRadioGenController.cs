﻿using System.Collections;
using System.Collections.Generic;
using TableProto;
using UnityEngine;
using System;

/// <summary>
/// 单例，实例化view和model，负责更新view，监听model的变化，处理service的变化。
/// 
/// </summary>
public class RainbowRadioGenController : Singleton<RainbowRadioGenController>,IPopPanel {

    Action _finishcallback;

    private RainbowRadioGenView view;

    private UserService service;

    private bool bLockInput;

    public bool LockInput
    {
        set
        {
            this.bLockInput = value;
        }
    }

    public override void Init()
    {
        base.Init();
        this.view = new RainbowRadioGenView();
        this.service = new UserService();

        HUIEventManager.Instance.AddUIEventListener(enUIEventID.RainbowRadioGen_Button_Close, this.CloseView);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.RainbowRadioGen_Button_Download, this.OnDownloadBtnClick);
    }

    public override void UnInit()
    {
        base.UnInit();

        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.RainbowRadioGen_Button_Close, this.CloseView);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.RainbowRadioGen_Button_Download, this.OnDownloadBtnClick);

    }

    public void OpenView(Action finishcallback = null)
    {
        this._finishcallback = finishcallback;
        this.view.OpenForm();
        bLockInput = false;

        string localpath = FileTools.CombinePath(GiftBagResManager.Instance.ResPath, FileTools.GetFullName(GiftBagResManager.url_popImage));
        if (FileTools.IsFileExist(localpath))
        {
            GiftBagResManager.Instance.LoadTexture(localpath, (tex) =>
            {
                this.view.radioTexture.texture = tex;
                this.view.IsVisible = true;
            });
        }
        else
        {
            CloseView();
        }
    }

    public void CloseView(HUIEvent evt = null)
    {
        this.view.CloseForm();
        if (_finishcallback != null) _finishcallback();
    }

    private void BlockInput()
    {
        this.view.BlockInput();
    }


    #region uievent

    private void OnDownloadBtnClick(HUIEvent evt = null)
    {
        Debug.Log("下载");
        CloseView();
        SDKManager.Instance.NativeDownload(GiftBagResManager.url_apk);
    }

    #endregion

    #region event

    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VersionUpdateController : Singleton<VersionUpdateController> {

	private VersionUpdateView view;

	public override void Init ()
	{
		base.Init ();
		this.view = new VersionUpdateView ();

		EventBus.Instance.AddEventHandler<float, string> (EventID.VERSION_UPDATE_PROGRESS_CHANGE, this.OnChangeProgress);
		EventBus.Instance.AddEventHandler<string> (EventID.VERSION_UPDATE_DES_CHANGE, this.OnChangeDes);
	}

	public override void UnInit ()
	{
		base.UnInit ();
		EventBus.Instance.RemoveEventHandler<float, string> (EventID.VERSION_UPDATE_PROGRESS_CHANGE, this.OnChangeProgress);
		EventBus.Instance.RemoveEventHandler<string> (EventID.VERSION_UPDATE_DES_CHANGE, this.OnChangeDes);
	}

	public void OpenView() {
		this.view.OpenForm ();
	}

	public void CloseView() {
		this.view.CloseForm ();
	}

    public void ShowSlider()
    {
        this.view.ShowSlider();
    }

	#region event
	private void OnChangeProgress(float progress, string count) {
		this.view.ChangeSlider (progress, count);
	}

	private void OnChangeDes(string des) {
		this.view.ChangeDescription (des);
	}
	#endregion
}

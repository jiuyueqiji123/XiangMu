﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enVersionUpdateState {
	None,
	StartCheckPathPermission,
	CheckPathPermission,
	StartUnzipResource,
	UnzipResource,
	StartCheckAppVersion,
	CheckAppVersion,
	StartCheckResourceVersion,
	CheckResourceVersion,
	StartDownloadResource,
	DownloadResource,
	StartUnZipDownloadResource,
	UnzipDownloadResource,
	Complete,
	End
}

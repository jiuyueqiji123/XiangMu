﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enUpdateFormWidget {
	Text_UpdateInfo,
	Text_UpdateCount,
	Slider_Progress,
    Slider,
}

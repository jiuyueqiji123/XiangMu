﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionUpdateView  {

	private const string VIEW_PATH = "ui/ui_update/UpdateForm";

	HUIFormScript form;

	private Text text_Des;
	private Text text_Count;
	private Image slider_Progress;
    private GameObject slider;

	public void OpenForm() {
		form = HUIManager.Instance.OpenForm (VIEW_PATH, false);

		text_Des = form.GetWidget ((int)enUpdateFormWidget.Text_UpdateInfo).GetComponent<Text> ();
		text_Count = form.GetWidget ((int)enUpdateFormWidget.Text_UpdateCount).GetComponent<Text> ();
		slider_Progress = form.GetWidget ((int)enUpdateFormWidget.Slider_Progress).GetComponent<Image> ();
        slider = form.GetWidget((int)enUpdateFormWidget.Slider).gameObject;
        slider.SetActive(false);

		ChangeSlider (0f, "");
		ChangeDescription ("");
	}

    public void ShowSlider()
    {
        this.slider.SetActive(true);
    }

	public void ChangeSlider(float progress, string count) {
		slider_Progress.fillAmount = progress;
		text_Count.text = count;
	}

	public void ChangeDescription(string des) {
		text_Des.text = des;
	}

	public void CloseForm() {
		HUIManager.Instance.CloseForm (form);
	}
}

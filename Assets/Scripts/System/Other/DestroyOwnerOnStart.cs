﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOwnerOnStart : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        Destroy(this.gameObject);
	}
	
}

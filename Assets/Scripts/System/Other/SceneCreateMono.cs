﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneCreateMono : MonoBehaviour {


    // 场景源资源文件
    public GameObject[] sceneAssetArray;

    // 场景坐标数据目标（待删除）
    public GameObject[] sceneCreateAsset;


    public void CreateScene()
    {
        List<Transform> childList = new List<Transform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name != "Camera")
                childList.Add(transform.GetChild(i));
        }
        foreach (Transform tt in childList)
        {
            DestroyImmediate(tt.gameObject);
        }
        childList.Clear();

        Dictionary<string, GameObject> assetDict = new Dictionary<string, GameObject>();
        for (int i = 0; i < sceneAssetArray.Length; i++)
        {
            assetDict.Add(sceneAssetArray[i].name, sceneAssetArray[i]);
        }

        foreach (GameObject go in sceneCreateAsset)
        {
            if (assetDict.ContainsKey(go.name))
            {
                GameObject target = assetDict[go.name];

                GameObject CreateTarget = Instantiate(target);
                CreateTarget.transform.parent = transform;
                CreateTarget.name = go.name;
                CreateTarget.transform.position = go.transform.position;
                CreateTarget.transform.rotation = go.transform.rotation;
                CreateTarget.transform.localScale = go.transform.localScale;
            }
            else
            {
                Debug.LogError("找不到对应资源：" + go.name);
            }
        }


    }

    public void ClearAsset()
    {
        sceneAssetArray = new GameObject[] { };
        sceneCreateAsset = new GameObject[] { };
    }


    public Transform createTarget;
    public float xOffset;
    public int count;
    public void CreateTest()
    {
        for (int i = 0; i < count; i++)
        {
            Transform target = Instantiate(createTarget);
            target.parent = createTarget.parent;
            target.position = createTarget.position + new Vector3(xOffset * (i + 1), 0, 0);
            target.name = createTarget.name + "_" + (i + 2);
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnglishWindow : SingletonBaseWindow<EnglishWindow>
{



    //private MainWindowModel model;

    #region ui property

    public Button _Btn_Return { get; set; }
    public Button _Btn_Next { get; set; }
    public Transform _Mask { get; set; }

    #endregion

    private bool isLockInput;

    private System.Action onNextCallback;


    protected override void OnOpen(params object[] paramArray)
    {
        _Mask.gameObject.SetActive(false);
    }

    protected override void OnRefresh()
    {
        base.OnRefresh();

        isLockInput = false;
    }

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
        _Btn_Next.onClick.AddListener(OnBtnNextClick);
        EventBus.Instance.AddEventHandler<string>(EventID.ADS_DONE_CALLBACK, OnAdsCallback);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
        _Btn_Next.onClick.RemoveAllListeners();
        EventBus.Instance.RemoveEventHandler<string>(EventID.ADS_DONE_CALLBACK, OnAdsCallback);
    }

    private void OnAdsCallback(string adsName)
    {
        if (adsName == AdsNameConst.Ads_English.Name)
        {
            StudyEnglishProgressInfo info = LocalDataManager.Instance.Load<StudyEnglishProgressInfo>(ELocalDataType.StudyEnglishGame);
            info.adsCount++;
            LocalDataManager.Instance.Save(ELocalDataType.StudyEnglishGame, info);
            OnRefresh();
        }
    }

    #region uievent

    private void OnButtonReturnClick()
    {
        if (EasyLoadingEx.IsLoading)
            return;
        //Debug.LogError("退出游戏");
        EventBus.Instance.BroadCastEvent(EventID.ON_ENGLISH_WINDOW_BTN_RETURN_CLICK);
    }

    private void OnBtnNextClick()
    {
        if (isLockInput)
            return;

        isLockInput = true;
        bool isBuy = PayManager.Instance.IsBuyItem((int)MainItemEnum.StudyEnglish);

        if (isBuy)
        {
            if (onNextCallback != null)
            {
                onNextCallback();
            }
        }
        else
        {
#if UNITY_IOS
            if (StudyEnglishManager.Instance.playingLetter == EnglishSelectLetterWindow.ADS_INDEX - 1)
            {
                StudyEnglishProgressInfo info = LocalDataManager.Instance.Load<StudyEnglishProgressInfo>(ELocalDataType.StudyEnglishGame);

                if (info.adsCount == 0)
                {
                    TopNoticeManager.Instance.ShowOkTip(EnglishSelectLetterWindow.ADS_TIPS, () =>
                    {
                        if (AdsManager.Instance.AdsReady(AdsNameConst.Ads_English))
                        {
                            AdsManager.Instance.ShowAds(AdsNameConst.Ads_English);
                        }
                        else
                        {
                            Debug.Log("广告还未准备好. name:" + AdsNameConst.Ads_English.Name);
                            OnRefresh();
                        }
                    }, OnRefresh);
                    return;
                }
            }
#endif

            RemoveBuyEvent();
            if (StudyEnglishManager.Instance.playingLetter == EnglishSelectLetterWindow.BUY_INDEX - 1)
            {
                EventBus.Instance.AddEventHandler(EventID.CANCEL_VERITY, this.OnCancelBuy);
                EventBus.Instance.AddEventHandler(EventID.CANCEL_PAY, this.OnCancelBuy);
                PayManager.Instance.Pay((int)MainItemEnum.StudyEnglish, (id, suc) =>
                {
                    RemoveBuyEvent();
                    isLockInput = false;
                });
            }
            else
            {
                if (onNextCallback != null)
                {
                    onNextCallback();
                }
            }
        }

    }

    #endregion

    private void RemoveBuyEvent()
    {
        EventBus.Instance.RemoveEventHandler(EventID.CANCEL_VERITY, this.OnCancelBuy);
        EventBus.Instance.RemoveEventHandler(EventID.CANCEL_PAY, this.OnCancelBuy);
    }

    private void OnCancelBuy()
    {
        RemoveBuyEvent();
        isLockInput = false;
    }

    public void AddNextEvent(System.Action callback)
    {
        isLockInput = false;
        onNextCallback = callback;
        _Mask.SetActive(true);
        _Btn_Next.gameObject.SetActive(true);
    }

    public void HideMaskAndButton()
    {
        _Mask.SetActive(false);
        _Btn_Next.gameObject.SetActive(false);
    }



}

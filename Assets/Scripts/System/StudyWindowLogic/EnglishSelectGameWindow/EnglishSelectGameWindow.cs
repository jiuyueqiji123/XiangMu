﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;

public class EnglishSelectGameWindow : SingletonBaseWindow<EnglishSelectGameWindow>
{


    #region ui property

    public Button _Btn_Return { get; set; }
    public Button _Btn_Game1 { get; set; }
    public Button _Btn_Game2 { get; set; }
    public Button _Btn_Game3 { get; set; }
    public Button _Btn_Game4 { get; set; }

    public Transform _Btn_Game1_Mask { get; set; }
    public Transform _Btn_Game2_Mask { get; set; }
    public Transform _Btn_Game3_Mask { get; set; }
    public Transform _Btn_Game4_Mask { get; set; }
    public Transform _Lawn_Game2_Bg { get; set; }
    public Transform _Paddyfield_Game2_Bg { get; set; }
    public Transform _Sea_Game2_Bg { get; set; }
    public Transform _Snow_Game2_Bg { get; set; }
    public Transform _Lawn_Game3_Bg { get; set; }
    public Transform _Paddyfield_Game3_Bg { get; set; }
    public Transform _Sea_Game3_Bg { get; set; }
    public Transform _Snow_Game3_Bg { get; set; }

    public Image _Game1_Word_Image { get; set; }
    public Text _Game1_Word_Text { get; set; }
    public Text _Lawn_Game2_Text { get; set; }
    public Text _Paddyfield_Game2_Text { get; set; }
    public Text _Sea_Game2_Text { get; set; }
    public Text _Snow_Game2_Text { get; set; }
    public Text _Lawn_Game3_Text { get; set; }
    public Text _Paddyfield_Game3_Text { get; set; }
    public Text _Sea_Game3_Text { get; set; }
    public Text _Snow_Game3_Text { get; set; }

    public Text _Title_Text { get; set; }
    public Image _Game4_LetterImage { get; set; }

    #endregion

    private int letterIndex;
    private uint dataID;
    private bool m_isSelect;


    protected override void OnOpen(params object[] paramArray)
    {
        if (paramArray.Length > 0)
        {
            letterIndex = (int)paramArray[0];
            dataID = FEnglishTool.GetEnglishDataID(letterIndex);
        }
        InitPanel();

        //Debug.Log("letterID:" + letterID + "  dataID:" + dataID);

        this.PlayMusicEx(StudyAudioName.t_30002);
        m_isSelect = false;

    }

    protected override void OnRefresh()
    {
        RefreshGameLockState();
    }

    protected override void OnClose()
    {
        //this.StopMusicEx();
        m_isSelect = false;
    }


    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
        _Btn_Game1.onClick.AddListener(On_Btn_Game1_Click);
        _Btn_Game2.onClick.AddListener(On_Btn_Game2_Click);
        _Btn_Game3.onClick.AddListener(On_Btn_Game3_Click);
        _Btn_Game4.onClick.AddListener(On_Btn_Game4_Click);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
        _Btn_Game1.onClick.RemoveAllListeners();
        _Btn_Game2.onClick.RemoveAllListeners();
        _Btn_Game3.onClick.RemoveAllListeners();
        _Btn_Game4.onClick.RemoveAllListeners();
    }

    #region uievent

    private void OnButtonReturnClick()
    {
        //WindowManager.Instance.CloseWindow(WinNames.DrawGeometryPanel);
        if (EasyLoadingEx.IsLoading)
            return;
        EventBus.Instance.BroadCastEvent<int, EEnglishGameEnum>(EventID.ON_ENGLISH_SELECT_GAME, letterIndex, EEnglishGameEnum.None);
    }

    private void On_Btn_Game1_Click()
    {
        if (m_isSelect) return;
        m_isSelect = true;
        EventBus.Instance.BroadCastEvent<int, EEnglishGameEnum>(EventID.ON_ENGLISH_SELECT_GAME, letterIndex, EEnglishGameEnum.Game1);
    }

    private void On_Btn_Game2_Click()
    {
        if (m_isSelect) return;
        m_isSelect = true;
        EventBus.Instance.BroadCastEvent<int, EEnglishGameEnum>(EventID.ON_ENGLISH_SELECT_GAME, letterIndex, EEnglishGameEnum.Game2);
    }

    private void On_Btn_Game3_Click()
    {
        if (m_isSelect) return;
        m_isSelect = true;
        EventBus.Instance.BroadCastEvent<int, EEnglishGameEnum>(EventID.ON_ENGLISH_SELECT_GAME, letterIndex, EEnglishGameEnum.Game3);
    }

    private void On_Btn_Game4_Click()
    {
        if (m_isSelect) return;
        m_isSelect = true;
        EventBus.Instance.BroadCastEvent<int, EEnglishGameEnum>(EventID.ON_ENGLISH_SELECT_GAME, letterIndex, EEnglishGameEnum.Game4);
    }

    #endregion

    private void InitPanel()
    {
        EnglishLevelInfo info = FTableManager.GetEnglishLevelInfo(dataID);
        if (info == null)
        {
            return;
        }

        _Title_Text.text = info.name;

        _Game1_Word_Text.text = info.word;

        _Lawn_Game2_Text.text = info.name;
        _Paddyfield_Game2_Text.text = info.name;
        _Sea_Game2_Text.text = info.name;
        _Snow_Game2_Text.text = info.name;

        _Lawn_Game3_Text.text = info.word;
        _Paddyfield_Game3_Text.text = info.word;
        _Sea_Game3_Text.text = info.word;
        _Snow_Game3_Text.text = info.word;

        //Sprite sprite = UISpriteManager.Instance.GetSprite(emUIAltas.English_SelectGame_Letter, info.name + "-write"); 
        Sprite sprite = Res.LoadSprite(StudyPath.english_selectgame_letter_basepath + info.name + "-write");
        if (sprite == null)
            Debug.LogError("Load sprite error! name:" + info.name + "-write");
        _Game4_LetterImage.sprite = sprite;
        _Game4_LetterImage.SetNativeSize();

        _Lawn_Game2_Bg.SetActive(false);
        _Paddyfield_Game2_Bg.SetActive(false);
        _Sea_Game2_Bg.SetActive(false);
        _Snow_Game2_Bg.SetActive(false);

        _Lawn_Game3_Bg.SetActive(false);
        _Paddyfield_Game3_Bg.SetActive(false);
        _Sea_Game3_Bg.SetActive(false);
        _Snow_Game3_Bg.SetActive(false);

        EEnglishSceneType playmode = (EEnglishSceneType)info.play_mode;
        switch (playmode)
        {
            case EEnglishSceneType.Lawn:
                _Lawn_Game2_Bg.SetActive(true);
                _Lawn_Game3_Bg.SetActive(true);
                break;
            case EEnglishSceneType.Sea:
                _Sea_Game2_Bg.SetActive(true);
                _Sea_Game3_Bg.SetActive(true);
                break;
            case EEnglishSceneType.Paddyfield:
                _Paddyfield_Game2_Bg.SetActive(true);
                _Paddyfield_Game3_Bg.SetActive(true);
                break;
            case EEnglishSceneType.Snow:
                _Snow_Game2_Bg.SetActive(true);
                _Snow_Game3_Bg.SetActive(true);
                break;
        }
    }

    private void RefreshGameLockState()
    {

        StudyEnglishProgressInfo info = LocalDataManager.Instance.Load<StudyEnglishProgressInfo>(ELocalDataType.StudyEnglishGame);

        Button[] btnArray = new Button[4] { _Btn_Game1, _Btn_Game2, _Btn_Game3, _Btn_Game4};
        Transform[] maskArray = new Transform[4] { _Btn_Game1_Mask, _Btn_Game2_Mask, _Btn_Game3_Mask, _Btn_Game4_Mask };

        if (letterIndex < info.letterProgress)
        {
            for (int i = 0; i < btnArray.Length; i++)
            {
                btnArray[i].enabled = true;
                maskArray[i].gameObject.SetActive(false);
            }
        }
        else if (letterIndex == info.letterProgress)
        {
            for (int i = 0; i < btnArray.Length; i++)
            {
                bool isEnable = i <= info.letterGameProgress;
                btnArray[i].enabled = isEnable;
                maskArray[i].gameObject.SetActive(!isEnable);
            }
        }
        else
        {
            for (int i = 0; i < btnArray.Length; i++)
            {
                btnArray[i].enabled = false;
                maskArray[i].gameObject.SetActive(true);
            }
        }

    }


}

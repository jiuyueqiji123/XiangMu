﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MathNumberWindow : SingletonBaseWindow<MathNumberWindow>
{


    #region ui property

    public Button _Btn_Return { get; set; }

    #endregion

    private bool isLockInput;

    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        isLockInput = false;
    }

    protected override void OnClose()
    {
        base.OnClose();

        isLockInput = true;
    }

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
    }


    public void UpdateLockInputState(bool value)
    {
        isLockInput = value;
    }

    #region ui event

    private void OnButtonReturnClick()
    {
        if (EasyLoadingEx.IsLoading || isLockInput)
            return;
        Debug.LogError("认识数字 返回事件");

        //GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
        EventBus.Instance.BroadCastEvent(EventID.ON_MATHNUMBER_WINDOW_BTN_RETURN_CLICK);
    }

    #endregion
    

}

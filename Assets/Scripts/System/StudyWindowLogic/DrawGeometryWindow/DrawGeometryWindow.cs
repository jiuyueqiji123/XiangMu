﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawGeometryWindow : BaseDrawWindow<DrawGeometryWindow>
{


    #region ui property

    public Button _Btn_Return { get; set; }

    public Transform _Brush { get; set; }
    public Transform _Obj_Parent { get; set; }
    public Transform _AnimParent { get; set; }

    public Image _Mask { get; set; }
    public Image _Sign { get; set; }

    #endregion


    private const string objBasePath = StudyPath.math_drawgeometryBasePath;


    private GameObject m_LoadObj;    // 加载的资源
    private System.Action<bool> completeCallback;

    

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
    }

    protected override void OnClose()
    {
        if (m_LoadObj != null)
            GameObject.Destroy(m_LoadObj);
    }


    protected override void PlayBrushAnimation()
    {
        if (isBrushPlaying)
            return;
        isBrushPlaying = true;
        PlayBrushAudio();
    }

    protected override void StopBrushAnimation()
    {
        if (!isBrushPlaying)
            return;

        StopBrushAudio();
        isBrushPlaying = false;
    }

    protected override void PlayBrushAudio()
    {
        if (m_audio == null)
        {
            m_audio = this.PlayAudioEx(StudyAudioName.t_21028, true);
        }
    }


    #region uievent

    private void OnButtonReturnClick()
    {
        DrawRealize2D.DestroyInstance();
        if (completeCallback != null)
        {
            completeCallback(false);
            completeCallback = null;
        }

        WindowManager.Instance.CloseWindow(WinNames.DrawGeometryPanel);
    }

    #endregion

    public void StartDrawGeometry(EGeometryEnum geometryType, System.Action<bool> callback = null)
    {
        if (m_LoadObj != null)
        {
            GameObject.Destroy(m_LoadObj);
            m_LoadObj = null;
        }

        m_LoadObj = LoadObj(geometryType.ToString());
        if (m_LoadObj == null)
        {
            bIsDrawing = false;
            //if (callback != null)
            //{
            //    callback(false);
            //}
            return;
        }

        Vector3 scale = transform.localScale;
        transform.SetScale(1);

        Transform brush = _Brush;
        
        this.PlayAudioEx(StudyAudioName.t_22017);
        DrawRealize2D.Instance.DrawNumber(m_LoadObj.transform, brush, _Mask.transform, _Sign, WindowManager.Instance.canvas, _AnimParent, () =>
        {
            GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_21024);
            this.AddTimerEx(audio.Length, () =>
            {

                PlayGeometryAudio(geometryType, () =>
                {
                    this.AddTimerEx(1000, () =>
                    {
                        if (!bIsDrawing)
                            return;
                        if (completeCallback != null)
                        {
                            completeCallback(true);
                        }
                    });
                });
            });
        });


        completeCallback = callback;
        bIsDrawing = true;
        isBrushPlaying = true;
        m_spriteSeqArray = brush.GetComponentsInChildren<SpriteSequenceFrame>(true);
        StopBrushAnimation();

        DrawRealize2D.Instance.Pause();
        this.AddTimerEx(0.5f, () => {
            DrawRealize2D.Instance.Resume();
        });

        transform.localScale = scale;
    }

    private GameObject LoadObj(string name)
    {
        string path = objBasePath + "Geometry_" + name;

        GameObject go = Res.LoadObj(path);
        if (go == null)
            return null;

        go.transform.SetParent(_Obj_Parent);
        RectTransform rect = go.transform as RectTransform;
        rect.anchoredPosition3D = Vector3.zero;
        rect.sizeDelta = Vector2.zero;
        rect.localScale = Vector3.one;
        rect.localRotation = Quaternion.identity;

        return go;
    }

    protected void PlayGeometryAudio(EGeometryEnum type, System.Action callback = null)
    {
        GameAudioSource audio = null;

        switch (type)
        {
            case EGeometryEnum.Circle:
                audio = this.PlayAudioEx(StudyAudioName.t_22011);
                break;
            case EGeometryEnum.Square:
                audio = this.PlayAudioEx(StudyAudioName.t_22013);
                break;
            case EGeometryEnum.Triangle:
                audio = this.PlayAudioEx(StudyAudioName.t_22012);
                break;
            case EGeometryEnum.Rectangle:
                audio = this.PlayAudioEx(StudyAudioName.t_22014);
                break;
            case EGeometryEnum.Trapezoid:
                audio = this.PlayAudioEx(StudyAudioName.t_22015);
                break;
            case EGeometryEnum.Ellipse:
                audio = this.PlayAudioEx(StudyAudioName.t_22016);
                break;
            default:
                audio = this.PlayAudioEx(StudyAudioName.t_22011);
                break;
        }

        if (callback != null)
            this.AddTimerEx(audio.Length, callback);
    }


}

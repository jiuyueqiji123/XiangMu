﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;
using WCBG.ToolsForUnity.Extension;
using WCBG.ToolsForUnity.Tools;

public class BaseDrawWindow<T> : SingletonBaseWindow<T> where T : class, new()
{

    protected GameAudioSource m_audio;
    protected SpriteSequenceFrame[] m_spriteSeqArray;

    protected bool bIsDrawing = false;
    protected bool isBrushPlaying;
    protected bool isBrushEnter;

    protected float delayTime = 0.2f;
    protected float addTime;


    protected override void OnUpdate()
    {

        if (bIsDrawing)
        {
            if (DrawRealize2D.Instance.IsBrushMoving)
            {
                PlayBrushAnimation();
            }
            else
            {
                if (!isBrushEnter && isBrushPlaying)
                {
                    addTime += Time.deltaTime;
                    if (addTime > delayTime)
                    {
                        addTime = 0;
                        StopBrushAnimation();
                    }
                }
            }
        }
    }

    protected void OnMouseUp()
    {
        StopBrushAnimation();
        isBrushEnter = false;
    }

    protected void OnBrushDown()
    {
        isBrushEnter = true;
        PlayBrushAnimation();
    }

    protected void OnBrushEnter()
    {
        isBrushEnter = true;
    }

    protected void OnBrushExit()
    {
        isBrushEnter = false;
    }

    protected virtual void PlayBrushAudio()
    {

    }

    protected virtual void StopBrushAudio()
    {
        if (m_audio != null)
        {
            m_audio.Stop();
            m_audio = null;
        }
    }

    protected virtual void PlayBrushAnimation()
    {
        if (isBrushPlaying)
            return;
        isBrushPlaying = true;
        PlayBrushAudio();

        if (m_spriteSeqArray != null)
        {
            foreach (SpriteSequenceFrame seq in m_spriteSeqArray)
            {
                if (seq.transform.name == "BrushImage")
                {
                    seq.Play();
                }
                else
                {
                    seq.gameObject.SetActive(true);
                    seq.Play();
                }
            }
        }
    }

    protected virtual void StopBrushAnimation()
    {
        if (!isBrushPlaying)
            return;

        StopBrushAudio();
        isBrushPlaying = false;

        if (m_spriteSeqArray != null)
        {
            foreach (SpriteSequenceFrame seq in m_spriteSeqArray)
            {
                if (seq.transform.name == "BrushImage")
                {
                    seq.Stop();
                }
                else
                {
                    seq.gameObject.SetActive(false);
                    seq.Stop();
                }
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;


public class ReadLetterWindow : SingletonBaseWindow<ReadLetterWindow>
{

    #region ui property

    public Button _Btn_Return { get; set; }
    public Button _Btn_Audio { get; set; }

    public Transform _Animation1 { get; set; }
    public Transform _Animation2 { get; set; }

    public Text _LetterText { get; set; }
    public Image _Target1 { get; set; }
    public Text _Target1_Text_Head { get; set; }
    public Text _Target1_Text_End { get; set; }

    #endregion

    private System.Action audioCallback;

    private uint m_dataID;

    private bool bViewState;

    private bool bNextAnimatoin;


    protected override void OnOpen(params object[] paramArray)
    {
        bViewState = true;
        bNextAnimatoin = true;
    }

    protected override void OnClose()
    {
        bViewState = false;
        bNextAnimatoin = false;
    }

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
        _Btn_Audio.onClick.AddListener(OnButtonAudioClick);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
        _Btn_Audio.onClick.RemoveAllListeners();
    }

    public void InitializeData(uint dataID)
    {
        if (dataID == 0)
            return;
        m_dataID = dataID;
        //EnglishLetterInfo info = TableProtoLoader.EnglishLetterInfoDict[dataID];

        //_Target1.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EnglishLearn, info.word_texture1);
        //_Target1_Text_Head.text = info.word1.Substring(0, 1);
        //_Target1_Text_End.text = info.word1.Substring(1);

        _Animation1.gameObject.SetActive(false);
        _Animation2.gameObject.SetActive(false);
        _Btn_Audio.gameObject.SetActive(false);

    }

    public void StartReadLetter(System.Action callback)
    {
        audioCallback = callback;
        ReadLetter1();
        bNextAnimatoin = true;
    }

    private void ReadLetter1()
    {
        _Animation1.gameObject.SetActive(true);
        EnglishLetterInfo info = TableProtoLoader.EnglishLetterInfoDict[m_dataID];
        // 乐迪说：XX，跟我一起读A  固定音频组合

        ////XX，跟我一起读
        //PlayEnglishAudio(StudyAudioName.talk_403010208, () => {
        //    // A
        //    PlayEnglishAudio(info.letter_big_audio, () =>
        //    {
        //        // 乐迪说：A
        //        PlayEnglishAudio(info.letter_big_audio, () =>
        //        {

        //            _Btn_Audio.gameObject.SetActive(true);
        //        });

        //    }, 0.5f);
        //});
    }

    private void ReadLetter2()
    {
        _Animation2.gameObject.SetActive(true);

        //EnglishLetterInfo info = TableProtoLoader.EnglishLetterInfoDict[m_dataID];
        // 乐迪说：XX，跟我一起读A  固定音频组合

        ////XX，跟我一起读
        //PlayEnglishAudio(StudyAudioName.talk_403010208, () =>
        //{
        //    // A
        //    PlayEnglishAudio(info.word1_audio, () =>
        //    {
        //        // 乐迪说：A
        //        PlayEnglishAudio(info.word1_audio, () =>
        //        {
        //            bNextAnimatoin = false;
        //            _Btn_Audio.gameObject.SetActive(true);
        //        });

        //    }, 0.5f);
        //});
    }


    #region uievent

    private void OnButtonReturnClick()
    {
        WindowManager.Instance.CloseWindow(WinNames.DrawNumberPanel);
    }

    private void OnButtonAudioClick()
    {
        _Btn_Audio.gameObject.SetActive(false);

        if (bNextAnimatoin)
        {
            ReadLetter2();
        }
        else
        {
            if (audioCallback != null)
            {
                audioCallback();
            }
        }
    }

    #endregion

}

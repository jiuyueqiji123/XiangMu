﻿/*----------------------------------------------------------------
// Copyright (C) 2018 
// author：meng cheng xin
// data：2018/3/2
// describe： ugui holder 
//----------------------------------------------------------------*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UGUIHolder : UGUIBaseHolder
{



    protected BaseWindow baseWindow;

    public override void Register(string name, BaseWindow win)
    {
        _Name = name;
        baseWindow = win;
        baseWindow.Register(name, gameObject);
    }

    protected override void OnOpenWindow(params object[] paramArray)
    {
        //if (baseWindow != null)
            //baseWindow.OpenWindow(paramArray);
    }

    protected override void OnRefreshWindow()
    {
        //if (baseWindow != null)
            //baseWindow.RefreshWindow();
    }

    protected override void OnUpdateWindow()
    {
        //if (baseWindow != null && baseWindow.IsOpen)
            //baseWindow.UpdateWindow();
    }

    protected override void OnCloseWindow()
    {
        //if (baseWindow != null)
            //baseWindow.CloseWindow();
    }


}

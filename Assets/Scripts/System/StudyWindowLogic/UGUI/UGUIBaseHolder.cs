﻿/*----------------------------------------------------------------
// Copyright (C) 2018 
// author：meng cheng xin
// data：2018/3/2
// describe： ugui holder base
//----------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public abstract class UGUIBaseHolder : MonoBehaviour {


    public const float FADE_TIME = 0.4f;

    public event System.Action<object[]> OnOpenEvent;
    public event System.Action OnRefreshEvent;
    public event System.Action OnUpdateEvent;
    public event System.Action OnCloseEvent;

    private BaseWindow m_window;

    [SerializeField]
    protected string _Name;
    public string Name
    {
        get { return _Name; }
    }

    private EWindowLayerEnum _WindowLayer;
    public EWindowLayerEnum WindowLayer
    {
        get { return _WindowLayer; }
    }
    
    public bool isOpen
    {
        get;
        private set;
    }

    private EWindowFadeEnum fadeType = EWindowFadeEnum.None;



    public virtual void Register(string name)
    {
        _Name = name;
    }

    public virtual void Register(string name, BaseWindow win)
    {
        _Name = name;
        m_window = win;
    }

    private void Update()
    {
        UpdateWindow();
    }

    public void OpenWindow(EWindowFadeEnum windowFadeType, EWindowLayerEnum windowLayer, params object[] paramArray)
    {
        _WindowLayer = windowLayer;
        isOpen = true;
        gameObject.SetActive(true);

        OnOpenWindow(paramArray);
        if (OnOpenEvent != null)
            OnOpenEvent(paramArray);
        FadeIn(windowFadeType);
    }
    public void RefreshWindow()
    {
        OnRefreshWindow();
        if (OnRefreshEvent != null)
            OnRefreshEvent();
    }
    public void UpdateWindow()
    {
        OnUpdateWindow();
        if (OnUpdateEvent != null)
            OnUpdateEvent();
    }
    public void CloseWindow(System.Action callback = null)
    {
        isOpen = false;

        FadeOut(()=>
        {
            OnCloseWindow();
            if (OnCloseEvent != null)
                OnCloseEvent();
            gameObject.SetActive(false);
            if (callback != null)
                callback();
        });
    }


    protected abstract void OnOpenWindow(params object[] paramArray);
    protected abstract void OnRefreshWindow();
    protected abstract void OnUpdateWindow();
    protected abstract void OnCloseWindow();


    private void FadeIn(EWindowFadeEnum windowFadeType)
    {
        transform.SetScale(1);

        fadeType = windowFadeType;
        if (fadeType == EWindowFadeEnum.None)
            return;

        if (fadeType == EWindowFadeEnum.Auto)
        {
            fadeType = EWindowFadeEnum.ScaleIn;
        }
        AnimationCurve curve = WindowManager.Instance.curveHolder.GetAnimationCurve(fadeType);
        if (curve == null)
            return;

        switch (fadeType)
        {
            case EWindowFadeEnum.ScaleIn:

                StartCoroutine(FadeSalceIn(curve));
                break;
        }
    }

    IEnumerator FadeSalceIn(AnimationCurve curve)
    {
        float addTime = 0;
        while (true)
        {
            addTime += Time.deltaTime;
            if (addTime > FADE_TIME)
            {
                transform.SetScale(1);
                yield break;
            }
            transform.SetScale(curve.Evaluate(addTime / FADE_TIME));

            yield return null;
        }
    }

    private void FadeOut(System.Action callback = null)
    {
        if (fadeType == EWindowFadeEnum.None)
        {
            if (callback != null)
                callback();
            return;
        }

        AnimationCurve curve = WindowManager.Instance.curveHolder.GetAnimationCurve(fadeType);
        if (curve == null)
        {
            if (callback != null)
                callback();
            return;
        }

        switch (fadeType)
        {
            case EWindowFadeEnum.ScaleIn:

                StartCoroutine(FadeSalceOut(curve, callback));
                break;
            default:
                if (callback != null)
                    callback();
                break;
        }
    }

    IEnumerator FadeSalceOut(AnimationCurve curve, System.Action callback = null)
    {
        float addTime = FADE_TIME;
        while (true)
        {
            addTime -= Time.deltaTime;
            if (addTime <= 0)
            {
                transform.SetScale(0);
                if (callback != null)
                    callback();
                yield break;
            }
            transform.SetScale(curve.Evaluate(addTime / FADE_TIME));

            yield return null;
        }
    }





    protected virtual void OnDestroy()
    {
        transform.DOKill();
        if (m_window != null)
            m_window.UnRegister();
    }

}

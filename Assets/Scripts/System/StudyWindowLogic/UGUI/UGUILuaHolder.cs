﻿/*----------------------------------------------------------------
// Copyright (C) 2018 
// author：meng cheng xin
// data：2018/3/2
// describe： ugui lua holder 
//----------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UGUILuaHolder : UGUIBaseHolder
{


    public override void Register(string name)
    {
        _Name = name;

    }

    protected override void OnOpenWindow(params object[] paramArray)
    {
        gameObject.SetActive(true);

    }

    protected override void OnRefreshWindow()
    {

    }

    protected override void OnUpdateWindow()
    {
        //if (baseWindow != null)
            //baseWindow.UpdateWindow();
    }

    protected override void OnCloseWindow()
    {
        gameObject.SetActive(false);

    }



}

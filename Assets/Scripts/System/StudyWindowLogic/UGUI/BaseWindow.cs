﻿/*----------------------------------------------------------------
// Copyright (C) 2018 
// author：meng cheng xin
// data：2018/3/2
// describe： basewindow implement
//----------------------------------------------------------------*/

using System;
using UnityEngine;
using WCBG.ToolsForUnity.Tools;
using System.Reflection;

public class BaseWindow : IBaseWindow, IEasyTimer, IEasyAudio, IEasyEffect
{
    public bool bTimerCallback
    {
        get { return IsOpen; }
    }

    private bool _IsOpen;
    public bool IsOpen
    {
        get { return _IsOpen; }
    }

    private GameObject _gameObject;
    public GameObject gameObject
    {
        get { return _gameObject; }
    }

    private Transform _transform;
    public Transform transform
    {
        get { return _transform; }
    }

    private string _Name;
    public string Name
    {
        get { return _Name; }
    }

    public void Register(string name, GameObject go)
    {
        _Name = name;
        _gameObject = go;
        _transform = go.transform;

        UGUIBaseHolder holder = go.GetComponent<UGUIBaseHolder>();

        if (holder != null)
        {
            holder.OnOpenEvent += OnOpenEvent;
            holder.OnRefreshEvent += OnRefreshEvent;
            holder.OnUpdateEvent += OnUpdateEvent;
            holder.OnCloseEvent += OnCloseEvent;
        }
    }

    public void UnRegister()
    {
        if (gameObject != null)
        {
            UGUIBaseHolder holder = gameObject.GetComponent<UGUIBaseHolder>();

            if (holder != null)
            {
                holder.OnOpenEvent -= OnOpenEvent;
                holder.OnRefreshEvent -= OnRefreshEvent;
                holder.OnUpdateEvent -= OnUpdateEvent;
                holder.OnCloseEvent -= OnCloseEvent;
            }
        }
    }

    private void OnOpenEvent(object[] paramArray)
    {
        if (_IsOpen)
        {
            OnRefreshEvent();
            return;
        }

        _IsOpen = true;
        OnAutoSetUIProperty();
        OnOpen(paramArray);
        OnRefresh();
        AddListeners();
    }

    private void OnUpdateEvent()
    {
        OnUpdate();
    }

    private void OnCloseEvent()
    {
        _IsOpen = false;
        OnClose();
        RemoveListensers();
    }

    private void OnRefreshEvent()
    {
        OnRefresh();
    }

    /// <summary>
    /// 每个Window 必须要调用
    /// </summary>
    protected virtual void OnAutoSetUIProperty() { }
    /// <summary>
    /// 界面打开调用
    /// </summary>
    /// <param name="paramArray"></param>
    protected virtual void OnOpen(params object[] paramArray) { }
    /// <summary>
    /// 界面刷新，数据刷新逻辑放在此函数上。
    /// </summary>
    protected virtual void OnRefresh() { }
    /// <summary>
    /// 每帧刷新
    /// </summary>
    protected virtual void OnUpdate() { }
    /// <summary>
    /// 关闭界面调用
    /// </summary>
    protected virtual void OnClose() { }
    /// <summary>
    /// 添加监听
    /// </summary>
    protected virtual void AddListeners() { }
    /// <summary>
    /// 移除监听
    /// </summary>
    protected virtual void RemoveListensers() { }


    protected void AutoSetUIProperty<T>(T view)
    {
        if (gameObject != null && view != null)
        {
            FTools.AutoSetUIProperty(view, gameObject);
            //AutoSetUIProperty(view, gameObject);
        }
    }
    
}

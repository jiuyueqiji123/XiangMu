﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonBaseWindow<T> : BaseWindow where T : class, new()
{


    public SingletonBaseWindow()
    {
        _Instance = this as T;
        if (_Instance == null)
            Debug.LogError("---------- Window Error! Window: " + this.GetType().Name + " -- T: " + typeof(T).Name);
    }

    private static T _Instance;
    public static T Instance
    {
        get
        {
            return _Instance;
        }
    }

    protected override void OnAutoSetUIProperty()
    {
        //base.OnAutoSetUIProperty();
        Debug.LogWarning(" ------ OnAutoSetUIProperty  -----  Type: " + _Instance.GetType().Name);
        AutoSetUIProperty(_Instance);
    }


}

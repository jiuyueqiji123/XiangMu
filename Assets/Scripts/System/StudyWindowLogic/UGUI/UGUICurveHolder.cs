﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UGUIFadeInfo
{
    public EWindowFadeEnum fadeType;
    public AnimationCurve curve;
}
public class UGUICurveHolder : MonoBehaviour {


    public UGUIFadeInfo[] curveInfoArray;


    private Dictionary<EWindowFadeEnum, AnimationCurve> m_curveDict = new Dictionary<EWindowFadeEnum, AnimationCurve>();


    private void Start()
    {
        if (curveInfoArray != null)
        {
            foreach (UGUIFadeInfo info in curveInfoArray)
            {
                m_curveDict.Add(info.fadeType, info.curve);
            }
        }
    }

    public AnimationCurve GetAnimationCurve(EWindowFadeEnum type)
    {
        if (m_curveDict.ContainsKey(type))
            return m_curveDict[type];
        return null;
    }

}

﻿/*----------------------------------------------------------------
// Copyright (C) 2018 
// author：meng cheng xin
// data：2018/3/1
// describe：接口
//----------------------------------------------------------------*/

using UnityEngine;
using UnityEngine.EventSystems;

public interface IBaseWindow
{
    GameObject gameObject { get; }

    string Name { get; }
    
    bool IsOpen { get; }

    void Register(string name, GameObject go);

    //void OpenWindow(params object[] paramArray);

    //void RefreshWindow();

    //void UpdateWindow();

    //void CloseWindow();

}

﻿/*----------------------------------------------------------------
// Copyright (C) 2018 
// author：meng cheng xin
// data：2018/3/1
// describe：接口
//----------------------------------------------------------------*/

using UnityEngine;
using UnityEngine.EventSystems;


public interface IWindowManager
{
    Canvas canvas { get; }
    Camera camera { get; }
    EventSystem eventSystem { get; }

    void OpenWindow(string name, params object[] paramArray);

    //void ReplaceWindow(string name);

    void CloseWindow(string name, bool isCache = false, System.Action callback = null);

    void OnUpdate(float deltaTime);


}
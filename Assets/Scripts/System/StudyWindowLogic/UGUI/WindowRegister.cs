﻿
public class WindowRegister
{
    public static void InitAllWindows()
    {
        WindowManager mgr = WindowManager.Instance;


        // 公共
        mgr.RegisterWindow(WinNames.MedalPanel, new MedalWindow());                 // 勋章界面
        mgr.RegisterWindow(WinNames.StoryMicroPanel, new StoryMicroWindow());       // 冒险麦克风
        mgr.RegisterWindow(WinNames.EnglishMioPanel, new EnglishMioWindow());       // 英语公众号
        mgr.RegisterWindow(WinNames.AdsTipsWindow, new AdsTipsWindow());       // IOS 广告提示弹窗

        // 乐学
        mgr.RegisterWindow(WinNames.EnglishPanel, new EnglishWindow()); //英语界面
        mgr.RegisterWindow(WinNames.MathNumberPanel, new MathNumberWindow()); //认识数字界面
        mgr.RegisterWindow(WinNames.DrawNumberPanel, new DrawNumberWindow()); //画数字
        mgr.RegisterWindow(WinNames.DrawLetterPanel, new DrawLetterWindow()); //画字母
        mgr.RegisterWindow(WinNames.DrawGeometryPanel, new DrawGeometryWindow()); //画图形
        mgr.RegisterWindow(WinNames.ReadLetterPanel, new ReadLetterWindow()); //
        mgr.RegisterWindow(WinNames.LetterAnimationPanel, new LetterAnimationWindow());  //
        mgr.RegisterWindow(WinNames.EnglishSelectGamePanel, new EnglishSelectGameWindow()); //英语游戏选择界面
        mgr.RegisterWindow(WinNames.EnglishTVPanel, new EnglishTVWindow()); //绘本界面
        mgr.RegisterWindow(WinNames.EnglishSelectLetterPanel, new EnglishSelectLetterWindow()); // 字母选择
        mgr.RegisterWindow(WinNames.EnglishLediAnimationPanel, new EnglishLediAnimationWindow()); // 乐迪2D动画
        mgr.RegisterWindow(WinNames.EnglishKCJSPanel, new EnglishKCJSWindow()); // 课程介绍
        mgr.RegisterWindow(WinNames.CardGamePanel, new CardGameWindow());//翻翻看

        // 英语颜色
        mgr.RegisterWindow(WinNames.EnglishColorMainWindow, new EnglishColorMainWindow()); // 英语颜色游戏主界面
        mgr.RegisterWindow(WinNames.FillColorGameWindow, new FillColorGameWindow()); // 涂色游戏
        mgr.RegisterWindow(WinNames.FillLetterGameWindow, new FillLetterGameWindow()); // 单词游戏
        mgr.RegisterWindow(WinNames.FindHouseGameWindow, new FindHouseGameWindow()); // 找房子游戏

        // 快递
        mgr.RegisterWindow(WinNames.StartPanel, new StartPanel());
        mgr.RegisterWindow(WinNames.GallaryPanel, new GallaryPanel());
        mgr.RegisterWindow(WinNames.PhotoShowPanel, new PhotoShowPanel());
        mgr.RegisterWindow(WinNames.WarningPanel, new WarningPanel());
        mgr.RegisterWindow(WinNames.GallayPictureShowPanel, new GallayPictureShowPanel());

        //医院
        mgr.RegisterWindow(WinNames.ClinicRoomPanel, new ClinicRoomWindow());
        mgr.RegisterWindow(WinNames.StomachachePanel, new StomachachePanle());
        mgr.RegisterWindow(WinNames.VaccinePanel, new VaccinePanel());
        mgr.RegisterWindow(WinNames.DispenPanel, new DispenPanel());
        mgr.RegisterWindow(WinNames.LowFeventPanel, new LowFeverWindow());
        mgr.RegisterWindow(WinNames.ToothachePanle, new ToothachePanel());
        mgr.RegisterWindow(WinNames.HospitalMicroPanel, new HospitalMicroWindow());
        mgr.RegisterWindow(WinNames.InjectionPanle, new InjectionWindow());

        // 冒险
        mgr.RegisterWindow(WinNames.StoryPanel, new StoryWindow()); // 冒险
        mgr.RegisterWindow(WinNames.StoryPuzzlePanel, new StoryPuzzleWindow()); // 丹麦恐龙 - 拼图
        mgr.RegisterWindow(WinNames.StoryDMKLPKPanel, new StoryDMKLPKWindow()); // 丹麦恐龙 - PK

        // 摩纳哥
        mgr.RegisterWindow(WinNames.StoryFlyGamePanel,new FlyGameWindow()); // 摩纳哥 - 乐迪飞行游戏

        // 自然拼读
        mgr.RegisterWindow(WinNames.EnglishZRPDKCJSPanel, new EnglishZRPDKCJSWindow()); // 课程介绍

        mgr.RegisterWindow(WinNames.MatrixLevelPanel, new MatrixLevelWindow()); // 矩阵
        mgr.RegisterWindow(WinNames.MatrixInfoPanel, new MatrixWindow()); // 矩阵

        mgr.RegisterWindow(WinNames.GuessPanel, new GuessWindow()); // 猜拳
        mgr.RegisterWindow(WinNames.DinosaurPkPanel, new DinosaurPkWindow()); // 恐龙
        mgr.RegisterWindow(WinNames.LediCleanPanel, new LediCleanPanel()); // 清洁



        #region package-english

#if PACKAGE_ENGLISH

        // main
        //mgr.RegisterWindow(WinNames.VersionUpdateWindow, new VersionUpdateWindow());
        mgr.RegisterWindow(WinNames.EnglishMainWindow, new EnglishMainWindow()); // 测试主界面
        mgr.RegisterWindow(WinNames.SettingWindow, new SettingWindow()); // 测试主界面
        mgr.RegisterWindow(WinNames.TipWindow, new TipWindow());

        //ar百科
        mgr.RegisterWindow(WinNames.ARBaikeDownloadPage, new ARBaikeDownloadPage());
        mgr.RegisterWindow(WinNames.ARbaikeIndroducePage, new ARBaikeIndroducePage());
        mgr.RegisterWindow(WinNames.ARbaikeUnlockPage, new ARBaikeUnlockPage());
        mgr.RegisterWindow(WinNames.ARHandPoseWindow, new ArHandPoseTipWindow());
        mgr.RegisterWindow(WinNames.CircleProgressWindow, new CircleOProgressWindow());
        mgr.RegisterWindow(WinNames.ARBaikeMainPage, new ARBaikeMainPage());
        mgr.RegisterWindow(WinNames.ARBaikeSharePage, new ARBaikeSharePage());
        mgr.RegisterWindow(WinNames.ARBaikeUnlockPageSingle, new ArBaikeUnlockpageSingle());
        mgr.RegisterWindow(WinNames.ARbaikeIndroducePageSingle, new ARbaikeIndroducePageSingle());
        mgr.RegisterWindow(WinNames.ARBaikeLoadingPage, new ARBaikeLoadingPage());
        mgr.RegisterWindow(WinNames.ARBaikeWaitPage, new ARBaikeWaitPage());

#endif

        #endregion

        #region package-math

#if PACKAGE_MATH
        mgr.RegisterWindow(WinNames.MathMainWindow, new MathMainWindow());
#endif

        #endregion



    }
}
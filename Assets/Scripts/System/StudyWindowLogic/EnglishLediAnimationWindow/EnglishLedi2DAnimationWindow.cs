﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;
using DG.Tweening;


public class EnglishLedi2DAnimationWindow : SingletonBaseWindow<EnglishLedi2DAnimationWindow>
{

    #region ui property

    //public Transform _Ledi_Actor { get; set; }
    public Transform _Ledi_Image { get; set; }
    public Transform _Ledi_Talk_Start { get; set; }
    public Transform _Ledi_Talk_End { get; set; }
    public Transform _Ledi_Cheer_End { get; set; }
    public Transform _Ledi_Center_Start { get; set; }
    public Transform _Ledi_Center_End { get; set; }
    public Transform _Ledi_TalkJS_Start { get; set; }
    public Transform _Ledi_TalkJS_End { get; set; }

    public Transform _Mask { get; set; }
    public Transform _EffectParent { get; set; }
    public Transform _GiveFiveEffect { get; set; }

    public Button _Btn_GiveFive { get; set; }


    #endregion


    public enum ELediAnimation
    {
        Idle,  // 待机
        Talk,  // 左下角出来说话
        Talk_Ts,  // 说话抬手
        Talk_Fs,  // 放手
        Cheer,  // 欢呼
        GiveFive,  // 击掌
        Talk_JS,  // 选字母界面介绍动画
    }

    public enum ELediFadeInType
    {
        BottomLeft,  // 左下角出来说话
        BottomCenterToCenter,  // 屏幕中心
    }

    private bool isLockInput;
    private int timerSeq;
    private GameAudioSource m_currentAudio;

    private UISpriteSequence m_SpriteSeq;
    private System.Action onGiveFiveCallback;



    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        _Ledi_Talk_Start.gameObject.SetActive(false);
        _Ledi_Talk_End.gameObject.SetActive(false);
        _Ledi_Center_Start.gameObject.SetActive(false);
        _Ledi_Center_End.gameObject.SetActive(false);
        _Ledi_Cheer_End.gameObject.SetActive(false);
        _Ledi_TalkJS_Start.gameObject.SetActive(false);
        _Ledi_TalkJS_End.gameObject.SetActive(false);

        m_SpriteSeq = _Ledi_Image.GetComponent<UISpriteSequence>();
        _Mask.gameObject.SetActive(false);
        _Btn_GiveFive.gameObject.SetActive(false);
        isLockInput = false;
    }

    protected override void OnClose()
    {
        base.OnClose();
        isLockInput = false;
        StopAnimation();
    }

    protected override void AddListeners()
    {
        _Btn_GiveFive.onClick.AddListener(OnGiveFiveClick);

        EventBus.Instance.AddEventHandler(EventID.ON_ENGLISH_GAME_PART_EXIT, OnStudyGameExit);
    }

    protected override void RemoveListensers()
    {
        _Btn_GiveFive.onClick.RemoveAllListeners();

        EventBus.Instance.RemoveEventHandler(EventID.ON_ENGLISH_GAME_PART_EXIT, OnStudyGameExit);
    }

    private void OnStudyGameExit()
    {
        StopAnimation();
    }

    private void OnGiveFiveClick()
    {
        if (isLockInput)
            return;

        isLockInput = true;
        _GiveFiveEffect.gameObject.SetActive(false);
        GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_32005);
        this.PlayEffectEx(StudyPath.effect_502003005, _EffectParent, 3, EGameLayerMask.UI);
        timerSeq = this.AddTimerEx(3f, () =>
        {

            isLockInput = false;
            if (onGiveFiveCallback != null)
            {
                onGiveFiveCallback();
            }

        });
    }





    // 播放    Idle > Talk_Ts > Talk > Talk_Fs > Idle
    public void PlayLediTalkAnimation(ELediFadeInType type, string audioName, System.Action callback = null)
    {
        this.RemoveTimerEx(timerSeq);

        switch (type)
        {
            case ELediFadeInType.BottomLeft:
                PlayTalkAnimation(_Ledi_Talk_Start, _Ledi_Talk_End, audioName, callback);
                break;
            case ELediFadeInType.BottomCenterToCenter:
                PlayTalkAnimation(_Ledi_Center_Start, _Ledi_Center_End, audioName, callback);
                break;
        }
    }

    // 播放    Talk_JS
    public void PlayLediTalkJSAnimation(ELediFadeInType type, string audioName, System.Action callback = null)
    {
        this.RemoveTimerEx(timerSeq);
        switch (type)
        {
            case ELediFadeInType.BottomLeft:
                PlayTalkJSAnimation(_Ledi_TalkJS_Start, _Ledi_TalkJS_End, audioName, callback);
                break;
        }
    }

    // 播放击掌    Idle > Cheer > Cheer > GiveFive
    public void PlayLediWinAnimation(string audioName, System.Action callback = null)
    {
        this.RemoveTimerEx(timerSeq);
        _Ledi_Image.DOKill();
        onGiveFiveCallback = callback;
        _Btn_GiveFive.gameObject.SetActive(false);
        _Mask.gameObject.SetActive(true);

        PlayWinAnimation(_Ledi_Center_Start, _Ledi_Cheer_End, audioName, () =>
        {

            _Btn_GiveFive.gameObject.SetActive(true);
            _GiveFiveEffect.gameObject.SetActive(true);
        });
    }

    public void StopAnimation()
    {
        _Ledi_Image.DOKill();
        _Mask.gameObject.SetActive(false);
        _Ledi_Image.gameObject.SetActive(false);
        _Btn_GiveFive.gameObject.SetActive(false);
        if (m_currentAudio != null)
        {
            m_currentAudio.Stop();
            m_currentAudio = null;
        }
    }


    // 播放 抬手 说话 放手
    private void PlayTalkAnimation(Transform start, Transform end, string audioName, System.Action callback = null)
    {
        if (_Ledi_Image == null || start == null || end == null)
            return;
        _Ledi_Image.DOKill();
        _Ledi_Image.gameObject.SetActive(true);
        _Ledi_Image.SetTransformFormTarget(start);
        m_SpriteSeq.Play(ELediAnimation.Idle);

        float Induration = 0.5f;
        _Ledi_Image.DOMove(end.position, Induration);
        _Ledi_Image.DORotateQuaternion(end.rotation, Induration);

        timerSeq = this.AddTimerEx(Induration, () =>
        {
            m_SpriteSeq.Play(ELediAnimation.Talk_Ts, false, () =>
            {
                m_SpriteSeq.Play(ELediAnimation.Talk);

                m_currentAudio = this.PlayEnglishAudioEx(audioName);
                float audioLength = m_currentAudio.Length;
                if (audioLength == 0)
                    audioLength = 1;

                timerSeq = this.AddTimerEx(audioLength, () =>
                {
                    m_currentAudio = null;
                    m_SpriteSeq.Play(ELediAnimation.Talk_Fs, false, () =>
                    {
                        m_SpriteSeq.Play(ELediAnimation.Idle);

                        float Outduration = 0.5f;
                        _Ledi_Image.DOMove(start.position, Outduration);
                        _Ledi_Image.DORotateQuaternion(start.rotation, Outduration);

                        timerSeq = this.AddTimerEx(Outduration, () =>
                        {
                            m_SpriteSeq.Stop();
                            if (callback != null)
                                callback();
                        });
                    });
                });
            });
        });
    }

    // 播放 说话
    private void PlayTalkJSAnimation(Transform start, Transform end, string audioName, System.Action callback = null)
    {
        if (_Ledi_Image == null || start == null || end == null)
            return;
        _Ledi_Image.DOKill();
        _Ledi_Image.gameObject.SetActive(true);
        _Ledi_Image.SetTransformFormTarget(start);
        m_SpriteSeq.SetFirstSprite(ELediAnimation.Talk_JS);

        float Induration = 0.5f;
        _Ledi_Image.DOMove(end.position, Induration);
        _Ledi_Image.DORotateQuaternion(end.rotation, Induration);

        timerSeq = this.AddTimerEx(Induration, () =>
        {
            m_SpriteSeq.Play(ELediAnimation.Talk_JS);

            m_currentAudio = this.PlayEnglishAudioEx(audioName);
            float audioLength = m_currentAudio.Length;
            if (audioLength == 0)
                audioLength = 1;

            timerSeq = this.AddTimerEx(audioLength, () =>
            {
                m_currentAudio = null;

                float Outduration = 0.5f;
                _Ledi_Image.DOMove(start.position, Outduration);
                _Ledi_Image.DORotateQuaternion(start.rotation, Outduration);

                timerSeq = this.AddTimerEx(Outduration, () =>
                {
                    m_SpriteSeq.Stop();
                    if (callback != null)
                        callback();
                });
            });
        });
    }

    // 播放 欢呼 击掌
    private void PlayWinAnimation(Transform start, Transform end, string audioName, System.Action callback = null)
    {
        if (_Ledi_Image == null || start == null || end == null)
            return;
        _Ledi_Image.DOKill();
        _Ledi_Image.gameObject.SetActive(true);
        _Ledi_Image.SetTransformFormTarget(start);
        m_SpriteSeq.Play(ELediAnimation.Idle);

        float Induration = 0.5f;
        _Ledi_Image.DOMove(end.position, Induration);
        _Ledi_Image.DORotateQuaternion(end.rotation, Induration);

        timerSeq = this.AddTimerEx(Induration, () =>
        {
            m_currentAudio = this.PlayEnglishAudioEx(audioName);
            timerSeq = this.AddTimerEx(m_currentAudio.Length, () =>
            {
                m_currentAudio = null;
            });
            m_SpriteSeq.Play(ELediAnimation.Cheer, false, () =>
            {
                m_SpriteSeq.Play(ELediAnimation.Cheer, false, () =>
                {
                    m_SpriteSeq.Play(ELediAnimation.GiveFive, false, () =>
                    {
                        if (callback != null)
                        {
                            callback();
                        }
                    });
                });
            });
        });
    }


}

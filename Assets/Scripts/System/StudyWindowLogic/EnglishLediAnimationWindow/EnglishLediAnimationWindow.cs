﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;
using DG.Tweening;


public class EnglishLediAnimationWindow : SingletonBaseWindow<EnglishLediAnimationWindow>
{

    #region ui property

    public Transform _Ledi_Actor { get; set; }
    //public Transform _Ledi_Image { get; set; }
    public Transform _Ledi_Talk_Start { get; set; }
    public Transform _Ledi_Talk_End { get; set; }
    public Transform _Ledi_Cheer_End { get; set; }
    public Transform _Ledi_Center_Start { get; set; }
    public Transform _Ledi_Center_End { get; set; }
    public Transform _Ledi_TalkJS_Start { get; set; }
    public Transform _Ledi_TalkJS_End { get; set; }

    public Transform _Mask { get; set; }
    public Transform _EffectParent { get; set; }
    public Transform _GiveFiveEffect { get; set; }

    public Button _Btn_GiveFive { get; set; }


    #endregion


    public enum ELediFadeInType
    {
        BottomLeft,  // 左下角出来说话
        BottomCenterToCenter,  // 屏幕中心
    }

    private bool isLockInput;
    private bool isMoveBack;
    private int timerSeq;
    private string m_givefiveAudioName; // 击掌的时候播放的声音
    private GameAudioSource m_currentAudio;
    private Dictionary<Ledi2DActor.EAnimationType, Ledi2DActor> lediDict;

    private System.Action onGiveFiveCallback;




    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        _Ledi_Talk_Start.gameObject.SetActive(false);
        _Ledi_Talk_End.gameObject.SetActive(false);
        _Ledi_Center_Start.gameObject.SetActive(false);
        _Ledi_Center_End.gameObject.SetActive(false);
        _Ledi_Cheer_End.gameObject.SetActive(false);
        _Ledi_TalkJS_Start.gameObject.SetActive(false);
        _Ledi_TalkJS_End.gameObject.SetActive(false);

        _Mask.gameObject.SetActive(false);
        _Btn_GiveFive.gameObject.SetActive(false);
        isLockInput = false;

        if (lediDict == null)
            lediDict = new Dictionary<Ledi2DActor.EAnimationType, Ledi2DActor>();

        lediDict.Clear();
        Ledi2DActor[] arr = _Ledi_Actor.GetComponentsInChildren<Ledi2DActor>();
        foreach (Ledi2DActor actor in arr)
        {
            lediDict.Add(actor.AnimationType, actor);
            actor.gameObject.SetActive(false);
        }
    }

    protected override void OnClose()
    {
        base.OnClose();
        isLockInput = false;
        StopAnimation();
    }

    protected override void AddListeners()
    {
        _Btn_GiveFive.onClick.AddListener(OnGiveFiveClick);

        EventBus.Instance.AddEventHandler(EventID.ON_ENGLISH_GAME_PART_EXIT, OnStudyGameExit);
    }

    protected override void RemoveListensers()
    {
        _Btn_GiveFive.onClick.RemoveAllListeners();

        EventBus.Instance.RemoveEventHandler(EventID.ON_ENGLISH_GAME_PART_EXIT, OnStudyGameExit);
    }

    private void OnStudyGameExit()
    {
        StopAnimation();
    }

    private void OnGiveFiveClick()
    {
        if (isLockInput)
            return;

        isLockInput = true;
        _GiveFiveEffect.gameObject.SetActive(false);
        //GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_32005);
        if (!string.IsNullOrEmpty(m_givefiveAudioName))
        {
            PlayAudio(m_givefiveAudioName);
        }
        this.PlayAudioEx(StudyAudioName.t_31025);
        this.PlayEffectEx(StudyPath.effect_502003005, _EffectParent, 3, EGameLayerMask.UI);

        if (isMoveBack)
        {
            this.AddTimerEx(1.0f, () =>
            {
                _Ledi_Actor.DOMove(_Ledi_Center_Start.position, 0.5f).OnComplete(() =>
                {
                    if (onGiveFiveCallback != null)
                    {
                        onGiveFiveCallback();
                    }
                });
            });
        }
        else
        {
            //timerSeq = this.AddTimerEx(2.0f, () =>
            //{
            if (onGiveFiveCallback != null)
            {
                onGiveFiveCallback();
            }
            //});
        }
    }






    public void HideMask()
    {
        _Mask.SetActive(false);
    }

    // 播放    Idle > Talk_Ts > Talk > Talk_Fs > Idle
    public void PlayLediTalkAnimation(ELediFadeInType type, string audioName, System.Action callback = null)
    {
        this.RemoveTimerEx(timerSeq);

        switch (type)
        {
            case ELediFadeInType.BottomLeft:
                PlayTalkAnimation(_Ledi_Talk_Start, _Ledi_Talk_End, audioName, callback);
                break;
            case ELediFadeInType.BottomCenterToCenter:
                PlayTalkAnimation(_Ledi_Center_Start, _Ledi_Center_End, audioName, callback);
                break;
        }
    }

    // 播放    Talk_JS 字母选择主界面
    public void PlayLediTalkJSAnimation(ELediFadeInType type, string audioName, System.Action callback = null)
    {
        this.RemoveTimerEx(timerSeq);
        switch (type)
        {
            case ELediFadeInType.BottomLeft:
                PlayTalkJSAnimation(_Ledi_TalkJS_Start, _Ledi_TalkJS_End, audioName, callback);
                break;
        }
    }

    // 播放击掌    Idle > Cheer > Cheer > GiveFive
    public void PlayLediWinAnimation(string audioName, string givefiveAudioName, bool isLediMoveBack,bool isHideMask, System.Action callback = null)
    {
        this.RemoveTimerEx(timerSeq);
        _Ledi_Actor.DOKill();
        isLockInput = false;
        m_givefiveAudioName = givefiveAudioName;
        isMoveBack = isLediMoveBack;
        onGiveFiveCallback = callback;
        _Btn_GiveFive.gameObject.SetActive(false);
        _Mask.gameObject.SetActive(!isHideMask);

        PlayWinAnimation(_Ledi_Center_Start, _Ledi_Cheer_End, audioName, () =>
        {
            _Btn_GiveFive.gameObject.SetActive(true);
            _GiveFiveEffect.gameObject.SetActive(true);
            PlayAction(Ledi2DActor.EAnimationType.clap_p_2d);
        });
    }

    public void StopAnimation()
    {
        _Ledi_Actor.DOKill();
        _Mask.gameObject.SetActive(false);
        _Ledi_Actor.gameObject.SetActive(false);
        _Btn_GiveFive.gameObject.SetActive(false);
        _GiveFiveEffect.gameObject.SetActive(false);
        if (m_currentAudio != null)
        {
            m_currentAudio.Stop();
            m_currentAudio = null;
        }
    }


    // 播放 抬手 说话 放手
    private void PlayTalkAnimation(Transform start, Transform end, string audioName, System.Action callback = null)
    {
        if (_Ledi_Actor == null || start == null || end == null)
            return;
        _Ledi_Actor.DOKill();
        _Ledi_Actor.gameObject.SetActive(true);
        _Ledi_Actor.SetTransformFormTarget(start);

        float Induration = 0.5f;
        _Ledi_Actor.DOMove(end.position, Induration);
        _Ledi_Actor.DORotateQuaternion(end.rotation, Induration);
        PlayAction(Ledi2DActor.EAnimationType.idell_p_2d);

        timerSeq = this.AddTimerEx(Induration, () =>
        {
            m_currentAudio = PlayAudio(audioName);
            float audioLength = m_currentAudio.Length;
            if (audioLength == 0)
                audioLength = 1;

            PlayAction(Ledi2DActor.EAnimationType.talk_p_2d);
            timerSeq = this.AddTimerEx(audioLength, () =>
            {
                m_currentAudio = null;
                PlayAction(Ledi2DActor.EAnimationType.idell_p_2d);
                float Outduration = 0.5f;
                _Ledi_Actor.DOMove(start.position, Outduration);
                _Ledi_Actor.DORotateQuaternion(start.rotation, Outduration);

                timerSeq = this.AddTimerEx(Outduration, () =>
                {
                    if (callback != null)
                        callback();
                });
            });
        });
    }

    // 播放 说话
    private void PlayTalkJSAnimation(Transform start, Transform end, string audioName, System.Action callback = null)
    {
        if (_Ledi_Actor == null || start == null || end == null)
            return;
        _Ledi_Actor.DOKill();
        _Ledi_Actor.gameObject.SetActive(true);
        _Ledi_Actor.SetTransformFormTarget(start);

        float Induration = 0.5f;
        _Ledi_Actor.DOMove(end.position, Induration);
        _Ledi_Actor.DORotateQuaternion(end.rotation, Induration);
        PlayAction(Ledi2DActor.EAnimationType.raise_p_2d);

        timerSeq = this.AddTimerEx(Induration, () =>
        {
            m_currentAudio = PlayAudio(audioName);
            float audioLength = m_currentAudio.Length;
            if (audioLength == 0)
                audioLength = 1;

            timerSeq = this.AddTimerEx(audioLength, () =>
            {
                m_currentAudio = null;
                float Outduration = 0.5f;
                _Ledi_Actor.DOMove(start.position, Outduration);
                _Ledi_Actor.DORotateQuaternion(start.rotation, Outduration);

                timerSeq = this.AddTimerEx(Outduration, () =>
                {
                    StopAction(Ledi2DActor.EAnimationType.raise_p_2d);
                    if (callback != null)
                        callback();
                });
            });
        });
    }

    // 播放 欢呼 击掌
    private void PlayWinAnimation(Transform start, Transform end, string audioName, System.Action callback = null)
    {
        if (_Ledi_Actor == null || start == null || end == null)
            return;
        _Ledi_Actor.DOKill();
        _Ledi_Actor.gameObject.SetActive(true);
        _Ledi_Actor.SetTransformFormTarget(start);

        float Induration = 0.5f;
        _Ledi_Actor.DOMove(end.position, Induration);
        _Ledi_Actor.DORotateQuaternion(end.rotation, Induration);
        PlayAction(Ledi2DActor.EAnimationType.idell_p_2d);

        timerSeq = this.AddTimerEx(Induration, () =>
        {
            m_currentAudio = PlayAudio(audioName);
            timerSeq = this.AddTimerEx(m_currentAudio.Length, () =>
            {
                m_currentAudio = null;
            });

            PlayAction(Ledi2DActor.EAnimationType.happy_p_2d);
            float waitTime = m_currentAudio.Length > 1.5f ? m_currentAudio.Length + 0.5f : 1.8f;
            timerSeq = this.AddTimerEx(waitTime, () =>
            {
                if (callback != null)
                {
                    callback();
                }
            });
        });

    }

    private void PlayAction(Ledi2DActor.EAnimationType type)
    {
        StopAllAction();
        Ledi2DActor actor = GetLedi2DActor(type);
        if (actor)
        {
            actor.gameObject.SetActive(true);
        }
    }

    private void StopAllAction()
    {
        foreach (Ledi2DActor actor in lediDict.Values)
        {
            actor.gameObject.SetActive(false);
        }
    }

    private void StopAction(Ledi2DActor.EAnimationType type)
    {
        Ledi2DActor actor = GetLedi2DActor(type);
        if (actor)
        {
            actor.gameObject.SetActive(false);
        }
    }

    private Ledi2DActor GetLedi2DActor(Ledi2DActor.EAnimationType type)
    {
        return lediDict.ContainsKey(type) ? lediDict[type] : null;
    }



    private GameAudioSource PlayAudio(string name)
    {
        GameAudioSource audio = this.PlayEnglishAudioEx(name);
        if (audio.Clip == null)
            return this.PlayAudioEx(name);
        return audio;
    }




}

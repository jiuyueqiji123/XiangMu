﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ledi2DActor : MonoBehaviour {


    public enum EAnimationType
    {
        talk_p_2d,
        raise_p_2d,
        praise_p_2d,
        idell_p_2d,
        happy_p_2d,
        clap_p_2d,
    }

    [SerializeField]
    private EAnimationType _AnimationType;
    public EAnimationType AnimationType
    {
        get { return _AnimationType; }
    }




}

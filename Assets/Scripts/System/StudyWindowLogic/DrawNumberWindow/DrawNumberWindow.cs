﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawNumberWindow : BaseDrawWindow<DrawNumberWindow>, IEasyAudio
{


    #region ui property

    public Button _Btn_Return { get; set; }

    public Transform _Brush { get; set; }
    public Transform _Number_Parent { get; set; }
    public Transform _AnimParent { get; set; }

    public Image _Mask { get; set; }
    public Image _Sign { get; set; }

    #endregion

    private System.Action<bool> completeCallback;

    private const string numberBasePath = StudyPath.math_drawnumberBasePath;

    // 加载的资源
    private GameObject m_LoadObj;


    

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
    }

    protected override void OnClose()
    {
        if (m_LoadObj != null)
            GameObject.Destroy(m_LoadObj);
    }


    protected override void PlayBrushAnimation()
    {
        if (isBrushPlaying)
            return;
        isBrushPlaying = true;
        PlayBrushAudio();
    }

    protected override void StopBrushAnimation()
    {
        if (!isBrushPlaying)
            return;

        StopBrushAudio();
        isBrushPlaying = false;
    }

    protected override void PlayBrushAudio()
    {
        if (m_audio == null)
        {
            m_audio = this.PlayAudioEx(StudyAudioName.t_11021, true);
        }
    }

    #region uievent

    private void OnButtonReturnClick()
    {
        DrawRealize2D.DestroyInstance();
        if (completeCallback != null)
        {
            completeCallback(false);
            completeCallback = null;
        }

        WindowManager.Instance.CloseWindow(WinNames.DrawNumberPanel);
    }

    #endregion

    public void StartDrawNumber(int number, System.Action<bool> callback = null)
    {
        if (m_LoadObj != null)
        {
            GameObject.Destroy(m_LoadObj);
            m_LoadObj = null;
        }

        m_LoadObj = LoadNumber(number);
        if (m_LoadObj == null)
        {
            bIsDrawing = false;
            //if (callback != null)
            //{
            //    callback(false);
            //}
            return;
        }

        Vector3 scale = transform.localScale;
        transform.SetScale(1);

        Transform brush = _Brush;

        DrawRealize2D.Instance.DrawNumber(m_LoadObj.transform, brush, _Mask.transform, _Sign, WindowManager.Instance.canvas, _AnimParent, () =>
        {
            GameAudioSource audio = this.PlayAudioEx(StudyAudioName.t_11017);
            this.AddTimerEx(audio.Length, () => {
                this.PlayAudioEx((uint)(StudyAudioName.t_12006 + number - 1));
            });
            this.AddTimerEx(2f, () =>
            {
                if (!bIsDrawing)
                    return;

                //GameAudioSource audio = this.PlayMathAudioEx(StudyAudioName.DrawNumberCompleteAudio);
                //this.AddTimerEx(audio.Length, () =>
                //{
                if (completeCallback != null)
                {
                    completeCallback(true);
                }
                //});
            });
        }, null, new DrawSetting(100, 30, 10, 1));

        completeCallback = callback;
        bIsDrawing = true;
        isBrushPlaying = true;
        m_spriteSeqArray = brush.GetComponentsInChildren<SpriteSequenceFrame>(true);
        StopBrushAnimation();

        DrawRealize2D.Instance.Pause();
        this.AddTimerEx(0.5f, () =>
        {
            DrawRealize2D.Instance.Resume();
        });

        transform.localScale = scale;
    }

    private GameObject LoadNumber(int number)
    {
        string path = numberBasePath + "Number_" + number;

        GameObject go = Res.LoadObj(path);

        go.transform.SetParent(_Number_Parent);
        RectTransform rect = go.transform as RectTransform;
        rect.anchoredPosition3D = Vector3.zero;
        rect.sizeDelta = Vector2.zero;
        rect.localScale = Vector3.one;
        rect.localRotation = Quaternion.identity;

        return go;
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MedalWindow : SingletonBaseWindow<MedalWindow>
{


    #region ui property

    public RectTransform _XunzhangBg { get; set; }
    public Transform _XunZhang { get; set; }
    public Transform _XunZhang_Start { get; set; }

    #endregion

    public enum EXZCountEnum
    {
        None = 0,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
    }


    private int m_showIndex;
    private GameObject m_createXZ;
    private System.Action onReturnButtonClick;


    private Material _grayMaterial;
    public Material grayMaterial
    {
        get
        {
            if (_grayMaterial == null)
            {
                _grayMaterial = new Material(Shader.Find("UI/Transparent Colored Gray"));
            }
            return _grayMaterial;
        }
    }

    private int m_currentCount;
    private List<GameObject> m_xzList = new List<GameObject>();
    private List<Image> m_xzImageList = new List<Image>();

    protected override void OnOpen(params object[] paramArray)
    {
        onReturnButtonClick = null;
        m_currentCount = 0;
        m_xzList.Clear();
        m_xzList.Add(_XunZhang.gameObject);
        m_xzImageList.Add(_XunZhang.GetComponentInChildren<Image>());
    }

    protected override void OnClose()
    {
        if (m_createXZ != null)
        {
            GameObject.Destroy(m_createXZ);
            m_createXZ = null;
        }
        for (int i = 1; i < m_xzList.Count; i++)
        {
            GameObject.Destroy(m_xzList[i]);
        }
        m_xzList.Clear();
        m_xzImageList.Clear();
    }

    /// <summary>
    /// 显示勋章动画
    /// </summary>
    /// <param name="currentCount">显示勋章数量 0 就是全黑的， 1就是开始播放第一个勋章动画</param>
    /// <param name="playAnim">是否播放动画，为 false 直接显示对应勋章</param>
    /// <param name="type">勋章总数</param>
    /// <param name="callback">完成回调</param>
    public void ShowXunzhang(int currentCount, bool playAnim, EXZCountEnum type, System.Action callback = null)
    {
        ShowXunzhang(currentCount, playAnim, (int)type, callback);
    }

    public void ShowXunzhang(int currentCount, bool playAnim, System.Action callback = null)
    {
        ShowXunzhang(currentCount, playAnim, m_currentCount, callback);
    }

    public void ShowXunzhang(int currentCount, bool playAnim, int allCount, System.Action callback = null)
    {
        Show(currentCount, playAnim, allCount, ()=> {
            if (playAnim)
            {
                AdsManager.Instance.ShowAds(AdsNameConst.Ads_Xunzhang);
            }
            if (callback != null)
            {
                callback();
            }
        });
    }

    private void Show(int currentCount, bool playAnim, int allCount, System.Action callback = null)
    {
        if (allCount == 0)
        {
            SetGrayXZ(currentCount, allCount);
            if (callback != null)
                callback();
            return;
        }
        if (currentCount > allCount)
        {
            if (callback != null)
                callback();
            return;
        }
        if (m_currentCount != allCount)
        {
            m_currentCount = allCount;
            CreateXZ(allCount);
        }

        if (playAnim && currentCount > 0)
            this.PlayAudioEx(StudyAudioName.t_1002);

        m_showIndex = currentCount;
        if (playAnim)
        {
            SetGrayXZ(currentCount - 1, allCount);
            PlayAnim(currentCount, allCount, callback);
        }
        else
        {
            SetGrayXZ(currentCount, allCount);
            if (callback != null)
            {
                callback();
            }
        }
    }

    private void CreateXZ(int xzCount)
    {
        if (xzCount == m_xzList.Count)
            return;

        if (xzCount < m_xzList.Count)
        {
            for (int i = 0; i < m_xzList.Count; i++)
            {
                m_xzList[i].SetActive(i < xzCount);
            }
        }
        else
        {
            for (int i = 0; i < m_xzList.Count; i++)
            {
                m_xzList[i].SetActive(true);
            }
            for (int i = m_xzList.Count; i < xzCount; i++)
            {
                GameObject go = GameObject.Instantiate(_XunZhang.gameObject, _XunZhang.parent);
                m_xzList.Add(go);
                m_xzImageList.Add(go.GetComponentInChildren<Image>());
            }
        }
        _XunzhangBg.sizeDelta = new Vector2(30 + xzCount * 90, 120);
    }

    private void PlayAnim(int currentCount, int allCount, System.Action callback)
    {
        if (currentCount == 0)
        {
            if (callback != null)
                callback();
            return;
        }
        if (m_createXZ == null)
        {
            m_createXZ = GameObject.Instantiate(_XunZhang_Start.gameObject);
            GameObject effect = Res.LoadObj(StudyPath.xz_effect);
            effect.transform.SetParent(m_createXZ.transform);
            effect.transform.localScale = Vector3.one;
            effect.transform.localPosition = Vector3.zero;
            effect.SetLayer(m_createXZ.layer);
        }
        m_createXZ.gameObject.SetActive(true);
        RectTransform rt = m_createXZ.transform as RectTransform;
        rt.SetParent(_XunZhang_Start.parent);
        rt.position = _XunZhang_Start.position;
        rt.localScale = _XunZhang_Start.localScale;
        rt.rotation = _XunZhang_Start.rotation;

        Image moveto = m_xzImageList[currentCount - 1];

        float scaletime = 0.5f;
        rt.localScale = Vector3.zero;
        rt.DOScale(1, scaletime).SetEase(Ease.InQuad);
        this.AddTimerEx(scaletime + 0.5f, () =>
        {
            float time = 0.5f;
            rt.DOMove(moveto.transform.position, time).SetEase(Ease.InOutSine);
            rt.DOScale(moveto.transform.localScale, time).SetEase(Ease.InOutSine);
            this.AddTimerEx(time, () =>
            {
                if (m_createXZ == null)
                    return;
                SetGrayXZ(currentCount, allCount);
                m_createXZ.gameObject.SetActive(false);

                if (callback != null)
                {
                    callback();
                }
            });
        });

    }

    private void SetGrayXZ(int currentCount, int allCount)
    {
        _XunzhangBg.SetActive(allCount != 0);
        for (int i = 0; i < allCount; i++)
        {
            Image img = m_xzImageList[i];
            if (i >= currentCount)
            {
                img.material = grayMaterial;
            }
            else
            {
                img.material = null;
            }
        }
    }

    #region ui event

    private void OnButtonReturnClick()
    {
        //Debug.LogError("认识数字 退出游戏");

        //GameStateManager.Instance.GotoState(GameStateName.MAIN_STATE);
    }

    #endregion
    

}

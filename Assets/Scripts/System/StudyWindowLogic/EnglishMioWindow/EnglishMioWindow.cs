﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;


public class EnglishMioWindow : SingletonBaseWindow<EnglishMioWindow>
{

    #region ui property

    //public RectTransform _Content { get; set; }
    public Button _Btn_Return { get; set; }


    #endregion

    private System.Action completeCallback;




    protected override void OnClose()
    {
        base.OnClose();

        completeCallback = null;
    }

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
    }
    
    

    #region uievent

    private void OnButtonReturnClick()
    {
        WindowManager.Instance.CloseWindow(WinNames.EnglishMioPanel, false, completeCallback);
        //if (completeCallback != null)
        //    completeCallback();
    }

    #endregion


    public void AddCloseEvent(System.Action callback)
    {
        completeCallback = callback;
    }





}

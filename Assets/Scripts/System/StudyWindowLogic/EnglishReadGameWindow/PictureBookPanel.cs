﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;

public class PictureBookPanel : PanelInfo
{


    public Transform parentRoot;
    private uint m_dataID;

    public Image m_CardBg;
    public Image m_CardImage;
    public Text m_CardLetter;
    public Text m_CardStatement;


    public void Initialize(int panelIndex, uint dataID)
    {
        _index = panelIndex;
        parentRoot = transform;
        position = parentRoot.position;
        m_dataID = dataID;

        m_CardBg = transform.Find("CardBg").GetComponent<Image>();
        m_CardImage = transform.Find("CardImage").GetComponent<Image>();
        m_CardLetter = transform.Find("CardLetter").GetComponent<Text>();
        m_CardStatement = transform.Find("CardStatement").GetComponent<Text>();

        RefreshInfo();
    }

    public override void UpdatePosition(Vector3 newPosition)
    {
        base.UpdatePosition(newPosition);

        parentRoot.position = newPosition;
    }

    public override void MoveLeft()
    {
        base.MoveLeft();

        if (type == EPanelPointEnum.Right)
        {
            if (_index + 3 > count - 1)
            {
                switch (_index)
                {
                    case 1:
                        _index = 0;
                        break;
                    case 2:
                        _index = 1;
                        break;
                    case 3:
                        _index = 2;
                        break;
                }
            }
            else
            {
                _index = _index + 3;
            }
            RefreshInfo();
        }
    }

    public override void MoveRight()
    {
        base.MoveRight();

        if (type == EPanelPointEnum.Left)
        {
            if (_index - 3 < 0)
            {
                switch (_index)
                {
                    case 0:
                        _index = 1;
                        break;
                    case 1:
                        _index = 2;
                        break;
                    case 2:
                        _index = 3;
                        break;
                }
            }
            else
            {
                _index = _index - 3;
            }
            RefreshInfo();
        }
    }

    public override void Dispose()
    {
        base.Dispose();
        
    }

    private void RefreshInfo()
    {
        if (m_dataID == 0) return;
        EnglishLetterInfo info = FTableManager.GetEnglishLetterInfo(m_dataID);
        if (info == null)
        {
            return;
        }
        if (info.game1_bg_list.Count == 4)
        {
            m_CardBg.sprite = Res.LoadSprite(StudyPath.english_sprite_path + GetBgName(info.game1_bg_list[index]));
        }
        switch (_index)
        {
            case 0:
                m_CardLetter.gameObject.SetActive(true);
                m_CardImage.gameObject.SetActive(false);
                m_CardStatement.text = info.game1_statement1;
                m_CardLetter.text = info.name;
                break;
            case 1:
                m_CardLetter.gameObject.SetActive(true);
                m_CardImage.gameObject.SetActive(false);
                m_CardStatement.text = info.game1_statement2;
                m_CardLetter.text = info.name.ToLower();
                break;

            case 2:
                m_CardLetter.gameObject.SetActive(false);
                m_CardImage.gameObject.SetActive(true);
                m_CardStatement.text = info.game1_statement3;
                Sprite sprite = Res.LoadSprite(StudyPath.english_sprite_picturebook + info.game1_texture1);// UISpriteManager.Instance.GetSprite(emUIAltas.English_WordImage, info.game1_texture1);
                if (sprite != null)
                {
                    m_CardImage.sprite = sprite;
                    m_CardImage.SetNativeSize();
                }
                else
                {
                    Debug.LogError("Load sprite error! sprite path:" + StudyPath.english_sprite_picturebook + info.game1_texture1 + "  EnglishLetterInfo excel ID:" + m_dataID);
                }
                break;
            case 3:
                m_CardLetter.gameObject.SetActive(false);
                m_CardImage.gameObject.SetActive(true);
                m_CardStatement.text = info.game1_statement4;
                //Sprite sprite1 = UISpriteManager.Instance.GetSprite(emUIAltas.English_WordImage, info.game1_texture2);
                Sprite sprite1 = Res.LoadSprite(StudyPath.english_sprite_picturebook + info.game1_texture2);
                if (sprite1 != null)
                {
                    m_CardImage.sprite = sprite1;
                    m_CardImage.SetNativeSize();
                }
                else
                {
                    Debug.LogError("Load sprite error! sprite path:" + StudyPath.english_sprite_picturebook + info.game1_texture2 + "  EnglishLetterInfo excel ID:" + m_dataID);
                }
                break;
        }


    }

    private string GetBgName(int index)
    {
        switch (index)
        {
            case 0:
                return "bg01-senlin";
            case 1:
                return "bg02-tiankong";
            case 2:
                return "bg03-shamo";
            case 3:
                return "bg04-haiyang";
            default:
                return "bg05-fangjian";
        }
    }


}

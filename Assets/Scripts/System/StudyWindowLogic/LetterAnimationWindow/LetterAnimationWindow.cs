﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;


public class LetterAnimationWindow : SingletonBaseWindow<LetterAnimationWindow>
{

    #region ui property

    public Button _Btn_Return { get; set; }

    public Transform _Animation1 { get; set; }
    public Image _Letter_Big { get; set; }
    public Image _Letter_Small { get; set; }
    public Transform _Animation2 { get; set; }

    public Image _Target1 { get; set; }
    public Text _Target1_Text_Head { get; set; }
    public Text _Target1_Text_End { get; set; }
    public Image _Target2 { get; set; }
    public Text _Target2_Text_Head { get; set; }
    public Text _Target2_Text_End { get; set; }

    #endregion

    private System.Action completeCallback;

    private uint m_dataID;

    private bool bViewState;



    protected override void OnOpen(params object[] paramArray)
    {
        bViewState = true;
    }

    protected override void OnClose()
    {
        bViewState = false;
    }

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
    }

    public void StartAnimation(uint dataID, System.Action callback = null)
    {
        if (dataID == 0)
            return;
        m_dataID = dataID;
        completeCallback = callback;
        //EnglishLetterInfo info = TableProtoLoader.EnglishLetterInfoDict[dataID];

        //_Letter_Big.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.English_BigAndSmallLetter, info.letter_big + "(big)");
        //_Letter_Small.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.English_BigAndSmallLetter, info.letter_small + "(small)");
        //_Target1.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EnglishLearn, info.word_texture1);
        //_Target2.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EnglishLearn, info.word_texture2);

        //if (info.word1.Length > 1)
        //{
        //    _Target1_Text_Head.text = info.word1.Substring(0, 1);
        //    _Target1_Text_End.text = info.word1.Substring(1);
        //}
        //else
        //{
        //    Debug.LogError("letter error! word1:" + info.word1);
        //}
        //if (info.word2.Length > 1)
        //{
        //    _Target2_Text_Head.text = info.word2.Substring(0, 1);
        //    _Target2_Text_End.text = info.word2.Substring(1);
        //}
        //else
        //{
        //    Debug.LogError("letter error! word2:" + info.word2);
        //}


        PlayAnimation1();
    }

    private void PlayAnimation1()
    {
        _Animation1.gameObject.SetActive(true);
        AnimationEventHolder holder = _Animation1.GetComponent<AnimationEventHolder>() == null ? _Animation1.gameObject.AddComponent<AnimationEventHolder>() : _Animation1.GetComponent<AnimationEventHolder>();

        //Debug.Log("PlayAnimation1");
        holder.PlayAnimatoin("LetterAnimation1", OnAudioEvent, () => {

            //view.Animation1.SetActive(false);
            if (!bViewState)
                return;
            PlayAnimation2();

        });
    }

    private void PlayAnimation2()
    {
        _Animation1.gameObject.SetActive(true);
        AnimationEventHolder holder = _Animation1.GetComponent<AnimationEventHolder>() == null ? _Animation1.gameObject.AddComponent<AnimationEventHolder>() : _Animation1.GetComponent<AnimationEventHolder>();

        //Debug.Log("PlayAnimation2");
        holder.PlayAnimatoin("LetterAnimation2", OnAudioEvent, () =>
        {
            //Debug.LogError("LetterAnimation2");
            _Animation1.gameObject.SetActive(false);
            if (!bViewState)
                return;
            PlayAnimation3();

        });

    }

    private void PlayAnimation3()
    {
        _Animation2.gameObject.SetActive(true);
        AnimationEventHolder holder = _Animation2.GetComponent<AnimationEventHolder>() == null ? _Animation2.gameObject.AddComponent<AnimationEventHolder>() : _Animation2.GetComponent<AnimationEventHolder>();

        holder.PlayAnimatoin("LetterAnimation3", OnAudioEvent, () =>
        {
            //view.Animation2.SetActive(false);
            if (!bViewState)
                return;
            PlayAnimation4();
        });
    }

    private void PlayAnimation4()
    {
        _Animation2.gameObject.SetActive(true);
        AnimationEventHolder holder = _Animation2.GetComponent<AnimationEventHolder>() == null ? _Animation2.gameObject.AddComponent<AnimationEventHolder>() : _Animation2.GetComponent<AnimationEventHolder>();

        holder.PlayAnimatoin("LetterAnimation4", OnAudioEvent, () =>
        {
            _Animation2.gameObject.SetActive(false);
            if (!bViewState)
                return;
            if (completeCallback != null)
            {
                completeCallback();
            }
        });
    }

    private void OnAudioEvent(int index)
    {
        Debug.Log("OnAudioEvent:" + index);
        //EnglishLetterInfo info = TableProtoLoader.EnglishLetterInfoDict[m_dataID];
        //switch (index)
        //{
        //    case 1:
        //        this.PlayEnglishAudioEx(info.letter_big_audio);
        //        break;
        //    case 2:
        //        this.PlayEnglishAudioEx(info.letter_small_audio);
        //        break;
        //    case 3:
        //        this.PlayEnglishAudioEx(info.word1_audio);
        //        break;
        //    case 4:
        //        this.PlayEnglishAudioEx(info.word2_audio);
        //        break;
        //}
    }


    #region uievent

    private void OnButtonReturnClick()
    {

        WindowManager.Instance.CloseWindow(WinNames.DrawNumberPanel);
    }

    #endregion

}

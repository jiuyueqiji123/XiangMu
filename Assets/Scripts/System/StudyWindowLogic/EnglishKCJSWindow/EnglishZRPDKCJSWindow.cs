﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TableProto;


public class EnglishZRPDKCJSWindow : SingletonBaseWindow<EnglishZRPDKCJSWindow>
{

    #region ui property

    public RectTransform _Content { get; set; }
    public Button _Btn_Return { get; set; }


    #endregion




    protected override void OnOpen(params object[] paramArray)
    {
        base.OnOpen(paramArray);

        _Content.anchoredPosition = Vector2.zero;
    }

    protected override void AddListeners()
    {
        _Btn_Return.onClick.AddListener(OnButtonReturnClick);
    }

    protected override void RemoveListensers()
    {
        _Btn_Return.onClick.RemoveAllListeners();
    }
    
    

    #region uievent

    private void OnButtonReturnClick()
    {
        WindowManager.Instance.CloseWindow(WinNames.EnglishZRPDKCJSPanel);
    }

    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingManager : Singleton<SettingManager> {

    private SettingView view;

    public override void Init()
    {
        base.Init();
        this.view = new SettingView();
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Setting_Close_Btn, this.OnBtnCloseClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.Setting_More_Game, this.OnBtnMoreGameClick);
    }

    public override void UnInit()
    {
        base.UnInit();
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Setting_Close_Btn, this.OnBtnCloseClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.Setting_More_Game, this.OnBtnMoreGameClick);
    }

    public void OpenView()
    {
        string id = UserManager.Instance.UserId.ToString();
        string version = SDKManager.Instance.GetVersion();
        bool sound = !AudioSetManager.Instance.SoundMuted;
        bool music = !AudioSetManager.Instance.MusicMuted;
        this.view.Open(id, version, sound, music);
    }

    public void CloseView()
    {
        this.view.Close();
    }

    private void OnBtnCloseClick(HUIEvent evt)
    {
        CloseView();
    }

    private void OnBtnMoreGameClick(HUIEvent evt)
    {
        SDKManager.Instance.MoreGame();
    }
}

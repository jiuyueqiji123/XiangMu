﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingView {

    private const string Path = "ui/ui_parent/SettingForm";

    private HUIFormScript formScript;

    private Text IdText;

    private UIButtonToggle SoundToggle;

    private UIButtonToggle MusicToggle;

    private Text VersionText;

    private GameObject CloseObj;

    private GameObject MoreGameObj;

    public void Open(string id, string version, bool sound, bool music)
    {
        this.formScript = HUIManager.Instance.OpenForm(Path, true);
        IdText = formScript.GetWidget((int)emSettingWidget.ID).GetComponent<Text>();
        SoundToggle = formScript.GetWidget((int)emSettingWidget.SoundToggle).GetComponent<UIButtonToggle>();
        MusicToggle = formScript.GetWidget((int)emSettingWidget.MusicToggle).GetComponent<UIButtonToggle>();
        VersionText = formScript.GetWidget((int)emSettingWidget.Version).GetComponent<Text>();
        CloseObj = formScript.GetWidget((int)emSettingWidget.Close);
        HUIUtility.SetUIMiniEvent(CloseObj, enUIEventType.Click, enUIEventID.Setting_Close_Btn);
        MoreGameObj = formScript.GetWidget((int)emSettingWidget.MoreGame);
        HUIUtility.SetUIMiniEvent(MoreGameObj, enUIEventType.Click, enUIEventID.Setting_More_Game);

        //Debug.LogError(sound);
        //Debug.LogError(music);
      
       MoreGameObj.SetActive(SDKManager.Instance.GetChannelName() == "egame");

        IdText.text = id;
        VersionText.text = version;
        SoundToggle.Set(sound);
        MusicToggle.Set(music);
        SoundToggle.ValueChangeAction = OnSoundToggleChange;
        MusicToggle.ValueChangeAction = OnMusicToggleChange;
    }

    public void Close()
    {
        SoundToggle.ValueChangeAction = null;
        MusicToggle.ValueChangeAction = null;
        HUIManager.Instance.CloseForm(formScript);
        this.CloseObj = null;
    }

    private void OnSoundToggleChange(bool val)
    {
        //Debug.LogError(val);
        AudioSetManager.Instance.SetMuteSound(!val);
    }

    private void OnMusicToggleChange(bool val)
    {
        //Debug.LogError(val);
        AudioSetManager.Instance.SetMuteMusic(!val);
    }
}

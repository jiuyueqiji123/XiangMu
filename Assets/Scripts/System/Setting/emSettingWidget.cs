﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum emSettingWidget  {
    ID,
    MusicToggle,
    SoundToggle,
    Version,
    Close,
    MoreGame,
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxModule : MonoBehaviour {

    private int number;
    private int index;
    public void Init(int n,int i)
    {
        number = n;
        index = i;
    }

    //拖拽
    private void OnMouseDrag()
    {

    }

    //按下
    private void OnMouseDown()
    {
        //点击
        HUIEventManager.Instance.DispatchUIEvent(enUIEventID.AddPlus_Box_Click, new stUIEventParams { argInt = number, argInt2 = index });
        
    }

    //抬起
    private void OnMouseUp()
    {

    }
}

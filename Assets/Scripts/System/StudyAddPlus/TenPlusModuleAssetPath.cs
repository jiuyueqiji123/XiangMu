﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//资源路径
public class TenPlusModuleAssetPath
{

    #region common
    public const float timeout = 60f;

    public const string anim_ledi_happy = "introduce_p";
    public const string anim_ledi_prsise= "introduce_p";
    public const string anim_ledi_clap = "clap_p_01";
    public const string anim_ledi_clap2 = "clap_p_03";

    public const string anim_sheep_eat = "sheep_eating";
    public const string anim_sheep_speak = "sheep_speaking";
    public static List<string> sheep_animations = new List<string>{
        "sheep_stand_up",
        "sheep_speaking",
        "sheep_eating" };

    public static List<string> car_animations = new List<string> {
        "car_stop",
        "car_move_stop",
        "car_stop_move",
        "touch_car",
        "wheel_rotating"
    };
    

    public const string anim_hand_click = "hand_click01";
    //编号内容详见策划文档<10以内加法运算>
    //场景内音效
    public const string audio_603030101 = "study_ten_add/sound/603030101";
    public const string audio_603030102 = "study_ten_add/sound/603030102";
    public const string audio_603030201 = "study_ten_add/sound/603030201";
    public const string audio_603030202 = "study_ten_add/sound/603030202";
    public const string audio_603030301 = "study_ten_add/sound/603030301";
    public const string audio_603030302 = "study_ten_add/sound/603030302";
    public const string audio_603030303 = "study_ten_add/sound/603030303";
    public const string audio_603030401 = "study_ten_add/sound/603030401";
    public const string audio_603030501 = "study_ten_add/sound/603030501";
    //乐迪语音 
    public const string audio_403030100 = "study_ten_add/sound/403030100";
    public const string audio_403030101 = "study_ten_add/sound/403030101";
    public const string audio_403030102 = "study_ten_add/sound/403030102";
    public const string audio_403030103 = "study_ten_add/sound/403030103";
    public const string audio_403030104 = "study_ten_add/sound/403030104";
    public const string audio_403030105 = "study_ten_add/sound/403030105";
    public const string audio_403030106 = "study_ten_add/sound/403030106";
    //numbers
    public const string audio_number_base = "study_ten_add/sound/";
    public static List<string> audio_number = new List<string>
    {
        "403020202",
        "403020203",
        "403020204",
        "403020205",
        "403020206",
        "403020207",
        "403020208",
        "403020209",
        "403020210",
        "403020211"
    };

    public static string GetNumberAudioname(int num)
    {
        return audio_number_base + audio_number[num - 1];
    }

    //
    public const string audio_403030201 = "study_ten_add/sound/403030201";
    public const string audio_403030202 = "study_ten_add/sound/403030202";
    public const string audio_403030203 = "study_ten_add/sound/403030203";
    public const string audio_403030204 = "study_ten_add/sound/403030204";
    public const string audio_403030205 = "study_ten_add/sound/403030205";
    public const string audio_403030206 = "study_ten_add/sound/403030206";
    public const string audio_403030207 = "study_ten_add/sound/403030207";
    public const string audio_403030208 = "study_ten_add/sound/403030208";
    public const string audio_403030209 = "study_ten_add/sound/403030209";
    public const string audio_403030210 = "study_ten_add/sound/403030210";//组合读音，单独列出来？

    public const string audio_403030301 = "study_ten_add/sound/403030301";
    public const string audio_403030302 = "study_ten_add/sound/403030302";
    public const string audio_403030303 = "study_ten_add/sound/403030303";
    public const string audio_403030304 = "study_ten_add/sound/403030304";

    public const string ledi = "study_ten_add/study_addplus_prefab_sheep/101001000";

    public const string xunzhangEffect = "Effect/502003002";

    public const string sahua = "Effect/502003005";

    public const string atlasPath = "atlas/atlas_study_tenplus/tenplus";
    public const string common_atlasPath = "atlas/atlas_common/CommonAtlas";

    public const string numbers_normalBackground = "bg_card";
    public const string numbers_overBackground = "bg_card";
    public static List<string> numbers = new List<string> {
        "0","1","2","3","4","5","6","7","8","9"
    };

    public static List<string> boxNumbers = new List<string> {
        "study_ten_add/study_addplus_textures_number/1",
        "study_ten_add/study_addplus_textures_number/2",
        "study_ten_add/study_addplus_textures_number/3",
        "study_ten_add/study_addplus_textures_number/4",
        "study_ten_add/study_addplus_textures_number/5",
        "study_ten_add/study_addplus_textures_number/6",
        "study_ten_add/study_addplus_textures_number/7",
        "study_ten_add/study_addplus_textures_number/8",
        "study_ten_add/study_addplus_textures_number/9",
        "study_ten_add/study_addplus_textures_number/10"
    };

    /// <summary>
    /// UI资源路径
    /// </summary>
    public const string m_ViewPath = "study_ten_add/study_addplus_ugui_panel/StudyAddPlusPanel";
    

    /// <summary>
    /// 学习过程数据控制
    /// </summary>
    public const string ProcedureData = "study_ten_add/study_addplus_data/303030_serializedData";


    public const string hand = "study_ten_add/study_addplus_prefab_sheep/hand";

    #endregion

    #region 羊
    /// <summary>
    /// 羊模板[羊和羊毛是分开的]
    /// </summary>
    public const string sheepModule = "study_ten_add/study_addplus_prefab_sheep/sheepModule";

    /// <summary>
    /// 白羊没羊毛
    /// </summary>
    //public const string whiteSheepWithoutFeather = "study_ten_add/study_addplus_prefab_sheep/303030201_White";

    /// <summary>
    /// 白羊毛
    /// </summary>
    //public const string whiteSheepFeather = "study_ten_add/study_addplus_prefab_sheep/303030202_White";
    /// <summary>
    /// 灰羊没羊毛
    /// </summary>
    //public const string graySheepWithoutFeather = "study_ten_add/study_addplus_prefab_sheep/303030201_Gray";

    /// <summary>
    /// 灰羊毛
    /// </summary>
    //public const string graySheepFeather = "study_ten_add/study_addplus_prefab_sheep/303030202_Gray";

    public const string grayMaterial = "study_ten_add/Materials/gray";

    public const string whiteMaterial = "study_ten_add/Materials/white";
    #endregion

    #region 赶羊
    public const string SheepHerdConfig = "study_ten_add/study_addplus_data/303030_12_config";

    /// <summary>
    /// 一个羊圈的资源路径
    /// </summary>
    public const string oneSheepFold = "study_ten_add/study_addplus_scene/303030100";
    /// <summary>
    /// 两个羊圈的资源路径
    /// </summary>
    public const string twoSheepFold = "study_ten_add/study_addplus_scene/303030101";
   
    
    #endregion

    #region 剪羊毛

    public const string SheepFeatherCutConfig = "study_ten_add/study_addplus_data/303030200_config";

    public const string cutSheepFeatherScene = "study_ten_add/study_addplus_scene/303030200";

    public const string sprite_feather_white = "303030202_white_ui";

    public const string sprite_feather_gray = "303030202_gray_ui";

    public const string sprite_feather_white_frame = "yangmaoxuxian";

    public const string sprite_feather_gray_frame = "yangmaoxuxian02";

    #endregion

    #region 运羊毛

    public const string TransportFeatherScene = "study_ten_add/study_addplus_scene/303030300";

    public const string TransportFeatherConfigData = "study_ten_add/study_addplus_data/303030300_config";

    public const string TransportFeather_Car = "study_ten_add/study_addplus_prefab_sheep/303030301";

    public const string TransportFeather_Box = "study_ten_add/study_addplus_prefab_sheep/303030302";
    #endregion
}

//注：根据动画|音效时长顺序进行的流程不在此类定义
public class TenPlusAnimLength
{
    public const float animationCrossTime = 0.2f;   //动画过渡时间
    public const float lediEnterTime = 0.6f;        //乐迪入场时间
    public const float lediExitTime = 0.6f;         //乐迪消失时间
    public const float sheepEnterFoldTime = 0.3f;   //绵羊入圈时间
    public const float featherEntertime = 0.8f;     //羊毛飞行时间
    public const float featherExitTime = 0.8f;      //羊毛消失时间

    public const float xunzhangEnterTime = 1f;      //勋章飞行时间
    public const float sheepInfoldNumberEnterTime = 0.2f;//羊圈中羊的数量图片飞行时间
    public const float cardTurn_90_AngleTime = 0.2f;    //卡片翻转90°的时间
    public const float numbersExitTime = 0.6f;      //数字消失时间
    public const float numberShakeTime = 0.6f;      //数字震动时间
    public const float equationEntertime = 0.6f;      //算式出现时间
    public const float equationExittime = 0.6f;       //算式消失时间

    public const float miemieUIFlyTime = 0.3f;      //绵羊咩咩叫的UI飞行时间[和绵羊入圈时间同]
    public const float miemieUIExistTime = 1f;    //咩咩UI存在时间
}
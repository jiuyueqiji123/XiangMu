﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddPlusLedi : MonoBehaviour {
    Action callback;

    public void Setup(Action cb)
    {
        callback = cb;
    }

    private void OnMouseDown()
    {
        if (callback != null) callback();
    }
}

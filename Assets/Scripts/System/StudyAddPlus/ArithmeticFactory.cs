﻿using System;
using System.Collections.Generic;
using UnityEngine;


//10以内加减法生成工厂
public class ArithmeticFactory : Singleton<ArithmeticFactory>
{
    private Degree m_CurDegree = Degree.Low;
    private List<int> m_ArithmeticList;

    //难易
    public enum Degree
    {
        Low,
        Middle,
        High,
    }

    public override void Init()
    {
        base.Init();
        //int num = PlayerPrefs.GetInt("ArithDegree");
        m_CurDegree = Degree.Low;
    }

    public override void UnInit()
    {
        base.UnInit();
    }

    public void GenerateDegree(bool random)
    {
        if (random)
            m_CurDegree = (Degree)UnityEngine.Random.Range(0, 2);
        else
        {
            switch (m_CurDegree)
            {
                case Degree.Low: m_CurDegree = Degree.Middle; break;
                case Degree.Middle: m_CurDegree = Degree.High; break;
                case Degree.High: m_CurDegree = Degree.High; break;
            }
        }
    }

    public List<int> GetArithmeticList()
    {
        return m_ArithmeticList;
    }

    //根据结果只生成加法
    public List<int> GenerateArithmetic(int sum)
    {
        if(sum <= 0 || sum > 10)
            return null;
        if(m_ArithmeticList == null)
            m_ArithmeticList = new List<int>();
        m_ArithmeticList.Clear();
        int count = UnityEngine.Random.Range(1, sum-1);
        m_ArithmeticList.Add(count);
        m_ArithmeticList.Add(sum - count);
        m_ArithmeticList.Add(sum);
        return m_ArithmeticList;
    }
    public List<int> GenerateArithmetic()
    {
        Degree degree = (Degree)UnityEngine.Random.Range(0, 2);
        int sum = GenerateSum(degree);
        return GenerateArithmetic(sum);
    }

    public List<int> GenerateArithmetic(Degree degree)
    {
        int sum = GenerateSum(degree);
        return GenerateArithmetic(sum);
    }

    //根据难易生成结果
    private int GenerateSum(Degree degree)
    {
        int count = 0;
        switch(degree)
        {
            case Degree.Low: count = UnityEngine.Random.Range(2, 5); break;
            case Degree.Middle: count = UnityEngine.Random.Range(6, 8); break;
            case Degree.High:count = UnityEngine.Random.Range(9, 10); break;
        }
        return count;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 羊圈限定辅助类
/// </summary>
public class SheepFoldManager : MonoBehaviour
{
    //包围盒元素父节点
    public GameObject boundBoxElementParent;

    //想要的Y轴高度
    public float axisY = 0;

    //元素边长
    public float elementEdgeLength = 1f;

    public float elementEdgeWidth = 1f;

    //场景内的凸四边形范围限定
    public GameObject boundLimitParent;

    //包围盒颜色
    public Color boundBoxColor = Color.green;

    //编辑器场景限定颜色
    public Color limitBoundColor = Color.magenta;
     
    //合法的位置颜色标记
    public Color legalColor = Color.green;
    //非法的位置颜色标记
    public Color inLegalColor = Color.red;

    public GameObject roomlimitObj;

    public float sheepLowest = 0f;
    public float sheepHighest = 80f;

    public bool DrawSheepFoldLimit = false;

    public bool DrawRoomLimit = true;
    RoomLimit roomLimit;

    bool IsInitOver = false;
    
    SheepFoldAreaDetect areaDetect;

    public SheepFoldAreaDetect Init()
    {
        if (boundBoxElementParent == null ||
            boundLimitParent==null || 
            boundBoxElementParent.transform.childCount != 4 ||
            boundLimitParent.transform.childCount != 4) return null;
        else
        {
            int i = 0;
            List<Vector2> listBoundbox = new List<Vector2>();
            for (; i < boundBoxElementParent.transform.childCount; i++)
                listBoundbox.Add(boundBoxElementParent.transform.GetChild(i).transform.position.Vector3ToVector2_XZ());

            List<Vector2> listLimitPoints = new List<Vector2>();
            for (i=0; i < boundLimitParent.transform.childCount; i++)
                listLimitPoints.Add(boundLimitParent.transform.GetChild(i).transform.position.Vector3ToVector2_XZ());
            

            areaDetect = new SheepFoldAreaDetect(listBoundbox, listLimitPoints, elementEdgeLength, elementEdgeWidth);

            if (roomlimitObj != null)
            {
                List<Vector2> xzPlanePoints = new List<Vector2>();
                for( i=0;i<roomlimitObj.transform.childCount;i++)
                    xzPlanePoints.Add(roomlimitObj.transform.GetChild(i).position.Vector3ToVector2_XZ());
                Rectangle xzPlane = new Rectangle(xzPlanePoints);
                roomLimit = new RoomLimit(new Vector2(sheepLowest, sheepHighest), xzPlane);
            }

        }
        IsInitOver = true;
        return areaDetect;
    }

    public RoomLimit GetRoomlimit()
    {
        return roomLimit;
    }

    void Update()
    {
        if (!IsInitOver)
            Init();
        if (IsInitOver)
        {
            if (DrawSheepFoldLimit)
            {
                areaDetect.theLimitArea.DrawRect(boundBoxColor, axisY);
                areaDetect.theBiggestBound.DrawRect(limitBoundColor, axisY);
                for (int i = 0; i < areaDetect.points.GetUpperBound(0) + 1; i++)
                {
                    for (int j = 0; j < areaDetect.points.GetUpperBound(1) + 1; j++)
                    {
                        Rectangle boxElement = new Rectangle(areaDetect.points[i, j], elementEdgeLength, elementEdgeWidth);
                        if (areaDetect.sheepGroup[i, j] == 0)
                        {
                            boxElement.DrawRect(legalColor, axisY);
                        }
                        else
                        {
                            boxElement.DrawRect(inLegalColor, axisY);
                        }
                    }
                }

            }
            if (roomLimit != null)
            {
                if(DrawRoomLimit)
                roomLimit.Draw(Color.cyan);
            }
        }
    }
}

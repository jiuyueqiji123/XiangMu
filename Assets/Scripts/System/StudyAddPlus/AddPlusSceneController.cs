﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class AddPlusSceneController : Singleton<AddPlusSceneController>
{
    private Transform m_AddPlusScene;
    private int m_SumSheep;
    private List<int> m_ArithmeticList;

    //所有的羊
    private List<Transform> m_SheepList;

    //已经进羊圈的羊
    private List<Transform> m_InsideList;
    //在羊圈外面的羊
    private List<Transform> m_OutsideList;

    public override void Init()
    {
        base.Init();
        ArithmeticFactory.Instance.Init();
        m_ArithmeticList = ArithmeticFactory.Instance.GenerateArithmetic();
        if (m_ArithmeticList == null)
            return;
        m_SumSheep = m_ArithmeticList[2];
    }

    public override void UnInit()
    {
        base.UnInit();
        ArithmeticFactory.Instance.UnInit();
    }

    public void ExistScene()
    {
        UnInit();
    }

    private void InitScene()
    {
        CreateScene();
    }
    private void CreateScene()
    {

    }
    private void CreateSheep()
    {

    }
}

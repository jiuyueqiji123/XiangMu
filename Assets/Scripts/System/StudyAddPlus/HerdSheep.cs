﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

/// <summary>
/// 赶羊进羊圈
/// </summary>
public class HerdSheep
{
    private GameObject config;

    private GameObject m_oneSheepFold;
    private GameObject m_twoSheepFold;
    private List<GameObject> m_whiteSheeps = new List<GameObject>();
    private List<GameObject> m_graySheeps = new List<GameObject>();
    private int sheepFoldNum;
    private int factor1 = 0;
    private int factor2 = 0;
    private int sum = 0;
    private TenPlusModuleRuntimeData runtimeData;

    private GameObject oneSheepFoldSpawnPointsParent;
    private GameObject twoSheepFoldSpawnPointsParent;

    private RoomLimit roomLimit;

    private GameRound gameround;

    private SheepFoldAreaDetect onefoldDetect;
    private SheepFoldAreaDetect twofoldDetect1;
    private SheepFoldAreaDetect twofoldDetect2;

    private GameObject oneFoldDetectObj;
    private GameObject twoFoldDetectObj;
    

    private GameObject Ledi;
    private Animator lediAni;

    private bool IsNumberBtnCanClick = false;
    
    private float lastOperateTime;

    private GameObject sahuaObj;

    private Camera cacheCam;

    private GameObject handEndPos;

    private GameObject handObj;

    private GameObject adaptConfig;

    public int currentFoldNum
    {
        get
        {
            return gameround.sheepfoldNum;
        }
    }

    public SheepFoldAreaDetect onefold { get { return onefoldDetect; } }

    public SheepFoldAreaDetect twofold1 { get { return twofoldDetect1; } }

    public SheepFoldAreaDetect twofold2 { get { return twofoldDetect2; } }

    public float foldHeight { get { return 80f; } }
    /// <summary>
    /// 初始化场景
    /// </summary>
    /// <param name="r">该回合信息</param>
    /// <param name="num">当前计算式之和</param>
    public HerdSheep(GameRound r, int s, TenPlusModuleRuntimeData trd)
    {
        Reset(r,s,trd);
    }

    #region 加载

    public void Reset(GameRound r, int s, TenPlusModuleRuntimeData trd)
    {
        gameround = r;
        runtimeData = trd;
        sheepFoldNum = r.sheepfoldNum;
        sum = s;
        factor1 = UnityEngine.Random.Range(1, s);
        factor2 = s - factor1;

        Resource resource;
        resource = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.SheepHerdConfig, typeof(GameObject), enResourceType.Prefab);
        if (config == null)
            config = GameObject.Instantiate(resource.m_content) as GameObject;

        //适配方案
        
        Resolution resolution = AddPlusController.Instance.resolution;
        float rate = resolution.width / resolution.height;
        if (rate > AddPlusController.Instance.HighRes)
            adaptConfig = config.transform.Find("high").gameObject;
        else if (rate < AddPlusController.Instance.LowRes)
            adaptConfig = config.transform.Find("low").gameObject;
        else
            adaptConfig = config.transform.Find("middle").gameObject;

        //1.根据羊圈个数加载羊圈资源
        LoadSheepFold(r.sheepfoldNum);

        //2.加载羊圈数据
        LoadFoldDetect();

        //3.根据难度得到羊的个数并让羊站位
        //一个羊圈只有白羊,两个羊圈会有白羊和灰羊
        LoadSheep(r.hardlevel, r.sheepfoldNum);

        //4.初始化勋章个数
        LoadHonor();

        //5.加载乐迪
        LoadLedi();

        //6.注册事件
        Register();

        //7.初始化控制变量
        InitCtrParams();

        //8.游戏开始
        GameStart();
    }

    //加载乐迪
    void LoadLedi()
    {
        if (Ledi == null)
        {
            Ledi = new GameObject("ledi(1)");
            Resource r = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.ledi, typeof(GameObject), enResourceType.Prefab);
            GameObject lediModel = GameObject.Instantiate(r.m_content) as GameObject;
            lediModel.transform.SetParent(Ledi.transform);
            lediAni = lediModel.GetComponent<Animator>();
            Ledi.transform.position = config.transform.Find("ledi/1").position;
            Ledi.transform.localRotation = config.transform.Find("ledi/1").localRotation;
        }
    }

    void InitCtrParams()
    {
        IsNumberBtnCanClick = true;
        handEndPos = config.transform.Find("hand").gameObject;
    }

    void Register()
    {
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_NumbersBtn_Click, this.NumberClick);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_Numbes_HideOver, this.NumberHideOver);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_RoundOver, this.RoundOver);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_UpdateOperateTime, this.UpdateOperateTime);
    }

    void RemoveLisenter()
    {
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_NumbersBtn_Click, this.NumberClick);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_Numbes_HideOver, this.NumberHideOver);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_RoundOver, this.RoundOver);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_UpdateOperateTime, this.UpdateOperateTime);
    }


    void LoadSheepFold(int num)
    {
        Resource r;
        if (num == 1)
        {
            if (m_oneSheepFold == null)
            {
                r = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.oneSheepFold, typeof(GameObject), enResourceType.Prefab);

                m_oneSheepFold = GameObject.Instantiate(r.m_content) as GameObject;
            }
            m_oneSheepFold.SetActive(true);


            if (oneSheepFoldSpawnPointsParent == null)
                oneSheepFoldSpawnPointsParent = adaptConfig.transform.Find("oneFoldSheepPostion").gameObject;

            if (m_twoSheepFold != null)
                m_twoSheepFold.SetActive(false);

            cacheCam = m_oneSheepFold.GetComponentInChildren<Camera>();
        }
        else
        {
            if (m_twoSheepFold == null)
            {
                r = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.twoSheepFold, typeof(GameObject), enResourceType.Prefab);
                m_twoSheepFold = GameObject.Instantiate(r.m_content) as GameObject;
            }
            m_twoSheepFold.SetActive(true);

            if (twoSheepFoldSpawnPointsParent == null)
                twoSheepFoldSpawnPointsParent = adaptConfig.transform.Find("twoFoldSheepPosition").gameObject;

            if (m_oneSheepFold != null)
                m_oneSheepFold.SetActive(false);
        }
    }

    //一个羊圈:羊圈中有某几只羊
    //两个羊圈:一个羊圈是满的，另一个羊圈是空的
    void LoadSheep(HardLevelEnum hdLevel, int foldnum)
    {
        m_whiteSheeps.Clear();
        m_graySheeps.Clear();
        int notinfoldShepNum = 0;//不在羊圈的羊个数
        int length = 0;
        Resource r;
        if (foldnum == 1)
        {
            if (sum == 1)
                notinfoldShepNum = sum;
            else
                notinfoldShepNum = sum - UnityEngine.Random.Range(1, sum);

            length = sum;
            for (int i = 0; i < length; i++)
            {
                r = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.sheepModule, typeof(GameObject), enResourceType.Prefab);
                GameObject sheep = GameObject.Instantiate(r.m_content) as GameObject;
                sheep.GetComponent<Collider>().enabled = false;
                m_whiteSheeps.Add(sheep);

                Vector3 target = oneSheepFoldSpawnPointsParent.transform.GetChild(i).transform.position;
                bool infold = false;
                if (i >= notinfoldShepNum)
                {
                    target = onefoldDetect.GetLegalPos().Vector2ToVector3_XZ(target.y);
                    infold = true;
                    sum--;
                }
                sheep.AddComponent<SheepModule>().Set(SheepType.whiteSheepWithFeather, target, this, roomLimit, i * 1f,infold);
                
            }
        }
        else
        {
            int whoInFold = UnityEngine.Random.Range(1, 100) % 2;

            int i = 0;
            //if (factor1 == 1)
            //    notinfoldShepNum = factor1;
            //else
            //{
            //    notinfoldShepNum = factor1 - UnityEngine.Random.Range(1, factor1);
            //    Debug.Log("factor1 羊圈外:"+notinfoldShepNum);
            //}

            int notIn1;
            int notin2;

            if (whoInFold == 0)
            {
                notIn1 = factor1;
                notin2 = 0;
                Debug.Log("白羊都不在羊圈");
            }
            else
            {
                notIn1 = 0;
                notin2 = factor2;
                Debug.Log("灰羊都不在羊圈");
            }

            length = factor1;
            for (i = 0; i < length; i++)
            {


                r = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.sheepModule, typeof(GameObject), enResourceType.Prefab);
                GameObject sheep = GameObject.Instantiate(r.m_content) as GameObject;
                sheep.GetComponent<Collider>().enabled = false;
                m_whiteSheeps.Add(sheep);

                
                Vector3 target = twoSheepFoldSpawnPointsParent.transform.GetChild(i).transform.position;
                bool infold = false;
                if (i >= notIn1)
                {
                    target = twofoldDetect1.GetLegalPos().Vector2ToVector3_XZ(target.y);
                    infold = true;
                    factor1--;
                }
                sheep.AddComponent<SheepModule>().Set(SheepType.whiteSheepWithFeather, target, this, roomLimit, i * 1f,infold);
            }

            //if (factor2 == 1)
            //    notinfoldShepNum = factor2;
            //else
            //{
            //    notinfoldShepNum = factor2 - UnityEngine.Random.Range(1, factor2);
            //}
            
            length = factor2;
            for (int j = 0; j < length; j++)
            {
                r = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.sheepModule, typeof(GameObject), enResourceType.Prefab);
                GameObject sheep = GameObject.Instantiate(r.m_content) as GameObject;
                sheep.GetComponent<Collider>().enabled = false;
                m_graySheeps.Add(sheep);

                //sheep.AddComponent<SheepModule>().Set(SheepType.graySheepWithFeather, twoSheepFoldSpawnPointsParent.transform.GetChild(i + j).transform.position, this, roomLimit);
                Vector3 target = twoSheepFoldSpawnPointsParent.transform.GetChild(i+j).transform.position;
                bool infold = false;
                if (j >= notin2)
                {
                    target = twofoldDetect2.GetLegalPos().Vector2ToVector3_XZ(target.y);
                    infold = true;
                    factor2--;
                }
                sheep.AddComponent<SheepModule>().Set(SheepType.graySheepWithFeather, target, this, roomLimit, i * 1f, infold);

            }
        }
    }

    //场景开始前先在羊圈里边放几只羊
    void EnterFoldFirst(GameObject sheep)
    {

    }

    //加载羊圈数据
    void LoadFoldDetect()
    {
        if (oneFoldDetectObj == null)
            oneFoldDetectObj = adaptConfig.transform.Find("oneSheepFoldDetect").gameObject;
        onefoldDetect = oneFoldDetectObj.GetComponent<SheepFoldManager>().Init();
        roomLimit = oneFoldDetectObj.GetComponent<SheepFoldManager>().GetRoomlimit();

        if (twoFoldDetectObj == null)
            twoFoldDetectObj = adaptConfig.transform.Find("twoSheepFoldDetect").gameObject;

        twofoldDetect1 = twoFoldDetectObj.transform.GetChild(0).GetComponent<SheepFoldManager>().Init();
        twofoldDetect2 = twoFoldDetectObj.transform.GetChild(1).GetComponent<SheepFoldManager>().Init();
    }


    //加载勋章
    void LoadHonor()
    {
        HUIEvent param = new HUIEvent();
        param.m_eventID = enUIEventID.AddPlus_Image_XunZhangAdd;
        param.m_eventParams.argInt = runtimeData.currentRoundIndex;
        HUIEventManager.Instance.DispatchUIEvent(param);
    }
    #endregion

    #region 超时处理

    /// <summary>
    /// 开启总计时器
    /// </summary>
    void StartTimer()
    {
        lastOperateTime = Time.realtimeSinceStartup;
        TimerManager.Instance.AddTimer(100, int.MaxValue, Detect);
    }

    void UpdateOperateTime(HUIEvent hUIEvent)
    {
        Updatetime();
    }

    void Updatetime()
    {
        lastOperateTime = Time.realtimeSinceStartup;
    }


    private void Detect(int timerSequence)
    {
        if (Time.realtimeSinceStartup - lastOperateTime > TenPlusModuleAssetPath.timeout)
        {
            Debug.Log("超时了.");
            lastOperateTime = Time.realtimeSinceStartup;
            if (gameround.sheepfoldNum == 1)
            {
                if (sum == 0)
                {
                    //围栏的羊变成几只了呢？
                    AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_403030102);
                }
                else
                {
                    //我们把小羊赶回去吧
                    AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_403030105);
                }
            }
            else
            {
                if (factor1 == 0 && factor2 == 0)
                {
                    //围栏的羊变成几只了呢？
                    AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_403030102);
                }
                else
                {
                    //我们把小羊赶回去吧
                    AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_403030105);
                }
            }
        }
    }

    void RemoveTimer()
    {
        TimerManager.Instance.RemoveTimer(this.Detect);
    }

    #endregion

    #region 流程

    void GameStart()
    {
        //限定:第一个回合有乐迪提示
        if (runtimeData.currentRoundIndex == 0)
        {
            //乐迪
            //播放音乐&乐迪动画
            //乐迪离开
            //手势指引

            StartIndtroduce();
        }
        else
        {
            EnableSheep();
        }
    }

    //显示手势指引
    void ShowGesturesGuide()
    {
        //Vector2 v2 = cacheCam.WorldToScreenPoint(m_whiteSheeps[0].transform.position);
        //string str = string.Format("{0},{1}",v2.x,v2.y);
        //HUIEventManager.Instance.DispatchUIEvent(enUIEventID.AddPlus_GestureGuide_Show, new stUIEventParams { argString = str });
        Resource r;
        r = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.hand,typeof(GameObject),enResourceType.Prefab);
        handObj = GameObject.Instantiate(r.m_content) as GameObject;
        handObj.transform.position = m_whiteSheeps[0].transform.position + Vector3.up*100 + new Vector3(-60f, 0f, 0f);

        Animator handani = handObj.GetComponent<Animator>();
        float time = AddPlusController.GetAnimationClipLength(handani.runtimeAnimatorController,TenPlusModuleAssetPath.anim_hand_click);
        TimerManager.Instance.AddTimer(time,()=>
        {
            handObj.transform.DOMove(handEndPos.transform.position + Vector3.up * 100 , time + 1f).SetEase(Ease.Linear).OnComplete(() =>
            {
                handObj.SetActive(false);
                EnableSheep();
            });
        });
        
        //TimerManager.Instance.AddTimer(time+1f,EnableSheep);
    }

    //手势指引隐藏检测
    void GestureDisplayTest()
    {
        if (runtimeData.currentLearnProcedureIndex == 1 && runtimeData.currentRoundIndex == 0)
        {
            handObj.SetActive(false);
        }
    }

    //开场介绍 music&乐迪介绍动画
    void StartIndtroduce()
    {
        Vector3 endPos = config.transform.Find("ledi/2").position;
        Ledi.transform.DOMove(endPos, TenPlusAnimLength.lediEnterTime).SetEase(Ease.Flash).OnComplete(()=> 
        {
            //播放动画&语音
            lediAni.CrossFade(TenPlusModuleAssetPath.anim_ledi_happy, TenPlusAnimLength.animationCrossTime);
            float showtime = AddPlusController.GetAudioClipLength(TenPlusModuleAssetPath.audio_403030100);
            float anitime = AddPlusController.GetAnimationClipLength(lediAni.runtimeAnimatorController, TenPlusModuleAssetPath.anim_ledi_happy) + 0.2f;
            showtime = showtime > anitime ? showtime : anitime;
            AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_403030100);
            TimerManager.Instance.AddTimer(showtime, HideLedi);
        });
    }

    void HideLedi()
    {
        Vector3 endPos = config.transform.Find("ledi/1").position;
        Ledi.transform.DOMove(endPos, TenPlusAnimLength.lediExitTime).SetEase(Ease.Flash).OnComplete(()=> 
        {
            //限定:在第一个学习过程有手势指引
            if (runtimeData.currentLearnProcedureIndex == 1 && runtimeData.currentRoundIndex == 0)
            {
                //未完待续
                ShowGesturesGuide();
            }
            else
                EnableSheep();
        });

    }

    void HideLediOnly()
    {
        Vector3 endPos = config.transform.Find("ledi/1").position;
        Ledi.transform.DOMove(endPos, TenPlusAnimLength.lediExitTime).SetEase(Ease.Flash);
    }

    void EnableSheep()
    {
        
        foreach (GameObject go in m_whiteSheeps)
            go.GetComponent<Collider>().enabled = true;
        foreach (GameObject go in m_graySheeps)
            go.GetComponent<Collider>().enabled = true;

        StartTimer();
    }




    void DestroySheep()
    {
        foreach (GameObject go in m_whiteSheeps)
            GameObject.DestroyImmediate(go);
        foreach (GameObject go in m_graySheeps)
            GameObject.DestroyImmediate(go);
    }

    int theRresult;
    void DragEndDetect()
    {
        bool isDragOver = false;
        
        if (sheepFoldNum == 1)
        {
            if (sum == 0)
            {
                theRresult = m_whiteSheeps.Count;
                isDragOver = true;
            }
        }
        else
        {
            if (factor2 == 0 && factor1 == 0)
            {
                theRresult = m_whiteSheeps.Count + m_graySheeps.Count;
                isDragOver = true;
            }
        }
        
        if (isDragOver)
        {
            //乐迪语音
            float showtime = AddPlusController.GetAudioClipLength(TenPlusModuleAssetPath.audio_403030102);
            AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_403030102);
            TimerManager.Instance.AddTimer(showtime, DispatchNumbersShowUp);

        }
    }

    void DispatchNumbersShowUp()
    {
        HideLediOnly();
        HUIEvent huievent = new HUIEvent();
        huievent.m_eventID = enUIEventID.AddPlus_Numbers_ShowUp;
        huievent.m_eventParams.argString = MathHelp.GetThreeNoSameNumber(1, 10, theRresult);
        HUIEventManager.Instance.DispatchUIEvent(huievent);
    }

    /// <summary>
    /// 绵羊入圈检测
    /// </summary>
    /// <param name="color">绵羊颜色</param>
    /// <param name="sheepPos">绵羊位置</param>
    public bool SheepEnterFoldTest(int color, Vector3 sheepPos, GameObject sheep, float axisY,out Vector3 pos)
    {
        Vector2 legalPos;
        if (sheepFoldNum == 1)
        {
            //一个羊圈
            if (onefoldDetect.TryPutSheepIn(sheepPos.Vector3ToVector2_XZ(), false, out legalPos))
            {
                pos = legalPos.Vector2ToVector3_XZ(axisY);
                return true;
            }
            else
            {
                pos = Vector3.zero;
                return false;
            }
        }
        else
        {
            //两个羊圈
            if (color < (int)SheepType.graySheepClean)
            {
                //白
                if (twofoldDetect1.TryPutSheepIn(sheepPos.Vector3ToVector2_XZ(),false, out legalPos))
                {
                    pos = legalPos.Vector2ToVector3_XZ(axisY);
                    return true;
                }
                else
                {
                    pos = Vector3.zero;
                    return false;
                }
            }
            else
            {
                //灰
                if (twofoldDetect2.TryPutSheepIn(sheepPos.Vector3ToVector2_XZ(), false,out legalPos))
                {
                    pos = legalPos.Vector2ToVector3_XZ(axisY);
                    return true;
                }
                else
                {
                    pos = Vector3.zero;
                    return false;
                }
            }
        }
    }

    
    /// <summary>
    /// 绵羊入圈检测
    /// </summary>
    /// <param name="color">绵羊颜色</param>
    /// <param name="sheepPos">绵羊位置</param>
    public void SheepEnterFold(int color, Vector3 sheepPos, GameObject sheep, float axisY)
    {
        GestureDisplayTest();

        Vector2 legalPos;
        if (sheepFoldNum == 1)
        {
            //一个羊圈
            if (onefoldDetect.TryPutSheepIn(sheepPos.Vector3ToVector2_XZ(), true,out legalPos))
            {

                sheep.transform.DOMove(legalPos.Vector2ToVector3_XZ(axisY), TenPlusAnimLength.sheepEnterFoldTime);
                sum--;
            }
        }
        else
        {
            //两个羊圈
            if (color < (int)SheepType.graySheepClean)
            {
                //白
                if (twofoldDetect1.TryPutSheepIn(sheepPos.Vector3ToVector2_XZ(),true ,out legalPos))
                {
                    sheep.transform.DOMove(legalPos.Vector2ToVector3_XZ(axisY), TenPlusAnimLength.sheepEnterFoldTime);
                    factor1--;
                }
            }
            else
            {
                //灰
                
                if (twofoldDetect2.TryPutSheepIn(sheepPos.Vector3ToVector2_XZ(), true, out legalPos))
                {
                    sheep.transform.DOMove(legalPos.Vector2ToVector3_XZ(axisY), TenPlusAnimLength.sheepEnterFoldTime);
                    factor2--;
                }
            }
        }
        //Debug.Log(string.Format("{0},{1},{2}",factor1,factor2,sum));
        DragEndDetect();
    }


    //点击数字
    public void NumberClick(HUIEvent hUIEvent)
    {
        Updatetime();
        if (!IsNumberBtnCanClick) return;
        int numberIndex = hUIEvent.m_eventParams.argInt2;
        if (hUIEvent.m_eventParams.argInt == theRresult)
        {
            IsNumberBtnCanClick = false;
            //语音:你真棒
            float showtime = AddPlusController.GetAudioClipLength(TenPlusModuleAssetPath.audio_403030103);
            AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_403030103);
            TimerManager.Instance.AddTimer(showtime, DisPatchNumberHide);
        }
        else
        {
            AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_603030303);
            HUIEventManager.Instance.DispatchUIEvent(enUIEventID.AddPlus_WrongNumber_Click, new stUIEventParams { argInt = numberIndex});
        }
    }

    void DisPatchNumberHide()
    {
        //发送所有数字隐藏信息
        HUIEventManager.Instance.DispatchUIEvent(enUIEventID.AddPlus_Numbers_Hide);
        
    }

    public void NumberHideOver(HUIEvent hUIEvent)
    {
        RemoveTimer();
        DispatchXunzhangAnim();
    }

    void DispatchXunzhangAnim()
    {
        //发送勋章+1的消息
        HUIEvent ev = new HUIEvent();
        ev.m_eventID = enUIEventID.AddPlus_StartMove_Xunzhang;
        ev.m_eventParams.argInt = runtimeData.currentRoundIndex + 1;
        HUIEventManager.Instance.DispatchUIEvent(ev);
    }

    //回合结束，做本脚本内的清理工作
    public void RoundOver(HUIEvent hUIEvent)
    {
        //回合结束
        if (IsNeedClapInteractive())
        {
            Clap();
        }
        else
        {
            TheEnd();
        }
    }


    //是否需要击掌
    void Clap()
    {
        //加载乐迪击掌模型
        //击掌特效
        //等待击掌
        Ledi.SetActive(true);
        Ledi.transform.position = config.transform.Find("ledi/2").position;
        lediAni.SetBool("enterClap", true);
        float clapAnimLength = AddPlusController.GetAnimationClipLength(lediAni.runtimeAnimatorController, TenPlusModuleAssetPath.anim_ledi_clap);
        TimerManager.Instance.AddTimer(clapAnimLength, ShowClapBowen);
    }

    void ShowClapBowen()
    {
        GameObject effect = Ledi.transform.GetChild(0).Find("Effect").gameObject;
        effect.SetActive(true);
        effect.AddComponent<AddPlusLedi>().Setup(ClapOver);
    }

    void ClapOver()
    {
        GameObject effect = Ledi.transform.GetChild(0).Find("Effect").gameObject;
        effect.SetActive(false);
        GameObject.Destroy(effect.GetComponent<AddPlusLedi>());
        lediAni.CrossFade(TenPlusModuleAssetPath.anim_ledi_clap2, TenPlusAnimLength.animationCrossTime);
        //音效 啪
        AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_603030501);
        //撒花特效
        FlowerShow();
    }

    void FlowerShow()
    {
        Resource r;
        r = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.sahua, typeof(GameObject), enResourceType.Prefab);
        sahuaObj = GameObject.Instantiate(r.m_content) as GameObject;
        sahuaObj.transform.position = config.transform.Find("sahua").transform.position;

        TimerManager.Instance.AddTimer(1f,TheEnd);
    }


    void TheEnd()
    {
        DoClean();
        //发送 游戏数据改变信息
        HUIEventManager.Instance.DispatchUIEvent(enUIEventID.AddPlus_GameProcedure_Change);
    }

    //是否需要击掌交互
    bool IsNeedClapInteractive()
    {
        if (runtimeData.currentRoundIndex == 3)
            return true;
        else
            return false;
    }

    #endregion

    #region 善后


    public void HideScene()
    {
        if (m_oneSheepFold != null) m_oneSheepFold.SetActive(false);
        if (m_twoSheepFold != null) m_twoSheepFold.SetActive(false);
    }


    public void DoClean()
    {
        TimerManager.Instance.RemoveAllTimer();
        DestroySheep();
        RemoveLisenter();
        GameObject.DestroyImmediate(Ledi);
        GameObject.DestroyImmediate(sahuaObj);
        GameObject.DestroyImmediate(handObj);
    }

    #endregion
}

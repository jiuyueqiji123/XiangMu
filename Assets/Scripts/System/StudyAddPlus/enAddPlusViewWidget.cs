﻿using System;
using System.Collections.Generic;

/// <summary>
/// 控件索引
/// </summary>
public enum enAddPlusViewWidget
{
    Button_Return,
    Xunzhang_Start,
    Xunzhang_parent,
    SheepInFoldNumbers,
    Equation,
    SheepInfoldNumbersEndPosition,
    SheepInfoldNumbersStartPosition,
    EquationStart,
    EquationEnd,
    SheepShout,
    GestureGuide,
    imageObj,
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// 赶羊位置辅助
/// </summary>
public class SheepFoldAreaDetect
{
    /// <summary>
    /// 最外围的四边形
    /// </summary>
    public Rectangle theBiggestBound;

    /// <summary>
    /// 羊群限定区域
    /// </summary>
    public Rectangle theLimitArea;

    /// <summary>
    /// 羊长
    /// </summary>
    float sheepLength;

    /// <summary>
    /// 羊宽
    /// </summary>
    float sheepWidth;

    /// <summary>
    /// 羊群在区域中的占位状态
    /// </summary>
    public int[,] sheepGroup;

    /// <summary>
    /// 最大包围盒内的全部区域划分
    /// </summary>
    public Vector2[,] points;

    /// <summary>
    /// 初始化
    /// </summary>
    /// <param name="theBiggestBoundPoints"></param>
    /// <param name="theSheepLimitAreaPoints"></param>
    /// <param name="length"></param>
    /// <param name="width"></param>
    public SheepFoldAreaDetect(
        List<Vector2> theBiggestBoundPoints,
        List<Vector2> theSheepLimitAreaPoints,
        float length,
        float width)
    {
        this.theBiggestBound = InitBoundBoxRectangleByPoints(theBiggestBoundPoints);
        this.theLimitArea = new Rectangle(theSheepLimitAreaPoints.ToArray());
        sheepLength = length;
        sheepWidth = width;
        InitPoints_SheepGroup();
    }

    //初始化格子和羊群
    void InitPoints_SheepGroup()
    {
        float maxx = theBiggestBound.rightUp.x;
        float maxy = theBiggestBound.rightUp.y;
        float minx = theBiggestBound.leftBottom.x;
        float miny = theBiggestBound.leftBottom.y;
        int Xnum = Mathf.FloorToInt(Mathf.Abs(maxx - minx) / sheepLength);
        float XInsertLength = (Mathf.Abs(maxx-minx) - Xnum * sheepLength) / (Xnum + 1);

        int Znum = Mathf.FloorToInt(Mathf.Abs(maxy - miny) / sheepWidth);
        float ZInsertLength = (Mathf.Abs(maxy - miny) - Znum * sheepWidth) / (Znum + 1);

        points = new Vector2[Xnum, Znum];
        sheepGroup = new int[Xnum, Znum];
        
        for (int i = 0; i < Xnum; i++)
        {
            for (int j = 0; j < Znum; j++)
            {
                points[i, j] = new Vector3(minx + XInsertLength * (i + 1) + sheepLength * (i + 0.5f), miny + ZInsertLength * (j + 1) + sheepWidth * (j + 0.5f));
                
                Rectangle boxElement = new Rectangle(points[i, j], sheepLength, sheepWidth);
                if (MathHelp.GetTwoRectState(theLimitArea, boxElement) == -1)
                {
                    sheepGroup[i, j] = 0;//可用
                }
                else
                {
                    sheepGroup[i, j] = -1;//不可用
                }
            }
        }
        
    }

    /// <summary>
    /// 得到一个合理的位置
    /// </summary>
    /// <returns></returns>
    public Vector2 GetLegalPos()
    {
        int xmax = points.GetUpperBound(0);
        int ymax = points.GetUpperBound(1);
        int randomx;
        int randomy;
        while (true)
        {
            randomx = Random.Range(0, xmax + 1);
            randomy = Random.Range(0, ymax + 1);
            if (sheepGroup[randomx, randomy] == 0)
            {
                sheepGroup[randomx, randomy] = 1;
                return points[randomx, randomy];
            }
        }
    }

    /// <summary>
    /// 尝试将羊赶进羊圈
    /// </summary>
    /// <param name="sheepPos"></param>
    /// <param name="enter">是否做占位动作</param>
    /// <param name="legalPos">被占的那个点</param>
    /// <returns></returns>
    public bool TryPutSheepIn(Vector2 sheepPos, bool enter, out Vector2 legalPos)
    {
        if (MathHelp.IsPointInRect(sheepPos, theBiggestBound) == -1)//点在最大限定范围内
        {
            int xmax = points.GetUpperBound(0);
            int ymax = points.GetUpperBound(1);
            for (int i = 0; i <= xmax; i++)
            {
                for (int j = 0; j <= ymax; j++)
                {
                    if (MathHelp.IsPointInRect(sheepPos, new Rectangle(points[i, j], sheepLength, sheepWidth)) == -1)
                    {
                        //羊的点在当前区域
                        if (sheepGroup[i, j] == 0)
                        {
                            if (enter)
                                sheepGroup[i, j] = 1;
                            legalPos = points[i, j];
                            return true;
                        }
                        else
                        {
                            //该位置已经被占用
                            Vector2Int somewhereEmpty = MathHelp.GetSpaceForSheepWantIn(sheepGroup, new Vector2Int(i, j));

                            if (somewhereEmpty.x == -1 && somewhereEmpty.y == -1)
                            {
                                //所有可用的位置都已经用完
                                legalPos = Vector2.zero;
                                return false;
                            }
                            else
                            {
                                //找到可用的位置
                                if (enter)
                                    sheepGroup[somewhereEmpty.x, somewhereEmpty.y] = 1;
                                legalPos = points[somewhereEmpty.x, somewhereEmpty.y];
                                return true;
                            }
                        }
                    }
                }
            }
            //羊在边界线上，不放入
            legalPos = Vector2.zero;
            return false;
        }
        legalPos = Vector2.zero;
        return false;
    }

    /// <summary>
    /// AABB盒子
    /// </summary>
    /// <param name="thePoints"></param>
    /// <returns></returns>
    public static Rectangle InitBoundBoxRectangleByPoints(List<Vector2> thePoints) 
    {
        Vector2 min = thePoints[0];
        Vector2 max = thePoints[0];
        foreach (Vector2 v2 in thePoints)
        {
            min.x = min.x < v2.x ? min.x : v2.x;
            min.y = min.y < v2.y ? min.y : v2.y;
            max.x = max.x > v2.x ? max.x : v2.x;
            max.y = max.y > v2.y ? max.y : v2.y;

        }

        Rectangle result = new Rectangle(
            new Vector2(min.x, max.y),
            new Vector2(max.x, max.y),
            new Vector2(max.x, min.y),
            new Vector2(min.x, min.y));
        return result;
    }
    
}

/// <summary>
/// 由xz平面和Y轴组成的空间限定盒子
/// </summary>
public class RoomLimit
{
    //Y轴[高]限定
    public Vector2 axisY_min_max;
    //XZ平面
    public Rectangle plane_xz;

    /// <summary>
    /// 初始化
    /// </summary>
    /// <param name="v2"></param>
    /// <param name="r"></param>
    public RoomLimit(Vector2 v2,Rectangle r)
    {
        axisY_min_max = v2;
        plane_xz = r;
    }

    /// <summary>
    /// 限定某点在盒子内部
    /// </summary>
    /// <param name="pointIn">需要限定的点</param>
    /// <param name="legalPos">限定之后的点</param>
    public void LimitPointInThisRoom(Vector3 pointIn, out Vector3 legalPos)
    {
        legalPos = Vector3.zero;
        if (pointIn.y < axisY_min_max.x)
            legalPos.y = axisY_min_max.x;
        else if (pointIn.y > axisY_min_max.y)
            legalPos.y = axisY_min_max.y;
        else
            legalPos.y = pointIn.y;
        

        if (MathHelp.IsPointInRect(pointIn.Vector3ToVector2_XZ(), plane_xz) == -1)
        {
            //在平面xz之内
            legalPos.x = pointIn.x;
            legalPos.z = pointIn.z;
        }
        else
        {
            //不在平面xz之内
            //求点离平面最近的点
            Vector2 nearestPoint;
            MathHelp.GetNearestPointInRectFromPoint(pointIn.Vector3ToVector2_XZ(), plane_xz, out nearestPoint);
            legalPos.x = nearestPoint.x;
            legalPos.z = nearestPoint.y;
        }
    }

    /// <summary>
    /// 限定某点在盒子内部
    /// </summary>
    /// <param name="pointIn">需要限定的点</param>
    /// <param name="legalPos">限定之后的点</param>
    public void LimitPointInThisRoom(Vector3 pointIn,  Rectangle insideRect,float insideRectHeight, out Vector3 legalPos)
    {
        legalPos = Vector3.zero;
        if (pointIn.y < axisY_min_max.x)
            legalPos.y = axisY_min_max.x;
        else if (pointIn.y > axisY_min_max.y)
            legalPos.y = axisY_min_max.y;
        else
            legalPos.y = pointIn.y;

        //Debug.Log(string.Format("{0} {1} {2}", axisY_min_max.x, axisY_min_max.y, insideRectHeight));

        if (MathHelp.IsPointInRect(pointIn, insideRect) == -1)
        {
            if (legalPos.y < insideRectHeight) legalPos.y = insideRectHeight;
        }


        if (MathHelp.IsPointInRect(pointIn.Vector3ToVector2_XZ(), plane_xz) == -1)
        {
            //在平面xz之内
            legalPos.x = pointIn.x;
            legalPos.z = pointIn.z;
        }
        else
        {
            //不在平面xz之内
            //求点离平面最近的点
            Vector2 nearestPoint;
            MathHelp.GetNearestPointInRectFromPoint(pointIn.Vector3ToVector2_XZ(), plane_xz, out nearestPoint);
            legalPos.x = nearestPoint.x;
            legalPos.z = nearestPoint.y;
        }
    }

    public void Draw(Color color)
    {
        Vector3 Up0 = new Vector3(plane_xz.leftUp.x, axisY_min_max.y, plane_xz.leftUp.y);
        Vector3 Up1 = new Vector3(plane_xz.rightUp.x, axisY_min_max.y, plane_xz.rightUp.y);
        Vector3 Up2 = new Vector3(plane_xz.rightBottom.x, axisY_min_max.y, plane_xz.rightBottom.y);
        Vector3 Up3 = new Vector3(plane_xz.leftBottom.x, axisY_min_max.y, plane_xz.leftBottom.y);
        Vector3 Down0 = new Vector3(plane_xz.leftUp.x, axisY_min_max.x, plane_xz.leftUp.y);
        Vector3 Down1 = new Vector3(plane_xz.rightUp.x, axisY_min_max.x, plane_xz.rightUp.y);
        Vector3 Down2 = new Vector3(plane_xz.rightBottom.x, axisY_min_max.x, plane_xz.rightBottom.y);
        Vector3 Down3 = new Vector3(plane_xz.leftBottom.x, axisY_min_max.x, plane_xz.leftBottom.y);
        Debug.DrawLine(Up0, Up1, color);
        Debug.DrawLine(Up1, Up2, color);
        Debug.DrawLine(Up2, Up3, color);
        Debug.DrawLine(Up3, Up0, color);

        Debug.DrawLine(Down0, Down1, color);
        Debug.DrawLine(Down1, Down2, color);
        Debug.DrawLine(Down2, Down3, color);
        Debug.DrawLine(Down3, Down0, color);

        Debug.DrawLine(Up0, Down0, color);
        Debug.DrawLine(Up1, Down1, color);
        Debug.DrawLine(Up2, Down2, color);
        Debug.DrawLine(Up3, Down3, color);
    }

}


/// <summary>
/// 羊圈内格子状态
/// </summary>
public enum SheepFoldAreaState
{
    Disable = -1,           //不可用
    UnUse = 0,              //可用
    Used = 1,               //已经被占用
}



public static class MathHelp
{
    //三维点转为二维点 x->x z->y
    public static Vector2 Vector3ToVector2_XZ(this Vector3 v3)
    {
        return new Vector2(v3.x, v3.z);
    }

    //二维点转为三维点 x->x y->z height->y
    public static Vector3 Vector2ToVector3_XZ(this Vector2 v2, float height)
    {
        return new Vector3(v2.x, height, v2.y);
    }

    //三维点转为二维点 x->x z->y
    public static Vector2 Vector3ToVector2_XY(this Vector3 v3)
    {
        return new Vector2(v3.x, v3.y);
    }

    //二维点转为三维点 x->x y->z height->y
    public static Vector3 Vector2ToVector3_XY(this Vector2 v2, float forwardLength)
    {
        return new Vector3(v2.x, v2.y, forwardLength);
    }

    //判定点在凸四边形的位置
    //-1  四边形内
    //0    边上
    //1     四边形外
    public static int IsPointInRect(Vector2 point, Rectangle rect)
    {
        Vector2 P = point;
        Vector2 A = rect.leftUp;
        Vector2 B = rect.rightUp;
        Vector2 C = rect.rightBottom;
        Vector2 D = rect.leftBottom;

        Vector2 AP = P - A;
        Vector2 BP = P - B;
        Vector2 CP = P - C;
        Vector2 DP = P - D;

        Vector2 AB = B - A;
        Vector2 BC = C - B;
        Vector2 CD = D - C;
        Vector2 DA = A - D;

        float angle1 = Vector2.SignedAngle(AP, AB);//逆时针取最近角度范围(-180f,180f]
        float angle2 = Vector2.SignedAngle(BP, BC);
        float angle3 = Vector2.SignedAngle(CP, CD);
        float angle4 = Vector2.SignedAngle(DP, DA);
        if (angle1 == 0f || angle2 == 0f || angle3 == 0f || angle4 == 0f)
        {
            return 0;
        }
        else if (angle1 == 180f || angle2 == 180f || angle3 == 180f || angle4 == 180f)
        {
            return 1;
        }
        else if ((angle1 > 0f && angle1 < 180f) && (angle2 > 0f && angle2 < 180f) && (angle3 > 0f && angle3 < 180f) && (angle4 > 0f && angle4 < 180f))
        {
            return -1;
        }
        else
            return 1;

    }



    //A大B小
    //-1 B在A内
    //0 AB相交
    //1 B在A外
    public static int GetTwoRectState(Rectangle rectA, Rectangle rectB)
    {
        List<Vector2> anglePoints = rectB.GetAngles();
        int length = anglePoints.Count;
        int count = 0;//点在A内的计数
        foreach (Vector2 v2 in anglePoints)
        {
            if (IsPointInRect(v2, rectA) == -1)
            {
                count++;
            }

        }
        if (count == length)
        {
            return -1;//B在A内
        }
        else if (count == 0)
        {
            anglePoints = rectA.GetAngles();
            //B没有点在A内
            foreach (Vector2 v in anglePoints)
            {
                if (IsPointInRect(v, rectB) == -1)
                    return 0;//相交
            }
            return 1;//B在A外
        }
        else
        {
            return 0;//B有若干点在A内，相交
        }

    }

    //算法：放羊入羊圈
    //羊圈范围以二维数组表示
    //羊的占位为数组元素
    //此方法为放入的羊腾出空间算法
    //数组内元素含义 0:未被占用 1:已被占用 -1:非法(边界)
    //返回值:合法的位置
    public static Vector2Int GetSpaceForSheepWantIn(int[,] sheepGroup, Vector2Int wantIn)
    {
        if (sheepGroup[wantIn.x, wantIn.y] == 0)
        {
           
            return wantIn;
        }

        int maxX = sheepGroup.GetUpperBound(0);
        int maxY = sheepGroup.GetUpperBound(1);

        int maxLevel = 0;
        maxLevel = (wantIn.x > maxLevel) ? wantIn.x : maxLevel;
        maxLevel = ((maxX - wantIn.x) > maxLevel) ? (maxX - wantIn.x) : maxLevel;
        maxLevel = (wantIn.y > maxLevel) ? wantIn.y : maxLevel;
        maxLevel = ((maxY - wantIn.y) > maxLevel) ? (maxY - wantIn.y) : maxLevel;

        for (int i = 1; i < maxLevel; i++)
        {
            int xMin = wantIn.x - i, xMax = wantIn.x + i, yMin = wantIn.y - i, yMax = wantIn.y + i;
            int k = 0;

            for (k = xMax; k >= xMin; k--)
            {
                if (k >= 0 && k <= maxX && yMin >= 0 && yMin <= maxY)
                    if (sheepGroup[k, yMin] == 0)
                    {
                        
                        return new Vector2Int(k, yMin);
                    }
            }

            for (k = yMin; k < yMax; k++)
            {
                if (k >= 0 && k <= maxY && xMin >= 0 && xMin <= maxX)
                    if (sheepGroup[xMin, k] == 0)
                    {
                        
                        return new Vector2Int(xMin, k);
                    }
            }

            for (k = yMax; k >= yMin; k--)
            {
                if (k >= 0 && k <= maxY && xMax >= 0 && xMax <= maxX)
                    if (sheepGroup[xMax, k] == 0)
                    {
                        
                        return new Vector2Int(xMax, k);
                    }
            }

            for (k = xMin; k < xMax; k++)
            {
                if (k >= 0 && k <= maxX && yMax >= 0 && yMax <= maxY)
                    if (sheepGroup[k, yMax] == 0)
                    {
                        
                        return new Vector2Int(k, yMax);
                    }
            }

        }
        
        return new Vector2Int(-1, -1);
    }

    /// <summary>
    /// 获取四边形内距离四边形外一点距离最近的点
    /// </summary>
    /// <param name="point">四边形外一点</param>
    /// <param name="rect">四边形</param>
    /// <param name="nearestPointInRect">结果</param>
    public static void GetNearestPointInRectFromPoint(Vector2 point,Rectangle rect,out Vector2 nearestPointInRect)
    {
        nearestPointInRect = Vector2.zero;
        if (IsPointInRect(point, rect) == -1)
        {
            nearestPointInRect = point;
        }
        else
        {
            if (IsPointInSegment(rect.leftUp, rect.rightUp, point) ||
                IsPointInSegment(rect.rightUp, rect.rightBottom, point) ||
                IsPointInSegment(rect.rightBottom, rect.leftBottom, point) ||
                IsPointInSegment(rect.leftBottom, rect.leftUp, point))
            {
                //点在平面边上
                nearestPointInRect = point;
            }
            else
            {
                //点在平面之外

                Vector2 pa = GetNearestPointInSegment(rect.leftUp, rect.rightUp, point);
                Vector2 pb = GetNearestPointInSegment(rect.rightUp, rect.rightBottom, point);
                Vector2 pc = GetNearestPointInSegment(rect.rightBottom, rect.leftBottom, point);
                Vector2 pd = GetNearestPointInSegment(rect.leftBottom, rect.leftUp, point);

                float a = Vector2.Distance(pa, point);
                float min = a;
                float b = Vector2.Distance(pb, point);
                min = min < b ? min : b;
                float c = Vector2.Distance(pc, point);
                min = min < c ? min : c;
                float d = Vector2.Distance(pd, point);
                min = min < d ? min : d;

                if (min == a)
                {
                    nearestPointInRect = pa;
                }
                else if (min == b)
                {
                    nearestPointInRect = pb;
                }
                else if (min == c)
                {
                    nearestPointInRect = pc;
                }
                else
                {
                    nearestPointInRect = pd;
                }
                
            }
        }
    }

    /// <summary>
    /// 点在直线上？
    /// </summary>
    /// <param name="p1">直线A点</param>
    /// <param name="p2">直线B点</param>
    /// <param name="o">要检测的点</param>
    /// <returns></returns>
    public static bool IsPointInBeeline(Vector2 p1,Vector2 p2,Vector2 o)
    {
        if ((p1.x == p2.x) && (p1.y == p2.y))
            throw new System.Exception("两点是同一点");
        else
        {
            //(x-x1)*(y2-y1) = (y-y1)*(x2-x1)[弃用，太精准，但是电脑会丢失很多精准度]
            //用斜率检测
            if (Mathf.Abs ((o.x - p1.x) * (p2.y - p1.y) - (o.y - p1.y) * (p2.x - p1.x)) < 0.1f)
                return true;
            else
                return false;
        }
    }

    /// <summary>
    /// 点在线段内？
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="o"></param>
    /// <returns></returns>
    public static bool IsPointInSegment(Vector2 p1, Vector2 p2, Vector2 o)
    {
        if (!IsPointInBeeline(p1, p2, o))//不在直线上
        {
            return false;
        }
        else
        {
            if (Mathf.Abs(Vector2.Distance(p1, o) + Vector2.Distance(p2, o) - Vector2.Distance(p1, p2)) < 0.1f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// 获取点在直线上垂足点
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="o"></param>
    /// <returns></returns>
    public static Vector2 GetNearestPointInBeeline(Vector2 p1, Vector2 p2, Vector2 o)
    {
        //A、B两点的一般直线方程式: 
        //Ax +By+C=0
        //A=y2-y1
        //B=x1-x2
        //C=x2*y1-x1*y2

        // 点到直线的距离公式:d = (Ax0+By0+C)/sqrt(A*A+B*B)
        // 垂足C(x,y)的计算公式
        // x = (B*B*x0 - A*B*y0- A*C)/(A*A+B*B)
        // y = (-A*B*x0 + A*A*y0- B*C)/(A*A+B*B)
        float A = p2.y - p1.y;
        float B = p1.x - p2.x;
        float C = p2.x * p1.y - p1.x * p2.y;

        float sqrt = A * A + B * B;

        float x = (B * B * o.x - A * B * o.y - A * C) / sqrt;
        float y = (A * A * o.y - A * B * o.x - B * C) / sqrt;
        
        return new Vector2(x, y);
    }

    /// <summary>
    /// 获取点到线段上最近的点
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="o"></param>
    /// <returns></returns>
    public static Vector2 GetNearestPointInSegment(Vector2 p1, Vector2 p2, Vector2 o)
    {
        Vector2 nearestPointInBeeline = GetNearestPointInBeeline(p1,p2,o);
        if (IsPointInSegment(p1, p2, nearestPointInBeeline))
        {
            return nearestPointInBeeline;
        }
        else
        {
            //交点在线段外
            if (Vector2.Distance(o, p1) < Vector2.Distance(o, p2))
            {
                return p1;
            }
            else
            {
                return p2;
            }
        }
    }

    /// <summary>
    /// 获得三个数
    /// </summary>
    /// <param name="min">最小</param>
    /// <param name="max">最大</param>
    /// <param name="mustHave">必须有的</param>
    /// <returns></returns>
    public static string GetThreeNoSameNumber(int min,int max,int mustHave)
    {
        List<int> list = new List<int>();
        list.Add(mustHave);
        string result = "";
        while (list.Count<3)
        {
            int i = Random.Range(min, max + 1);
            if (!list.Contains(i))
                list.Add(i);
        }

        int[] array = list.ToArray();
        Shuffle(array, 3);
        for (int i=0;i<array.Length;i++)
        {
            if (i < array.Length - 1)
                result += array[i] + ",";
            else
                result += array[i];
        }
       // Debug.Log(result);
        return result;
    }

    public static void Shuffle(int[] array, int num)
    {
        int a, b;
        for (int i = 0; i < num; i++)
        {
            a = Random.Range(0, array.Length);
            b = Random.Range(0, array.Length);
            int tmp = array[a];
            array[a] = array[b];
            array[b] = tmp;
        }
    }

    /// <summary>
    /// 在数组中得到一个除了except之外的数
    /// </summary>
    /// <param name="list"></param>
    /// <param name="except"></param>
    /// <returns></returns>
    public static int GetNumbersExcept(List<int>list , int except)
    {
        if (list == null || list.Count == 0) return 0;
        int r;
        while (true)
        {
            r = UnityEngine.Random.Range(0, list.Count);
            if (list[r] != except)
                return list[r];
        }
    }
}


[System.Serializable]
public class Rectangle
{
    //左上
    public Vector2 leftUp;
    //右上
    public Vector2 rightUp;
    //右下
    public Vector2 rightBottom;
    //左下
    public Vector2 leftBottom;

    public Rectangle(Vector2 lu, Vector2 ru, Vector2 rb, Vector2 lb)
    {
        leftUp = lu;
        rightUp = ru;
        leftBottom = lb;
        rightBottom = rb;
    }

    public Rectangle(Vector2 ct, float lg, float wd)
    {
        float halfLength = lg / 2;
        float halfWidth = wd / 2;
        leftUp = ct + new Vector2(-halfLength, halfWidth);
        rightUp = ct + new Vector2(halfLength, halfWidth);
        leftBottom = ct + new Vector2(-halfLength, -halfWidth);
        rightBottom = ct + new Vector2(halfLength, -halfWidth);
    }

    public Rectangle(Vector2[] points)
    {
        leftUp = points[0];
        rightUp = points[1];
        rightBottom = points[2];
        leftBottom = points[3];
    }

    public Rectangle(List<Vector2> points)
    {
        leftUp = points[0];
        rightUp = points[1];
        rightBottom = points[2];
        leftBottom = points[3];
    }

    public void Reset(Vector2 lu, Vector2 ru, Vector2 rb, Vector2 lb)
    {
        leftUp = lu;
        rightUp = ru;
        leftBottom = lb;
        rightBottom = rb;
    }

    public void DrawRect(Color color, float high)
    {
        Debug.DrawLine(leftUp.Vector2ToVector3_XZ(high), rightUp.Vector2ToVector3_XZ(high), color);
        Debug.DrawLine(rightUp.Vector2ToVector3_XZ(high), rightBottom.Vector2ToVector3_XZ(high), color);
        Debug.DrawLine(rightBottom.Vector2ToVector3_XZ(high), leftBottom.Vector2ToVector3_XZ(high), color);
        Debug.DrawLine(leftBottom.Vector2ToVector3_XZ(high), leftUp.Vector2ToVector3_XZ(high), color);
    }

    public List<Vector2> GetAngles()
    {
        List<Vector2> result = new List<Vector2>();
        result.Add(leftBottom);
        result.Add(leftUp);
        result.Add(rightBottom);
        result.Add(rightUp);
        return result;
    }

}

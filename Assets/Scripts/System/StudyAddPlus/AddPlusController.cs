﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 十以内加法总控制
/// </summary>
public class AddPlusController : Singleton<AddPlusController>
{
    public static int lastSum = 1;

    //入/出口状态
    public string enterState = GameStateName.MAIN_STATE;
    
    //视图层
    private AddPlusView m_View;
    //数据层
    private TenPlusModule m_Data;
    private GameObject serialisedData;

    private HerdSheep herdSheep;
    private CutSheepFeather cutFeather;
    private TransportSheepFeather transportFeather;

    /// <summary>
    /// 当前分辨率
    /// </summary>
    public Resolution resolution
    {
        get { return Screen.currentResolution; }
    }

    public float HighRes = 2.165f;//2436 / 1125约等于2.1653取2.165 iphonex
    public float LowRes = 1.34f;//2048 / 1536 约等于1.33  取1.34f ipad

    public override void Init()
    {
        base.Init();

        //视图层初始化
        if (m_View == null)
            m_View = new AddPlusView();
        
        RegisterEvent();
    }

    public override void UnInit()
    {
        base.UnInit();
        RemoveEvent();
    }
    
    //开始游戏
    public void StartGame()
    {
        //数据层初始化
        if (serialisedData == null)
        {
            Resource re = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.ProcedureData, typeof(GameObject), enResourceType.Prefab, false);
            serialisedData = GameObject.Instantiate(re.m_content as GameObject);
        }
        m_Data = serialisedData.GetComponent<TenPlusModule>();
        m_Data.GetReady();

        //根据数据显示页面
        OpenView();
        
        //根据数据调整游戏场景内的布置
        CreateRealGame();
    }

    //创建具体游戏
    private void CreateRealGame()
    {
        int i = m_Data.runtimeData.currentGameIndex;
        GameRound round = m_Data.gameplayline.games[i].gameround[m_Data.runtimeData.currentRoundIndex];

        //难度对应的结果
        int sum = m_Data.GetRandomSumByHardlevel(round.hardlevel,lastSum);
        lastSum = sum;
        if (i == 0)
        {
            if (cutFeather != null) cutFeather.HideScene();
            if (transportFeather != null) transportFeather.HideScene();

            if (herdSheep == null) herdSheep = new HerdSheep(round, sum, m_Data.runtimeData);
            else herdSheep.Reset(round, sum, m_Data.runtimeData);


        }
        else if (i == 1)
        {
            //转场

            if (herdSheep != null) herdSheep.HideScene();
            if (transportFeather != null) transportFeather.HideScene();

            if (cutFeather == null) cutFeather = new CutSheepFeather(round, sum, m_Data.runtimeData);
            else cutFeather.Reset(round, sum, m_Data.runtimeData);

        }
        else
        {
            //转场

            if (herdSheep != null) herdSheep.HideScene();
            if (cutFeather != null) cutFeather.HideScene();

            if (transportFeather == null) transportFeather = new TransportSheepFeather(round, sum, m_Data.runtimeData);
            else transportFeather.Reset(round, sum, m_Data.runtimeData);
        }
    }
    

    //退出游戏
    public void ExitGame()
    {

    }

    //退出State
    public void Exit()
    {
        CloseView();
        ClearData();
    }
    private void OpenView()
    {
        m_View.OpenForm();
    }

    private void CloseView()
    {
        m_View.CloseForm();
    }

    private void ClearData()
    {
        m_Data.SetDataToOrgin();
    }


    private void RegisterEvent()
    {
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_Btn_Return,OnClickReturn);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_GameProcedure_Change,GameProcedureChange);
    }

    private void RemoveEvent()
    {
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_Btn_Return, OnClickReturn);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_GameProcedure_Change, GameProcedureChange);
    }

    private void OnClickReturn(HUIEvent evt)
    {
        DoClean();
        GameStateManager.Instance.GotoState(enterState);
    }

    private void DoClean()
    {
        if (herdSheep != null) herdSheep.DoClean();
        if (cutFeather != null) cutFeather.DoClean();
        if (transportFeather != null) transportFeather.DoClean();
        if (m_View != null) m_View.DoClean();
    }

    //游戏进程改变了,进程+1
    private void GameProcedureChange(HUIEvent evt)
    {
        int currentProcedureIndex = m_Data.runtimeData.currentLearnProcedureIndex;
        int currentGameIndex = m_Data.runtimeData.currentGameIndex;
        int currentRoundIndex = m_Data.runtimeData.currentRoundIndex;
        string dynamicData = "";
        int currentRoundCount = m_Data.gameplayline.games[m_Data.runtimeData.currentGameIndex].gameround.Count;

        if (currentGameIndex == 2 && currentRoundIndex == 3)
        {
            HUIEventManager.Instance.DispatchUIEvent(enUIEventID.AddPlus_Btn_Return);
        }
        else
        {
            if (currentRoundIndex == currentRoundCount - 1)
            {
                //该类型游戏结束.开始下一个游戏
                m_Data.runtimeData.currentRoundIndex = 0;
                if (m_Data.runtimeData.currentGameIndex == m_Data.gameplayline.games.Count - 1)
                {
                    //最后一个类型的游戏,游戏过程+1,游戏类型，游戏回合重置
                    dynamicData = string.Format("{0},{1},{2}", currentProcedureIndex, 0, 0);
                }
                else
                {
                    //游戏索引+1,回合索引重置为0
                    dynamicData = string.Format("{0},{1},{2}", currentProcedureIndex - 1, currentGameIndex + 1, 0);
                }

            }
            else
            {
                //该类型游戏未结束，回合索引+1
                dynamicData = string.Format("{0},{1},{2}", currentProcedureIndex - 1, currentGameIndex, currentRoundIndex + 1);
            }
            m_Data.Save(dynamicData);

            //reStart.
            StartGame();
        }
    }

    public static float GetAnimationClipLength(RuntimeAnimatorController runtimeAnimatorCtr,string clipname)
    {
        for (int i=0;i<runtimeAnimatorCtr.animationClips.Length;i++)
        {
            if (clipname == runtimeAnimatorCtr.animationClips[i].name)
                return runtimeAnimatorCtr.animationClips[i].length;
        }
        return 0f;
    }

    public static float GetAudioClipLength(string audioClipname)
    {
        AudioClip audio = AudioManager.Instance.GetAudioClip(audioClipname);
        if (audio == null) return 1f;
        return audio.length;
    }
}

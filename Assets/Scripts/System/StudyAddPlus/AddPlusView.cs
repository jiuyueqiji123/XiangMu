﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// 十以内加法UI面板
/// </summary>
public class AddPlusView
{
    private HUIFormScript m_Form;

    private SpriteAtlas atlas;
    private SpriteAtlas commonAtlas;

    //返回键
    private GameObject ReturnBtn;
    //勋章动画开始点
    private GameObject honorStartPoint;
    //勋章父节点
    private GameObject honorParent;
    //羊进羊圈之后出现的 数字
    private GameObject sheepinFoldNumbers;
    //算式
    private GameObject equation;
    //数字的终点
    private GameObject sheepInfoldNumbersEndPosition;
    //数字的起始点
    private GameObject sheepInfoldNumbersStartPosition;
    //算式起点
    private GameObject equationStartPos;
    //算式终点
    private GameObject equationEndPos;
    //绵羊叫UI模板
    private GameObject sheepShout;
    //手势指引
    private GameObject gestureObj;
    //羊毛点
    private GameObject featherObj;
    //image
    private GameObject imageObj;

    public void OpenForm()
    {
        //HUIManager.Instance.OpenForm(TenPlusModuleAssetPath.m_ViewPath,true)貌似取得不是缓存值
        if (m_Form != null && m_Form.gameObject != null)
        {
            RemoveRegister();
            GameObject.DestroyImmediate(m_Form.gameObject);
        }
        m_Form = HUIManager.Instance.OpenForm(TenPlusModuleAssetPath.m_ViewPath,false);
        this.InitForm();
    }


    public void CloseForm()
    {
        if (!m_Form.IsClosed())
            HUIManager.Instance.CloseForm(m_Form);
    }

    public void InitForm()
    {
        if (m_Form == null)
            return;

        atlas = Resources.Load<SpriteAtlas>(TenPlusModuleAssetPath.atlasPath);
        commonAtlas = Resources.Load<SpriteAtlas>(TenPlusModuleAssetPath.common_atlasPath);

        ReturnBtn = m_Form.GetWidget((int)enAddPlusViewWidget.Button_Return);
        honorStartPoint = m_Form.GetWidget((int)enAddPlusViewWidget.Xunzhang_Start);
        honorParent = m_Form.GetWidget((int)enAddPlusViewWidget.Xunzhang_parent);
        sheepinFoldNumbers = m_Form.GetWidget((int)enAddPlusViewWidget.SheepInFoldNumbers);
        equation = m_Form.GetWidget((int)enAddPlusViewWidget.Equation);
        sheepInfoldNumbersEndPosition = m_Form.GetWidget((int)enAddPlusViewWidget.SheepInfoldNumbersEndPosition);
        sheepInfoldNumbersStartPosition = m_Form.GetWidget((int)enAddPlusViewWidget.SheepInfoldNumbersStartPosition);
        equationStartPos = m_Form.GetWidget((int)enAddPlusViewWidget.EquationStart);
        equationEndPos = m_Form.GetWidget((int)enAddPlusViewWidget.EquationEnd);
        sheepShout = m_Form.GetWidget((int)enAddPlusViewWidget.SheepShout);
        gestureObj = m_Form.GetWidget((int)enAddPlusViewWidget.GestureGuide);
        imageObj = m_Form.GetWidget((int)enAddPlusViewWidget.imageObj);
        featherObj = equation.transform.Find("Feather").gameObject;

        Register();

        ResetToOrigin();

        HUIUtility.SetUIMiniEvent(ReturnBtn, enUIEventType.Click, enUIEventID.AddPlus_Btn_Return);

    }

    void Register()
    {
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_Image_XunZhangAdd,SetHonor);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_Numbers_ShowUp, NumberShowUp);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_Numbers_Hide, NumberHide);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_StartMove_Xunzhang, StartPlayHonorAnim);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_Load_Equation, LoadEquation);
        //HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_SheepShout, SheepShout);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_HideEquation, HideEquation);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_WrongNumber_Click, ClickWrongNumber);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_GestureGuide_Show, GestureGuideShow);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_GestureGuide_Hide, GestureGuideHide);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_FeatherInit, FeatherInit);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_FeatherAdd, FeatherAdd);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_FeatherRightShow, FeatherRightShow);
        HUIEventManager.Instance.AddUIEventListener(enUIEventID.AddPlus_FeatherDestroy, FeatherDestroy);
    }

    void RemoveRegister()
    {
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_Image_XunZhangAdd, SetHonor);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_Numbers_ShowUp, NumberShowUp);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_Numbers_Hide, NumberHide);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_StartMove_Xunzhang, StartPlayHonorAnim);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_Load_Equation, LoadEquation);
        //HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_SheepShout, SheepShout);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_HideEquation, HideEquation);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_WrongNumber_Click, ClickWrongNumber);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_GestureGuide_Show, GestureGuideShow);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_GestureGuide_Hide, GestureGuideHide);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_FeatherInit, FeatherInit);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_FeatherAdd, FeatherAdd);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_FeatherRightShow, FeatherRightShow);
        HUIEventManager.Instance.RemoveUIEventListener(enUIEventID.AddPlus_FeatherDestroy, FeatherDestroy);
    }

    public void DoClean()
    {
        RemoveRegister();
    }

    //手势指引粗线
    void GestureGuideShow(HUIEvent hUIEvent)
    {
        string[] axisArray = hUIEvent.m_eventParams.argString.Split(',');
        RectTransform rect = m_Form.transform.GetComponent<RectTransform>();
        Vector2 screenPoint = new Vector2(float.Parse(axisArray[0]), float.Parse(axisArray[1]));
        Camera cam = m_Form.transform.parent.Find("Camera_Form").GetComponent<Camera>();
        Vector2 localPoint;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rect, screenPoint, cam, out localPoint))
        {
            gestureObj.SetActive(true);
            gestureObj.transform.SetParent(m_Form.transform, false);
            gestureObj.GetComponent<RectTransform>().localPosition = localPoint.Vector2ToVector3_XY(0f);
            gestureObj.transform.DOMove(Vector3.zero, 1.5f).SetLoops(-1, LoopType.Restart).SetEase(Ease.Flash);
        }
        else
        {
            Debug.LogError("点不对");
        }
    }

    void FeatherInit(HUIEvent hUIEvent)
    {
        int factor1 = hUIEvent.m_eventParams.argInt;
        int factor2 = hUIEvent.m_eventParams.argInt2;
        int sheepTypes = hUIEvent.m_eventParams.togleIsOn?1:2;
        
        List<int> list = new List<int> { factor1, factor2, factor1 + factor2 };
        GameObject row1;
        GameObject row2;
        GameObject go;
        int j = 0;
        for (int i = 0; i < list.Count; i++)
        {
            row1 = featherObj.transform.GetChild(i).GetChild(0).gameObject;
            row2 = featherObj.transform.GetChild(i).GetChild(1).gameObject;
            row1.SetActive(true);
            row2.SetActive(list[i] > 5);
            if (list[i] > 5)
            {
                int k;
                for (k = 0; k < list[i] - 5; k++)
                {
                    go = GameObject.Instantiate(imageObj);
                    go.transform.SetParent(row1.transform, false);
                    FeatherImageReset(go,i,k,sheepTypes,factor1);
                }

                for (j = 0; j < 5; j++)
                {
                    go = GameObject.Instantiate(imageObj);
                    go.transform.SetParent(row2.transform, false);
                    FeatherImageReset(go,i,j+k,sheepTypes,factor1);
                }
            }
            else
                for (j = 0; j < list[i]; j++)
                {
                    go = GameObject.Instantiate(imageObj);
                    go.transform.SetParent(row1.transform, false);
                    FeatherImageReset(go, i, j, sheepTypes, factor1);
                }
        }



    }

    /// <summary>
    /// 羊毛图片展示预设置
    /// </summary>
    /// <param name="go"></param>
    /// <param name="index">第几个因子</param>
    /// <param name="selfIndex">在自己的列表中索引</param>
    /// <param name="sheepTypes">羊种类</param>
    /// <param name="factor1">算式因子1的长度</param>
    void FeatherImageReset(GameObject go,int index,int selfIndex,int sheepTypes,int factor1)
    {
        go.name = "0";
        go.transform.localScale = Vector3.one;
        go.GetComponent<RectTransform>().sizeDelta = new Vector2(60f, 60f);
        Vector3 pos = go.GetComponent<RectTransform>().anchoredPosition3D;
        go.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(pos.x, pos.y, 0f);
        go.GetComponent<Image>().enabled = true;
        Sprite frame1 = atlas.GetSprite(TenPlusModuleAssetPath.sprite_feather_white_frame);
        Sprite frame2 = atlas.GetSprite(TenPlusModuleAssetPath.sprite_feather_gray_frame);
        if (index == 0)
        {
            go.GetComponent<Image>().sprite = frame1;
            go.name = "1";
            Transform child = go.transform.GetChild(0);
            child.gameObject.GetComponent<Image>().enabled = true;
            child.gameObject.GetComponent<Image>().sprite = atlas.GetSprite(TenPlusModuleAssetPath.sprite_feather_white);
        }
        else if (index == 1)
        {
            if (sheepTypes == 1)
                go.GetComponent<Image>().sprite = frame1;
            else
                go.GetComponent<Image>().sprite = frame2;
        }
        else
        {
            if (sheepTypes == 1)
                go.GetComponent<Image>().sprite = frame1;
            else
            {
                if (selfIndex < factor1)
                    go.GetComponent<Image>().sprite = frame1;
                else
                    go.GetComponent<Image>().sprite = frame2;
            }
        }
    }

    void FeatherAdd(HUIEvent hUIEvent)
    {
        int factor = hUIEvent.m_eventParams.argInt;//位置
        int color = hUIEvent.m_eventParams.argInt2;//颜色

        string spriteStr = color == 1 ? TenPlusModuleAssetPath.sprite_feather_white : TenPlusModuleAssetPath.sprite_feather_gray;
        
        Sprite sp = atlas.GetSprite(spriteStr);
        

        Transform parent = featherObj.transform.GetChild(factor);
        GameObject child;
        for (int i=0;i<parent.childCount;i++)
        {
            Transform layout = parent.GetChild(i);
            for (int j = 0; j < layout.childCount; j++)
            {
                GameObject image = layout.GetChild(j).gameObject;
                child = image.transform.GetChild(0).gameObject;
                if (!child.GetComponent<Image>().enabled&&image.name =="0")
                {
                    image.GetComponent<Image>().enabled = true;
                    image.name = "1";
                    string[] axisArray = hUIEvent.m_eventParams.argString.Split(',');
                    RectTransform rect = m_Form.transform.GetComponent<RectTransform>();
                    Vector2 screenPoint = new Vector2(float.Parse(axisArray[0]), float.Parse(axisArray[1]));
                    Camera cam = m_Form.transform.parent.Find("Camera_Form").GetComponent<Camera>();
                    Vector2 localPoint;
                    if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rect, screenPoint, cam, out localPoint))
                    {
                        GameObject sheepFeather = GameObject.Instantiate(imageObj);
                        sheepFeather.SetActive(true);
                        sheepFeather.GetComponent<Image>().enabled = true;
                        sheepFeather.GetComponent<Image>().sprite = sp;
                        sheepFeather.transform.SetParent(m_Form.transform, false);
                        sheepFeather.GetComponent<RectTransform>().localPosition = localPoint.Vector2ToVector3_XY(0f);
                        sheepFeather.transform.DOMove(child.transform.parent.position, TenPlusAnimLength.miemieUIExistTime).SetEase(Ease.Flash).OnComplete(() =>
                        {
                            GameObject.DestroyImmediate(sheepFeather);

                            child.GetComponent<Image>().sprite = sp;
                            child.GetComponent<Image>().enabled = true;
                        });
                    }
                    else
                    {
                        Debug.LogError("点不对");
                    }
                    return;
                }
            }
        }
        
    }

    void FeatherRightShow(HUIEvent hUIEvent)
    {
        int factor1 = featherObj.transform.GetChild(0).GetChild(0).childCount
            + featherObj.transform.GetChild(0).GetChild(1).childCount;
        //int factor2 = featherObj.transform.GetChild(1).GetChild(0).childCount
        //    + featherObj.transform.GetChild(1).GetChild(1).childCount;
        int colorTypes = hUIEvent.m_eventParams.argInt;//羊毛种类
        Sprite sp_white = atlas.GetSprite(TenPlusModuleAssetPath.sprite_feather_white);
        Sprite sp_gray = atlas.GetSprite(TenPlusModuleAssetPath.sprite_feather_gray);
        List<GameObject> resultList = new List<GameObject>();
        Transform parent = featherObj.transform.GetChild(2);
        int i;
        int j;
        for (i = 0; i < parent.childCount; i++)
        {
            Transform layout = parent.GetChild(i);
            for (j = 0; j < layout.childCount; j++)
            {
                resultList.Add(layout.GetChild(j).gameObject);
            }
        }

        for (i = 0; i < resultList.Count; i++)
        {
            Transform ele;
            ele = resultList[i].transform.GetChild(0);
            ele.GetComponent<Image>().enabled = true;
            if (i < factor1)
            {
                ele.GetComponent<Image>().sprite = sp_white;
            }
            else
            {

                if (colorTypes == 1)
                    ele.GetComponent<Image>().sprite = sp_white;
                else
                    ele.GetComponent<Image>().sprite = sp_gray;
            }
        }
    }

    void FeatherDestroy(HUIEvent hUIEvent)
    {
        for (int i = 0; i < featherObj.transform.childCount; i++)
        {
            Transform factor = featherObj.transform.GetChild(i);
            for (int j = 0; j < factor.childCount; j++)
            {
                Transform layout = factor.GetChild(j);
                for (int k = layout.childCount - 1; k >= 0; k--)
                {
                    GameObject.Destroy(layout.GetChild(k).gameObject);
                }
            }
        }
    }


    //手势指引消失
    void GestureGuideHide(HUIEvent hUIEvent)
    {
        gestureObj.SetActive(false);
    }

    /// <summary>
    /// 还原到原点
    /// </summary>
    public void ResetToOrigin()
    {
        sheepinFoldNumbers.SetActive(false);
        sheepinFoldNumbers.transform.position = sheepInfoldNumbersStartPosition.transform.position;
        equation.SetActive(false);

        for (int i=0;i< honorParent.transform.childCount;i++)
        {
            honorParent.transform.GetChild(i).GetComponent<Image>().sprite = commonAtlas.GetSprite("xunzhang02");
        }


        FeatherDestroy(null);
    }

    /// <summary>
    /// 显示几个勋章
    /// </summary>
    /// <param name="showNum"></param>
    public void SetHonor(HUIEvent huievent)
    {
        ShowXunZhang(huievent.m_eventParams.argInt);
    }

    void ShowXunZhang(int num)
    {
        for (int i = 0; i < honorParent.transform.childCount; i++)
        {
            if (i < num)
                honorParent.transform.GetChild(i).GetComponent<Image>().sprite = commonAtlas.GetSprite("xunzhang");
            else
                honorParent.transform.GetChild(i).GetComponent<Image>().sprite = commonAtlas.GetSprite("xunzhang02");
        }
        
    }

    /// <summary>
    /// 播放勋章移动动画
    /// </summary>
    /// <param name="callback">动画结束回调</param>
    public void StartPlayHonorAnim(HUIEvent hUIEvent)
    {
        GameObject go = GameObject.Instantiate(honorStartPoint);
        go.SetActive(true);
        go.transform.SetParent(honorStartPoint.transform.parent, false);
        go.transform.position = honorStartPoint.transform.position;
        Resource r = ResourceManager.Instance.GetResource(TenPlusModuleAssetPath.xunzhangEffect,typeof(GameObject),enResourceType.Prefab);
        GameObject effect = GameObject.Instantiate(r.m_content) as GameObject;
        effect.SetLayer(5,true);//设置特效layer为UI层
        effect.transform.SetParent(go.transform,false);

        Vector3 endPos = honorParent.transform.GetChild(hUIEvent.m_eventParams.argInt - 1).position;
        float endScale = honorParent.transform.GetChild(hUIEvent.m_eventParams.argInt - 1).localScale.x;

        go.transform.DOScale(endScale, TenPlusAnimLength.xunzhangEnterTime );
        AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_603030102);
        go.transform.DOMove(endPos, TenPlusAnimLength.xunzhangEnterTime ).SetEase(Ease.Flash).OnComplete(() =>
        {
            //勋章个数+1
            ShowXunZhang(hUIEvent.m_eventParams.argInt);

            //游戏结束
            HUIEventManager.Instance.DispatchUIEvent(enUIEventID.AddPlus_RoundOver);
            GameObject.DestroyImmediate(go);
        });
    }

    float scale = 0.8f;
    float offset = 10f;
    
    // 数字出现
    public void NumberShowUp(HUIEvent huievent)
    {
        string[] numberStr = huievent.m_eventParams.argString.Split(',');
       
        for (int i=0;i<sheepinFoldNumbers.transform.childCount;i++)
        {
            Transform tran = sheepinFoldNumbers.transform.GetChild(i);
            tran.GetComponent<RectTransform>().localRotation = Quaternion.identity;
            tran.GetComponent<Image>().sprite = atlas.GetSprite(TenPlusModuleAssetPath.numbers_normalBackground);
            Transform tran1 = sheepinFoldNumbers.transform.GetChild(i).GetChild(0);
            Transform tran2 = sheepinFoldNumbers.transform.GetChild(i).GetChild(1);
            int num = int.Parse(numberStr[i]);
            RectTransform rt;
            Sprite sp;
            if (num < 10)
            {
                rt = tran1.GetComponent<RectTransform>();
                sp = atlas.GetSprite(TenPlusModuleAssetPath.numbers[int.Parse(numberStr[i])]);
                tran1.GetComponent<Image>().sprite = atlas.GetSprite(TenPlusModuleAssetPath.numbers[int.Parse(numberStr[i])]);
                ResetRectTransform(rt, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), Vector2.zero, new Vector2(sp.rect.width, sp.rect.height) * scale);
                
                tran1.SetActive(true);
                tran2.SetActive(false);
            }
            else
            {
                int units = num/1%10;
                int tens = num/10%10;
                rt = tran1.GetComponent<RectTransform>();
                sp = atlas.GetSprite(TenPlusModuleAssetPath.numbers[tens]);//十位
                tran1.GetComponent<Image>().sprite = sp;
                ResetRectTransform(rt, new Vector2(0, 0.5f), new Vector2(0, 0.5f), new Vector2(0, 0.5f), new Vector2(offset, 0f), new Vector2(sp.rect.width, sp.rect.height) * scale);

                rt = tran2.GetComponent<RectTransform>();
                sp = atlas.GetSprite(TenPlusModuleAssetPath.numbers[units]);//个位
                tran2.GetComponent<Image>().sprite = sp;
                ResetRectTransform(rt, new Vector2(1f, 0.5f), new Vector2(1f, 0.5f), new Vector2(1f, 0.5f), new Vector2(-offset, 0f), new Vector2(sp.rect.width, sp.rect.height) * scale);

                tran1.SetActive(true);
                tran2.SetActive(true);
            }
            
            HUIUtility.SetUIMiniEvent(tran.gameObject, enUIEventType.Click, enUIEventID.AddPlus_NumbersBtn_Click, new stUIEventParams { argInt = int.Parse(numberStr[i]),argInt2 = i  });
            
        }
        sheepinFoldNumbers.SetActive(true);
        sheepinFoldNumbers.transform.position = sheepInfoldNumbersStartPosition.transform.position;

        Sequence seq = DOTween.Sequence();
        for (int i = 0; i < sheepinFoldNumbers.transform.childCount; i++)
        {
            Transform tran = sheepinFoldNumbers.transform.GetChild(i);
            Tweener tweener = tran.DOMove(sheepInfoldNumbersEndPosition.transform.GetChild(i).position, TenPlusAnimLength.sheepInfoldNumberEnterTime).SetEase(Ease.Flash);
            if (i == 0)
                seq.Append(tweener);
            else
                seq.Insert(i * TenPlusAnimLength.sheepInfoldNumberEnterTime / 2, tweener);
        }
    }

    public void ClickWrongNumber(HUIEvent hUIEvent)
    {
        for (int i = 0; i < sheepinFoldNumbers.transform.childCount; i++)
        {
            if (hUIEvent.m_eventParams.argInt == i)
            {

                Transform tran = sheepinFoldNumbers.transform.GetChild(i);
                if (tran.localEulerAngles.y != 0f)
                    return;
                else
                {
                    //AudioManager.Instance.PlaySound(TenPlusModuleAssetPath.audio_403030104);
                    tran.DOShakePosition(TenPlusAnimLength.numberShakeTime, new Vector3(10f,-10f,10f)).SetEase(Ease.Flash);
                    tran.DORotate(new Vector3(0, -90f, 0), TenPlusAnimLength.cardTurn_90_AngleTime).SetEase(Ease.Flash).OnComplete(()=> 
                    {
                        tran.GetComponent<Image>().sprite = atlas.GetSprite(TenPlusModuleAssetPath.numbers_overBackground);
                        for (int j = 0; j < tran.childCount; j++)
                            tran.GetChild(j).gameObject.SetActive(false);
                        tran.DORotate(new Vector3(0, -180f, 0f), TenPlusAnimLength.cardTurn_90_AngleTime).SetEase(Ease.Flash);
                    });
                }
            }
        }
    }

    public void NumberHide(HUIEvent huievent)
    {
        //Tweener tweener = sheepinFoldNumbers.transform.DOMove(sheepInfoldNumbersStartPosition.transform.position, TenPlusAnimLength.numbersExitTime).SetEase(Ease.Linear).OnComplete(() =>
        //{
        //    sheepinFoldNumbers.SetActive(false);
        //
        //    HUIEventManager.Instance.DispatchUIEvent(enUIEventID.AddPlus_Numbes_HideOver);
        //});

        DOTween.ClearCachedTweens();
        Sequence seq = DOTween.Sequence();
        for (int i = 0; i < sheepinFoldNumbers.transform.childCount; i++)
        {
            Transform tran = sheepinFoldNumbers.transform.GetChild(i);
            Tweener tweener = tran.DOMove(sheepInfoldNumbersStartPosition.transform.GetChild(i).position, TenPlusAnimLength.sheepInfoldNumberEnterTime).SetEase(Ease.Flash);
            if (i == 0)
                seq.Append(tweener);
            else
                seq.Insert(i * TenPlusAnimLength.sheepInfoldNumberEnterTime / 2, tweener);
            if (i == sheepinFoldNumbers.transform.childCount - 1)
            {
                seq.AppendInterval(TenPlusAnimLength.sheepInfoldNumberEnterTime);
                seq.AppendCallback(() =>
                {
                    HUIEventManager.Instance.DispatchUIEvent(enUIEventID.AddPlus_Numbes_HideOver);
                    sheepinFoldNumbers.SetActive(false);
                });
            }
        }
    }

    //加载算式
    public void LoadEquation(HUIEvent huievent)
    {
        string[] numberStr = huievent.m_eventParams.argString.Split(',');
        equation.SetActive(true);
        for (int i=0;i<numberStr.Length;i++)
        {
            int num = int.Parse(numberStr[i]);
            int units = num / 1 % 10;
            int tens = num / 10 % 10;

            Transform tran = equation.transform.GetChild(2 * i);
            RectTransform rectUnits;
            RectTransform rectTens;
            Sprite spUnits;
            Sprite spTens;
            if (tens == 0)
            {
                //个位数
                tran.GetChild(0).gameObject.SetActive(false);
                tran.GetChild(1).gameObject.SetActive(true);
                rectUnits = tran.GetChild(1).GetComponent<RectTransform>();
                spUnits = atlas.GetSprite(TenPlusModuleAssetPath.numbers[units]);
                ResetRectTransform(rectUnits, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), Vector2.zero, new Vector2(spUnits.rect.width, spUnits.rect.height)*scale);
                rectUnits.GetComponent<Image>().sprite = spUnits;
            }
            else
            {
                //两位数
                tran.GetChild(0).gameObject.SetActive(true);
                tran.GetChild(1).gameObject.SetActive(true);
                rectTens = tran.GetChild(0).GetComponent<RectTransform>();
                rectUnits = tran.GetChild(1).GetComponent<RectTransform>();
                spTens = atlas.GetSprite(TenPlusModuleAssetPath.numbers[tens]);
                spUnits = atlas.GetSprite(TenPlusModuleAssetPath.numbers[units]);
                ResetRectTransform(rectTens, new Vector2(0f, 0.5f), new Vector2(0f, 0.5f), new Vector2(0f, 0.5f), new Vector2(offset, 0f), new Vector2(spTens.rect.width, spTens.rect.height)*scale);
                ResetRectTransform(rectUnits, new Vector2(1f, 0.5f), new Vector2(1f, 0.5f), new Vector2(1f, 0.5f), new Vector2(-offset, 0f), new Vector2(spUnits.rect.width, spUnits.rect.height)*scale);
                rectTens.GetComponent<Image>().sprite = spTens;
                rectUnits.GetComponent<Image>().sprite = spUnits;
            }
            
        }
        
        Camera cam = m_Form.transform.parent.Find("Camera_Form").GetComponent<Camera>();
        Vector2 screenPoint = cam.WorldToScreenPoint(equation.transform.position);
        HUIEvent huie = new HUIEvent();
        huie.m_eventID = enUIEventID.AddPlus_ConfigFeatherPoints;
        huie.m_eventParams.argFloat = screenPoint.x;
        huie.m_eventParams.argFloat2 = screenPoint.y+ equation.GetComponent<RectTransform>().sizeDelta.y;
        huie.m_eventParams.argInt = (int)equation.GetComponent<RectTransform>().sizeDelta.x;
        huie.m_eventParams.argInt2= (int)equation.GetComponent<RectTransform>().sizeDelta.y;
        //Debug.Log(string.Format("{0}   {1},{2}", screenPoint, huie.m_eventParams.argInt, huie.m_eventParams.argInt2));
        HUIEventManager.Instance.DispatchUIEvent(huie);
        equation.transform.DOMove(equationEndPos.transform.position, TenPlusAnimLength.equationEntertime);
    }

    //隐藏算式
    public void HideEquation(HUIEvent huievent)
    {
        equation.transform.DOMove(equationStartPos.transform.position, TenPlusAnimLength.equationExittime);
    }

    //绵羊咩咩叫
    public void SheepShout(HUIEvent hUIEvent)
    {
        string[] axisArray = hUIEvent.m_eventParams.argString.Split(',');
        RectTransform rect = m_Form.transform.GetComponent<RectTransform>();
        Vector2 screenPoint = new Vector2(float.Parse(axisArray[0]), float.Parse(axisArray[1]));
        Camera cam = m_Form.transform.parent.Find("Camera_Form").GetComponent<Camera>();
        Vector2 localPoint;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rect,screenPoint,cam,out localPoint))
        {
            GameObject miemie = GameObject.Instantiate(sheepShout);
            miemie.SetActive(true);
            miemie.transform.SetParent(m_Form.transform, false);
            miemie.GetComponent<RectTransform>().localPosition = localPoint.Vector2ToVector3_XY(0f);
            miemie.transform.DOScale(0.8f, TenPlusAnimLength.miemieUIExistTime).SetEase(Ease.Flash).OnComplete(() =>
            {
                GameObject.DestroyImmediate(miemie);
            });
            if (hUIEvent.m_eventParams.argString2 != "")
            {
                string[] axisArray2 = hUIEvent.m_eventParams.argString2.Split(',');
                Vector2 screenPoint2 = new Vector2(float.Parse(axisArray2[0]), float.Parse(axisArray2[1]));
                Vector2 localPoint2;
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rect, screenPoint2, cam, out localPoint2))
                {

                    miemie.transform.DOLocalMove(localPoint2.Vector2ToVector3_XY(0f), TenPlusAnimLength.miemieUIFlyTime).SetEase(Ease.Flash);
                }
            }
        }
        else
        {
            Debug.LogError("点不对");
        }
    }

    void ResetRectTransform(
        RectTransform rt,
        Vector2 anchorMin,
        Vector2 anchorMax,
        Vector2 pivot,
        Vector2 anchoredPosition,
        Vector2 sizeDelta)
    {
        rt.anchorMin = anchorMin;
        rt.anchorMax = anchorMax;
        rt.pivot = pivot;
        rt.anchoredPosition = anchoredPosition;
        rt.sizeDelta = sizeDelta;
    }
}

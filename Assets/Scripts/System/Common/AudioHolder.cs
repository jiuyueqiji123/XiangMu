﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHolder : MonoBehaviour, IEasyAudio
{


    public string path = "";
    public string audioName = "";


    private GameAudioSource m_audio;


    private void OnEnable()
    {
        m_audio = this.PlayAudioEx(path + audioName, true);
    }


    private void OnDisable()
    {
        if (m_audio != null)
        {
            m_audio.Stop();
            m_audio = null;
        }
    }



}

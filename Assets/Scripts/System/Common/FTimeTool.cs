﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FTimeTool
{

    private static Dictionary<string, float> m_timeDict = new Dictionary<string, float>();


    public static void Record(string key)
    {
        if (m_timeDict.ContainsKey(key))
        {
            Debug.LogError(key + ":" + (Time.realtimeSinceStartup - m_timeDict[key]));
            m_timeDict.Remove(key);
        }
        else
        {
            m_timeDict.Add(key, Time.realtimeSinceStartup);
        }
    }


}

﻿using UnityEngine;
using TableProto;

public class FTableManager
{

    public static EnglishLevelInfo GetEnglishLevelInfo(uint id)
    {
        if (TableProtoLoader.EnglishLevelInfoDict.ContainsKey(id))
        {
            return TableProtoLoader.EnglishLevelInfoDict[id];
        }
        Debug.LogError("Get EnglishLevelInfo Error! ID:" + id);
        return null;
    }

    public static EnglishLetterInfo GetEnglishLetterInfo(uint id)
    {
        if (TableProtoLoader.EnglishLetterInfoDict.ContainsKey(id))
        {
            return TableProtoLoader.EnglishLetterInfoDict[id];
        }
        Debug.LogError("Get EnglishLevelInfo Error! ID:" + id);
        return null;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardGameManarge : MonoSingleton<CardGameManarge>, IEasyAudio
{

    List<Sprite> Card_Sprite = new List<Sprite>();
    List<Sprite> NumberSprite = new List<Sprite>();
    List<Sprite> ComponentsSprite = new List<Sprite>();
    CardGameModel Model;
    private int CarryOutTime = 0;

    private int GameLevel = 1;

    protected override void Awake()
    {
        base.Awake();
        Model = new CardGameModel();
        UISpriteManager.Instance.LoadAtlas(emUIAltas.Card_Game);
        AudioManager.Instance.PlayMusic("study_cardgame/study_cardgame_sound/603090100",true,false);

    }


    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    public void EnterGame()
    {
        WindowManager.Instance.OpenWindow(WinNames.EnglishLediAnimationPanel);
        EnglishLediAnimationWindow.Instance.PlayLediTalkAnimation(EnglishLediAnimationWindow.ELediFadeInType.BottomLeft, "study_cardgame/study_cardgame_sound/403090101", () =>
        {
            StartGame();

        });
    }

    public void ExitGame()
    {
        WindowManager.Instance.OpenWindow(WinNames.EnglishLediAnimationPanel);
        AudioManager.Instance.PlaySound("study_cardgame/study_cardgame_sound/603090105");
        EnglishLediAnimationWindow.Instance.PlayLediWinAnimation(this.Model.ExitGameAudio[RandomNumbers(1,3)[0]], this.GetAudioNameEx(StudyAudioName.t_32005), false,false,()=>
        {
            AudioManager.Instance.PlaySound("study_cardgame/study_cardgame_sound/603090106");
            //AudioClip GuZhang = ResourceManager.Instance.GetResource("study_subtraction/study_subtraced_sound/603020207", typeof(AudioClip), enResourceType.Sound, false).m_content as AudioClip;
            //AudioManager.Instance.PlaySound(GuZhang);
            TimerManager.Instance.AddTimer(2f,()=> {

            OnExitGame();

            });

        });
    }

    public void StartGame()
    {
        if (GameLevel == 1)
        {
            Card_Sprite = SetSpriteList(RandomNumber(GetCardCount()));
            if (CarryOutTime == 0)
            {
                AudioManager.Instance.PlaySound("study_cardgame/study_cardgame_sound/403090102",true);
            }
            CardGameWindow.Instance.StartGame(GameLevel, GetCardCount(), Card_Sprite, RandomNumbers(GetCardCount(),GetCardCount()), () =>
            {
                this.CarryOutTime++;
                if (this.CarryOutTime == 2)
                {
                    this.GameLevel++;
                }
                MedalWindow.Instance.ShowXunzhang(this.CarryOutTime, true, MedalWindow.EXZCountEnum.Six, () =>
                {
                    CardGameWindow.Instance.CompleteTime = 0;
                    
                    this.ClearData();
                    if (CarryOutTime <= 5)
                    {
                        CardGameManarge.Instance.StartGame();
                    }
                    
                    Debug.Log("斯温这下可牛逼了！");
                });

            });
        }
        if (GameLevel == 2)
        {
            if (CarryOutTime ==2)
            {
                AudioManager.Instance.PlaySound("study_cardgame/study_cardgame_sound/403090103", true);
            }
            NumberSprite = SetSpriteList(Model.Card_Game_Number_Name, RandomNumbers((GetCardCount() / 2),Model.Card_Game_Number_Name.Count));
            ComponentsSprite = SetSpriteList(Model.Card_Game_Components_Name, RandomNumbers((GetCardCount() / 2),Model.Card_Game_Components_Name.Count));
            CardGameWindow.Instance.StartGame(GameLevel, GetCardCount(), NumberSprite,ComponentsSprite, RandomNumbers(GetCardCount(),GetCardCount()), () =>
            {
                this.CarryOutTime++;
                if (this.CarryOutTime == 4)
                {
                    this.GameLevel++;
                }
                MedalWindow.Instance.ShowXunzhang(this.CarryOutTime, true, MedalWindow.EXZCountEnum.Six, () =>
                {
                    CardGameWindow.Instance.CompleteTime = 0;
                    
                    this.ClearData();
                    CardGameManarge.Instance.StartGame();
                    Debug.Log("斯温这下可牛逼了！");
                });

            });
        }
        if (GameLevel == 3)
        {
            if (CarryOutTime == 4)
            {
                AudioManager.Instance.PlaySound("study_cardgame/study_cardgame_sound/403090104", true);
            }
            NumberSprite = SetSpriteList(Model.Card_Game_Number_Name, RandomNumbers((GetCardCount() / 2),Model.Card_Game_Number_Name.Count));
            ComponentsSprite = SetSpriteList(Model.Card_Game_Graphics_Name, RandomNumbers((GetCardCount() / 2),Model.Card_Game_Graphics_Name.Count));
            CardGameWindow.Instance.StartGame(GameLevel, GetCardCount(), NumberSprite, ComponentsSprite, RandomNumbers(GetCardCount(),GetCardCount()), () =>
            {
                this.CarryOutTime++;
                if (this.CarryOutTime == 4)
                {
                    this.GameLevel++;
                }
                MedalWindow.Instance.ShowXunzhang(this.CarryOutTime, true, MedalWindow.EXZCountEnum.Six, () =>
                {
                    CardGameWindow.Instance.CompleteTime = 0;
                    
                    this.ClearData();
                    if (CarryOutTime <= 5)
                    {
                        CardGameManarge.Instance.StartGame();
                    }
                    if (CarryOutTime > 5)
                    {
                        ExitGame();
                    }
                    
                    Debug.Log("斯温这下可牛逼了！");
                    
                });

            });
        }


    }


    public void OnExitGame()
    {
        this.CarryOutTime = 0;
        this.GameLevel = 1;
        CardGameWindow.Instance.OnExitGame();
    }

    public void OpenForm()
    {
        
        WindowManager.Instance.OpenWindow(WinNames.CardGamePanel);
        WindowManager.Instance.OpenWindow(WinNames.MedalPanel);
        MedalWindow.Instance.ShowXunzhang(0, false, MedalWindow.EXZCountEnum.Six);
    }


    public List<Sprite> SetSpriteList(List<int> RandomNumber)
    {
        List<Sprite> Sprites = new List<Sprite>();
        for (int i = 0; i < RandomNumber.Count*2; i++)
        {
            Sprites.Add(UISpriteManager.Instance.GetSprite(emUIAltas.Card_Game,GetSpriteName(RandomNumber)[i]));
        }
        return Sprites;
    }

    public List<Sprite> SetSpriteList(List<string> CardGameSpites,List<int> RandomNumber)
    {
        List<Sprite> Sprites = new List<Sprite>();
        for (int i = 0; i < RandomNumber.Count; i++)
        {
            Sprites.Add(UISpriteManager.Instance.GetSprite(emUIAltas.Card_Game, CardGameSpites[RandomNumber[i]]));
        }
        return Sprites;
    }

    public List<string> GetSpriteName(List<int> RandomNumber)
    {

        List<string> SpriteName = new List<string>();
        List<string[]> SpriteNames = new List<string[]>();
        for (int i = 0; i < RandomNumber.Count; i++)
        {
            SpriteNames.Add(Model.Card_Game_Hero_Atlas_Name[RandomNumber[i]].Split(';'));
        }
        foreach (string[] spriteName in SpriteNames)
        {
            foreach (string spritename in spriteName)
            {
                SpriteName.Add(spritename);
            }
        }
        return SpriteName;
    }

    public List<int> RandomNumbers(int CardCount,int Rand)
    {
        List<int> RNumber = new List<int>();
        while (RNumber.Count < CardCount)
        {
            int x = 0;
            bool y = false;
            x = (int)Mathf.Floor(Random.Range(0, Rand));
            if (RNumber.Count == 0)
            {
                RNumber.Add(x);
            }
            else
            {
                foreach (int item in RNumber)
                {
                    if (x == item)
                    {
                        y = true;
                    }
                }
                if (!y)
                {
                    RNumber.Add(x);
                }
            }

        }
        return RNumber;
    }

    public List<int> RandomNumber(int CardCount)
    {
        List<int> RNumber = new List<int>();
        while (RNumber.Count < CardCount/2)
        {
            int x = 0;
            bool y = false;
            x = (int)Mathf.Ceil(Random.Range(0, Model.Card_Game_Hero_Atlas_Name.Count));
            if (RNumber.Count == 0)
            {
                RNumber.Add(x);
            }
            else
            {
                foreach (int item in RNumber)
                {
                    if (x == item)
                    {
                        y = true;
                    }
                }
                if (!y)
                {
                    RNumber.Add(x);
                }
            }
            
        }
        return RNumber;
    }

    public void DestroyComponent(Component Card)
    {
        Destroy(Card);
    }



    public int GetCardCount()
    {
        if (CarryOutTime == 1)
        {
            return 8;
        }
        else if (CarryOutTime % 2 == 0)
        {
            return 6;
        }
        else
        {
            return 8;
        }
    }


    public void ClearData()
    {
        //CarryOutTime = 0;
        CardGameWindow.Instance.ClearData();
        
       
    }



    public void SetIsClick(bool IsClick)
    {
        Model.IsClick = IsClick;
    }

    public bool GetIsClick()
    {
        return Model.IsClick;
    }


    public void CloseCardClick()
    {
        StartCoroutine("CloseClick");
    }

    public void SetStartPos(int cardcount,List<Vector3> Startpos)
    {
        if (cardcount == 6)
        {
            Model.StartPosforSix = Startpos;
            return;
        }
        if (cardcount == 8)
        {
            Model.StartPosforEight = Startpos;
            return;
        }
       
    }

    public List<Vector3> GetStartPos(int cardcount)
    {
        if (cardcount == 6)
        {
            return Model.StartPosforSix;

        }
        if (cardcount == 8)
        {
            return Model.StartPosforEight;
        }
        return null;
    }

    public int GetGameLevel()
    {
        return this.GameLevel;
    }




    IEnumerator CloseClick()
    {
        yield return new WaitForSeconds(0.5f);
        SetIsClick(true);
        
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CardEventListen : MonoBehaviour {

    [HideInInspector]
    public bool IsClick = false; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (this.gameObject.transform.localRotation.eulerAngles.y >= 90)
        {
            this.gameObject.transform.GetChild(1).SetActive(false);
        }
        if (this.gameObject.transform.localRotation.eulerAngles.y < 90)
        {
            this.gameObject.transform.GetChild(1).SetActive(true);
        }
        if (this.gameObject.transform.rotation.y == 0)
        {
            this.IsClick = false;
        }
        if (this.gameObject.transform.rotation.y != 0)
        {
            this.IsClick = true;
        }
	}

    public void OnClickCard()
    {
       
        if (!CardGameManarge.Instance.GetIsClick() || this.IsClick)
        {
            Debug.LogError(CardGameWindow.Instance.Completecount);
            Debug.LogError(this.IsClick);
            return;
        }
        this.IsClick = true;
        //Debug.LogError("11111111111");
        //Debug.LogError(this.gameObject.name);
        this.gameObject.transform.DORotate(new Vector3(0, 180, 0), 1f);
        AudioManager.Instance.PlaySound("study_cardgame/study_cardgame_sound/603090104");
        CardGameWindow.Instance.Completecount++;
        CardGameWindow.Instance.SetFlip_Card(this.gameObject.transform);
        if (CardGameWindow.Instance.Completecount >= 2)
        {
            CardGameManarge.Instance.SetIsClick(false);
            StartCoroutine("IsCarryOut");
            //StartCoroutine("CloseClick");
        }
        
    }

    IEnumerator IsCarryOut()
    {
        yield return new WaitForSeconds(1f);
        EventBus.Instance.BroadCastEvent(EventID.ON_CARD_GAME_CARRY_OUT);
    }


    IEnumerator CloseClick()
    {
        yield return new WaitForSeconds(1f);
        this.IsClick = false;
    }

}

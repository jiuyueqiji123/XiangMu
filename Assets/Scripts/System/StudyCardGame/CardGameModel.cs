﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardGameModel {


    public bool IsClick = true;

    public List<AudioClip> GameLeDiSpeek = new List<AudioClip>();

    public List<Vector3> StartPosforSix = new List<Vector3>();

    public List<Vector3> StartPosforEight = new List<Vector3>();

    public List<string> Card_Game_Hero_Atlas_Name = new List<string>()
    {
        "swxl-kapai-01;swxl-kapai-01", "swxl-kapai-02;swxl-kapai-02", "swxl-kapai-03;swxl-kapai-03","swxl-kapai-04;swxl-kapai-04","swxl-kapai-05;swxl-kapai-05","swxl-kapai-06;swxl-kapai-06",
        "swxl-kapai-07;swxl-kapai-07","swxl-kapai-08;swxl-kapai-08","swxl-kapai-09;swxl-kapai-09","swxl-kapai-10;swxl-kapai-10","swxl-kapai-11;swxl-kapai-11","swxl-kapai-12;swxl-kapai-12",
    };

    public List<string> Card_Game_Number_Name = new List<string>()
    {
        "swxl-number-01","swxl-number-02","swxl-number-03","swxl-number-04","swxl-number-05","swxl-number-06","swxl-number-07","swxl-number-08","swxl-number-09",
    };

    public List<string> Card_Game_Components_Name = new List<string>()
    {
        "swxl-wawa","swxl-shuye","swxl-yingtao-01","swxl-hua",/*"swxl-huoche",*/
    };

    public List<string> Card_Game_Graphics_Name = new List<string>()
    {
        "swxl-juxing","swxl-shuidi","swxl-yuan","swxl-zhangfangxing",
    };

    public List<string> ExitGameAudio = new List<string>()
    {
        "study_subtraction/study_subtraced_sound/403090201","study_cardgame/study_cardgame_sound/403090202","study_cardgame/study_cardgame_sound/403090203","study_cardgame/study_cardgame_sound/403090204",
    };

    //public void GetGameSound()
    //{
    //    ResourceManager.Instance.GetResource("");
    //}

}

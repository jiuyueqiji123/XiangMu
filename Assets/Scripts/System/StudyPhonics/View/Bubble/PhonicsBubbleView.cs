﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class PhonicsBubbleView : MonoBehaviour, IPointerClickHandler {

	private Text text;

	private Image bubble;

	private PhonicsBubbleGame game;

	private int index;

	private Image thing;

	public List<Sprite> sprites = new List<Sprite>();

	private Tweener tween;

    public GameObject effect;

    //public GameObject clickEffect;

    private int randomNum;

    private char curNum;

    private bool isClick = false;
	// Use this for initialization
	void Awake () {
        
        this.text = transform.Find ("Text").GetComponent<Text>();
		this.bubble = transform.Find ("Image").GetComponent<Image>();
		this.thing = transform.Find ("Thing").GetComponent<Image>();
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void InitView() {
		this.bubble.sprite = sprites[Random.Range(0, sprites.Count)];
		this.transform.localScale = Vector3.one * Random.Range (0.7f, 1f);
		this.bubble.gameObject.SetActive (true);
	}

	public void Init(PhonicsBubbleGame game, int index, char num) {

        this.InitView ();
        isClick = false;
        effect.SetActive(false);
        //clickEffect.SetActive(false);
        this.game = game;
		this.index = index;
        randomNum = Random.Range(1, 3);
        curNum = num;
        if (num == '0') {
			this.text.text = ((char)index).ToString ();
			this.text.gameObject.SetActive (true);
			this.text.transform.localScale = Vector3.one;
			this.thing.gameObject.SetActive (false);
		} else {
            this.text.gameObject.SetActive (false);
			this.thing.gameObject.SetActive (true);
            switch(num) {
                //'B', 'E', 'H', 'K', 'N', 'Q', 'T', 'W', 'Z'
                case 'B':
                    thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path +( randomNum == 1 ? "ball": "bear"));
                    break;
                case 'E':
                    thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "egg" : "elephant"));
                    break;
                case 'H':
                    thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "hen" : "horse"));
                    break;
                case 'K':
                    thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "key" : "kite"));
                    break;
                case 'N':
                    thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "nest" : "noodle"));
                    break;
                case 'Q':
                    thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "queen" : "quilt"));
                    break;
                case 'T':
                    thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "tea" : "tiger"));
                    break;
                case 'W':
                    thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "wind" : "wolf"));
                    break;
                case 'Z':
                    thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "zebra" : "ZERO"));
                    break;
            }
            
            this.thing.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            thing.SetNativeSize();
		}

		float time = Random.Range (5f, 7f);
		tween = transform.DOLocalMoveY (800f, time).SetEase(Ease.Linear).OnComplete (new TweenCallback (delegate() {
			this.game.RecycleBubble(this);
		}));
	}

	public void OnPointerClick (PointerEventData eventData) {
        if (isClick)
            return;
        isClick = true;
        AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "603010802");

        StartCoroutine(PlayLetterSound());

        this.game.ClickBubble (false);
        effect.SetActive(true);
        //clickEffect.SetActive(true);
        this.bubble.gameObject.SetActive (false);
		tween.Kill ();
		Transform tf = this.text.gameObject.activeSelf ? this.text.transform : this.thing.transform;
		tf.DOScale (tf.localScale * 1.3f, 0.5f).OnComplete (new TweenCallback (delegate {
			tf.gameObject.SetActive(false);
			this.game.RecycleBubble(this);
            
		}));
	}

	public void Recycle() {
        if (tween != null)
        {
            tween.Kill();
            tween = null;
        }
        this.game.RecycleBubble(this, false);
	}

    IEnumerator PlayLetterSound()
    {
        yield return new WaitForSeconds(0.2f);
        if (curNum == '0')
        {
            switch ((char)index)
            {
                case 'B':
                case 'b':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020402");
                    break;
                case 'E':
                case 'e':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020405");
                    break;
                case 'H':
                case 'h':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020408");
                    break;
                case 'K':
                case 'k':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020411");
                    break;
                case 'N':
                case 'n':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020414");
                    break;
                case 'Q':
                case 'q':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020417");
                    break;
                case 'T':
                case 't':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020420");
                    break;
                case 'W':
                case 'w':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020423");
                    break;
                case 'Z':
                case 'z':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020426");
                    break;
            }
        }
        else
        {
            switch ((char)index)
            {
                case 'B':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + (randomNum == 1 ? "401020202" : "401020228"));
                    break;
                case 'E':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + (randomNum == 1 ? "401020205" : "401020231"));
                    break;
                case 'H':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + (randomNum == 1 ? "401020234" : "401020208"));
                    break;
                case 'K':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + (randomNum == 1 ? "401020237" : "401020211"));
                    break;
                case 'N':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + (randomNum == 1 ? "401020214" : "401020240"));
                    break;
                case 'Q':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + (randomNum == 1 ? "401020217" : "401020243"));
                    break;
                case 'T':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + (randomNum == 1 ? "401020246" : "401020220"));
                    break;
                case 'W':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + (randomNum == 1 ? "401020249" : "401020223"));
                    break;
                case 'Z':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + (randomNum == 1 ? "401020226" : "401020252"));
                    break;
            }
        }
    }
}

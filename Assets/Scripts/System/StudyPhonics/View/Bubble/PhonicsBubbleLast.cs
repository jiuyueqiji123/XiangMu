﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class PhonicsBubbleLast : MonoBehaviour, IPointerClickHandler {

	private Text text;

	private PhonicsBubbleGame game;

	private int index;

	private Tweener tween;

	private Image bubble;

    private GameObject effect;

    //private GameObject clickEffect;

    private bool isClick = false;

    public float maxY = 10f;
    public float minY = -5f;
    public float aniSpeed = 0.4f;

    // Use this for initialization
    void Awake () {
		this.text = transform.Find ("Text").GetComponent<Text>();
		this.bubble = transform.Find ("Image").GetComponent<Image>();
        this.effect = transform.Find("502003054").gameObject;
        //this.clickEffect = transform.Find("502003056").gameObject;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Init(PhonicsBubbleGame game, int index) {
        isClick = false;
		this.game = game;
		this.index = index;
		this.text.text = ((char)index).ToString ();
		this.text.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
		this.bubble.gameObject.SetActive (true);

        tween = transform.DOLocalMoveY (minY, 3).SetEase (Ease.Linear).OnComplete<Tweener>(()=> 
        {
            //0f
            Debug.Log(" OnComplete ====  ");
            PlayUpAni();
        });            
    }

    void PlayUpAni()
    {
        tween = transform.DOLocalMoveY(maxY, aniSpeed).SetEase(Ease.Linear).SetLoops<Tweener>(-1, LoopType.Yoyo);
        //.OnComplete<Tweener>(() =>
        //{
            //PlayDownAni();
        //})
    }

    public void killAnim()
    {
        //Debug.Log("killAnim === ");
        //tween.SetAutoKill();
        //tween.Pause();
        tween.Kill();        
    }
    void PlayDownAni()
    {
        transform.DOLocalMoveY(minY, aniSpeed).SetEase(Ease.Linear).OnComplete<Tweener>(() =>
        {
            PlayUpAni();
        });
    }

    public void OnPointerClick (PointerEventData eventData) {
        if (isClick)
            return;
        isClick = true;
        AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "603010802");
        switch ((char)index)
            {
                case 'B':
                case 'b':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020402");
                    break;
                case 'E':
                case 'e':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020405");
                    break;
                case 'H':
                case 'h':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020408");
                    break;
                case 'K':
                case 'k':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020411");
                    break;
                case 'N':
                case 'n':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020414");
                    break;
                case 'Q':
                case 'q':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020417");
                    break;
                case 'T':
                case 't':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020420");
                    break;
                case 'W':
                case 'w':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020423");
                    break;
                case 'Z':
                case 'z':
                    AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020426");
                    break;
            }
        
        effect.SetActive(true);
        //clickEffect.SetActive(true);
        this.game.ClickBubble (true);
		tween.Kill ();
		transform.localPosition = Vector3.zero;
		this.bubble.gameObject.SetActive (false);

		text.transform.DOScale (1.8f, 0.5f).OnComplete (new TweenCallback (delegate {
			this.game.End(true);
            effect.SetActive(false);
            //clickEffect.SetActive(false);
        }));
	}
}

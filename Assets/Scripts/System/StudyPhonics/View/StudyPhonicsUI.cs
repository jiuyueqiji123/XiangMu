﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StudyPhonicsUI {

	private static string UI_PATH = "study_phonics/StudyPhonicsForm";

	private Dictionary<emPhonicsGameType, PhonicsGameBase> gameTypeDict = new Dictionary<emPhonicsGameType, PhonicsGameBase>();

	private HUIFormScript form;

	private GameObject BackBtn;

    private GameObject KcjsBtn;


	private PhonicsEggGame eggGame;

	private PhonicsBubbleGame bubbleGame;

	private PhonicsTapGame tapGame;

	private PhonicsEncouragePanel encouragePanel;

	public void OpenUI() {
		this.form = HUIManager.Instance.OpenForm (UI_PATH, false);

		this.BackBtn = this.form.transform.Find ("ButtonHome").gameObject;
		HUIUtility.SetUIMiniEvent (BackBtn, enUIEventType.Click, enUIEventID.Phonics_Back_Click);
        this.KcjsBtn = this.form.transform.Find("KcjsBtn").gameObject;
        HUIUtility.SetUIMiniEvent(KcjsBtn, enUIEventType.Click, enUIEventID.Phonics_Kcjs_Click);

#if SUPER_MY
        this.KcjsBtn.gameObject.SetActive(SDKManager.Instance.IsShowGZH());
#endif

        eggGame = this.form.transform.Find ("PanelEgg").GetComponent<PhonicsEggGame>();
		bubbleGame = this.form.transform.Find ("PanelBubble").GetComponent<PhonicsBubbleGame>();
		tapGame = this.form.transform.Find ("PanelTap").GetComponent<PhonicsTapGame>();
		eggGame.gameObject.SetActive (false);
		bubbleGame.gameObject.SetActive (false);
		tapGame.gameObject.SetActive (false);

		gameTypeDict[emPhonicsGameType.Bubble] = bubbleGame;
		gameTypeDict[emPhonicsGameType.Tap]=tapGame;
		gameTypeDict[emPhonicsGameType.Egg]= eggGame;

		encouragePanel = this.form.transform.Find ("PanelEncourage").GetComponent<PhonicsEncouragePanel> ();
		encouragePanel.gameObject.SetActive (false);

        guidao_img = this.form.transform.Find("BG/guidao").GetComponent<Image>();

    }

	public PhonicsGameBase GetGame(emPhonicsGameType type) {
		PhonicsGameBase game;
		if (gameTypeDict.TryGetValue (type, out game)) {
			return game;
		}
		return null;
	}

	public void CloseUI() {
		HUIManager.Instance.CloseForm (UI_PATH);
        gameTypeDict.Clear();
        this.BackBtn = null;
        this.KcjsBtn = null;
        this.eggGame = null;
        this.bubbleGame = null;
        this.tapGame = null;
        this.encouragePanel = null;
        this.form = null;
	}



	public void SetEncourageView(bool enable) {
		encouragePanel.gameObject.SetActive (enable);
	}

    Image guidao_img;
    public Image GetGuiDao() {
        return guidao_img;
    }

    public GameObject GetZhuanPan() {
        return this.form.transform.Find("zhuanpan").gameObject;
    }

    public GameObject GetLedi2d() {
        return this.form.transform.Find("ledi-donghua02").gameObject;
    }

    public GameObject PanelEncourage
    {
        get
        {
            return this.form.transform.Find("PanelEncourage").gameObject;
        }
    }
}

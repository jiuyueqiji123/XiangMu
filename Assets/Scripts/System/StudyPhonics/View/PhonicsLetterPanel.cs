﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class PhonicsLetterPanel : MonoBehaviour
{

    private const string LETTER_PATH = "study_phonics/study_phonics_prefab/Letter";

    private const string PAGE_PATH = "study_phonics/LetterPage";

    public GameObject[] letters;


    private float angle = 72f;

    private bool moveDown = false;

    public float ShakeStrength = 15f;

    /// <summary>  
    /// 第一次按下的位置  
    /// </summary>  
    private Vector2 first = Vector2.zero;
    /// <summary>  
    /// 鼠标的拖拽位置（第二次的位置）  
    /// </summary>  
    private Vector2 second = Vector2.zero;

    private float timer;

    public float offsetTime = 0.2f;

    // Use this for initialization
    void Awake()
    {
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.PhonicsTapGameAtlas);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.PhonicsTapGame1Atlas);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.PhonicsTapGame2Atlas);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.PhonicsBubbleGameAtlas);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.PhonicsEggGameAtlas);
        //UISpriteManager.Instance.LoadAtlas(emUIAltas.PhonicsEggGame1Atlas);
        UISpriteManager.Instance.LoadAtlas(emUIAltas.EggAtlas);




        for (int i = 0; i < letters.Length; i++)
        {
            letters[i].GetComponent<PhonicsLetterItem>().Change((int)'A' + i, this);
        }

    }

    public void ShakeZm()
    {
        string zm = StudyPhonicsManager.Instance.GetLastLevel();
        if (!string.IsNullOrEmpty(zm))
        {
            char a = zm.ToCharArray()[0];
            char tempZM = (char)((int)a + 1);
            Debug.Log("  tempZM ===  " + tempZM);
            for (int i = 0; i < letters.Length; i++)
            {
                if (letters[i].GetComponent<PhonicsLetterItem>().index == (int)tempZM)
                    letters[i].GetComponent<PhonicsLetterItem>().ShakeZm();
            }
        }
    }

    public void SetBuyIconState(bool isActive)
    {
        for (int i = 0; i < letters.Length; i++)
        {
            letters[i].GetComponent<PhonicsLetterItem>().SetBuyIconState(isActive);
        }
    }

    public void SetBIconState(bool isActive)
    {
        PhonicsLetterItem pli = letters[1].GetComponent<PhonicsLetterItem>();
        pli.SetBuyIconState(isActive && !pli.isBuy);
    }


    void OnGUI()
    {
        if (WindowManager.Instance.IsOpenWindow(WinNames.EnglishZRPDKCJSPanel) || WindowManager.Instance.IsOpenWindow(WinNames.EnglishMioPanel))
            return;
        if (ShopDialogController.Instance.view.form != null && ShopDialogController.Instance.view.form.gameObject.activeSelf)
        {
            return;
        }
        if (Event.current.type == EventType.MouseDown)
        {
            //记录鼠标按下的位置 　　  
            first = Event.current.mousePosition;


        }
        if (Event.current.type == EventType.MouseDrag)
        {

            timer += Time.deltaTime;
            //记录鼠标拖动的位置 　　
            if (timer > offsetTime)
            {
                second = Event.current.mousePosition;

                if (second.x < first.x - 8)
                {
                    MoveStart();
                    if (moveDown == false)
                    {
                        //拖动的位置的x坐标比按下的位置的x坐标小时,响应向左事件 
                        transform.DORotate(new Vector3(0, 0, 72f), 1f, RotateMode.LocalAxisAdd);
                        moveDown = true;
                        Invoke("SetMoveDown", 1f);
                    }

                }
                if (second.x > first.x + 8)
                {
                    MoveStart();
                    if (moveDown == false)
                    {
                        //拖动的位置的x坐标比按下的位置的x坐标大时,响应向右事件 　　  
                        transform.DORotate(new Vector3(0, 0, -72f), 1f, RotateMode.LocalAxisAdd);
                        moveDown = true;
                        Invoke("SetMoveDown", 1f);
                    }
                }
                first = second;
                timer = 0;
            }
        }
    }

    void SetMoveDown()
    {
        moveDown = false;
        foreach (var temp in letters)
        {
            temp.GetComponent<PhonicsLetterItem>().enabled = true;
        }
    }

    void MoveStart()
    {
        foreach (var temp in letters)
        {
            temp.GetComponent<PhonicsLetterItem>().enabled = false;
        }
    }


    /// <summary>
    /// 向右拖动  
    /// </summary>
    public void MoveRight()
    {
        MoveStart();
        if (moveDown == false)
        {
            //拖动的位置的x坐标比按下的位置的x坐标小时,响应向左事件 
            transform.DORotate(new Vector3(0, 0, 72f), 1f, RotateMode.LocalAxisAdd);
            moveDown = true;
            Invoke("SetMoveDown", 1f);
        }
    }

    /// <summary>
    /// 一开始 初始化 移动到目标位置
    /// </summary>
    /// <param name="pos"></param>
    public void MoveTarget(string l)
    {
        float z = -36f;
        //a - e : -36   f 36  k 108  p  -180  u -108
        if (l == "A" || l == "B" || l == "C" || l == "D" || l == "E")
            z = -36f;
        else if (l == "F" || l == "G" || l == "H" || l == "I" || l == "J")
            z = 36f;
        else if (l == "K" || l == "L" || l == "M" || l == "N" || l == "O")
            z = 108f;
        else if (l == "P" || l == "Q" || l == "R" || l == "S" || l == "T")
            z = -180f;
        else if (l == "U" || l == "V" || l == "W" || l == "X" || l == "Y" || l == "Z")
            z = -108f;

        transform.rotation = Quaternion.Euler(new Vector3(0, 0, z));
    }

    public void SetItemColor(int asc, bool Enable)
    {
        for (int i = 0; i < letters.Length; i++)
        {
            if (letters[i].GetComponent<PhonicsLetterItem>().index == asc)
            {
                if (Enable)
                    letters[i].GetComponent<PhonicsLetterItem>().material.shader = Shader.Find("Unlit/Transparent");
                else
                    letters[i].GetComponent<PhonicsLetterItem>().material.shader = Shader.Find("Unlit/Transparent Color");
            }
        }
    }

}

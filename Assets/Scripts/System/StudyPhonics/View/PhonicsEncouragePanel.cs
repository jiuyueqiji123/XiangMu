﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhonicsEncouragePanel : MonoBehaviour {

	private GameObject nextBtn;

	// Use this for initialization
	void Awake () {
		this.nextBtn = this.transform.Find ("ButtonNext").gameObject;
		HUIUtility.SetUIMiniEvent (this.nextBtn, enUIEventType.Click, enUIEventID.Phonics_Next_Letter);
        
    }



    public void SetLetter(int letter) {
		if (letter == (int)'Z') {
			this.nextBtn.gameObject.SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetLetterTrue() {
        if (this.nextBtn != null)
        {
            this.nextBtn.gameObject.SetActive(true);
        }
    }
}

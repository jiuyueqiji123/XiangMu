﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PhonicsEggView : MonoBehaviour
{

    private Transform Left;

    private Transform Right;

    private Image Thing;

    private Image image;

    private Text text;

    private PhonicsEggGame game;

    private Text letter;
    public static Sequence tw;

    private int curNum;

    private int curRandomNum;


    public void Init(PhonicsEggGame game)
    {
        this.GetComponent<Animator>().runtimeAnimatorController = null;

        Left = transform.Find("Left");
        Right = transform.Find("Right");
        Thing = transform.Find("Thing").GetComponent<Image>();
        object Controller = ResourceManager.Instance.GetResource("Controller/dongwu_dance", typeof(object), enResourceType.Prefab, false).m_content as object;
        Thing.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Controller;
        image = GetComponent<Image>();
        text = transform.Find("Text").GetComponent<Text>();
        letter = transform.Find("letter").GetComponent<Text>();
        letter.transform.SetActive(false);
        text.transform.SetActive(true);
        this.game = game;
        Left.gameObject.SetActive(false);
        Right.gameObject.SetActive(false);
        image.enabled = true;
        Thing.gameObject.SetActive(false);
        char num = (char)StudyPhonicsManager.Instance.CurLetter;
        text.text = num.ToString();
        switch (this.name)
        {
            case "EggLeftTop":
                image.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "egg1");
                Left.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "landan01");
                Right.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "landan02");
                break;
            case "EggLeftDown":
                image.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "egg2");
                Left.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "fendan01");
                Right.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "fendan02");
                break;
            case "EggRightTop":
                image.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "egg3");
                Left.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "lvdan01");
                Right.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "lvdan02");
                break;
            case "EggRightDown":
                image.sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "egg4");
                Left.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "hongdan01");
                Right.GetComponent<Image>().sprite = UISpriteManager.Instance.GetSprite(emUIAltas.EggAtlas, "hongdan02");
                break;

        }


    }


    public void ShowBreak(char num, int index, int randomNum)
    {
        letter.gameObject.SetActive(true);
        curNum = num;
        curRandomNum = randomNum;
        image.enabled = false;
        Left.gameObject.SetActive(true);
        Right.gameObject.SetActive(true);
        Thing.gameObject.SetActive(true);
        this.GetComponent<Animator>().runtimeAnimatorController = null;
        this.transform.localRotation = Quaternion.Euler(Vector3.zero);
        Sequence seq = DOTween.Sequence();
        seq.Append(Left.DOLocalRotate(new Vector3(0f, 0f, 60f), 0.5f));
        seq.Insert(0f, Right.DOLocalRotate(new Vector3(0f, 0f, -60f), 0.5f));
        seq.AppendInterval(2f);
        Invoke("CloseLeftAndRight", 0.5f);
        tw = seq.OnComplete(new TweenCallback(delegate
        {           
            letter.gameObject.SetActive(false);
            this.game.MoveSmall();

        }));
        seq.Play();

        //Debug.Log(" PlaySound    603010905 ");
        AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "603010905");

        switch (num)
        {
            case 'D':
                Thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "dog" : "duck") + "1");
                letter.text = "<color=#FF0000>D</color><color=#000000>" + (randomNum == 1 ? "dog" : "duck").Replace("d", string.Empty) + "</color>";

                break;
            case 'F':
                Thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "finger" : "fish") + "1");
                letter.text = "<color=#FF0000>F</color><color=#000000>" + (randomNum == 1 ? "finger" : "fish").Replace("f", string.Empty) + "</color>";

                break;
            case 'I':
                Thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "ice-cream" : "iguana") + "1");
                letter.text = "<color=#FF0000>I</color><color=#000000>" + (randomNum == 1 ? "ice-cream" : "iguana").Replace("i", string.Empty) + "</color>";

                break;
            case 'L':
                Thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "lemon" : "lion")  +"1");
                letter.text = "<color=#FF0000>L</color><color=#000000>" + (randomNum == 1 ? "lemon" : "lion").Replace("l", string.Empty) + "</color>";

                break;
            case 'O':
                Thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "octopus" : "orange") +"1");
                letter.text = "<color=#FF0000>O</color><color=#000000>" + (randomNum == 1 ? "octopus" : "orange").Replace("o", string.Empty) + "</color>";

                break;
            case 'R':
                Thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "rabbit" : "robot") + "1");
                letter.text = "<color=#FF0000>R</color><color=#000000>" + (randomNum == 1 ? "rabbit" : "robot").Replace("r", string.Empty) + "</color>";

                break;
            case 'U':
                Thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "umbrella" : "umbrella") + "1");//ukelele
                letter.text = "<color=#FF0000>U</color><color=#000000>" + (randomNum == 1 ? "umbrella" : "umbrella").Replace("u", string.Empty) + "</color>";//ukelele

                break;
            case 'X':
                Thing.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (randomNum == 1 ? "box" : "fox") + "1");
                letter.text = (randomNum == 1 ? "Bo" : "Fo") + "<color=#FF0000>x</color><color=#000000>" + "</color>";

                break;

        }

        text.transform.SetActive(false);

        Thing.SetNativeSize();

    }

    private void CloseLeftAndRight()
    {
        Left.gameObject.SetActive(false);
        Right.gameObject.SetActive(false);
        switch (curNum)
        {
            case 'D':
                AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggSound + (curRandomNum == 1 ? "401020445" : "401020446"));
                break;
            case 'F':

                AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggSound + (curRandomNum == 1 ? "401020448" : "401020447"));
                break;
            case 'I':

                AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggSound + (curRandomNum == 1 ? "401020449" : "401020450"));
                break;
            case 'L':

                AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggSound + (curRandomNum == 1 ? "401020452" : "401020451"));
                break;
            case 'O':

                AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggSound + (curRandomNum == 1 ? "401020454" : "401020453"));
                break;
            case 'R':

                AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggSound + (curRandomNum == 1 ? "401020455" : "401020456"));
                break;
            case 'U':

                AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggSound + (curRandomNum == 1 ? "401020457" : "401020457"));
                break;
            case 'X':

                AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggSound + (curRandomNum == 1 ? "401020458" : "401020459"));
                break;
        }
    }
}

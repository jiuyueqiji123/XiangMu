﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class PhonicsEggBreak : MonoBehaviour, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
{

    public Image BreakImage;

    private int time;

    private PhonicsEggGame game;

    public GameObject clickEffect;

    public Camera cam;

    private List<GameObject> effects = new List<GameObject>();

    private Vector3 pos;

    private bool isDrag = false;

    public void Show()
    {
        this.gameObject.SetActive(true);
        this.BreakImage.fillAmount = 0f;
        cam = GameObject.Find("Camera_Form").GetComponent<Camera>();
        time = 0;
    }

    public void Init(PhonicsEggGame game)
    {

        Hide();
        this.game = game;

    }

    public void Hide()
    {
        CloseEffect();
        this.gameObject.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isDrag == true)
        {
            return;
        }
        Debug.Log("OnPointerClick  603010902 ==== ");
        clickEffect = Instantiate(ResourceManager.Instance.GetResource(Itempathinfo.EffectPath + "502003054", typeof(object), enResourceType.Prefab).m_content as GameObject);
        clickEffect.transform.SetParent(game.transform);
        pos = cam.ScreenToWorldPoint(eventData.position);
        Debug.Log(pos);
        Vector3 effectpos = new Vector3(pos.x, pos.y, pos.z + 50);
        clickEffect.transform.position = effectpos;

        effects.Add(clickEffect);
        AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "603010902");

        //Invoke("CloseEffect", 3f);
        if (time == 10)
        {
            return;
        }
        StartCoroutine(PlayLetterSound());
        this.time++;
        this.BreakImage.fillAmount += 0.1f;
        if (time == 10)
        {
            Invoke("CloseEffect", 1f);
            this.game.BreakEgg();
        }
    }

    private void CloseEffect()
    {
        foreach (var temp in effects)
        {
            Destroy(temp);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("OnPointerUp  603010902 ==== ");
        clickEffect = Instantiate(ResourceManager.Instance.GetResource(Itempathinfo.EffectPath + "502003054", typeof(object), enResourceType.Prefab).m_content as GameObject);
        clickEffect.transform.SetParent(game.transform);
        Vector3 pointpos = cam.ScreenToWorldPoint(eventData.position);
        Debug.Log(pointpos);
        Vector3 effectpos = new Vector3(pointpos.x, pointpos.y, pointpos.z + 50);
        clickEffect.transform.position = effectpos;

        effects.Add(clickEffect);
        AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "603010902");

        //Invoke("CloseEffect", 3f);
        this.time++;
        this.BreakImage.fillAmount += 0.1f;

        StartCoroutine(PlayLetterSound());

        if (time == 10)
        {
            Invoke("CloseEffect", 1f);
            this.game.BreakEgg();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isDrag = true;
    }

    IEnumerator PlayLetterSound()
    {
        yield return new WaitForSeconds(0.2f);
        switch ((char)StudyPhonicsManager.Instance.CurLetter)
        {
            case 'D':

                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020404");
                break;
            case 'F':

                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020406");
                break;
            case 'I':

                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020409");
                break;
            case 'L':

                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020412");
                break;
            case 'O':

                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020415");
                break;
            case 'R':

                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020418");
                break;
            case 'U':

                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020421");
                break;
            case 'X':

                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020424");
                break;
        }
    }
}

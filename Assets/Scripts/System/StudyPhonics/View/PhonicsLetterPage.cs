﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhonicsLetterPage : MonoBehaviour {

	public List<Transform> Positions = new List<Transform> (6);

	private int num;

	public void SetNum(int num) {
		this.num = num;
		if (num == 5) {
			Positions [5].gameObject.SetActive (false);
		} else if (num == 6) {
			for (int i = 0; i < 3; i++) {
				Positions [i + 3].localPosition = new Vector3 (Positions [i].localPosition.x, Positions [i + 3].localPosition.y, Positions [i + 3].localPosition.z);
			}
		}
	}

	public void AddLetter(GameObject go, int index) {
		for (int i = 0; i < num; i++) {
			GameObject item = GameObject.Instantiate (go) as GameObject;
			item.transform.SetParent (Positions [i]);
			item.transform.localPosition = Vector3.zero;
			item.transform.localRotation = Quaternion.identity;
			item.transform.localScale = Vector3.one;

			//item.GetComponent<PhonicsLetterItem> ().Change (index + i);
		}
	}
}

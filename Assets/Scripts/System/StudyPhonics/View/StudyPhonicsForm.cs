﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StudyPhonicsForm : MonoBehaviour {
    public GameObject shdz;

    public GameObject ledidonghua02;

    public GameObject zhuanpan;

    public GameObject fengche;

    public GameObject sun;

    public GameObject yun1;

    public GameObject yun2;

    public float rotateZ=0.8f;

    private Tweener tweenerSun;

    private Tweener tweenerYun1;

    private Tweener tweenerYun2;

    private const string studyPhonicPath = "study_phonics/study_phonics_sound/";

    float rotateSpeed = 1;
    // Use this for initialization
    void Start () {
        Input.multiTouchEnabled = false;
        tweenerSun = sun.transform.DOScale(Vector3.one * 1.2f, 1f);
        tweenerSun.SetAutoKill(false);
        tweenerSun.SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        tweenerSun.Play();
        shdz.SetActive(true);
        ledidonghua02.SetActive(false);
        AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "403010101");
        StudyPhonicsManager.Instance.SetZhuanPanEnable(false);
        zhuanpan.GetComponent<PhonicsLetterPanel>().SetBIconState(false);
        Invoke("LediLeave", 6f);
        Invoke("ShowLedi", 6.5f);
        foreach(Transform child in zhuanpan.transform) {
            child.GetComponent<PhonicsLetterItem>().enabled = false;
        }
        /*var button = fengche.transform.GetChild(0).GetComponent<UnityEngine.UI.Button>();
                if (button != null)
                  {
                       button.onClick.RemoveAllListeners();
                        button.onClick.AddListener(FengcheRotateSpeed);
                  }*/
        //AudioManager.Instance.PlayMusic(studyPhonicPath + "603010701", true, true);


    }
	
	// Update is called once per frame
	void Update () {
        fengche.transform.Rotate(new Vector3(0, 0, -1f) * rotateSpeed);
        yun1.transform.Translate(new Vector3(0.1f, 0, 0));
        yun2.transform.Translate(new Vector3(0.1f, 0, 0));
        if(yun1.transform.localPosition.x>=1750) {
            yun1.transform.localPosition = new Vector3(-764, 9, 0);
        }
        if(yun2.transform.localPosition.x>=1265) {
            yun2.transform.localPosition = new Vector3(-1270, 225, 0);
        }

        if (isAddSpeed)
        {
            speedTime -= Time.deltaTime;
            if (speedTime <= 0)
            {
                rotateSpeed = Mathf.Lerp(rotateSpeed, 1,  Time.deltaTime);
                //Debug.Log(rotateSpeed + "  ====== rotateSpeed");
                if (rotateSpeed <= 1.1)
                {                   
                    rotateSpeed = 1f;
                    isAddSpeed = false;
                }
            }            
        }
        
    }

    float speedTime = 2f;
    bool isAddSpeed = false;
    public void FengcheRotateSpeed()
    {
        isAddSpeed = true;
        speedTime = 2f;
        rotateSpeed++;
        if (rotateSpeed > 6)
            rotateSpeed = 6;
    }

    private void ShowLedi() {
       
        ledidonghua02.SetActive(true);
        //改变轨道位置
        string zm = StudyPhonicsManager.Instance.GetLastLevel();
        if (!string.IsNullOrEmpty(zm))
        {
            //StudyPhonicsManager.Instance.RefreshGuiDao(zm);
            if (zm != "Z")
            {
                char a = zm.ToCharArray()[0];
                string tempZM = ((char)((int)a + 1)).ToString();
                Debug.Log("  tempZM ===  " + tempZM);
                StudyPhonicsManager.Instance.LediMove(tempZM);
                //转转盘
                //StudyPhonicsManager.Instance.MoveTargetZp(tempZM);
            }
            else
                ledidonghua02.transform.DOLocalMove(new Vector3(-705f, -323f, -462), 0.5f);
        }
        else
            ledidonghua02.transform.DOLocalMove(new Vector3(-705f, -323f, -462), 0.5f);
        foreach (Transform child in zhuanpan.transform)
        {
            child.GetComponent<PhonicsLetterItem>().enabled = true;
        }
        StudyPhonicsManager.Instance.SetZhuanPanEnable(true);
    }

    private void LediLeave() {
        zhuanpan.GetComponent<PhonicsLetterPanel>().SetBIconState(true);
        shdz.transform.DOLocalMoveX(-1460f, 0.5f);
    }
}

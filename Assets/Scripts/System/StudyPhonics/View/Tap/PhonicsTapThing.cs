﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using System;

public class PhonicsTapThing : MonoBehaviour, IPointerClickHandler,IPointerDownHandler,IPointerUpHandler {

	private PhonicsTapGame game;

	private Sequence seqStart;

    public Camera cam;

    private Sequence dragSeq;

    private Vector3 dragBegin;

    private Vector3 dragEnd;

    private GameObject clickEffect;

    private bool isDrag = false;

    private bool isClick = false;


    // Use this for initialization
    void Start () {
        cam = GameObject.Find("Camera_Form").GetComponent<Camera>();
        

    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Init(PhonicsTapGame game) {
        isClick = false;
		this.game = game;
        if (dragSeq != null && dragSeq.IsPlaying())
        {
            dragSeq.Kill();
        }

		seqStart = DOTween.Sequence ();
		seqStart.Append (transform.DOScale (new Vector3 (1.2f, 1.2f, 1.2f), 0.5f));
		seqStart.Append (transform.DOScale (new Vector3 (1f, 1f, 1f), 0.1f));
		seqStart.Play ();
	}

    public void OnPointerClick(PointerEventData eventData) {
        if (isClick)
            return;
        isClick = true;
        if (isDrag == false)
        {
            clickEffect = Instantiate(ResourceManager.Instance.GetResource(Itempathinfo.EffectPath + "502003054", typeof(object), enResourceType.Prefab).m_content as GameObject);
            clickEffect.transform.SetParent(game.transform);
            Vector3 pos = cam.ScreenToWorldPoint(eventData.position);
            Vector3 effectpos = new Vector3(pos.x, pos.y, pos.z + 50);
            Invoke("CloseEffect", 1f);
            clickEffect.transform.position = effectpos;
            //AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "603010708");
            seqStart.Kill();
            dragSeq = DOTween.Sequence();
            switch (UnityEngine.Random.Range(1, 3))
            {
                case 1:
                    dragSeq.Append(transform.DOLocalJump(new Vector3(3000f, 0f, 0f), 500f, 1, 2f));
                    break;
                case 2:
                    dragSeq.Append(transform.DOLocalJump(new Vector3(-3000f, 0f, 0f), 500f, 1, 2f));
                    break;

            }
            dragSeq.Insert(0f, transform.DOLocalRotate(new Vector3(0f, 0f, -720f), 2f, RotateMode.FastBeyond360));
            dragSeq.Play();
            this.game.TapThings();
            char letter = (char)StudyPhonicsManager.Instance.CurLetter;
            StartCoroutine(PlayLetterSound(letter));
        }

    }

    private void CloseEffect() {
        Destroy(clickEffect);
    }

   

    public void OnPointerDown(PointerEventData eventData)
    {
        dragBegin = eventData.position;
        //Debug.Log("dragBegin" + dragBegin);
       
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        
        dragEnd = eventData.position;
        if (Math.Abs(dragEnd.x -dragBegin.x)>=90f|| Math.Abs(dragEnd.y - dragBegin.y) >= 90f) 
        {
            isDrag = true;
            this.game.TapThings();
            Debug.Log("dragEnd" + dragEnd);
            Vector3 targetposition = this.transform.position + (dragEnd - dragBegin).normalized*2000;
            this.transform.DOLocalMove(targetposition , 1f);
            transform.DOLocalRotate(new Vector3(0f, 0f, -720f), 2f, RotateMode.FastBeyond360);
            clickEffect = Instantiate(ResourceManager.Instance.GetResource(Itempathinfo.EffectPath + "502003054", typeof(object), enResourceType.Prefab).m_content as GameObject);
            clickEffect.transform.SetParent(game.transform);
            Vector3 pos = cam.ScreenToWorldPoint(dragBegin);
            Vector3 effectpos = new Vector3(pos.x, pos.y, pos.z + 50);
            //AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "603010708");
            Invoke("CloseEffect", 1f);
            clickEffect.transform.position = effectpos;
            Invoke("SetDrag", 1f);
            char letter = (char)StudyPhonicsManager.Instance.CurLetter;
            StartCoroutine(PlayLetterSound(letter));
        }
        
    }

    private void SetDrag() {
        isDrag = false;
    }

    IEnumerator PlayLetterSound(char letter)
    {
        yield return new WaitForSeconds(0.2f);
        switch (letter)
        {
            case 'A':
                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020401");
                break;
            case 'C':
                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020403");
                break;
            case 'G':
                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020407");
                break;
            case 'J':
                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020410");
                break;
            case 'M':
                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020413");
                break;
            case 'P':
                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020416");
                break;
            case 'S':
                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020419");
                break;
            case 'V':
                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020422");
                break;
            case 'Y':
                AudioManager.Instance.PlaySound(Itempathinfo.SOUND + "401020425");
                break;

        }
    }
}

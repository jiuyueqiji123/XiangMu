﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PhonicsTapGame : PhonicsGameBase {

	private const int THING_NUM = 10;

	public GameObject TextObj;

	public Text BigText;

	public Text SmallText;

	public GameObject LetterObj;

	public Image LetterImage;

	public Text LetterText;

	private const string THING_PATH = "study_phonics/study_phonics_prefab/TapThing";



    private bool isInit;

	private List<Vector3> posList = new List<Vector3>();

	private List<PhonicsTapThing> things = new List<PhonicsTapThing>();

	private int curTapTime;

    private int curletter;

    private object Controller;

    private int a;

    public Transform[] textPos;

    private Tweener tw;


    private const string studyPhonicPath = "study_phonics/study_phonics_sound/";

    void Awake() {
        
        for (int i = 0; i < 3; i++) {
			posList.Add (new Vector3 (-275f + i * 275f, 150f, 0f));
		}
		for (int i = 0; i < 4; i++) {
			posList.Add (new Vector3 (-350f + i * 250f, 0f, 0f));
		}
		for (int i = 0; i < 3; i++) {
			posList.Add (new Vector3 (-275f + i * 275f, -150f, 0f));
		}
	}

	public override void Play ()
	{
        //AudioManager.Instance.PlayMusic(studyPhonicPath + "603010704", true, true);
        base.Play ();
        BigText.color = new Color32(42, 16, 16, 255);
        SmallText.color = new Color32(42, 16, 16, 255);
        curTapTime = 0;
        curletter = StudyPhonicsManager.Instance.CurLetter;
        SmallText.GetComponent<Animator>().runtimeAnimatorController = null;
        BigText.GetComponent<Animator>().runtimeAnimatorController = null;
        BigText.transform.position = textPos[0].position;
        SmallText.transform.position = textPos[1].position;


        if (!isInit) {
			for (int i = 0; i < THING_NUM; i++) {
				GameObject thing = ResourceManager.Instance.GetResource (THING_PATH, typeof(GameObject), enResourceType.Prefab, true).m_content as GameObject;
                
                GameObject thingObj = GameObject.Instantiate (thing) as GameObject;
				HUIUtility.SetParent (thingObj.transform, this.transform);
				PhonicsTapThing tap = thingObj.GetComponent<PhonicsTapThing> ();
				things.Add (tap);
				thingObj.SetActive (false);
                
            } 
        }
        isInit = true;

         a = Random.Range(1, 3);
        foreach (var temp in things)
        {
            switch ((char)curletter)
            {
                case 'A':
                    temp.GetComponent<Image>().sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path  + (a == 1 ? "ant" : "apple") + Random.Range(1, 5).ToString());
                    LetterImage.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path+ (a == 1 ? "ant" : "apple") + Random.Range(1, 5).ToString());
                    LetterText.text = "<color=#FF0000>A</color><color=#000000>" + (a == 1 ? "ant" : "apple").Replace("a",string.Empty)+ "</color>";
                    break;
                case 'C':
                    temp.GetComponent<Image>().sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path+(a == 1 ? "car" : "cat") + Random.Range(1, 5).ToString());
                    LetterImage.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "car" : "cat") + Random.Range(1, 5).ToString());
                    LetterText.text = "<color=#FF0000>C</color><color=#000000>" + (a == 1 ? "car" : "cat").Replace("c", string.Empty) + "</color>";
                    break;
                case 'G':
                    temp.GetComponent<Image>().sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "gift" : "goat") + Random.Range(1, 5).ToString());
                    LetterImage.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "gift" : "goat") + Random.Range(1, 5).ToString());
                    LetterText.text = "<color=#FF0000>G</color><color=#000000>" + (a == 1 ? "gift" : "goat").Replace("g", string.Empty) + "</color>";
                    break;
                case 'J':
                    temp.GetComponent<Image>().sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "jet" : "juice") + Random.Range(1, 5).ToString());
                    LetterImage.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "jet" : "juice") + Random.Range(1, 5).ToString());
                    LetterText.text = "<color=#FF0000>J</color><color=#000000>" + (a == 1 ? "jet" : "juice").Replace("j", string.Empty) + "</color>";
                    break;
                case 'M':
                    temp.GetComponent<Image>().sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "mango" : "monkey") + Random.Range(1, 5).ToString());
                    LetterImage.sprite= Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "mango" : "monkey") + Random.Range(1, 5).ToString());
                    LetterText.text = "<color=#FF0000>M</color><color=#000000>" + (a == 1 ? "mango" : "monkey").Replace("m", string.Empty) + "</color>";
                    BigText.transform.position = textPos[2].position;
                    SmallText.transform.position = textPos[3].position;
                    break;
                case 'P':
                    temp.GetComponent<Image>().sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "pig" : "panda") + Random.Range(1, 5).ToString());
                    LetterImage.sprite= Res.LoadSprite(StudyPath.english_daoju_sprite_path +(a == 1 ? "pig" : "panda") + Random.Range(1, 5).ToString());
                    LetterText.text = "<color=#FF0000>P</color><color=#000000>" + (a == 1 ? "pig" : "panda").Replace("p", string.Empty) + "</color>";
                    break;
                case 'S':
                    temp.GetComponent<Image>().sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path +(a == 1 ? "snake" : "sun") + Random.Range(1, 5).ToString());
                    LetterImage.sprite= Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "snake" : "sun") + Random.Range(1, 5).ToString());
                    LetterText.text = "<color=#FF0000>S</color><color=#000000>" + (a == 1 ? "snake" : "sun").Replace("s", string.Empty) + "</color>";
                    break;
                case 'V':
                    temp.GetComponent<Image>().sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path +(a == 1 ? "vase" : "vest") + Random.Range(1, 5).ToString());
                    LetterImage.sprite= Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "vase" : "vest") + Random.Range(1, 5).ToString());
                    LetterText.text = "<color=#FF0000>V</color><color=#000000>" + (a == 1 ? "vase" : "vest").Replace("v", string.Empty) + "</color>";
                    break;
                case 'Y':
                    temp.GetComponent<Image>().sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "yak" : "Yoyo") + Random.Range(1, 5).ToString());
                    LetterImage.sprite = Res.LoadSprite(StudyPath.english_daoju_sprite_path + (a == 1 ? "yak" : "Yoyo") + Random.Range(1, 5).ToString());
                    LetterText.text = "<color=#FF0000>Y</color><color=#000000>" + (a == 1 ? "ak" : "oyo")+ "</color>";
                    break;

            }
            LetterImage.SetNativeSize();
        }

        curTapTime = 0;
		SetText ();
		this.LetterObj.SetActive (false);
		for(int i=0; i < THING_NUM; i++) {
			things [i].transform.localScale = new Vector3 (0.3f, 0.3f, 0.3f);
			things [i].gameObject.SetActive (true);
			things [i].Init (this);
            things[i].transform.localPosition = posList[i];
        }
	}

	public override void End (bool done)
	{
        base.End (done);
        if (tw != null)
        {
            tw.Kill();
        }
	}



	private void SetText() {
		this.TextObj.SetActive (true);
		int c = StudyPhonicsManager.Instance.CurLetter;
		BigText.text = ((char)c).ToString ();
		SmallText.text = ((char)(c+32)).ToString ();
	}

	private void ShowLetter() {
       //暂时注释掉，以后可能有用
        //switch ((char)curletter)
        //{
        //    case 'A':
        //        AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + ((a == 1 ? "401020227" : "401020201")));
        //        break;
        //    case 'C':
        //        AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + ((a == 1 ? "401020229" : "401020203")));
        //        break;
        //    case 'G':
        //        AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + ((a == 1 ? "401020233" : "401020207")));
        //        break;
        //    case 'J':
        //        AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + ((a == 1 ? "401020236" : "401020210")));
        //        break;
        //    case 'M':
        //        AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + ((a == 1 ? "401020239" : "401020213")));
        //        break;
        //    case 'P':
        //        AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + ((a == 1 ? "401020242" : "401020216")));
        //        break;
        //    case 'S':
        //        AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + ((a == 1 ? "401020245" : "401020219")));
        //        break;
        //    case 'V':
        //        AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + ((a == 1 ? "401020248" : "401020222")));
        //        break;
        //    case 'Y':
        //        AudioManager.Instance.PlaySound(Itempathinfo.PhonicWordSound + ((a == 1 ? "401020251" : "401020225")));
        //        break;

        //}
        this.TextObj.SetActive (false);
		this.LetterObj.SetActive (true);
        
		this.LetterImage.transform.DOScale (new Vector3 (1.2f, 1.2f, 1.2f), 2f).OnComplete (new TweenCallback (delegate {
			End(true);
		}));
	}

	public void TapThings() {
    
        curTapTime++;
		if (curTapTime == THING_NUM) {
            switch ((char)curletter)
            {
                case 'A':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicA_Z + ((a == 1 ? "401020428" : "401020427")));
                    break;
                case 'C':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicA_Z + ((a == 1 ? "401020430" : "401020429")));
                    break;
                case 'G':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicA_Z + ((a == 1 ? "401020432" : "401020431")));
                    break;
                case 'J':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicA_Z + ((a == 1 ? "401020434" : "401020433")));
                    break;
                case 'M':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicA_Z + ((a == 1 ? "401020436" : "401020435")));
                    break;
                case 'P':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicA_Z + ((a == 1 ? "401020437" : "401020438")));
                    break;
                case 'S':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicA_Z + ((a == 1 ? "401020439" : "401020440")));
                    break;
                case 'V':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicA_Z + ((a == 1 ? "401020442" : "401020441")));
                    break;
                case 'Y':
                    AudioManager.Instance.PlaySound(Itempathinfo.PhonicA_Z + ((a == 1 ? "401020444" : "401020443")));
                    break;

            }
            AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "603010705");
            BigText.color = new Color32(50, 135, 221, 255);
            SmallText.color = new Color32(50, 135, 221, 255);
            Controller = ResourceManager.Instance.GetResource("Controller/502003055", typeof(object), enResourceType.Prefab, false).m_content as object;
            BigText.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Controller;
            SmallText.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Controller;
            tw = this.TextObj.transform.DOScale (new Vector3 (1.2f, 1.2f, 1.2f),4.4f).OnComplete (new TweenCallback (delegate {
				ShowLetter();
			}));
		}
	}
}

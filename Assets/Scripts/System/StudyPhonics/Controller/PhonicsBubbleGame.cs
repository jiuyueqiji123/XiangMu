﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class PhonicsBubbleGame : PhonicsGameBase {

	private const string BUBBLE_PATH = "study_phonics/study_phonics_prefab/Bubble";

	private const string LAST_PATH = "study_phonics/study_phonics_prefab/BubbleLast";

	private const int CREATE_TIME = 12;

	public List<Sprite> Things = new List<Sprite>();

	private GameObject bubbleObj;

	private List<char> letters = new List<char> () {
		'B', 'E', 'H', 'K', 'N', 'Q', 'T', 'W', 'Z'
	};

	private List<int> createNums = new List<int>() {
		1, 1, 2, 2, 3, 3, 3, 3, 3, 2,1,1
	};

	private List<Sprite> curThings = new List<Sprite> ();

	private int curCreateIndex;

	private Queue<PhonicsBubbleView> bubbleViews = new Queue<PhonicsBubbleView> ();

	private int curLetter;

	private GameObject LastBubble;

	private bool isInit;

	private List<PhonicsBubbleView> curShowView = new List<PhonicsBubbleView>();

    private GameObject go;

    private char num;

    private const string studyPhonicPath = "study_phonics/study_phonics_sound/";

    void Awake() {

	}

	public override void Play ()
	{
        //AudioManager.Instance.PlayMusic(studyPhonicPath + "603010801", true, true);

        base.Play ();
		curCreateIndex = 0;
		curShowView.Clear ();
		curThings.Clear ();
		curLetter = StudyPhonicsManager.Instance.CurLetter;
        num = (char)curLetter;
        //Debug.Log(num);

        int index = letters.IndexOf ((char)curLetter);
        ////测试
        //index= 0;
       
        //curThings = Things.GetRange (index * 4, 4);

		if (!isInit) {
			bubbleObj = ResourceManager.Instance.GetResource (BUBBLE_PATH, typeof(GameObject), enResourceType.Prefab, true).m_content as GameObject;
			for (int i = 0; i < 20; i++) {
				 go = GameObject.Instantiate (bubbleObj) as GameObject;
				go.SetActive (false);
                
				HUIUtility.SetParent (go.transform, this.transform);
				bubbleViews.Enqueue (go.GetComponent<PhonicsBubbleView> ());
			}

			GameObject last = ResourceManager.Instance.GetResource (LAST_PATH, typeof(GameObject), enResourceType.Prefab, true).m_content as GameObject;
			LastBubble = GameObject.Instantiate (last) as GameObject;
			LastBubble.SetActive (false);
			HUIUtility.SetParent (LastBubble.transform, this.transform);

			isInit = true;
		}
     

		LastBubble.SetActive (false);
		this.CreateBubble (0);
		TimerManager.Instance.AddTimer (1500, CREATE_TIME, new Timer.OnTimeUpHandler (this.CreateBubble));

        //Debug.Log(bubbleViews.Count);
    }

	public override void End (bool done)
	{
        base.End (done);
		TimerManager.Instance.RemoveTimer (new Timer.OnTimeUpHandler (this.CreateBubble));
		ResourceManager.Instance.RemoveCachedResource (BUBBLE_PATH);
        foreach(PhonicsBubbleView view in curShowView)
        {
            view.Recycle ();
		}
        curShowView.Clear();
        LastBubble.GetComponent<PhonicsBubbleLast>().killAnim() ;
    }

	public void ClickBubble(bool last) {

	}

    private void CreateBubble(int seq) {
        //Debug.Log(" curCreateIndex ===  " + curCreateIndex);        
        if (curCreateIndex == CREATE_TIME) {
			Debug.LogError ("最后一个");
			TimerManager.Instance.RemoveTimer (new Timer.OnTimeUpHandler (this.CreateBubble));
			LastBubble.transform.localPosition = new Vector3 (0f, -900f, 0f);
            LastBubble.SetActive(true);
            LastBubble.GetComponent<PhonicsBubbleLast> ().Init (this, curLetter);
			return;
		}
		float pre = 1000f / CreateNum;
		for (int i=0; i < CreateNum; i++) {
			PhonicsBubbleView view = bubbleViews.Dequeue ();
			float y = Random.Range (-900f-i*50f, -800f);
			float x = Random.Range (i * pre, (i + 1) * pre - 100f);
			view.transform.localPosition = new Vector3 (-500f + x, y, 0f);
			view.gameObject.SetActive (true);
			RandomBubble (view);
			curShowView.Add (view);
		}
		curCreateIndex++;
	}

	private void RandomBubble(PhonicsBubbleView view) {
		if (Random.Range (0, 2) == 0) {
			int bs = Random.Range (0, 2) == 0 ? 0 : 32;
			view.Init (this, curLetter + bs,'0');
		} else {
			int ind = Random.Range (0, 4);
			view.Init(this, curLetter,num);
		}
	}

	private int CreateNum {
		get {
			return createNums [curCreateIndex];
		}
	}

	public void RecycleBubble(PhonicsBubbleView view, bool removeList = true) {
        //Debug.LogError("remove " + view + "," + removeList + "," + this.bubbleViews.Count);
		view.gameObject.SetActive (false);
		this.bubbleViews.Enqueue (view);
		if (removeList)
			this.curShowView.Remove (view);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PhonicsEggGame : PhonicsGameBase {

	public List<PhonicsEggView> Eggs = new List<PhonicsEggView>();

	public PhonicsEggBreak eggBreak;

	private Vector3 initScale = new Vector3 (0.7f, 0.7f, 0.7f);

	private List<Vector3> eggPos = new List<Vector3> ();

	private int curIndex = 0;

    private int curletter = 0;

    private int randomNum = 0;

    public GameObject breakobj;

    private Sequence seq;

    private const string studyPhonicPath = "study_phonics/study_phonics_sound/";


    void Awake() {
		
        
	}

	public override void Play ()
	{
        for (int i = 0; i < Eggs.Count; i++)
        {
            eggPos.Add(Eggs[i].transform.localPosition);
            
            Eggs[i].Init(this);
        }
        base.Play ();
		Init ();
		curIndex = 0;
        eggBreak.Init (this);
		MoveBig ();
        curletter = StudyPhonicsManager.Instance.CurLetter;
        //AudioManager.Instance.PlayMusic(studyPhonicPath + "603010904", true, true);

    }

	public override void End (bool done)
	{
        base.End (done);
        if(PhonicsEggView.tw !=null) {
            PhonicsEggView.tw.Kill();
        }
    
	}

	private void Init() {
		for (int i = 0; i < Eggs.Count; i++) {
			Eggs [i].transform.localPosition = eggPos [i];
			Eggs [i].transform.localScale = initScale;
		}
	}

	private void MoveBig() {
        AudioManager.Instance.PlaySound(Itempathinfo.PhonicEggGamePath + "603010903");
        breakobj.transform.SetParent(Eggs[curIndex].transform);
        breakobj.transform.localPosition = Vector3.zero;
        breakobj.transform.localScale = new Vector3(0.83f, 0.83f, 0.83f);
		 seq = DOTween.Sequence ();
       
		seq.Append (Eggs [curIndex].transform.DOLocalMove (Vector3.zero, 0.5f, true));
		seq.Append (Eggs [curIndex].transform.DOScale (new Vector3(1.2f,1.2f,1.2f), 0.5f));
		seq.OnComplete (new TweenCallback (delegate {
            eggBreak.Show();
            object Controller = ResourceManager.Instance.GetResource("Controller/502003054", typeof(object), enResourceType.Prefab, false).m_content as object;
            Eggs[curIndex].GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Controller;


        }));
		seq.Play ();
	}

	public void BreakEgg() {
        randomNum = Random.Range(1, 3);
        eggBreak.Hide ();
		Eggs [curIndex].ShowBreak ((char)curletter ,curIndex,randomNum);
        
	}

	public void MoveSmall() {

        Eggs[curIndex].transform.Find("Thing").GetComponent<Animator>().runtimeAnimatorController = null;
        Sequence seq = DOTween.Sequence ();
		seq.Append (Eggs [curIndex].transform.DOLocalMove (eggPos[curIndex], 0.5f, true));
		seq.Insert (0f, Eggs [curIndex].transform.DOScale (initScale, 0.5f));
		seq.OnComplete (new TweenCallback (delegate {
			NextEgg();
		}));
		seq.Play ();
	}

	private void NextEgg() {
		if (curIndex == 3) {
			End (true);
			return;
		}
		curIndex++;
		MoveBig ();
	}
}

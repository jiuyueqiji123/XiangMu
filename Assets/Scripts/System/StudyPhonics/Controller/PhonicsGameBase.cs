﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhonicsGameBase : MonoBehaviour {

	public virtual void Play() {
		this.gameObject.SetActive (true);
	}

	public virtual void End(bool done) {
		
		EventBus.Instance.BroadCastEvent<bool> (EventID.PHONICS_GAME_END, done);
        
	}

    public void SetActive() {
        this.gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StudyPhonicesModel {



	public StudyPhonicesModel() {

	}
 
	public emPhonicsGameType GetGameType(int index) {
		return GetGameType ((char)index);
	}

	public emPhonicsGameType GetGameType(char chr) {
		switch (chr) {
		case 'A':
		case 'C':
		case 'G':
		case 'J':
		case 'M':
		case 'P':
		case 'S':
		case 'V':
		case 'Y':
			return emPhonicsGameType.Tap;
		case 'B':
		case 'E':
		case 'H':
		case 'K':
		case 'N':
		case 'Q':
		case 'T':
		case 'W':
		case 'Z':
			return emPhonicsGameType.Bubble;
		case 'D':
		case 'F':
		case 'I':
		case 'L':
		case 'O':
		case 'R':
		case 'U':
		case 'X':
			return emPhonicsGameType.Egg;
		default:
			return emPhonicsGameType.None;
		}
	}
}

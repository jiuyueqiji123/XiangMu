﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum emPhonicsGameType {
	None,
	Tap,
	Bubble,
	Egg,
}

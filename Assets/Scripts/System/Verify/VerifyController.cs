﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerifyController : Singleton<VerifyController> {

    private VerifyView view;

    private Action verifyBack;

    public override void Init()
    {
        base.Init();
        this.view = new VerifyView();
    }

    public override void UnInit()
    {
        base.UnInit();
    }

    public void OpenView(Action action)
    {
        this.view.OpenForm();
        this.verifyBack = action;
    }

    public void CloseView()
    {
        this.view.CloseForm();
    }

    public void VerifyResult()
    {
        this.CloseView();
        if (verifyBack != null)
        {
            verifyBack();
        }
    }
}

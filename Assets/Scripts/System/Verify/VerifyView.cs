﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerifyView  {

    private const string VIEW_PATH = "ui/ui_parent/VerifyForm";

    private GameObject form;

    private bool isInit = false;

    private VerifyPanel panel;

    public void OpenForm()
    {
        if (this.form == null)
        {
            GameObject go = ResourceManager.Instance.GetResource(VIEW_PATH, typeof(GameObject), enResourceType.ScenePrefab).m_content as GameObject;
            this.form = GameObject.Instantiate(go);
            GameObject.DontDestroyOnLoad(this.form);
        }
        this.form.SetActive(true);
        this.Init();
    }

    public void CloseForm()
    {
        if (this.form != null && this.form.activeSelf)
        {
            this.form.SetActive(false);
        }
    }

    private void Init()
    {
        if (!isInit)
        {
            isInit = true;
            this.panel = this.form.transform.Find("Panel").GetComponent<VerifyPanel>();
        }
        this.panel.Init();
    }
}

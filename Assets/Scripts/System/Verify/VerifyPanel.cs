﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerifyPanel : MonoBehaviour {

    public Text FirstText;
    public Text SecondText;
    public Text ResultText;

    private int first;
    private int second;
    private int result;

    public void Init()
    {
        first = Random.Range(2, 10);
        second = Random.Range(2, 10);
        result = 0;
        FirstText.text = first.ToString();
        SecondText.text = second.ToString();
        SetResultText();
    }

    private void SetResultText()
    {
        if (result == 0)
        {
            ResultText.text = "";
        }
        else
        {
            ResultText.text = result.ToString();
        }
    }

    public void NumBtnClick(int num)
    {
        if (result > 9)
        {
            return;
        }
        if (result == 0)
        {
            result = num;
        }
        else
        {
            result = result * 10 + num;
        }
        SetResultText();
    }

    public void DeleteBtnClick()
    {
        result = 0;
        SetResultText();
    }

    public void OkBtnClick()
    {
        if (first + second == result)
        {
            VerifyController.Instance.VerifyResult();
        }
        else
        {
            DeleteBtnClick();
        }
    }

    public void CloseBtnClick()
    {
        EventBus.Instance.BroadCastEvent(EventID.CANCEL_VERITY);
        VerifyController.Instance.CloseView();
    }
}

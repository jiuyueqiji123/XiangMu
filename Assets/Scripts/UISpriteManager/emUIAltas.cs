﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.ComponentModel;

public enum emUIAltas {

    [Description("atlas/atlas_common/CommonAtlas")]
    CommonAtlas = 10001,

    [Description("main_controller/main_controller_atlas/MainSpriteAtlas")]
	Main = 20001,

    // math
    [Description("study_math/study_math_atlas/Atlas_Math_DrawNumber")]
    Math_DrawNumber = 30001,

    // english
    [Description("study_english/study_english_atlas/Atlas_English_SelectLetterScene")]
    English_SelectLetterScene = 40001,
    [Description("study_english/study_english_atlas/Atlas_English_SelectGame")]
    English_SelectGame,
    [Description("study_english/study_english_atlas/Atlas_English_TV")]
    English_TV,

    [Description("study_english_color/study_english_color_atlas/Atlas_English_Colours")]
    English_Colours,

    //[Description("game_puzzle/game_puzzle_atlas_email/EmailAtlas")]
    //Email,
    //[Description("game_puzzle/game_puzzle_atlas_email/EmailAtlas1")]
    //Email1,
    //[Description("game_puzzle/game_puzzle_atlas_email/EmailPictureAtlas")]
    //EmailPicture,
    //[Description("game_puzzle/game_puzzle_atlas_email/EmailPicture1Atlas")]
    //EmailPicture1,
    //[Description("game_puzzle/game_puzzle_atlas_email/EmailPuzzle1Atlas")]
    //EmailPuzzle1,
    //[Description("game_puzzle/game_puzzle_atlas_email/EmailPuzzle2Atlas")]
    //EmailPuzzle2,
    //[Description("game_puzzle/game_puzzle_atlas_email/EmailPuzzle3Atlas")]
    //EmailPuzzle3,
    //[Description("game_puzzle/game_puzzle_atlas_email/EmailPuzzle_db")]
    //EmailPuzzle_db,


    [Description("atlas/atlas_Express/ExpressAltas_01")]
    Express_01,

    [Description("game_hospital/game_hospital_atlas/HospitalAtlas")]
    Hospital,

    //[Description("study_phonics/study_phonics_altas/PhonicsTapGameAtlas")]
    //PhonicsTapGameAtlas = 70001,
    //[Description("study_phonics/study_phonics_altas/PhonicsTapGame1Atlas")]
    //PhonicsTapGame1Atlas = 70002,
    //[Description("study_phonics/study_phonics_altas/PhonicsTapGame2Atlas")]
    //PhonicsTapGame2Atlas = 70003,
    //[Description("study_phonics/study_phonics_altas/PhonicsBubbleGameAtlas")]
    //PhonicsBubbleGameAtlas = 70004,
    //[Description("study_phonics/study_phonics_altas/PhonicsEggGameAtlas")]
    //PhonicsEggGameAtlas = 70005,
    //[Description("study_phonics/study_phonics_altas/PhonicsEggGame1Atlas")]
    //PhonicsEggGame1Atlas = 70006,
    [Description("study_phonics/study_phonics_altas/EggAtlas")]
    EggAtlas,


    // CakeShop
    [Description("game_cake/game_cake_atlas/CakeAtlas")]
    CakeAtlas,

    // Addition
    [Description("study_addition/study_addition_atlas/AdditionAtlas")]
    AdditionAtlas,

    // ChineseRestaurant
    [Description("game_restaurant_zh/game_restaurant_zh_atlas/ChineseRestaurantAtlas")]
    ChineseRestaurantAtlas,
    [Description("paint_asset/atlas_paint/paintAtlas")]
    PaintAtlas,



    [Description("super_market/super_market_atlas/SuperMarketAtlas")]
    SuperMarketAtlas,

    [Description("study_matrix/study_matrix_atlas/StudyMatrix_atlas")]
    StudyMatrixAtlas,



    [Description("study_cardgame/study_cardgame_atlas/study_cardgame_atlas")]
    Card_Game,

    [Description("game_hospital/game_hospital_atlas/HospitalAtlas")]
    Game_Hospital,


    #region package-english

    [Description("package_english/package_english_arbaike_altas/arbaike")]
    arbaike,
    [Description("package_english/package_english_arbaike_altas/arcommon")]
    arcommon,

    #endregion


}
